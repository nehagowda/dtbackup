@pdl
Feature: PDL Smoke Tests

  Background:
    Given I go to the pdl homepage

  @smoketest_pdl
  @10720
  Scenario Outline: PDL_Auto_Driving Detials UI (ALM #10720)
    Then I verify that the "Enter" button is disabled
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    Then I am brought to the page with header "SELECT YOUR VEHICLE" in pdl
    And  I verify the zip code is set to "<ZipCode>"
    And  I verify the city and state are set to "<CityState>"
    And  I verify the default values of the car dropdowns
    And  I verify that the Miles Driven value is "15"
    And  I verify that the Tire Size value is "<TireSize>"
    And  I verify that the "Everyday" Driving Priority is selected
    And  I verify the "Everyday" Driving Priorities
    And  I verify that the "View Tire Results" button is disabled

    Examples:
      | StoreID | PayrollID | ZipCode | CityState  | TireSize   |
      | AZP20   | 919919    | 85250   | Scottsdale | 000/00 R00 |

  @smoketest_pdl
  @10728
  @10166
  Scenario Outline: PDL_Auto_Recommended Tires (ALM #10728)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    And  I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    When I select View Tire Results
    Then I verify the "Everyday" summary Driving Priorities

    Examples:
      | StoreID | PayrollID | Year | Make  | Model | Trim     | Assembly | TireSize   |
      | AZP20   | 919919    | 2012 | Honda | Civic | Coupe DX | none     | 195/65 R15 |

  @smoketest_pdl
  @9817
  @9974
  @9840
  @DrivingPrioritiesPerformance
  Scenario Outline: PDL_DD2A_Driving Priorities_Performance (ALM #9817)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    And  I switch to performance for driving priorities
    Then I verify the "performance" Driving Priorities
    When I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    When I select View Tire Results
    Then I verify the summary zip code is set to "<ZipCode>"

    Examples:
      | StoreID | PayrollID | ZipCode | Year | Make  | Model | Trim     | Assembly | TireSize   |
      | MNM29   | 919919    | 55125   | 2012 | Honda | Civic | Coupe DX | none     | 195/65 R15 |

  @smoketest_pdl
  @10840
  @standalonevehicleoptionaltirestaggered
  Scenario Outline: PDL_DD5-05_Vehicle-OE TIRE SIZE - StandAlone_Vehicle Optional tire_Staggered (ALM #10840)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    And  I select a vehicle with details and no assembly "<Year>" "<Make>" "<Model>" "<Trim>"
    Then I verify that the Tire Size value is "<TireSize>"
    And  I verify that the "O.E." tire size category is displayed in pdl
    When I select the "edit" button in pdl
    And  I click on "<TireDiameter>" in pdl
    And  I select optional tire size fitment "<OptionalTireSize>" in pdl
    Then I verify that the Tire Size value is "<NewTireSize>"
    And  I verify the option label is set to "<OptionalValue>"

    Examples:
      | StoreID | PayrollID | ZipCode | Year | Make       | Model       | Trim  |  TireSize   | OptionalTireSize      | TireDiameter | NewTireSize | OptionalValue |
      | MNM29   | 919919    | 55125   | 2010 | Chevrolet  | Corvette    | Base  |  245/40 R18 | 195/45-17 - 245/35-18 | 17           | 195/45 R17  | - 1           |

  @smoketest_pdl
  @10838
  @tirecomparisonengagestaggered
  Scenario Outline: PDL_RE3-01_RESULTS_Tire Comparison Engage_Staggered (ALM #10838)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    And  I select a vehicle with details and no assembly "<Year>" "<Make>" "<Model>" "<Trim>"
    Then I verify that the Tire Size value is "<TireSize>"
    When I select View Tire Results
    When I select the product checkbox at position "0" from the products list results
    And  I select the product checkbox at position "3" from the products list results
    Then I verify that compare tires button is "enabled" on results page
    When I select the compare tires button
    Then I verify tire comparison page header is "Tire Results"

    Examples:
      | StoreID | PayrollID | Year | Make       | Model        | Trim    | TireSize   | ItemId1 | ItemId2 |
      | MNM29   | 919919    | 2010 | Chevrolet  | Corvette     | Base    | 245/40 R18 | 19600   | 31380   |

  @smoketest_pdl
  @9846
  Scenario Outline: PDL_RE2-02_RESULTS_FILTER RESULTS-Brands (ALM # 9846)
    """TODO - will fail in QA due only one Michelin product """
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    And  I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select View Tire Results
    And  I wait for "3" seconds
    And  I save total number of tires displayed
    Then I verify that the filter window "displays"
    When I select filter "Brands"
    And  I select "Michelin" checkbox
    Then I verify that only tires of brand "<Brand1>" are listed in results
    When I select filter "Tire Type"
    And  I wait for "2" seconds
    And  I select "Performance Tires" checkbox
    And  I select filter "Other"
    And  I select "Best Seller" checkbox
    And  I select filter "Clear Filters"
    And  I wait for "3" seconds
    Then I verify the total tire results matches the total displayed when I launched the page

    Examples:
      | StoreID | PayrollID | Year | Make  | Model | Trim     | Assembly | Brand1   |
      | MNM29   | 919919    | 2012 | Honda | Civic | Coupe DX | none     | Michelin |

  @smoketest_pdl
  @9976
  @primaryDrivingLocationChangeInZipCode
  Scenario Outline: PDL_DD4-04_Primary Driving Location - change zip code(ALM #9976)
  """TODO - login to the application and update the zip to invalid """
    When I update the zip code to "<ZipCode>"
    Then I verify the zip code is set to "<ZipCode>"
    And  I verify the city and state are set to "<CityState>"

    Examples:
      | StoreID | PayrollID | DefaultTireSize | Year | Make  | Model | Trim     | Assembly | TireSize   | CityState      | ZipCode | Miles |
      | MNM29   | 919919    | XXX/XX XX       | 2012 | Honda | Civic | Coupe DX | none     | 195/65 R15 | SCOTTSDALE, AZ | 85250   | 30K   |

  @smoketest_pdl
  @10834
  @resultsConfirmMainPerformance
  Scenario Outline: PDL_RE1-01_RESULTS_1Confirm MAIN_Performance (ALM #10834)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    When I switch to performance for driving priorities
    Then I verify the "performance" Driving Priorities
    Examples:
      | StoreID | PayrollID | ZipCode | CityState      | Year | Make  | Model | Trim     | Assembly | TireSize   | MilesDriven | Brand    | TireName    |
      | MNM29   | 919919    | 55125   | SAINT PAUL, MN | 2012 | Honda | Civic | Coupe DX | none     | 195/65 R15 | 15000       | Michelin | Premier A/S |

 