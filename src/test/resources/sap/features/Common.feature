@sap
@common
Feature: Common

  @sapLogin
  Scenario Outline: maintain sap passwords
    When I set Module baseUrl to "<Module>" in "<Environment>" Environment
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username "<User Name>", and Password to "<Module>" for dataset "<Environment>"
    And  I enter t-code "<TCode>" in the command field for module "<Module>"

    Examples:
      | User Name  | Module    | Environment | TCode         |
      | AUTOUSER01 | SAP ECC   | QA          | migo          |
      | AUTOUSER02 | SAP ECC   | QA          | migo          |
      | AUTOUSER03 | SAP ECC   | QA          | migo          |
      | AUTOUSER04 | SAP ECC   | QA          | migo          |
      | AUTOUSER05 | SAP ECC   | QA          | migo          |
      | AUTOUSER01 | SAP ECC   | STG         | migo          |
      | AUTOUSER02 | SAP ECC   | STG         | migo          |
      | AUTOUSER03 | SAP ECC   | STG         | migo          |
      | AUTOUSER04 | SAP ECC   | STG         | migo          |
      | AUTOUSER05 | SAP ECC   | STG         | migo          |
      | AUTOUSER01 | SAP POSDM | QA          | /n/posdw/mon0 |
      | AUTOUSER02 | SAP POSDM | QA          | /n/posdw/mon0 |
      | AUTOUSER03 | SAP POSDM | QA          | /n/posdw/mon0 |
      | AUTOUSER04 | SAP POSDM | QA          | /n/posdw/mon0 |
      | AUTOUSER01 | SAP POSDM | STG         | /n/posdw/mon0 |
      | AUTOUSER02 | SAP POSDM | STG         | /n/posdw/mon0 |
      | AUTOUSER03 | SAP POSDM | STG         | /n/posdw/mon0 |
      | AUTOUSER04 | SAP POSDM | STG         | /n/posdw/mon0 |
      | AUTOUSER01 | SAP POSDT | QA          | /n/posdw/mon0 |
      | AUTOUSER02 | SAP POSDT | QA          | /n/posdw/mon0 |
      | AUTOUSER03 | SAP POSDT | QA          | /n/posdw/mon0 |
      | AUTOUSER04 | SAP POSDT | QA          | /n/posdw/mon0 |
      | AUTOUSER01 | SAP POSDT | STG         | /n/posdw/mon0 |
      | AUTOUSER02 | SAP POSDT | STG         | /n/posdw/mon0 |
      | AUTOUSER03 | SAP POSDT | STG         | /n/posdw/mon0 |
      | AUTOUSER04 | SAP POSDT | STG         | /n/posdw/mon0 |
      | AUTOUSER01 | SAP ECC   | DEV         | migo          |
      | AUTOUSER02 | SAP ECC   | DEV         | migo          |
      | AUTOUSER03 | SAP ECC   | DEV         | migo          |
      | AUTOUSER04 | SAP ECC   | DEV         | migo          |