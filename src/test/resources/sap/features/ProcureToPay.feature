@sap
@ProcureToPay
Feature: Procure to pay

  Scenario Outline: FC_07 - Create Payment Proposal - F110
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode1>" in the command field
    And  I enter "<Vendor>" from preferred data source into the field with label "Vendor account"
    And  I hit return
    And  I click on "All items"
    And  I enter the purchase order value for "Posting Date From" to "<From Date>" from preferred data source
    And  I enter the purchase order value for "Posting Date To" to "<To Date>" from preferred data source
    And  I execute
    And  I save the document number to the scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter the unique 5 digit identification code in f110
    And  I click on partial text "Parameter"
    And  I enter the purchase order value for "Company Codes In Payment Control" to "<Company Code>" from preferred data source
    And  I enter the purchase order value for "Pmt meths" to "<Pmt meths>" from preferred data source
    And  I hit tab
    And  I enter "future" date with "2" months into "Next p/date" element
    And  I click on "Free selection"
    And  I click on "Field Name"
    And  I click on the "drop down" element in po page to navigate
    And  I switch to the first iFrame in SAP
    And  I click on partial text "Document"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter the document number into "Values"
    And  I click on "Additional Log"
    And  I click on "Due date check"
    And  I click on "Payment method selection in all cases"
    And  I click on "Line items of the payment documents"
    And  I click on "Printout/data medium"
    And  I enter the purchase order value for "RFFOUS_C" to "<Payment Type>" from preferred data source
    And  I click on partial text "Parameter"
    And  I enter "<Vendor>" from preferred data source into the field with label "Vendor"
    And  I click on "Status"
    And  I switch to the first iFrame in SAP
    And  I click on "Yes"
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    And  I check for element with text "Parameters have been entered"
    When I click on element with title attribute value "Schedule Proposal"
    And  I switch to the first iFrame in SAP
    And  I click on "Start immediately"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment proposal has been created"
    When I click on element with title attribute value "Schedule payment run"
    And  I switch to the first iFrame in SAP
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment run has been carried out"
    When I click on element with title attribute value "Schedule Print"
    And  I switch to the first iFrame in SAP
    And  I enter the unique printout job name in "Job name" in f110
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP

    Examples:
      | TCode1   | Vendor | From Date | To Date    | TCode2  | Company Code | Pmt meths | Payment Type |
      | /n FBL1N | 10226  | 1/1/2018  | 12/31/2019 | /n F110 | 1000         | C         | ZCHECK1000   |

  @2999
  @3021
  @3022
  @16972
  Scenario Outline: Create Non Merch PO in SAP_ME21N, posting in SAP_MIGO, validating in SAP_ME23N and generating and invoice reciept in SAP_MIRO (ALM#2999,3021,3022 and 16972 )
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I click on "Org. Data"
    And  I enter "<Purchase Group>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Asset Category" to "<Asset Category>" from preferred data source
    And  I hit return
    And  I collapse header tab
    And  I click on partial text "Account Assignment"
    And  I enter the Asset ID based on the environment
    And  I click on partial text "Conditions"
    Then I save the "Net Cost" value from the page
    When I click on partial text "Confirmations"
    And  I enter " " into the field with label "Conf. Control"
    And  I hit return
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on element with title attribute value "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    Then I verify Item detail tab is open
    When I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on partial text "Purchase Order History"
    Then I verify the article document matches the previously saved number
    When I enter t-code "<TCode3>" in the command field
    And  I enter "present" date with "0" months into "Invoice date" element
    And  I enter "Automation" with random number into "Reference" in Incoming Invoice page
    And  I enter saved "Net Cost" element value into following element "Amount" in the page
    And  I enter previously saved PO into field with title "Purchasing Document"
    And  I hit return
    And  I click on the "Booking OK" element in po page to navigate
    And  I scroll and enter the purchase order value for "Tax Code in PO Reference" to "I0 (A/P Tax Exempt)" from preferred data source
    And  I click on element with title attribute value "Post"
    Then I verify the success status in SAP
    When I save the article document number to scenario data
    And  I enter t-code "<TCode4>" in the command field
    And  I click on "Payment transactions"
    And  I hit return
    And  I save the "Payment methods" value from the page
    And  I click on element with title attribute value "Display"
    And  I click on element with title attribute value "Save"
    And  I enter t-code "<TCode5>" in the command field
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter the unique 5 digit identification code in f110
    And  I save the "Identification" value from the page
    And  I click on partial text "Parameter"
    And  I enter the purchase order value for "Company Codes In Payment Control" to "<Company Code>" from preferred data source
    And  I enter saved "Payment methods" element value into following table element "Pmt meths" in the page
    And  I hit tab
    And  I enter "future" date with "5" months into "Next p/date" element
    And  I click on "Free selection"
    And  I click on "Field Name"
    And  I click on the "drop down" element in po page to navigate
    And  I switch to the first iFrame in SAP
    And  I click on partial text "Document"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter the Article document number into "Values"
    And  I click on "Additional Log"
    And  I click on "Due date check"
    And  I click on "Payment method selection in all cases"
    And  I click on "Line items of the payment documents"
    And  I click on "Printout/data medium"
    And  I enter the purchase order value for "Payment type in F110" to "<Variant>" from preferred data source
    And  I click on partial text "Parameter"
    And  I enter "<Vendor>" from preferred data source into the field with label "Vendor"
    And  I click on "Status"
    And  I switch to the first iFrame in SAP
    And  I click on "Yes"
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    And  I check for element with text "Parameters have been entered"
    When I click on element with title attribute value "Schedule Proposal"
    And  I switch to the first iFrame in SAP
    And  I click on "Start immediately"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment proposal has been created"
    When I click on element with title attribute value "Schedule payment run"
    And  I switch to the first iFrame in SAP
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment run has been carried out"
    When I click on element with title attribute value "Schedule Print"
    And  I switch to the first iFrame in SAP
    And  I enter the unique printout job name in "Job name" in f110
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I click on element with title attribute value "Display Proposal"
    Then I verify Payment Success
    When I save the time "before" generating the file
    And  I enter t-code "<TCode6>" in the command field
    And  I click on element with title attribute value "Get Variant"
    And  I switch to the first iFrame in SAP
    And  I click on partial data "ACH_MERGE_1000"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter saved "Identification" element value into following element "Identification" in the page
    And  I execute
    And  I hit return
    And  I switch to the first iFrame in SAP
    Then I check for element with text "payments were selected"
    And  I check for element with text "Payment medium run"
    And  I check for element with text "Payment medium program is started in separate job"
    When I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "15" seconds
    And  I save the time "after" generating the file
    And  I enter t-code "<TCode7>" in the command field
    And  I enter the Article document number into "Document Number"
    And  I hit return
    And  I enter t-code "<TCode8>" in the command field
    And  I double click on "Second Dir_Comm in Al11" element
    And  I double click on "int" element
    And  I double click on "o" element
    And  I double click on "banking"
    And  I select both Lastchange columns using control key
    And  I click on element with title attribute value "Set Filter"
    And  I switch to the first iFrame in SAP
    And  I enter "present" date with "0" months into "Last Changed On" element
    And  I hit tab
    And  I enter saved timestamps "before" file creation in "Time before file creation in Al11" field
    And  I enter saved timestamps "after" file creation in "Time after file creation in Al11" field
    And  I click on "CONTINUE" in order to cash page
    And  I hit return
    And  I switch back to the main window
    Then I check for element with text "Wells_Fargo_achpayments_1000_"

    Examples:
      | TCode   | Vendor | Purchase Group | Article | PO Quantity | Site | TCode1 | TCode2  | Asset Category | TCode3 | PO Type           | TCode4 | TCode5 | Company Code | TCode7 | Variant | TCode6  | TCode8 |
      | /nME21N | 14816  | 04             | 91506   | 2           | 1434 | /nMIGO | /nME23N | A              | /nMIRO | Corp Non Merch PO | /nXK03 | /nF110 | 1000         | /nfb03 | ACH1000 | /nfbpm1 | /nAL11 |

  @12513
  Scenario Outline: New PO Doc Type ZFDS (ALM#12513)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    Then I verify if item Overview is open
    When I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I select the input field with label "Purch. Group" at position "1" and enter "<Purchase Group>"
    And  I verify if item Overview is open
    And  I enter the PO value for "Article Number" to "<Article>", "Quantity" to "<Quantity>", "Site" to "<Site>" from preferred data source
    And  I hit return
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode2>" in the command field
    Then I verify Item detail tab is open
    When I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Messages"
    Then I validate "Medium Input" entered matches original "<Value>" in order output
    When I click on element with title attribute value "Back"
    And  I collapse header tab
    And  I verify Item detail tab is open
    And  I click on partial text "Conditions"
    And  I click on element with title attribute value "Pricing log"
    Then I check for element with text "ZELPG1"

    Examples:
      | TCode    | Vendor | Article | Quantity | Site | Purchase Group | PO Type           | TCode2   | Value | Text   |
      | /n ME21N | 26418  | 29487   | 1        | 9301 | 01             | Factory Direct PO | /n ME23N | EDI   | ZELPG1 |

  @9055
  @crossDockRegression
  Scenario Outline:STORE - NEW CD Receiving HH_MIM - GI Posted Before Receiving (PB) and GR Posted Successfully
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select "Transaction Code" and enter "WSRS_HI_MENU" in common tcodes
    And  I click on element with title attribute value "Test"
    And  I select "Scanner Site" and set the site "<Start Site>" in scanner
    And  I select "PDC Type" and set the site "<PDC Type>" in scanner
    And  I click on "ENT OK"
    And  I select "Scanner Select" and enter "<Goods Receipt>" in common tcodes
    And  I click on "ENT OK"
    And  I select "Scanner Select" and enter "<Cross Dock Orders>" in common tcodes
    And  I click on "ENT OK"
    And  I enter "<Invalid Deliveries 1>" into the field with label "Tot.Deliveries"
    And  I click on "ENT OK"
    Then I check for element with text "Error:"
    When I click on "ENT OK"
    And  I enter "<Invalid Deliveries 2>" into the field with label "Tot.Deliveries"
    And  I click on "ENT OK"
    Then I check for element with text "Error:"

    Examples:
      | TCode   | PDC Type | Start Site | Goods Receipt | Cross Dock Orders | Invalid Deliveries 1 | Invalid Deliveries 2 |
      | /n SE93 | MC9190   | 1022       | 1             | 1                 | 250                  | CISCO                |

  @2998
  Scenario Outline: DP_07 - Create Non Merch PO (no article) - Store & DTD
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I click on "Org. Data"
    And  I enter "<Purch. Group>" from preferred data source into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I hit return
    And  I save the "Short Text" value from the page
    And  I save the "Net Price" value from the page
    And  I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "Ordering" in NWBC page
    And  I click on "Create Non Merchandise Order" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I enter "Back Room Supplies" into the field with label "Merch. Category"
    And  I enter "<Vendor>" from preferred data source into the field with label "Vendor"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "Short Text" previously saved value into column name "Description" at line number "1" in items table in nwbc
    And  I enter "Net Price" previously saved value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image

    Examples:
      | TCode | Vendor | Purch. Group | Article | Quantity | PO Type           |
      | ME21N | 14816  | 04           | 91506   | 2        | Corp Non Merch PO |

  @7216
  Scenario Outline: AGM # 11732 - MIM - Creation of New Doc Types ZSUB and ZDUB For Store and DTD Transfers (ALM#7216)
  """ Scenario 1 / Creating ZSUB"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "1002"
    And  I click on "Ordering" in NWBC page
    And  I click on "Create Store Transfer" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I enter "<Supplying Site>" from preferred data source into the field with label "Supplying Site"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    And  I save the "Store Transfer PO" PO
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return

    Examples:
      | TCode | Article | Quantity | Supplying Site |
      | ME23N | 10039   | 3        | 1112           |


  @7216
  Scenario Outline: AGM # 11732 - MIM - Creation of New Doc Types ZSUB and ZDUB For Store and DTD Transfers (ALM#7216)
  """ Scenario 2 / Creating ZDUB"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "Ordering" in NWBC page
    And  I click on "Create DTD Transfer" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I enter "Vehicle" from preferred data source into the field with label "Vehicle Info"
    And  I enter "<Supplying Site>" from preferred data source into the field with label "Supplying Site"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    And  I save the "Store Transfer PO" PO
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return

    Examples:
      | TCode | Supplying Site | Article | Quantity |
      | ME23N | 1001           | 10039   | 10       |

  @corpMerchPoP2P
  Scenario Outline: Create Corp Merch PO in SAP_ME21N, posting in SAP_MIGO, validating in SAP_ME23N and generating and invoice reciept in SAP_MIRO
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I click on "Org. Data"
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I verify if item Overview is open
    And  I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Asset Category" to "<Asset Category>" from preferred data source
    And  I hit return
    And  I collapse header tab
    And  I click on partial text "Account Assignment"
    And  I enter the Asset ID based on the environment
    And  I click on partial text "Conditions"
    And  I save the "Net Cost" value from the page
    When I click on partial text "Confirmations"
    And  I enter " " into the field with label "Conf. Control"
    And  I hit return
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on element with title attribute value "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I verify Item detail tab is open
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on partial text "Purchase Order History"
    Then I verify the article document matches the previously saved number
    When I enter t-code "<TCode3>" in the command field
    And  I enter "present" date with "0" months into "Invoice date" element
    And  I enter "Automation" with random number into "Reference" in Incoming Invoice page
    And  I enter saved "Net Cost" element value into following element "Amount" in the page
    And  I enter previously saved PO into field with title "Purchasing Document"
    And  I hit return
    And  I click on the "Booking OK" element in po page to navigate
    And  I scroll and enter the purchase order value for "Tax Code in PO Reference" to "I0 (A/P Tax Exempt)" from preferred data source
    And  I click on element with title attribute value "Post"
    Then I verify the success status in SAP
    When I save the article document number to scenario data
    And  I enter t-code "<TCode4>" in the command field
    And  I click on "Payment transactions"
    And  I hit return
    And  I save the "Payment methods" value from the page
    And  I click on element with title attribute value "Display"
    And  I click on element with title attribute value "Save"
    And  I enter t-code "<TCode5>" in the command field
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter the unique 5 digit identification code in f110
    And  I save the "Identification" value from the page
    And  I click on "Parameter"
    And  I enter the purchase order value for "Company Codes In Payment Control" to "<Company Code>" from preferred data source
    And  I enter saved "Payment methods" element value into following table element "Pmt meths" in the page
    And  I hit tab
    And  I enter "future" date with "5" months into "Next p/date" element
    And  I click on "Free selection"
    And  I click on "Field Name"
    And  I click on the "drop down" element in po page to navigate
    And  I switch to the first iFrame in SAP
    And  I click on partial text "Document"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter the Article document number into "Values"
    And  I click on "Additional Log"
    And  I click on "Due date check"
    And  I click on "Payment method selection in all cases"
    And  I click on "Line items of the payment documents"
    And  I click on "Printout/data medium"
    And  I scroll and enter the purchase order value for "Payment type in F110" to "<Variant>" from preferred data source
    And  I click on "Parameter"
    And  I enter "<Vendor>" from preferred data source into the field with label "Vendor"
    And  I click on "Status"
    And  I switch to the first iFrame in SAP
    And  I click on "Yes"
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    And  I check for element with text "Parameters have been entered"
    When I click on element with title attribute value "Schedule Proposal"
    And  I switch to the first iFrame in SAP
    And  I click on "Start immediately"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment proposal has been created"
    When I click on element with title attribute value "Schedule payment run"
    And  I switch to the first iFrame in SAP
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment run has been carried out"
    And  I click on element with title attribute value "Schedule Print"
    And  I switch to the first iFrame in SAP
    And  I enter the unique printout job name in "Job name" in f110
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I click on element with title attribute value "Display Proposal"
    Then I verify Payment Success
    When I save the time "before" generating the file
    And  I enter t-code "<TCode6>" in the command field
    And  I click on element with title attribute value "Get Variant"
    And  I switch to the first iFrame in SAP
    And  I click on partial data "ACH_MERGE_1000"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter saved "Identification" element value into following element "Identification" in the page
    And  I execute
    And  I hit return
    And  I switch to the first iFrame in SAP
    Then I check for element with text "payments were selected"
    And  I check for element with text "Payment medium run"
    And  I check for element with text "Payment medium program is started in separate job"
    When I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "15" seconds
    And  I save the time "after" generating the file
    And  I enter t-code "<TCode7>" in the command field
    And  I enter the Article document number into "Document Number"
    And  I hit return
    When I enter t-code "<TCode8>" in the command field
    And  I double click on "Second Dir_Comm in Al11" element
    And  I double click on "int" element
    And  I double click on "o" element
    And  I double click on "banking"
    And  I select both Lastchange columns using control key
    And  I click on element with title attribute value "Set Filter"
    And  I switch to the first iFrame in SAP
    And  I enter "present" date with "0" months into "Last Changed On" element
    And  I hit tab
    And  I enter saved timestamps "before" file creation in "Time before file creation in Al11" field
    And  I enter saved timestamps "after" file creation in "Time after file creation in Al11" field
    And  I click on "CONTINUE" in order to cash page
    And  I hit return
    And  I switch back to the main window
    Then I check for element with text "Wells_Fargo_achpayments_1000_"

    Examples:
      | TCode | Vendor | Purch. Group | Article | PO Quantity | Site | TCode1 | TCode2  | Asset Category | TCode3 | PO Type       | TCode4 | TCode5 | Company Code | TCode7 | Variant | TCode6  | TCode8 |
      | ME21N | 14816  | 04           | 91506   | 2           | 1434 | /nMIGO | /nME23N | A              | /nMIRO | Corp Merch PO | /nXK03 | /nF110 | 1000         | /nfb03 | ACH1000 | /nfbpm1 | /nAL11 |

  @storeNonMerchPoP2P
  Scenario Outline: Create Store Non Merch PO in SAP_ME21N, posting in SAP_MIGO, validating in SAP_ME23N and generating and invoice reciept in SAP_MIRO
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I click on "Org. Data"
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I verify if item Overview is open
    And  I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Asset Category" to "<Asset Category>" from preferred data source
    And  I hit return
    And  I collapse header tab
    And  I click on partial text "Account Assignment"
    And  I enter the Asset ID based on the environment
    And  I click on partial text "Conditions"
    And  I save the "Net Cost" value from the page
    And  I click on partial text "Confirmations"
    And  I enter " " into the field with label "Conf. Control"
    And  I hit return
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    And  I click on "Save"
    And  I switch back to the main window in SAP
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on element with title attribute value "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    Then I verify Item detail tab is open
    When  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on partial text "Purchase Order History"
    Then I verify the article document matches the previously saved number
    When I enter t-code "<TCode3>" in the command field
    And  I enter "present" date with "0" months into "Invoice date" element
    And  I enter "Automation" with random number into "Reference" in Incoming Invoice page
    And  I enter saved "Net Cost" element value into following element "Amount" in the page
    And  I enter previously saved PO into field with title "Purchasing Document"
    And  I hit return
    And  I click on the "Booking OK" element in po page to navigate
    And  I scroll and enter the purchase order value for "Tax Code in PO Reference" to "I0 (A/P Tax Exempt)" from preferred data source
    And  I click on element with title attribute value "Post"
    Then I verify the success status in SAP
    When I save the article document number to scenario data
    And  I enter t-code "<TCode4>" in the command field
    And  I click on "Payment transactions"
    And  I hit return
    And  I save the "Payment methods" value from the page
    And  I click on element with title attribute value "Display"
    And  I click on element with title attribute value "Save"
    And  I enter t-code "<TCode5>" in the command field
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter the unique 5 digit identification code in f110
    And  I save the "Identification" value from the page
    And  I click on "Parameter"
    And  I enter the purchase order value for "Company Codes In Payment Control" to "<Company Code>" from preferred data source
    And  I enter saved "Payment methods" element value into following table element "Pmt meths" in the page
    And  I hit tab
    And  I enter "future" date with "5" months into "Next p/date" element
    And  I click on "Free selection"
    And  I click on "Field Name"
    And  I click on the "drop down" element in po page to navigate
    And  I switch to the first iFrame in SAP
    And  I click on partial text "Document"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter the Article document number into "Values"
    And  I click on "Additional Log"
    And  I click on "Due date check"
    And  I click on "Payment method selection in all cases"
    And  I click on "Line items of the payment documents"
    And  I click on "Printout/data medium"
    And  I enter the purchase order value for "Payment type in F110" to "<Variant>" from preferred data source
    And  I click on "Parameter"
    And  I enter "<Vendor>" from preferred data source into the field with label "Vendor"
    And  I click on "Status"
    And  I switch to the first iFrame in SAP
    And  I click on "Yes"
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    And  I check for element with text "Parameters have been entered"
    When I click on element with title attribute value "Schedule Proposal"
    And  I switch to the first iFrame in SAP
    And  I click on "Start immediately"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment proposal has been created"
    When I click on element with title attribute value "Schedule payment run"
    And  I switch to the first iFrame in SAP
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment run has been carried out"
    When I click on element with title attribute value "Schedule Print"
    And  I switch to the first iFrame in SAP
    And  I enter the unique printout job name in "Job name" in f110
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I click on element with title attribute value "Display Proposal"
    Then I verify Payment Success
    When I save the time "before" generating the file
    And  I enter t-code "<TCode6>" in the command field
    And  I click on element with title attribute value "Get Variant"
    And  I switch to the first iFrame in SAP
    And  I click on partial data "ACH_MERGE_1000"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter saved "Identification" element value into following element "Identification" in the page
    And  I execute
    And  I hit return
    And  I switch to the first iFrame in SAP
    Then I check for element with text "payments were selected"
    And  I check for element with text "Payment medium run"
    And  I check for element with text "Payment medium program is started in separate job"
    When I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "15" seconds
    And  I save the time "after" generating the file
    And  I enter t-code "<TCode7>" in the command field
    And  I enter the Article document number into "Document Number"
    And  I hit return
    And  I enter t-code "<TCode8>" in the command field
    And  I double click on "Second Dir_Comm in Al11" element
    And  I double click on "int" element
    And  I double click on "o" element
    And  I double click on "banking"
    And  I select both Lastchange columns using control key
    And  I click on element with title attribute value "Set Filter"
    And  I switch to the first iFrame in SAP
    And  I enter "present" date with "0" months into "Last Changed On" element
    And  I hit tab
    And  I enter saved timestamps "before" file creation in "Time before file creation in Al11" field
    And  I enter saved timestamps "after" file creation in "Time after file creation in Al11" field
    And  I click on "CONTINUE" in order to cash page
    And  I hit return
    And  I switch back to the main window
    Then I check for element with text "Wells_Fargo_achpayments_1000_"

    Examples:
      | TCode | Vendor | Purch. Group | Article | PO Quantity | Site | TCode1 | TCode2  | Asset Category | TCode3 | PO Type            | TCode4 | TCode5 | Company Code | TCode7 | Variant | TCode6  | TCode8 |
      | ME21N | 14816  | 04           | 91506   | 2           | 1434 | /nMIGO | /nME23N | A              | /nMIRO | Store Non Merch PO | /nXK03 | /nF110 | 1000         | /nfb03 | ACH1000 | /nfbpm1 | /nAL11 |

  @storeMerchPoP2P
  Scenario Outline: Create Store Merch PO in SAP_ME21N, posting in SAP_MIGO, validating in SAP_ME23N and generating and invoice reciept in SAP_MIRO
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I click on "Org. Data"
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Asset Category" to "<Asset Category>" from preferred data source
    And  I hit return
    And  I collapse header tab
    And  I click on partial text "Account Assignment"
    And  I enter the Asset ID based on the environment
    And  I click on partial text "Conditions"
    And  I save the "Net Cost" value from the page
    And  I click on partial text "Confirmations"
    And  I enter " " into the field with label "Conf. Control"
    And  I hit return
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on element with title attribute value "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    Then I verify Item detail tab is open
    When I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on partial text "Purchase Order History"
    Then I verify the article document matches the previously saved number
    When I enter t-code "<TCode3>" in the command field
    And  I enter "present" date with "0" months into "Invoice date" element
    And  I enter "Automation" with random number into "Reference" in Incoming Invoice page
    And  I enter saved "Net Cost" element value into following element "Amount" in the page
    And  I enter previously saved PO into field with title "Purchasing Document"
    And  I hit return
    And  I click on the "Booking OK" element in po page to navigate
    And  I scroll and enter the purchase order value for "Tax Code in PO Reference" to "I0 (A/P Tax Exempt)" from preferred data source
    And  I click on element with title attribute value "Post"
    Then I verify the success status in SAP
    When I save the article document number to scenario data
    And  I enter t-code "<TCode4>" in the command field
    And  I click on "Payment transactions"
    And  I hit return
    And  I save the "Payment methods" value from the page
    And  I click on element with title attribute value "Display"
    And  I click on element with title attribute value "Save"
    And  I enter t-code "<TCode5>" in the command field
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter the unique 5 digit identification code in f110
    And  I save the "Identification" value from the page
    And  I click on "Parameter"
    And  I enter the purchase order value for "Company Codes In Payment Control" to "<Company Code>" from preferred data source
    And  I enter saved "Payment methods" element value into following table element "Pmt meths" in the page
    And  I hit tab
    And  I enter "future" date with "5" months into "Next p/date" element
    And  I click on "Free selection"
    And  I click on "Field Name"
    And  I click on the "drop down" element in po page to navigate
    And  I switch to the first iFrame in SAP
    And  I click on partial text "Document"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter the Article document number into "Values"
    And  I click on "Additional Log"
    And  I click on "Due date check"
    And  I click on "Payment method selection in all cases"
    And  I click on "Line items of the payment documents"
    And  I click on "Printout/data medium"
    And  I enter the purchase order value for "Payment type in F110" to "<Variant>" from preferred data source
    And  I click on "Parameter"
    And  I enter "<Vendor>" from preferred data source into the field with label "Vendor"
    And  I click on "Status"
    And  I switch to the first iFrame in SAP
    And  I click on "Yes"
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    And  I check for element with text "Parameters have been entered"
    When I click on element with title attribute value "Schedule Proposal"
    And  I switch to the first iFrame in SAP
    And  I click on "Start immediately"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment proposal has been created"
    When I click on element with title attribute value "Schedule payment run"
    And  I switch to the first iFrame in SAP
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment run has been carried out"
    When I click on element with title attribute value "Schedule Print"
    And  I switch to the first iFrame in SAP
    And  I enter the unique printout job name in "Job name" in f110
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I click on element with title attribute value "Display Proposal"
    Then I verify Payment Success
    When I save the time "before" generating the file
    And  I enter t-code "<TCode6>" in the command field
    And  I click on element with title attribute value "Get Variant"
    And  I switch to the first iFrame in SAP
    And  I click on partial data "ACH_MERGE_1000"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter saved "Identification" element value into following element "Identification" in the page
    And  I execute
    And  I hit return
    And  I switch to the first iFrame in SAP
    Then I check for element with text "payments were selected"
    And  I check for element with text "Payment medium run"
    And  I check for element with text "Payment medium program is started in separate job"
    When I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "15" seconds
    And  I save the time "after" generating the file
    And  I enter t-code "<TCode7>" in the command field
    And  I enter the Article document number into "Document Number"
    And  I hit return
    And  I enter t-code "<TCode8>" in the command field
    And  I double click on "Second Dir_Comm in Al11" element
    And  I double click on "int" element
    And  I double click on "o" element
    And  I double click on "banking"
    And  I select both Lastchange columns using control key
    And  I click on element with title attribute value "Set Filter"
    And  I switch to the first iFrame in SAP
    And  I enter "present" date with "0" months into "Last Changed On" element
    And  I hit tab
    And  I enter saved timestamps "before" file creation in "Time before file creation in Al11" field
    And  I enter saved timestamps "after" file creation in "Time after file creation in Al11" field
    And  I click on "CONTINUE" in order to cash page
    And  I hit return
    And  I switch back to the main window
    Then I check for element with text "Wells_Fargo_achpayments_1000_"

    Examples:
      | TCode | Vendor | Purch. Group | Article | PO Quantity | Site | TCode1 | TCode2  | TCode3 | PO Type        | TCode4 | TCode5 | Company Code | TCode7 | Variant | Asset Category | TCode6  | TCode8|
      | ME21N | 10273  | 01           | 13189   | 15          | 1001 | /nMIGO | /nME23N | /nMIRO | Store Merch PO | /nXK03 | /nF110 | 1000         | /nfb03 | ACH1000 | A              | /nfbpm1 | /nAL11|

  @3238
  Scenario Outline: FC_07 - Create Payment Proposal - F110 - AR Customer Refund (ALM#3238)
    """To be fixed later: DEFECT - Not able to accept price"""
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I click on "Org. Data"
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I enter the purchase order value for "Net Price" to "<NetPrice>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Asset Category" to "<Asset Category>" from preferred data source
    And  I hit return
    And  I collapse header tab
    And  I click on partial text "Account Assignment"
    And  I enter the Asset ID based on the environment
    And  I click on partial text "Conditions"
    And  I save the "Net Cost" value from the page
    And  I click on partial text "Confirmations"
    And  I enter " " into the field with label "Conf. Control"
    And  I hit return
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    And  I click on "Save"
    And  I switch back to the main window in SAP
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on element with title attribute value "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I verify Item detail tab is open
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on partial text "Purchase Order History"
    Then I verify the article document matches the previously saved number
    When I enter t-code "<TCode3>" in the command field
    And  I enter "present" date with "0" months into "Invoice date" element
    And  I enter "Automation" with random number into "Reference" in Incoming Invoice page
    And  I enter saved "Net Cost" element value into following element "Amount" in the page
    And  I enter previously saved PO into field with title "Purchasing Document"
    And  I hit return
    And  I click on the "Booking OK" element in po page to navigate
    And  I scroll and enter the purchase order value for "Tax Code in PO Reference" to "I0 (A/P Tax Exempt)" from preferred data source
    And  I click on element with title attribute value "Post"
    Then I verify the success status in SAP
    When I save the article document number to scenario data
    And  I enter t-code "<TCode4>" in the command field
    And  I click on "Payment transactions"
    And  I hit return
    And  I save the "Payment methods" value from the page
    And  I click on element with title attribute value "Display"
    And  I click on element with title attribute value "Save"
    And  I enter t-code "<TCode5>" in the command field
    And  I enter "present" date with "0" months into "Payment Run Date" element
    And  I enter the unique 5 digit identification code in f110
    And  I save the "Identification" value from the page
    And  I click on "Parameter"
    And  I enter the purchase order value for "Company Codes In Payment Control" to "<Company Code>" from preferred data source
    And  I enter saved "Payment methods" element value into following table element "Pmt meths" in the page
    And  I hit tab
    And  I enter "future" date with "5" months into "Next p/date" element
    And  I click on "Free selection"
    And  I click on "Field Name"
    And  I click on the "drop down" element in po page to navigate
    And  I switch to the first iFrame in SAP
    And  I click on partial text "Document"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I enter the Article document number into "Values"
    And  I click on "Additional Log"
    And  I click on "Due date check"
    And  I click on "Payment method selection in all cases"
    And  I click on "Line items of the payment documents"
    And  I click on "Printout/data medium"
    And  I enter the purchase order value for "Payment type in F110" to "<Variant>" from preferred data source
    And  I click on "Parameter"
    And  I enter "<Vendor>" from preferred data source into the field with label "Vendor"
    And  I click on "Status"
    And  I switch to the first iFrame in SAP
    And  I click on "Yes"
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    And  I check for element with text "Parameters have been entered"
    When I click on element with title attribute value "Schedule Proposal"
    And  I switch to the first iFrame in SAP
    And  I click on "Start immediately"
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment proposal has been created"
    When I click on element with title attribute value "Schedule payment run"
    And  I switch to the first iFrame in SAP
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Status"
    Then I check for element with text "Payment run has been carried out"
    When I click on element with title attribute value "Schedule Print"
    And  I switch to the first iFrame in SAP
    And  I enter the unique printout job name in "Job name" in f110
    And  I click on "CONTINUE" in order to cash page
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I enter t-code "<TCode6>" in the command field
    And  I enter the Article document number into "Document Number"
    And  I hit return

    Examples:
      | TCode | Vendor | Purch. Group | Article | PO Quantity | Site | Asset Category | TCode1 | TCode2  | TCode3 | PO Type           | TCode4 | TCode5 | Company Code | TCode6 | Payment Type | NetPrice |
      | ME21N | 10051  | 04           | 10001   | 2           | 1434 | A              | /nMIGO | /nME23N | /nMIRO | Corp Non Merch PO | /nXK03 | /nF110 | 1000         | /SP01  | ZCHECK1000   | 50       |
    
  @9914
  Scenario Outline: ECC_Manually update Vendor PO Confirmations in SAP - ME22N (ALM#9914)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    Then I verify Head data tab is open
    When I click on "Org. Data"
    And  I verify if item Overview is open
    And  I switch back to the main window in SAP
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I verify if item Overview is open
    And  I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I verify Item detail tab is open
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on element in stage with value "Display"
    And  I click on partial text "Confirmations"
    And  I enter " " into the field with label "Conf. Control"
    And  I hit return
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    And  I click on "Save"
    Then I verify the success status in SAP

    Examples:
      | TCode | Vendor | Purch. Group | Article | PO Quantity | Site | TCode1  | PO Type           |
      | ME21N | 14816  | 04           | 91506   | 2           | 1434 | /nME22N | Corp Non Merch PO |

  @10162
  Scenario Outline: AGM # 26953 - One Source Call Redesign (ALM#10162)
    """Scenario 1"""
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    And  I verify Head data tab is open
    And  I click on "Org. Data"
    Then I verify if item Overview is open
    When I switch back to the main window in SAP
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I click on "Invoice"
    Then I verify "Tax Code" field is not empty

    Examples:
      | TCode | Vendor | Purch. Group | Article | PO Quantity | Site | PO Type           |
      | ME21N | 14816  | 04           | 91506   | 2           | 1434 | Corp Non Merch PO |
      | ME21N | 14816  | 04           | 91506   | 2           | 1434 | Corp Merch PO     |
      | ME21N | 10273  | 01           | 13189   | 2           | 1001 | Store Merch PO    |
      | ME21N | 26418  | 01           | 29487   | 2           | 9301 | Factory Direct PO |

  @10162
  Scenario Outline: AGM # 26953 - One Source Call Redesign (ALM#10162)
  """ Scenario 2 / No tax for ZSUB and ZDUB"""
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    Then I verify Head data tab is open
    When I click on "Org. Data"
    Then I verify if item Overview is open
    When I switch back to the main window in SAP
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I enter the purchase order value for "Supplying Site/Plant" to "1002" from preferred data source
    And  I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I enter the purchase order value for "Site In Store Transfer" to "<Site>" from preferred data source
    And  I click on "Invoice"
    And  I click on "Taxes"
    Then I verify "Tax" element has same value as "0.00 " element

    Examples:
      | TCode | Purch. Group | Article | PO Quantity | Site | PO Type        |
      | ME21N | 01           | 10039   | 2           | 1001 | Store Transfer |
      | ME21N | 01           | 10039   | 2           | 1112 | DTD Transfer   |

  @13630
  Scenario Outline: AGM # 39418 - INV - ECC - Program To Assist With PO Article Changes - ZA2ACHANGE (ALM#13630)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    Then I verify Head data tab is open
    When I click on "Org. Data"
    Then I verify if item Overview is open
    When I switch back to the main window in SAP
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on element with title attribute value "Post"
    And  I save the article document number to scenario data
    When I enter t-code "<TCode2>" in the command field
    And  I enter previously saved PO into field with label name "PO Number"
    And  I enter "<PO Line Item>" into the field with label "PO Line Item"
    And  I enter "<Article>" into the field with label "Old Article"
    And  I enter "<New Article>" into the field with label "New Article"
    And  I enter "<PO Quantity>" into the field with label "Quantity"
    And  I execute
    And  I click on "Process Article to Article Change"
    Then I verify "Status 100" is displayed in iDoc page

    Examples:
      | TCode | TCode1 | TCode2       | PO Line Item | New Article | PO Quantity | Purch. Group | Article | Site | Vendor | PO Type        |
      | ME21N | /nMIGO | /nZA2ACHANGE | 0010         | 43135       | 2           | 01           | 43130   | 1022 | 10226  | Store Merch PO |

  @9233
  Scenario Outline: AGM # 17304 - ORD_ REC - ECC - ZAUTOREC Program New Enhancements For External Orders - 3(ALM#9233)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    Then I verify Head data tab is open
    When I click on "Org. Data"
    Then I verify if item Overview is open
    When I switch back to the main window in SAP
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I click on "External Purchase Order"
    And  I click on "Set Delivery Complete"
    And  I enter "NB" into the field with label "Document Type"
    And  I enter previously saved PO into field with label name "PO Number"
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    And  I check for element with text "X"
    When I click on element with title attribute value "Back"
    And  I click on "Test"
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    And  I check for element with text "X"
    And  I click on element with title attribute value "Back"
    When I enter t-code "<TCode2>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I verify Head data tab is open
    Then I verify if item Overview is open
    When I switch back to the main window in SAP
    And  I click on the "Status Tab" element in po page to navigate
    Then I check for element with title "Still to be Deliv."
    And  I verify Item detail tab is open
    When I click on "Delivery"
    Then I verify delivery completion

    Examples:
      | TCode   | Vendor | Purch. Group | Article | PO Quantity | Site | PO Type       | TCode1     | TCode2  |
      | /nME21N | 26418  | 01           | 25552   | 2           | 1434 | Corp Merch PO | /nZAUTOREC | /nme23n |
  
  @17391
  @9814
  Scenario Outline: SAP_ECC_EKBE Table (ALM#17391,#9814)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<PO Type>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I click on "Org. Data"
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I hit return
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I enter the purchase order value for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value for "Asset Category" to "<Asset Category>" from preferred data source
    And  I collapse header tab
    And  I click on partial text "Account Assignment"
    And  I enter the Asset ID based on the environment
    And  I click on partial text "Conditions"
    And  I save the "Net Cost" value from the page
    And  I click on partial text "Confirmations"
    And  I enter " " into the field with label "Conf. Control"
    And  I hit return
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on element with title attribute value "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data
    And  I enter t-code "<TCode3>" in the command field
    And  I enter "present" date with "0" months into "Invoice date" element
    And  I enter "Automation" with random number into "Reference" in Incoming Invoice page
    And  I enter saved "Net Cost" element value into following element "Amount" in the page
    And  I enter previously saved PO into field with title "Purchasing Document"
    And  I hit return
    And  I click on the "Booking OK" element in po page to navigate
    And  I scroll and enter the purchase order value for "Tax Code in PO Reference" to "I0 (A/P Tax Exempt)" from preferred data source
    And  I click on element with title attribute value "Save"
    Then I verify the success status in SAP
    When I enter t-code "<TCode2>" in the command field
    And  I enter "EKBE" into the field with label "Table Name"
    And  I hit return
    And  I enter previously saved PO into field with label name "Purchasing Doc."
    And  I execute
    And  I save the "Amount in EKBE" value from the page
    And  I save the "Posting Date in EKBE" value from the page
    And  I enter t-code "<TCode4>" in the command field
    Then I verify Item detail tab is open
    When I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on partial text "Purchase Order History"
    Then I verify the article document matches the previously saved number

    Examples:
      | TCode2 | TCode | TCode1 | TCode3 | TCode4  | Vendor | Purch. Group | Article | PO Quantity | Site | PO Type           | Asset Category |
      | /nSE16 | ME21N | /nmigo | /nMIRO | /nme23n | 14816  | 04           | 91506   | 2           | 1434 | Corp Non Merch PO | A              |

  @3443
  Scenario Outline: Validation rule for Allocation Accounts - No Direct Posting (ALM#3443)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Date>" date with "0" months into "Document Date" element
    And  I enter the purchase order value for "G/L Account number" to "<G/L Account value>" from preferred data source
    And  I enter the purchase order value for "Debit or Credit" to "<Card Type>" from preferred data source
    And  I enter the purchase order value for "Amount" to "<Amount Value>" from preferred data source
    And  I hit return
    And  I switch to the first iFrame in SAP
    Then I check for element with text "GL Account 670001 cannot be posted to"
    When I hit return
    And  I switch back to the main window
    Then I verify the success status in SAP

    Examples:
      | TCode | Date    | G/L Account value | Card Type | Amount Value |
      | FB50  | present | 670001            | Debit     | 100.00       |

  @4497
  Scenario Outline: FUT - Defect 4288 - Enable Withholding Tax fields to be Editable (ALM#4497)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Vendor>" into the field with label "Vendor account"
    And  I click on "All items"
    And  I execute
    And  I double click on "KR Document number" element
    And  I click on element with title attribute value "Change Display"
    And  I wait for "5" seconds
    And  I enter "<new W.tax base>" into the field with label "W/Tax Base"
    And  I enter "<new W.tax exempt>" into the field with label "W/Tax-Exempt"
    And  I enter "<new W.Tax Code>" into the field with label "W.Tax Code"
    And  I click on element with title attribute value "Save"
    Then I check for element with text "Changes have been saved"
    When I double click on "KR Document number" element
    And  I click on element with title attribute value "Change Display"
    And  I wait for "5" seconds
    And  I enter "<old W.tax base>" into the field with label "W/Tax Base"
    And  I enter "<old W.tax exempt>" into the field with label "W/Tax-Exempt"
    And  I enter "<old W.Tax Code>" into the field with label "W.Tax Code"
    And  I click on element with title attribute value "Save"
    Then I check for element with text "Changes have been saved"

    Examples:
      | TCode | Vendor | new W.tax base | new W.tax exempt | new W.Tax Code | old W.tax base | old W.tax exempt | old W.Tax Code |
      | FBL1N | 10050  | 90.90          | 8.35             | 08             | 91.90          | 7.35             | 07             |

  @4517
  Scenario Outline: Defect 4330 - Create Lease Asset Class Validation (ALM#4517)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Asset Class value>" into the field with label "Asset Class"
    And  I enter "<Company Code value>" into the field with label "Company Code"
    And  I hit return
    And  I enter "<Description value>" into the field with label "Description"
    And  I click on "Time-dependent"
    And  I enter "<Cost Center value>" into the field with label "Cost Center"
    And  I click on "Deprec. Areas"
    And  I execute
    And  I hit return
    And  I switch to the first iFrame in SAP
    And  I check for element with text "Asset Class = 1500 or 1510 - Review Useful Life"

    Examples:
      | TCode | Asset Class value | Company Code value | Description value | Cost Center value |
      | AS01  | 1500              | 1000               | Lease Test        | 1975              |