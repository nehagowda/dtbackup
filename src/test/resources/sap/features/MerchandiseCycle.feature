@sap
@merchandiseCycle
Feature: Merchandise cycle

  @4078
  @merchandiseScenario1
  Scenario Outline: ML_01_Manually enter final retail price using standard SAP Dist Ch 20_VKP5 (ALM#4078)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Article Number>" into the field with label "Article" and hit tab
    And  I enter "<Sales Organization>" into the field with label "Sales organization" and hit tab
    And  I enter "<Distribution Channel>" into the field with label "Distribution channel" and hit tab
    And  I enter "<Purchase Price Determ>" into the field with label "Purchase Price Determ. Seq." and hit tab
    And  I enter "<Sales Price Determ>" into the field with label "Sales price determination seq." and hit tab
    And  I enter "<List Variant>" into the field with label "List Variant" and hit tab
    And  I execute
    And  I click on element with title attribute value "Select All"
    And  I click on "Final Pr Value" in merchandise cycle page
    And  I click on "Change List Field"
    And  I switch to the first iFrame in SAP
    And  I click on "Percentage Change"
    And  I enter "<Amount>" into the field with label "Amount" and hit tab
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Select All"
    And  I click on "Final Pr Value" in merchandise cycle page
    And  I click on "Change List Field"
    And  I switch to the first iFrame in SAP
    And  I click on "Change by Absolute Val."
    And  I enter "<Amount>" into the field with label "Amount" and hit tab
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    Then I verify the success status in SAP

    Examples:
      | TCode | Article Number | Sales Organization | Distribution Channel | Purchase Price Determ | Sales Price Determ | List Variant | Amount |
      | VKP5  | 10334          | 1000               | 20                   | 01                    | 02                 | 01           | 5      |
      | VKP5  | 10337          | 1000               | 20                   | 01                    | 02                 | 01           | 5      |

  @4064
  @pricing
  Scenario Outline: ML_01_Manual entry of OV price using RPM Dist Ch 20- ZRPM (ALM#4064)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Article Number>" into the field with label "Article" and hit tab
    And  I enter "<Sales Organization>" into the field with label "Sales Organization" and hit tab
    And  I enter "<Distribution Channel>" into the field with label "Distribution Channel" and hit tab
    And  I enter "<Price List>" into the field with label "Price List" and hit tab
    And  I execute
    And  I wait for "5" seconds
    And  I hit return
    And  I enter new price information for "Override Price"
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I enter "<Article Number>" into the field with label "Article" and hit tab
    And  I enter "1" to be added to todays date in "Validity Date from" in calculate competitor pricing page
    Then I verify "Test Run" checkbox is "On"
    When I deselect "Display Errors Only" checkbox
    And  I execute
    And  I wait for "2" seconds
    Then I verify new price is displayed in "New Cond." field
    When I enter t-code "<TCode2>" in the command field
    And  I enter "<Article Number>" into the field with label "Article" and hit tab
    And  I enter "1" to be added to todays date in "Validity Date from" in calculate competitor pricing page
    And  I deselect "Test Run" checkbox
    And  I click on "Menu"
    And  I click on "Program"
    And  I click on "Execute in Background"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "<Output Device>" into the field with label "Output Device" and hit tab
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Immediate"
    And  I click on element with title attribute value "Save"
    And  I switch back to the main window in SAP
    Then I verify "Background job message" matches "<Message>" displayed in merchandise cycle page
    When I enter t-code "<TCode3>" in the command field
    And  I enter "<Job Name>" into the field with label "Job Name" and hit tab
    And  I execute
    Then I verify "Spool list" matches "<SpoolMessage>" displayed in merchandise cycle page
    When I enter t-code "<TCode4>" in the command field
    And  I enter "<Article Number>" into the field with label "Article" and hit tab
    And  I execute
    Then I verify new price is displayed in "Override Price" field
    When I save the order number to the "top" of the "Pricing" tab of the "SAP_PURCHASE_ORDER_EXCEL" excel file for updated price
    And  I enter t-code "<TCode5>" in the command field
    And  I enter "<Sales Organization>" into the field with label "Sales Organization" and hit tab
    And  I enter "<Distribution Channel>" into the field with label "Distribution Channel" and hit tab
    And  I enter "<Assortment List Type>" into the field with label "Assortment List Type" and hit tab
    And  I wait for "2" seconds
    And  I deselect "Do not use cycle" checkbox
    And  I click on "Menu"
    And  I click on "Program"
    And  I click on "Execute in Background"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "<Output Device>" into the field with label "Output Device" and hit tab
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Immediate"
    And  I click on element with title attribute value "Save"
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode6>" in the command field
    And  I enter "<Job Name2>" into the field with label "Job Name" and hit tab
    And  I execute
    And  I wait for "10" seconds
    Then I verify "Job Status" matches "Finished" displayed in merchandise cycle page
#    When I make a Post call to 'oauth/token' endpoint and capture the token
#    And  I do a Get call to "<Price Endpoint>" endpoint for article "<Article Number>" and customer price list "<Customer Price List>" and catalog version "<Catalog Version>" and the previously saved token
#    Then I compare the updated price from SAP to price in Hybris

    Examples:
      | TCode | Sales Organization | Distribution Channel | Price List | Article Number | Page Title                                       | Amount | TCode2        | Output Device | Message                                                               | TCode3 | Job Name                     | SpoolMessage | TCode4 | Article Description                                               | TCode5 | Assortment List Type | TCode6 | Job Name2 | Price Endpoint | Customer Price List | Catalog Version |
      | ZRPM  | 1000               | 10                   | 01         | 34301          | Manual Price w/override for C000000001(Override) | 120.00 | /nZCPCL_DELTA | LOCL          | Background job was scheduled for program ZMLE_COMP_PRICING_CALC_DELTA | /nsm37 | ZMLE_COMP_PRICING_CALC_DELTA | Finished     | /nZRPM | PBX A/T Hardcore [34301] - Discount Tire Product Catalog : Online |  /nWDBU | 9                   | /nSM37 | RWDBBUPD  | products       | ug-0001             | Online          |

  @inventoryUpdateScript
  Scenario Outline: Script to update the article inventory in SAP for QA and STG
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "MARD" into the field with label "Table"
    And  I hit return
    And  I extract the values from excel and input all articles in MARD table in se16n
    And  I enter site information extracted from environment variables on "Site" in merchandise cycle page
    And  I execute
    And  I click on element with title attribute value "Views "
    And  I click on "List Output"
    And  I check and store the value of inventory which should be added to a specific article
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode2>" in the command field
    And  I enter "<Movement Type>" into the field with label "Movement Type" and hit tab
    And  I enter "<Storage Location>" into the field with label "Storage Location" and hit tab
    And  I enter site information from environment variables into the field with label "Site" and hit tab
    And  I hit return
    And  I enter all articles and values in the table
    And  I hit return
    And  I click on element with title attribute value "Post"
    Then I verify the success status in SAP

    Examples:
      | TCode | TCode2 | Movement Type | Storage Location |
      | Se16n | MB1c   | 501           | 0001             |

  @7936
  @pricing
  Scenario Outline: AGM # 16114 - MAP Pricing to behave as floor price when Market goes below MAP (Min Advertise Price)
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Article Number>" into the field with label "Article" and hit tab
    And  I enter "<Sales Organization>" into the field with label "Sales Organization" and hit tab
    And  I enter "<Distribution Channel>" into the field with label "Distribution Channel" and hit tab
    And  I enter "<Price List>" into the field with label "Price List" and hit tab
    And  I enter "<Competitor>" into the field with label "Competitor" and hit tab
    And  I select "<Price Parameter>" Parameter
    And  I execute
    And  I enter new price information for "MAP Pricing"
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I enter "<Article Number>" into the field with label "Article" and hit tab
    And  I enter "1" to be added to todays date in "Validity Date from" in calculate competitor pricing page
    Then I verify "Test Run" checkbox is "On"
    When I deselect "Display Errors Only" checkbox
    And  I execute
    And  I wait for "2" seconds
    Then I verify new price is displayed in "New Cond." field
    When I enter t-code "<TCode2>" in the command field
    And  I enter "<Article Number>" into the field with label "Article" and hit tab
    And  I enter "1" to be added to todays date in "Validity Date from" in calculate competitor pricing page
    And  I execute
    And  I deselect "Test Run" checkbox
    And  I click on "Menu"
    And  I click on "Program"
    And  I click on "Execute in Background"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "<Output Device>" into the field with label "Output Device" and hit tab
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Immediate"
    And  I click on element with title attribute value "Save"
    And  I switch back to the main window in SAP
    Then I verify "Background job message" matches "<Message>" displayed in merchandise cycle page
    When I enter t-code "<TCode3>" in the command field
    And  I enter "<Job Name>" into the field with label "Job Name" and hit tab
    And  I execute
    Then I verify "Spool list" matches "<SpoolMessage>" displayed in merchandise cycle page
    When I enter t-code "<TCode4>" in the command field
    And  I enter "<Article Number>" into the field with label "Article" and hit tab
    And  I select "<Price Parameter>" Parameter
    And  I execute
    Then I verify new price is displayed in "MAP Pricing" field
    When I save the order number to the "top" of the "Pricing" tab of the "SAP_PURCHASE_ORDER_EXCEL" excel file for updated price
    And  I enter t-code "<TCode5>" in the command field
    And  I enter "<Sales Organization>" into the field with label "Sales Organization" and hit tab
    And  I enter "<Distribution Channel>" into the field with label "Distribution Channel" and hit tab
    And  I enter "<Assortment List Type>" into the field with label "Assortment List Type" and hit tab
    And  I wait for "2" seconds
    And  I deselect "Do not use cycle" checkbox
    And  I click on "Menu"
    And  I click on "Program"
    And  I click on "Execute in Background"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "<Output Device>" into the field with label "Output Device" and hit tab
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Immediate"
    And  I click on element with title attribute value "Save"
    And  I switch back to the main window in SAP
    And  I verify the success status in SAP
    And  I enter t-code "<TCode6>" in the command field
    And  I enter "<Job Name>" into the field with label "Job Name" and hit tab
    And  I execute
    And  I wait for "10" seconds
    Then I verify "Job Status" matches "<SpoolMessage>" displayed in merchandise cycle page

    Examples:
      | TCode | Sales Organization | Distribution Channel | Price List | Article Number | Page Title                               | Amount | TCode2        | Output Device | Message                                                               | TCode3 | Job Name                     | SpoolMessage | TCode4 | Article Description                                           | TCode5  | Assortment List Type | TCode6 | Job Name2 | Price Parameter         | Competitor |
      | ZRPM  | 1000               | 10                   | 01         | 12127          | Manual Price for C000000135(MAP Pricing) | 260.00 | /nZCPCL_DELTA | LOCL          | Background job was scheduled for program ZMLE_COMP_PRICING_CALC_DELTA | /nsm37 | ZMLE_COMP_PRICING_CALC_DELTA | Finished     | /nZRPM | Primacy MXM4 [12127] - Discount Tire Product Catalog : Online |  /nWDBU | 9                    | /nSM37 | RWDBBUPD  | Competitor Manual Price | C000000135 |

  @flagEntryLevelPricing
  Scenario Outline: Flag Entry Level Pricing (ALM#NONE)
  """Waiting on Bhaumic for further action on the test. Please ignore commented steps"""
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Sales Organization>" into the field with label "Sales Organization" and hit tab
    And  I enter "<Distribution Channel>" into the field with label "Distribution Channel" and hit tab
    And  I enter "<Price List>" into the field with label "Price List" and hit tab
    And  I enter "<Competitor>" into the field with label "Competitor" and hit tab
    And  I select "<Price Parameter>" Parameter
    And  I wait for "2" seconds
    And  I enter "<CrossSection>" into the field with label "Cross Section" and hit tab
    And  I enter "<AspectRatio>" into the field with label "Aspect Ratio" and hit tab
    And  I enter "<RimSize>" into the field with label "Rim Size" and hit tab
#    And  I enter "<CrossSection>" information on "Cross Section" in merchandise cycle page
#    And  I enter "<AspectRatio>" information on "Aspect Ratio" in merchandise cycle page
#    And  I enter "<RimSize>" information on "Rim Size" in merchandise cycle page
    And  I execute
#    And  I switch to the iFrame at Index "0" in SAP
#    And  I switch back to the main window in SAP
#    Then I am brought to the page with the title "<Page Title>"
    When I enter "<GroupID>" for the "first" subordinate article
    And  I select the checkbox for the "first checkbox" subordinate article
    And  I enter price relationship as "<PriceRelationship>" for the "first row" subordinate article
    And  I hit return
    And  I hit tab
    And  I wait for "2" seconds
    And  I enter "<GroupID>" for the "second" subordinate article
    And  I select the checkbox for the "second checkbox" subordinate article
    And  I enter price relationship as "<PriceRelationship>" for the "second row" subordinate article
    And  I hit tab
    And  I wait for "2" seconds
    And  I click on "Save Button" in merchandise cycle page
    And  I switch to the iFrame at Index "0" in SAP
    Then I validate "Saved Successfully" entered matches original "saved successfully." in merchandise cycle page
    When I click on "Continue" in merchandise cycle page
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode5>" in the command field
    And  I enter "<Sales Organization>" information on "Article" in merchandise cycle page
    And  I enter "<Distribution Channel>" information on "Sales Organization" in merchandise cycle page
    And  I enter "<Price List>" information on "Distribution Channel" in merchandise cycle page
    And  I enter "<Assortment List Type>" information on "Pricing Article" in merchandise cycle page
    And  I wait for "2" seconds
    And  I deselect "Do not use cycle" checkbox
    And  I click on "Menu"
    And  I click on "Program"
    And  I click on "Execute in Background"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "<Output Device>" information on "Output Device" in merchandise cycle page
    And  I click on "Green Check on Print Parameters" in merchandise cycle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Green Check on Information" in merchandise cycle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "immediate" in merchandise cycle page
    And  I click on "Check Start Time" in merchandise cycle page
    And  I click on "Save Start Time" in merchandise cycle page
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode6>" in the command field
    And  I enter "<Job Name2>" information on "Job name" in merchandise cycle page
    And  I click on "Execute" in merchandise cycle page
    And  I wait for "10" seconds
    Then I verify "Job Status" matches "<SpoolMessage>" displayed in merchandise cycle page

    Examples:
      | TCode | Sales Organization | Distribution Channel | Price List | Page Title                  | Output Device | SpoolMessage | TCode5  | Assortment List Type | TCode6 | Job Name2 | Price Parameter  | Competitor | CrossSection | AspectRatio | RimSize | PriceRelationship | GroupID   |
      | ZRPM  | 1000               | 10                   | 01         | Flag Article as Entry Level | LOCL          |  Finished    | /nWDBU | 9                     | /nSM37 | RWDBBUPD  | Flag Entry Level | C000000135 | 255          | 55          | 18      | 2                 | 255-55-18 |

  @matchignoreprocessing
  @pricing
  Scenario Outline: Match Ignore Processing (ALM NONE)
  """Waiting on Bhaumic for further action on the test. Please ignore commented steps"""
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Sales Organization>" into the field with label "Sales Organization" and hit tab
    And  I enter "<Distribution Channel>" into the field with label "Distribution Channel" and hit tab
    And  I enter "<Price List>" into the field with label "Price List" and hit tab
    And  I hit return
#    And  I click on "Multiple selection" in Competitor Pricing page
    And  I click on element with title attribute value "Multiple selection"
    And  I switch to the iFrame at Index "0" in SAP
    And  I set the input for single value at row number "1" to "<Article Number1>"
    And  I set the input for single value at row number "2" to "<Article Number2>"
    And  I set the input for single value at row number "3" to "<Article Number3>"
    And  I set the input for single value at row number "4" to "<Article Number4>"
    And  I set the input for single value at row number "5" to "<Article Number5>"
    And  I set the input for single value at row number "6" to "<Article Number6>"
#    And  I click on "Continue" in merchandise cycle page
    And  I click on element with title attribute value "Copy"
    And  I switch back to the main window in SAP
    And  I select "<Price Parameter>" Parameter
    And  I execute
    And  I wait for "2" seconds
    And  I switch back to the main window in SAP
#    And  I save matched competitor
    And  I click the box at "Match" row number "1"
    And  I click the box at "Match" row number "2"
    And  I click the box at "Match" row number "3"
    And  I click the box at "Match" row number "4"
    And  I click the box at "Match" row number "5"
    And  I click the box at "Match" row number "6"
    And  I click on element with title attribute value "Save"
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Multiple selection"
    And  I switch to the iFrame at Index "0" in SAP
#    When I click on "Continue" in merchandise cycle page
    And  I click on element with title attribute value "Copy"
    And  I switch back to the main window in SAP
    And  I select "<Price Parameter>" Parameter
    And  I execute
    And  I wait for "2" seconds
    And  I select the new competitor
    And  I click the box at "Match" row number "1"
    And  I click the box at "Match" row number "2"
    And  I click the box at "Match" row number "3"
    And  I click the box at "Match" row number "4"
    And  I click the box at "Match" row number "5"
    And  I click the box at "Match" row number "6"
    And  I click on "Save Button" in merchandise cycle page
    And  I wait for "2" seconds
    And  I enter t-code "<TCode2>" in the command field
    And  I click on "Multiple selection" in Competitor Pricing page
    And  I switch to the iFrame at Index "0" in SAP
    And  I set the input for single value at row number "1" to "<Article Number1>"
    And  I set the input for single value at row number "2" to "<Article Number2>"
    And  I set the input for single value at row number "3" to "<Article Number3>"
    And  I set the input for single value at row number "4" to "<Article Number4>"
    And  I set the input for single value at row number "5" to "<Article Number5>"
    And  I set the input for single value at row number "6" to "<Article Number6>"
    When I click on "Continue" in merchandise cycle page
    And  I click on "Copy F8" in merchandise cycle page
    And  I switch back to the main window in SAP
    And  I enter "1" to be added to todays date in "Validitiy Date from" in calculate competitor pricing page
    And  I deselect "Test Run" checkbox
    And  I click on "Menu"
    And  I click on "Program"
    And  I click on "Execute in Background"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "<Output Device>" information on "Output Device" in merchandise cycle page
    And  I click on "Green Check on Print Parameters" in merchandise cycle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Green Check on Information" in merchandise cycle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "immediate" in merchandise cycle page
    And  I click on "Check Start Time" in merchandise cycle page
    And  I click on "Save Start Time" in merchandise cycle page
    And  I switch back to the main window in SAP
    Then I verify "Background job message" matches "<Message>" displayed in merchandise cycle page
    When I enter t-code "<TCode3>" in the command field
    And  I enter "<Job Name>" information on "Job name" in merchandise cycle page
    And  I select the "Execute" button on the Idoc page
    Then I verify "Spool list" matches "<SpoolMessage>" displayed in merchandise cycle page

    Examples:
      | TCode | Sales Organization | Distribution Channel | Price List | Article Number1 | Article Number2 | Article Number3 | Article Number4 | Article Number5 | Article Number6 | Price Parameter         | TCode2        | Output Device | Message                                                               | TCode3 | Job Name                     | SpoolMessage |
      | ZRPM  | 1000               | 10                   | 01         | 10673           | 11016           | 11093           | 11094           | 12237           | 12750           | Match/Ignore Processing | /nZCPCL_DELTA | LOCL          | Background job was scheduled for program ZMLE_COMP_PRICING_CALC_DELTA | /nsm37 | ZMLE_COMP_PRICING_CALC_DELTA | Finished     |