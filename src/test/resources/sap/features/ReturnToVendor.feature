@sap
@returnToVendor
Feature: Return To Vendor

  @5397
  @returnToVendorFromSap
  Scenario Outline: AGM # 5656 New Field addition in RTV PO (ALM#5397)
  """ TODO: Add extra ME23N validation at last
      Also all the test cases require test data from Legacy """
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I click on "Org. Data"
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I click on "Return Information"
    And  I enter "<Driver name>" information on "Driver Name" in Purchase order page
    And  I enter "<RGA/RMA#>" information on "RGA/RMA#" in Purchase order page
    And  I enter "<BOL/PRO#>" information on "BOL/PRO#" in Purchase order page
    And  I enter "<Original PO#>" information on "Original PO#" in Purchase order page
    And  I enter "<Method of return>" information on "Method of Return" in Purchase order page
    And  I click on "Return Information"
    Then I verify if item Overview is open
    When I enter the PO value for "Article Number" to "<Article>", "Quantity" to "<PO Quantity>", "Site" to "<Site>" from preferred data source
    And  I click "Returns Item" at row number "1"
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I delete all cookies
    And  I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    Then I set the site to "<Site>"
    And  I click on "RETURNS"
    And  I switch to the first iFrame in SAP
    And  I click on the purchase order number for open returns in NWBC page
    And  I click on "Open Purchase Order"
    And  I switch back to the main window in SAP
    And  I switch to the pop up window
    And  I switch to RTV popup Iframe
    And  I enter "6" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "Damaged" value into column name "Reason" at line number "1" in items table in nwbc
    And  I switch back to the main window in SAP
    And  I switch to RTV popup Iframe
    And  I click on "Return Information"
    And  I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I click on "Method of Return" in po page
    And  I click on "<Delivery Type>"
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to RTV popup Iframe
    And  I click on "Save and Print Preview"
    Then I verify RTV confirmation message

    Examples:
      | TCode | POType | Purch. Group | Vendor | Driver name | RGA/RMA# | BOL/PRO#   | Original PO# | Method of return | Article | PO Quantity | Site | Delivery Type  | Folder Name      | File Name | Sheet name | row number |
      | ME21N | RTV PO | 01           | 10173  | DN01        | RGA 01   | BOL 012345 | OP01         | 01               | 40520   | 2           | 1002 | Vendor Pick-up | Return To Vendor | 5397.xlsx | 5397       | 2          |

  @5397
  @returnToVendorFromMimPart1
  Scenario Outline: AGM # 5656 New Field addition in RTV PO (ALM#5397)- part 2.1
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    Then I set the site to "Store"
    When I click on "RETURNS"
    And  I click on "Create Return to Vendor"
    And  I switch to the first iFrame in SAP
    And  I select "Vendor Field" and enter "<Vendor>" in NWBC page
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I enter "Damaged" value into column name "Reason" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    And  I verify the success message with green image
    And  I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I enter "<Driver name>" information on "Driver Name" in Purchase order page
    And  I hit tab
    And  I enter "<RGA/RMA#>" information on "RGA/RMA#" in Purchase order page
    And  I hit tab
    And  I enter "<BOL/PRO#>" information on "BOL/PRO#" in Purchase order page
    And  I hit tab
    And  I enter "<Original PO#>" information on "Original PO#" in Purchase order page
    And  I hit tab
    And  I click on "Method of Return" in po page
    And  I click on "<Delivery Type>"
    And  I enter "4" tracking numbers
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "Create Return Order" Iframe in NWBC
    And  I click on "Save and Print Preview"
    Then I verify the error message with red image
    When I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I clear one tracking number at row "4"
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "Create Return Order" Iframe in NWBC
    And  I click on "Save and Print Preview"
    Then I save the Return purchase order
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "RETURNS"
    And  I click on "Open Returns" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I click on the purchase order number for open returns in NWBC page
    And  I click on "Post Goods Return"
    And  I switch back to the main window in SAP
    And  I switch to the pop up window
    And  I switch to RTV popup Iframe
    Then I verify "<Article>" value for column name "Article" at line number "1" in items table in nwbc
    And  I verify "<Quantity>" value for column name "Quantity" at line number "1" in items table in nwbc
    And  I verify "<Quantity>" value for column name "Ordered" at line number "1" in items table in nwbc
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    Then I verify "<Driver name>", "<RGA/RMA#>", "<BOL/PRO#>", "<Original PO#>" and tracking numbers in return information tab
    When I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to RTV popup Iframe
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I enter "ZDP_RTV_TRACK_NO" into the field with label "Table"
    And  I hit return
    And  I select "Purchasing Doc." field and enter previously saved po in "from" field in se16n
    And  I execute
    Then I verify the tracking numbers for RTV in SE16N

    Examples:
      | Vendor | Article | Quantity | Unit Price | TCode    | Delivery Type | Driver name | RGA/RMA# | BOL/PRO#   | Original PO# |
      | 10173  | 90058   | 3        | 20         | /n SE16n | FedEx/UPS     | DN01        | RGA 01   | BOL 012345 | OP01         |

  @5397
  @returnToVendorFromMimPart2
  Scenario Outline: AGM # 5656 New Field addition in RTV PO (ALM#5397)- part 2.2
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "1022"
    And  I click on "RETURNS"
    And  I click on "Create Return to Vendor"
    And  I switch to the first iFrame in SAP
    And  I select "Vendor Field" and enter "<Vendor>" in NWBC page
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I enter "Damaged" value into column name "Reason" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    And  I verify the success message with green image
    And  I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I enter "<Driver name>" information on "Driver Name" in Purchase order page
    And  I hit tab
    And  I enter "<RGA/RMA#>" information on "RGA/RMA#" in Purchase order page
    And  I hit tab
    And  I enter "<BOL/PRO#>" information on "BOL/PRO#" in Purchase order page
    And  I hit tab
    And  I enter "<Original PO#>" information on "Original PO#" in Purchase order page
    And  I hit tab
    And  I click on "Method of Return" in po page
    And  I click on "<Delivery Type>"
    And  I enter "4" tracking numbers
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "Create Return Order" Iframe in NWBC
    And  I click on "Save and Print Preview"
    Then I verify the error message with red image
    When I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I clear one tracking number at row "4"
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "Create Return Order" Iframe in NWBC
    And  I click on "Save and Print Preview"
    Then I save the Return purchase order
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "RETURNS"
    And  I click on "Open Returns" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I click on the purchase order number for open returns in NWBC page
    And  I click on "Post Goods Return"
    And  I switch back to the main window in SAP
    And  I switch to the pop up window
    And  I switch to RTV popup Iframe
    Then I verify "<Article>" value for column name "Article" at line number "1" in items table in nwbc
    And  I verify "<Quantity>" value for column name "Quantity" at line number "1" in items table in nwbc
    And  I verify "<Quantity>" value for column name "Ordered" at line number "1" in items table in nwbc
    When I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    Then I verify "<Driver name>", "<RGA/RMA#>", "<BOL/PRO#>", "<Original PO#>" and tracking numbers in return information tab
    When I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to RTV popup Iframe
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I enter "ZDP_RTV_TRACK_NO" into the field with label "Table"
    And  I hit return
    And  I select "Purchasing Doc." field and enter previously saved po in "from" field in se16n
    And  I execute
    Then I verify the tracking numbers for RTV in SE16N

    Examples:
      | Vendor | Article | Quantity | Unit Price | TCode | Delivery Type | Driver name | RGA/RMA# | BOL/PRO#   | Original PO# |
      | 10173  | 90058   | 3        | 20         | SE16n | Call Tag      | DN01        | RGA 01   | BOL 012345 | OP01         |

  @5588
  Scenario Outline: AG5660_2 Create new functionality to allow stores & WH to cancel incorrectly created RTV PO
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I click on "Return to Vendor"
    And  I enter "<DeptOfTransfer>" value into column name "Dept of Trans#" at line number "3" in items table in nwbc
    And  I click on "Save"
    And  I select row number "2" in adjustment table
    And  I click on "Return to Vendor"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "Yes"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    Then I save the "mim adjustment po" PO
    When I switch back to the main window in SAP
    And  I click on "Corrective Actions"
    And  I switch to the first iFrame in SAP
    And  I enter previously saved PO into field with label name "RTV PO #"
    And  I click on "Continue"
    And  I click on "Save"
    Then I validate reversed RTV PO success message
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    Then I verify Item detail tab is open
    When I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window
    Then I verify if item Overview is open
    And  I verify if the delete indicator is visible
    When I click on partial text "Purchase Order History"
    Then I verify article numbers are displayed under Purchase Order History Tab

    Examples:
      | TCode | DeptOfTransfer | Site  |
      | ME23N | DISCOUNT0717   | Store |

  @5397
  @returnToVendorHandheldMIM
  Scenario Outline: AGM # 5656 New Field addition in RTV PO (ALM#5397)- part 6
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "RETURNS"
    And  I click on "Create Return to Vendor"
    And  I switch to the first iFrame in SAP
    And  I select "Vendor Field" and enter "<Vendor>" in NWBC page
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I enter "Damaged" value into column name "Reason" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    And  I verify the success message with green image
    And  I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I enter "<Driver name>" information on "Driver Name" in Purchase order page
    And  I hit tab
    And  I enter "<RGA/RMA#>" information on "RGA/RMA#" in Purchase order page
    And  I hit tab
    And  I enter "<BOL/PRO#>" information on "BOL/PRO#" in Purchase order page
    And  I hit tab
    And  I enter "<Original PO#>" information on "Original PO#" in Purchase order page
    And  I hit tab
    And  I click on "Method of Return" in po page
    And  I click on "<Delivery Type>"
    And  I enter "4" tracking numbers
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "Create Return Order" Iframe in NWBC
    And  I click on "Save and Print Preview"
    Then I verify the error message with red image
    When I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I clear one tracking number at row "4"
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "Create Return Order" Iframe in NWBC
    And  I click on "Save and Print Preview"
    Then I save the Return purchase order
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I select "Transaction Code" and enter "WSRS_HI_MENU" in common tcodes
    And  I click on element with title attribute value "Test"
    And  I select "Scanner Site" and set the site "<Site>" in scanner
    And  I select "PDC Type" and set the site "<PDC Type>" in scanner
    And  I click on "ENT OK"
    And  I select "Scanner Select" and enter "<Goods Movement>" in common tcodes
    And  I click on "ENT OK"
    And  I select "Scanner Select" and enter "<Goods Movement>" in common tcodes
    And  I click on "ENT OK"
    And  I enter Purchase Order Number into "RefDocNo input" in common tcodes
    And  I click on "ENT OK"
    And  I click on "ENT OK"
    And  I enter "<Article>" into the field with label "Article"
    And  I click on "ENT OK"
    And  I enter "<Article>" into the field with label "Article"
    And  I click on "ENT OK"
    And  I enter "<Article>" into the field with label "Article"
    And  I click on "ENT OK"
    When I click on "F1 Save"
    And  I check for element with text "Success"

    Examples:
      | Vendor | Article | Quantity | Unit Price | TCode   | PDC Type | Delivery Type | Goods Movement | Site  | Driver name | RGA/RMA# | BOL/PRO#   | Original PO# |
      | 10173  | 90058   | 3        | 20         | /n SE93 | MC9190   | FedEx/UPS     | 2              | Store | DN01        | RGA 01   | BOL 012345 | OP01         |

  @5397
  @returnToVendorBillOfLading
  Scenario Outline: New Field addition in RTV PO (BOL/PRO# verification)
    """ edit function is not enabled"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    Then I set the site to "<Start Site>"
    When I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    Then I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "AutomationRTV"
    And  I select row number "3" in adjustment table
    When I click on "Return to Vendor"
    When I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    Then I enter alphanumeric number in BOL/PRO# field
    When I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    Then I verify the success message with green image

    Examples:
      | Start Site |
      | 9000       |

  @5397
  @returnToVendorScanSaveGenerate
  Scenario Outline: New Field addition in RTV PO (RTV Scan & save, RTV Generate)
  """ Cannot run in stage as automationrtv is edit button is not enabled """
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Start Site>"
    And  I click on "ADJUSTMENTS" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    Then I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "AutomationRTV"
    And  I select row number "3" in adjustment table
    And  I click on "Details"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I save the value of "Article Document Num" field in details tab
    And  I save the value of "Art. Doc.Item" field in details tab
    And  I save the value of "Vendor" field in details tab
    And  I save the value of "Mat. Doc. Year" field in details tab
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I delete all cookies
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "SE93" in the command field
    And  I select "Transaction Code" and enter "WSRS_HI_MENU" in common tcodes
    And  I hit return
    And  I click on element with title attribute value "Test"
    And  I select "Scanner Site" and set the site "<Start Site>" in scanner
    And  I enter "<PDC Type>" into the field with label "PDC Type"
    And  I click on "ENT OK"
    And  I select "Scanner Select" and enter "<Adjustments>" in common tcodes
    And  I click on "ENT OK"
    And  I wait for "Scanner Select" field to be visible in common tcodes
    And  I select "Scanner Select" and enter "<RTV - Scan and Save>" in common tcodes
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I click on "ENT OK"
    And  I click on "F2 Item O"
    Then I verify Article Document number is present in Item Overview
    When I click on "F1 Save"
    And  I check for element with text "Success"
    When I click on "ENT OK"
    And  I click on "F12 Exit"
    And  I select "Scanner Select" and enter "<RTV - Generate>" in common tcodes
    And  I click on "ENT OK"
    And  I wait for "vendor input" field to be visible in common tcodes
    And  I enter vendor number in Scanner's RTV - generate
    And  I click on "ENT OK"
    And  I wait for element with text of "Total" to be visible
    And  I click on "F1 Save"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I check for element with text "Success"

    Examples:
      | Start Site | PDC Type | Adjustments | RTV - Scan and Save | RTV - Generate |
      | 9000       | MC9190   | 5           | 1                   | 2              |

  @5682
  Scenario Outline: AGM Defect # 892 Not able to edit Adjustment RTV PO in SAP ECC.(Negative)
  """ Not Working Currently due to problem of price input"""
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    When I select the PO type "Corp Merch PO"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I click on "Org. Data"
    And  I select the input field with label "Purch. Group" at position "1" and enter "<Purch. Group>"
    Then I verify if item Overview is open
    When I enter the PO value for "Article Number" to "<Article>", "Quantity" to "<Quantity>", "Site" to "<Site>" from preferred data source
    And  I click "Returns Item" at row number "1"
    And  I enter the purchase order value for "Net Price" to "<Net Price>" from preferred data source
    And  I verify the Error status in SAP
    Examples:
      | TCode    | Vendor | Purch. Group | Article | Quantity | Net Price | Site |
      | /n ME21N | 10236  | 01           | 80007   | 1        | 10        | 1024 |

  @5682
  Scenario Outline: AGM Defect # 892 Not able to edit Adjustment RTV PO in SAP ECC.(Positive)
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Start Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "RTV Pending"
    And  I select row number "3" in adjustment table
    And  I click on "Return to Vendor"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "Yes"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    Then I save the Return purchase order
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window
    Then I verify if item Overview is open
    And  I set the input for "Quantity" at row number "1" to "<PO Quantity>"
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    And  I click on "Save"
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP

    Examples:
      | TCode | PO Quantity | Start Site |
      | ME22N | 6           | Store      |

  @4273
  Scenario Outline: SM_03_Adjustment Processing_Process Adjustments (Handheld) Part-1
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "RTV Pending"
    And  I select row number "3" in adjustment table
    And  I click on "Details"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I save the value of "Article Document Num" field in details tab
    And  I save the value of "Art. Doc.Item" field in details tab
    And  I save the value of "Vendor" field in details tab
    And  I save the value of "Mat. Doc. Year" field in details tab
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I select "Transaction Code" and enter "WSRS_HI_MENU" in common tcodes
    And  I hit return
    And  I click on element with title attribute value "Test"
    And  I select "Scanner Site" and set the site "<Site>" in scanner
    And  I enter "<PDC Type>" into the field with label "PDC Type"
    And  I click on "ENT OK"
    And  I enter "<Adjustments>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter "<Edit Adjustment Data>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I click on "ENT OK"
    And  I enter "10" into the field with label "Tread Depth"
    And  I enter "DISCOUNT0717" into the field with label "DOT"
    And  I click on "ENT OK"
    And  I check for element with text "Success"
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I enter "WSRS_HI_MENU" into the field with label "Transaction Code"
    And  I hit return
    And  I click on element with title attribute value "Test"
    And  I select "Scanner Site" and set the site "<Site>" in scanner
    And  I enter "<PDC Type>" into the field with label "PDC Type"
    And  I click on "ENT OK"
    And  I enter "<Adjustments>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter "<Scrap Product>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I enter "31" into the field with label "Reason"
    And  I click on "ENT OK"
    And  I click on "F2 Item O"
    Then I verify Article Document number is present in Item Overview
    When I click on "F1 Save"
    And  I check for element with text "Success"

    Examples:
      | Site  | PDC Type | Adjustments | Scrap Product | TCode | Edit Adjustment Data |
      | Store | MC9190   | 5           | 3             | SE93  | 4                    |

  @5681
  Scenario Outline: SM_03_ Provide new icons on MIM adj.screen to show if RTW STO has been received in WH and ready to process
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "Store"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "RTV Pending"
    And  I select row number "3" in adjustment table
    And  I click on "Details"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I save the value of "Article Document Num" field in details tab
    And  I save the value of "Art. Doc.Item" field in details tab
    And  I save the value of "Vendor" field in details tab
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Send to Warehouse"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I save the "Adjustment Transfer created" PO
    And  I switch back to the main window in SAP
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I search for purchase order number in mim adjustment table and select the line item
    And  I click on "Return to Vendor"
    Then I verify the error message with red image
    When I switch back to the main window in SAP
    And  I click on "RECEIVING"
    And  I click on "Post Goods Receipt"
    And  I switch to the first iFrame in SAP
    And  I enter Purchase Order Number into "Post Goods Issue PO"
    And  I click on "Continue"
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image
    Examples:
      | Site |
      | 9000 |

  @5652
  Scenario Outline: SM_03_Create new functionality to allow stores & WH to cancel the incorrect scrapping
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "RTV Pending"
    And  I select row number "3" in adjustment table
    And  I click on "Details"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I save the value of "Article Document Num" field in details tab
    And  I save the value of "Art. Doc.Item" field in details tab
    And  I save the value of "Vendor" field in details tab
    And  I save the value of "Mat. Doc. Year" field in details tab
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Scrap"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I enter "<Reason>" into the field with label "Reason for Mvmt"
    And  I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I switch back to the main window in SAP
    And  I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    When I click on "ADJUSTMENTS"
    And  I click on "Corrective Actions"
    And  I switch to the first iFrame in SAP
    And  I click on "Undo Scrap"
    And  I enter "Article Document Num" of the line item into input field with label "Article Doc #"
    And  I enter "Art. Doc.Item" of the line item into input field with label "Art. Doc.Item"
    And  I enter "Mat. Doc. Year" of the line item into input field with label "Mat. Doc. Year"
    And  I click on "Continue"
    And  I click on "Save"
    Then I verify the success message with green image
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I enter "ZTB_ADJ_RET_REV" into the field with label "Table"
    And  I hit return
    Then I select "Article Document Num" field and enter the value of "Article Document Num" saved previously in "from" field in se16n table
    And  I execute
    And  I save Reversed article document
    And  I enter t-code "<TCode2>" in the command field
    And  I enter order number into field with "Article Doc." label
    And  I hit return
    Then I verify the value attribute of element "Movement Type in MB03" matches "<Movement Type>"

    Examples:
      | Site  | TCode    | TCode2  | Movement Type | Reason      |
      | Store | /n se16n | /n MB03 | 352           | Road Hazard |

  @5712
  Scenario Outline: SM_04_Move the ZRTV Printout & EDI 142 in Post GR
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Start Site>"
    And  I click on "RETURNS"
    And  I click on "Create Return to Vendor"
    And  I switch to the first iFrame in SAP
    And  I enter "<Vendor>" into the field with label "Vendor"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I enter "Damaged" value into column name "Reason" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    And  I verify the success message with green image
    And  I click on "Return Information"
    And  I switch back to the main window in SAP
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I click on "Method of Return" in po page
    And  I click on "<Delivery Type>"
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "Create Return Order" Iframe in NWBC
    And  I click on "Save and Print Preview"
    Then I save the Return purchase order
    When I switch back to the main window in SAP
    And  I click on "Post Goods Return"
    And  I switch to the first iFrame in SAP
    And  I enter Purchase Order Number into "Post Goods Issue PO"
    And  I click on "Continue"
    And  I click on "Save and Print Preview"
    And  I save the Return purchase order
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I enter order number into field with "Article Doc." label
    And  I hit return
    And  I click on partial text "Details from Item"
    And  I click on partial text "Messages"
    Then I check for element with text "Z348"
    And  I check for element with text "Z142"

    Examples:
      | Vendor | Article | Quantity | Unit Price | TCode | Delivery Type  | Start Site |
      | 10173  | 90058   | 3        | 20         | MB03  | Vendor Pick-up | Store      |

  @5737
  Scenario Outline: AGM # 7859 Change Reason code Michelin Type 5 to Policy Concession (ALM#5737)
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I select row number "2" in adjustment table
    And  I click on "Scrap"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I enter "<Reason>" into the field with label "Reason for Mvmt"
    And  I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    And  I save the Return purchase order
    And  I switch back to the main window
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I enter order number into field with "Article Doc." label
    And  I hit return
    And  I click on partial text "Details from Item"
    And  I check for element with text "Too Old"

    Examples:
      | Site  | TCode | Reason  |
      | Store | MB03  | Too Old |

  @9684
  Scenario Outline: AGM # 26117 RTV - Product Service Claim not Printing_RTK Testing (ALM#9684)
    """Edit function not available in stage"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "RTV Pending"
    And  I select row number "3" in adjustment table
    And  I click on "Details"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I save the value of "Article Document Num" field in details tab
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Return to Vendor"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "Yes"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "SAVE"
    And  I switch back to the main window
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    Then I save the "mim adjustment po" PO
    When I switch back to the main window in SAP
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I select "Site Input" and set the site "<Site>" in scanner
    And  I enter order number into field with "Purchase Order number" label
    And  I execute
    And  I select line item with Purchase order number
    And  I click on "Print Button" in common Tcodes
    And  I switch to the first iFrame in SAP
    And  I enter "LOCL" into the field with label "Output Device"
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on element with title attribute value "Continue"
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I enter t-code "<TCode1>" in the command field
    And  I execute
    And  I click on "First Smart Form" in common Tcodes
    And  I copy the pdf content to clip board
    Then I verify the PDF has the saved Article document Number
    Examples:
      | TCode              | TCode1  | Site  |
      | ZSM_PRNT_SERVCLAIM | /n SP01 | Store |

  @5396
  Scenario Outline: SM_04_Desktop MIM to process new returns with regular and MISC. Articles (ALM#5396) Part-1,2,3&4
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Start Site>"
    And  I click on "RETURNS"
    And  I click on "Create Return to Vendor"
    And  I switch to the first iFrame in SAP
    And  I enter "<Vendor>" into the field with label "Vendor"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I enter "<Reason>" value into column name "Reason" at line number "1" in items table in nwbc
    And  I hit return
    Then I verify the success message with green image
    When I click on "Save and Print Preview"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    Then I verify the success message with green image

    Examples:
      | Vendor | Article | Quantity | Unit Price | Start Site | Reason        |
      | 10226  | 44444   | 3        | 20         | Store      | Damaged       |
      | 10226  | 44444   | 5        | 30         | Store      | New Defective |
      | 11621  | 10059   | 2        | 50         | Store      | Wrong Fitment |
      | 11621  | 10059   | 4        | 100        | Store      | New Defective |

  @5396
  Scenario Outline: SM_04_Desktop MIM to process new returns with one article when PIR Exist and doesn't exist   (ALM#5396) Part-5&6
  """NOTE:Part 5& 6 is seperated from the above part because we dont have to verify for the success message with these scenarios"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Start Site>"
    And  I click on "RETURNS" in NWBC page
    And  I click on "Create Return to Vendor"
    And  I switch to the first iFrame in SAP
    And  I enter "<Vendor>" into the field with label "Vendor"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I enter "<Reason>" value into column name "Reason" at line number "1" in items table in nwbc
    And  I hit return
    And  I click on "Save and Print Preview"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    Then I verify the success message with green image

    Examples:
      | Vendor | Article | Quantity | Unit Price | Start Site | Reason      |
      | 10846  | 90019   | 3        |            | Store      | Damaged     |
      | 10846  | 90019   | 3        | 30         | Store      | Mis-shipped |

  @5396
  Scenario Outline: SM_04_Desktop MIM to process new returns with two articles (ALM#5396) Part-7&8
  """NOTE:Part 5& 6 is seperated from the above part because here we have to validate with two article for the above
     scenarios we are just validating with one article."""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Start Site>"
    And  I click on "RETURNS" in NWBC page
    And  I click on "Create Return to Vendor"
    And  I switch to the first iFrame in SAP
    And  I enter "<Vendor>" into the field with label "Vendor"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I enter "<Reason>" value into column name "Reason" at line number "1" in items table in nwbc
    And  I hit return
    Then I verify the success message with green image
    And  I enter "<Article2>" value into column name "Article" at line number "2" in items table in nwbc
    And  I enter "<Quantity2>" value into column name "Quantity" at line number "2" in items table in nwbc
    And  I enter "<Unit Price2>" value into column name "Unit Price" at line number "2" in items table in nwbc
    And  I enter "<Reason>" value into column name "Reason" at line number "2" in items table in nwbc
    And  I hit return
    When I click on "Save and Print Preview"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    Then I verify the success message with green image

    Examples:
      | Vendor | Article | Quantity | Unit Price | Start Site | Reason        | Article2 | Quantity2 | Unit Price2 |
      | 11621  | 44444   | 1        | 20         | Store      | Damaged       | 10059    | 1         | 20          |
      | 11621  | 44444   | 3        | 30         | Store      | Wrong Fitment | 10059    | 4         | 30          |

  @6126
  Scenario Outline: Testing_AGM # 9669 DOT # 8 digit validation (ALM#6126)
  """ Last two steps on ALM are tested in #6142 and #6145 """
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I enter "<Thirty Seconds>" value into column name "32nds" at line number "3" in items table in nwbc
    And  I enter "<Miles On>" value into column name "Miles On" at line number "3" in items table in nwbc
    And  I enter "<Miles Off>" value into column name "Miles Off" at line number "3" in items table in nwbc
    And  I enter "<Input>" value into column name "Dept of Trans#" at line number "3" in items table in nwbc
    And  I click on "Save"
    Then  I verify the error message with red image
    When I click on "Cancel"
    And  I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I enter "<Input2>" value into column name "Dept of Trans#" at line number "3" in items table in nwbc
    And  I click on "Save"
    Then  I verify the success message with green image
    And  I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I enter "<Input3>" value into column name "Dept of Trans#" at line number "3" in items table in nwbc
    And  I click on "Save"
    Then  I verify the success message with green image

    Examples:
      | Site  | Miles On | Miles Off | Thirty Seconds | Input        | Input2       | Input3       |
      | Store | 1000     | 2000      | 11             | @incorrect25 | Discount1115 | Discount1117 |

  @5195
  Scenario Outline: SM_03_New PO type and new changes in Adjustment form (ALM#5195)
    """ Awaiting valid validations"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "RTV Pending"
    And  I select row number "3" in adjustment table
    And  I click on "Send to Warehouse"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    And  I save the "Adjustment Transfer created" PO
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    Then I check for element with value "<Text>"
    When I enter t-code "<TCode2>" in the command field
    And  I execute
    And  I click on "First Smart Form" in common Tcodes
    And  I copy the pdf content to clip board

    Examples:
      | Site  | TCode    | Text                | Input        | TCode2  | Miles On | Miles Off | Thirty Seconds |
      | Store | /n ME23N | Adjustment Transfer | Discount1115 | /n SP01 | 1000     | 2000      | 11             |

  @5195
  Scenario Outline: Delete PDF (ALM#5195)
  """ These steps have to be separate, as it has to run regardless if the first PDF test case fails or not also, this script will run only in RTQ"""
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I execute
    And  I select all check boxes
    And  I click on element with title attribute value "Delete"
    And  I acknowledge pdf delete

    Examples:
      | TCode   |
      | /n SP01 |

  @5512
  Scenario Outline: SM_03_Edit functionality for Original Article in NWBC MIM (ALM#5512) Part-1
  """Before and after change print labels"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "store"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "RTV Pending"
    And  I select row number "3" in adjustment table
    And  I click on "Details"
    And  I switch back to the main window in SAP
    Then I switch to the first iFrame in SAP
    And  I save the value of "Article Document Num" field in details tab
    And  I save the value of "Art. Doc.Item" field in details tab
    And  I save the value of "Vendor" field in details tab
    And  I save the value of "Mat. Doc. Year" field in details tab
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Print Labels"
    Then I verify the success message with green image
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I execute
    And  I click on "First Smart Form" in common Tcodes
    And  I copy the pdf content to clip board
    Then I verify the PDF in SAP has Content Like "<Input>"
    And  I verify the PDF in SAP has Content Like "<Thirty Seconds>"
    And  I verify the PDF in SAP has Content Like "<Miles On>"
    And  I verify the PDF in SAP has Content Like "<Miles Off>"
    And  I verify the PDF has the saved Article document Number
    And  I enter t-code "<TCode1>" in the command field
    And  I select "Transaction Code" and enter "WSRS_HI_MENU" in common tcodes
    And  I click on element with title attribute value "Test"
    And  I select "Scanner Site" and set the site "<Start Site>" in scanner
    And  I enter "<PDC Type>" into the field with label "PDC Type"
    And  I click on "ENT OK"
    And  I enter "<Adjustments>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter "<Edit Adjustment Data>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I click on "ENT OK"
    And  I wait for "5" seconds
    And  I enter "10" into the field with label "Tread Depth"
    And  I enter "<Input>" into the field with label "DOT"
    And  I click on "ENT OK"
    When I click on "ENT OK"
    And  I click on "F12 Exit"
    And  I enter "<Print Adjustment Label>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I click on "ENT OK"
    And  I click on "ENT Print"
    When I click on "ENT OK"
    And  I enter t-code "<TCode>" in the command field
    And  I execute
    And  I click on "First Spool" in NWBC page
    And  I copy the pdf content to clip board
    Then I verify the PDF in SAP has Content Like "<Input>"
    And  I verify the PDF in SAP has Content Like "10"
    And  I verify the PDF has the saved Article document Number

    Examples:
      | TCode  | TCode1  | Start Site | PDC Type | Adjustments | Edit Adjustment Data | Print Adjustment Label | Input        | Miles On | Miles Off | Thirty Seconds |
      | /nSP01 | /n SE93 | Store      | MC9190   | 5           | 4                    | 5                      | DISCOUNT0717 | 56000    | 60000     | 08             |

  @6142
  Scenario Outline: Testing_AGM # 8412 Make Tread Depth Mandatory (ALM#6142)
  """ Scenario 3 and 4"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I enter "<Thirty Seconds>" value into column name "32nds" at line number "3" in items table in nwbc
    And  I enter "<Miles On>" value into column name "Miles On" at line number "3" in items table in nwbc
    And  I enter "<Miles Off>" value into column name "Miles Off" at line number "3" in items table in nwbc
    And  I enter "<Input>" value into column name "Dept of Trans#" at line number "3" in items table in nwbc
    And  I click on "Save"
    And  I select row number "2" in adjustment table
    And  I click on "Details"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I save the value of "Article Document Num" field in details tab
    And  I save the value of "Art. Doc.Item" field in details tab
    And  I save the value of "Vendor" field in details tab
    And  I save the value of "Mat. Doc. Year" field in details tab
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I select "Transaction Code" and enter "WSRS_HI_MENU" in common tcodes
    And  I click on element with title attribute value "Test"
    And  I select "Scanner Site" and set the site "<Site>" in scanner
    And  I enter "<PDC Type>" into the field with label "PDC Type"
    And  I click on "ENT OK"
    And  I enter "<Adjustments>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter "<Return To Vendor>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I click on "F2 Item O"
    And  I enter "<Item Number>" into the field with label "Select"
    And  I click on "F1 Save"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I check for element with text "Success"

    Examples:
      | Site  | TCode | PDC Type | Adjustments | Item Number | Return To Vendor | Input        | Miles On | Miles Off | Thirty Seconds |
      | Store | SE93  | MC9190   | 5           | 1           | 2                | DISCOUNT1112 | 1000     | 2000      | 11             |

  @6165
  Scenario Outline: Testing_AGM # 8412 Make Tread Depth Mandatory (ALM#6165)
  """ Scenario 1 and 2 """
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I enter "<Thirty Seconds>" value into column name "32nds" at line number "3" in items table in nwbc
    And  I enter "<Miles On>" value into column name "Miles On" at line number "3" in items table in nwbc
    And  I enter "<Miles Off>" value into column name "Miles Off" at line number "3" in items table in nwbc
    And  I enter "<Input>" value into column name "Dept of Trans#" at line number "3" in items table in nwbc
    And  I click on "Save"
    And  I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I enter "<Input1>" value into column name "32nds" at line number "3" in items table in nwbc
    And  I click on "Save"
    Then I verify the error message with red image
    And  I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I enter "<Thirty Seconds>" value into column name "32nds" at line number "3" in items table in nwbc
    And  I click on "Save"
    And  I select row number "2" in adjustment table
    And  I click on "Details"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I save the value of "Article Document Num" field in details tab
    And  I save the value of "Art. Doc.Item" field in details tab
    And  I save the value of "Vendor" field in details tab
    And  I save the value of "Mat. Doc. Year" field in details tab
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I select "Transaction Code" and enter "WSRS_HI_MENU" in common tcodes
    And  I click on element with title attribute value "Test"
    And  I select "Scanner Site" and set the site "<Site>" in scanner
    And  I enter "<PDC Type>" into the field with label "PDC Type"
    And  I click on "ENT OK"
    And  I enter "<Adjustments>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter "<Return To Vendor>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I click on "F2 Item O"
    And  I enter "<Item Number>" into the field with label "Select"
    And  I click on "F1 Save"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I check for element with text "Success"

    Examples:
      | Site  | Input1 | Input        | Miles On | Miles Off | Thirty Seconds | TCode | Adjustments | Return To Vendor | PDC Type | Item Number |
      | Store | %      | DISCOUNT1114 | 1000     | 2000      | 12             | SE93  | 5           | 1                | MC9190   | 1           |

  @6165
  Scenario Outline: Testing_AGM # 8412 Make Tread Depth Mandatory (ALM#6165)
  """ Scenario 3 """
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I enter "<Thirty Seconds>" value into column name "32nds" at line number "3" in items table in nwbc
    And  I enter "<Miles On>" value into column name "Miles On" at line number "3" in items table in nwbc
    And  I enter "<Miles Off>" value into column name "Miles Off" at line number "3" in items table in nwbc
    And  I enter "<Input>" value into column name "Dept of Trans#" at line number "3" in items table in nwbc
    And  I click on "Save"
    And  I select row number "2" in adjustment table
    And  I click on "Details"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I save the value of "Article Document Num" field in details tab
    And  I save the value of "Art. Doc.Item" field in details tab
    And  I save the value of "Vendor" field in details tab
    And  I save the value of "Mat. Doc. Year" field in details tab
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I select "Transaction Code" and enter "WSRS_HI_MENU" in common tcodes
    And  I click on element with title attribute value "Test"
    And  I select "Scanner Site" and set the site "<Site>" in scanner
    And  I enter "<PDC Type>" into the field with label "PDC Type"
    And  I click on "ENT OK"
    And  I enter "<Adjustments>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I enter "<Return To Vendor>" into the field with label "Select"
    And  I click on "ENT OK"
    And  I click on "ENT OK"
    And  I enter article document number in Scanner's RTV-Scan and Save
    And  I click on "ENT OK"
    And  I click on "F2 Item O"
    And  I enter "<Item Number>" into the field with label "Select"
    And  I click on "F1 Save"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I check for element with text "Success"

    Examples:
      | Site  | Input        | Miles On | Miles Off | Thirty Seconds | TCode | Return To Vendor | Item Number | PDC Type | Adjustments |
      | Store | DISCOUNT1116 | 1000     | 2000      | 12             | SE93  | 2                | 1           | MC9190   | 5           |

  @5607
  Scenario Outline: SM_03_Create new functionality to allow stores & WH to cancel incorrectly created RTW STO (ALM#5607)
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "RTV Pending"
    And  I select row number "3" in adjustment table
    And  I click on "Send to Warehouse"
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    And  I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    Then I save the Return purchase order
    When I switch back to the main window
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "ADJUSTMENTS" in NWBC page
    And  I click on "Corrective Actions"
    And  I switch to the first iFrame in SAP
    And  I click on "Undo Return to Warehouse"
    And  I enter Purchase Order Number into "STO #"
    And  I hit return
    And  I click on "Save"
    Then I verify the success message with green image
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    Then I verify Item detail tab is open
    When I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify if item Overview is open
    And  I verify if the delete indicator is visible
    When I click on "Purchase Order History"
    Then I verify article numbers are displayed under Purchase Order History Tab

    Examples:
      | TCode    | Site  |
      | /n ME23N | Store |

  @6282
  Scenario Outline: Testing_AGM # 9676 Adjustment Label Printing without article description (ALM#6282)
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I edit one of the row in the warehouse adjustment table and edit the Article details with the view as "RTV Pending"
    And  I select row number "3" in adjustment table
    And  I click on "Print Labels"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    When  I execute
    And  I click on "First Smart Form" in common Tcodes
    And  I copy the pdf content to clip board
    Then I verify the PDF in SAP has Content Like "Return Reason"
    And  I verify the PDF in SAP has Content Like "Article/Description"

    Examples:
      | Site  | TCode   |
      | store | /n SP01 |

  @5478
  Scenario Outline: AGM # 6988 Create New Reasons for Return & Scrapping (AGM#5478)
  """Reason validation for scrap"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "store"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I enter "RTV Pending" into the field with label "View"
    And  I select row number "3" in adjustment table
    And  I click on "Scrap"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I enter "<Reason>" into the field with label "Reason for Mvmt"
    And  I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    And  I save the Return purchase order
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I enter order number into field with "Article Doc." label
    And  I hit return
    And  I click on partial text "Details from Item"
    Then I check for element with text "Reason for Mvmt"
    And  I check for element with text "Not Enough Tread"

    Examples:
      | Reason        | TCode |
      | New Defective | MB03  |

  @5478
  Scenario Outline: AGM # 6988 Create New Reasons for Return & Scrapping (AGM#5478)
  """Reason validation for return to vendor"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "Store"
    And  I click on "RETURNS"
    And  I click on "Create Return to Vendor"
    And  I switch to the first iFrame in SAP
    And  I enter "<Vendor>" into the field with label "Vendor"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I enter "<Reason>" value into column name "Reason" at line number "1" in items table in nwbc
    And  I hit return
    And  I enter "<Article2>" value into column name "Article" at line number "2" in items table in nwbc
    And  I enter "<Quantity2>" value into column name "Quantity" at line number "2" in items table in nwbc
    And  I enter "<Unit Price2>" value into column name "Unit Price" at line number "2" in items table in nwbc
    And  I enter "<Reason2>" value into column name "Reason" at line number "2" in items table in nwbc
    And  I hit return
    And  I click on "Save and Print Preview"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I save the Return purchase order
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode1>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    Then I click on partial text "Retail"
    And  I check for element with text "<Reason>"
    And  I click on element with partial value "[ 10 ] 40520 , 275/40R18  99W B NIT  NT05"
    And  I click on partial text "[ 20 ] 40206"
    Then I check for element with text "<Reason2>"
    Examples:
      | Vendor | Article | Quantity | Unit Price | Reason        | Article2 | Quantity2 | Unit Price2 | Reason2 | TCode | TCode1   |
      | 10173  | 40520   | 3        | 20         | New Defective | 40206    | 4         | 20          | Recall  | MB03  | /n ME23N |

  @6446
  Scenario Outline: Testing_AGM # 1550 Undo RTW not working (ALM#6446)
  """In RTD Not able to correctly access values in items table in nwbc"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And  I key "down" the control key
    And  I select row number "2" in adjustment table
    And  I select row number "3" in adjustment table
    And  I key "up" the control key
    And  I click on "Edit"
    And  I enter "<Input>" value into column name "Dept of Trans#" at line number "3" in items table in nwbc
    And  I enter "<Thirty Seconds>" value into column name "32nds" at line number "3" in items table in nwbc
    And  I enter "<Miles On>" value into column name "Miles On" at line number "3" in items table in nwbc
    And  I enter "<Miles Off>" value into column name "Miles Off" at line number "3" in items table in nwbc
    And  I hit return
    And  I enter "<Input>" value into column name "Dept of Trans#" at line number "4" in items table in nwbc
    And  I enter "<Thirty Seconds>" value into column name "32nds" at line number "4" in items table in nwbc
    And  I enter "<Miles On>" value into column name "Miles On" at line number "4" in items table in nwbc
    And  I enter "<Miles Off>" value into column name "Miles Off" at line number "4" in items table in nwbc
    And  I click on "Save"
    And  I key "down" the control key
    And  I select row number "2" in adjustment table
    And  I select row number "3" in adjustment table
    And  I key "up" the control key
    And  I click on "Send to Warehouse"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "SAVE"
    And  I switch back to the main window in SAP
    And  I switch to "MIM Adjustment Frame" Iframe in NWBC
    And  I save the Return purchase order
    And  I switch back to the main window in SAP
    And  I click on "Corrective Actions"
    And  I switch to the first iFrame in SAP
    And  I click on "Undo Return to Warehouse"
    And  I enter Purchase Order Number into "STO #"
    And  I click on "Continue"
    And  I click on "Save"
    And  I wait for "5" seconds
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify if item Overview is open
    And  I verify if the delete indicator is visible
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode2>" in the command field
    And  I enter "ZTB_ADJ_RET" into the field with label "Table"
    And  I hit return
    And  I select "Article Document Num" field and enter previously saved po in "from" field in se16n
    And  I hit return
    And  I execute
    Then I verify the Error status in SAP

    Examples:
      | Site  | TCode | Input2        | Input        | Miles On | Miles Off | Thirty Seconds | TCode2 |
      | Store | ME23N | Discount 1518 | Discount1115 | 1000     | 2000      | 11             | SE16n  |

  @8397
  Scenario Outline: ERP_PRC Error in Net price calculating during order creation for articles with FET_SAP ECC PO creation(ALM#8397)
    """ Not Working Currently due to problem of price input"""
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode1>" in the command field
    When I select the PO type "RTV PO"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    And  I click on "Return Information"
    And  I enter "<Driver name>" into the field with label "Driver Name"
    And  I enter "<RGA/RMA#>" into the field with label "RGA/RMA #"
    And  I enter "<BOL/PRO#>" into the field with label "BOL/PRO #"
    And  I enter "<Original PO#>" into the field with label "Original PO #"
    And  I enter "<Method of return>" into the field with label "Method of Return"
    And  I verify if item Overview is open
    And  I set the input for "Article Number" at row number "1" to "40468"
    And  I set the input for "Quantity" at row number "1" to "<PO Quantity>"
    And  I set the input for "Net Price" at row number "1" to "<Net Price>"
    And  I set the input for "Site" at row number "1" to "<Site>"
    And  I set the input for "Article Number" at row number "2" to "44444"
    And  I set the input for "Quantity" at row number "2" to "<PO Quantity>"
    And  I set the input for "Site" at row number "2" to "<Site>"
    And  I set the input for "Net Price" at row number "2" to "10"
    And  I click "Returns Item" at row number "1"
    And  I click "Returns Item" at row number "2"
    And  I hit return
    And  I click on "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I click on "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I click on the "Conditions tab" element in po page to navigate

    Examples:
      | TCode | TCode1   | Vendor | Driver name | RGA/RMA# | BOL/PRO#   | Original PO# | Method of return | PO Quantity | Site | Purch. Group | store location | Net Price | TCode2  |
      | SE16N | /n ME21N | 10173  | DN01        | RGA 01   | BOL 012345 | OP01         | 01               | 2           | 1002 | 01           | 1024           | 10        | /nMe23N |

  @5443
  Scenario Outline: SM_04_Restrict manual incoming  PO with 80007 (ALM#5443)
  """ Does not work in RTD because of different warning returned when PO created with Article 80007"""
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "RETURNS" in NWBC page
    And  I click on "Create Return to Vendor"
    And  I switch to the first iFrame in SAP
    And  I enter "<Vendor>" into the field with label "Vendor"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I enter "<Reason>" value into column name "Reason" at line number "1" in items table in nwbc
    And  I hit return
    Then I verify the error message with red image
    When I switch back to the main window
    And  I click on "Ordering" in NWBC page
    And  I accept the alert message
    And  I click on "Create Non Merchandise Order" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I enter "Back Room Supplies" into the field with label "Merch. Category"
    And  I enter "<VendorNumber>" into the field with label "Vendor"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<ArticleQuantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I enter "<Unit Price>" value into column name "Unit Price" at line number "1" in items table in nwbc
    And  I hit return
    Then I verify the error message with red image
    When I switch back to the main window
    And  I click on "RETURNS" in NWBC page
    And  I accept the alert message
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    When I select the PO type "Corp Merch PO"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I enter "<Purch. Group>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value for "Article Number" to "<Article>" from preferred data source
    And  I enter the purchase order value for "Quantity" to "<PO Quantity>" from preferred data source
    And  I enter the purchase order value for "Site" to "<store location>" from preferred data source
    And  I set the input for "Net Price" at row number "1" to "<Net Price1>"
    And  I hit return
    Then I verify the Error status in SAP

    Examples:
      | Vendor | Article | Quantity | Unit Price | Site  | Reason  | VendorNumber | ArticleQuantity | TCode | Purch. Group | PO Quantity | Net Price1 | store location |
      | 10236  | 80007   | 3        | 20         | Store | Damaged | 16559        | 2               | ME21n | 01           | 01          | 10         | 1024           |

  @6143
  Scenario Outline: Testing_AGM # 1277 DOT # field is not working (ALM#6143)
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>"
    And  I click on "ADJUSTMENTS"
    And  I switch to the first iFrame in SAP
    And  I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue
    And I select row number "2" in adjustment table
    And  I click on "Edit"
    And  I enter "<Thirty Seconds>" value into column name "32nds" at line number "3" in items table in nwbc
    And  I enter "<Miles On>" value into column name "Miles On" at line number "3" in items table in nwbc
    And  I enter "<Miles Off>" value into column name "Miles Off" at line number "3" in items table in nwbc
    And  I enter "<Input2>" value into column name "Dept of Trans#" at line number "3" in items table in nwbc
    And  I click on "Save"
    Then I verify the error message with red image
    When I select row number "3" in adjustment table
    And  I click on "Edit"
    And  I enter "<Thirty Seconds>" value into column name "32nds" at line number "4" in items table in nwbc
    And  I enter "<Miles On>" value into column name "Miles On" at line number "4" in items table in nwbc
    And  I enter "<Miles Off>" value into column name "Miles Off" at line number "4" in items table in nwbc
    And  I enter "<Input2>" value into column name "Dept of Trans#" at line number "4" in items table in nwbc
    And  I click on "Save"
    Then I verify the error message with red image

    Examples:
      | Site  | Input2 | Miles On | Miles Off | Thirty Seconds |
      | Store | %      | 1000     | 2000      | 11             |