@sap
@vehicle
Feature: Vehicle

  Background:
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL

  @7171
  Scenario Outline: Adding a Trim to an existing vehicle in ZVEHICLE (ALM#7171)
    When  I enter t-code "<TCode>" in the command field
    And   I click on "Vehicle drop down button" in vehicle page
    And   I click on "Car"
    And   I click on "Year drop down button" in vehicle page
    And   I click on "<Year>"
    And   I click on "Make drop down button" in vehicle page
    And   I click on "<Make>"
    And   I click on "Model drop down button" in vehicle page
    And   I click on "<Model>"
    And   I click on element with title attribute value "Change an existing vehicle"
    And   I click on element with title attribute value "Build a new Trim with desc"
    And   I "double click" on row "1" into "Chassis Configurations" table in "1" column in vehicle page
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on "input box1" in vehicle page
    And   I hit F4 action
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "1" in SAP
    And   I click on "Menu Button" in vehicle page
    And   I scroll to following "Cab Style" element and click
    And   I double click on "<Cab Style value>"
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "input box1 text" matches "CABYSTYLE" displayed in vehicle page
    When  I click on "input box2" in vehicle page
    And   I hit F4 action
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "1" in SAP
    And   I scroll to following "TRIM Alpha Numeric" element and click
    And   I double click on "<TRIM Alpha Numeric value>"
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "input box2 text" matches "TRIM" displayed in vehicle page
    When  I click on "input box3" in vehicle page
    And   I hit F4 action
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "1" in SAP
    And   I scroll to following "Trim Letters" element and click
    And   I double click on "<Trim Letters value>"
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "input box3 text" matches "TRIMLTTRS" displayed in vehicle page
    When  I click on "input box4" in vehicle page
    And   I hit F4 action
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "1" in SAP
    And   I scroll to following "Trim Names" element and click
    And   I double click on "<Trim Names value>"
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "input box4 text" matches "TRIMNAMES" displayed in vehicle page
    When  I click on "input box5" in vehicle page
    And   I hit F4 action
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "1" in SAP
    And   I scroll to following "Drive Trim Names" element and click
    And   I double click on "<Drive Trim Names value>"
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "input box5 text" matches "DRVETRMNAM" displayed in vehicle page
    When  I click on "input box6" in vehicle page
    And   I hit F4 action
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "1" in SAP
    And   I scroll to following "Transmission" element and click
    And   I double click on "<Transmission value>"
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "input box6 text" matches "TRANSMSN" displayed in vehicle page
    When  I click on element with title attribute value "Apply the Trim/Submodel (Enter)"
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I select on row "2" into "Chassis Configurations" table in vehicle page
    And   I click on "Delete a Trim" in vehicle page
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Switch between Change / Display mode (F7)"

    Examples:
      | TCode    | Cab Style value | TRIM Alpha Numeric value | Trim Letters value | Trim Names value | Drive Trim Names value | Transmission value | Year | Make  | Model |
      | ZVEHICLE | Access Cab      | 1500                     | AMG                | Adventure        | AWD                    | 4-Speed            | 2012 | Acura | MDX   |

  @6534
  Scenario Outline: Test the functionality of "Global Notes" in ZVEHICLE (ALM#6534)
    When  I enter t-code "<TCode>" in the command field
    And   I click on "Special notes"
    And   I execute
    And   I click on element with title attribute value "Create Text"
    And   I switch to the iFrame at Index "0" in SAP
    And   I enter "<Input Text>" into the field with label "Text Name"
    And   I enter "<Input Text>" into the field with label "Short Title"
    And   I hit return
    And   I switch back to the main window in SAP
    And   I select "Input text box" and enter "<Input Text>" in vehicle page
    And   I click on element with title attribute value "Save"
    Then  I check for element with text "saved successfully"
    When  I click on element with title attribute value "Back"
    And   I click on element with title attribute value "Refresh"
    And   I wait for "5" seconds
    Then  I validate "Last Row" entered matches original "SPEC_ZVEHICLEAUTOMATION9999" in vehicle page
    When  I click on partial data "SPEC_ZVEHICLEAUTOMATION9999"
    And   I click on element with title attribute value "Delete Row"
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on element with title attribute value "Yes"
    And   I switch back to the main window
    Then  I check for element with text "Texts have been deleted successfuly"

    Examples:
      | TCode        | Input Text             |
      | /nZVEHGBNOTE | ZVEHICLEAUTOMATION9999 |

  @4800
  Scenario Outline: SAP-ECC_VEHICLE_VEHICLE_VER_CHASSIS_GLOBAL_NOTES_ADD_ADD_EDIT (ALM#4800)
    When  I enter t-code "<TCode>" in the command field
    And   I click on "Vehicle drop down button" in vehicle page
    And   I click on "Car"
    And   I click on "Year drop down button" in vehicle page
    And   I click on "<Year>"
    And   I click on "Make drop down button" in vehicle page
    And   I click on "<Make>"
    And   I click on "Model drop down button" in vehicle page
    And   I click on "<Model>"
    And   I click on element with title attribute value "Change an existing vehicle"
    And   I scroll to following "Display/change special notes" element and click
    And   I switch to the iFrame at Index "0" in SAP
    And   I wait for "2" seconds
    And   I select "text notes" and enter "special notes_Random" in vehicle page
    And   I click on element with title attribute value "Save entries and exit editor (Enter)"
    And   I switch back to the main window in SAP
    And   I scroll to following "Display/change TPMS notes" element and click
    And   I switch to the iFrame at Index "0" in SAP
    And   I select "text notes" and enter "TPMS notes_Random" in vehicle page
    And   I click on element with title attribute value "Save entries and exit editor (Enter)"
    And   I switch back to the main window in SAP
    And   I scroll to following "Display/change lift notes" element and click
    And   I switch to the iFrame at Index "0" in SAP
    And   I select "text notes" and enter "lift notes_Random" in vehicle page
    And   I click on element with title attribute value "Save entries and exit editor (Enter)"
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Switch between Change / Display mode (F7)"
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on element with title attribute value "Yes"
    And   I switch back to the main window in SAP
    And   I switch to the iFrame at Index "0" in SAP
    And   I hit return
    And   I switch back to the main window in SAP
    Then  I validate "input special notes" entered matches with saved data "saved_special note" in vehicle page
    And   I validate "input TPMS notes" entered matches with saved data "saved_TPMS note" in vehicle page
    And   I validate "input lift notes" entered matches with saved data "saved_lift note" in vehicle page

    Examples:
      | TCode    | Year | Make | Model   |
      | ZVEHICLE | 2014 | Ford | Mustang |

  @7080
  Scenario Outline: SAP-ECC_VEHICLE_VEHICLE_VER_OFFSET_CREATE_REAR (ALM#7080)
    When  I enter t-code "<TCode>" in the command field
    And   I click on "Vehicle drop down button" in vehicle page
    And   I click on "Car"
    And   I click on "Year drop down button" in vehicle page
    And   I click on "<Year>"
    And   I click on "Make drop down button" in vehicle page
    And   I click on "<Make>"
    And   I click on "Model drop down button" in vehicle page
    And   I click on "<Model>"
    And   I click on element with title attribute value "Change an existing vehicle"
    And   I click on element with title attribute value "Front or Front/Both"
    And   I click on "Front"
    And   I click on element with title attribute value "Maintain rear offsets"
    And   I switch to the iFrame at Index "0" in SAP
    And   I enter "8" into the field with label "For diameters greater than:"
    And   I click on "Create new rear axle row" in vehicle page
    And   I enter "5" value into "Rear Axle" table in "1" column in vehicle page
    And   I enter "0" value into "Rear Axle" table in "2" column in vehicle page
    And   I enter "4" value into "Rear Axle" table in "3" column in vehicle page
    And   I enter "0" value into "Rear Axle" table in "4" column in vehicle page
    And   I enter "5" value into "Rear Axle" table in "5" column in vehicle page
    And   I click on "Create new over sized row" in vehicle page
    And   I enter "5" value into "Oversized ranges" table in "1" column in vehicle page
    And   I enter "0" value into "Oversized ranges" table in "2" column in vehicle page
    And   I enter "4" value into "Oversized ranges" table in "3" column in vehicle page
    And   I enter "0" value into "Oversized ranges" table in "4" column in vehicle page
    And   I enter "7" value into "Oversized ranges" table in "5" column in vehicle page
    And   I click on element with title attribute value "Store the offsets above (Enter)"
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    Then  I check for element with text "Offset information updated successfully."
    When  I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Maintain rear offsets"
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on "Delete axle" in vehicle page
    And   I click on "Delete over sized tire" in vehicle page
    And   I click on element with title attribute value "Store the offsets above (Enter)"
    And   I switch back to the main window in SAP
    And   I wait for "2" seconds
    And   I click on element with title attribute value "Front or Front/Both"
    And   I click on "Both"
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    Then  I check for element with text "Offset information updated successfully."
    When  I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Switch between Change / Display mode (F7)"

    Examples:
      | TCode    | Year | Make  | Model |
      | ZVEHICLE | 2012 | Acura | MDX   |

  @9575
  Scenario Outline: SAP-ECC_VEHICLE_VEHICLE_Chassis and Misc Functions (ALM#9575)
    When  I enter t-code "<TCode>" in the command field
    And   I click on "Vehicle drop down button" in vehicle page
    And   I click on "Car"
    And   I click on "Year drop down button" in vehicle page
    And   I click on "<Year>"
    And   I click on "Make drop down button" in vehicle page
    And   I click on "<Make>"
    And   I click on "Model drop down button" in vehicle page
    And   I click on "<Model>"
    And   I click on element with title attribute value "Change an existing vehicle"
    And   I click on element with title attribute value "Create a new chassis"
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on element with title attribute value "Yes"
    And   I switch back to the main window in SAP
    And   I save the "First" chassis number from "Chassis Number" field
    And   I click on element with title attribute value "Front or Front/Both"
    And   I click on "<Axle Type>"
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "saved message" matches "Vehicle Drive Options updated Successfully." displayed in vehicle page
    When  I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Delete the current chassis"
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on element with title attribute value "Yes"
    And   I switch back to the main window in SAP
    Then  I verify "Save message" matches "has been deleted." displayed in vehicle page
    When  I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "saved message" matches "Vehicle Drive Options updated Successfully." displayed in vehicle page
    When  I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Create a new chassis"
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on element with title attribute value "Yes"
    And   I switch back to the main window in SAP
    And   I save the "Second" chassis number from "Chassis Number" field
    Then  I verify if "First" saved number and "Second" saved number are in right sequence

    Examples:
      | TCode    | Year | Make   | Model | Axle Type |
      | ZVEHICLE | 2014 | Toyota | Camry | Both      |

  @4801
  Scenario Outline: SAP-ECC_VEHICLE_VEHICLE_VER_CHASSIS_LOCAL_NOTES_ADD_ADD_EDIT (ALM#4801)
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I save the "First" notes number from "<Notes Type number>" field
    And  I click on "<Notes type link>" in vehicle page
    And  I switch to the iFrame at Index "0" in SAP
    And  I wait for "2" seconds
    And  I select the following "10" index in "Global notes selection"
    And  I click on "Add Selected Notes"
    And  I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I save the "Second" notes number from "<Notes Type number>" field
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "saved message" matches "Global note assigned to the Vehicle Chassis Successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    Then I verify if "First" saved number and "Second" saved number are in right sequence
    When I click on "<Notes Type number>" in vehicle page
    And  I switch to the iFrame at Index "0" in SAP
    And  I select the following "0" index in "Global notes selection"
    And  I click on element with title attribute value "Delete the selected rows"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on element with title attribute value "Yes"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Apply the changes of the Notes ordering"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Switch between Change / Display mode (F7)"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Yes"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "saved message" matches "Global note assigned to the Vehicle Chassis Successfully." displayed in vehicle page

    Examples:
      | TCode    | Year | Make  | Model | Notes Type number    | Notes type link    |
      | ZVEHICLE | 2012 | Honda | CR-V  | Special notes number | Special Notes link |

  @4803
  @4818
  Scenario Outline: SAP-ECC_VEHICLE_VEHICLE_VER_VEHICLE_TRIM - Adding and Managing Trims (ALM#4803 and ALM#4818)
    """Web GUI stops responding after creation of assembly in RTK """
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on element with title attribute value "Build a new Trim with desc"
    Then I verify the success status in SAP
    When I "double click" on row "1" into "Chassis Configurations" table in "1" column in vehicle page
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "input box1" in vehicle page
    And  I hit F4 action
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Menu Button" in vehicle page
    And  I scroll to following "Cab Style" element and click
    And  I double click on "<Cab Style value>"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "input box1 text" matches "CABYSTYLE" displayed in vehicle page
    When I click on "input box2" in vehicle page
    And  I hit F4 action
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I scroll to following "TRIM Alpha Numeric" element and click
    And  I double click on "<TRIM Alpha Numeric value>"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "input box2 text" matches "TRIM" displayed in vehicle page
    When I click on "input box3" in vehicle page
    And  I hit F4 action
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I scroll to following "Trim Letters" element and click
    And  I double click on "<Trim Letters value>"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "input box3 text" matches "TRIMLTTRS" displayed in vehicle page
    When I click on element with title attribute value "Apply the Trim/Submodel (Enter)"
    And  I switch back to the main window in SAP
    And  I click on "Trim Dropdown" in vehicle page
    And  I click on "01"
    And  I click on "Assembly row 2" in vehicle page
    And  I click on "Delete an Assembly" in vehicle page
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Yes"
    And  I switch back to the main window in SAP
    And  I select the following "1" index in "Chassis Config Table"
    And  I click on "Delete a Trim" in vehicle page
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I enter t-code "<ExitTCode>" in the command field
    And  I wait for "10" seconds

    Examples:
      | TCode    | Cab Style value | TRIM Alpha Numeric value | Trim Letters value | Year | Make | Model | ExitTCode |
      | ZVEHICLE | Access Cab      | 1500                     | AMG                | 2012 | Audi | R8    | /nex      |

  @4803
  @4818
  Scenario Outline: SAP-ECC_VEHICLE_VEHICLE_VER_VEHICLE_TRIM - Adding and Managing Trims (ALM#4803 and ALM#4818)
  """Web GUI stops responding after creation of assembly in RTK """
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on element with title attribute value "Create an assembly "
    And  I click on partial text "Front Axle"
    Then I verify the success status in SAP
    When I save
    And  I wait for "2" seconds
    And  I switch to the iFrame at Index "0" in SAP
    When I click on "Close button" in vehicle page
    And  I wait for "5" seconds
    And  I switch back to the main window in SAP
    When I click on "Width Of Front Wheel" in vehicle page
    And  I click on "input button" in vehicle page
    And  I wait for "2" seconds
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "125"
    And  I switch back to the main window in SAP
    And  I click on "Aspect Ratio Of Front Wheel" in vehicle page
    And  I click on "input button" in vehicle page
    And  I wait for "2" seconds
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "12.50"
    And  I switch back to the main window in SAP
    And  I click on "Tire Diameter Of Front Wheel" in vehicle page
    And  I wait for "2" seconds
    And  I click on "input button" in vehicle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "12"
    And  I switch back to the main window in SAP
    When I click on "Width Of Rear Wheel" in vehicle page
    And  I wait for "2" seconds
    And  I click on "input button" in vehicle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "125"
    And  I switch back to the main window in SAP
    And  I click on "Aspect Ratio Of Rear Wheel" in vehicle page
    And  I wait for "2" seconds
    And  I click on "input button" in vehicle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "12.50"
    And  I switch back to the main window in SAP
    And  I click on "Tire Diameter Of Rear Wheel" in vehicle page
    And  I click on "input button" in vehicle page
    And  I wait for "2" seconds
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "12"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    Then I verify "saved message" matches "Assemblies Update Successful." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Switch between Change / Display mode (F7)"

    Examples:
      | TCode    | Year | Make | Model |
      | ZVEHICLE | 2012 | Audi | R8    |

  @7079
  Scenario Outline: SAP-ECC_VEHICLE_VEHICLE_VER_OFFSET_CREATE (ALM#7079)
    When  I enter t-code "<TCode>" in the command field
    And   I click on "Vehicle drop down button" in vehicle page
    And   I click on "Car"
    And   I click on "Year drop down button" in vehicle page
    And   I click on "<Year>"
    And   I click on "Make drop down button" in vehicle page
    And   I click on "<Make>"
    And   I click on "Model drop down button" in vehicle page
    And   I click on "<Model>"
    And   I click on element with title attribute value "Change an existing vehicle"
    And   I click on element with title attribute value "Maintain F / B offsets"
    And   I switch to the iFrame at Index "0" in SAP
    And   I enter "8" into the field with label "For diameters greater than:"
    And   I click on "Create new rear axle row" in vehicle page
    And   I enter "5" value into "Front / Both Axle" table in "1" column in vehicle page
    And   I enter "0" value into "Front / Both Axle" table in "2" column in vehicle page
    And   I enter "4" value into "Front / Both Axle" table in "3" column in vehicle page
    And   I enter "0" value into "Front / Both Axle" table in "4" column in vehicle page
    And   I enter "5" value into "Front / Both Axle" table in "5" column in vehicle page
    And   I click on "Create new over sized row" in vehicle page
    And   I enter "5" value into "Oversized ranges" table in "1" column in vehicle page
    And   I enter "0" value into "Oversized ranges" table in "2" column in vehicle page
    And   I enter "4" value into "Oversized ranges" table in "3" column in vehicle page
    And   I enter "0" value into "Oversized ranges" table in "4" column in vehicle page
    And   I enter "7" value into "Oversized ranges" table in "5" column in vehicle page
    And   I click on element with title attribute value "Store the offsets above (Enter)"
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "saved message" matches "Offset information updated successfully." displayed in vehicle page
    When  I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Maintain F / B offsets"
    And   I switch to the iFrame at Index "0" in SAP
    And   I click on "Delete axle" in vehicle page
    And   I click on "Delete over sized tire" in vehicle page
    And   I click on element with title attribute value "Store the offsets above (Enter)"
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "saved message" matches "Offset information updated successfully." displayed in vehicle page
    When  I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Switch between Change / Display mode (F7)"

    Examples:
      | TCode    | Year | Make  | Model |
      | ZVEHICLE | 2012 | Acura | MDX   |

  @15566
  Scenario Outline: SAP_ECC_Vehicle Segment ID (ALM#15566)
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on element with title attribute value "Show Trim IDs for each Trim"
    And  I switch to the iFrame at Index "0" in SAP
    And  I wait for "2" seconds
    And  I "double click" on row "0" into "List of Trim IDs for the Current Vehicle" table in "3" column in vehicle page
    And  I wait for "2" seconds
    And  I hit F4 action
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on "4"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Continue (Enter)"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Show Trim IDs for each Trim"
    And  I switch to the iFrame at Index "0" in SAP
    And  I save the "VSegCatId" value from the page
    And  I click on element with title attribute value "Continue (Enter)"
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode1>" in the command field
    And  I wait for "5" seconds
    And  I enter "<Vehicle Class>" into the field with label "Vehicle Class"
    And  I hit tab
    And  I enter "<Make>" into the field with label "Vehicle Make"
    And  I hit tab
    And  I enter "<Model>" into the field with label "Vehicle Model"
    And  I hit tab
    And  I enter "<Year>" into the field with label "Year"
    And  I execute
    And  I save the "Vehicle ID From Vehlist" value from the page
    And  I enter t-code "<TCode2>" in the command field
    And  I enter "<Table Name>" into the field with label "Table Name"
    And  I hit return
    Then I verify the success status in SAP
    When I enter saved "Vehicle ID From Vehlist" element value into following element "Vehicle ID" in vehicle page
    And  I execute
    And  I save the "Veh Seg Cat ID" value from the page
    Then I check if the "VSegCatId" numeric value and "Veh Seg Cat ID" numeric value is the same
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on element with title attribute value "Show Trim IDs for each Trim"
    And  I switch to the iFrame at Index "0" in SAP
    And  I "double click" on row "0" into "List of Trim IDs for the Current Vehicle" table in "3" column in vehicle page
    And  I wait for "2" seconds
    And  I hit F4 action
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on "4"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Continue (Enter)"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Back"

    Examples:
      | TCode       | TCode1      | Vehicle Class | TCode2  | Table Name       | Year | Make  | Model |
      | /n ZVEHICLE | /n ZVEHLIST | PAS           | /n se16 | zveh_T2V_Seg_cat | 2012 | Acura | MDX   |

  @6338
  Scenario Outline: SAP-ECC_VEHICLE_VEHICLE_VER_GLOBALNOTES_DELETE (ALM#6338)
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on "Special Notes link" in vehicle page
    And  I switch to the iFrame at Index "0" in SAP
    And  I wait for "2" seconds
    And  I select the following "0" index in "Global notes selection"
    And  I click on "Add Selected Notes"
    And  I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I save the "First" notes number from "Special notes number" field
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "saved message" matches "Global note assigned to the Vehicle Chassis Successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Switch between Change / Display mode (F7)"
    And  I click on element with title attribute value "Back"
    And  I enter t-code "<TCode2>" in the command field
    And  I click on "Special notes"
    And  I execute
    And  I wait for "5" seconds
    And  I click on element with title attribute value "Name"
    And  I click on element with title attribute value "Sort in Ascending Order"
    And  I wait for "5" seconds
    And  I select the following "0" index in "Global notes selection"
    And  I click on element with title attribute value "Delete Row"
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Yes"
    And  I switch back to the main window
    Then I verify "Save message" matches "Texts have been deleted successfuly" displayed in vehicle page
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I save the "Second" notes number from "Special notes number" field
    Then I verify if "Second" saved number and "First" saved number are in right sequence
    When I enter t-code "<TCode2>" in the command field
    And  I click on "Special notes"
    And  I execute
    And  I click on element with title attribute value "Create Text"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "<Input Text>" into the field with label "Text Name"
    And  I enter "<Input Text>" into the field with label "Short Title"
    And  I hit return
    And  I switch back to the main window in SAP
    And  I select "Input text box" and enter "<Input Text>" in vehicle page
    And  I click on element with title attribute value "Save"
    Then I verify "Save message" matches "saved successfully" displayed in vehicle page
    When I click on element with title attribute value "Back"
    Then I validate "Last Row" entered matches original "SPEC_AAAAA" in vehicle page

    Examples:
      | TCode       | TCode2        | Year | Make  | Model | Input Text |
      | /n ZVEHICLE | /n ZVEHGBNOTE | 2012 | Acura | MDX   | AAAAA      |

  @14250
  Scenario Outline: SAP_ECC_Vehicle_Global Notes_Edit (ALM#14250)
    When I enter t-code "<TCode1>" in the command field
    And  I click on "Special notes"
    And  I execute
    And  I click on element with title attribute value "Create Text"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "<Input Text>" into the field with label "Text Name"
    And  I enter "<Input Text>" into the field with label "Short Title"
    And  I hit return
    And  I switch back to the main window in SAP
    And  I select "Input text box" and enter "<Input Text>" in vehicle page
    And  I click on element with title attribute value "Save"
    Then I verify "Save message" matches "saved successfully" displayed in vehicle page
    When I click on element with title attribute value "Back"
    Then I validate "Last Row" entered matches original "SPEC_A01" in vehicle page
    When I enter t-code "<TCode2>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I save the "First" notes number from "<Notes Type number>" field
    And  I click on "Special Notes link" in vehicle page
    And  I switch to the iFrame at Index "0" in SAP
    And  I wait for "2" seconds
    And  I select the following "0" index in "Global notes selection"
    And  I click on "Add Selected Notes"
    And  I click on element with title attribute value "Close the window (Enter)"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "saved message" matches "Global note assigned to the Vehicle Chassis Successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I save the "Second" notes number from "<Notes Type number>" field
    And  I click on "<Notes Type number>" in vehicle page
    And  I switch to the iFrame at Index "0" in SAP
    And  I select the following "0" index in "Global notes selection"
    And  I click on element with title attribute value "Delete the selected rows"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on element with title attribute value "Yes"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Apply the changes of the Notes ordering"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Switch between Change / Display mode (F7)"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Yes"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "saved message" matches "Global note assigned to the Vehicle Chassis Successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Back"
    And  I enter t-code "<TCode1>" in the command field
    And  I click on "Special notes"
    And  I execute
    And  I click on element with title attribute value "Name"
    And  I click on element with title attribute value "Sort in Ascending Order"
    And  I select the following "0" index in "Global notes selection"
    And  I click on element with title attribute value "Delete Row"
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Yes"
    And  I switch back to the main window
    Then I verify "Save message" matches "Texts have been deleted successfuly" displayed in vehicle page

    Examples:
      | TCode1       | Input Text | TCode2      | Year | Make  | Model | Notes Type number    |
      | /nZVEHGBNOTE | A01        | /n ZVEHICLE | 2014 | Acura | MDX   | Special notes number |

  @4794
  Scenario Outline: SAP-ECC_VEHICLE_VEHICLE_VER_VEHICLE_TPMS - Adding a TPMS Article to a Vehicle (ALM#4794)
    When  I enter t-code "<TCode>" in the command field
    And   I click on "Vehicle drop down button" in vehicle page
    And   I click on "Car"
    And   I click on "Year drop down button" in vehicle page
    And   I click on "<Year>"
    And   I click on "Make drop down button" in vehicle page
    And   I click on "<Make>"
    And   I click on "Model drop down button" in vehicle page
    And   I click on "<Model>"
    And   I click on element with title attribute value "Change an existing vehicle"
    And   I scroll to following "Create new TPMS record" element and click
    And   I select the "TPMS last row" row in "TPMS scroll bar" table
    And   I hit F4 action
    And   I switch to the iFrame at Index "0" in SAP
    And   I wait for "5" seconds
    And   I click on "TPMS Entry" in vehicle page
    And   I click on element with title attribute value "Copy"
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "saved message" matches "TPMS details updated successfully." displayed in vehicle page
    When  I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I select the "TPMS row delete" row in "TPMS scroll bar" table
    And   I click on "TPMS delete button" in vehicle page
    And   I click on element with title attribute value "Save"
    And   I switch to the iFrame at Index "0" in SAP
    Then  I verify "saved message" matches "TPMS details updated successfully." displayed in vehicle page
    When  I click on "Close button" in vehicle page
    And   I switch back to the main window in SAP
    And   I click on element with title attribute value "Switch between Change / Display mode (F7)"

    Examples:
      | TCode    | Year | Make | Model   |
      | ZVEHICLE | 2014 | Ford | Mustang |

  @15977
  Scenario Outline: SAP ECC_Vehicle_ZVEHSEGCAT Report (ALM#15977)
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on element with title attribute value "Show Trim IDs for each Trim "
    And  I switch to the iFrame at Index "0" in SAP
    And  I save the "Trim Sub Model" value in vehicle page
    And  I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I click on element with title attribute value "Search"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "17107" from preferred data source into the field with label "Search Term:"
    And  I click on element with title attribute value "OK (Enter)"
    And  I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I save the "Trim Sub Model Description" value in vehicle page
    Then I compare if the "Trim Sub Model" value and "Trim Sub Model Description" value are the same
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make2>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model2>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on element with title attribute value "Show Trim IDs for each Trim "
    And  I switch to the iFrame at Index "0" in SAP
    And  I save the "VSegCatId" value in vehicle page
    And  I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I click on element with title attribute value "Search"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on partial text "Find only"
    And  I enter "6406" from preferred data source into the field with label "Search Term:"
    And  I click on element with title attribute value "OK (Enter)"
    And  I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I save the "Veh Seg Cat Id Report" value in vehicle page
    Then I check if the "VSegCatId" numeric value and "Veh Seg Cat Id Report" numeric value is the same

    Examples:
      | TCode       | TCode2        | Year | Make  | Model | Make2     | Model2   |
      | /n ZVEHICLE | /n ZVEHSEGCAT | 2014 | Acura | MDX   | Chevrolet | Corvette |

  @14816
  Scenario Outline: SAP_ECC_Vehicle Web Infosheets (ALM#14816)
    When I set baseUrl to "SAP web content"
    And  I navigate to the stored Base URL
    And  I select "Input info sheet search" and enter "<Search data>" in vehicle page
    And  I click on "Info sheet search result" in vehicle page
    And  I click on element with title attribute value "Send comments as email"
    And  I switch to the pop up window
    And  I enter "<Employee data>" from preferred data source into the field with label "Employee name"
    And  I enter "<Store data>" from preferred data source into the field with label "Store id"
    And  I click on "Send mail"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Extended Infosheet Search"
    And  I click on "Keyword Search"
    And  I enter "<Search data>" from preferred data source into the field with label "Search String"
    And  I execute
    Then I validate "Keyword Search result" entered matches original "<Search data>" in vehicle page

    Examples:
      | TCode    | Search data | Employee data | Store data |
      | /N ZINFO | MICHELIN    | Employee      | 1234       |

  @4793
  Scenario Outline: Vehicle Master Maintain - ZVEHICLE (ALM#4793)
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on element with title attribute value "Create an assembly"
    And  I click on "Both Axles (1 row)"
    Then I verify the success status in SAP
    When I refresh the browser
    And  I switch back to the main window in SAP
    And  I click on "Width Of Front Wheel" in vehicle page
    And  I click on "input button" in vehicle page
    And  I wait for "2" seconds
    And  I switch to the iFrame at Index "0" in SAP
    And  I send "<Cross Section data>" text into input element "Search input box" without clear
    And  I hit return
    And  I double click on "<Cross Section data>"
    And  I switch back to the main window in SAP
    And  I click on "Aspect Ratio Of Front Wheel" in vehicle page
    And  I click on "input button" in vehicle page
    And  I wait for "2" seconds
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I send "<Aspect ratio data>" text into input element "Search input box" without clear
    And  I hit return
    And  I double click on "<Aspect ratio data>"
    And  I switch back to the main window in SAP
    And  I click on "Tire Diameter Of Front Wheel" in vehicle page
    And  I wait for "2" seconds
    And  I click on "input button" in vehicle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I send "<Wheel Rim diameter data>" text into input element "Search input box" without clear
    And  I hit return
    And  I double click on "<Wheel Rim diameter data>"
    And  I switch back to the main window in SAP
    And  I select the "Article list" row in "Assembly horizontal scroll bar" table
    And  I double click on "Added last article" in vehicle page
    And  I switch to the first iFrame in SAP
    And  I click on "Article list selection" in vehicle page
    And  I click on element with title attribute value "Continue (Enter)"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    Then I verify "saved message" matches "Assemblies Update Successful." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I select on row "2" into "OEM Tire / Wheel (Assemblies)" table in vehicle page
    And  I click on "Delete an Assembly" in vehicle page
    And  I switch to the first iFrame in SAP
    And  I click on element with title attribute value "Yes"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    Then I verify "saved message" matches "Assemblies Update Successful." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Switch between Change / Display mode (F7)"

    Examples:
      | TCode    | Year | Make | Model | Cross Section data | Aspect ratio data | Wheel Rim diameter data |
      | ZVEHICLE | 2012 | Audi | A4    | 285                | 40                | 19                      |

  @4795
  Scenario Outline: ZVEHICLE - ACC - Adding an Accessory Article to a Vehicle (ALM#4795)
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on partial text "Accessories"
    And  I click on element with title attribute value "Create a TPMS record"
    And  I enter "<Chassis value>" value into "Chassis details" table in "1" column in vehicle page
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "saved message" matches "Accessories updated successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I select on row "1" into "Chassis details" table in vehicle page
    And  I click on "Tpms Delete Row" in vehicle page
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    Then I verify "saved message" matches "Accessories updated successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Switch between Change / Display mode (F7)"

    Examples:
      | TCode    | Year | Make      | Model    | Chassis value |
      | ZVEHICLE | 2014 | Chevrolet | Corvette | 10007         |

  @14028
  Scenario Outline: SAP_ECC_Vehicle Global Notes (ALM#14028)
    When I enter t-code "<TCode>" in the command field
    And  I enter "<Table Name>" from preferred data source into the field with label "Table Name"
    And  I hit return
    Then I verify if correct URL for the current environment is up or not
    When I save the response body node "list" count from request GET call
    And  I enter "<Text ID>" from preferred data source into the field with label "Text ID"
    And  I execute
    And  I save the "Data Browser page" number from field with key "list count"
    Then I check if the "list count" value and "list" value is the same
    When I enter t-code "<TCode>" in the command field
    And  I enter "ZVEH_CHASS_NOTES" from preferred data source into the field with label "Table Name"
    And  I hit return
    And  I enter "<Vehicle ID>" from preferred data source into the field with label "Vehicle ID"
    And  I execute
    And  I save the "Data Browser page" number from field with key "Fixed Columns Nr"
    And  I enter t-code "<TCode4>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Display"
    And  I add different attributes of two number of element "TPMS Count number" and element "Chasis count number"
    Then I check if the "Fixed Columns Nr" value and "Sum of Notes" value is the same
    When I click on element with title attribute value "Back"
    And  I enter t-code "<TCode3>" in the command field
    And  I execute
    And  I click on element with title attribute value "Create Text"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter "SAPVEHICLE" from preferred data source into the field with label "Text Name"
    And  I enter "test" from preferred data source into the field with label "Short Title"
    And  I hit return
    And  I switch back to the main window in SAP
    And  I select "Input text box" and enter "test1" in vehicle page
    And  I click on element with title attribute value "Save"
    Then I verify "Save message" matches "saved successfully" displayed in vehicle page
    When I click on element with title attribute value "Back"
    Then I validate "Last Row" entered matches original "SPEC_SAPVEHICLE" in vehicle page
    When I click on element with title attribute value "Refresh"
    Then I verify if correct URL for the current environment is up or not
    When I save the response body node "list2" count from request GET call
    Then I verify if "list" saved number and "list2" saved number are in right sequence
    When I select the "newly added row to delete" row in "Global scroll bar" table
    And  I click on element with title attribute value "Delete Row"
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Yes"

    Examples:
      | TCode   | TCode3        | Year | Make  | Model | TCode4      | Vehicle ID | Table Name | Text ID |
      | /n SE16 | /n zvehgbnote | 2014 | Acura | MDX   | /n ZVEHICLE | 2958       | STXH       | ZGBL    |

  @4819
  Scenario Outline: ZVEHICLE - DATAVAL - Delete OEM Assembly (ALM#4819)
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on "Assembly row 2" in vehicle page
    And  I click on "Delete an Assembly" in vehicle page
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Yes"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Close button" in vehicle page
    And  I wait for "5" seconds
    And  I switch back to the main window in SAP
    And  I enter t-code "<ExitTCode>" in the command field
    And  I wait for "10" seconds

    Examples:
      | TCode    | Year | Make  | Model |ExitTCode|
      | ZVEHICLE | 2014 | Honda | Pilot |  /nex   |

  @4819
  Scenario Outline: ZVEHICLE - DATAVAL - Delete OEM Assembly (ALM#4819)
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on element with title attribute value "Create an assembly "
    And  I click on partial text "Front Axle"
    Then I verify the success status in SAP
    When I save
    And  I wait for "2" seconds
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Close button" in vehicle page
    And  I wait for "5" seconds
    And  I switch back to the main window in SAP
    And  I click on "Width Of Front Wheel" in vehicle page
    And  I click on "input button" in vehicle page
    And  I wait for "2" seconds
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "125"
    And  I switch back to the main window in SAP
    And  I click on "Aspect Ratio Of Front Wheel" in vehicle page
    And  I click on "input button" in vehicle page
    And  I wait for "2" seconds
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "12.50"
    And  I switch back to the main window in SAP
    And  I click on "Tire Diameter Of Front Wheel" in vehicle page
    And  I wait for "2" seconds
    And  I click on "input button" in vehicle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "12"
    And  I switch back to the main window in SAP
    And  I click on "Width Of Rear Wheel" in vehicle page
    And  I wait for "2" seconds
    And  I click on "input button" in vehicle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "125"
    And  I switch back to the main window in SAP
    And  I click on "Aspect Ratio Of Rear Wheel" in vehicle page
    And  I wait for "2" seconds
    And  I click on "input button" in vehicle page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "12.50"
    And  I switch back to the main window in SAP
    And  I click on "Tire Diameter Of Rear Wheel" in vehicle page
    And  I click on "input button" in vehicle page
    And  I wait for "2" seconds
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "12"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    Then I verify "saved message" matches "Assemblies Update Successful." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Back"

    Examples:
      | TCode    | Year | Make  | Model |
      | ZVEHICLE | 2014 | Honda | Pilot |

  @4819
  Scenario Outline: ZVEHICLE - DATAVAL - Delete OEM Assembly (ALM#4819)
    When I enter t-code "<TCode1>" in the command field
    And  I wait for "2" seconds
    And  I enter "<Vehicle Class>" from preferred data source into the field with label "Vehicle Class"
    And  I hit tab
    And  I enter "<Make>" from preferred data source into the field with label "Vehicle Make"
    And  I hit tab
    And  I enter "<Model>" from preferred data source into the field with label "Vehicle Model"
    And  I hit tab
    And  I enter "<Year>" from preferred data source into the field with label "Year"
    And  I execute
    And  I save the "Vehicle ID From Vehlist" value in vehicle page
    And  I enter t-code "<TCode2>" in the command field
    And  I enter "ZR_VEH_IDOC_OUTBOUND_MAIN" from preferred data source into the field with label "Program"
    And  I execute
    And  I enter saved "Vehicle ID From Vehlist" element value into following element "Vehicle ID" in vehicle page
    And  I click on "Initialization"
    And  I execute
    Then I save the "Idoc For Vehicle Outbound" number in idoc page
    When I enter t-code "<TCode3>" in the command field
    And  I enter Idoc Number into "Idoc List" in idoc page
    And  I execute
    And  I select the "Expand Node" button on the Idoc page
    And  I click "Scroll In Idoc Page" till "First Mart Row" is visible in idoc page
    And  I click on the data record which has the recently deleted assembly
    Then I validate "LVORM" entered matches original "X" in Idoc page

    Examples:

      | TCode1      | TCode2  | TCode3 | Year | Make  | Model | Vehicle Class |
      | /n ZVEHLIST | /n SE38 | /nWE02 | 2014 | Honda | Pilot | PAS           |

  @15570
  Scenario Outline: SAP_ECC_Vehicle Completion Report (ALM#15570)
    When I enter t-code "<TCode>" in the command field
    And  I click on "Vehicle drop down button" in vehicle page
    And  I click on "Car"
    And  I click on "Year drop down button" in vehicle page
    And  I click on "<Year>"
    And  I click on "Make drop down button" in vehicle page
    And  I click on "<Make>"
    And  I click on "Model drop down button" in vehicle page
    And  I click on "<Model>"
    And  I click on element with title attribute value "Change an existing vehicle"
    And  I click on element with title attribute value "Vehicle completion view"
    And  I switch to the iFrame at Index "0" in SAP
    Then I validate the element "OEM status" status with "<Yellow>" image
    And  I validate the element "Chassis status" status with "<Yellow>" image
    And  I validate the element "Chassis config status" status with "<Green>" image
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I select the "Article list" row in "Assembly horizontal scroll bar" table
    And  I double click on "Added last article" in vehicle page
    And  I switch to the first iFrame in SAP
    And  I click on "Article list selection" in vehicle page
    And  I click on element with title attribute value "Continue (Enter)"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    Then I verify "saved message" matches "OEM Article Assignment updated successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Vehicle completion view"
    And  I switch to the iFrame at Index "0" in SAP
    Then I validate the element "OEM status" status with "<Green>" image
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I wait for "2" seconds
    And  I enter "26" from preferred data source into the field with label "Threaded Length"
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    Then I verify "saved message" matches "Hub Interface Updated successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Vehicle completion view"
    And  I switch to the iFrame at Index "0" in SAP
    Then I validate the element "Chassis status" status with "<Green>" image
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I select the "Article list" row in "Assembly horizontal scroll bar" table
    And  I double click on "Added last article" in vehicle page
    And  I switch to the first iFrame in SAP
    And  I click on "Article list selection" in vehicle page
    And  I click on element with title attribute value "Continue (Enter)"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    Then I verify "saved message" matches "OEM Article Assignment updated successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Vehicle completion view"
    And  I switch to the iFrame at Index "0" in SAP
    Then I validate the element "OEM status" status with "<Yellow>" image
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I enter " " from preferred data source into the field with label "Threaded Length"
    And  I click on element with title attribute value "Save"
    And  I switch to the first iFrame in SAP
    Then I verify "saved message" matches "Hub Interface Updated successfully." displayed in vehicle page
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Vehicle completion view"
    And  I switch to the iFrame at Index "0" in SAP
    Then I validate the element "Chassis status" status with "<Yellow>" image
    When I click on "Close button" in vehicle page
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Switch between Change / Display mode (F7)"

    Examples:
      | TCode    | Year | Make  | Model | Yellow        | Green         |
      | ZVEHICLE | 2014 | Acura | MDX   | /s_s_ledy.png | /s_s_ledg.png |