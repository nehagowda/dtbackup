@sap
@invoiceCreation
Feature: Invoice Creation

  @13734
  @livScenario1
  @livScenarios
  @AllItemsNonTaxableNoTaxOnInvoice
  Scenario Outline: SAPECC_APIA_IDOC_TAX_LIV_PO_01_NonTaxable_All_NoTaxOnInvoice (ALM#13734)
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I enter "<PurchaseGroup>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value at row "1" for "Article Number" to "<ArticleNumber>" from preferred data source
    And  I enter the purchase order value at row "2" for "Article Number" to "<ArticleNumber2>" from preferred data source
    And  I enter the purchase order value at row "1" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "2" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "1" for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value at row "2" for "Site" to "<Site>" from preferred data source
    And  I hit return
    And  I click on "Invoice"
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Next Item Button In Migo" in po page
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the success status in SAP
    When I enter t-code "<TCode2>" in the command field
    And  I enter a test idoc to copy from
    And  I select the "Create button in we19" button on the Idoc page
    And  I update idoc values for "<IDocType>" and "<Vendor>"
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode3>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    When I click on "Record Id" in IDOC page
    And  I print "Tax amount" element value in Idoc page
    Then I validate "Tax code" entered matches original "<TaxCode>" in Idoc page
    And  I verify if tax field at row number "2", column number "9" contains "<TaxCode>"
    And  I verify if tax field at row number "2", column number "9" contains "<TaxCode>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on "Invoice Number" in IDOC page
    And  I wait for element with text of "Reference" to be visible
    And  I take page screenshot

    Examples:
      | TCode    | TCode1  | Vendor | PurchaseGroup | ArticleNumber | ArticleNumber2 | Quantity | Site | TaxCode | IDocType   | TCode2  | TCode3       | TaxCodeCount | POType         | Folder Name      | File Name | Sheet name | row number |
      | /n ME21N | /n MIGO | 10226  | 01            | 43195         | 43196          | 2        | 1434 | I0      | 2 Articles | /n WE19 | /n /dol/ap2n | 3            | Store Merch PO | Invoice Creation | liv.xlsx  | Scenario 1 | 2          |


  @13738
  @livScenario3
  @livScenarios
  @MixedItemsNoTaxInovice
  Scenario Outline: SAPECC_APIA_IDOC_TAX_LIV_PO_03_SomeItemsAreTaxable_SomeNot_NoTaxOnInvoice (ALM#13738)
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I enter "<PurchaseGroup>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value at row "1" for "Article Number" to "<ArticleNumber>" from preferred data source
    And  I enter the purchase order value at row "1" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "1" for "Site" to "<Site>" from preferred data source
    And  I enter "<short text>" information on "Short Text input in row 2" in IDOC page
    And  I enter "<Order per unit>" information on "Order unit input in row 2" in IDOC page
    And  I enter "<Net price>" information on "Net price input in row 2" in IDOC page
    And  I enter "<Order per unit>" information on "Order price unit input in row 2" in IDOC page
    And  I enter "<Merchandise category 2>" information on "Merch Category input in row 2" in IDOC page
    And  I set the input for "Quantity" at row number "2" to "<Quantity>"
    And  I set the input for "Site" at row number "2" to "<Site>"
    And  I hit return
    And  I click on "Invoice"
    Then I verify the "Tax Code" field has "<TaxCode>" value in ME21N page
    When I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Next Item Button In Migo" in po page
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the alert is displayed for the article document being posted
    When I enter t-code "<TCode2>" in the command field
    And  I enter a test idoc to copy from
    And  I select the "Create button in we19" button on the Idoc page
    And  I update idoc values for "<IDocType>" and "<Vendor>"
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode3>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    And  I verify "Status 100" is displayed in iDoc page
    Then I click on "Record Id" in IDOC page
    When I print "Tax amount" element value in Idoc page
    Then I validate "Tax amount" entered matches original "0.00" in Idoc page
    And  I validate "Tax code" entered matches original "**" in Idoc page
    And  I verify if tax field at row number "1", column number "9" contains "<TaxCode>"
    And  I verify if tax field at row number "2", column number "9" contains "<TaxCode1>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on "Invoice Number" in IDOC page
    And  I wait for element with text of "Reference" to be visible
    And  I take page screenshot

    Examples:
      | TCode    | TCode1  | TCode2  | TCode3       | Vendor | PurchaseGroup | ArticleNumber | Quantity | Site | TaxCode | TaxCode1 | TaxCodeCount | short text      | Order per unit | Net price | Merchandise category 2 | TaxCodeCount | IDocType   | POType         | Folder Name      | File Name | Sheet name | row number |
      | /n ME21N | /n MIGO | /n WE19 | /n /dol/ap2n | 10226  | 01            | 43195         | 2        | 1434 | I0      | I1       | 4            | Taxable article | EA             | 100.00    | PROD SUPPLY MISC       | 4            | 2 Articles | Store Merch PO | Invoice Creation | liv.xlsx  | Scenario 3 | 2          |

  @13737
  @livScenario2
  @livScenarios
  @AllItemsNonTaxableTaxOnInvoice
  Scenario Outline: SAPECC_APIA_IDOC_TAX_LIV_PO_01_NonTaxable_All_TaxOnInvoice (ALM#13737)
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I enter "<PurchaseGroup>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value at row "1" for "Article Number" to "<ArticleNumber>" from preferred data source
    And  I enter the purchase order value at row "2" for "Article Number" to "<ArticleNumber2>" from preferred data source
    And  I enter the purchase order value at row "1" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "2" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "1" for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value at row "2" for "Site" to "<Site>" from preferred data source
    And  I hit return
    And  I click on "Invoice"
    Then I verify the "Tax Code" field has "<TaxCode>" value in ME21N page
    When I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Next Item Button In Migo" in po page
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the success status in SAP
    When I enter t-code "<TCode2>" in the command field
    And  I enter a test idoc to copy from
    And  I select the "Create button in we19" button on the Idoc page
    And  I update idoc values for "<IDocType>" and "<Vendor>"
    And  I wait for "10" seconds
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode3>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    When I click on "Record Id" in IDOC page
    And  I print "Tax amount" element value in Idoc page
    Then I validate "Tax code" entered matches original "<TaxCode1>" in Idoc page
    And  I verify if tax field at row number "2", column number "9" contains "<TaxCode1>"
    And  I verify if tax field at row number "2", column number "9" contains "<TaxCode1>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on "Invoice Number" in IDOC page
    And  I wait for element with text of "Reference" to be visible
    And  I take page screenshot

    Examples:
      | TCode    | TCode1  | Vendor | PurchaseGroup | ArticleNumber | ArticleNumber2 | Quantity | Site | TaxCode | TaxCode1 | TaxCodeCount | IDocType   | TCode2  | TCode3       | POType         |Folder Name      | File Name | Sheet name | row number |
      | /n ME21N | /n MIGO | 10226  | 01            | 43195         | 43196          | 2        | 1434 | I0      | I1       | 5            | 2 Articles | /n WE19 | /n /dol/ap2n | Store Merch PO |Invoice Creation | liv.xlsx  | Scenario 2 | 2          |

  @13736
  @livScenario6
  @livScenarios
  @AllItemsTaxableNoTaxOnInvoice
  Scenario Outline: SAPECC_APIA_IDOC_TAX_LIV_PO_06_Taxable_All_NoTaxOnInvoice (ALM#13736)
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I enter "<PurchaseGroup>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value at row "1" for "Article Number" to "<ArticleNumber>" from preferred data source
    And  I enter the purchase order value at row "2" for "Article Number" to "<ArticleNumber2>" from preferred data source
    And  I enter the purchase order value at row "1" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "2" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "1" for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value at row "2" for "Site" to "<Site>" from preferred data source
    And  I hit return
    And  I click on "Invoice"
    Then I verify the "Tax Code" field has "<TaxCode>" value in ME21N page
    When I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Next Item Button In Migo" in po page
    And  I click on "Item OK"
    And  I click on "Post"
    And  I verify the success status in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I enter a test idoc to copy from
    And  I select the "Create button in we19" button on the Idoc page
    And  I update idoc values for "<IDocType>" and "<Vendor>"
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode3>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    And  I click on "Record Id" in IDOC page
    And  I print "Tax amount" element value in Idoc page
    Then I validate "Tax code" entered matches original "U1" in Idoc page
    And  I verify if tax field at row number "1", column number "9" contains "<TaxCode>"
    And  I verify if tax field at row number "2", column number "9" contains "<TaxCode>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on "Invoice Number" in IDOC page
    And  I wait for element with text of "Reference" to be visible
    And  I take page screenshot

    Examples:
      | TCode    | TCode1  | Vendor | PurchaseGroup | ArticleNumber | ArticleNumber2 | Quantity | Site | TaxCode | IDocType   | TCode2  | TCode3       | TaxCodeCountWithTax | POType         |Folder Name      | File Name | Sheet name | row number |
      | /n ME21N | /n MIGO | 13182  | 01            | 92912         | 91966          | 2        | 1434 | I1      | 2 Articles | /n WE19 | /n /dol/ap2n | 9                   | Store Merch PO |Invoice Creation | liv.xlsx  | Scenario 6 | 2          |

  @backgroundJob
  Scenario Outline: Background Job
  """NOTE: This is not a Test Scenario, Its a script to run a background job in SAP"""
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Program>" information on "Program" in IDOC page
    And  I execute
    And  I wait for "Background execution first page" field to be visible in idoc page
    And  I execute
    And  I wait for "Background execution result page" field to be visible in idoc page

    Examples:
      | TCode   | Program                    |
      | /n SE38 | /DOL/AP_AEI_BACKGR_POSTING |

  @apTax
  @apScenario1
  @apScenario2Part1
  @14078
  Scenario Outline: GL is taxable; vendor did not charge tax
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode1>" in the command field
    And  I enter a test idoc to copy from
    And  I execute
    And  I click on partial text "APEDI"
    And  I switch to the first iFrame in SAP
    And  I enter "<Cost Center1>" into the field with label "HEADER_TXT"
    And  I enter current date in yyyymmdd into the field with label "DOC_DATE"
    And  I enter current date in yyyymmdd into the field with label "PSTNG_DATE"
    And  I enter "AUTOMATION" with random number into "REF_DOC_NO" in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on "Standard Inbound"
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    And  I click on "Line Item" in IDOC page
    And  I click on "My Action List"
    And  I wait for element with text of "Process" to be visible
    And  I click on "Line Item" in IDOC page
    And  I click on "Process"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I enter "<CompanyCode>" into the field with label "Company Code*"
    And  I click checkbox or radio button with label "Pre-approved"
    And  I enter "<GL>" information on "GL" in IDOC page and verify text input
    And  I hit return
    And  I click on partial text "Attempt Posting"
    And  I switch to the first iFrame in SAP
    And  I click on element with title attribute value "Save "
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on partial text "posted successfully"
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    When I click on "Record Id" in IDOC page
    And  I click on partial text "Posted"
    And  I print "Tax amount" element value in Idoc page
    Then I verify if tax field at row number "1", column number "10" contains "<TaxType>"
    And  I verify if tax field at row number "2", column number "10" contains "<TaxType>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on element with title attribute value "Invoice details"
    And  I wait "30" seconds for element with partial text of "Display Currency" to be visible
    And  I take page screenshot

    Examples:
      | TCode1  | TCode2       | TaxCodeCount | CompanyCode | GL     | TaxType | Cost Center1 | Folder Name      | File Name | Sheet name | row number |
      | /n WE19 | /n /dol/ap2n | 5            | 1000        | 630050 | U1      | 1089         | Invoice Creation | ap.xlsx   | Scenario 1 | 2          |
      | /n WE19 | /n /dol/ap2n | 5            | 1000        | 630050 | U1      | 1257         | Invoice Creation | ap.xlsx   | Scenario 1 | 2          |
      | /n WE19 | /n /dol/ap2n | 2            | 1000        | 630020 | I0      | 1022         | Invoice Creation | ap.xlsx   | Scenario 1 | 2          |
      | /n WE19 | /n /dol/ap2n | 2            | 1000        | 630020 | I0      | 1519         | Invoice Creation | ap.xlsx   | Scenario 1 | 2          |

  @apTax
  @apScenario3
  Scenario Outline: Some (GL's) are taxable and some are not taxable; vendor did not charge tax
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode1>" in the command field
    And  I enter a test idoc to copy from
    And  I execute
    And  I click on partial text "APEDI"
    And  I switch to the first iFrame in SAP
    And  I enter "<Cost Center1>" into the field with label "HEADER_TXT"
    And  I enter current date in yyyymmdd into the field with label "DOC_DATE"
    And  I enter current date in yyyymmdd into the field with label "PSTNG_DATE"
    And  I enter "AUTOMATION" with random number into "REF_DOC_NO" in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on "Standard Inbound"
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    And  I click on "Line Item" in IDOC page
    And  I click on "My Action List"
    And  I wait for element with text of "Process" to be visible
    And  I click on "Line Item" in IDOC page
    And  I click on "Process"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I enter "<CompanyCode>" into the field with label "Company Code*"
    And  I click checkbox or radio button with label "Pre-approved"
    And  I enter "<GL>" information on "GL" in IDOC page and verify text input
    And  I enter "<GL Taxable>" information on "GL line 2" in IDOC page and verify text input
    And  I hit return
    And  I click on partial text "Attempt Posting"
    And  I switch to the first iFrame in SAP
    And  I click on element with title attribute value "Save "
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on partial text "posted successfully"
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    When I click on "Record Id" in IDOC page
    And  I click on partial text "Posted"
    And  I print "Tax amount" element value in Idoc page
    Then I verify if tax field at row number "1", column number "10" contains "<TaxType>"
    And  I verify if tax field at row number "2", column number "10" contains "<TaxType>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on element with title attribute value "Invoice details"
    And  I wait "30" seconds for element with partial text of "Display Currency" to be visible
    And  I take page screenshot

    Examples:
      | TCode1  | TCode2       | Cost Center1 | TaxCodeCount | CompanyCode | GL     | TaxType | GL Taxable | Folder Name      | File Name | Sheet name | row number |
      | /n WE19 | /n /dol/ap2n | 1428         | 5            | 1000        | 630010 | U1      | 630075     | Invoice Creation | ap.xlsx   | Scenario 3 | 2          |
      | /n WE19 | /n /dol/ap2n | 1257         | 6            | 1000        | 630010 | U1      | 630075     | Invoice Creation | ap.xlsx   | Scenario 3 | 2          |

  @apTax
  @apScenario4
  @3199
  Scenario Outline: Some (GL's) are taxable and some are not taxable; vendor charged tax
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode1>" in the command field
    And  I enter a test idoc to copy from
    And  I execute
    And  I click on partial text "APEDI"
    And  I switch to the first iFrame in SAP
    And  I enter "<Cost Center1>" into the field with label "HEADER_TXT"
    And  I enter current date in yyyymmdd into the field with label "DOC_DATE"
    And  I enter current date in yyyymmdd into the field with label "PSTNG_DATE"
    And  I enter "AUTOMATION" with random number into "REF_DOC_NO" in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on "Standard Inbound"
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    And  I click on "Line Item" in IDOC page
    And  I click on "My Action List"
    And  I wait for element with text of "Process" to be visible
    And  I click on "Line Item" in IDOC page
    And  I click on "Process"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I enter "<CompanyCode>" into the field with label "Company Code*"
    And  I click checkbox or radio button with label "Pre-approved"
    And  I enter "<GL>" information on "GL" in IDOC page and verify text input
    And  I enter "<GL Taxable>" information on "GL line 2" in IDOC page and verify text input
    And  I hit return
    And  I click on partial text "Attempt Posting"
    And  I switch to the first iFrame in SAP
    And  I click on element with title attribute value "Save "
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on partial text "posted successfully"
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    When I click on "Record Id" in IDOC page
    And  I click on partial text "Posted"
    And  I print "Tax amount" element value in Idoc page
    Then I verify if tax field at row number "1", column number "10" contains "<TaxType>"
    And  I verify if tax field at row number "2", column number "10" contains "<TaxType>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on element with title attribute value "Invoice details"
    And  I wait "30" seconds for element with partial text of "Display Currency" to be visible
    And  I take page screenshot
    And  I switch back to the main window in SAP
    Then I verify "The account number" matches "<GL>" displayed in Idoc page
    And  I verify "The cost center number" matches "<Cost Center1>" displayed in Idoc page
    And  I verify "The profit ctr" matches "<Profit Ctr>" displayed in Idoc page
    And  I verify "The account number 2" matches "<GL Taxable>" displayed in Idoc page
    And  I verify "The cost center number 2" matches "<Profit Ctr>" displayed in Idoc page
    And  I verify "The profit ctr 2" matches "<Cost Center1>" displayed in Idoc page
    When I enter t-code "<TCode4>" in the command field
    And  I enter "AIF" into the field with label "Application"
    And  I hit tab
    And  I enter "ZFC" into the field with label "Namespace"
    And  I hit tab
    And  I enter "ZFC_APINVS" into the field with label "Interface Name"
    And  I click on "Select All"
    And  I execute
    And  I click on "Expand Node" in IDOC page
    And  I double click on "First line item in Zfc_Apinvs" in Idoc page
    And  I click on the verticle scroll "Up" button
    And  I double click on "EDIDC"
    And  I take page screenshot
    Then I validate the execution status of the idoc created

    Examples:
      | TCode1  | TCode2       | TCode3  | Cost Center1 | TaxCodeCount | CompanyCode | GL     | TaxType | GL Taxable | Profit Ctr | TCode4     | Folder Name      | File Name | Sheet name | row number |
      | /n WE19 | /n /dol/ap2n | /n fb03 | 1679         | 3            | 1000        | 630015 | I1      | 630050     | 1679       | /N/AIF/ERR | Invoice Creation | ap.xlsx   | Scenario 4 | 2          |
      | /n WE19 | /n /dol/ap2n | /n fb03 | 1428         | 3            | 1000        | 630015 | I1      | 630050     | 1428       | /N/AIF/ERR | Invoice Creation | ap.xlsx   | Scenario 4 | 2          |

  @13735
  @livScenario4
  @livScenarios
  @AllItemsTaxableTaxOnInvoice
  Scenario Outline: SAPECC_APIA_IDOC_TAX_LIV_PO_04_Taxable_All_TaxOnInvoice (ALM#13735)
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I enter "<PurchaseGroup>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value at row "1" for "Article Number" to "<ArticleNumber>" from preferred data source
    And  I enter the purchase order value at row "2" for "Article Number" to "<ArticleNumber2>" from preferred data source
    And  I enter the purchase order value at row "1" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "2" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "1" for "Site" to "<Site>" from preferred data source
    And  I enter the purchase order value at row "2" for "Site" to "<Site>" from preferred data source
    And  I hit return
    And  I click on "Invoice"
    Then I verify the "Tax Code" field has "<TaxCode>" value in ME21N page
    When I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Next Item Button In Migo" in po page
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the success status in SAP
    When I enter t-code "<TCode2>" in the command field
    And  I enter a test idoc to copy from
    And  I select the "Create button in we19" button on the Idoc page
    And  I update idoc values for "<IDocType>" and "<Vendor>"
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode3>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    When I click on "Record Id" in IDOC page
    And  I print "Tax amount" element value in Idoc page
    Then I validate "Tax code" entered matches original "<TaxCode>" in Idoc page
    And  I verify if tax field at row number "1", column number "9" contains "<TaxCode>"
    And  I verify if tax field at row number "2", column number "9" contains "<TaxCode>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on "Invoice Number" in IDOC page
    And  I wait for element with text of "Reference" to be visible
    And  I take page screenshot

    Examples:
      | TCode    | TCode1  | Vendor | PurchaseGroup | ArticleNumber | ArticleNumber2 | Quantity | Site | TaxCode | IDocType   | TCode2  | TCode3       | TaxCodeCountWithTax | POType         |Folder Name      | File Name | Sheet name | row number |
      | /n ME21N | /n MIGO | 13182  | 01            | 92912         | 91966          | 2        | 1434 | I1      | 2 Articles | /n WE19 | /n /dol/ap2n | 5                   | Store Merch PO |Invoice Creation | liv.xlsx  | Scenario 4 | 2          |

  @13739
  @livScenario5
  @livScenarios
  @SomeItemsAreTaxableTaxOnInvoice
  Scenario Outline: SAPECC_APIA_IDOC_TAX_LIV_PO_05_MixedItems_TaxonInvoice (ALM#13739)
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>"
    And  I enter "<PurchaseGroup>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the purchase order value at row "1" for "Article Number" to "<ArticleNumber>" from preferred data source
    And  I enter the purchase order value at row "1" for "Quantity" to "<Quantity>" from preferred data source
    And  I enter the purchase order value at row "1" for "Site" to "<Site>" from preferred data source
    And  I enter "<short text>" information on "Short Text input in row 2" in IDOC page
    And  I enter "<Order per unit>" information on "Order unit input in row 2" in IDOC page
    And  I enter "<Net price>" information on "Net price input in row 2" in IDOC page
    And  I enter "<Order per unit>" information on "Order price unit input in row 2" in IDOC page
    And  I enter "<Merchandise category 2>" information on "Merch Category input in row 2" in IDOC page
    And  I set the input for "Quantity" at row number "2" to "<Quantity>"
    And  I set the input for "Site" at row number "2" to "<Site>"
    And  I hit return
    And  I click on "Invoice"
    Then I verify the "Tax Code" field has "<TaxCode>" value in ME21N page
    When I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Next Item Button In Migo" in po page
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the alert is displayed for the article document being posted
    When I enter t-code "<TCode2>" in the command field
    And  I enter a test idoc to copy from
    And  I select the "Create button in we19" button on the Idoc page
    And  I update idoc values for "<IDocType>" and "<Vendor>"
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode3>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    When I click on "Record Id" in IDOC page
    And  I print "Tax amount" element value in Idoc page
    Then I validate "Tax code" entered matches original "**" in Idoc page
    And  I verify if tax field at row number "1", column number "9" contains "<TaxCode>"
    And  I verify if tax field at row number "2", column number "9" contains "<TaxCode1>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on "Invoice Number" in IDOC page
    And  I wait for element with text of "Reference" to be visible
    And  I take page screenshot

    Examples:
      | TCode    | TCode1  | TCode2  | TCode3       | Vendor | PurchaseGroup | ArticleNumber | Quantity | Site | TaxCode | TaxCode1 | TaxCodeCount | short text      | Order per unit | Net price | Merchandise category 2 | TaxCodeCount | IDocType   | POType         |Folder Name      | File Name | Sheet name | row number |
      | /n ME21N | /n MIGO | /n WE19 | /n /dol/ap2n | 10226  | 01            | 43195         | 2        | 1434 | I0      | I1       | 2            | Taxable article | EA             | 100.00    | PROD SUPPLY MISC       | 2            | 2 Articles | Store Merch PO |Invoice Creation | liv.xlsx  | Scenario 5 | 2          |

  @apScenario2Part2
  Scenario Outline: Invoice creation from FB60
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I enter Today's date into "Invoice Date" in FB60
    And  I enter "<Vendor value>" from preferred data source into the field with label "Vendor"
    And  I hit tab
    And  I enter "<Amount value>" from preferred data source into the field with label "Amount"
    And  I enter "AUTOMATION" from preferred data source into the field with label "Text"
    And  I enter reference number from prefered data source
    And  I enter "<G/L account value>" information on "G/L account FB60" in IDOC page
    And  I scroll to following "Cost center FB60" element
    And  I enter "<Cost center value>" information on "Cost center FB60" in IDOC page
    And  I enter "<Amount in Doc Value>" information on "Amount in doc" in IDOC page
    And  I select the "Amount in doc" button on the Idoc page
    And  I select the "Status line check 1" button on the Idoc page
    And  I wait for "1" seconds
    And  I hit return
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Continue (Enter)" in IDOC page
    And  I switch back to the main window in SAP
    And  I select the "Save FB60" button on the Idoc page
    And  I hit return
    And  I save the document number from "Doc number" in Idoc page
    And  I click on "Menu"
    And  I click on "Document"
    And  I click on "Display"
    Then I validate "Document number FB60" entered matches original saved document number
    And  I verify "Vendor number FB60" matches "<Vendor value>" displayed in Idoc page
    And  I verify "G/L number" matches "<G/L account value>" displayed in Idoc page

    Examples:
      | TCode   | Vendor value | Amount value | G/L account value | Cost center value | Amount in Doc Value |
      | /n FB60 | 10101        | 100.00       | 630020            | 1022              | 100                 |
      | /n FB60 | 10101        | 100.00       | 630020            | 1519              | 100                 |

  @apScenario5
  @apScenario6
  @apTax
  Scenario Outline: All (GL's) are taxable and vendor billed tax more than 2 lines && All (GL's) are Not taxable and vendor billed tax more than 2 lines
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode1>" in the command field
    And  I enter a test idoc to copy from
    And  I execute
    And  I click on partial text "APEDI"
    And  I switch to the first iFrame in SAP
    And  I enter "<Cost Center1>" into the field with label "HEADER_TXT"
    And  I enter current date in yyyymmdd into the field with label "DOC_DATE"
    And  I enter current date in yyyymmdd into the field with label "PSTNG_DATE"
    And  I enter "AUTOMATION" with random number into "REF_DOC_NO" in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on "Standard Inbound"
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    And  I click on "Line Item" in IDOC page
    And  I click on "My Action List"
    And  I wait for element with text of "Process" to be visible
    And  I click on "Line Item" in IDOC page
    And  I click on "Process"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I enter "<CompanyCode>" into the field with label "Company Code*"
    And  I click checkbox or radio button with label "Pre-approved"
    And  I enter "<GL>" information on "GL" in IDOC page and verify text input
    And  I hit tab
    And  I enter "<GL Taxable>" information on "GL line 2" in IDOC page and verify text input
    And  I hit tab
    And  I enter "<GL Taxable 2>" information on "GL line 3" in IDOC page and verify text input
    And  I hit tab
    And  I enter "<GL Taxable 3>" information on "GL line 4" in IDOC page and verify text input
    And  I hit tab
    And  I hit return
    And  I click on partial text "Attempt Posting"
    And  I switch to the first iFrame in SAP
    And  I click on element with title attribute value "Save "
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on partial text "posted successfully"
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    When I click on "Record Id" in IDOC page
    And  I click on partial text "Posted"
    And  I print "Tax amount" element value in Idoc page
    Then I verify if tax field at row number "1", column number "10" contains "<TaxType>"
    And  I verify if tax field at row number "2", column number "10" contains "<TaxType>"
    When I click on "SAP Document Header" in IDOC page
    And  I click on element with title attribute value "Invoice details"
    And  I wait "30" seconds for element with partial text of "Display Currency" to be visible
    And  I take page screenshot

    Examples:
      | TCode1  | TCode2       | Cost Center1 | TaxCodeCount | CompanyCode | GL     | TaxType | GL Taxable | GL Taxable 2 | GL Taxable 3 |Folder Name      | File Name | Sheet name | row number |
      | /n WE19 | /n /dol/ap2n | 1292         | 5            | 1000        | 630060 | I1      | 630050     | 630030       | 630100       |Invoice Creation | ap.xlsx   | Scenario 5 | 2          |
      | /n WE19 | /n /dol/ap2n | 1887         | 5            | 1000        | 630060 | I1      | 630050     | 630030       | 630100       | Invoice Creation | ap.xlsx   | Scenario 5 | 2          |
      | /n WE19 | /n /dol/ap2n | 1292         | 5            | 1000        | 630010 | I1      | 630020     | 630115       | 630110       | Invoice Creation | ap.xlsx   | Scenario 5 | 2          |
      | /n WE19 | /n /dol/ap2n | 1887         | 5            | 1000        | 630010 | I1      | 630020     | 630115       | 630110       | Invoice Creation | ap.xlsx   | Scenario 5 | 2          |

  @apTax
  @apScenario8
  Scenario Outline: GL used is not tax relevent and Vendor invoice has no tax.
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode1>" in the command field
    And  I enter a test idoc to copy from
    And  I execute
    And  I click on partial text "APEDI"
    And  I switch to the first iFrame in SAP
    And  I enter "<Cost Center1>" into the field with label "HEADER_TXT"
    And  I enter current date in yyyymmdd into the field with label "DOC_DATE"
    And  I enter current date in yyyymmdd into the field with label "PSTNG_DATE"
    And  I enter "AUTOMATION" with random number into "REF_DOC_NO" in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on "Standard Inbound"
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    And  I click on "Line Item" in IDOC page
    And  I click on "My Action List"
    And  I wait for element with text of "Process" to be visible
    And  I click on "Line Item" in IDOC page
    And  I click on "Process"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I enter "<CompanyCode>" into the field with label "Company Code*"
    And  I click checkbox or radio button with label "Pre-approved"
    And  I enter "<GL>" information on "GL" in IDOC page and verify text input
    And  I hit return
    And  I click on partial text "Attempt Posting"
    And  I switch to the first iFrame in SAP
    And  I click on element with title attribute value "Save "
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on partial text "posted successfully"
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify the success status in SAP
    When I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    Then I verify "Status 100" is displayed in iDoc page
    When I click on "Record Id" in IDOC page
    And  I click on partial text "Posted"
    And  I print "Tax amount" element value in Idoc page
    Then I validate "Tax amount" entered matches original "0.00" in Idoc page
    And  I validate "Tax code" entered matches original "" in Idoc page
    When I click on "SAP Document Header" in IDOC page
    And  I click on element with title attribute value "Invoice details"
    And  I wait "30" seconds for element with partial text of "Display Currency" to be visible
    And  I take page screenshot

    Examples:
      | TCode1  | TCode2       | CompanyCode | GL     | Cost Center1 |Folder Name      | File Name | Sheet name | row number |
      | /n WE19 | /n /dol/ap2n | 1000        | 630004 | 1760         |Invoice Creation | ap.xlsx   | Scenario 1 | 2          |
      | /n WE19 | /n /dol/ap2n | 1000        | 630004 | 1743         |Invoice Creation | ap.xlsx   | Scenario 1 | 2          |

  @apTax
  @apScenario7
  Scenario Outline: GL used is not tax relevent: Vendor invoice has tax (Idoc will fail and AP will coordinate with tax and acct to address non tax relevent GL)
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode1>" in the command field
    And  I enter a test idoc to copy from
    And  I execute
    And  I click on partial text "APEDI"
    And  I switch to the first iFrame in SAP
    And  I enter "<Cost Center1>" into the field with label "HEADER_TXT"
    And  I enter current date in yyyymmdd into the field with label "DOC_DATE"
    And  I enter current date in yyyymmdd into the field with label "PSTNG_DATE"
    And  I enter "AUTOMATION" with random number into "REF_DOC_NO" in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on "Standard Inbound"
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I switch back to the main window in SAP
    And  I wait for "Idoc Generator" iFrame to appear
    And  I switch to "Idoc Generator" Iframe in Idoc page
    And  I save the "IDOC" number in idoc page
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter t-code "<TCode2>" in the command field
    And  I enter Idoc Number into "IDOC input field info center" in idoc page
    And  I execute
    And  I click on "Line Item" in IDOC page
    And  I click on "My Action List"
    And  I wait for element with text of "Process" to be visible
    And  I click on "Line Item" in IDOC page
    And  I click on "Process"
    And  I wait for iframe with check ok to appear
    And  I hit return
    And  I enter "<CompanyCode>" into the field with label "Company Code*"
    And  I click checkbox or radio button with label "Pre-approved"
    And  I enter "<GL>" information on "GL" in IDOC page and verify text input
    And  I hit return
    And  I click on partial text "Attempt Posting"
    And  I switch to the first iFrame in SAP
    And  I click on element with title attribute value "Save "
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on partial text "Cannot post tax"
    And  I hit return

    Examples:
      | TCode1  | TCode2       | CompanyCode | GL     | Cost Center1 |Folder Name      | File Name | Sheet name | row number |
      | /n WE19 | /n /dol/ap2n | 1000        | 630004 | 1760         |Invoice Creation | ap.xlsx   | Scenario 7 | 2          |
      | /n WE19 | /n /dol/ap2n | 1000        | 630004 | 1743         |Invoice Creation | ap.xlsx   | Scenario 7 | 2          |