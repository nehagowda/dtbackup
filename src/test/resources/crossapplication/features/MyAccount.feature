@myAccountIntegration
Feature: My Accounts Integration

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @ropisOrderWithVehicleAndAppointment
  Scenario Outline: ROPIS Order with appointment for standard vehicle_tire_and_wheel
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select the "<Brand>" checkbox
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select the "<Brand1>" checkbox
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                              | password  | FitmentOption | ItemCode | Brand       | Brand1 | ItemCode1 | Customer                | Excel sheet                | Excel File                     |
      | 2012 | Honda | Civic | Coupe DX | none     | integration.testsuiteone@gmail.com | Discount1 | tire           | 26899    | Continental | Maxxim | 23500     | CUSTOMER_INTEGRATION_AZ | ropiscreatewithappointment | myAccountWebOrdersSuiteOne.xls |


  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @bopisOrderWithVehicleAndAppointment
  Scenario Outline: BOPIS Order with appointment for standard vehicle_tire_and_wheel
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select the "<Brand>" checkbox
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select the "<Brand1>" checkbox
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                              | password  | FitmentOption | Customer                    | ItemCode | Brand       | Brand1 | ItemCode1 | Customer1               | Credit Card | Excel sheet                | Excel File                     |
      | 2012 | Honda | Civic | Coupe DX | none     | integration.testsuiteone@gmail.com | Discount1 | tire           | DEFAULT_CUSTOMER_BOPIS_VISA | 26899    | Continental | Maxxim | 23500     | CUSTOMER_INTEGRATION_AZ | Visa Bopis  | bopiscreatewithappointment | myAccountWebOrdersSuiteOne.xls |

  @dtd
  @web
  @dtdset
  @myAccountOrderIntegration
  @dtdOrderWithVehicle
  Scenario Outline: DTD Create - Vehicle [Tire and Wheel]
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I click on the "Continue to Shipping Method" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttiredirect"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make   | Model  | Trim  | Assembly       | Email                              | password  | FitmentOption | Customer                    | ItemCode | ItemCode1 | Credit Card | Customer1               | Excel sheet | Excel File                     |
      | 2015 | Nissan | Altima | Sedan | 215 /55 R17 SL | integration.testsuiteone@gmail.com | Discount1 | tire           | DEFAULT_CUSTOMER_BOPIS_VISA | 14293    | 23516     | visa bopis  | CUSTOMER_INTEGRATION_AZ | dtdcreate   | myAccountWebOrdersSuiteOne.xls |


  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @serviceAppointmentWithOneLineItem
  Scenario Outline: Service appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the "APPOINTMENTS" navigation link
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I enter "<FirstName>" into the "First Name" field
    And  I enter "<LastName>" into the "Last Name" field
    And  I enter "<Email>" into the "E-mail Address" field
    And  I enter "<Phone>" into the "Phone" field
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I store the order number
    And  I take page screenshot
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Email                              | password  | ServiceOptions                | Customer1               | Excel sheet           | Excel File                     | FirstName   | LastName | Phone        |
      | integration.testsuiteone@gmail.com | Discount1 | New Tires/Wheels Consultation | CUSTOMER_INTEGRATION_AZ | serviceappointmentone | myAccountWebOrdersSuiteOne.xls | Integration | Suiteone | 928-214-0480 |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @ropisOrderWithFreeTextSearchWithAppointment
  Scenario Outline: ROPIS Create -  No Vehicle [Free text search] - with appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I do a free text search for "<ItemCode1>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Email                              | password  | ItemCode | ItemCode1 | Customer1               | Excel sheet                   | Excel File                     |
      | integration.testsuiteone@gmail.com | Discount1 | 17687    | 41143     | CUSTOMER_INTEGRATION_AZ | ropisnovehiclewithappointment | myAccountWebOrdersSuiteOne.xls |

  @dtd
  @web
  @dtdset
  @myAccountOrderIntegration
  @dtdOrderWithFreeTextSearch
  Scenario Outline: DTD Create - [Free text search]
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I do a free text search for "<ItemCode1>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I click on the "Continue to Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttiredirect"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Email                              | password  | Customer                    | ItemCode | ItemCode1 | Customer1               | Excel sheet  | Excel File                     |
      | integration.testsuiteone@gmail.com | Discount1 | DEFAULT_CUSTOMER_BOPIS_VISA | 12059    | 75887     | CUSTOMER_INTEGRATION_AZ | dtdnovehicle | myAccountWebOrdersSuiteOne.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @ropisOrderTireAndWheelFetAndPromoWithAppointment
  Scenario Outline: ROPIS Create - Vehicle [Tire[FET] and Wheel] - Promo - with appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make | Model | Trim         | Assembly       | Email                              | password  | FitmentOption | ItemCode | ItemCode1 | Customer1               | Excel sheet             | Excel File                     |
      | 2016 | Ram  | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | integration.testsuiteone@gmail.com | Discount1 | tire          | 40480    | 48235     | CUSTOMER_INTEGRATION_AZ | ropisfetwithappointment | myAccountWebOrdersSuiteOne.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @bopisOrderTireAndWheelFetAndPromoWithAppointment
  Scenario Outline: BOPIS Create - Vehicle [Tire[FET] and Wheel] - Promo - with appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I click on the "PLACE ORDER" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make | Model | Trim         | Assembly       | Email                              | password  | FitmentOption | Customer                        | ItemCode | Credit Card    | ItemCode1 | Customer1               | Excel sheet             | Excel File                     |
      | 2016 | Ram  | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | integration.testsuiteone@gmail.com | Discount1 | tire          | DEFAULT_CUSTOMER_BOPIS_DISCOVER | 40480    | Discover Bopis | 48235     | CUSTOMER_INTEGRATION_AZ | bopisfetwithappointment | myAccountWebOrdersSuiteOne.xls |

  @dtd
  @web
  @dtdset
  @myAccountOrderIntegration
  @dtdOrderWithTireAndWheelWithFetAndPromo
  Scenario Outline: DTD Create - Vehicle [Tire[FET] and Wheel] - Promo
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttiredirect"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make | Model | Trim         | Assembly       | Email                              | password  | FitmentOption | Customer                    | ItemCode | ItemCode1 | Customer1               | Excel sheet | Excel File                     |
      | 2016 | Ram  | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | integration.testsuiteone@gmail.com | Discount1 | tire           | DEFAULT_CUSTOMER_BOPIS_VISA | 26068    | 48235     | CUSTOMER_INTEGRATION_AZ | dtdfetpromo | myAccountWebOrdersSuiteOne.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @ropisOrderTireAndWheelPromoWithAppointment
  Scenario Outline: ROPIS Create - Vehicle [Tire and Wheel] - Promo - with appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption1>" and shop for all tires/wheels
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    When I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                              | password  | FitmentOption | FitmentOption1 | ItemCode | Year1 | Make1 | Model1 | Trim1        | Assembly1      | ItemCode1 | Customer1               | Excel sheet               | Excel File                     |
      | 2011 | Honda | Civic | Coupe EX | none     | integration.testsuiteone@gmail.com | Discount1 | tire           | wheel           | 34572    | 2016  | Ram   | 2500   | MEGA CAB 4WD | 275 /70 R18 E1 | 48235     | CUSTOMER_INTEGRATION_AZ | ropispromowithappointment | myAccountWebOrdersSuiteOne.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @bopisOrderTireAndWheelPromoWithAppointment
  Scenario Outline: BOPIS Create - Vehicle [Tire and Wheel] - Promo - with appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption1>" and shop for all tires/wheels
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                              | password  | FitmentOption | ItemCode | Customer                    | Year1 | Make1 | Model1 | Trim1        | Assembly1      | FitmentOption1 | ItemCode1 | Customer1               | Excel sheet               | Excel File                     |
      | 2011 | Honda | Civic | Coupe EX | none     | integration.testsuiteone@gmail.com | Discount1 | tire           | 34572    | DEFAULT_CUSTOMER_BOPIS_VISA | 2016  | Ram   | 2500   | MEGA CAB 4WD | 275 /70 R18 E1 | wheel           | 48235     | CUSTOMER_INTEGRATION_AZ | bopispromowithappointment | myAccountWebOrdersSuiteOne.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @serviceAppointmentWithMoreThanOneLineItem
  Scenario Outline: Service appointment - more than 1 line item - with appointment[Next Day]
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the "APPOINTMENTS" navigation link
    And  I select service option(s): "<ServiceOptions>"
    And  I select service option(s): "<ServiceOptions1>"
    And  I select service option(s): "<ServiceOptions2>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I enter "<FirstName>" into the "First Name" field
    And  I enter "<LastName>" into the "Last Name" field
    And  I enter "<Email>" into the "E-mail Address" field
    And  I enter "<Phone>" into the "Phone" field
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I store the order number
    And  I take page screenshot
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Email                              | password  | ServiceOptions                | ServiceOptions1           | ServiceOptions2 | Customer1               | Excel sheet           | Excel File                     | FirstName   | LastName | Phone        |
      | integration.testsuiteone@gmail.com | Discount1 | New Tires/Wheels Consultation | Tire Rotation and Balance | Tire Balancing  | CUSTOMER_INTEGRATION_AZ | serviceappointmenttwo | myAccountWebOrdersSuiteOne.xls | Integration | Suiteone | 928-214-0480 |

  @dt
    @at
    @web
    @set1
    @myAccountOrderIntegration
    @myAccountOrderIntegrationScenariosSet2
    @ropisOrderWithVehicleAndWithoutAppointment
  Scenario Outline: ROPIS Order without appointment for standard vehicle_tire_and_wheel
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select the "<Brand>" checkbox
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select the "<Brand1>" checkbox
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I click on the "Continue To Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                       | password  | FitmentOption | ItemCode | Brand       | Brand1 | ItemCode1 | Reason                              | Customer                | Excel sheet                   | Excel File                     |
      | 2012 | Honda | Civic | Coupe DX | none     | discountusertwo@outlook.com | Discount1 | tire           | 26899    | Continental | Maxxim | 23500     | Make an appointment at a later time | CUSTOMER_INTEGRATION_TX | ropiscreatewithoutappointment | myAccountWebOrdersSuiteTwo.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @myAccountOrderIntegrationScenariosSet2
  @bopisOrderWithVehicleAndWithoutAppointment
  Scenario Outline: BOPIS Order without appointment for standard vehicle_tire_and_wheel
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select the "<Brand>" checkbox
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select the "<Brand1>" checkbox
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer1>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                       | password  | FitmentOption | Customer1                         | ItemCode | Brand       | Brand1 | ItemCode1 | Reason                              | Credit Card      | Customer                | Excel sheet                   | Excel File                     |
      | 2012 | Honda | Civic | Coupe DX | none     | discountusertwo@outlook.com | Discount1 | tire           | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | 26899    | Continental | Maxxim | 23500     | Make an appointment at a later time | MasterCard Bopis | CUSTOMER_INTEGRATION_TX | bopiscreatewithoutappointment | myAccountWebOrdersSuiteTwo.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @myAccountOrderIntegrationScenariosSet2
  @ropisOrderWithFreeTextSearchWithoutAppointment
  Scenario Outline: ROPIS Create -  No Vehicle [Free text search] - without appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I do a free text search for "<ItemCode1>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Email                       | password  | ItemCode | ItemCode1 | Reason                              | Customer                | Excel sheet                   | Excel File                     |
      | discountusertwo@outlook.com | Discount1 | 17687    | 41143     | Make an appointment at a later time | CUSTOMER_INTEGRATION_TX | ropisnovehiclewoutappointment | myAccountWebOrdersSuiteTwo.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @myAccountOrderIntegrationScenariosSet2
  @ropisOrderTireAndWheelFetAndPromoWithoutAppointment
  Scenario Outline: ROPIS Create - Vehicle [Tire[FET] and Wheel] - Promo - without appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make | Model | Trim         | Assembly       | Email                       | password  | FitmentOption | ItemCode | ItemCode1 | Reason                              | Customer                | Excel sheet                | Excel File                     |
      | 2016 | Ram  | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | discountusertwo@outlook.com | Discount1 | tire           | 40480    | 48235     | Make an appointment at a later time | CUSTOMER_INTEGRATION_TX | ropisfetwithoutappointment | myAccountWebOrdersSuiteTwo.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @myAccountOrderIntegrationScenariosSet2
  @bopisOrderTireAndWheelFetAndPromoWithoutAppointment
  Scenario Outline: BOPIS Create - Vehicle [Tire[FET] and Wheel] - Promo - without appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select "On Promotion" option
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I click on the "PLACE ORDER" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make | Model | Trim         | Assembly       | Email                       | password  | FitmentOption | Customer                        | ItemCode | Credit Card    | ItemCode1 | Reason                              | Customer1               | Excel sheet                | Excel File                     |
      | 2016 | Ram  | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | discountusertwo@outlook.com | Discount1 | tire           | DEFAULT_CUSTOMER_BOPIS_DISCOVER | 40480    | Discover Bopis | 48235     | Make an appointment at a later time | CUSTOMER_INTEGRATION_TX | bopisfetwithoutappointment | myAccountWebOrdersSuiteTwo.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @myAccountOrderIntegrationScenariosSet2
  @ropisOrderTireAndWheelPromoWithoutAppointment
  Scenario Outline: ROPIS Create - Vehicle [Tire and Wheel] - Promo - without appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption1>" and shop for all tires/wheels
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    Then I am brought to the order confirmation page
    When I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                       | password  | FitmentOption | FitmentOption1 | ItemCode | Year1 | Make1 | Model1 | Trim1        | Assembly1      | ItemCode1 | Reason                              | Customer                | Excel sheet                  | Excel File                     |
      | 2011 | Honda | Civic | Coupe EX | none     | discountusertwo@outlook.com | Discount1 | tire           | wheel           | 34572    | 2016  | Ram   | 2500   | MEGA CAB 4WD | 275 /70 R18 E1 | 48235     | Make an appointment at a later time | CUSTOMER_INTEGRATION_TX | ropispromowithoutappointment | myAccountWebOrdersSuiteTwo.xls |

  @dt
  @at
  @web
  @set1
  @myAccountOrderIntegration
  @myAccountOrderIntegrationScenariosSet2
  @bopisOrderTireAndWheelPromoWithoutAppointment
  Scenario Outline: BOPIS Create - Vehicle [Tire and Wheel] - Promo - without appointment
    When I change to the default store
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption1>" and shop for all tires/wheels
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer1>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                       | password  | FitmentOption | ItemCode | Customer1                         | Year1 | Make1 | Model1 | Trim1        | Assembly1      | FitmentOption1 | ItemCode1 | Reason                              | Customer                | Excel sheet                  | Excel File                     |
      | 2011 | Honda | Civic | Coupe EX | none     | discountusertwo@outlook.com | Discount1 | tire           | 34572    | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | 2016  | Ram   | 2500   | MEGA CAB 4WD | 275 /70 R18 E1 | wheel           | 48235     | Make an appointment at a later time | CUSTOMER_INTEGRATION_TX | bopispromowithoutappointment | myAccountWebOrdersSuiteTwo.xls |

  @bopisWithAppointmentmyAccountOrderIntegrationValidation
  @myAccountSalesOrderValidation
  Scenario Outline: BOPIS Web Order Sales Order Validation [Method - Hybris Web Order direct Search] with appointments
    And  I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter BOPIS web order number into field name with label "Transaction Number"
    And  I click on "Execute" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter BOPIS web order number into field name with label "Transaction Number"
    And  I execute
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number in posdt and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I make a post call to car item header service with field name "HYBORDRNUM"
    And  I make a post call to car header service for Customer with Email/Customer ID "<Email>"

    Examples:
      | TCode 1       | Excel File                     | Excel Sheet                   | Email                              |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | bopiscreatewithappointment    | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | bopisfetwithappointment       | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | bopispromowithappointment     | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteTwo.xls | bopiscreatewithoutappointment | discountusertwo@outlook.com        |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteTwo.xls | bopisfetwithoutappointment    | discountusertwo@outlook.com        |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteTwo.xls | bopispromowithoutappointment  | discountusertwo@outlook.com        |

  @dtdmyAccountOrderIntegrationValidation
  @myAccountSalesOrderValidationDtd
  Scenario Outline: DTD Web Order Sales Order Validation [Method - Hybris Web Order direct Search] My Account
    When I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "before" from excel sheet which is in "2"th row and "4"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "after" from excel sheet which is in "2"th row and "5"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttiredirect"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter start time and end time in posdm
    And  I enter site numbers for DTD in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I click on "Execute" in posdm page
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time in posdm
    And  I enter site numbers for DTD in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    And  I make a post call to car item header service with field name "HYBORDRNUM"
    And  I make a post call to car header service for Customer with Email/Customer ID "<Email>"

    Examples:
      | TCode 1       | Excel File                     | Excel Sheet  | Transaction Type | Email                              |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | dtdcreate    | 1014             | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | dtdnovehicle | 1014             | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | dtdfetpromo  | 1014             | integration.testsuiteone@gmail.com |

  @ropisWithAppointmentmyAccountOrderIntegrationValidation
  @myAccountSalesOrderValidation
  Scenario Outline: DT Web Order Sales Order Validation [Method - Hybris Web Order direct Search] with appointment
    When I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "before" from excel sheet which is in "2"th row and "4"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "after" from excel sheet which is in "2"th row and "5"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter start time and end time in posdm
    And  I enter store number for DT orders in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I click on "Execute" in posdm page
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time in posdm
    And  I enter store number for DT orders in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number in posdt and save it to scenario data
    When I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I make a post call to car item header service with field name "HYBORDRNUM"
    And  I make a post call to car header service for Customer with Email/Customer ID "<Email>"

    Examples:
      | TCode 1       | Excel File                     | Excel Sheet                   | Transaction Type | Email                              |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | ropiscreatewithappointment    | 1014             | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | ropisnovehiclewithappointment | 1014             | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | ropisfetwithappointment       | 1014             | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | ropispromowithappointment     | 1014             | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteTwo.xls | ropiscreatewithoutappointment | 1014             | discountusertwo@outlook.com        |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteTwo.xls | ropisnovehiclewoutappointment | 1014             | discountusertwo@outlook.com        |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteTwo.xls | ropisfetwithoutappointment    | 1014             | discountusertwo@outlook.com        |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteTwo.xls | ropispromowithoutappointment  | 1014             | discountusertwo@outlook.com        |

  @serviceAppointmentmyAccountOrderIntegrationValidation
  @myAccountSalesOrderValidation
  Scenario Outline: DT Service Appointment Sales Order Validation [Method - Hybris Web Order direct Search] My Account
    When I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "before" from excel sheet which is in "2"th row and "4"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "after" from excel sheet which is in "2"th row and "5"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I set appointment Flag to true
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter start time and end time in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I click on "Execute" in posdm page
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number in posdt and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I make a post call to car item header service with field name "HYBORDRNUM"
    And  I make a post call to car header service for Customer with Email/Customer ID "<Email>"

    Examples:
      | TCode 1       | Excel File                     | Excel Sheet           | Transaction Type | Email                              |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | serviceappointmentone | 1003             | integration.testsuiteone@gmail.com |
      | /n/posdw/mon0 | myAccountWebOrdersSuiteOne.xls | serviceappointmenttwo | 1003             | integration.testsuiteone@gmail.com |
