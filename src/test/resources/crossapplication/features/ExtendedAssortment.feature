@extendedAssortment
@crossApplication
Feature: Cross Application Extended Assortment tests

  @15837
  @15849
  @15847
  Scenario Outline: Cross Application Extended Assortment Positive test case 1 (@extendedAssortmentOrders_15837)
    """ INT_SAP_POC_Extended Assortment_Validate PO with Partner Function and Line item details - ALM 15849
        INT_SAP_POC_Extended Assortment_Validated Automated PO Creation in ECC_1 Web Order - ALM 15847 """
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I update quantity for item "<ItemCode>" to "<UpdatedQuantity>"
    Then I verify quantity for "<ItemCode>" is set to "<UpdatedQuantity>" in the "cart"
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "Order Confirmation" page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select verbose logging
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    And  I extract the Purchase Order Number and save it to scenario data
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<TCode>" in the command field
    And  I search for the previously saved "Extended Assortment" PO number
    Then I verify the "Extended Assortment" purchase order number is in the page title
    And  I verify "<POType>" matches the "PO Type" displayed information
    When I click on "Partners"
    Then I verify "OA" is listed in Partner function
    Then I verify "VN" is listed in Partner function
    Then I verify "GS" is listed in Partner function
    When I click on "Texts"
    Then I verify "<Text>" matches text displayed
    When I click on "Partners"
    Then I verify "<ArticleNumber>" matches the "Article Number" displayed information
    And  I verify "<Quantity>" matches the "Quantity" displayed information
    And  I verify "<NetPrice>" matches the "Net Price" displayed information
    And  I verify "<Currency>" matches the "Currency" displayed information
    And  I verify "<Site>" matches the "Site" displayed information
    And  I verify "<StorageLocation>" matches the "Stor. Location" displayed information

    Examples:
      | Year | Make   | Model | Trim  | Assembly       | ItemCode | UpdatedQuantity | Customer                    | Credit Card | InventoryMessage                         | TCode | POType         | ArticleNumber | Quantity | Site              | Text                | NetPrice | Currency | StorageLocation | DateRange |
      | 2013 | BMW    | 335i  | Sedan | 225 /45 R18 SL | 17111    | 4               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | ME23N | Store Merch PO | 17111         | 4        | BRIGHTON (COD 36) | IMA BOPIS-VISA-TEST | 111.02   | USD      | Inventory       | This Day  |
      | 2016 | Toyota | Camry | XLE   | none           | 34146    | 4               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | ME23N | Store Merch PO | 34146         | 4        | BRIGHTON (COD 36) | IMA BOPIS-VISA-TEST | 111.02   | USD      | Inventory       | This Day  |

  @15993
  Scenario Outline:  INT_SAP_POC_Extended Assortment_Email_Full Good Receipt would generate Idocs (ALM#15993)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I update quantity for item "<ItemCode>" to "<UpdatedQuantity>"
    Then I verify quantity for "<ItemCode>" is set to "<UpdatedQuantity>" in the "cart"
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "Order Confirmation" page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select verbose logging
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I extract the Purchase Order Number and save it to scenario data
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<TCode>" in the command field
    Then I am brought to the page with the title "Goods Receipt Purchase Order"
    And  I verify the dropdown boxes are defaulted to "Goods Receipt" and "Purchase Order"
    When I enter the purchase order number in the entry field
    Then I verify the "generated" purchase order number is in the page title
    When I select the "Item Okay" checkbox for each article in the purchase order
    And  I click on "Save"
    Then I verify the alert is displayed for the article document being posted
    And  I save the article document number to scenario data
    When I click on "back"
    And  I enter t-code "<TCode2>" in the command field
    And  I search the desired PO number
    And  I select the Purchase Order History Tab for Receipt Validation
    Then I verify the article document matches the previously saved number
    When I select the article document link
    And  I select the Display outputs button on the Outputs tab
    And  I select the email output process code row and the processing log button
    And  I switch to the first iFrame
    Then I extract the email validation IDoc number

    Examples:
      | Year | Make   | Model | Trim  | Assembly       | DateRange | ItemCode | UpdatedQuantity | Customer                    | Credit Card | InventoryMessage                         | TCode | TCode2 | DateRange |
      | 2013 | BMW    | 335i  | Sedan | 225 /45 R18 SL | This Day  | 17111    | 4               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | MIGO  | ME23N  | This Day  |
      | 2016 | Toyota | Camry | XLE   | none           | This Day  | 34146    | 4               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | MIGO  | ME23N  | This Day  |
      

  @15848
  @15998
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_Validate checkout flow when I add tires from secondary suppliers with 2 brands having regular fitment (ALM #15848)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeB>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeA>" on "Order Confirmation" page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeB>" on "Order Confirmation" page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select verbose logging
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    And  I extract the Purchase Order Numbers for each article and save it to scenario data
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<TCode>" in the command field
    And  I search for the previously saved Extended Assortment PO numbers with "<ItemCodeABrand>"
    Then I verify the automated purchase order number for "<ItemCodeABrand>" is in the page title
    And  I verify "<POType>" matches the "PO Type" displayed information
    When I click on "Partners"
    Then I verify "OA" is listed in Partner function
    And  I verify "VN" is listed in Partner function
    And  I verify "GS" is listed in Partner function
    When I click on "Communication"
    Then I verify reference matches the web order number
    When I click on "Texts"
    Then I verify "<Text>" matches text displayed
    When I click on "Partners"
    Then I verify "<ItemCodeA>" matches the "Article Number" displayed information
    And  I verify "<Quantity>" matches the "Quantity" displayed information
    And  I verify "<Currency>" matches the "Currency" displayed information
    And  I verify "<Site>" matches the "Site" displayed information
    And  I verify "<StorageLocation>" matches the "Stor. Location" displayed information
    And  I search for the previously saved Extended Assortment PO numbers with "<ItemCodeBBrand>"
    Then I verify the automated purchase order number for "<ItemCodeBBrand>" is in the page title
    And  I verify "<POType>" matches the "PO Type" displayed information
    When I click on "Partners"
    Then I verify "OA" is listed in Partner function
    And  I verify "VN" is listed in Partner function
    And  I verify "GS" is listed in Partner function
    When I click on "Communication"
    Then I verify reference matches the web order number
    When I click on "Texts"
    Then I verify "<Text>" matches text displayed
    When I click on "Partners"
    Then I verify "<ItemCodeB>" matches the "Article Number" displayed information
    And  I verify "<Quantity>" matches the "Quantity" displayed information
    And  I verify "<Currency>" matches the "Currency" displayed information
    And  I verify "<Site>" matches the "Site" displayed information
    And  I verify "<StorageLocation>" matches the "Stor. Location" displayed information

    Examples:
      | Year | Make   | Model | Trim | Assembly | ItemCodeA | ItemCodeABrand | ItemCodeB | ItemCodeBBrand | Customer                    | Credit Card | InventoryMessage                         | TCode | POType         | Quantity | Site              | Text                | Currency | StorageLocation | DateRange |
      | 2016 | Toyota | Camry | XLE  | none     | 44286     | YOK            | 30431     | GDY            | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | ME23N | Store Merch PO | 4        | BRIGHTON (COD 36) | IMA BOPIS-VISA-TEST | USD      | Inventory       | This Day  |
      | 2016 | Toyota | Camry | XLE  | none     | 37068     | YOK            | 10708     | GDY            | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | ME23N | Store Merch PO | 4        | BRIGHTON (COD 36) | IMA BOPIS-VISA-TEST | USD      | Inventory       | This Day  |

  @15834
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier and wheels avaialble in my store added with Regular fitment (ALM #15834)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the "<InventoryMessageA>" displayed for "<ItemCodeA>" on "Order Confirmation" page
    And  I verify the "<InventoryMessageB>" displayed for "<ItemCodeB>" on "Order Confirmation" page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select verbose logging
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    When I extract the Purchase Order Numbers for each article and save it to scenario data
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<TCode>" in the command field
    And  I search for the previously saved Extended Assortment PO numbers with "<ItemCodeBrand>"
    Then I verify the automated purchase order number for "<ItemCodeBrand>" is in the page title
    And  I verify "<POType>" matches the "PO Type" displayed information
    When I click on "Partners"
    Then I verify "OA" is listed in Partner function
    And  I verify "VN" is listed in Partner function
    And  I verify "GS" is listed in Partner function
    When I click on "Texts"
    Then I verify "<Text>" matches text displayed
    When I click on "Partners"
    Then I verify "<ItemCodeA>" matches the "Article Number" displayed information
    And  I verify "<Quantity>" matches the "Quantity" displayed information
    And  I verify "<Currency>" matches the "Currency" displayed information
    And  I verify "<Site>" matches the "Site" displayed information
    And  I verify "<StorageLocation>" matches the "Stor. Location" displayed information

    Examples:
      | Year | Make   | Model | Trim | Assembly | ItemCodeA | ItemCodeB | Customer                    | Credit Card | InventoryMessageA                        | InventoryMessageB | ItemCodeBrand | TCode | POType         | Text                | Quantity | Currency | Site              | StorageLocation | DateRange |
      | 2016 | Toyota | Camry | XLE  | none     | 43518     | 75929     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | Available today   | YOK           | ME23N | Store Merch PO | IMA BOPIS-VISA-TEST | 4        | USD      | BRIGHTON (COD 36) | Inventory       | This Day  |
      | 2016 | Toyota | Camry | XLE  | none     | 43518     | 75929     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | Available today   | YOK           | ME23N | Store Merch PO | IMA BOPIS-VISA-TEST | 4        | USD      | BRIGHTON (COD 36) | Inventory       | This Day  |

  @15996
  @15851
  Scenario Outline: INT_SAP_POC_Extended Assortment_EMAIL_Partial GR for 1 PO, email would not triggered until all products received (ALM #15996)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I update quantity for item "<ItemCode>" to "<UpdatedQuantity>"
    Then I verify quantity for "<ItemCode>" is set to "<UpdatedQuantity>" in the "cart"
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "Order Confirmation" page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select verbose logging
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    When I extract the Purchase Order Number and save it to scenario data
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<TCode>" in the command field
    Then I am brought to the page with the title "Goods Receipt Purchase Order"
    And  I verify Head data tab is open
    And  I verify the dropdown boxes are defaulted to "Goods Receipt" and "Purchase Order"
    When I enter the purchase order number in the entry field
    And  I verify Detail data tab is open
    And  I click on the "Quantity" element in po page to navigate
    And  I enter the purchase order value for "Qty in Unit of Entry" to "<Partial>"
    And  I select the "Item Okay" checkbox for each article in the purchase order
    And  I click on "Save"
    And  I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I search for the previously saved "Extended Assortment" PO number
    And  I verify Item detail tab is open
    And  I select the Purchase Order History Tab for Receipt Validation
    And  I select the article document link
    And  I click on the "Output" element in po page to navigate
    And  I click on the "Display Output" element in po page to navigate
    And  I click on the "ZNFY" element in po page to navigate
    And  I click on the "Processing Log" element in po page to navigate
    And  I switch to the first iFrame
    Then I verify ZNFY processing log

    Examples:
      | Year | Make | Model | Trim  | Assembly       | ItemCode | UpdatedQuantity | Customer                    | Credit Card | InventoryMessage                         | TCode | TCode2   | Partial | DateRange |
      | 2013 | BMW  | 335i  | Sedan | 225 /45 R18 SL | 17111    | 4               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | MIGO  | /n ME23n | 3       | This Day  |

  @15999
  @15995
  @15994
  Scenario Outline: INT_SAP_POC_Extended Assortment_EMAIL_Validate PO information_Header, Line itmes etc. once Partial GR completed in full (ALM #15999)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I update quantity for item "<ItemCode>" to "<UpdatedQuantity>"
    Then I verify quantity for "<ItemCode>" is set to "<UpdatedQuantity>" in the "cart"
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "Order Confirmation" page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select verbose logging
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    When I extract the Purchase Order Number and save it to scenario data
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<TCode>" in the command field
    Then I am brought to the page with the title "Goods Receipt Purchase Order"
    And  I verify Head data tab is open
    And  I verify the dropdown boxes are defaulted to "Goods Receipt" and "Purchase Order"
    When I enter the purchase order number in the entry field
    And  I verify Detail data tab is open
    And  I click on the "Quantity" element in po page to navigate
    And  I enter the purchase order value for "Qty in Unit of Entry" to "<Partial>"
    And  I select the "Item Okay" checkbox for each article in the purchase order
    And  I click on "Save"
    And  I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I search for the previously saved "Extended Assortment" PO number
    And  I verify Item detail tab is open
    And  I select the Purchase Order History Tab for Receipt Validation
    And  I select the article document link
    And  I click on the "Output" element in po page to navigate
    And  I click on the "Display Output" element in po page to navigate
    And  I click on the "ZNFY" element in po page to navigate
    And  I click on the "Processing Log" element in po page to navigate
    And  I switch to the first iFrame
    Then I verify ZNFY processing log
    When I select the "Close" button on the Idoc page
    And  I switch back to the main window
    And  I enter t-code "<TCode>" in the command field
    Then I am brought to the page with the title "Goods Receipt Purchase Order"
    And  I verify Head data tab is open
    And  I verify the dropdown boxes are defaulted to "Goods Receipt" and "Purchase Order"
    When I enter the purchase order number in the entry field
    And  I select the "Item Okay" checkbox for each article in the purchase order
    And  I click on "Save"
    And  I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I search for the previously saved "Extended Assortment" PO number
    And  I verify Item detail tab is open
    And  I select the Purchase Order History Tab for Receipt Validation
    And  I select the article document link
    And  I click on the "Output" element in po page to navigate
    And  I click on the "Display Output" element in po page to navigate
    And  I click on the "ZNFY" element in po page to navigate
    And  I click on the "Processing Log" element in po page to navigate
    And  I switch to the first iFrame
    Then I extract the email validation IDoc number
    When I select the "Close" button on the Idoc page
    And  I switch back to the main window
    And  I enter t-code "<Tcode3>" in the command field
    And  I enter iDoc Number
    And  I select the "Execute" button on the Idoc page
    Then I verify iDoc status is successful
#    And  I verify "basic type" element if it has "ZCUSTOMER_NOTIFY"
    When I select the "expand Z1CUST NOTIFY header button" button on the Idoc page
    And  I select the "expand Z1CUST NOTIFY item button" button on the Idoc page
    And  I select the "Z1CUST NOTIFY item link" button on the Idoc page
    Then I verify the vehicle "vehicle make" to "<Make>"
    And  I verify the vehicle "vehicle model" to "<Model>"
    And  I verify the vehicle "vehicle year" to "<Year>"
    When I enter t-code "<Tcode3>" in the command field
    And  I enter iDoc Number
    And  I select the "Execute" button on the Idoc page
    Then I verify iDoc status is successful
#    And  I verify "basic type" element if it has "ZCUSTOMER_NOTIFY"
    When I select the "expand Z1CUST NOTIFY header button" button on the Idoc page
    And  I select the "Z1CUST NOTIFY header link" button on the Idoc page
    Then I verify the customer "First name" information of type "<Customer>"
    And  I verify the customer "Last name" information of type "<Customer>"
    And  I verify the customer "Address" information of type "<Customer>"
    And  I verify the customer "City" information of type "<Customer>"
    And  I verify the customer "order number" information of type "<Customer>"
    And  I verify the customer "Communication type" information of type "<Customer>"

    Examples:
      | Year | Make | Model | Trim  | Assembly       | ItemCode | UpdatedQuantity | Customer                    | Credit Card | InventoryMessage                         | TCode   | TCode2   | Tcode3  | Partial | DateRange |
      | 2013 | BMW  | 335i  | Sedan | 225 /45 R18 SL | 17111    | 4               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | /n MIGO | /n ME23n | /n WE02 | 2       | This Day  |

  @15997
  Scenario Outline: INT_SAP_POC_Extended Assortment_EMAIL_Partial GR for Multiple PO, email would not triggered until last PO Goods received ALM #15997)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeA>" on "Order Confirmation" page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeB>" on "Order Confirmation" page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select verbose logging
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    And  I extract the Purchase Order Numbers for each article and save it to scenario data
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<TCode>" in the command field
    Then I am brought to the page with the title "Goods Receipt Purchase Order"
    And  I verify the dropdown boxes are defaulted to "Goods Receipt" and "Purchase Order"
    And  I enter purchase order number in the entry field with "<ItemCodeABrand>"
    And  I select the "Item Okay" checkbox for each article in the purchase order
    And  I click on "Save"
    And  I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I search for the previously saved Extended Assortment PO numbers with "<ItemCodeABrand>"
    And  I select the Purchase Order History Tab for Receipt Validation
    And  I select the article document link
    And  I click on the "Output" element in po page to navigate
    And  I click on the "Display Output" element in po page to navigate
    And  I click on the "ZNFY" element in po page to navigate
    And  I click on the "Processing Log" element in po page to navigate
    And  I switch to the first iFrame
    Then I verify ZNFY processing log
    When I select the "Close" button on the Idoc page
    And  I switch back to the main window
    And  I enter t-code "<TCode>" in the command field
    Then I am brought to the page with the title "Goods Receipt Purchase Order"
    And  I verify the dropdown boxes are defaulted to "Goods Receipt" and "Purchase Order"
    And  I enter purchase order number in the entry field with "<ItemCodeBBrand>"
    And  I select the "Item Okay" checkbox for each article in the purchase order
    And  I click on "Save"
    And  I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I search for the previously saved Extended Assortment PO numbers with "<ItemCodeBBrand>"
    And  I select the Purchase Order History Tab for Receipt Validation
    And  I select the article document link
    And  I click on the "Output" element in po page to navigate
    And  I click on the "Display Output" element in po page to navigate
    And  I click on the "ZNFY" element in po page to navigate
    And  I click on the "Processing Log" element in po page to navigate
    And  I switch to the first iFrame
    Then I extract the email validation IDoc number

    Examples:
      | Year | Make   | Model | Trim | Assembly | ItemCodeA | ItemCodeABrand | ItemCodeB | ItemCodeBBrand | Customer                    | Credit Card | InventoryMessage                         | TCode    | TCode2   | DateRange |
      | 2016 | Toyota | Camry | XLE  | none     | 44286     | YOK            | 30431     | GDY            | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | /n  MIGO | /n ME23n | This Day  |

  @15958
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I add tires from secondary suppliers having Qty greater than 8 regular fitment (ALM #15958)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    And  I update quantity for item "<ItemCodeA>" to "<UpdatedQuantityA>"
    And  I update quantity for item "<ItemCodeB>" to "<UpdatedQuantityB>"
    And  I select the checkout option "none"
    Then I verify "install with appointment" option is enabled on the Checkout page
    When I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify that previous transaction does not continue to sap

    Examples:
      | Year | Make  | Model | Trim               | Assembly | ItemCodeA | ItemCodeB | UpdatedQuantityA | UpdatedQuantityB | Customer                    | DateRange | Credit Card |
      | 2014 | Honda | Civic | Coupe LX Automatic | none     | 31820     | 30192     | 4                | 5                | DEFAULT_CUSTOMER_BOPIS_VISA | This Day  | Visa Bopis  |

  @15972
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I add tires sourced from suppliers with 3 brands are added to cart for regular fitment (ALM #15972)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeB>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeC>" of type "none" to my cart and "View shopping Cart"
    And  I update quantity for item "<ItemCodeA>" to "<UpdatedQuantity>"
    And  I update quantity for item "<ItemCodeB>" to "<UpdatedQuantity>"
    And  I select the checkout option "none"
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify that previous transaction does not continue to sap

    Examples:
      | ZipCode | Store  | Year | Make  | Model | Trim               | Assembly | ItemCodeA | ItemCodeB | ItemCodeC | UpdatedQuantity | Customer                    | Credit Card | DateRange |
      | 80601   | COD 36 | 2014 | Honda | Civic | Coupe LX Automatic | none     | 31820     | 30192     | 19789     | 2               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | This Day  |

  @15980
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier and wheels not avaialble in my store added with regular fitment (ALM #15980)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "none"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify that previous transaction does not continue to sap

    Examples:
      | ZipCode | Store  | Year | Make  | Model | Trim               | Assembly | ItemCodeA | ItemCodeB | Customer                    | DateRange | Credit Card |
      | 80601   | COD 36 | 2014 | Honda | Civic | Coupe LX Automatic | none     | 30192     | 78001     | DEFAULT_CUSTOMER_BOPIS_VISA | This Day  | Visa Bopis  |

  @15981
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier and no fitment is selected (ALM #15981)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "none"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    And  I store the order number
    And  I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify that previous transaction does not continue to sap

    Examples:
      | ItemCode | Customer                    | Customer                    | DateRange |
      | 17595    | DEFAULT_CUSTOMER_BOPIS_VISA | DEFAULT_CUSTOMER_BOPIS_VISA | This Day  |

  @15982
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in My Store and Nearby Stores (ALM #15982)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "none"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify that previous transaction does not continue to sap

    Examples:
      | ZipCode | Year | Make  | Model | Trim               | Assembly | ItemCode | Customer                    | Credit Card | DateRange |
      | 80601   | 2014 | Honda | Civic | Coupe LX Automatic | none     | 25454    | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | This Day  |
      | 80601   | 2014 | Honda | Civic | Coupe LX Automatic | none     | 34362    | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | This Day  |

  @15983
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tires available in secondary supplier added with quantity great than 8 for Staggered fitment (ALM #15983)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire"
    And  I add item "<ItemCodeA>" of type "sets" to my cart and "View shopping Cart"
    And  I update quantity for item "<ItemCodeA>" to "<QuantityA>"
    And  I update quantity for item "<ItemCodeB>" to "<QuantityB>"
    And  I select the checkout option "none"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify that previous transaction does not continue to sap

    Examples:
      | ZipCode | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | QuantityA | QuantityB | Customer                    | Credit Card | DateRange |
      | 80601   | 2010 | Chevrolet | Corvette | Base | none     | 30018     | 30019     | 5         | 4         | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | This Day  |

  @15961
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier and wheels not avaialble in my store added with Staggered fitment (ALM #15961)
    When I set baseUrl to "hybris"
    And  I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire"
    And  I add item "<ItemCodeA>" of type "sets" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "none"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I store the order number
    When I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for transaction number on interface monitor page
    And  I select the date range "<DateRange>"
    And  I complete the search
    Then I verify that previous transaction does not continue to sap

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | Customer                    | Credit Card | DateRange |
      | 2010 | Chevrolet | Corvette | Base | none     | 30018     | 74964     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | This Day  |

  @eaRopisOrderValidation
  Scenario Outline: Extended Assortment Order validation for ROPIS
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I load start time, end time, store number, web order, article, quantity
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time in posdm
    And  I enter store number for DT orders in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number in posdt and save it to scenario data
    And  I expand the first line item and save the purchase order number
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify Head data tab is open
    And  I verify if item Overview is open
    And  I verify "<POType>" matches the "PO Type" displayed information
    When I click on "Partners"
    Then I check for element with text "OA"
    And  I check for element with text "VN"
    And  I check for element with text "GS"
    When I click on "Texts"
    Then I check for "name" element with text from scenario data
    And  I check for "Article" element with text from scenario data
    And  I check for "Quantity" element with text from scenario data
    When I enter t-code "<TCode 2>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on "Purchase Order History"
    Then I verify the article document matches the previously saved number
    When I select the article document link
    And  I click on "Output"
    And  I click on "Display outputs"
    And  I click on "ZNFY" in po page
    And  I click on element with title attribute value "Processing log"
    And  I switch to the first iFrame in SAP
    Then I extract the email validation IDoc number

    Examples:
      | TCode   | TCode 1       | Transaction Type | TCode 2 | Folder Name         | File Name | Sheet name | row number | POType         |
      | /nme23n | /n/posdw/mon0 | 1014             | /nmigo  | Extended Assortment | ea.xlsx   | scenario 1 | 2          | Store Merch PO |

  @eaBopisOrderValidation
  Scenario Outline: Extended Assortment Order validation for BOPIS
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I load start time, end time, store number, web order, article, quantity
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter BOPIS web order number into field name with label "Transaction Number"
    And  I enter store number for DT orders in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number in posdt and save it to scenario data
    And  I expand the first line item and save the purchase order number
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify Head data tab is open
    And  I verify if item Overview is open
    And  I verify "<POType>" matches the "PO Type" displayed information
    When I click on "Partners"
    Then I check for element with text "OA"
    And  I check for element with text "VN"
    And  I check for element with text "GS"
    When I click on "Texts"
    Then I check for "name" element with text from scenario data
    And  I check for "Article" element with text from scenario data
    And  I check for "Quantity" element with text from scenario data
    When I enter t-code "<TCode 2>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on "Purchase Order History"
    Then I verify the article document matches the previously saved number
    When I select the article document link
    And  I click on "Output"
    And  I click on "Display outputs"
    And  I click on "ZNFY" in po page
    And  I click on element with title attribute value "Processing log"
    And  I switch to the first iFrame in SAP
    Then I extract the email validation IDoc number

    Examples:
      | TCode   | TCode 1       | Transaction Type | TCode 2 | Folder Name         | File Name | Sheet name | row number | POType         |
      | /nme23n | /n/posdw/mon0 | 1015             | /nmigo  | Extended Assortment | ea.xlsx   | scenario 2 | 2          | Store Merch PO |