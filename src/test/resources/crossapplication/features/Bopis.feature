@crossapplication_bopis
Feature: Cross Application BOPIS tests

  @dt
  @web
  @sendWebOrderNumberToExcel
  @sendBopisWebOrderNumberToExcelWithAppointment
  Scenario Outline: Bopis Single Card Payment No Other Recipient - Send Web Order Number to Excel - With Appointment
  """ Will fail if ROPIS/BOPIS option is not turned on. Will fail if there are no available appointments at the store. """
    When I change to the integrated test store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "<FitmentType>" fitment
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I enter customer information for "<Customer>"
    And  I enter "valid" customer phone number: "random"
    And  I click on the "Place Order" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I save the order number to the "top" of the "<Column Name>" column of the "<File Tab>" tab of the "<Excel File>" excel file

    Examples:
      | Year | Make  | Model | Trim         | Assembly       | FitmentType   | ProductName                 | ItemCode | Quantity | Checkout         | Customer                          | Credit Card      | Customer            | Excel File                      | Column Name           | File Tab  |
      | 2016 | Ram   | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | non-staggered | DISCOVERER ATP              | 15949    | 4        | with appointment | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis       | DEFAULT_CUSTOMER_AZ | WEBORDERS_LEGACY_STOREPOS_EXCEL | WebOrderID_VISA_FET   | WebOrders |
      | 2012 | Honda | Civic | Coupe DX     | None           | non-staggered | CONTROL CONTACT TOURING A/S | 19661    | 4        | with appointment | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis | DEFAULT_CUSTOMER_AZ | WEBORDERS_LEGACY_STOREPOS_EXCEL | WebOrderID_MasterCard | WebOrders |

  @dt
  @web
  @sendWebOrderNumberToExcel
  @sendBopisWebOrderNumberToExcelWithoutAppointment
  Scenario Outline: Bopis Single Card Payment No Other Recipient - Send Web Order Number to Excel - Without Appointment
  """ Will fail if ROPIS/BOPIS option is not turned on. Will fail if there are no available appointments at the store. """
    When I change to the integrated test store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "<FitmentType>" fitment
    When I set quantity to "<Quantity>" and add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    And  I enter customer information for "<Customer>"
    And  I enter "valid" customer phone number: "random"
    And  I click on the "Place Order" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I save the order number to the "top" of the "<Column Name>" column of the "<File Tab>" tab of the "<Excel File>" excel file

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentType   | ProductName                 | ItemCode | Quantity | Checkout            | Reason                                  | Customer                        | Credit Card            | Customer            | Excel File                      | Column Name               | File Tab  |
      | 2012 | Honda | Civic | Coupe DX | None     | non-staggered | CONTROL CONTACT TOURING A/S | 19661    | 4        | without appointment | Not sure of my availability             | DEFAULT_CUSTOMER_BOPIS_CC1      | CarCareOne Bopis       | DEFAULT_CUSTOMER_AZ | WEBORDERS_LEGACY_STOREPOS_EXCEL | WebOrderID_CC1            | WebOrders |
      | 2012 | Honda | Civic | Coupe DX | None     | non-staggered | CONTROL CONTACT TOURING A/S | 19661    | 4        | without appointment | Make an appointment at a later time     | DEFAULT_CUSTOMER_BOPIS_CC1_2    | CarCareOne_2 Bopis     | DEFAULT_CUSTOMER_AZ | WEBORDERS_LEGACY_STOREPOS_EXCEL | WebOrderID_CC1_2          | WebOrders |
      | 2012 | Honda | Civic | Coupe DX | None     | non-staggered | CONTROL CONTACT TOURING A/S | 19661    | 4        | without appointment | These items are for multiple vehicles   | DEFAULT_CUSTOMER_BOPIS_DISCOVER | Discover Bopis         | DEFAULT_CUSTOMER_AZ | WEBORDERS_LEGACY_STOREPOS_EXCEL | WebOrderID_Discover       | WebOrders |
      | 2012 | Honda | Civic | Coupe DX | None     | non-staggered | Silver Edition III          | 29935    | 4        | without appointment | My preferred date/time is not available | DEFAULT_CUSTOMER_BOPIS_AMEX     | American Express Bopis | DEFAULT_CUSTOMER_AZ | WEBORDERS_LEGACY_STOREPOS_EXCEL | WebOrderID_AMEX_Promotion | WebOrders |
