@checkout
@dtd
Feature: Checkout

  Background:
    Given I go to the homepage

  @web
  @dtd
  @6180
  @envFeeValidation
  Scenario Outline: Verify the environmental fee for an item appears in the shopping cart (ALM 6180)
  """TODO: Only works in QA (AZ), STG (Portland) stores don't have state requirement for env fee on tires."""
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the "Environmental Fee" label present on the shopping cart page
    And  I verify the environment fee amount "<Env Fee>" on the shopping cart page

    Examples:
      | ItemCode | ProductName        | Env Fee |
      | 26895    | Extreme Contact DW | 8.00    |

  @web
  @dtd
  @6178
  @tiresCertificatesValidation
  Scenario Outline: Verify the tires certificates fee of an item appears in the shopping cart (ALM 6178)
  """TODO - Reactivate the line below 'I confirm shipping options' when the additional shipping options are available/
    ready in all environments
    | 26895    | R265        | 117.00   |
    Originally = |26895    |EXTREME CONTACT DW|156.00   |"""
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the certificate service fee amount "<Cert Fee>" on the shopping cart page

    Examples:
      | ItemCode | ProductName        | Cert Fee |
      | 26895    | Extreme Contact DW | 165.00   |

  @dtd
  @web
  @8775
  @9635
  @9634
  @checkoutRegression  
  @checkoutRegression_APO
  Scenario Outline: Checkout with major credit cards using Homepage Keyword Search US, APO, FPO (ALM #8775,9635,9634)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |

    Examples:
      | ItemCode | ProductName  | Checkout | Customer            | ShippingOption         | Credit Card      |
      | 34302    | Defender A/S | default  | default_customer_az | Ground                 | MasterCard Bopis |
      | 34302    | Defender A/S | default  | default_customer_hi | Ground                 | Visa             |
      | 34302    | Defender A/S | default  | apo_customer        | Priority Mail Military | American Express |
      | 34302    | Defender A/S | default  | fpo_customer        | Priority Mail Military | Discover         |

  @dtd
  @8775
  @mobile
  @checkoutRegression
  @checkoutRegression_APO
  Scenario Outline: Mobile - Checkout with major credit cards using Homepage Keyword Search US, APO, FPO (ALM #8775)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |

    Examples:
      | ItemCode | ProductName  | Checkout | Customer            | ShippingOption         | Credit Card      |
      | 34302    | Defender A/S | default  | default_customer_az | Ground                 | MasterCard Bopis |
      | 34302    | Defender A/S | default  | default_customer_hi | Ground                 | Visa             |
      | 34302    | Defender A/S | default  | apo_customer        | Priority Mail Military | American Express |
      | 34302    | Defender A/S | default  | fpo_customer        | Priority Mail Military | Discover         |

  @dtd
  @web
  @8775
  @checkoutRegression
  Scenario Outline: Checkout with major credit cards using Homepage Keyword Search CAN (ALM #8775)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "Next Day Air" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I confirm that taxes are listed on the "order" page
    And  I store the order number

    Examples:
      | ItemCode | ProductName | Checkout | Customer             | Credit Card      |
      | 26899    | Pro Contact | default  | default_customer_can | American Express |
      | 26899    | Pro Contact | default  | default_customer_can | Discover         |
      | 26899    | Pro Contact | default  | default_customer_can | MasterCard Bopis |
      | 26899    | Pro Contact | default  | default_customer_can | Visa             |

  @dtd
  @8775
  @mobile
  @checkoutRegression
  Scenario Outline: Mobile - Checkout with major credit cards using Homepage Keyword Search CAN (ALM #8775)
    When I do a free text search for "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "Next Day Air" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I confirm that taxes are listed on the "order" page
    When I store the order number

    Examples:
      | ItemCode | ProductName | Checkout | Customer             | Credit Card      |
      | 26899    | Pro Contact | default  | default_customer_can | American Express |
      | 26899    | Pro Contact | default  | default_customer_can | Discover         |
      | 26899    | Pro Contact | default  | default_customer_can | MasterCard Bopis |
      | 26899    | Pro Contact | default  | default_customer_can | Visa             |

  @dtd
  @web
  @8775
  @checkoutRegression
  @checkoutRegression_DifferentShippingAndBillingAddress_FreeTextSearch
  Scenario Outline: Checkout with different shipping and billing address using Homepage Keyword Search (ALM #8775)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Shipping Customer>" and continue to next page
    And  I select shipping option: "Ground" as "<Shipping Customer>"
    And  I extract the "sales tax" on the checkout page for "<Shipping Customer>"
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Payment Customer>"
    And  I place the order for "<Payment Customer>"
    Then I am brought to the order confirmation page
    And  I verify the sales tax on order confirmation page matches with "checkout" sales tax for "<Shipping Customer>"
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    When I store the order number
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | ItemCode | ProductName        | Checkout | Shipping Customer   | Payment Customer     |
      | 29935    | Silver Edition III | default  | default_customer_az | default_customer_can |

  @dtd
  @8775
  @mobile
  @checkoutRegression
  @checkoutRegression_DifferentShippingAndBillingAddress_FreeTextSearch
  Scenario Outline: Mobile - Checkout with different shipping and billing address using Homepage Keyword Search (ALM #8775)
    When I do a free text search for "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Shipping Customer>" and continue to next page
    And  I select shipping option: "Ground" as "<Payment Customer>"
    And  I extract the "sales tax" on the checkout page for "<Shipping Customer>"
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Payment Customer>"
    And  I place the order for "<Payment Customer>"
    Then I am brought to the order confirmation page
    And  I verify the sales tax on order confirmation page matches with "checkout" sales tax for "<Shipping Customer>"
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I store the order number
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | ItemCode | ProductName        | Checkout | Shipping Customer   | Payment Customer     |
      | 29935    | Silver Edition III | default  | default_customer_az | default_customer_can |

  @dtd
  @web
  @8775
  @checkoutRegression
  @coreScenarioCheckout
  Scenario Outline: Checkout with Canadian customer with upper or lower case zip code and no spaces using Homepage Keyword Search (ALM #8775)
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    Then I confirm the shipping options are: "<Shipping Method>"
    And  I verify "Ground - Free" does not appear in the Delivery Method section
    When I select shipping option: "<Shipping Method>" as "<Customer>"
    And  I extract the "sales tax" on the checkout page for "<Customer>"
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the sales tax on order confirmation page matches with "checkout" sales tax for "<Customer>"
    And  I store the order number

    Examples:
      | ItemCode | ProductName        | Checkout | Customer               | Shipping Method |
      | 26895    | Extreme Contact DW | default  | UPPERCASE_CUSTOMER_CAN | Next Day Air    |
      | 26895    | Extreme Contact DW | default  | lowercase_customer_can | Ground          |

  @dtd
  @8775
  @mobile
  @checkoutRegression
  Scenario Outline: Mobile - Checkout with Canadian customer with upper or lower case zip code and no spaces using Homepage Keyword Search (ALM #8775)
    When I do a free text search for "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "Next Day Air" as "<Customer>"
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I confirm that taxes are listed on the "order" page
    When I store the order number

    Examples:
      | ItemCode | ProductName | Checkout | Customer               |
      | 26899    | Pro Contact | default  | UPPERCASE_CUSTOMER_CAN |
      | 26899    | Pro Contact | default  | lowercase_customer_can |

  @dtd
  @web
  @mailinator
  @tireOrderEmailValidation
  Scenario Outline: Tire size order email validation with Mailinator
  """TODO: ALM number pending"""
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I store the order number
    And  I login to email for the "<Customer>"
    Then I confirm customer receives an email for the "Order Confirmation"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | Checkout | Customer                  | ShippingOption |
      | 2012 | Honda | Civic | Coupe DX | none     | 28704    | default  | email_validation_customer | Ground         |

  @dtd
  @web
  @core
  @8579
  @defect
  @coreScenarioCheckout
  @checkoutFromReviewComparisonPageShippingUPS
  Scenario Outline: Checkout from Review Comparison Page by Shipping UPS (ALM # 8579)
  """ Fails for staggered fitment due to Defect 11941. The selected product from PLP is not displayed
  on the Compare tire reviews page. Also Defect 12633: Products not loading."""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I click compare tire reviews link
    Then I verify the compare tire reviews page displays
    When I click add to cart for selected tire on compare tire reviews page
    Then I verify the item added to your cart popup displays
    And  I verify item added to your cart popup contains selected tires
    When I click on the "View shopping Cart" link
    Then I see selected products on the cart page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I store the order number

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | FitmentOption | Checkout | Customer              | ShippingOption |
      | 2010 | Chevrolet | Corvette | Base     | none     | tire          | default  | default_customer_can  | Next Day Air   |
      | 2010 | Chevrolet | Corvette | Base     | none     | tire          | default  | default_customer_ca_2 | Ground         |
      | 2012 | Honda     | Civic    | Coupe DX | none     | tire          | default  | default_customer_can  | Next Day Air   |
      | 2012 | Honda     | Civic    | Coupe DX | none     | tire          | default  | default_customer_ca_2 | Ground         |

  @dtd
  @8575
  @web
  Scenario Outline: HYBRIS_213_CHECKOUT_Checkout from Product Comparison Page by Paypal Payment (ALM #8575)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "Cooper Tires" menu option
    And  I select shop all from the Product Brand page
    And  I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select the default shipping option as "<Customer>"
    And  I select the "<Shipping>" payment option
    And  I continue on to PayPal checkout
    And  I log into paypal as "<Customer>"
    And  I continue with the paypal payment
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I confirm that taxes are listed on the "order" page
    And  I store the order number

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Checkout | Customer           | Shipping |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | paypal_customer_az | paypal   |

  @dtd
  @8577
  @web
  Scenario Outline: HYBRIS_215_CHECKOUT_Checkout from Review Comparison Page with Shipping Address Change (ALM # 8577)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I click compare tire reviews link
    Then I verify the compare tire reviews page displays
    When I click add to cart for the first tire on the compare tire reviews page
    And  I click on the "View shopping Cart" link
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I select edit shipping details
    And  I set "Address Line 1" to "<NewAddressLine1>"
    And  I set "Zip / Postal Code" to "<NewZip>"
    And  I submit the updated address information and "check for AVS popup"
    And  I select the default shipping option as "<Customer>"
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "<NewAddressLine1>" is listed on the order confirmation page
    And  I verify "<NewZip>" is listed on the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Checkout | Customer            | ShippingOption | NewAddressLine1     | NewZip     |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | default_customer_az | Ground         | 2240 W Camelback Rd | 85015-3445 |

  @web
  @dtd
  @8227
  @regression
  @checkoutRegression
  Scenario Outline: HYBRIS_215_CHECKOUT_Checkout from Review Comparison Page with Shipping Address Change (ALM # 8227)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I go to the homepage
    And  I select mini cart and "View Cart"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I select the "<PaymentType>" payment option
    When I select paypal checkout
    And  I switch to "Paypal" window
    And  I log into paypal as "<Customer>"
    And  I continue with the paypal payment
    And  I switch to main window
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | ItemCode | ProductName        | Checkout | Customer           | PaymentType | ShippingOption |
      | 29935    | Silver Edition III | default  | PAYPAL_CUSTOMER_OH | paypal      | Ground         |

  @dtd
  @web
  @8653
  @mobile
  @smoketest
  @regression
  @ordersSmoke
  @checkoutRegression
  Scenario Outline: Checkout with Credit card using Homepage Keyword Search (ALM #8653)
    When I do a free text search for "<ProductName>" and hit enter
    And  I select the "First" product result image on "PLP" page
    Then I should see product detail page with "<ProductName>"
    And  I set first available size options on PDP page
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping and payment info with "<CardType>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I verify the required fees and add-ons sections are expanded
    Then I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    And  I store the order number

    Examples:
      | ProductName        | Checkout | CardType | Customer            |
      | Silver Edition III | default  | Visa     | default_customer_az |

  @dtd
  @web
  @8654
  @mobile
  @smoketest
  @regression
  @ordersSmoke
  @checkoutRegression
  Scenario Outline: Checkout with Paypal using Vehicle Search via My Vehicles (ALM #8654)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select a fitment option "wheel"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select the default shipping option as "<Customer>"
    Then I verify the "order total" on the checkout page
    When I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I select the "<Checkout>" payment option
    And  I select paypal checkout
    And  I switch to "Paypal" window
    And  I log into paypal as "<Customer>"
    And  I continue with the paypal payment
    And  I switch to main window
    Then I am brought to the order confirmation page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    And  I store the order number

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | Checkout | Customer           |
      | 2012 | Honda | Civic | Coupe DX | none     | 16293    | paypal   | paypal_customer_az |

  @dt
  @at
  @web
  @12806
  Scenario Outline: Cart checkout validate order summary on checkout pages (ALM#12806)
    When I change to the default store
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item with message "Available today" to my cart
    Then I verify the item added to your cart popup displays
    And  I verify "add product to cart modal Popup" has the text of "Continue Shopping" in PLP
    And  I verify "add product to cart modal Popup" has the text of "View Cart" in PLP
    When I click View Cart button
    Then I verify the required fees and add-ons sections are expanded
    And  I verify "Shopping Cart page" has the text of "Shopping cart" in PLP
    When I extract "Address" from shopping cart as "address"
    And  I extract "Quantity" from shopping cart as "quantity"
    And  I extract "ProductName" from shopping cart as "product name"
    And  I extract "Item Price" from shopping cart as "item price"
    And  I extract "Subtotal" from shopping cart as "subtotal"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Installation Fee" from shopping cart as "installation fee"
    And  I extract "Disposal Fee" from shopping cart as "disposal fee"
    And  I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Address" with "address" from "cart" page
    And  I verify extracted "Quantity" with "quantity" from "cart" page
    And  I verify extracted "ProductName" with "product name" from "cart" page
    And  I verify extracted "Item Price" with "item price" from "cart" page
    And  I verify extracted "Subtotal" with "subtotal" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Installation Fee" with "installation fee" from "cart" page
    And  I verify extracted "Disposal Fee" with "disposal fee" from "cart" page
    And  I verify extracted "Enviro" with "environmental fee" from "cart" page
    When I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Address" with "address" from "cart" page
    And  I verify extracted "Quantity" with "quantity" from "cart" page
    And  I verify extracted "ProductName" with "product name" from "cart" page
    And  I verify extracted "Item Price" with "item price" from "cart" page
    And  I verify extracted "Subtotal" with "subtotal" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Installation Fee" with "installation fee" from "cart" page
    And  I verify extracted "Disposal Fee" with "disposal fee" from "cart" page
    And  I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | Honda | Civic | Coupe DX | none     |

  @dtd
  @web
  @mobile
  @16388
  Scenario Outline: Validate TPMS Options Details on Confirmation Page After Order is Placed Successfully_DTD (ALM #16388)
  """ Will fail due to TPMS option showing singular (i.e. TPMS Sensor) on the confirmation page.
        It should be plural (i.e. TPMS Sensors). Defect 10623. """
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I select TPMS option "<TPMS>"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify selected TPMS option "<TPMS>" is displayed on order confirmation page
    And  I store the order number

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | Checkout | Customer            | ShippingOption | Credit Card      | TPMS              |
      | 2012 | Honda | Civic | Coupe DX | none     | 26899    | default  | default_customer_az | Ground         | MasterCard Bopis | TPMS Rebuild Kits |
      | 2012 | Honda | Civic | Coupe DX | none     | 26899    | default  | default_customer_az | Ground         | MasterCard Bopis | TPMS Sensors      |
      | 2012 | Honda | Civic | Coupe DX | none     | 26899    | default  | default_customer_az | Ground         | MasterCard Bopis | Valve Stems       |

  @dtd
  @web
  @6945
  @mobile
  @defect
  @nexusTax
  Scenario Outline: HYBRIS_CHECKOUT_NEXUS_AVS_Verify auto corrected zip code for valid shipping address_DTD (ALM#6945)
  """ Will fail validating Zip + 4 upon returning to edit the form: Defect# 11538. """
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I select the checkout option "default"
    Then I verify "Checkout" page is displayed
    When I enter shipping info as "<Customer1>"
    Then I verify "City, State" values for "<Customer1>" are now pre-populated
    When I set "Country" to "<CanadaCountry>"
    Then I verify "Address Line 1, Postal Code, City, State" values for shipping details reset to null
    When I set "Country" to "<USCountry>"
    And  I enter shipping info as "<Customer2>"
    Then I verify "City, State" values for "<Customer2>" are now pre-populated
    When I set "Zip / Postal Code" to "<PostalZip>"
    Then I verify "City, State, Country" values for "<Customer3>" are now pre-populated
    When I set "Country" to "<USCountry>"
    And  I enter shipping info as "<Customer4>"
    And  I submit the updated address information and "check for AVS popup"
    Then I verify auto corrected zip code for "<Customer4>"
    When I select edit shipping details
    Then I verify zip plus 4 is displayed on shipping details page for "<Customer4>"

    Examples:
      | ItemCode | Customer1           | Customer2           | Customer3            | Customer4           | CanadaCountry | USCountry     | PostalZip |
      | 29935    | default_customer_tx | default_customer_az | default_customer_can | default_customer_ga | Canada        | United States | T1Y 1H3   |
      | 29935    | default_customer_la | default_customer_oh | default_customer_can | apo_customer        | Canada        | United States | T1Y 1H3   |

  @dtd
  @web
  @C003
  @15549
  @mobile
  @nexusFt
  @nexusTax
  @nexusTaxSit
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Validate the Environmental Fee and Taxes displayed when products are added to the cart without fitment_DTD (ALM#15549)
    When I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    Then I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeB>" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "Heat Cycling" fee for item
    And  I select the optional "Studding" fee for item
    And  I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I take page screenshot
    And  I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I take page screenshot
    And  I verify the required fees and add-ons sections are expanded
    Then I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Order Confirmation" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeB>" product on "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | ItemCodeA | ItemCodeB | Checkout | Customer            | ShippingOption | Credit Card |
      | 10937     | 40297     | default  | default_customer_az | Next Day Air   | Visa        |