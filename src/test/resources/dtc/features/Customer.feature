@customer
Feature: Customer

  Background:
    Given I go to the homepage

  @dtd
  @web
  @6999
  @mobile
  @smoketest
  @regression
  @customerSmoke
  @customerRegression
  Scenario Outline: Wheel size order email validation (ALM #6999)
    When I navigate to Shop By "Size"
    And  I select the "Wheels" subheader on the fitment grid
    And  I select the "<Diameter>" fitment grid option
    And  I click on the "VIEW WHEELS" button
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I store the order number
    When I login to email for the "<Customer>"
    And  I switch to "Outlook" window
    Then I confirm customer receives an email for the "Order Confirmation"

    Examples:
      | Diameter | Checkout | Customer                  | ShippingOption |
      | 15       | default  | email_validation_customer | Ground         |

  @dt
  @at
  @dtd
  @web
  @9905
  Scenario Outline: HYBRIS_Email_Verification_Is_Case_Sensitive_Appointment_TC_01 (ALM #9905)
    When I open the "SERVICES" navigation link
    And  I click the "SCHEDULE APPOINTMENT" menu option
    And  I select service option(s): "<ServiceOption>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    Then I verify selected service option(s): "<ServiceOption>" is displayed on Service Appointment page
    And  I verify default store on the customer details appointment page
    When I select first available appointment date
    And  I extract date and time for validation
    And  I click continue for appointment customer details page
    Then I verify date and time on the customer details appointment page
    When I select "Schedule Appointment" after entering customer information for "<Customer>"
    Then I verify the Appointment Confirmation email address is "LOWERCASE"
    And  I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"
    And  I should see my previously selected store, date and time, in the appointment details section
    And  I store the order number
    When I login to email for the "<Customer>"
    Then I confirm customer receives an email for the "Order Confirmation"

    Examples:
      | ServiceOption      | Customer            |
      | Winter Tire Change | DEFAULT_CUSTOMER_GA |


  @at
  @dt
  @web
  @8276
  @smoketest
  @customerSmoke
  Scenario Outline: HYBRIS_CREATE_Service_Appointment_HomePage_Header_Flow_No_Product (ALM #8276)
    When I change to the default store
    And  I open the "APPOINTMENTS" navigation link
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I click continue for appointment customer details page
    And  I select "Schedule Appointment" after entering customer information for "<Customer>"
    And  I store the order number

    Examples:
      | ServiceOptions  | Customer                  |
      | Tire Inspection | EMAIL_VALIDATION_CUSTOMER |

  @at
  @dt
  @web
  @9613
  @9617
  Scenario Outline: HYBRIS_CREATE_Service_Appointment_HomePage_Command_Button_Flow_No_Product (ALM #9613, 9617)
    When I schedule an appointment for my current store
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I click continue for appointment customer details page
    And  I select "Schedule Appointment" after entering customer information for "<Customer>"
    And  I store the order number
    And  I login to email for the "<Customer>"
    Then I confirm customer receives an email for the "Appointment Confirmation"

    Examples:
      | ServiceOptions     | Customer                  |
      | Winter Tire Change | EMAIL_VALIDATION_CUSTOMER |

  @at
  @dt
  @web
  @9614
  Scenario Outline: HYBRIS_CREATE_Service_Appointment_With_Address_HomePage_Command_Button_Flow_No_Product (ALM #9614)
    When I open the "SERVICES" navigation link
    And  I click the "Schedule an appointment" menu option
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I click continue for appointment customer details page
    And  I click reserve after entering customer information, including the address, for "<Customer>"
    Then I store the order number
    When I login to email for the "<Customer>"
    And  I confirm customer receives an email for the "Appointment Confirmation"

    Examples:
      | ServiceOptions | Customer                  |
      | Flat Repair    | EMAIL_VALIDATION_CUSTOMER |

  @at
  @dt
  @web
  @9615
  Scenario Outline: HYBRIS_CREATE_Service_Appointment_HomePage_MyStore_Change_Store_Flow_No_Product_AT (ALM #9615)
    When I search for store within "25" miles of "92324"
    And  I select "Appointment" for store #"2" in the location results
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I click continue for appointment customer details page
    And  I select "Schedule Appointment" after entering customer information for "<Customer>"
    And  I store the order number
    And  I login to email for the "<Customer>"
    Then I confirm customer receives an email for the "Appointment Confirmation"

    Examples:
      | ServiceOptions            | Customer                  |
      | Tire Rotation and Balance | EMAIL_VALIDATION_CUSTOMER |

  @at
  @dt
  @web
  @8274
  @regression
  @customerRegression
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_Product CompareTireReviewsChart Link PLP Staggered (ALM#8274)
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    And  I verify Customer Rating and reviews are displayed for listed products on PLP result page
    And  I verify compare tire reviews link is displayed for products that have reviews
    When I click compare tire reviews link
    Then I verify the compare tire reviews page displays
    When I navigate back to previous page
    Then I verify the product list page is displayed having "staggered" fitment
    When I select "FRONT" staggered tab on PLP result page
    Then I verify Customer Rating and reviews are displayed for listed products on PLP result page
    And  I verify compare tire reviews link is displayed for products that have reviews
    When I navigate back to previous page
    Then I verify the product list page is displayed having "staggered" fitment
    When I select "REAR" staggered tab on PLP result page
    Then I verify Customer Rating and reviews are displayed for listed products on PLP result page
    And  I verify compare tire reviews link is displayed for products that have reviews

    Examples:
      | Year | Make      | Model    | Trim | Assembly |
      | 2010 | Chevrolet | Corvette | Base | none     |

  @at
  @dt
  @web
  @6893
  @regression
  @customerRegression
  Scenario: HYBRIS_CUSTOMER_REVIEWS_Store Solicited Review form validation desktop (ALM #6893)
  """TODO:  Verify Terms and Conditions will fail for America's Tire stores due to defect 9508."""
    When I navigate to Store Review page
    Then I verify store logo is displayed in store info section
    And  I verify store name, address, and phone number are displayed in store info section
    And  I verify "Store Hours" are displayed in store info section
    And  I verify store review page header text
    And  I verify "All fields required unless marked as optional" displayed
    And  I verify store ratings headings
    And  I verify "Would You Recommend This Store" displayed with YES NO buttons
    And  I verify "Comments About Your Experience" displayed
    And  I verify "Comments" text area displayed with "10000" character limit
    And  I verify "Yes I agree to Terms and Conditions" text displayed with checkbox and Terms and Conditions link
    When I click "Terms and Conditions" link
    Then I verify "CUSTOMER RATINGS AND REVIEW TERMS OF USE" pop-up is displayed
    When I close "CUSTOMER RATINGS AND REVIEW TERMS OF USE" pop-up
    Then I verify the "Submit Review" button is displayed
    When I click "Submit Review" button
    Then I verify the "Please provide feedback for the following" pop-up is displayed
    When I close the "Please provide feedback for the following" pop-up

  @at
  @dt
  @web
  @7250
  @regression
  @customerRegression
  Scenario: HYBRIS_CUSTOMER_REVIEWS_Store Solicited Review Submission (ALM #7250)
  """ The "verify error messages on page" step will fail in IE or Safari after entering text in the Comments field,
        clicking Submit, and closing pop-up window. The !Required label is retained in IE and Safari - defect 9654 """
    When I navigate to Store Review page
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "store"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "store"
    When I select "random" stars for "Employee Knowledge/Friendliness" store rating category
    And  I select "random" stars for "Store Cleanliness" store rating category
    And  I select "random" stars for "Overall Rating" store rating category
    And  I "select" "Yes, I agree to Discount Tire's Terms & Conditions" checkbox
    And  I enter "test comment" into the Comments text box
    And  I click "Submit Review" button
    Then I verify "Store Recommendation" is the only item listed in the "Please provide feedback for the following" pop-up
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "store"
    When I select "random" for "Would You Recommend" question
    And  I click "Submit Review" button
    Then I verify completed review message displayed for "store"

  @at
  @dt
  @dtd
  @web
  @6891
  @8266
  @regression
  @customerRegression
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_Product Solicited Review Submission (ALM #6891, #8266-staggered)
  """ The "verify error messages on page" step will fail in IE or Safari after entering valid value(s) in the Zip Code
      and/or Miles Driven  field(s), clicking Submit, and closing pop-up window. The !Required label is retained
      in IE and Safari - defect 9656 """
    When I navigate to Product Review page with vehicle details for "<Year>" "<Make>" "<Model>" "<ProductId>"
    Then I verify product review page header text with product name "<ProductName>"
    When I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    And  I verify "Pros" text area displayed with "150" character limit
    And  I verify "Cons" text area displayed with "150" character limit
    And  I verify "Comments" text area displayed with "10000" character limit
    When I select "random" stars for "RIDE COMFORT" product rating category
    And  I select "random" stars for "CORNERING & STEERING" product rating category
    And  I select "random" stars for "RIDE NOISE" product rating category
    And  I select "random" stars for "TREAD LIFE" product rating category
    And  I select "random" stars for "DRY TRACTION" product rating category
    And  I select "random" stars for "WET TRACTION" product rating category
    And  I select "random" stars for "WINTER WEATHER TRACTION" product rating category
    And  I enter "valid" Driving zip code "86001"
    And  I enter "15000" Miles driven on tires
    And  I enter "test comment" into the Comments text box
    And  I select "random" from "Driving conditions" dropdown list
    And  I select "random" from "Type of driving" dropdown list
    And  I "select" "Yes, I agree to Discount Tire's Terms & Conditions" checkbox
    And  I click "Submit Review" button
    Then I verify "Product Recommendation" is the only item listed in the "Please provide feedback for the following" pop-up
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    When I select "YES" for "Would You Recommend" question
    And  I click "Submit Review" button
    Then I verify completed review message displayed for "product"
    When I navigate to Product Review page with vehicle Id for "<VehicleId>" "<ProductId>"
    Then I verify product review page header text with product name "<ProductName>"
    When I select "random" stars for "RIDE COMFORT" product rating category
    And  I select "random" stars for "CORNERING & STEERING" product rating category
    And  I select "random" stars for "RIDE NOISE" product rating category
    And  I select "random" stars for "TREAD LIFE" product rating category
    And  I select "random" stars for "DRY TRACTION" product rating category
    And  I select "random" stars for "WET TRACTION" product rating category
    And  I select "random" stars for "WINTER WEATHER TRACTION" product rating category
    And  I enter "valid" Driving zip code "86001"
    And  I enter "15000" Miles driven on tires
    And  I enter "test comment" into the Comments text box
    And  I select "random" from "Driving conditions" dropdown list
    And  I select "random" from "Type of driving" dropdown list
    And  I select "NO" for "Would You Recommend" question
    And  I "select" "Yes, I agree to Discount Tire's Terms & Conditions" checkbox
    And  I click "Submit Review" button
    Then I verify completed review message displayed for "product"

    Examples:
      | Year | Make      | Model    | ProductId | ProductName          | VehicleId |
      | 2012 | Honda     | Civic    | 26899     | Pro Contact          | 105163    |
      | 2010 | Chevrolet | Corvette | 37068     | G-Force Sport Comp 2 | 105167    |

  @at
  @dt
  @dtd
  @web
  @6776
  @regression
  @customerRegression
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_Product Solicited Review FormValidation Regular (ALM #6776)
    When I navigate to Product Review page with vehicle details for "<Year>" "<Make>" "<Model>" "<ProductId>"
    Then I verify product review page header text with product name "<ProductName>"
    And  I verify product rating headings displayed with five stars
    And  I verify the "Submit Review" button is displayed
    When I click "Submit Review" button
    Then I verify the "Please provide feedback for the following" pop-up is displayed
    And  I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    When I select "random" stars for "RIDE COMFORT" product rating category
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    And  I verify "Would you recommend this product to family and friends" question displayed with YES NO buttons
    And  I verify "Driving zip code" field displayed
    And  I verify "Miles driven on tires" field displayed
    And  I verify "Driving conditions" list box displayed
    And  I verify "Type of driving" list box displayed
    And  I verify "Yes I agree to Terms and Conditions" text displayed with checkbox and Terms and Conditions link
    When I enter "invalid" Driving zip code "0"
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    And  I verify "5 digit zip code required" error message displayed
    When I enter "invalid" Driving zip code "ababa"
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    And  I verify "5 digit zip code required" error message displayed
    When I enter "valid" Driving zip code "86001"
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    When I enter "valid" Driving zip code "T1Y1H3"
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    When I enter "invalid" Driving zip code ""
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    When I enter "0" Miles driven on tires
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    And  I verify "Please type a number between 1 and 150000" error message displayed
    When I enter "1" Miles driven on tires
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    When I enter "150001" Miles driven on tires
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    And  I verify "Please type a number between 1 and 150000" error message displayed
    When I enter "150000" Miles driven on tires
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"
    When I enter "" Miles driven on tires
    And  I click "Submit Review" button
    Then I verify items listed in "Please provide feedback for the following" pop-up for "product"
    When I close the "Please provide feedback for the following" pop-up
    Then I verify error messages on page for "product"

    Examples:
      | Year | Make  | Model | ProductId | ProductName |
      | 2012 | Honda | Civic | 26899     | Pro Contact |

  @at
  @dt
  @dtd
  @web
  @7980
  @7887
  @regression
  @customerRegression
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_Product Compare Tire Reviews Chart Sort (ALM #7980, 7887)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I click compare tire reviews link
    Then I verify the compare tire reviews page displays
    And  I verify tire quantity is "<Quantity>" on "Compare tire reviews"
    And  I verify "Overall Rating" sort option is displayed
    And  I verify "Ride Comfort" sort option is displayed
    And  I verify "Cornering / Steering" sort option is displayed
    And  I verify "Ride Noise" sort option is displayed
    And  I verify "Tread Life" sort option is displayed
    And  I verify "Dry Traction" sort option is displayed
    And  I verify "Wet Traction" sort option is displayed
    And  I verify "Winter Traction" sort option is displayed
    And  I verify "Buy Tire Again" sort option is displayed
    And  I verify the result list for "Overall Rating" is sorted in "Descending" order
    When I select the arrow on the sort option
    Then I verify the result list for "Overall Rating" is sorted in "Ascending" order

    Examples:
      | Year | Make      | Model    | Trim       | Assembly | Quantity |
      | 2012 | Honda     | Civic    | Coupe DX   | none     | 4        |
      | 2014 | Ram       | 3500     | Dually 4WD | none     | 6        |
      | 2010 | Chevrolet | Corvette | Base       | none     | 2        |

  @at
  @dt
  @web
  @7978
  @regression
  @customerRegression
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_Product CompareTireReviewsChart ContentDisplay (ALM#7978)
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I click compare tire reviews link
    Then I verify the compare tire reviews page displays
    And  I verify the "TIRES" navigation link is displayed
    And  I verify the "WHEELS" navigation link is displayed
    And  I verify the "APPOINTMENTS" navigation link is displayed
    And  I verify the "TIPS & GUIDES" navigation link is displayed
    And  I verify the "<Breadcrumbs>" link in the breadcrumb container
    And  I verify the window with header "Compare tire reviews" is displayed
    When I click add to cart for the first tire on the compare tire reviews page
    Then I verify item added to your cart popup contains selected tires
    When I click on the "Continue Shopping" link
    And  I click on the "Tires for my vehicle" link
    And  I select "footer" "Site Map"
    Then I am brought to the page with header "Site Map"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Breadcrumbs         |
      | 2012 | Honda     | Civic    | Coupe DX | none     | Home, Compare Tires |
      | 2010 | Chevrolet | Corvette | Base     | none     | Home, Compare Tires |

  @at
  @dt
  @dtd
  @web
  @7979
  @regression
  @customerRegression
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_Product CompareTireReviewsChart OverallRating Staggered (ALM #7979)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    Then I verify the product list page is displayed having "staggered" fitment
    When I click compare tire reviews link
    Then I verify the compare tire reviews page displays
    And  I verify the overall rating for the selected tire on the compare tire reviews page

    Examples:
      | Year | Make      | Model    | Trim | Assembly |
      | 2010 | Chevrolet | Corvette | Base | none     |

  @at
  @dt
  @web
  @15426
  @storeDetails
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_My Store PopUp_Sort By defaults to Most Recent (ALM#15426)
    When I change to the default store
    And  I click on "My Store" title
    And  I select Read Reviews link in the popup
    Then I verify "Customer Reviews" is displayed
    And  I verify the store review displayed with "Sort" dropdown list
    And  I verify the "Store Reviews" sort by dropdown value is set to "<SortValue>"
    And  I verify the "Store Reviews" result list for "<SortValue>" is sorted in "Descending" order

    Examples:
      | SortValue   |
      | Most Recent |

  @at
  @dt
  @6893
  @6392
  @17774
  @mobile
  @storeDetails
  Scenario Outline: MOBILE - HYBRIS_CUSTOMER_REVIEWS_Store_Reviews Sort By defaults to Most Recent (ALM#17774,6893,6392)
    When I search for store within "25" miles of "flagstaff"
    And  I select "Read reviews" for store #"2" in the location results
    Then I verify "Customer Reviews" is displayed
    And  I verify the "Store Reviews" sort by dropdown value is set to "<SortValue>"
    And  I verify the "Store Reviews" result list for "<SortValue>" is sorted in "Descending" order
    And  I verify the store review displayed with "Filter" dropdown list
    And  I verify the 'Store Reviews' result list displays "<Default Count>" results
    When I click the "Load More Reviews" button link
    Then I verify the 'Store Reviews' result list displays "<First Load More Review Count>" results
    When I click the "Load More Reviews" button link
    Then I verify the 'Store Reviews' result list displays "<Second Load More Review Count>" results
    When I select the 'Store Reviews' filter for "5 Stars" rating
    Then I verify the 'Store Reviews' result list displays all "5" star rating
    When I select Sort By reviews
    Then I verify the sort filter highest and lowest rated greyed out
    When I sort the reviews by "Highest Rated"
    Then I verify the 'Store Reviews' filter displays "All Ratings"

    Examples:
      | SortValue   | Default Count | First Load More Review Count | Second Load More Review Count |
      | Most Recent | 3             | 5                            | 10                            |

  @at
  @dt
  @web
  @core
  @8609
  @6392
  @15459
  @storeDetails
  @coreScenarioCustomer
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_My Store PopUp_Sort By changes are not retained (ALM#15459,6392,8609)
    When I change to the default store
    And  I click on "My Store" title
    And  I select Read Reviews link in the popup
    Then I verify "Customer Reviews" is displayed
    And  I verify the store review displayed with "Sort" dropdown list
    And  I verify the "Store Reviews" sort by dropdown value is set to "<SortValue2>"
    When I select Sort By reviews
    And  I sort the reviews by "<SortValue1>"
    Then I verify the "Store Reviews" sort by dropdown value is set to "<SortValue1>"
    And  I verify the "Store Reviews" result list for "<SortValue1>" is sorted in "Ascending" order
    When I go to the homepage
    And  I click on "My Store" title
    And  I select Read Reviews link in the popup
    Then I verify "Customer Reviews" is displayed
    And  I verify the "Store Reviews" sort by dropdown value is set to "<SortValue2>"
    And  I verify the "Store Reviews" result list for "<SortValue2>" is sorted in "Descending" order
    And  I verify the 'Store Reviews' result list displays "<Default Count>" results
    When I click the "Load More Reviews" button link
    Then I verify the 'Store Reviews' result list displays "<First Load More Review Count>" results
    When I click the "Load More Reviews" button link
    Then I verify the 'Store Reviews' result list displays "<Second Load More Review Count>" results

    Examples:
      | SortValue1   | SortValue2  | Default Count | First Load More Review Count | Second Load More Review Count |
      | Lowest Rated | Most Recent | 3             | 5                            | 10                            |

  @at
  @dt
  @7983
  @8609
  @mobile
  @storeDetails
  Scenario Outline: MOBILE - HYBRIS_CUSTOMER_REVIEWS_Store_Reviews_Sort By changes are not retained (ALM#7983,8609)
    When I search for store within "25" miles of "flagstaff"
    And  I select "Read reviews" for store #"1" in the location results
    Then I verify the store review displayed with "Sort" dropdown list
    When I select Sort By reviews
    And  I sort the reviews by "<SortValue1>"
    Then I verify the "Store Reviews" sort by dropdown value is set to "<SortValue1>"
    And  I verify the result list for "<SortValue1>" is sorted in "Ascending" order
    When I go to the homepage
    And  I search for store within "25" miles of "flagstaff"
    And  I select "Read reviews" for store #"1" in the location results
    Then I verify the "Store Reviews" sort by dropdown value is set to "<SortValue2>"
    And  I verify the "Store Reviews" result list for "<SortValue2>" is sorted in "Descending" order

    Examples:
      | SortValue1   | SortValue2  |
      | Lowest Rated | Most Recent |

  @at
  @dt
  @dtd
  @web
  @15625
  @mobile
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_Product Reviews_Sort By defaults to Most Recent (ALM #15625)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I am brought to the page with path "/fitmentresult/tires"
    And  I verify Customer Rating and reviews are displayed for listed products on PLP result page
    When I select the 'Read Reviews' link on the PLP
    Then I verify "Standard" PDP page is displayed
    And  I verify the window with header "Customer reviews" is displayed
    And  I verify the "Product Reviews" sort by dropdown value is set to "Most Recent"
    And  I verify the "Product Reviews" result list for "Most Recent" is sorted in "Descending" order

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | Honda | Civic | Coupe DX | none     |

  @at
  @dt
  @web
  @6842
  @6392
  @15531
  @storeDetails
  Scenario Outline: HYBRIS_CUSTOMER_REVIEWS_My Store PopUp_Store Reviews filterable by review ratings dropdown (ALM#15531,6842,6392)
    When I change to the default store
    And  I click on "My Store" title
    And  I select Read Reviews link in the popup
    Then I verify "Customer Reviews" is displayed
    And  I verify the store review displayed with "Filter" dropdown list
    And  I verify the 'Store Reviews' result list displays "<Default Count>" results
    When I click the "Load More Reviews" button link
    Then I verify the 'Store Reviews' result list displays "<First Load More Review Count>" results
    When I click the "Load More Reviews" button link
    Then I verify the 'Store Reviews' result list displays "<Second Load More Review Count>" results
    When I select the 'Store Reviews' filter for "5 Stars" rating
    Then I verify the 'Store Reviews' result list displays all "5" star rating
    And  I verify the 'Store Reviews' result list displays "<Default Count>" results
    When I select Sort By reviews
    Then I verify the sort filter highest and lowest rated greyed out
    When I sort the reviews by "Highest Rated"
    Then I verify the 'Store Reviews' filter displays "All Ratings"
    When I click the "Load More Reviews" button link
    Then I verify the 'Store Reviews' result list displays "<First Load More Review Count>" results
    When I select the 'Store Reviews' filter for "4 Stars" rating
    Then I verify the 'Store Reviews' result list displays all "4" star rating
    When I select Sort By reviews
    Then I verify the sort filter highest and lowest rated greyed out
    When I sort the reviews by "Lowest Rated"
    Then I verify the 'Store Reviews' filter displays "All Ratings"

    Examples:
      | Default Count | First Load More Review Count | Second Load More Review Count |
      | 3             | 5                            | 10                            |