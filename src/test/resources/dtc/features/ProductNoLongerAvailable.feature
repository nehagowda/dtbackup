@productNoLongerAvailable
Feature: Product No Longer Available
""" Need to load data via hot folder (under external/products.csv) in related running env before executing the scenarios in this feature"""

  Background:
    Given I go to the homepage

  @dt
  @at
  @dtd
  @web
  @mobile
  @productNoLongerAvailable_01
  Scenario Outline: Verify the details page for No Longer Available Tire with Product Replacement
    When I navigate to the "<noLongerAvailableTirePath>" url
    Then I should see "tire" no longer available message
    And  I verify that product pricing, inventory info and add to cart button are not displayed
    And  I should see replacement section label
    And  I should see replacement product image
    And  I should see replacement product brand "<targetBrand>"
    And  I should see replacement product name "<targetName>"
    And  I should see replacement product iconography
    And  I should see replacement product warranty information "<targetWarranty>"
    And  I should see replacement product size "<targetSize>"
    And  I should see replacement product "<targetCode>" article number
    And  I should see product replacement button
    When I click the "product" replacement button
    Then I am brought to the replacement details page with "<targetCode>"

    Examples:
      | noLongerAvailableTirePath                       | targetCode | targetBrand | targetName             | targetWarranty       | targetSize             |
      | /buy-tires/arizonian-silver-edition-iii/p/29948 | 14172      | GOODYEAR    | EAGLE F1 ASYMMETRIC AS | 45,000 mile warranty | 255 /45 R18 99W SL VSB |

  @dt
  @at
  @dtd
  @web
  @mobile
  @productNoLongerAvailable_02
  Scenario Outline: Verify the details page for No Longer Available Wheel with Product Replacement
    When I navigate to the "<noLongerAvailableWheelPath>" url
    Then I should see "wheel" no longer available message
    And  I verify that product pricing, inventory info and add to cart button are not displayed
    And  I should see replacement section label
    And  I should see replacement product image
    And  I should see replacement product brand "<targetBrand>"
    And  I should see replacement product name "<targetName>"
    And  I should see replacement product size "<targetSize>"
    And  I should see replacement product "<targetCode>" article number
    And  I should see product replacement button
    When I click the "product" replacement button
    Then I am brought to the replacement details page with "<targetCode>"

    Examples:
      | noLongerAvailableWheelPath                      | targetCode | targetBrand | targetName | targetSize                |
      | /buy-wheels/method-race-wheels-fat-five/p/54274 | 46179      | Cray        | Hawk       | 20 X10 5-120.65 37 BKGLST |

  @dt
  @at
  @dtd
  @web
  @mobile
  @productNoLongerAvailable_03
  Scenario Outline: Verify the details page for No Longer Available Tire with Brand Replacement
    When I navigate to the "<noLongerAvailableTirePath>" url
    Then I should see "tire" no longer available message
    And  I verify that product pricing, inventory info and add to cart button are not displayed
    And  I should see brand replacement button for "tire" with "<targetBrand>" and "<sourceSize>"
    When I click the "brand" replacement button
    Then I am brought to the search results PLP with the product's size as the search criteria "<sizeCriteria>" and the brand "<targetBrand>" facet selected

    Examples:
      | noLongerAvailableTirePath                    | sourceSize | sizeCriteria | targetBrand |
      | /buy-tires/falken-eurowinter-hs449/p/18732   | 215/60 R16 | 215/60-16    | GOODYEAR    |
      | /buy-tires/nitto-nt555-nt555-extreme/p/40167 | 235/35 R19 | 235/35-19    | none        |

  @dt
  @at
  @dtd
  @web
  @mobile
  @productNoLongerAvailable_04
  Scenario Outline: Verify the details page for No Longer Available Wheel with Brand Replacement
    When I navigate to the "<noLongerAvailableWheelPath>" url
    Then I should see "wheel" no longer available message
    And  I verify that product pricing, inventory info and add to cart button are not displayed
    And  I should see brand replacement button for "wheel" with "<targetBrand>" and "<sourceSize>"
    When I click the "brand" replacement button
    Then I am brought to the search results PLP with the product's size as the search criteria "<sizeCriteria>" and the brand "<targetBrand>" facet selected

    Examples:
      | noLongerAvailableWheelPath      | sourceSize | sizeCriteria | targetBrand |
      | /buy-wheels/cray-manta/p/21787  | 20 INCH    | 20X          | COVENTRY    |
      | /buy-wheels/vision-bane/p/74909 | 16 INCH    | 16X          | none        |