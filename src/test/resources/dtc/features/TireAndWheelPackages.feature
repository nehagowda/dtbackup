@tireAndWheelPackages
@web

Feature: Tire And Wheel Packages

  Background:
    Given I change to the default store

  @dt
  @dtd
  @noVehicleInSessionT/WPackages
  Scenario Outline: Verify fitment to add a vehicle when trying to add a package (No vehicle in session).
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @web
  @ordersRegression
  @tireAndWheelPackages
  @vehicleInSessionT/WPackagesWithoutAppointmentGuestCustomer
  Scenario Outline: Package with Vehicle in session - Adding tire 1st and wheel 2nd , without appointment guest customer. (Entry point - Tires link from header)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP ALL TIRES" button
    And  I select "Add To Package"
    And  I save the product details with key value "Tire1"
    And  I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    And  I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I am brought to the page with header "TIRE & WHEEL PACKAGE COMPLETE"
    And  I click on the "VIEW CART" link
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    And  I extract the "sales tax" on the cart page
    Then I am brought to the order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
    And  I assert order xml subItems
    And  I assert order xml customer node values
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption | Customer            | Reason                              |
      | 2012 | Honda | Civic | Coupe DX | tire          | default_customer_az | Make an appointment at a later time |

  @dt
  @web
  @ordersRegression
  @tireAndWheelPackages
  @vehicleInSessionT/WPackagesWithAppointmentGuestCustomer
  Scenario Outline: Package with Vehicle in session - Adding tire 1st and wheel 2nd, with appointment guest customer. (Entry point - Tires link from header)
  """ Failing on customer address validation due to billing address being sent to POS and ESB as the customer address.
  'I assert order xml customer node values' will be commented out until resolved. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Mode>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption1>"
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I click on "CHOOSE TIRES" in tire wheel package page
    And  I close popup modal
    And  I click on "CHOOSE WHEELS" in tire wheel package page
    And  I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I click on the "CHOOSE YOUR TIRES" link
    And  I click on the "SHOP ALL TIRES" button
    And  I select "Add To Package"
    And  I click on the "VIEW CART" link
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click on the "Continue to Customer Details" button
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml payment node values
#    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |

    Examples:
      | Customer            | Year | Make  | Mode  | Trim     | FitmentOption1 | Credit Card      | Bopis Customer              |
      | default_customer_az | 2012 | Honda | Civic | Coupe DX | Package        | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_VISA |

  @dt
  @web
  @ordersRegression
  @tireAndWheelPackages
  @vehicleInSessionT/WPackagesWithoutAppointmentMyAccountCustomer
  Scenario Outline: Package with Vehicle in session - Adding tire 1st and wheel 2nd, without appointment MyAccount customer. (Entry point - Tires link from header)
  """ Failing on customer address validation due to billing address being sent to POS and ESB as the customer address.
  'I assert order xml customer node values' will be commented out until resolved. """
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I select mini cart
    And  I click on the "clear cart" link
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP ALL TIRES" button
    And  I select "Add To Package"
    And  I save the product details with key value "Tire1"
    And  I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    And  I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I click on the "VIEW CART" link
    And  I select the checkout option "default"
    And  I click on the "Continue to Customer Details" button
    And  I select "Alternate Information" from drop down
    Then I verify "Email, Phone Number" values for "<Alternate Customer>" are now pre-populated
    When I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml payment node values
#    And  I assert order xml customer node values
    And  I assert order xml subItems

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption | Customer            | Credit Card      | Bopis Customer                    | password  | Email                | Alternate Customer     |
      | 2012 | Honda | Civic | Coupe DX | tire          | default_customer_az | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | Discount1 | autouser_b@gmail.com | MY_ACCOUNT_USER_QATEST |

  @dt
  @web
  @ordersRegression
  @tireAndWheelPackages
  @vehicleInSessionT/WPackagesWithAppointmentMyAccountCustomer
  Scenario Outline: Package with Vehicle in session - Adding tire 1st and wheel 2nd, with appointment MyAccount customer. (Entry point - Tires link from header)
  """ Failing on customer address validation due to billing address being sent to POS and ESB as the customer address.
  'I assert order xml customer node values' will be commented out until resolved. """
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Mode>" "<Trim>" "none"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption1>"
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I click on "CHOOSE TIRES" in tire wheel package page
    And  I close popup modal
    And  I click on "CHOOSE WHEELS" in tire wheel package page
    And  I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I click on the "CHOOSE YOUR TIRES" link
    And  I click on the "SHOP ALL TIRES" button
    And  I select "Add To Package"
    And  I click on the "VIEW CART" link
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml payment node values
#    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |

    Examples:
      | Email                 | password  | Customer            | Year | Make  | Mode  | Trim     | FitmentOption1 | Credit Card      | Bopis Customer                    |
      | autouserdt1@gmail.com | Discount1 | default_customer_az | 2012 | Honda | Civic | Coupe DX | Package        | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_MASTERCARD |

  @dtd
  @21270
  @ordersRegression
  @tireAndWheelPackages
  @vehicleInSessionT/WPackages
  @tireAndWheelPackagesWithGuestCustomer
  Scenario Outline: Package with Vehicle in session - Adding tire 1st and wheel 2nd on DTD guest customer. (Entry point - Tires link from header)(ALM#21270)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP ALL TIRES" button
    And  I select "Add To Package"
    And  I save the product details with key value "Tire1"
    And  I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    And  I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I click on the "VIEW CART" link
    And  I select the checkout option "default"
    And  I populate all the required fields for "<Customer>"
    And  I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I extract the "order total" on the checkout page for "<Bopis Customer>"
    And  I extract the "sales tax" on the checkout page for "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption | Customer            | Credit Card | Bopis Customer              | Credit Card |
      | 2012 | Honda | Civic | Coupe DX | tire          | default_customer_az | visa Bopis  | DEFAULT_CUSTOMER_BOPIS_VISA |  visa       |

  @dt
  @dtd
  @removePackage
  Scenario Outline: Verity cart is empty after clear package link.
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "SILVER" in wheel tire package
    Then I verify the tire or wheel option "SILVER" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Chrome Silver"
    When I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I click on element "Delete package link" in cart page in tire and packages order number "1"
    And  I click on element "REMOVE PACKAGE" in cart page in tire and packages order number "1"
    Then I should see product has been "removed" in cart message

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @dtd
  @removeTireFromPackage
  Scenario Outline: Remove tires from package in cart page (Should see only wheels in cart page).
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>"
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    Then I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I click on element "Remove Tires link" in cart page in tire and packages order number "1"
    And  I click on element "REMOVE TIRES" in cart page in tire and packages order number "1"
    Then I verify product details by key name "Tire1" are "not present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | Package       |


  @dt
  @dtd
  @removeWheelFromPackage
  Scenario Outline: Remove wheels from package in cart page(Should see only tires in cart page - Entry point fitment page).
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>"
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I click on element "Remove Wheels link" in cart page in tire and packages order number "1"
    And  I click on element "REMOVE WHEELS" in cart page in tire and packages order number "1"
    Then I verify product details by key name "Wheel1" are "not present" at order position "1"
    And  I verify product details by key name "Tire1" are "present" at order position "1"

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | Package       |


  @dt
  @vehicleInSessionW/TPackages
  Scenario Outline: Package with Vehicle in session - Adding wheel 1st and tire 2nd. (Entry point - Wheels link from header)
  """This test will be added to order regression suite in next PR, till then commenting out the orders regression tag."""
    When I go to the homepage
    And  I open the "WHEELS" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE WHEELS" in tire wheel package page
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    And  I click on the "STEP 2: CHOOSE YOUR TIRES" link
    Then I am brought to the page with header "Step 2 - CHOOSE YOUR TIRES"
    When I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify product details of key "Tire1" in row "1" in checkout page
    When I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Customer            |
      | 2012 | Honda | Civic | Coupe DX | default_customer_az |

  @dt
  @dtd
  @changeTirePackage
  Scenario Outline: Change tires from cart page (Should see new tires along with wheel in cart package)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I click on element "Change Tires link" in cart page in tire and packages order number "1"
    And  I click on element "CHOOSE TIRES" in cart page in tire and packages order number "1"
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR TIRES"
    When I click on the "SHOP BY TIRE CATEGORY" button
    And  I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    And  I set quantity to "4" and add item "19661" of type "none" to my add to package
    And  I save the product details with key value "New Tire"
    And  I click on the "VIEW CART" link
    Then I verify product details by key name "New Tire" are "present" at order position "1"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |


  @dt
  @dtd
  @changeWheelPackage
  Scenario Outline: Change wheels from cart page (Should see new wheels along with tires in cart package)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I click on element "Change Wheels link" in cart page in tire and packages order number "1"
    And  I click on element "CHOOSE WHEELS" in cart page in tire and packages order number "1"
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I set quantity to "4" and add item "69161" of type "none" to my add to package
    And  I save the product details with key value "New Wheel"
    And  I click on the "VIEW CART" link
    Then I verify product details by key name "New Wheel" are "present" at order position "1"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @dtd
  @cancelPackageKeepTires
  Scenario Outline: Cancel package and keep tires link (verify tires in cart page)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I click on the "package details" link
    Then I verify package details by key name "Tire1" are present at in PLP page
    When I click on the "change Tire" link
    Then I am brought to the page with header "Step 1 - CHOOSE YOUR TIRES"
    When I click on the "SHOP BY TIRE CATEGORY" button
    And  I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I click on the "package details" link
    And  I click on the "cancel package" link
    And  I click on the "CANCEL PACKAGE" button
    Then I verify the header cart total is "$0.00"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @dtd
  @cancelPackageKeepWheels
  Scenario Outline: Cancel package and keep wheels link (verify wheels in cart page)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I click on element "Change Tires link" in cart page in tire and packages order number "1"
    And  I click on element "CHOOSE TIRES" in cart page in tire and packages order number "1"
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR TIRES"
    When I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I click on the "package details" link
    Then I verify package details by key name "Wheel1" are present at in PLP page
    When I click on the "change Wheel" link
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I click on the "package details" link
    And  I click on the "cancel package" link
    And  I click on the "CANCEL PACKAGE" button
    Then I verify the header cart total is "$0.00"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @dtd
  @changeTireClosePackage
  Scenario Outline: Change tire and land on PLP page after closing package popup
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>"
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I click on element "Change Tires link" in cart page in tire and packages order number "1"
    And  I click on element "CHOOSE TIRES" in cart page in tire and packages order number "1"
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR TIRES"
    When I close popup modal
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | Package       |

  @dt
  @dtd
  @changeWheelClosePackage
  Scenario Outline: Change wheel and land on PLP page after closing package popup
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>"
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I click on element "Change Wheels link" in cart page in tire and packages order number "1"
    And  I click on element "CHOOSE WHEELS" in cart page in tire and packages order number "1"
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I close popup modal
    Then I verify the message on the "PLP" banner contains "These wheels fit your vehicle"

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | Package       |


  @dt
  @dtd
  @addPackageFromImageWithNoVehicle
  Scenario Outline: Verify fitment to add a vehicle when trying to add a package through package image (No vehicle in session).
    When I go to the homepage
    And  I click on "Wheel Tire Image" in tire wheel package page
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>"
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | Package       |

  @dt
  @dtd
  @addPackageFromImageWithVehicle
  Scenario Outline: Package with Vehicle in session through package image - Adding tire 1st and wheel 2nd. (Entry point - Tires link from header)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select back to my vehicles button
    And  I click on "Wheel Tire Image" in tire wheel package page
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @dtd
  @packageOEtoOptional
  Scenario Outline: Change from OE to Optional at step 2
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select back to my vehicles button
    And  I click on "Wheel Tire Image" in tire wheel package page
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I click on the "Edit Vehicle" link
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I am brought to the page with header "CONFIRM CHANGE?"
    When I click on the "CANCEL EDIT" button

    Examples:
      | Year | Make  | Model | Trim     | SizeOption | TireSize  |
      | 2012 | Honda | Civic | Coupe DX | 17         | 225/45-17 |

  @dt
  @footerPackageCancellation
  Scenario Outline: Verify package cancellation from footer links
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select back to my vehicles button
    And  I click on "Wheel Tire Image" in tire wheel package page
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "footer" "Tire Search"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Wheel Search"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Services"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Deals And Rebates"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Customer Care"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I click on the "Store Locator" link
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Appointments"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Return Policy"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Tire Safety"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Tire Size Calculator"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Check Tire Pressure"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "More Topics..."
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "About Us"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Our Story"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Motorsports"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Careers"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Credit"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Apply Now"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Commercial Payments"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Regional Offices"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Contact Us"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Privacy Policy"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Site Map"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @dtd
  @packageFromCompare
  Scenario Outline: Add to package from compare page
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select back to my vehicles button
    And  I click on "Wheel Tire Image" in tire wheel package page
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select the first "2" results to compare
    And  I click the compare products Compare button
    And  I add "1st" product to add to package in compare page
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I select "Add To Package"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @dtd
  @cancelPackageCheckFromMyVehicleModel
  Scenario Outline: Verify cancel package from My Vehicle pop up
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I open the My Vehicles popup
    And  I select "shop tires" link
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I open the My Vehicles popup
    And  I select "shop wheels" link
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I open the My Vehicles popup
    And  I select "delete" link
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Continue Building" option
    Then I verify the message on the "PLP" banner contains "These wheels fit your vehicle"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @checkAvailabilityPLPPackage
  Scenario Outline: Add to package in check availablity page - PLP
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select the first "Check nearby stores" link on "PLP" page
    And  I click on the "Add to Package" button
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "SILVER" in wheel tire package
    Then I verify the tire or wheel option "SILVER" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Chrome Silver"
    When I select the first "Check nearby stores" link on "PLP" page
    And  I click on the "Add to Package" button
    Then I am brought to the page with header "Tire & Wheel Package Complete"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dtd
  @ordersRegression
  @vehicleInSessionT/WPackagesDtd
  Scenario Outline: Package with Vehicle in session - Adding tire 1st and wheel 2nd - DTD. (Entry point - Tires link from header)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    And  I verify product details of key value "Wheel1"
    When I click on the "VIEW CART" link
    And  I select the checkout option "default"
    And  I enter shipping info as "<Customer>"
    And  I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption | Customer             |
      | 2012 | Honda | Civic | Coupe DX | tire          | default_customer_az  |

  @dtd
  @footerPackageCancellationDtd
  Scenario Outline: Verify package cancellation from footer links - DTD
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select back to my vehicles button
    And  I click on "Wheel Tire Image" in tire wheel package page
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "footer" "Tire Search"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Wheel Search"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I click on the "Suspension" link
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Deals And Rebates"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Customer Care"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I click on the "Find An Installer" link
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Return Policy"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I click on the "Web Experience Survey" link
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Tire Safety"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Tire Size Calculator"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Check Tire Pressure"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "More Topics..."
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "About Us"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Contact Us"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Credit"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Apply Now"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Pay Your Bill"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Privacy Policy"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    And  I select "footer" "Site Map"
    Then I am brought to the page with header "CANCEL YOUR TIRE & WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING" link
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @myAccountPackageViewAccount
  Scenario Outline: With My Account - Package with Vehicle in session - View my account
    When I select my account header navigation
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select my account header navigation
    And  I click on the "View My Account" link
    Then I am brought to the page with header "CANCEL YOUR TIRE AND WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING PACKAGE" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I select my account header navigation
    And  I click on the "View My Account" link
    And  I select mini cart
    Then I verify the header cart total is "$0.00"

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption | password  | Email                |
      | 2012 | Honda | Civic | Coupe DX | tire          | Discount1 | autouser_a@gmail.com |

  @dt
  @myAccountPackageSignOut
  Scenario Outline: With My Account - Package with Vehicle in session - Sign out
    When I select my account header navigation
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select my account header navigation
    And  I click on the "Sign-out" link
    Then I am brought to the page with header "CANCEL YOUR TIRE AND WHEEL PACKAGE?"
    When I click on the "STAY SIGNED-IN & CONTINUE" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I select my account header navigation
    And  I click on the "Sign-out" link
    And  I click on the "SIGN OUT & CANCEL" link
    Then I am brought to the homepage

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption | password  | Email                |
      | 2012 | Honda | Civic | Coupe DX | tire          | Discount1 | autouser_a@gmail.com |

  @dt
  @web
  @core
  @21270
  @vehicleInSessionT/WPackages
  @coreScenarioTireAndWheelPackages
  Scenario Outline: Package with Vehicle in session - Adding tire 1st and wheel 2nd on DT. (Entry point - Tires link from header) (ALM#21270)
  """ Failing on customer address validation due to billing address being sent to POS and ESB as the customer address.
  'I assert order xml customer node values' will be commented out until resolved. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I click on the "SHOP ALL TIRES" button
    And  I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I open the "APPOINTMENTS" navigation link
    And  I select Clear Recent Searches and "Continue Building" option
    And  I open the My Vehicles popup
    And  I click on the "shop products" link
    Then I verify the "CANCEL & KEEP TIRES" link is displayed
    And  I verify the "CONTINUE BUILDING" link is displayed
    When I click on the "CONTINUE BUILDING" link
    And  I click on the "edit vehicle" link
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I am brought to the page with header "CONFIRM CHANGE?"
    When I click on the "CANCEL EDIT" button
    Then I verify the PLP header message contains "wheel result"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    When I click on the "edit vehicle" link
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I am brought to the page with header "CONFIRM CHANGE?"
    When I click on the "CHANGE & CONTINUE" button
    And  I click on the "SHOP ALL TIRES" link
    Then I verify the PLP header message contains "tire result"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "<NonOELabel>" and "<TireSize>"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    And  I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    And  I verify the PLP header message contains "wheel result"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "<NonOELabel>" and "<TireSize>"
    When I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    When I click on the "VIEW CART" link
    And  I click on element "Remove Tires link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP TIRES" in cart page in tire and packages order number "1"
    And  I click on element "Change Tires link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP TIRES" in cart page in tire and packages order number "1"
    And  I click on element "Remove Wheels link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP WHEELS" in cart page in tire and packages order number "1"
    And  I click on element "Change Wheels link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP WHEELS" in cart page in tire and packages order number "1"
    And  I click on element "Delete package link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP PACKAGE" in cart page in tire and packages order number "1"
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    And  I extract the "order total" on the cart page
    And  I extract the "sales tax" on the cart page
    Then I am brought to the order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly>"
    And  I select a fitment option "<FitmentOption1>"
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I click on "CHOOSE TIRES" in tire wheel package page
    Then I am brought to the page with header "<Header>"
    When I close popup modal
    And  I click on "CHOOSE WHEELS" in tire wheel package page
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I am brought to the page with header "<Header>"
    When I click on the "CANCEL PACKAGE & SHOP WHEELS" link
    And  I click the discount tire logo
    And  I click on the "<Year1> <Make1>" button
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header2>"
    When I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make2>" "<Model2>" "<Trim2>" "none"
    And  I select a fitment option "<FitmentOption1>"
    Then I am brought to the page with header "<No tire or Wheel Header>"
    When I click on the "CONTINUE SHOPPING" link
    And  I close popup modal
    And  I select my account header navigation
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select my account header navigation
    And  I click on the "View My Account" link
    Then I am brought to the page with header "CANCEL YOUR TIRE AND WHEEL PACKAGE?"
    When I click on the "CONTINUE BUILDING PACKAGE" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I select mini cart
    Then I verify the header cart total is "$0.00"
    When I select my account header navigation
    And  I click on the "View My Account" link
    And  I click on the "CONTINUE BUILDING PACKAGE" button
    And  I select "Add To Package"
    And  I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I click on the "VIEW CART" link
    And  I click on element "Change Tires link" in cart page in tire and packages order number "1"
    And  I click on the "CHOOSE TIRES" button
    Then I am brought to the page with header "Step 2 - CHOOSE YOUR TIRES"
    When I click on the "SHOP BY TIRE CATEGORY" button
    And  I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I click on the "VIEW CART" link
    And  I select the checkout option "default"
    And  I click on the "Continue to Customer Details" button
    Then I verify contact selection drop down displays "Primary Information"
    When I select "Alternate Information" from drop down
    Then I verify "Email, Phone Number" values for "<Alternate Customer>" are now pre-populated
    When I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml payment node values
#    And  I assert order xml customer node values
    And  I assert order xml subItems

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption | Customer            | Year1 | Make1 | Model1   | Trim1    | Assembly       | FitmentOption1 | Header                           | Header2      | Credit Card      | Bopis Customer                    | Make2 | Model2 | Trim2  | No tire or Wheel Header                         | password  | Email                | Reason                              | Alternate Customer     | SizeOption | TireSize   | OELabel | OETireSize | NonOELabel |
      | 2012 | Honda | Civic | Coupe DX | tire          | default_customer_az | 2014  | Buick | LaCrosse | 2.4 4Cyl | 235 /50 R17 SL | Package        | NO TIRE RESULTS FOR THIS VEHICLE | wheel result | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | Acura | MDX    | SH-AWD | NO TIRE/WHEELS PACKAGE RESULTS FOR THIS VEHICLE | Discount1 | autouser_b@gmail.com | Make an appointment at a later time | MY_ACCOUNT_USER_QATEST | 16         | 215/55-16  | O.E.    | 195/65-R15 | +1’’       |

  @dtd
  @core
  @vehicleInSessionT/WPackages
  @coreScenarioTireAndWheelPackages
  Scenario Outline: Package with Vehicle in session - Adding tire 1st and wheel 2nd on DTD. (Entry point - Tires link from header)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I click on the "SHOP ALL TIRES" button
    And  I select "Add To Package"
    And  I save the product details with key value "Tire1"
    Then I am brought to the page with header "Step 1 - Complete"
    And  I verify wheel/tire package navigation has "Choose Tires" field is complete
    And  I verify product details of key value "Tire1"
    When I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    When I open the My Vehicles popup
    And  I click on the "shop products" link
    Then I verify the "CANCEL & KEEP TIRES" link is displayed
    And  I verify the "CONTINUE BUILDING" link is displayed
    When I click on the "CONTINUE BUILDING" link
    And  I click on the "edit vehicle" link
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I am brought to the page with header "CONFIRM CHANGE?"
    When I click on the "CANCEL EDIT" button
    Then I verify the PLP header message contains "wheel result"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize1>"
    And  I click on the "edit vehicle" link
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I am brought to the page with header "CONFIRM CHANGE?"
    When I click on the "CHANGE & CONTINUE" button
    And  I click on the "SHOP ALL TIRES" link
    Then I verify the PLP header message contains "tire result"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "<NonOELabel>" and "<NonOETireSize1>"
    When I select "Add To Package"
    And  I save the product details with key value "Tire1"
    And  I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I select wheel of type "BLACK" in wheel tire package
    Then I verify the tire or wheel option "BLACK" is selected
    When I click on the "CHOOSE YOUR WHEELS" link
    Then I verify package filter has "Wheel Color: Black"
    And  I verify the PLP header message contains "wheel result"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "<NonOELabel>" and "<NonOETireSize1>"
    When I select "Add To Package"
    And  I save the product details with key value "Wheel1"
    Then I am brought to the page with header "Tire & Wheel Package Complete"
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Choose Wheels" field is complete
    And  I verify wheel/tire package navigation has "Package complete" field is complete
    When I click on the "VIEW CART" link
    And  I click on element "Remove Tires link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP TIRES" in cart page in tire and packages order number "1"
    And  I click on element "Change Tires link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP TIRES" in cart page in tire and packages order number "1"
    And  I click on element "Remove Wheels link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP WHEELS" in cart page in tire and packages order number "1"
    And  I click on element "Change Wheels link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP WHEELS" in cart page in tire and packages order number "1"
    And  I click on element "Delete package link" in cart page in tire and packages order number "1"
    And  I click on element "KEEP PACKAGE" in cart page in tire and packages order number "1"
    Then I verify product details by key name "Tire1" are "present" at order position "1"
    And  I verify product details by key name "Wheel1" are "present" at order position "1"
    When I select the checkout option "default"
    And  I populate all the required fields for "<Customer>"
    And  I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I extract the "order total" on the checkout page for "<Bopis Customer>"
    And  I extract the "sales tax" on the checkout page for "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly>"
    And  I select a fitment option "<FitmentOption1>"
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I click on "CHOOSE TIRES" in tire wheel package page
    Then I am brought to the page with header "<Header>"
    When I close popup modal
    And  I click on "CHOOSE WHEELS" in tire wheel package page
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I click on the "ADD TO PACKAGE" button
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I am brought to the page with header "<Header>"
    When I click on the "CANCEL PACKAGE & SHOP WHEELS" link
    And  I click the discount tire logo
    And  I click on the "<Year1> <Make1>" button
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header2>"
    When I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    And  I click on the "ADD TO CART" button
    And  I click on the "VIEW SHOPPING CART" link
    And  I select the checkout option "default"
    And  I populate all the required fields for "<Customer>"
    And  I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I extract the "order total" on the checkout page for "<Bopis Customer>"
    And  I extract the "sales tax" on the checkout page for "<Bopis Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make2>" "<Model2>" "<Trim2>" "none"
    And  I select a fitment option "<FitmentOption1>"
    Then I am brought to the page with header "<No tire or Wheel Header>"
    When I click on the "CONTINUE SHOPPING" link
    And  I close popup modal
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I open the "TIRES" navigation link
    And  I click the "TIRE & WHEEL PACKAGE" menu option
    Then I am brought to the page with header "SELECT YOUR TIRE & WHEEL PACKAGE"
    When I select "BUILD YOUR OWN" on Select Tire and Wheel Package modal
    Then I am brought to the page with header "BUILD YOUR OWN PACKAGE"
    And  I should see text "CHOOSE TIRES" present in the page source
    And  I should see text "CHOOSE WHEELS" present in the page source
    When I click on "CHOOSE TIRES" in tire wheel package page
    And  I click on the "SHOP BY TIRE CATEGORY" button
    Then I am brought to the page with header "STEP 1 - CHOOSE YOUR TIRES"
    When I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I select mini cart
    Then I verify the header cart total is "$0.00"
    When I select "Add To Package"
    And  I click on the "STEP 2: CHOOSE YOUR WHEELS" link
    Then I am brought to the page with header "STEP 2 - CHOOSE YOUR WHEELS"
    When I click on the "VIEW ALL" link
    And  I select "Add To Package"
    And  I click on the "VIEW CART" link
    And  I click on element "Change Tires link" in cart page in tire and packages order number "1"
    And  I click on the "CHOOSE TIRES" button
    Then I am brought to the page with header "Step 2 - CHOOSE YOUR TIRES"
    When I click on the "SHOP BY TIRE CATEGORY" button
    And  I select tire of type "PASSENGER" in wheel tire package
    And  I click on the "CHOOSE YOUR TIRES" link
    Then I verify package filter has "Tire Category: Passenger"
    When I select "Add To Package"
    And  I click on the "VIEW CART" link
    And  I select the checkout option "default"
    And  I populate all the required fields for "<Customer>"
    And  I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I extract the "order total" on the checkout page for "<Bopis Customer>"
    And  I extract the "sales tax" on the checkout page for "<Bopis Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption | Customer            | Year1 | Make1 | Model1   | Trim1    | Assembly       | FitmentOption1 | Header                           | Header2      | Credit Card | Bopis Customer              | Make2 | Model2 | Trim2  | No tire or Wheel Header                         | SizeOption | TireSize  | OELabel | OETireSize | NonOELabel | TireSize1 | NonOETireSize1 | OETireSize1 | ShippingOption | Credit Card |
      | 2012 | Honda | Civic | Coupe DX | tire          | default_customer_az | 2014  | Buick | LaCrosse | 2.4 4Cyl | 235 /50 R17 SL | Package        | NO TIRE RESULTS FOR THIS VEHICLE | wheel result | visa Bopis  | DEFAULT_CUSTOMER_BOPIS_VISA | Acura | MDX    | SH-AWD | NO TIRE/WHEELS PACKAGE RESULTS FOR THIS VEHICLE | 16         | 215/55-16 | O.E.    | 195/65 R15 | +1’’       | 215/55 16 | 215/55-16      | 195/65-R15  | Ground         |  visa       |