@dt
@at
@fitment
Feature: Fitment

  Background:
    Given I change to the default store

  @web
  @9622
  @9743
  @mobile
  @wheelDiameterNoDecimal
  Scenario Outline: Verify the Wheel Diameter dropdown displays with no decimal places (ALM #9743, 9622)
    When I open the fitment popup
    And  I navigate to Shop By "Size"
    And  I select the "Wheels" subheader on the fitment grid
    Then I verify that no results have ".0" on the fitment grid
    When I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the word "All" does not appear in the tire set tab titles

    Examples:
      | Year | Make      | Model    | Trim | Assembly |
      | 2010 | Chevrolet | Corvette | Base | none     |

  @dtd
  @web
  @9630
  @mobile
  @regression
  @fitmentRegression
  Scenario Outline: Verify New Vehicle Fit Indicator and Green Banner on PDP and PLP page for Wheels (ALM#9630)
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<WheelAttribute>" fitment box option
    And  I select a fitment option "<WheelSize>"
    And  I select a fitment option "wheel"
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | WheelAttribute | WheelSize |
      | 2012 | Honda | Civic | Coupe EX | none     | 17             | 225/45-17 |

  @web
  @dtd
  @9628
  @5362
  @mobile
  @regression
  @matchRegression
  @fitmentRegression
  Scenario Outline: Verify New Vehicle Fit Indicator and Green Banner on PDP and PLP page for Tires (ALM #9628)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  |
      | 2012 | Honda | Civic | Coupe EX | none     | 15         | 215/60-15 |
      | 2012 | Honda | Civic | Coupe EX | none     | 17         | 215/45-17 |

  @9628
  @mobile
  @dtd
  @regression
  @fitmentRegression
  Scenario Outline: Mobile - HYBRIS_FITMENT_FITMENT_Tire Error Bar_When Plus Size On Product Detail (ALM #9628)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the message on the "PLP" banner contains "<Year>" "<Make>" and "<Model>"
    And  I verify the "PLP" banner color is "Green"
    When I select the "First" product result image on "PLP" page
    Then I verify the message on the "PDP" banner contains "<Year>" "<Make>" and "<Model>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  |
      | 2012 | Honda | Civic | Coupe EX | none     | 15         | 205/65-15 |
      | 2012 | Honda | Civic | Coupe DX | none     | 16         | 205/55-16 |

  @web
  @5358
  @myVehicleTireSizeMatchesProductResults
  Scenario Outline: My Vehicle "All tires" option shows results matching vehicle size (ALM #5358)
    When I search for store within "25" miles of "Scottsdale"
    And  I select make "<ZipCode>" my store
    And  I go to the homepage
    And  I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the initial page displays products that match my tire size(s): "<Assembly>"

    Examples:
      | ZipCode | Year | Make   | Model   | Trim       | Assembly       |
      | 85260   | 2012 | Ford   | Mustang | GT         | 235 /50 R18 SL |
      | 85260   | 2010 | Nissan | 370Z    | Coupe Base | 245 /40 R19 SL |

  @web
  @dtd
  @9607
  @mobile
  @regression
  @fitmentRegression
  Scenario Outline: PLP Banner should display Wheel Size when searching for Wheels by Size (ALM #9607)
    When I do a "homepage" "wheel" size search with details "<Diameter>, <WheelWidth>, <BoltPattern>"
    Then I verify the PLP header message contains "<Header>"
    And  I verify the "PLP" banner color is "Yellow"
    And  I verify the "PLP" results banner message contains "<WheelSize>"

    Examples:
      | Diameter | WheelWidth | BoltPattern       | Header       | WheelSize                  |
      | 15       | 6.5        | 5-114.3 MM/5-4.5" | wheel result | 15 X 6.5 5-114.3 mm/5-4.5" |

  @9695
  @9877
  @dtd
  @web
  @mobile
  @bba
  @fitmentBBA
  Scenario Outline: Search by Vehicle using the Homepage menu (ALM #9695, 9877)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I navigate back to previous page
    Then I verify my vehicle details: "<Year>, <Make>, <Model>, <Trim>" are "displayed" in the 'My Vehicles' section of the fitment grid
    When I click the discount tire logo
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<NewYear>" "<NewMake>" "<NewModel>" "<NewTrim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<NewYear> <NewMake>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    When I navigate back to previous page
    Then I verify my vehicle details: "<NewYear>, <NewMake>, <NewModel>, <NewTrim>" are "displayed" in the 'My Vehicles' section of the fitment grid

    Examples:
      | Year | Make  | Model | Trim     | Assembly | NewYear | NewMake   | NewModel | NewTrim | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | none     | 2010    | Chevrolet | Corvette | Base    | tire          |

  @dt
  @at
  @dtd
  @web
  @9398
  @mobile
  @regression
  @fitmentRegression
  Scenario Outline: Clearing Fitment while on PLP (ALM #9398)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I remove my "selected vehicle" vehicle
    Then I verify "no recent vehicles" element displayed

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header      |
      | 2012 | Honda | Civic | Coupe DX | none     | tire result |

  @web
  @dtd
  @9326
  @regression
  @fitmentRegression
  Scenario Outline: PLP displays current vehicle in the My Vehicle section in the header (ALM #9326)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify that My Vehicles displays "<Make> <Model>" in the header
    When I open the My Vehicles popup
    Then I verify that My Vehicles displays "<Year> <Make> <Model>" as the current vehicle
    And  I verify "default" My Vehicles popup displays add vehicle
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model1>" "<Trim1>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I close popup modal
    Then I verify that My Vehicles displays "<Make> <Model1>" in the header

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Model1 | Trim1    | Header      |
      | 2012 | Honda | Civic | Coupe EX | none     | Accord | EX Coupe | tire result |

  @dtd
  @9326
  @mobile
  @regression
  @fitmentRegression
  Scenario Outline: PLP displays current vehicle in the My Vehicle section in the header_mobile (ALM #9326)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify that My Vehicles displays "<Year> <Make> <Model>" as the current vehicle in the mobile menu
    And  I verify My Vehicles displays add vehicle in the mobile menu
    When I select Add Vehicle in the mobile menu
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model1>" "<Trim1>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I close popup modal
    Then I verify that My Vehicles displays "<Year> <Make> <Model1>" as the current vehicle in the mobile menu

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Model1 | Trim1    | Header      |
      | 2012 | Honda | Civic | Coupe EX | none     | Accord | EX Coupe | tire result |

  @web
  @dtd
  @11327
  @regression
  @fitmentRegression
  Scenario Outline: No Results Page Message (ALM #11327)
  """TODO Need search criteria that will produce no results on PLP"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify message displayed in search no results page with "vehicle in session"

    Examples:
      | Year | Make     | Model              | Trim | Assembly |
      | 1999 | Kawasaki | KVF300 Brute Force | 4x2  | none     |

  @web
  @dtd
  @10691
  @regression
  @fitmentRegression
  Scenario Outline: Validate Wheels Bolt Pattern facet displays without Fitment (ALM #10691)
    When I do a free text search for "<Text>" and hit enter
    Then I verify the PLP header message contains "<Text>"
    And  I verify the "Bolt Pattern" facet is displayed when no vehicle is selected
    When I "expand" the "<Facet>" filter section
    And  I select the "<Refinement>" checkbox
    Then I verify the "<Refinement>" checkbox has been checked
    When I select the "<Refinement>" filter to uncheck the checkbox
    Then I verify the "<Refinement>" filter checkbox to be "deselected"
    When I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" "Brand" "wheel" search with option: "<Brand>"
    And  I select shop all from the Product Brand page
    Then I can see "<Brand>" Brand PLP page
    And  I verify the "Bolt Pattern" facet is displayed when no vehicle is selected
    When I click the discount tire logo
    Then I am brought to the homepage
    When I open the "WHEELS" navigation link
    And  I click the "Wheel Style" View All link in the header
    Then I am brought to the page with header "Wheels"
    When I select "chrome wheels" from the Product Brand page
    And  I click on the "SHOP ALL" link
    Then I am brought to the page with path "<Path>"
    And  I verify the "Bolt Pattern" facet is displayed when no vehicle is selected

    Examples:
      | Text   | Facet        | Refinement | Brand       | Path           |
      | Wheels | Bolt Pattern | 4-100.0    | DRAG WHEELS | /wheels/chrome |

  @10684
  @web
  @dtd
  @regression
  @fitmentRegression
  Scenario Outline: Validate Wheels Bolt Pattern facet does not display with Fitment (ALM #10684)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel"
    Then I verify the PLP header message contains "<Header>"
    And  I verify that My Vehicles displays "<Make> <Model>" in the header
    And  I verify "Bolt Pattern" facet is not displayed when vehicle is selected
    When I click the discount tire logo
    Then I am brought to the homepage
    When I do a "my vehicles" "wheel" size search with details "<Diameter>, <WheelWidth>, <BoltPattern>"
    Then I verify the PLP header message contains "<Header>"
    And  I verify "Bolt Pattern" facet is not displayed when vehicle is selected

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header       | Diameter | WheelWidth | BoltPattern |
      | 2012 | Honda | Civic | Coupe EX | none     | wheel result | 15       | 6.5        | 5-100.0 MM  |

  @web
  @dtd
  @10117
  @regression
  @fitmentRegression
  Scenario Outline: Brand Search Displays No Results Page And Incorrect Error Message DT (ALM #10117)
    When I navigate to Shop By "Brand"
    And  I select the "Wheels" subheader on the fitment grid
    And  I select the "<Brand>" fitment grid option
    Then I am brought to the page with header "<Brand>"
    When I select "<SubCategory1>" to shop
    Then I can see "<Brand>" Brand PLP page
    When I navigate back to previous page
    Then I am brought to the page with header "<Brand>"
    When I open the "WHEELS" navigation link
    And  I click the "Wheel Brand" View All link in the header
    Then I am brought to the page with header "All Brands"
    When I select the "<Brand Image>" brand image
    Then I am brought to the page with header "<Brand Image>"
    When I select "<SubCategory2>" to shop
    Then I can see "<Brand Image>" Brand PLP page

    Examples:
      | Brand     | SubCategory1                 | Brand Image     | SubCategory2                        |
      | MB WHEELS | Shop MB WHEELS Chrome wheels | AMERICAN OUTLAW | shop AMERICAN OUTLAW Painted wheels |

  @web
  @dtd
  @11572
  @regression
  @fitmentRegression
  Scenario Outline: Validate Tire Brand Category On PLP (ALM #11572)
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    Then I am brought to the page with header "All Brands"
    When I select the "<Brand Image>" brand image
    Then I am brought to the page with header "<Brand Image>"
    When I click on the "Enter vehicle" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "CONTINUE" button
    And  I select "All-Season tires" from the Product Brand page
    Then I verify expected brand "<Brand>" products displayed on PLP

    Examples:
      | Brand Image    | Year | Make  | Model | Trim     | Assembly | FitmentOption | Brand    | Filter Section |
      | MICHELIN TIRES | 2012 | Honda | Civic | Coupe EX | none     | tire          | Michelin | Brands         |

  @dtd
  @web
  @12000
  @regression
  @fitmentRegression
  Scenario Outline: Validate Check Availability Link for Staggered Vehicle Optional Tire Sizes (ALM #12000)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select the "Check nearby stores" link for item "<ItemCode>" on "PLP" page
    Then I should verify that the Check Availability popup loaded
    And  I should verify that default store MY STORE label is at top and visible
    And  I should verify that make my store button is displayed

    Examples:
      | Year | Make      | Model    | Trim | Assembly | Header      | SizeOption | ItemCode | FitmentOption |
      | 2010 | Chevrolet | Corvette | Base | none     | tire result | 17         | 43437    | tire          |


  @web
  @dtd
  @11578
  @regression
  @fitmentRegression
  Scenario Outline: Validate OE PLP Message (ALM #11578)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select the "<QuickFilter>" checkbox
    Then I verify the "<QuickFilter>" checkbox has been checked
    And  I verify Original Equipment tire is displayed on "PLP" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header      | QuickFilter        |
      | 2012 | Honda | Civic | Coupe DX | none     | tire result | Original Equipment |

  @12335
  @web
  @dtd
  @regression
  @fitmentRegression
  Scenario Outline: Validate the default quantity for Staggered Tires on PLP (ALM #12335)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify tire quantity is "<Quantity>" on "product list page"
    When I navigate back to previous page
    Then I verify my vehicle details: "<Year>, <Make>, <Model>, <Trim>" are "displayed" in the 'My Vehicles' section of the fitment grid
    When I click on the "<Year> <Make>" button
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    And  I verify tire quantity is "<Quantity>" on "product list page"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | Quantity | SizeOption |
      | 2010 | Chevrolet | Corvette | Base | none     | 2        | 19         |

  @dtd
  @web
  @12436
  @mobile
  @regression
  @fitmentRegression
  Scenario Outline: OE Tires Quick Filter Facet On PLP_Standard Vehicle (ALM #12436)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify the "<OE Filter>" checkbox to be "deselected" by default
    When I select the "<OE Filter>" checkbox
    Then I verify Original Equipment tire is displayed on "PLP" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | OE Filter          |
      | 2012 | Honda | CIVIC | COUPE DX | none     | Original Equipment |

  @web
  @dtd
  @15327
  @regression
  @fitmentRegression
  Scenario Outline: OE Tires Quick Filter Facet On PLP_Staggered Vehicle (ALM #15327)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    When I select "FRONT" staggered tab on PLP result page
    Then I verify the "<OE Filter>" checkbox to be "deselected" by default
    When I select the "<OE Filter>" checkbox
    Then I verify Original Equipment tire is displayed on "PLP" page
    When I select "REAR" staggered tab on PLP result page
    Then I verify the "<OE Filter>" checkbox to be "deselected" by default
    When I select the "<OE Filter>" checkbox
    Then I verify Original Equipment tire is displayed on "PLP" page

    Examples:
      | Year | Make      | Model    | Trim | Assembly | OE Filter          |
      | 2010 | Chevrolet | Corvette | Base | none     | Original Equipment |

  @at
  @dt
  @dtd
  @web
  @12336
  @regression
  @fitmentRegression
  Scenario Outline: Validate the default quantity for Staggered Wheel Sizes and Check Inventory PLP (ALM #12336)
    When I go to the homepage
    And  I search for store within "25" miles of "<ZipCode>"
    And  I "continue" the Welcome Popup
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "wheel"
    Then I verify the PLP header message contains "<Header>"
    And  I verify tire quantity is "<Quantity>" on "product list page"
    When I extract my store name on the cart page
    And  I select the "Check nearby stores" link for item "<ItemCode>" on "PLP" page
    Then I should verify that the Check Availability popup loaded
    And  I verify the address in the check availabilty popup with my store
    And  I should verify that make my store button is displayed

    Examples:
      | Year | Make      | Model    | Trim | Assembly | Header       | Quantity | SizeOption | ItemCode | ZipCode |
      | 2010 | Chevrolet | Corvette | Base | none     | wheel result | 2        | 17         | 78269    | 85250   |

  @web
  @dtd
  @9949
  @regression
  @fitmentRegression
  Scenario Outline: Compare Products (ALM #9949)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products
    When I remove the first item on the compare product page
    And  I click the X next to the first product on the compare product page
    Then I verify the PLP header message contains "<Header>"
    When I select the first "3" results to compare
    And  I click the compare products Compare button
    And  I click Remove All
    Then I verify the PLP header message contains "<Header>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header      |
      | 2012 | Honda | CIVIC | COUPE DX | none     | tire result |

  @dtd
  @web
  @15556
  @regression
  @fitmentRegression
  Scenario Outline: HYBRIS_FITMENT_FITMENT_Vehicle with staggered and standard options show correctly on PLP (ALM #15556)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly1>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly2>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment

    Examples:
      | Year | Make | Model | Trim            | Assembly1      | Assembly2                           |
      | 2013 | BMW  | 335i  | Convertible 2WD | 225 /45 R17 SL | F 225 /35 R19 XL - R 255 /30 R19 XL |

  @dtd
  @web
  @13120
  @mobile
  Scenario Outline: HYBRIS_FITMENT_FTMENT_Validate Vehicle Description is displaying above the product in the cart with an already selected vehicle (ALM #13120)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the vehicle "<Year> <Make> <Model> <Trim>" is displayed on "shopping cart" page
    And  I verify the required fees and add-ons sections are expanded

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | ProductName       |
      | 2012 | Honda | CIVIC | COUPE DX | none     | 29935    | Silver Edition III|

  @dtd
  @web
  @15692
  @regression
  @fitmentRegression
  Scenario Outline: Check Availability Modal_Add to cart button (ALM #15692)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify check nearby stores link is displayed for "<ItemCode>" on "PLP" page
    When I select the "Check nearby stores" link for item "<ItemCode>" on "PLP" page
    Then I should verify that the Check Availability popup loaded
    And  I verify the Add To Cart button is clickable and Red on "Check Availability" page
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode |
      | 2012 | Honda | CIVIC | COUPE DX | none     | 29935    |

  @dt
  @at
  @dtd
  @web
  @15587
  Scenario Outline: Fitment Optional Sizes - Year Make Model Trim Tire/Wheel Size (ALM#15587)
    When I go to the homepage
    Then I verify the default selection in the fitment component
    And  I verify "Year" breadcrumb is active on the fitment grid
    And  I verify "search bar" element displayed
    When I enter "<Special Characters>" into the "search bar" field
    Then I verify error message "No match found" is displayed
    When I enter "<Year>" into the "search bar" field
    Then I verify "12" years are displaying numerically in "descending" order
    And  I verify all years starting from "20" is displayed
    When I click on the "<Year1>" button
    Then I verify "<Year>" is displayed below the fitment breadcrumb menu links
    When I enter "<Special Characters>" into the "search bar" field
    Then I verify error message "No match found" is displayed
    When I enter "<Make>" into the "search bar" field
    And  I click on the "<Make>" button
    Then I verify "<Year1> <Make>" is displayed below the fitment breadcrumb menu links
    When I enter "<Special Characters>" into the "search bar" field
    Then I verify error message "No match found" is displayed
    When I enter "<Model>" into the "search bar" field
    And  I click on the "<Model>" button
    Then I verify "<Year1> <Make> <Model>" is displayed below the fitment breadcrumb menu links
    When I click on the "<Trim>" button
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I verify the PLP page is displayed
    And  I click the discount tire logo
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year2>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year2> <Make1>"
    When I select back to my vehicles button
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year1> <Make1>"
    When I select back to my vehicles button
    Then I verify 'My Vehicles' section displays only "3" vehicles on "Homepage"
    When I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year3>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year3> <Make2>"
    When I select back to my vehicles button
    Then I verify 'My Vehicles' section displays only "3" vehicles on "Homepage"

    Examples:
      | Year | Special Characters | Make  | Year1 | Model | Trim     | Year2 | Make1     | Model1   | Trim1 | Assembly | Year3 | Make2  | Model2 | Trim2     |
      | 20   | @@@@               | honda | 2012  | civic | Coupe DX | 2010  | Chevrolet | Corvette | Base  | none     | 2014  | Toyota | Camry  | Hybrid SE |

  @dtd
  @web
  @15697
  Scenario Outline: HYBRIS_FITMENT_FITMENT_REDESIGN _Size Tab For Width metric tire sizes (ALM #15697)
    When I navigate to Shop By "Size"
    Then I verify the "Size" tab is active on the fitment grid
    And  I verify the "TIRES" subheader is active on the fitment grid
    And  I verify "Width" breadcrumb is active on the fitment grid
    And  I verify "18" fitment grid options are displayed
    And  I verify option "Show all" is "displayed" on the fitment grid
    And  I verify option "Help me choose" is "displayed" on the fitment grid
    When I click on the "Help me choose" button
    Then I verify the verbiage for the "tire sidewall" section is displayed in the fitment grid
    And  I verify the 'tire sidewall help' image is displayed
    When I select the "<Width>" fitment grid option
    Then I verify "Width" breadcrumb is active on the fitment grid
    And  I verify "<Width>" is displayed below the fitment breadcrumb menu links
    And  I verify "Ratio" breadcrumb is active on the fitment grid
    And  I verify the current fitment grid options are displaying in "ascending" order
    And  I verify option "Help me choose" is "displayed" on the fitment grid
    When I select the "<Ratio>" fitment grid option
    Then I verify "<Ratio>" is displayed below the fitment breadcrumb menu links
    And  I verify "Diameter" breadcrumb is active on the fitment grid
    And  I verify the current fitment grid options are displaying in "ascending" order
    And  I verify option "Help me choose" is "displayed" on the fitment grid
    When I select the "<Diameter>" fitment grid option
    Then I verify "Width" breadcrumb is active on the fitment grid
    And  I verify "Ratio" breadcrumb is active on the fitment grid
    And  I verify "Diameter" breadcrumb is active on the fitment grid
    And  I verify "<Diameter>" is displayed below the fitment breadcrumb menu links
    And  I verify the "VIEW TIRES" button displays with a red background
    When I click on the "VIEW TIRES" button
    Then I verify the PLP header message contains "<Width>/<Ratio>R<Diameter>"

    Examples:
      | Width | Ratio | Diameter |
      | 195   | 65    | 15       |

  @dtd
  @web
  @15697
  @15711
  Scenario Outline: HYBRIS_FITMENT_FITMENT_REDESIGN _Size Tab For Width metric tire sizes Add Remove Multi Sizes (ALM #15697, 15711)
    When I navigate to Shop By "Size"
    Then I verify the "TIRES" subheader is active on the fitment grid
    When I select the "<Size Type>" fitment grid option: "<Width/Height1>"
    And  I select the "<Ratio/Width1>" fitment grid option
    And  I select the "<Diameter1>" fitment grid option
    And  I click on the "VIEW TIRES" button
    Then I verify the PLP header message contains "<Width/Height1>/<Ratio/Width1>R<Diameter1>"
    When I click the discount tire logo
    And  I navigate to Shop By "Size"
    Then I verify the "TIRES" subheader is active on the fitment grid
    And  I verify the verbiage for the "recent tire sizes" section is displayed in the fitment grid
    And  I verify recently selected option "<Width/Height1>/<Ratio/Width1>-<Diameter1>" is "displayed" on the fitment grid
    When I select the "<Size Type>" fitment grid option: "<Width/Height2>"
    And  I select the "<Ratio/Width2>" fitment grid option
    And  I select the "<Diameter2>" fitment grid option
    And  I click on the "VIEW TIRES" button
    Then I verify the PLP header message contains "<Width/Height2>/<Ratio/Width2>R<Diameter2>"
    When I click the discount tire logo
    And  I navigate to Shop By "Size"
    Then I verify the "TIRES" subheader is active on the fitment grid
    And  I verify recently selected option "<Width/Height1>/<Ratio/Width1>-<Diameter1>" is "displayed" on the fitment grid
    And  I verify recently selected option "<Width/Height2>/<Ratio/Width2>-<Diameter2>" is "displayed" on the fitment grid
    When I remove recently selected option "<Width/Height2>/<Ratio/Width2>-<Diameter2>" from the fitment grid
    Then I verify recently selected option "<Width/Height2>/<Ratio/Width2>-<Diameter2>" is "not displayed" on the fitment grid

    Examples:
      | Size Type    | Width/Height1 | Ratio/Width1 | Diameter1 | Width/Height2 | Ratio/Width2 | Diameter2 |
      | standard     | 195           | 65           | 15        | 225           | 35           | 19        |
      | non-standard | 25            | 10           | 12        | 40            | 15.5         | 20        |

  @dtd
  @web
  @15711
  Scenario Outline: HYBRIS_FITMENT_FITMENT_REDESIGN _Size Tab For High Flotation tire (ALM #15711)
    When I navigate to Shop By "Size"
    Then I verify the "Size" tab is active on the fitment grid
    And  I verify the "TIRES" subheader is active on the fitment grid
    And  I verify "Width" breadcrumb is active on the fitment grid
    And  I verify option "Show all" is "displayed" on the fitment grid
    When I select "Show all" on the vehicle grid
    Then I verify 'High flotation' tire sizes are displayed below the 'Metric' tire sizes
    And  I verify 'High flotation' tire sizes are displayed in "ascending" order
    When I select the "non-standard" fitment grid option: "<Height>"
    Then I verify "Height" breadcrumb is active on the fitment grid
    And  I verify "<Height>" is displayed below the fitment breadcrumb menu links
    And  I verify option "Help me choose" is "displayed" on the fitment grid
    When I click on the "Help me choose" button
    Then I verify the verbiage for the "tire sidewall" section is displayed in the fitment grid
    And  I verify the 'tire sidewall help' image is displayed
    And  I verify "Width" breadcrumb is active on the fitment grid
    When I select the "<Width>" fitment grid option
    Then I verify "Height" breadcrumb is active on the fitment grid
    And  I verify "Width" breadcrumb is active on the fitment grid
    And  I verify "<Width>" is displayed below the fitment breadcrumb menu links
    And  I verify "Diameter" breadcrumb is active on the fitment grid
    And  I verify the current fitment grid options are displaying in "ascending" order
    And  I verify option "Help me choose" is "displayed" on the fitment grid
    When I select the "<Diameter>" fitment grid option
    Then I verify "Height" breadcrumb is active on the fitment grid
    And  I verify "Width" breadcrumb is active on the fitment grid
    And  I verify "Diameter" breadcrumb is active on the fitment grid
    And  I verify "<Diameter>" is displayed below the fitment breadcrumb menu links
    And  I verify the "VIEW TIRES" button displays with a red background
    When I click on the "VIEW TIRES" button
    Then I verify the PLP header message contains "<Height>/<Width>R<Diameter>"

    Examples:
      | Height | Width | Diameter |
      | 25     | 10    | 12       |

  @dtd
  @web
  @15713
  Scenario Outline: HYBRIS_FITMENT_FITMENT_REDESIGN _Size Tab For  Wheels Quick Search (ALM #15713)
    When I navigate to Shop By "Size"
    Then I verify the "Size" tab is active on the fitment grid
    And  I verify the "TIRES" subheader is active on the fitment grid
    When I select the "Wheels" subheader on the fitment grid
    Then I verify the "WHEELS" subheader is active on the fitment grid
    And  I verify "Quick Search" breadcrumb is active on the fitment grid
    And  I verify "Diameter" breadcrumb is active on the fitment grid
    And  I verify the current fitment grid options are displaying in "ascending" order
    When I select the "<Diameter>" fitment grid option
    Then I verify "<Diameter>" is displayed below the fitment breadcrumb menu links
    And  I verify the "VIEW WHEELS" button displays with a red background
    When I click on the "VIEW WHEELS" button
    Then I verify the PLP header message contains "wheel result"

    Examples:
      | Diameter |
      | 15       |

  @dtd
  @web
  @15713
  Scenario Outline: HYBRIS_FITMENT_FITMENT_REDESIGN _Size Tab For  Wheels Quick Search ADD REMOVE (ALM #15713)
    When I do a "homepage" "wheel" size search with details "<WheelSize1>"
    Then I verify the PLP header message contains "wheel result"
    When I click the discount tire logo
    And  I navigate to Shop By "Size"
    And  I select the "Wheels" subheader on the fitment grid
    Then I verify the "WHEELS" subheader is active on the fitment grid
    And  I verify recently selected option "<WheelSize1>" is "displayed" on the fitment grid
    When I select the "<WheelSize2>" fitment grid option
    And  I click on the "VIEW WHEELS" button
    Then I verify the PLP header message contains "wheel result"
    When I click the discount tire logo
    And  I navigate to Shop By "Size"
    And  I select the "Wheels" subheader on the fitment grid
    Then I verify the "WHEELS" subheader is active on the fitment grid
    And  I verify recently selected option "<WheelSize1>" is "displayed" on the fitment grid
    And  I verify recently selected option "<WheelSize2>" is "displayed" on the fitment grid
    When I remove recently selected option "<WheelSize1>" from the fitment grid
    Then I verify recently selected option "<WheelSize1>" is "not displayed" on the fitment grid
    When I remove recently selected option "<WheelSize2>" from the fitment grid
    Then I verify recently selected option "<WheelSize2>" is "not displayed" on the fitment grid
    And  I verify the "WHEELS" subheader is active on the fitment grid
    And  I verify "Quick Search" breadcrumb is active on the fitment grid

    Examples:
      | WheelSize1 | WheelSize2 |
      | 18         | 20         |

  @dt
  @15731
  @mobile
  Scenario Outline: Mobile - HYBRIS_FITMENT_FITMENT_Optional tires sizes for Non-Staggered Vehicles(ALM #15731)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify the fitment box option has a value of "<SizeOption>"
    And  I verify no duplicate Optional tire sizes are displayed
    When I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make | Model | Trim  | Assembly       | SizeOption | TireSize  |
      | 2014 | BMW  | 328i  | Sedan | 225 /45 R18 SL | 17         | 215/50-17 |
      | 2014 | BMW  | 328i  | Sedan | 225 /45 R18 SL | 18         | 215/45-18 |
      | 2014 | BMW  | 328i  | Sedan | 225 /50 R17 SL | 18         | 235/40-18 |
      | 2014 | BMW  | 328i  | Sedan | 225 /50 R17 SL | 19         | 245/35-19 |

  @dtd
  @web
  @15714
  Scenario Outline: HYBRIS_FITMENT_FITMENT_REDESIGN _Size Tab For  Wheels Advanced  Search (ALM #15714)
    When I navigate to Shop By "Size"
    Then I verify the "Size" tab is active on the fitment grid
    When I select the "Wheels" subheader on the fitment grid
    Then I verify the "WHEELS" subheader is active on the fitment grid
    When I select the "Advanced Search" subheader on the fitment grid
    Then I verify "Advanced Search" breadcrumb is active on the fitment grid
    And  I verify "Diameter" breadcrumb is active on the fitment grid
    And  I verify the current fitment grid options are displaying in "ascending" order
    When I select the "<Diameter>" fitment grid option
    Then I verify "<Diameter>" is displayed below the fitment breadcrumb menu links
    And  I verify "Diameter" breadcrumb is active on the fitment grid
    And  I verify the current fitment grid options are displaying in "ascending" order
    And  I verify "Wheel Width" breadcrumb is active on the fitment grid
    When I select the "<Wheel Width>" fitment grid option
    Then I verify "<Wheel Width>" is displayed below the fitment breadcrumb menu links
    And  I verify "Wheel Width" breadcrumb is active on the fitment grid
    And  I verify the current fitment grid options are displaying in "ascending" order
    And  I verify "Bolt Pattern" breadcrumb is active on the fitment grid
    When I select the "<Bolt Pattern>" fitment grid option
    Then I verify "<Bolt Pattern>" is displayed below the fitment breadcrumb menu links
    And  I verify "Bolt Pattern" breadcrumb is active on the fitment grid
    And  I verify the "VIEW WHEELS" button displays with a red background
    When I click on the "VIEW WHEELS" button
    Then I verify the PLP header message contains "wheel result"

    Examples:
      | Diameter | Wheel Width | Bolt Pattern |
      | 12       | 7.0         | 4-110.0 mm   |

  @dtd
  @web
  @15714
  Scenario Outline: HYBRIS_FITMENT_FITMENT_REDESIGN _Size Tab For  Wheels Advanced  Search ADD REMOVE (ALM #15714)
    When I do a "homepage" "wheel" size search with details "<Diameter1>, <Wheel Width1>, <Bolt Pattern1>"
    Then I verify the PLP header message contains "wheel result"
    When I click the discount tire logo
    And  I navigate to Shop By "Size"
    And  I select the "Wheels" subheader on the fitment grid
    Then I verify the "WHEELS" subheader is active on the fitment grid
    And  I verify the verbiage for the "recent wheel sizes" section is displayed in the fitment grid
    And  I verify recently selected option "<Diameter1> X <Wheel Width1> - <Bolt Pattern1>" is "displayed" on the fitment grid
    When I do a "homepage" "wheel" size search with details "<Diameter2>, <Wheel Width2>, <Bolt Pattern2>"
    Then I verify the PLP header message contains "wheel result"
    When I click the discount tire logo
    And  I navigate to Shop By "Size"
    And  I select the "Wheels" subheader on the fitment grid
    Then I verify the "WHEELS" subheader is active on the fitment grid
    And  I verify the verbiage for the "recent wheel sizes" section is displayed in the fitment grid
    And  I verify recently selected option "<Diameter2> X <Wheel Width2> - <Bolt Pattern2>" is "displayed" on the fitment grid
    And  I verify recently selected option "<Diameter1> X <Wheel Width1> - <Bolt Pattern1>" is "displayed" on the fitment grid
    When I remove recently selected option "<Diameter1> X <Wheel Width1> - <Bolt Pattern1>" from the fitment grid
    Then I verify recently selected option "<Diameter1> X <Wheel Width1> - <Bolt Pattern1>" is "not displayed" on the fitment grid
    And  I verify recently selected option "<Diameter2> X <Wheel Width2> - <Bolt Pattern2>" is "displayed" on the fitment grid
    When I remove recently selected option "<Diameter2> X <Wheel Width2> - <Bolt Pattern2>" from the fitment grid
    Then I verify recently selected option "<Diameter2> X <Wheel Width2> - <Bolt Pattern2>" is "not displayed" on the fitment grid
    And  I verify the "WHEELS" subheader is active on the fitment grid
    And  I verify "Advanced Search" breadcrumb is active on the fitment grid

    Examples:
      | Diameter1 | Wheel Width1 | Bolt Pattern1 | Diameter2 | Wheel Width2 | Bolt Pattern2 |
      | 12        | 7.0          | 4-110.0 mm    | 20        | 8.5          | 5-112.0 mm    |

  @dt
  @at
  @dtd
  @web
  @18292
  Scenario Outline: HYBRIS_FITMENT_FITMENT_New Fitment Optional Sizes - Standard vehicles (ALM#18292)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify "Back to my vehicles" element displayed
    When I select back to my vehicles button
    And  I click on the "<Year> <Make>" button
    And  I select edit vehicle link
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify optional tire and wheel size is displayed
    And  I verify what are you shopping for heading is displayed
    When I "expand" "optional tire and wheel size"
    Then I verify that the optional tire and wheel size is "expanded"
    And  I verify no duplicate Optional tire sizes are displayed
    When I select the "<Diameter>" fitment box option
    Then I verify no duplicate optional fitment sizes are displayed
    And  I verify "O.E." badging is displayed on "<Size>"
    When I select a fitment option "<Size2>"
    Then I verify the selected fitment box option has a value of "<Size2>"
    And  I verify that the optional tire and wheel size is "collapsed"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select edit vehicle link
    And  I "expand" "optional tire and wheel size"
    And  I select a fitment option "<Size3>"
    And  I select a fitment option "<SizeOption3>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify all the products listed on plp are of "<displaySizeOptionPlp>"
    When I go to the homepage
    And  I click on the "<Year> <Make>" button
    And  I select a fitment option "<FitmentOption1>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header1>"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE Wheel" fitment banner with "<Label1>" and "<Size3>"
    When I click the discount tire logo
    And  I click on the "<Year> <Make>" button
    And  I "expand" "optional tire and wheel size"
    And  I select the "<Diameter>" fitment box option
    And  I select revert to O.E. size
    And  I select a fitment option "<FitmentOption1>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header1>"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "OE Wheel" fitment banner with "<OELabel>" and "<Diameter>"
    When I open the My Vehicles popup
    Then I verify recent vehicle "<Year><Make><Model>" "displayed"
    And  I verify "Regular" Vehicle Details Banner on "My Vehicles Modal" contains "OE Wheel" fitment banner with "<OELabel>" and "<Diameter>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "<First Name>" name displayed on homepage header
    When I select my account header navigation
    And  I click on the "View My Account" link
    And  I open the My Vehicles popup
    Then I verify recent vehicle "<Year><Make><Model>" "displayed"
    When I select a fitment option "<FitmentOption1>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header1>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | OELabel | OETireSize | Diameter | Size      | Size2     | FitmentOption | Header      | FitmentOption1 | Header1      | Email                | Password  | First Name | Size3 | SizeOption3 | displaySizeOptionPlp | Label1 |
      | 2012 | Honda | Civic | Coupe DX | none     | O.E.    | 195/65-R15 | 15       | 195/65-15 | 215/60-15 | tire          | tire result | wheel          | wheel result | autouser_a@gmail.com | Discount1 | Autouser   | 18    | 235/40-18   | 235 /40 R18          | +3’’   |

  @dt
  @at
  @web
  @19319
  Scenario Outline:HYBRIS_FITMENT_FITMENT_NEW_Fitment Optional Sizes-Edit Vehicle (ALM#19319)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select back to my vehicles button
    And  I click on the "<Year> <Make>" button
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select edit vehicle link
    Then I verify all of the fitment menus are displayed
    When I click on the "Year" button
    And  I click on the "<Year1>" button
    And  I click on the "Make" button
    And  I click on the "<Make1>" button
    And  I click on the "Model" button
    And  I click on the "<Model1>" button
    And  I click on the "Trim" button
    And  I click on the "<Trim1>" button
    Then I should see the fitment panel with vehicle "<Year1> <Make1>"
    When I select back to my vehicles button
    Then I verify my vehicle details: "<Year1>, <Make1>, <Model1>, <Trim1>" are "displayed" in the 'My Vehicles' section of the fitment grid
    When I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year2> <Make2>"
    When I select back to my vehicles button
    Then I verify 'My Vehicles' section displays only "2" vehicles on "Homepage"
    And  I verify my vehicle details: "<Year1>, <Make1>, <Model1>, <Trim1>" are "displayed" in the 'My Vehicles' section of the fitment grid
    And  I verify my vehicle details: "<Year2>, <Make2>, <Model2>, <Trim2>" are "displayed" in the 'My Vehicles' section of the fitment grid

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Year1  | Make1 | Model1 | Trim1 | Year2 | Make2 | Model2 | Trim2    |
      | 2012 | Honda | Civic | Coupe DX | none     | 2014   | Acura | ILX    | 2.0L  | 2011  | Honda | Civic  | Coupe EX |

  @dt
  @at
  @dtd
  @web
  @19166
  Scenario Outline: HYBRIS_FITMENT_FITMENT_New Fitment Optional Sizes - Staggered vehicles (ALM#19166)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I verify "Back to my vehicles" element displayed
    When I select back to my vehicles button
    And  I click on the "<Year> <Make>" button
    And  I select edit vehicle link
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    And  I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify optional tire and wheel size is displayed
    And  I verify what are you shopping for heading is displayed
    When I "expand" "optional tire and wheel size"
    Then I verify that the optional tire and wheel size is "expanded"
    And  I verify no duplicate Optional tire sizes are displayed
    When I select the "<Diameter>" fitment box option
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "<Label1>" and "<WheelSize>"
    And  I verify that the optional tire and wheel size is "collapsed"
    When I select a fitment option "FitmentOption" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE FRONT Wheel" fitment banner with "<Label1>" and "<WheelSize>"
    When I navigate back to previous page
    Then I am brought to the homepage
    When I click on the "<Year> <Make>" button
    And  I select the "Front" staggered menu option
    And  I "expand" "optional tire and wheel size"
    And  I select the "<WheelSize>" fitment box option
    And  I select revert to O.E. size
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    When I select "FRONT" staggered tab on PLP result page
    And  I navigate back to previous page
    Then I am brought to the homepage
    When I click on the "<Year> <Make>" button
    And  I select a fitment option "<FitmentOption1>"
    Then I verify the PLP header message contains "<Header1>"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    When I navigate back to previous page
    Then I am brought to the homepage
    When I click on the "<Year> <Make>" button
    And  I select the "Rear" staggered menu option
    And  I "expand" "optional tire and wheel size"
    And  I select the "<Wheel size2>" fitment box option
    Then I verify "O.E." badging is displayed on "<Optional Rearsize>"
    When I click on the "<Optional Rearsize1>" link
    And  I select a fitment option "<FitmentOption1>"
    Then I verify the PLP header message contains "<Header1>"
    When I open the My Vehicles popup
    Then I verify recent vehicle "<Year><Make><Model>" "displayed"
    And  I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE Tire" fitment banner with "<Label1>" and "<Wheel size2>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    And  I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "PLP" page
    When I select my account header navigation
    And  I click on the "View My Account" link
    And  I open the My Vehicles popup
    Then I verify recent vehicle "<Year><Make><Model>" "displayed"
    When I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header1>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | OELabel | OETireSize | Label1 | Diameter | WheelSize | FitmentOption | Header      | FitmentOption1 | Header1      | Optional Rearsize | Optional Rearsize1 | Wheel size2 | Email                | password  |
      | 2010 | Chevrolet | Corvette | Base | none     | O.E.    | 245/40-R18 | +0’’   | 18”/19”  | 18        | tire          | tire result | wheel          | wheel result | 285/35-19         | 265/40-19          |  19”        | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @19347
  Scenario Outline: HYBRIS_FITMENT_FITMENT_NEW_Fitment Optional Sizes - Standard Vehicle_Treadwell Flow (ALM#19347)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize1>"
    And  I verify optional tire and wheel size is displayed
    And  I verify what are you shopping for heading is displayed
    When I "expand" "optional tire and wheel size"
    Then I verify that the optional tire and wheel size is "expanded"
    When I select the "<Diameter>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify the selected fitment box option has a value of "<TireSize>"
    And  I verify that the optional tire and wheel size is "collapsed"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<TireSize>" is displayed
    When I select view recommended tires
    Then I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "<Non-OELabel>" and "<TireSize>"
    And  I verify that the sort by dropdown value is set to "<Value>"
    And  I verify that the treadwell filter is "present" in the first position in quick filters
    And  I verify the "Treadwell" checkbox to be "selected" by default
    When I select edit treadwell driving details on plp page
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<TireSize>" is displayed
    When I select edit icon on "treadwell modal"
    Then I verify fitment popup page displays
    When I "expand" "optional tire and wheel size"
    Then I verify that the optional tire and wheel size is "expanded"
    When I select the "<Diameter>" fitment box option
    And  I select revert to O.E. size
    Then I verify that the optional tire and wheel size is "collapsed"
    When I "expand" "optional tire and wheel size"
    And  I select the "<Diameter1>" fitment box option
    Then I verify "O.E." badging is displayed on "<OETireSize>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<OETireSize1>" is displayed
    When I select view recommended tires
    Then I verify "Regular" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize1>"
    And  I verify that the sort by dropdown value is set to "<Value>"
    And  I verify that the treadwell filter is "present" in the first position in quick filters
    When I select edit vehicle link
    And  I "expand" "optional tire and wheel size"
    And  I select the "<Diameter>" fitment box option
    And  I select a fitment option "<OptionalTireSize2>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify treadwell details section "absent"
    And  I verify all the products listed on plp are of "<displaySizeOptionPlp>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | OELabel | OETireSize | Diameter | TireSize  | Non-OELabel | Value       | Diameter1 | OETireSize1 | OptionalTireSize2 | displaySizeOptionPlp |
      | 2012 | Honda | Civic | Coupe DX | none     | O.E.    | 195/65-15  | 16       | 205/55-16 | +1          | Recommended | 15        | 195/65-R15  | 195/55-16         | 195 /55 R16          |

  @dt
  @at
  @dtd
  @web
  @19564
  Scenario Outline: HYBRIS_FITMENT_FITMENT_NEW_Fitment Optional Sizes - Fitment_Optional Sizes_No Results Page (ALM#19564)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    When I "expand" "optional tire and wheel size"
    Then I verify no duplicate Optional tire sizes are displayed
    When I select the "<Diameter>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify "Regular" Vehicle Details on "My Vehicles" page contains "Non-OE Tire" fitment banner with "<Non-OELabel>" and "<TireSize>"
    And  I verify that the optional tire and wheel size is "collapsed"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I do a free text search for "<text>" and hit enter
    Then I verify message displayed in search no results page with "vehicle in session"
    When I click on the "Shop tires that fit" link
    Then I verify the PLP header message contains "tire result"
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "Non-OE Tire" fitment banner with "<Non-OELabel>" and "<TireSize>"
    When I do a free text search for "<text>" and hit enter
    Then I verify message displayed in search no results page with "vehicle in session"
    When I click on the "Shop wheels that fit" link
    Then I verify the PLP header message contains "wheel result"
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "Non-OE Tire" fitment banner with "<Non-OELabel>" and "<TireSize>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | OELabel | OETireSize | Diameter | TireSize  | Non-OELabel | text    |
      | 2012 | Honda | Civic | Coupe DX | none     | O.E.    | 195/65-R15 | 15       | 205/65-15 | +0          | hghjghj |

  @dt
  @at
  @dtd
  @web
  @19565
  Scenario Outline: HYBRIS_FITMENT_FITMENT_Optional Sizes - Your cart is Empty page - shop tires shop wheels (ALM#19565)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    When I "expand" "optional tire and wheel size"
    Then I verify no duplicate Optional tire sizes are displayed
    When I select the "<Diameter>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I remove the item from the cart
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify all the products listed on plp are of "<TireSize1>"
    When I click the discount tire logo
    And  I click on the "<Year> <Make>" button
    And  I select a fitment option "wheel"
    Then I verify the PLP header message contains "<Header1>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I remove the item from the cart
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I select a fitment option "wheel"
    Then I verify the PLP header message contains "<Header1>"
    And  I verify all the products listed on plp are of "<Diameter>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | OELabel | OETireSize | Diameter | TireSize  | TireSize1   | Header      | Header1      |
      | 2012 | Honda | Civic | Coupe DX | none     | O.E.    | 195/65-R15 | 15       | 205/65-15 | 205 /65 R15 | tire result | wheel result |

  @dt
  @at
  @dtd
  @web
  @19606
  Scenario Outline: HYBRIS_FITMENT_FITMENT_optional Sizes-Shop tires that fit (ALM#19606)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<Diameter>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY SIZE" menu option
    Then I verify all of the fitment menus are displayed
    When I click on the "<Width>" button
    Then I verify all of the fitment menus are displayed
    When I click on the "<Ratio>" button
    Then I verify all of the fitment menus are displayed
    When I click on the "<Diameter1>" button
    Then I verify all of the fitment menus are displayed
    When I click on the "VIEW TIRES" button
    Then I verify the "PLP" banner color is "Red"
    And  I verify all the products listed on plp are of "<fitmentOptionSize>"
    When I click on the "Shop tires that fit" link
    Then I verify the "PLP" banner color is "Green"
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "Non-OE Tire" fitment banner with "<Non-OELabel>" and "<TireSize>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Diameter | TireSize  | Width | Ratio | Diameter1 | FitmentOption | Header      | fitmentOptionSize | Non-OELabel |
      | 2012 | Honda | Civic | Coupe DX | none     | 16       | 205/55-16 | 165   | 70    | 13        | tire          | tire result | 165 /70 R13       |  +1         |

  @dt
  @at
  @dtd
  @web
  @19607
  Scenario Outline: HYBRIS_FITMENT_FITMENT_optional Sizes-Shop tires that fit (ALM#19607)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<Diameter>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    And  I select view brands that don't fit your vehicle dropdown
    And  I select "VISION" from the Product Brands page
    Then I verify the "BRANDS" banner color is "Red"
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "Non-OE Tire" fitment banner with "<Non-OELabel>" and "<TireSize>"
    When I click on the "Shop wheels that fit" link
    Then I verify the "PLP" banner color is "Green"
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "Non-OE Tire" fitment banner with "<Non-OELabel>" and "<TireSize>"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify all the products listed on plp are of "<fitmentOptionSize>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Diameter | TireSize  | FitmentOption | Header       | fitmentOptionSize | Non-OELabel |
      | 2012 | Honda | Civic | Coupe DX | none     | 16       | 205/55-16 | wheel         | wheel result | 16                | +1          |

  @dt
  @at
  @dtd
  @web
  @19609
  Scenario Outline: HYBRIS_FITMENT_FITMENT_NEW_FITMENT_My Vehicle Modal_Edit Vehicle_Clear Cart Scenario (ALM#19609)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I open the My Vehicles popup
    And  I select edit vehicle link
    Then I verify fitment popup page displays
    And  I verify all of the fitment menus are displayed
    When I click on the "Year" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year1>" "<Make1>" "<Model1>" and "<Trim1>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Year1 | Make1     | Model1   | Trim1 |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | 2010  | Chevrolet | Corvette | Base  |

  @dt
  @at
  @web
  @19610
  Scenario Outline: HYBRIS_FITMENT_FITMENT_My Vehicles Modal - Fitment Edit Vehicle_BOPIS Flow (ALM#19610)
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install with appointment
    Then I verify "install with appointment" option is enabled on the Checkout page
    When I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BopisCustomer>"
    And  I place the order for "<BopisCustomer>"
    Then I am brought to the order confirmation page
    When I open the My Vehicles popup
    And  I select edit vehicle link
    Then I verify all of the fitment menus are displayed
    When I click on the "Year" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify that My Vehicles displays "<Year> <Make> <Model>" as the current vehicle

    Examples:
      | Year | Make   | Model | Trim | Assembly       | Customer            | BopisCustomer               | Credit Card |
      | 2016 | Toyota | Camry | SE   | 215 /55 R17 SL | default_customer_az | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @dt
  @at
  @dtd
  @web
  @19604
  Scenario Outline: HYBRIS_FITMENT_FITMENT Optional Sizes - Shop Tires Modal - Fitment Component - Standard OE Vehicles (ALM#19604)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    When I select a fitment option "<FitmentOption>"
    Then I verify the "go back" link is displayed
    And  I am brought to the page with header "<Header>"
    And  I should see the fitment panel page with fitment options
    When I select "Treadwell" option
    And  I select view recommended tires
    Then I verify "Regular" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<TireSize>"
    And  I verify that the sort by dropdown value is set to "<Value>"
    And  I verify that the treadwell filter is "present" in the first position in quick filters
    And  I verify the "Treadwell" checkbox to be "selected" by default
    When I select edit treadwell driving details on plp page
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<TireSize>" is displayed
    When I select edit icon on "treadwell modal"
    And  I select a fitment option "tire"
    And  I select "On Promotion" option
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    And  I verify the "On Promotion" checkbox to be "selected" by default
    When I select edit vehicle link
    And  I select a fitment option "<FitmentOption>"
    And  I select "Best Seller" option
    Then I verify that the sort by dropdown value is set to "Best Seller"
    When I select edit vehicle link
    And  I select a fitment option "tire"
    And  I select "Highest Rated" option
    Then I verify that the sort by dropdown value is set to "Highest Rated"
    When I select edit vehicle link
    And  I select a fitment option "tire"
    And  I select "Original Equipment" option
    Then I verify that the sort by dropdown value is set to "Best Seller"
    And  I verify the "Original Equipment" checkbox to be "selected" by default

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Header            | OELabel | TireSize   | Value       |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | Choose Your Tires | O.E     | 195/65-R15 | Recommended |

  @dt
  @at
  @dtd
  @web
  @19652
  Scenario Outline:HYBRIS_FITMENT_FITMENT_Fitment Optional Sizes- Adding vehicle from Fitment, deleting or clear reacent search from My Vehicles Modal, closing the modal, homepage with year make modal should display (ALM#19652)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    Then I verify "default" My Vehicles popup displays add vehicle
    When I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    Then I verify 'My Vehicles' section displays only "0" vehicles on "Homepage"
    And  I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @at
  @dtd
  @web
  @19627
  Scenario Outline: HYBRIS_FITMENT_FITMENT -Fitment Optional Sizes - Empty Cart Page(ALM#19627)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I remove the item from the cart
    And  I click on the "View All Tires" link
    Then I verify the PLP header message contains "<HeaderTire>"
    When I navigate back to previous page
    And  I click on the "View All Wheels" link
    Then I verify the PLP header message contains "<HeaderWheel>"
    When I navigate back to previous page
    And  I click on the "Shop by vehicle, size or brand" link
    Then I verify all of the fitment menus are displayed
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<HeaderTire>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I remove the item from the cart
    Then I should see product has been "removed" in cart message
    And  I verify all of the fitment menus are displayed
    And  I should see the fitment panel with vehicle "<Year> <Make>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | HeaderTire  | HeaderWheel  |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | tire result | wheel result |

  @dt
  @at
  @dtd
  @web
  @19653
  Scenario Outline: HYBRIS_FITMENT_FITMENT Fitment Optional Sizes - Fitment-Adding a new vehicle with different size/Changing size of one vehicle updates for all other vehicles on fitment(ALM#19653)
  """TODO: The Fitment modal on homepage does not display the type of fitment added, i.e. O.E or Optional size. This is why we have to choose the vehicle based on position.
             The type of fitment will be displayed as part of future enhancement and we will be able to change this step"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select back to my vehicles button
    Then I verify 'My Vehicles' section displays only "1" vehicles on "My Vehicles modal"
    When I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select back to my vehicles button
    Then I verify 'My Vehicles' section displays only "2" vehicles on "My Vehicles modal"
    When I select vehicle at position "2" on fitment modal
    Then I verify "Regular" Vehicle Details Banner on "My Vehicles Modal" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  | OELabel | OETireSize |
      | 2012 | Honda | Civic | Coupe DX | none     | 17         | 215/45-17 | O.E     | 195/65-R15 |