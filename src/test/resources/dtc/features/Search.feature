@search
Feature: Search

  Background:
    Given I change to the default store

  @at
  @dt
  @web
  @bba
  @dtd
  @bvt
  @8864
  @8867
  @8868
  @8790
  @8789
  @8782
  @mobile
  @smokeTest
  @searchBBA
  @bvtSolrSearch
  Scenario Outline: Search by Vehicle using the Homepage menu - tire (ALM #8864, 8867, 8868, 8790, 8789, 8782)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"

    Examples:
      | Year | Make  | Model | Trim         | Assembly       | FitmentOption | Header      |
      | 2012 | Honda | Civic | Coupe DX     | none           | tire          | tire result |
      | 2016 | Ram   | 2500  | Mega Cab 2WD | 275 /70 R18 E1 | tire          | tire result |
      | 2016 | Ram   | 3500  | Dually 2WD   | none           | tire          | tire result |

  @at
  @dt
  @bba
  @dtd
  @bvt
  @8864
  @8867
  @8868
  @8790
  @8789
  @8782
  @web
  @mobile
  @searchBBA
  @smokeTest
  @bvtSolrSearch
  Scenario Outline: Search by Vehicle using the Homepage menu - wheel(ALM #8864, 8867, 8868, 8790, 8789, 8782)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"

    Examples:
      | Year | Make  | Model  | Trim         | Assembly       | FitmentOption | Header       |
      | 2015 | Honda | Accord | Coupe EX     | none           | wheel         | wheel result |
      | 2012 | Honda | Civic  | Coupe DX     | none           | wheel         | wheel result |
      | 2016 | Ram   | 2500   | Mega Cab 2WD | 275 /70 R18 E1 | wheel         | wheel result |
      | 2016 | Ram   | 3500   | 2WD Crew Cab | 275 /70 R18 E1 | wheel         | wheel result |

  @at
  @dt
  @bba
  @web
  @dtd
  @8491
  @8916
  @mobile
  @searchBBA
  Scenario Outline: Search by Tire Size using the Homepage menu(ALM #8916, 8491)
    When I do a "homepage" "tire" size search with details "<Width>, <Ratio>, <Diameter>"
    Then I verify the PLP header message contains "<Header>"
    And  I am brought to the page with path "/fitmentresult/tires/size/205-55-16"
    And  The search results show tires/wheels with the specified measurements "<Measurements>"

    Examples:
      | Width | Ratio | Diameter | Header               | Measurements |
      | 205   | 55    | 16       | Shop 205/55R16 Tires | 205 /55 R16  |

  @at
  @dt
  @web
  @8916
  @searchByTireSizeViaMyVehicles
  Scenario Outline: Search by Tire Size using My Vehicles search(ALM #8916)
    When I do a "my vehicles" "tire" size search with details "<Width>, <Ratio>, <Diameter>"
    Then I verify the PLP header message contains "<Header>"
    And  The search results show tires/wheels with the specified measurements "<Measurements>"

    Examples:
      | Width | Ratio | Diameter | Header               | Measurements |
      | 205   | 55    | 16       | Shop 205/55R16 Tires | 205 /55 R16  |

  @at
  @dt
  @bba
  @web
  @8861
  @mobile
  @searchBBA
  @searchByBrandViaHomepage
  Scenario Outline: Search by Category Brand (ALM #8861)
    When I do a "homepage" "Brand" "tire" search with option: "<Brand>"
    Then I see the "<Brand>" that I selected
    When I select "<SubCategory>" to shop
    Then I can see "<Brand>" PLP page

    Examples:
      | Brand          | SubCategory      |
      | Michelin Tires | All-Season tires |

  @at
  @dt
  @bba
  @web
  @dtd
  @8862
  @searchBBA
  Scenario Outline: DTD - Search by Tire Brand (ALM #8862)
    When I do a "homepage" "Brand" "tire" search with option: "<Brand>"
    Then I see the "<Brand>" that I selected
    When I select "<SubCategory>" to shop
    Then I can see "<Brand>" PLP page
    Then I am brought to the page with path "<Expected Path>"

    Examples:
      | Brand            | SubCategory      | Expected Path            |
      | BFGoodrich Tires | All-Season tires | /tires/brands/bfgoodrich |

  @at
  @dt
  @bba
  @dtd
  @8862
  @mobile
  @searchBBA
  Scenario Outline: DTD Mobile - Search by Tire Brand (ALM #8862)
    When I do a "homepage" "Brand" "tire" search with option: "<Brand>"
    Then I see the "<Brand>" that I selected
    When I select "<SubCategory>" to shop
    Then I can see "<Brand>" PLP page

    Examples:
      | Brand          | SubCategory      |
      | Goodyear Tires | All-Season tires |

  @at
  @dt
  @web
  @dtd
  @5414
  @6358
  @6359
  @8849
  @regression
  @searchRegression
  @staggeredExperience
  @filterSortingByPrice
  Scenario Outline: Search filter sorting by Price (ALM #5414, 6359, 6358, 8849)
    When I do a free text search for "tires"
    And  I click on the "All-Season" link
    And  I select from the "Tire Category" filter section, "single" option(s): "Performance"
    And  I select the "<Value>" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "<Value>"
    And  I verify the results list is sorted in "<Order>" order by "price"
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store

    Examples:
      | ZipCode | Value               | Order      |
      | 32119   | Price (High to Low) | Descending |
      | 32119   | Price (Low to High) | Ascending  |

  @at
  @dt
  @dtd
  @web
  @5356
  @smoketest
  @regression
  @searchRegression
  @filterSortingClearAll
  Scenario Outline: Search filter sorting with Clear All (ALM #5356)
    When I do a free text search for "tires" and hit enter
    And  I select "All-Season tires" from the Product Brand page
    And  I select shop all from the Product Brand page
    And  I select the "Price (High to Low)" from the Sort By dropdown box
    And  I select from the "Brands" filter section, "single" option(s): "<Brand>"
    And  I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    And  I verify that the search refinement filters contain the "multiple" value(s): "<Brand>"
    When I clear all the currently active filters on the PLP page
    Then I verify no search refinement filters are being applied

    Examples:
      | Brand    |
      | Yokohama |

  @at
  @dt
  @dtd
  @5356
  @mobile
  @defect
  @regression
  @searchRegression
  @filterSortingClearAll
  Scenario Outline: Search filter sorting with Clear All - Mobile (ALM #5356)
  """ This will fail due to defect 10376. Clicking APPLY FILTERS the second time (i.e. in this case, after the price
    range selection) will cause the page to load indefinitely """
    When I do a free text search for "tires"
    And  I select the "Price (High to Low)" from the Sort By dropdown box
    And  I select from the "Brands" filter section, "multiple" option(s): "<Filters>"
    And  I set the "Price Range" slider filter to the range: $"<Minimum Price>" - "<Maximum Price>"
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filters>, $<Minimum Price> - $<Maximum Price>"
    When I clear all the currently active filters on the PLP page
    Then I verify no search refinement filters are being applied

    Examples:
      | Minimum Price | Maximum Price | Filters                   |
      | 125           | 225           | American Outlaw, Michelin |

  @at
  @dt
  @web
  @9929
  @defect
  @searchTireFiltering
  Scenario Outline: Search Tire Filtering Brand Filtering Refinements Validation(ALM #9929)
  """ Fails due to facet not selected in PLP after selecting All-Season tires and then View All from the Brands page.
  Defect 11539 has been raised with CapGemini to fix with Dynamic Category Pages feature. """
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "Tire Type" View All link in the header
    And  I select "All-Season tires" from the Product Brand page
    And  I do a "my vehicles" "tire" size search with details "<Width>, <Ratio>, <Diameter>"
    Then The search results show tires/wheels with the specified measurements "<Measurements>"
    When I "expand" the "<Facet>" filter section
    Then I verify the "<Refinement>" filter checkbox to be "deselected"
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "Tire Type" View All link in the header
    And  I select "All-Season tires" from the Product Brand page
    And  I "expand" the "<Facet>" filter section
    Then I verify that the search refinement filters contain the "single" value(s): "<Refinement>"
    And  I verify the "<Refinement>" filter checkbox to be "selected"
    When I "expand" the "<Facet>" filter section
    And  I select the "<Refinement>" filter to uncheck the checkbox
    And  I "expand" the "<Facet>" filter section
    Then I verify the "<Refinement>" filter checkbox to be "deselected"
    And  I verify no search refinement filters are being applied

    Examples:
      | Width | Ratio | Diameter | Measurements | Refinement | Facet         |
      | 195   | 65    | 15       | 195 /65 R15  | All-Season | Tire Category |

  @at
  @dt
  @web
  @20348
  @staggeredExperience
  @searchTireFiltering
  Scenario Outline: Search Tire Filtering Brand Filtering Refinements Validation(ALM #20348)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select from the "Good better best" filter section, "single" option(s): "Best"
    Then I verify that the search refinement filters contain the "single" value(s): "Best"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify that the search refinement filters contain the "single" value(s): "Best"
    When I select "REAR" staggered tab on PLP result page
    Then I verify that the search refinement filters contain the "single" value(s): "Best"
    When I clear all the currently active filters on the PLP page
    And  I select "SETS" staggered tab on PLP result page
    Then I verify no search refinement filters are being applied

    Examples:
      | Year | Make      | Model    | Trim | Assembly | Header  |
      | 2010 | Chevrolet | Corvette | Base | none     | Results |

  @at
  @dt
  @web
  @20553
  @staggeredExperience
  @searchTireFiltering
  Scenario Outline: HYBRIS_SEARCH_SEARCH_STAGGERED EXPERIENCE_Verify that the user has faceting options on the PLP sets tab_TIRES(ALM #20553)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    Then I verify the PLP header message contains "results"

    Examples:
      | Year | Make      | Model    | Trim | Assembly |
      | 2010 | Chevrolet | Corvette | Base | none     |

  @at
  @dt
  @web
  @20337
  @staggeredExperience
  @searchTireFiltering
  Scenario Outline: HYBRIS_FITMENT_FITMENT_NEW_Fitment Optional Sizes_Staggered VehicleTreadwell Flow(ALM #20337)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire"
    And  I select "Treadwell" option
    And  I select view recommended tires
    Then I verify the "Quick Filters, Brands, Price Range, Ratings, Speed Rating, Good Better Best" filter section(s) is/are displayed
    When I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    Then I verify the PLP header message contains "results"

    Examples:
      | Year | Make      | Model    | Trim | Assembly |
      | 2010 | Chevrolet | Corvette | Base | none     |

  @at
  @dt
  @web
  @20579
  @staggeredExperience
  @searchTireFiltering
  Scenario Outline: HYBRIS_SEARCH_SEARCH_STAGGERED EXPERIENCE_Verify that the user has faceting options on the PLP sets tab_WHEELS(ALM #20579)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the "Brands, Price Range, Wheel Category, Wheel Color, Wheel Width" filter section(s) is/are displayed
    When I select from the "Quick Filters" filter section, "single" option(s): "On Promotion"
    Then I verify the PLP header message contains "results"

    Examples:
      | Year | Make      | Model    | Trim       | Assembly |
      | 2010 | Chevrolet | Corvette | Gran Sport | none     |

  @at
  @dt
  @web
  @9777
  @mobile
  @vehicleNotMatchTire
  Scenario Outline:  Mobile - Hybris_Mobile_PLP_UI_Vehicle_Fitment_Match_Tire (ALM#9777)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify that the sort by dropdown value is set to "Best Seller"
    And  I verify PLP UI "sorting options"
    When I select the first "2" results to compare
    Then I verify the Add To Cart button is clickable and Red on "PLP" page
    When I enter "<Quantity>" into the first item quantity text box
    Then I see the Please Enter a Number error message
    And  I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "PLP" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Quantity |
      | 2012 | Honda | Civic | Coupe DX | none     | 0        |

  @at
  @dt
  @web
  @9776
  @mobile
  @vehiclePLPNotMatchTire
  Scenario Outline:  Mobile - Hybris_Mobile_PLP_UI_Vehicle_Fitment_Not_Match_Tire (ALM#9776)
    When I do a "homepage" "Brand" "tire" search with option: "<Brand>"
    And  I select "<SubCategory>" to shop
    Then I verify the "PLP" banner color is "Yellow"
    And  I verify the message on the "PLP" banner contains "We're not sure if these items will fit"
    And  I verify that the sort by dropdown value is set to "Relevance"
    And  I verify PLP UI "sorting options"
    When I select the first "2" results to compare
    And  I verify the Add To Cart button is clickable and Red on "PLP" page
    When I enter "<Quantity>" into the first item quantity text box
    Then I see the Please Enter a Number error message
    And  I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "PLP" page

    Examples:
      | Brand | SubCategory      | Quantity |
      | Nitto | All-Season tires | 0        |

  @at
  @dt
  @web
  @9778
  @mobile
  @wheelPDPVehicleNotSelected
  Scenario Outline: Hybris_Mobile_PDP_UI_Vehicle_Not_selected_Wheel_TC04 (ALM#9778)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I verify the "PDP" banner color is "Yellow"
    And  I verify the message on the "PDP" banner contains "Enter your vehicle to ensure these wheels fit"
    And  I verify the Add To Cart button is clickable and Red on "PDP" page
    When I enter "<Quantity>" into the first item quantity text box
    Then I see the Please Enter a Number error message

    Examples:
      | ItemCode | Quantity | ProductName |
      | 73745    | 0        | Optima      |

  @at
  @dt
  @dtd
  @web
  @bba
  @9066
  @20591
  @searchBBA
  Scenario Outline: HYBRIS_Search_Search Tires by Filters and Sorting (ALM#9066, 20591)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY BRAND" menu option
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "UPDATE VEHICLE" link
    And  I click on the "Continental" link
    And  I select "All-Season tires" from the Product Brand page
    Then I verify that the sort by dropdown value is set to "Best Seller"
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    And  I verify the results list is sorted in "Ascending" order by "price"
    When I select the "Price (High to Low)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (High to Low)"
    And  I verify the results list is sorted in "Descending" order by "price"
    When I go to the homepage
    And  I open the "Wheels" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    And  I click on the "TSW" link
    And  I click on the "SHOP ALL TSW WHEELS" link
    Then I verify that the sort by dropdown value is set to "Best Seller"
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    And  I verify the results list is sorted in "Ascending" order by "price"
    When I go to the homepage
    And  I do a free text search for "Michelin" and hit enter
    Then I verify the PLP header message contains "results"
    And  I verify that the sort by dropdown value is set to "Relevance"

    Examples:
      | Year | Make | Model | Trim  | Assembly       |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL |

  @at
  @dt
  @bba
  @dtd
  @9066
  @mobile
  @searchBBA
  Scenario: HYBRIS_164_Search_Search Tires by Filters and Sorting_Mobile (ALM #9066)
    When I click the mobile homepage menu
    And  I click on "Tire" menu link
    And  I click on "Tire Brand" menu link
    And  I click on "Michelin Tires" menu link
    And  I select "All-Season tires" from the Product Brand page
    And  I select the "Name (Descending)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Name (Descending)"
    And  I verify the results list is sorted in "Descending" order by "name"

  @at
  @dt
  @bba
  @dtd
  @9066
  @mobile
  @searchBBA
  Scenario: Mobile - HYBRIS_164_Search_Search Tires by Filters and Sorting (ALM #9066)
    When I click the mobile homepage menu
    And  I click on "Tire" menu link
    And  I click on "Tire Brand" menu link
    And  I click on "View all" menu link
    And  I select the "Michelin Tires" brand image
    And  I select "All-Season tires" from the Product Brand page
    And  I select the "Name (Ascending)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Name (Ascending)"
    And  I verify the results list is sorted in "Ascending" order by "name"

  @dt
  @at
  @web
  @8873
  @storeHourFormatting
  Scenario Outline: HYBRIS_162_Search_Search for Store hours (ALM #8873)
    When I click on "My Store" title
    Then I verify the "STORE HOURS" in the My Store popup
    When I go to the homepage
    And  I search for store within "75" miles of "<Zipcode>"
    Then I verify the "HOURS OF OPERATION" in the store location results
    When I select "Make This My Store" for store #"2" in the location results
    And  I click on Store details button in My Store popup
    Then I am brought to the page with header "Store Details"

    Examples:
      | Zipcode |
      | 86001   |
      | 96002   |

  @dt
  @at
  @8873
  @mobile
  @storeHourFormatting
  Scenario Outline: Mobile - HYBRIS_162_Search_Search for Store hours (ALM #8873)
    When I search for store within "75" miles of "<Zipcode>"
    Then I verify the "HOURS OF OPERATION" in the store location results
    When I select "<Zipcode>" for store details
    Then I am brought to the page with header "Store Details"

    Examples:
      | Zipcode |
      | 86001   |
      | 96002   |

  @at
  @dt
  @dtd
  @web
  @6609
  @6610
  @6613
  @6614
  @6615
  @6612
  @mobile
  @regression
  @searchRegression
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING BestSelling staggered (ALM #6609, 6610, 6612, 6613, 6614, 6615)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "<Tab>" staggered tab on PLP result page
    Then I verify the PLP header message contains "<Header>"
    When I select the "Best Seller" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Best Seller"

    Examples:
      | Year | Make      | Model    | Trim  | Assembly       | Tab   | FitmentOption | Header  |
      | 2010 | Chevrolet | Corvette | Base  | none           | FRONT | tire          | Results |
      | 2010 | Chevrolet | Corvette | Base  | none           | FRONT | wheel         | Results |
      | 2010 | Chevrolet | Corvette | Base  | none           | REAR  | tire          | Results |
      | 2010 | Chevrolet | Corvette | Base  | none           | REAR  | wheel         | Results |
      | 2014 | BMW       | 640i     | Coupe | 245 /40 R19 SL | REAR  | wheel         | Results |

  @at
  @dt
  @dtd
  @web
  @6606
  @regression
  @searchRegression
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING BestSelling All Tires standard (ALM #6606)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select the "Best Seller" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Best Seller"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header      |
      | 2012 | Honda | Civic | Coupe DX | none     | tire result |

  @at
  @dt
  @dtd
  @web
  @6360
  @20578
  @20593
  @mobile
  @regression
  @searchRegression
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING FilterByInverntory In Stock (ALM #6360, 20578)
    When I do a free text search for "<Search Term>" and hit enter
    Then I verify the PLP header message contains "Results for "<Search Term>""
    When I select from the "<Filter Section>" filter section, "single" option(s): "<Filter Value>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter Value>"
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify optional tire and wheel size is displayed
    And  I verify what are you shopping for heading is displayed
    And  I verify that the "Sub Header" element is not displayed
    And  I verify that the "treadwell" element is not displayed
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify "SETS" staggered option tab is displayed on PLP result page
    And  I verify "FRONT" staggered option tab is displayed on PLP result page
    And  I verify "REAR" staggered option tab is displayed on PLP result page
    When I select from the "Quick Filters" filter section, "single" option(s): "Run Flat"
    And  I select the "First" product result image on "PLP" page
    Then I verify the "RF" size is displayed on PDP page
    When I enter "<Quantity>" into the first item quantity text box
    Then I verify payment plan available is "displayed"
    When I add item to my cart and "View shopping Cart"
    Then I verify payment plan available is "displayed"

    Examples:
      | Search Term | Filter Section | Filter Value | Year | Make      | Model    | Trim | Assembly | OELabel | OETireSize | Header      | Quantity |
      | Michellin   | Quick Filters  | In Stock     | 2010 | Chevrolet | Corvette | Base | None     | O.E.    | 245/40-R18 | tire result | 4        |

  @at
  @dt
  @dtd
  @web
  @5347
  @5642
  @5643
  @5644
  @15448
  @mobile
  @regression
  @searchRegression
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING FilterBySpeedRating Facet Filter on PLP (ALM #5347, 5642, 5643, 5644, 15448)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "Results"
    And  I verify the initial page displays products that match my tire size(s): "<Measurements>"
    When I select from the "<Filter Section>" filter section, "single" option(s): "<Filter Option>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter Option>"

    Examples:
      | Year | Make   | Model  | Trim     | Assembly       | FitmentOption | Measurements | Filter Section       | Filter Option     |
      | 2012 | Honda  | Civic  | Coupe DX | none           | tire          | 195 /65 R15  | Speed Rating         | T - Up to 118 mph |
      | 2012 | Honda  | Civic  | Coupe DX | none           | tire          | 195 /65 R15  | Tire Category        | All-Season        |
      | 2012 | Honda  | Civic  | Coupe DX | none           | tire          | 195 /65 R15  | Mileage Warranty     | 60,000-70,000     |
      | 2012 | Honda  | Civic  | Coupe DX | none           | tire          | 195 /65 R15  | Sidewall Description | Black Side Wall   |
      | 2012 | Honda  | Civic  | Coupe DX | none           | tire          | 195 /65 R15  | Good Better Best     | Better            |
      | 2012 | Honda  | Civic  | Coupe DX | none           | tire          | 195 /65 R15  | Load Range           | SL                |
      | 2012 | Honda  | Civic  | Coupe DX | none           | tire          | 195 /65 R15  | Section Width        | 195               |
      | 2012 | Honda  | Civic  | Coupe DX | none           | tire          | 195 /65 R15  | Aspect Ratio         | 65                |
      | 2015 | Nissan | Altima | Sedan    | 215 /55 R17 SL | tire          | 215 /55 R17  | Diameter             | 17                |

  @at
  @dt
  @dtd
  @web
  @9237
  @regression
  @searchRegression
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING Validate Order Filters On PLP (ALM #9237)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select the "<Product Type>" menu option
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "Results"
    And  I verify the order of filter categories for "<Product Type>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Product Type |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | Tires        |
      | 2012 | Honda | Civic | Coupe DX | none     | wheel         | Wheels       |

  @at
  @dt
  @dtd
  @web
  @5349
  @regression
  @searchRegression
  Scenario: HYBRIS_SEARCH_SEARCH_FILTER SORTING WHEEL FilterByPromotional Offer (ALM #5349)
  """TODO - Fails in QA2 for all regions as no products are currently 'On Promotion'"""
    When I open the "WHEELS" navigation link
    And  I click the "Passenger Wheels" menu option
    And  I select shop all from the Product Brand page
    And  I select from the "Quick Filters" filter section, "single" option(s): "On Promotion"
    Then I verify that the search refinement filters contain the "single" value(s): "On Promotion"
    And  I verify all PLP results are on promotion

  @at
  @dt
  @dtd
  @5349
  @mobile
  @regression
  @searchRegression
  Scenario: HYBRIS_SEARCH_SEARCH_FILTER SORTING WHEEL FilterByPromotional Offer_Mobile (ALM #5349)
  """TODO - Fails in QA2 for all regions as no products are currently 'On Promotion'"""
    When I click the mobile homepage menu
    And  I click on "Wheel" menu link
    And  I click on "Vehicle Type" menu link
    And  I click on "Passenger Wheels" menu link
    And  I select shop all from the Product Brand page
    And  I select from the "Quick Filters" filter section, "single" option(s): "On Promotion"
    Then I verify that the search refinement filters contain the "single" value(s): "On Promotion"
    And  I verify all PLP results are on promotion

  @at
  @dt
  @dtd
  @web
  @5645
  @regression
  @searchRegression
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING Wheel FilterByWheelColor Facet Filter on PLP (ALM #5645)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select the "<Product Type>" menu option
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    When I select from the "<Filter Section>" filter section, "single" option(s): "<Filter Option>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter Option>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | FitmentOption | Filter Section | Filter Option |
      | 2012 | Honda | Civic | Coupe DX | none     | WHEELS       | wheel         | Wheel Color    | Black         |

  @at
  @dt
  @dtd
  @web
  @9258
  @defect
  @regression
  @searchRegression
  Scenario: HYBRIS_SEARCH_SEARCH_Validate By Tire Brand View All Brand Images (ALM #9258)
  """TODO - Fails in QA due to defect #9532"""
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY BRAND" menu option
    Then I verify all listed brands have corresponding images

  @at
  @dt
  @web
  @9369
  @regression
  @searchRegression
  Scenario: HYBRIS_SEARCH_SEARCH_Validate Scheduling Appointment From My Store Service Appointment (ALM #9369)
    When I schedule an appointment for my current store
    Then I am brought to the page with header "Service Appointment"

  @at
  @dt
  @web
  @9367
  @regression
  @searchRegression
  Scenario: HYBRIS_SEARCH_SEARCH_Validate Scheduling Appointment From My Store Store Locator (ALM #9367)
    When I click on "My Store" title
    Then I verify the "My Store" popup contains controls: "CHANGE STORE, STORE DETAILS, SCHEDULE APPOINTMENT"
    When I click on Store details button in My Store popup
    And  I click "Schedule appointment" on "Store Details" page
    Then I am brought to the page with header "Service Appointment"

  @dt
  @at
  @web
  @dtd
  @15686
  @19588
  @mobile
  @solrSearch
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Item Code Free Text Search Goes Directly To PDP(ALM #15686,19588)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I verify the "<Tire Breadcrumb>" link in the breadcrumb container
    And  I should see product detail page with "<ItemName>"
    When I select the "<Brand>" link in the breadcrumb
    And  I click on the "continue without vehicle" link
    Then I verify the "PLP" banner color is "Yellow"
    And  I verify the message on the "PLP" banner contains "Enter your vehicle to ensure these tires fit"
    When I do a free text search for "<GTIN>" and hit enter
    Then I verify the "<Tire Breadcrumb>" link in the breadcrumb container
    And  I should see product detail page with "<GTIN Product Name>"
    When I select the "<Brand>" link in the breadcrumb
    Then I verify the "PLP" banner color is "Yellow"
    And  I verify the message on the "PLP" banner contains "Enter your vehicle to ensure these tires fit"
    When I do a free text search for "<VPN>" and hit enter
    Then I verify the "<Tire Breadcrumb>" link in the breadcrumb container
    And  I should see product detail page with "<ItemName>"
    And  I should see product specification "<VPN Label>" value to be "<VPN>"
    When I select the "<Brand>" link in the breadcrumb
    Then I verify the "PLP" banner color is "Yellow"
    And  I verify the message on the "PLP" banner contains "Enter your vehicle to ensure these tires fit"

    Examples:
      | ItemCode | ItemName     | GTIN         | GTIN Product Name | Tire Breadcrumb                  | Brand    | VPN   | VPN Label                   |
      | 34299    | Defender A/S | 086699040350 | LTX M/S2          | Tires, Michelin, Product Details | Michelin | 10042 | Vendor Product Number / MPN |

  @dt
  @at
  @dtd
  @15720
  @15721
  @15722
  @mobile
  Scenario Outline: HYBRIS_Search_Search_Tires by TIRE TYPE (ALM #15720, 15721, 15722)
    When I click the mobile homepage menu
    And  I click on "<Link1>" menu link
    And  I click on "<Link2>" menu link
    And  I click the "<MenuOption>" menu option
    And  I select the "<Brand>" brand image
    Then I verify the PLP header message contains "<Message>"

    Examples:
      | Link1 | Link2        | MenuOption       | Brand          | Message      |
      | Tire  | Tire Type    | All-Season tires | MICHELIN TIRES | tire result  |
      | Tire  | Vehicle Type | ATV/UTV Tires    | CARLISLE Tires | tire result  |
      | Wheel | Wheel Style  | Painted Wheels   | KONIG Wheels   | wheel result |

  @dt
  @at
  @dtd
  @web
  @15720
  @15721
  @15722
  Scenario Outline: HYBRIS_Search_Search_Tires by TIRE TYPE (ALM #15720, 15721, 15722)
    When I open the "<Link>" navigation link
    And  I click the "<MenuOption>" menu option
    And  I select the "<Brand>" brand image
    Then I verify the PLP header message contains "<Message>"

    Examples:
      | Link   | MenuOption       | Brand          | Message      |
      | TIRES  | All-Season tires | MICHELIN TIRES | tire result  |
      | TIRES  | ATV/UTV Tires    | CARLISLE Tires | tire result  |
      | WHEELS | Painted Wheels   | KONIG Wheels   | wheel result |

  @at
  @dt
  @web
  @15696
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify the inventory messaging rule on PLP with free text search and zero stock at store (ALM #15696)
    When I change to the store with url "<StoreUrl>"
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a free text search for "<Brand>" and hit enter
    And  I select shop all from the Product Brand page
    Then I see "<ItemCode>" on the product list page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "PLP" page
    And  I verify check nearby stores link is displayed for "<ItemCode>" on "PLP" page
    And  I verify 'Need It Now? Call Store' display for "<ItemCode>" on "PLP" page

    Examples:
      | StoreUrl                    | Brand    | ItemCode | InventoryMessage                                     |
      | /store/az/scottsdale/s/1284 | Michelin | 19600    | Order now, available as soon as tomorrow at My Store |
      | /store/az/scottsdale/s/1284 | Michelin | 35356    | Order now, available in 2 days at My Store           |

  @at
  @dt
  @web
  @15715
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify the inventory messaging rule on PLP with fitment for regular vehicle (ALM #15715)
    When I change to the store with url "<StoreUrl>"
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    And  I see "<ItemCode>" on the product list page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "PLP" page
    And  I verify check nearby stores link is displayed for "<ItemCode>" on "PLP" page
    And  I verify 'Need It Now? Call Store' display for "<ItemCode>" on "PLP" page

    Examples:
      | StoreUrl                    | Year | Make   | Model  | Trim     | Assembly       | FitmentOption | ItemCode | InventoryMessage                                     |
      | /store/az/scottsdale/s/1284 | 2015 | Nissan | Altima | Sedan    | 215 /55 R17 SL | tire          | 30576    | Order now, available in 2 days at My Store           |
      | /store/az/scottsdale/s/1284 | 2015 | Nissan | Altima | Sedan    | 215 /55 R17 SL | tire          | 30431    | Order now, available in 3 - 5 days at My Store       |
      | /store/az/scottsdale/s/1284 | 2015 | Nissan | Altima | Sedan    | 215 /55 R17 SL | tire          | 29888    | Order now, available as soon as tomorrow at My Store |
      | /store/az/scottsdale/s/1284 | 2012 | Honda  | Civic  | Coupe DX | none           | tire          | 31098    | Available today at My Store                          |

  @at
  @dt
  @web
  @15712
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify the inventory messaging rule on PLP with free text search and stock available at near by store (ALM #15712)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a free text search for "<Brand>" and hit enter
    And  I select shop all from the Product Brand page
    Then I see "<ItemCode>" on the product list page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "PLP" page
    And  I verify check nearby stores link is displayed for "<ItemCode>" on "PLP" page
    And  I verify 'Need It Now? Call Store' display for "<ItemCode>" on "PLP" page

    Examples:
      | ZipCode | Brand      | ItemCode | InventoryMessage                                     |
      | 85260   | Goodyear   | 30576    | Order now, available in 2 days at My Store           |
      | 85260   | BFGoodrich | 29888    | Order now, available as soon as tomorrow at My Store |

  @at
  @dt
  @web
  @15735
  @15737
  @20366
  @mobile
  @defect
  @extendedAssortment
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify the inventory messaging rule on PLP with with fitment for staggered vehicle (ALM #15735, 15737, 20366)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    And  I see "<ItemCodeA>" on the product list page
    And  I see "<ItemCodeB>" on the product list page
    And  I verify the "<InventoryMessage>" for set displayed for "<ItemCodeA>" on "PLP" page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>" on "PLP" page
    And  I verify 'Need It Now? Call Store' display for "<ItemCodeA>" on "PLP" page
    When I click on the product "<ItemCodeA>"
    Then I verify the "<InventoryMessage>" for set displayed for "<ItemCodeA>" on "PDP" page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>" on "PDP" page
    And  I verify the stock count message on PDP
    And  I verify 'Need It Now? Call Store' display for "<ItemCodeA>" on "PDP" page
    When I add item to my cart and "View shopping Cart"
    Then I verify the "<InventoryMessage>" for set displayed for "<ItemCodeA>" on "Shopping Cart" page
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click continue for appointment customer details page
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I verify the "<InventoryMessage>" for set displayed for "<ItemCodeA>" on "Order Confirmation" page

    Examples:
      | VehicleType         | ItemCodeA | ItemCodeB | InventoryMessage                   | Reason                              | Customer            |
      | VEHICLE_STAGGERED_1 | 27142     | 27141     | Order now, available in 3 - 5 days | Make an appointment at a later time | DEFAULT_CUSTOMER_AZ |

  @dt
  @at
  @dtd
  @web
  @core
  @15718
  @coreScenarioSearch
  @vehiclePresentation
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify fit indicator with Green PLP and PDP Banner for search product by brand in dt (ALM#15718)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I go to the homepage
    And  I open the "<Taxonomy>" navigation link
    And  I click on "View all" menu link
    And  I select the "<Brand>" brand image
    And  I select "<SubCategory>" to shop
    Then I verify the PLP header message contains "<Header>"
    And  I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "<PLPFitMessage>"
    When I select the "First" available product result image on PLP page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "<PLPFitMessage>"

    Examples:
      | VehicleType             | Taxonomy | Brand     | Header       | SubCategory      | PLPFitMessage                 |
      | VEHICLE_NON_STAGGERED_1 | TIRES    | First     | tire result  | All-Season tires | These tires fit your vehicle  |
      | VEHICLE_NON_STAGGERED_1 | WHEELS   | MB WHEELS | wheel result | Passenger wheels | These wheels fit your vehicle |
      | VEHICLE_STAGGERED_1     | TIRES    | First     | tire result  | All-Season tires | These tires fit your vehicle  |
      | VEHICLE_STAGGERED_1     | WHEELS   | TSW       | wheel result | SHOP ALL         | These wheels fit your vehicle |

  @at
  @dt
  @web
  @15741
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify inventory messaging rule on PDP with free text search and zero stock at store (ALM #15741)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ItemName>"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "PDP" page
    And  I verify check nearby stores link is displayed for "<ItemCode>" on "PDP" page
    And  I verify the stock count message on PDP
    And  I verify 'Need It Now? Call Store' display for "<ItemCode>" on "PDP" page

    Examples:
      | ZipCode | ItemCode | InventoryMessage                           | ItemName                      |
      | 85260   | 29935    | Order now, available as soon as tomorrow at My Store  | Silver Edition III |
      | 85260   | 35356    | Order now, available in 2 days at My Store | Primacy MXM4                  |

  @at
  @dt
  @web
  @15743
  @mobile
  @extendedAssortment
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify inventory messaging rule on PDP with fitment (ALM #15743)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    And  I see "<ItemCode>" on the product list page
    When I click on the product "<ItemCode>"
    Then I should see product detail page with "<ItemName>"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "PDP" page
    And  I verify check nearby stores link is displayed for "<ItemCode>" on "PDP" page
    And  I verify the stock count message on PDP
    And  I verify 'Need It Now? Call Store' display for "<ItemCode>" on "PDP" page

    Examples:
      | Year | Make      | Model    | Trim  | Assembly       | ItemCode | InventoryMessage                               | ItemName            |
      | 2015 | Nissan    | Altima   | Sedan | 215 /55 R17 SL | 28120    | Order now, available in 3 - 5 days at My Store | ULTRA GRIP WRT      |
      | 2015 | Nissan    | Altima   | Sedan | 215 /55 R17 SL | 19178    | Order now, available as soon as tomorrow       | SPORT A/S           |
      | 2015 | Nissan    | Altima   | Sedan | 215 /55 R17 SL | 14293    | Available today at My Store                    | PRO CONTACT         |
      | 2010 | Chevrolet | Corvette | Base  | none           | 27142    | Order now, available in 3 - 5 days             | EAGLE F1 ASYMMETRIC |
      | 2010 | Chevrolet | Corvette | Base  | none           | 28653    | Available today at My Store                    | POTENZA RE050A      |

  @at
  @dt
  @web
  @15742
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify inventory messaging rule on PDP with free text search and stock available at near by store (ALM #15742)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ItemName>"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "PDP" page
    And  I verify check nearby stores link is displayed for "<ItemCode>" on "PDP" page
    And  I verify the stock count message on PDP
    And  I verify 'Need It Now? Call Store' display for "<ItemCode>" on "PDP" page

    Examples:
      | ZipCode | ItemCode | InventoryMessage                                     | ItemName           |
      | 85260   | 30576    | Order now, available in 2 days at My Store           | Eagle RS-A         |
      | 85260   | 29888    | Order now, available as soon as tomorrow at My Store | G-Force Comp 2 A/S |

  @at
  @dt
  @web
  @15744
  @15745
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_EXTENDEDASSORTMENT_Verify inventory messaging rule on PDP with fitment for staggered vehicle (ALM #15744,15745)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    And  I see "<ItemCodeA>" on the product list page
    When I click on the product "<ItemCodeA>"
    Then I should see "<ItemCodeA>" on product details page
    And  I should see "<ItemCodeB>" on product details page
    And  I verify the "<InventoryMessageA>" for set displayed for "<ItemCodeA>" on "PDP" page
    And  I verify the "<InventoryMessageB>" for set displayed for "<ItemCodeB>" on "PDP" page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>" on "PDP" page
    And  I verify 'Need It Now? Call Store' display for "<ItemCodeA>" on "PDP" page

    Examples:
      | ZipCode | Year | Make      | Model    | Trim       | Assembly       | FitmentOption | ItemCodeA | ItemCodeB | InventoryMessageA                                    | InventoryMessageB                                    |
      | 85260   | 2010 | Chevrolet | Corvette | Base       | none           | tire          | 30018     | 30019     | Order now, available in 3 - 5 days at My Store       | Order now, available in 3 - 5 days at My Store       |
      | 85260   | 2010 | Chevrolet | Corvette | Base       | none           | tire          | 37068     | 14777     | Order now, available as soon as tomorrow at My Store | Order now, available as soon as tomorrow at My Store |
      | 85260   | 2010 | Nissan    | 370Z     | Coupe Base | 225 /50 R18 SL | tire          | 17167     | 17177     | Order now, available in 3 - 5 days at My Store       | Available today at My Store                          |

  @dt
  @at
  @dtd
  @web
  @15565
  @mobile
  Scenario Outline: HYBRIS_Search_Search_PLP displays Top 3 Tiles for Standard Tires Default when all Tire data exists(OE,Our Recommendation,Best Seller and Highest Rated ) are returned ( ALM # 15565)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify the PLP displays 'Top 3 Tiles' below the PLP header
    And  I verify the PLP displays "Original Equipment","Our Recommendation","Best Seller" in the 'Top 3 Tiles'
    When I extract the product at position "1" from "PLP"
    Then I verify the product is displayed at position "3" in the 'Top 3 Tiles'
    When I select the "Original Equipment" checkbox
    Then I verify Original Equipment tire is displayed on "PLP" page
    When I extract the product at position "1" from "PLP"
    Then I verify the product is displayed at position "1" in the 'Top 3 Tiles'
    And  I verify "Michelin" is displayed at position "2" in the 'Top 3 Tiles'

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Header      |
      | 2012 | HONDA | CIVIC | COUPE DX | none     | tire          | tire result |

  @dt
  @at
  @dtd
  @web
  @15577
  @staggeredExperience
  Scenario Outline: HYBRIS_Search_Search_PLP displays Top 3 Tiles for staggered  Tires Default when all Tire data exists(OE,Our Recommendation,Best Seller and Highest Rated) are returned (ALM #15577)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify the PLP displays 'Top 3 Tiles' below the PLP header
    And  I verify the PLP displays "Original Equipment","Our Recommendation","Best Seller" in the 'Top 3 Tiles'
    When I extract the product at position "1" from "PLP"
    Then I verify the product is displayed at position "3" in the 'Top 3 Tiles'
    When I select the "Original Equipment" checkbox
    Then I verify Original Equipment tire is displayed on "PLP" page
    When I extract the product at position "1" from "PLP"
    Then I verify the product is displayed at position "1" in the 'Top 3 Tiles'
    And  I verify "<Brand>" is displayed at position "2" in the 'Top 3 Tiles'
    When I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | FitmentOption | Header      | Brand    |
      | 2010 | CHEVROLET | CORVETTE | BASE | none     | tire          | tire result | MICHELIN |

  @dt
  @web
  @15705
  @15708
  @15709
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify store inventory in check near by stores modal for the product selected from PLP, PDP, Cart Page Regular fitment (ALM #15705, 15708, 15709)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I see "<ItemCode>" on the product list page
    And  I verify check nearby stores link is displayed for "<ItemCode>" on "PLP" page
    When I select the "Check nearby stores" link for item "<ItemCode>" on "PLP" page
    Then I should verify that the Check Availability popup loaded
    And  I verify the "default" store MY STORE is displayed at top
    And  I verify the nearby store is displayed when directed from "PLP"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "Check Availability" page
    And  I verify the stock count message on "Check Availability" page for "<ItemCode>"
    When I close the Check Availability popup
    Then I see "<ItemCode>" on the product list page
    When I click on the product "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    And  I verify check nearby stores link is displayed for "<ItemCode>" on "PDP" page
    When I select the "Check nearby stores" link for item "<ItemCode>" on "PDP" page
    Then I should verify that the Check Availability popup loaded
    And  I verify the "default" store MY STORE is displayed at top
    And  I verify the nearby store is displayed when directed from "PDP"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "Check Availability" page
    And  I verify the stock count message on "Check Availability" page for "<ItemCode>"
    When I close the Check Availability popup
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify check nearby stores link is displayed for "<ItemCode>"
    When I select the "Check nearby stores" link for item "<ItemCode>" on "Shopping Cart" page
    Then I should verify that the Check Availability popup loaded
    And  I verify "MY STORE" is displayed on check inventory
    And  I verify "Nearby Stores" is displayed on check inventory
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "Check Availability" page
    And  I verify the stock count message on "Check Inventory" page for "<ItemCode>"
    When I close the Check Inventory popup
    Then I should see product "<ItemCode>" on the "cart" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | ItemCode | InventoryMessage                         | ProductName |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | 43006    | Available today                          | YK580       |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | 17737    | Order now, available as soon as tomorrow | GT Eco      |

  @dt
  @web
  @15762
  @15763
  @15962
  @mobile
  @extendedAssortment
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify store inventory in check near by stores modal for the product selected from PLP, PDP, Cart Page Staggered fitment (ALM #15762, 15763, 15962)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    And  I see "<ItemCodeA>" on the product list page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>" on "PLP" page
    When I select the "Check nearby stores" link for item "<ItemCodeA>" on "PLP" page
    Then I should verify that the Check Availability popup loaded
    And  I verify the "default" store MY STORE is displayed at top
    And  I verify the nearby store is displayed when directed from "PLP"
    And  I verify the "<CheckAvailabilityInventoryMessageA>" for "Front" product "<ItemCodeA>" on "Check Availability"
    And  I verify the "<CheckAvailabilityInventoryMessageB>" for "Rear" product "<ItemCodeB>" on "Check Availability"
    When I close popup modal
    Then I see "<ItemCodeA>" on the product list page
    When I click on the product "<ItemCodeA>"
    Then I should see "<ItemCodeA>" on product details page
    And  I should see "<ItemCodeB>" on product details page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>" on "PDP" page
    When I select the "Check nearby stores" link for item "<ItemCodeA>" on "PDP" page
    Then I should verify that the Check Availability popup loaded
    And  I verify the "default" store MY STORE is displayed at top
    And  I verify the nearby store is displayed when directed from "PDP"
    And  I verify the "<CheckAvailabilityInventoryMessageA>" for "Front" product "<ItemCodeA>" on "Check Availability"
    And  I verify the "<CheckAvailabilityInventoryMessageB>" for "Rear" product "<ItemCodeB>" on "Check Availability"
    When I close popup modal
    Then I should see "<ItemCodeA>" on product details page
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>"
    And  I verify check nearby stores link is displayed for "<ItemCodeB>"
    When I select the "Check nearby stores" link for item "<ItemCodeA>" on "Shopping Cart" page
    Then I should verify that the Check Availability popup loaded
    And  I verify "MY STORE" is displayed on check inventory
    And  I verify "Nearby Stores" is displayed on check inventory
    And  I verify the "<CheckAvailabilityInventoryMessageA>" for "Front" product "<ItemCodeA>" on "Check Availability"
    And  I verify the "<CheckAvailabilityInventoryMessageB>" for "Rear" product "<ItemCodeB>" on "Check Availability"
    When I close popup modal
    Then I should see product "<ItemCodeA>" on the "cart" page
    When I select the "Check nearby stores" link for item "<ItemCodeB>" on "Shopping Cart" page
    Then I should verify that the Check Availability popup loaded
    And  I verify "MY STORE" is displayed on check inventory
    And  I verify "Nearby Stores" is displayed on check inventory
    And  I verify the "<CheckAvailabilityInventoryMessageA>" for "Front" product "<ItemCodeA>" on "Check Availability"
    And  I verify the "<CheckAvailabilityInventoryMessageB>" for "Rear" product "<ItemCodeB>" on "Check Availability"
    When I close popup modal
    Then I should see product "<ItemCodeB>" on the "cart" page

    Examples:
      | Year | Make      | Model    | Trim | Assembly | FitmentOption | ItemCodeA | ItemCodeB | InventoryMessageA       | InventoryMessageB       | CheckAvailabilityInventoryMessageA | CheckAvailabilityInventoryMessageB |
      | 2010 | Chevrolet | Corvette | Base | none     | tire          | 27142     | 27141     | available in 3 - 5 days | available in 3 - 5 days | Front: In Stock                    | Rear: In Stock                     |

  @dtd
  @web
  @15931
  Scenario Outline: HYBRIS_SEARCH_SEARCH_PLP_ Changes to Found it Lower Link to Instant Price Match (ALM-15931)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify the "Instant Price Match" link displayed for "<Item>" on PLP page

    Examples:
      | Year | Make  | Model  | Trim     | Assembly | FitmentOption | Header       | Item  |
      | 2015 | Honda | Accord | Coupe EX | none     | wheel         | wheel result | 23476 |
      | 2012 | Honda | Civic  | Coupe DX | none     | tire          | tire result  | 43006 |

  @dtd
  @web
  @15931
  Scenario Outline: HYBRIS_SEARCH_SEARCH_PDP_ Changes to Found it Lower? Link to Instant Price Match ((ALM-15931)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I verify the "Instant Price Match" link is displayed

    Examples:
      | ItemCode |
      | 23500    |
      | 19600    |

  @dt
  @at
  @dtd
  @web
  @15336
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING Wheel FilterByWheelRimDiameter Facet Filter on PLP (ALM #15336)
    When I do a free text search for "<Search Term>" and hit enter
    Then I verify the PLP header message contains "Results for "<Search Term>""
    When I "expand" the "<Facet>" filter section
    And  I select from the "<Facet>" filter section, "single" option(s): "<Filter Option>"
    And  I "expand" the "<Facet>" filter section
    Then I verify all results match the search criteria: "<Filter Option>"

    Examples:
      | Search Term | Facet    | Filter Option |
      | Whee        | Diameter | 14            |

  @dt
  @at
  @dtd
  @web
  @5513
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING SortByRelevancy (ALM #5513)
    When I do a free text search for "<Search Term>" and hit enter
    Then I verify the PLP header message contains "Results for "<Search Term>""
    And  I verify that the sort by dropdown value is set to "Relevance"

    Examples:
      | Search Term |
      | Whee        |

  @dt
  @at
  @dtd
  @web
  @5345
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING FilterByReviewRating Facet Filter on PLP (ALM #5345)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select from the "<FilterSection>" filter section, "single" option(s): "<Option>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Value>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Header      | FilterSection  | Option   | Value     |
      | 2012 | Honda | CIVIC | COUPE DX | none     | tire          | tire result | Review Ratings | 3 and up | 3.0 - 3.9 |

  @at
  @dt
  @dtd
  @web
  @14871
  Scenario Outline: HYBRIS_SEARCH_SEARCH_NRR Secondary Nav Changes_Exclude_FitmentSearch (ALM #14871)
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I close popup modal
    Then I verify my vehicle details: "<Year>, <Make>, <Model>, <Trim>" are "displayed" in the 'My Vehicles' section of the fitment grid
    When I open the "TIRES" navigation link
    And  I click the "TIRE SEARCH" menu option
    Then I am brought to the page with path "<Expected Path>"
    And  I am brought to the page with header "<Expected Header>"
    When I select "All-Season tires" from the Product Brand page
    Then I verify the PLP header message contains "tire result"
    When I extract the tire results count from the PLP
    And  I open the My Vehicles popup
    And  I remove my "selected vehicle" vehicle
    Then I verify "no recent vehicles" element displayed
    When I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "TIRE SEARCH" menu option
    Then I am brought to the page with path "<Expected Path>"
    And  I am brought to the page with header "<Expected Header>"
    When I select "All-Season tires" from the Product Brand page
    Then I verify the PLP header message contains "tire result"
    And  I verify the tire results count from the PLP without fitment matches the tire results count on PLP with fitment

    Examples:
      | Year | Make  | Model  | Trim     | Assembly | Expected Path | Expected Header |
      | 2015 | Honda | Accord | Coupe EX | none     | /tires        | Tires           |

  @at
  @dt
  @dtd
  @web
  @15348
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING Fitment Result FilterByBrand Facet Filter on PLP CLEAR ALL (ALM #15348)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify the 'Brands' search bar contains the "placeholder text": "Search Brand..."
    And  I verify "Show More" is "displayed" for the "Brands" filter section
    When I "expand" the "Brands" filter section
    And  I select "Show More" option for the "Brands" filter section
    Then I verify the "<Filter 1>" checkbox to be "deselected" by default
    And  I verify the "<Filter 2>" checkbox to be "deselected" by default
    When I select from the "Brands" filter section, "multiple" option(s): "<Filter 1>, <Filter 2>"
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>, <Filter 2>"
    When I clear all the currently active filters on the PLP page
    And  I select "Show More" option for the "Brands" filter section
    Then I verify the "<Brand>" checkbox to be "deselected" by default
    And  I verify the "<Filter 2>" checkbox to be "deselected" by default

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Filter 1 | Filter 2    | Brand    |
      | 2012 | Honda | CIVIC | COUPE DX | none     | Yokohama | Bridgestone | Yokohama |

  @at
  @dt
  @dtd
  @web
  @15348
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING Fitment Result FilterByBrand Facet Filter on PLP SEARCH BRANDS BAR (ALM #15348)
  """Scenario will fail for IE11 as the 'Brands' filter options are not updated as text is entered into the 'Brands' search bar"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify the 'Brands' search bar contains the "placeholder text": "Search Brand..."
    When I enter "<Max Limit>" into the 'Brands' search bar
    Then I verify up to "25" characters were entered into the 'Brands' search bar
    When I clear the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "placeholder text": "Search Brand..."
    When I enter "<Partial Term>" into the 'Brands' search bar
    Then I verify the only options displayed for the 'Brands' filter section contain: "<Partial Term>"
    When I clear the 'Brands' search bar
    And  I enter "<Special Chars>" into the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "placeholder text": "Search Brand..."
    When I enter "<Full Name>" into the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "text": "<Full Name>"
    When I select from the "Brands" filter section, "single" option(s): "<Full Name>"
    Then I verify expected brand "<Full Name>" products displayed on PLP

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Max Limit                    | Partial Term | Special Chars | Full Name |
      | 2012 | Honda | CIVIC | COUPE DX | none     | iiiiiiiiiiiiiiiiiiiiiiiiiiii | Ari          | @$&*          | Yokohama  |

  @at
  @dt
  @dtd
  @web
  @bvt
  @core
  @5350
  @bvtPlp
  @coreScenarioPLPPDP
  @bvtScenarioQuickFilterFacetPriceRange
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING Fitment Result FilterByPriceRange Facet Filter on PLP - non-staggered (ALM #5350)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<HeaderMessage>"
    And  I verify the "<Facet>" filter section is expanded by default
    When I "collapse" the "<Facet>" filter section
    And  I "expand" the "<Facet>" filter section
    Then I verify 'Price Range' slider filter has two handles
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    And  I verify the extracted first product price is displayed as the "minimum" price point in the 'Price Range' filter section
    When I select the "Price (High to Low)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (High to Low)"
    And  I verify the extracted first product price is displayed as the "maximum" price point in the 'Price Range' filter section
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    When I clear all the currently active filters on the PLP page
    Then I verify no search refinement filters are being applied

    Examples:
      | VehicleType             | FitmentOption | Facet       | HeaderMessage |
      | VEHICLE_NON_STAGGERED_1 | tire          | Price Range | tire result   |
      | VEHICLE_NON_STAGGERED_1 | wheel         | Price Range | wheel result  |

  @at
  @dt
  @dtd
  @web
  @bvt
  @core
  @5350
  @bvtPlp
  @coreScenarioPLPPDP
  @bvtScenarioQuickFilterFacetPriceRange
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FILTER SORTING Fitment Result FilterByPriceRange Facet Filter on PLP - staggered (ALM #5350)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<HeaderMessage>"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify the "<Facet>" filter section is expanded by default
    When I "collapse" the "<Facet>" filter section
    And  I "expand" the "<Facet>" filter section
    Then I verify 'Price Range' slider filter has two handles
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    And  I verify the extracted first product price is displayed as the "minimum" price point in the 'Price Range' filter section
    When I select the "Price (High to Low)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (High to Low)"
    And  I verify the extracted first product price is displayed as the "maximum" price point in the 'Price Range' filter section
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    When I clear all the currently active filters on the PLP page
    Then I verify no search refinement filters are being applied
    When I select "REAR" staggered tab on PLP result page
    Then I verify the "<Facet>" filter section is expanded by default
    When I "collapse" the "<Facet>" filter section
    And  I "expand" the "<Facet>" filter section
    Then I verify 'Price Range' slider filter has two handles
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    And  I verify the extracted first product price is displayed as the "minimum" price point in the 'Price Range' filter section
    When I select the "Price (High to Low)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (High to Low)"
    And  I verify the extracted first product price is displayed as the "maximum" price point in the 'Price Range' filter section
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    When I clear all the currently active filters on the PLP page
    Then I verify no search refinement filters are being applied

    Examples:
      | VehicleType         | FitmentOption | Facet       | HeaderMessage |
      | VEHICLE_STAGGERED_1 | tire          | Price Range | tire result   |
      | VEHICLE_STAGGERED_1 | wheel         | Price Range | wheel result  |

  @dt
  @at
  @dtd
  @web
  @14595
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FreeText_Search_NRR (ALM #14595)
  """ fails in ie due to known resolution issue """
    When I open the My Vehicles popup
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I close popup modal
    And  I do a free text search for "<Search Term2>" and hit enter
    Then I verify message displayed in search no results page with "vehicle in session"
    And  I verify "Shop tires that fit" has the text "Shop tires that fit" in search results page
    And  I verify "Shop wheels that fit" has the text "Shop wheels that fit" in search results page
    And  I verify "Start A New Search" has the text "Start A New Search" in search results page
    When I click on the "Shop tires that fit" link
    Then I verify the PLP header message contains "tire result"
    When I navigate back to previous page
    And  I click on the "Shop wheels that fit" link
    Then I verify message displayed in search no results page with "vehicle in session"
    When I navigate back to previous page
    Then I verify "Shop By" exists in search results page
    When I click on the "All-Season tires" link
    Then I verify "My Vehicle" exists in search results page
    When I navigate back to previous page
    And  I click on the "ATV / UTV Wheels" link
    Then I verify "My Vehicle" exists in search results page
    When I navigate back to previous page
    And  I click on "Vehicle under Tires" in search results Page
    Then I verify "Fitment popup" exists in search results page
    When I navigate back to previous page
    And  I click on "Vehicle under Wheels" in search results Page
    Then I verify "Fitment popup" exists in search results page
    When I navigate back to previous page
    And  I click on "Size under Tires" in search results Page
    Then I verify "Fitment popup" exists in search results page
    When I navigate back to previous page
    And  I click on "Size under Wheels" in search results Page
    Then I verify "Fitment popup" exists in search results page
    When I navigate back to previous page
    And  I click on "Brand under Tires" in search results Page
    Then I verify "Fitment popup" exists in search results page
    When I navigate back to previous page
    And  I click on "Brand under Wheels" in search results Page
    Then I verify "Fitment popup" exists in search results page

    Examples:
      | Year | Make  | Model  | Trim     | Assembly | Search Term1 | Search Term2 |
      | 2015 | Honda | Accord | COUPE EX | none     | tir          | kBFKJHSEB    |

  @dt
  @at
  @dtd
  @web
  @15930
  Scenario Outline: HYBRIS_SEARCH_SEARCH_PLP_DT_TIRES (ALM # 15930)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select the "<Product Type>" menu option
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify "Compare" exists in PLP
    And  I verify "Compare" has the text of "Compare" in PLP
    And  I verify "Sub Total with Tool Tip" has the text of "Tire subtotal" in PLP
    And  I verify "Need It Now with Tool Tip" has the text of "Need it now?" in PLP
    And  I verify "Tool Tip" exists in PLP
    And  I verify check near by stores has the text of "Check nearby stores" in PLP
    And  I verify "Promotions" exists in PLP
    And  I verify "Found it lower" exists in PLP
    And  I verify "Best, Better, Good" exists in PLP
    When I click "Tool Tip" element in PLP
    Then I verify "Tire" Tool tip has the required text in PLP
    When I click "Sub total tool tip" element in PLP
    Then I verify Sub Total Tool Tip has the required text in PLP

    Examples:
      | ZipCode | Year | Make  | Model  | Trim     | Assembly | FitmentOption | Product Type |
      | 86001   | 2015 | Honda | Accord | Coupe EX | none     | tire          | Tires        |

  @dt
  @at
  @dtd
  @web
  @16208
  Scenario Outline: HYBRIS_SEARCH_SEARCH_PLP_DT_TIRES (ALM #16208)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    And  I verify "Sub Total with Tool Tip" has the text of "Wheel subtotal" in PLP
    And  I verify "Need It Now with Tool Tip" has the text of "Need it now?" in PLP
    And  I verify "Tool Tip" exists in PLP
    And  I verify check near by stores has the text of "Check nearby stores" in PLP
    And  I verify "Promotions" exists in PLP
    And  I verify "Found it lower" exists in PLP
    And  I verify "Best, Better, Good" exists in PLP
    When I click "Tool Tip" element in PLP
    Then I verify "Wheel" Tool tip has the required text in PLP
    When I click "Sub total tool tip" element in PLP
    Then I verify Sub Total Tool Tip has the required text in PLP

    Examples:
      | ZipCode | Year | Make  | Model  | Trim     | Assembly | FitmentOption | Product Type |
      | 86001   | 2015 | Honda | Accord | Coupe EX | none     | wheel         | Wheels       |

  @dt
  @web
  @Top3Pdl
  Scenario Outline: Verify Recommendations filter does not display for Optional tire size
    When I navigate to Shop By "Vehicle"
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select the "<SizeOption>" fitment box option
    Then I verify the selected fitment box option has a value of "<SizeOption>"
    When I select a fitment option "<TireSize>"
    Then I verify "Recommendations" does not display in the filters

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  |
      | 2012 | Honda | Civic | Coupe DX | none     | 14         | 185/70-14 |

  @dt
  @web
  @Top3Pdl
  Scenario Outline: Verify the presence of Driving Details block when user goes through PDL flow.
    When I navigate to Shop By "Vehicle"
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I select "Performance" as my driving priority
    And  I select view recommended tires
    Then I verify the PLP "displays" the Driving Details block

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @web
  @Top3Pdl
  Scenario Outline: Verify the presence of Driving Details block when user goes through the PDL flow comes back and selects optional tire size.
    When I navigate to Shop By "Vehicle"
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select enter driving details for recommended tires
    And  I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I select "Performance" as my driving priority
    And  I select view recommended tires
    And  I select edit treadwell driving details on plp page
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I click cancel on pdl fitment page
    Then I verify the fitment box option has a value of "<SizeOption>"
    When I select the "<SizeOption>" fitment box option
    Then I verify the selected fitment box option has a value of "<SizeOption>"
    When I select a fitment option "<TireSize>"
    Then I verify the PLP "displays" the Driving Details block

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  |
      | 2012 | Honda | Civic | Coupe DX | none     | 14         | 185/70-14 |

  @dt
  @web
  @Top3Pdl
  Scenario Outline: Verify absence of Driving Details Block on PLP page for Optional tire size
    When I navigate to Shop By "Vehicle"
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select the "<SizeOption>" fitment box option
    Then I verify the selected fitment box option has a value of "<SizeOption>"
    When I select a fitment option "<TireSize>"
    Then I verify the PLP "not displays" the Driving Details block

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  |
      | 2012 | Honda | Civic | Coupe DX | none     | 14         | 185/70-14 |

  @dt
  @web
  @Top3Pdl
  Scenario Outline: Verify the presence of PDL recommendation banner, tile and filter.
    When I navigate to Shop By "Vehicle"
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select enter driving details for recommended tires
    And  I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I select "Performance" as my driving priority
    And  I select view recommended tires
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PLP" page
    And  I verify the PLP displays "<TileType>" in the Top 3 Tiles
    And  I verify the "<FilterOption>" checkbox has been checked

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FilterOption       | TileType           |
      | 2012 | Honda | Civic | Coupe DX | none     | Recommendation (9) | Our Recommendation |

  @dt
  @web
  @Top3Pdl
  Scenario Outline: Verify the presence of user selected driving details on pdl page.
    When I navigate to Shop By "Vehicle"
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select enter driving details for recommended tires
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I set "Primary Driving Location" to "<NewZip>"
    And  I set the miles per year to "<Miles>"
    And  I select view recommended tires
    Then I verify the treadwell location display zipcode "<NewZip>" on "PLP"
    And  I should see "<Miles>" as my selected miles per year value on treadwell driving details section on "PLP"
    And  I should see "Stopping Distance","Life of Tire","Handling","Comfort & Noise" are in order on "PLP"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | NewZip | Miles |
      | 2012 | Honda | Civic | Coupe DX | none     | 85255  | 25    |

  @dt
  @web
  @Top3Pdl
  Scenario Outline: Verify the absence of Driving details block when user goes through the PDL flow comes back and select wheels
    When I navigate to Shop By "Vehicle"
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select enter driving details for recommended tires
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I select "Performance" as my driving priority
    And  I select view recommended tires
    Then I verify the PLP "displays" the Driving Details block
    When I select edit treadwell driving details on plp page
    Then I verify treadwell logo is displayed with title and subtitle on "PLP"
    When I click cancel on pdl fitment page
    Then I verify the PLP "displays" the Driving Details block

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @at
  @dtd
  @web
  @16452
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Top 3 Tiles verify banner color for Our Recommendation Tiles( ALM # 16452)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP displays 'Top 3 Tiles' below the PLP header
    When I select product at position "2" in 'Top 3 Tiles'
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make  | Model  | Trim     | Assembly | FitmentOption |
      | 2012 | HONDA | CIVIC  | COUPE DX | none     | tire          |
      | 2015 | HONDA | Accord | Coupe EX | none     | tire          |

  @dtd
  @web
  @mobile
  @16456
  Scenario Outline: HYBRIS_UI_Verify_DESKTOP_Free Shipping Message on PLP and PDP(ALM #16456)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify "Free Shipping" exists in PLP
    When I select the "First" product result image on "PLP" page
    Then I verify "Standard" PDP page is displayed
    And  I verify the free shipping label on PDP

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | HONDA | CIVIC | COUPE DX | none     |

  @dtd
  @web
  @mobile
  @16503
  Scenario Outline: HYBRIS_UI_Verify_DESKTOP_Free Shipping Message on PDP(ALM #16503)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I verify "Standard" PDP page is displayed
    And  I verify the free shipping label on PDP

    Examples:
      | ItemCode |
      | 17595    |

  @at
  @dt
  @dtd
  @web
  @15589
  @15606
  @mobile
  @wheelConfigurator
  @wheelConfiguratorUi
  @staggeredExperience
  @wheelConfiguratorChangeVehicle
  Scenario Outline: HYBRIS_SEARCH_Verify that a different image is displayed for the  front and rear wheels (ALM#15589, 15606)
    When I go to the homepage
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I select "View Details" option on "Wheel Configurator modal window" page
    Then I verify quantity value "4" on PDP page
    And  I should see "<Year1> <Make1>" details on the "PDP" page
    When I navigate back to previous page
    Then I should see previously selected wheel should be on focus
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I choose a wheel on Wheel Configurator modal window
    And  I click on the "Add to Cart" button
    Then I should see Cart Popup has "<Header>" option displayed
    When I click on the "View shopping Cart" link
    Then I am brought to the page with header "Shopping cart"
    When I go to the homepage
    And  I select mini cart and "Clear Cart"
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I choose a wheel on Wheel Configurator modal window
    And  I select "share" option on wheel configurator window
    Then I should see the Social Site "Email" option displayed
    And  I should see the Social Site "Facebook" option displayed
    And  I should see the Social Site "Twitter" option displayed
    And  I should see the Social Site "Google Plus" option displayed
    And  I should see the Social Site "Mobile" option displayed
    And  I should see the Social Site "pinterest" option displayed
    When I choose 'Change Vehicle' option on the Wheel Configurator modal window
    And  I choose "Assembly" to view its options and set the value to "<Value>"
    And  I click on the "See Wheels" button
    Then I should see the wheel results displayed on the Wheel Configurator modal window
    When I choose 'Change Vehicle' option on the Wheel Configurator modal window
    And  I choose "Year" to view its options and set the value to "<Year2>"
    And  I choose "Make" to view its options and set the value to "<Make2>"
    And  I choose "Model" to view its options and set the value to "<Model2>"
    And  I choose "Trim" to view its options and set the value to "<Trim2>"
    And  I click on the "See Wheels" button
    Then I should see the wheel results displayed on the Wheel Configurator modal window

    Examples:
      | Year1 | Make1 | Model1 | Trim1 | Assembly1      | Header             | Value          | Year2 | Make2     | Model2   | Trim2      | Assembly2 |
      | 2014  | BMW   | 640i   | Coupe | 245 /40 R19 XL | Item added to cart | 245 /45 R18 XL | 2010  | Chevrolet | Corvette | Gran Sport | none      |


  @dt
  @web
  @wheelSizeColor
  @wheelConfigurator
  @wheelConfiguratorUi
  @staggeredExperience
  Scenario Outline: Verify the Wheel Configurator UI for standard and staggered
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    Then I should see "title" "<Title>" on the Wheel Configurator modal window displayed
    And  I should see "subtitle" "<SubTitle>" on the Wheel Configurator modal window displayed
    And  I should see the "X" on the "Wheel Configurator modal window" page
    When I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    Then I should see the default "Wheel Color" in the Wheel Configurator modal window
    Then I should see vehicle "<Make>" on the Wheel Configurator modal window
    And  I should see the "Vehicle Image" on the "Wheel Configurator modal window" page
    And  I should see the wheel results displayed on the Wheel Configurator modal window
    When I choose 'Change Vehicle' option on the Wheel Configurator modal window
    And  I should see the wheel has Image Name Brand Price displayed
    Then I should see "title" "Enter your vehicle" on the Wheel Configurator modal window displayed
    And  I verify the wheel count on the header should match with the displayed wheel results

    Examples:
      | Year | Make  | Model | Trim     | Assembly          | Title              | SubTitle                                                               |
      | 2012 | Honda | Civic | Coupe DX | none              | Enter your vehicle | Please note that not all vehicles are represented in the Wheel Browser |
      | 2014 | BMW   | 640i  | Coupe    | F: 245 /45 R18 SL | Enter your vehicle | Please note that not all vehicles are represented in the Wheel Browser |


  @dt
  @web
  @checkSorting
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify the mandatory sorting options and Sorting Functionality on Wheel Configurator
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I set fitment type to "<FitmentType>"
    And  I choose FILTER option
    Then I should see the wheel filter options on Wheel Configurator modal window
    And  I verify that the sort by dropdown value is set to "<SortValueDefault>"
    When I select the "Price (Low To High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "<SortValue>"
    When I enter "<NewBrand>" into the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "text": "<NewBrand>"
    When I select the "<NewBrand>" checkbox
    And  I choose filter option "Apply Filters" on Wheel Configurator modal window
    Then I verify the Wheel Configurator filters are "not displayed"

    Examples:
      | Year | Make  | Model | Trim     | Assembly       | SortValueDefault | SortValue           | NewBrand | FitmentType   |
      | 2012 | Honda | Civic | Coupe DX | none           | Best Seller      | Price (Low to High) | Rage     | non-staggered |
      | 2014 | BMW   | 640i  | Coupe    | 245 /40 R19 SL | Best Seller      | Price (Low to High) | TSW      | staggered     |

  @dt
  @web
  @20554
  @checkSorting
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify the Sorting Functionality on Wheel Configurator
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I click on the "Filter" button
    And  I select from the "Wheel Category" filter section, "single" option(s): "<Filter1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter1>"
    When I click on the "Apply Filters" link
    Then I should see the wheel results displayed on the Wheel Configurator modal window
    When I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I select the "<SortByOption>" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "<SortByOption>"
    When I reset the minimum and maximum price range
    And  I click on the "Apply Filters" link
    And  I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I select from the "Brands" filter section, "single" option(s): "<Filter2>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter2>"
    When I click on the "Apply Filters" link
    Then I should see the wheel results displayed on the Wheel Configurator modal window
    When I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I enter "<NewBrand>" into the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "text": "<NewBrand>"
    When I select from the "Brands" filter section, "single" option(s): "<NewBrand>"
    And  I click on the "Apply Filters" link
    Then I should see the wheel results displayed on the Wheel Configurator modal window
    When I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    When I click on the "Apply Filters" link
    And  I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I select from the "Wheel Category" filter section, "single" option(s): "<Filter1>"
    And  I select from the "Brands" filter section, "single" option(s): "<Filter2>"
    And  I reset the minimum and maximum price range
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter1>,<Filter2>"

    Examples:
      | Year | Make | Model | Trim  | Assembly          | Filter1 | SortByOption        | Filter2 | NewBrand |
      | 2014 | BMW  | 640i  | Coupe | F: 245 /45 R18 SL | Painted | Price (High to Low) | Beyern  | Beyern   |

  @dt
  @web
  @20565
  @wheelConfigurator
  @staggeredExperience
  @checkWheeelNavigationFromPdp
  Scenario Outline: Verify the navigation on click of back button from the PDP
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I select "View Details" option on "Wheel Configurator modal window" page
    Then I should see "<Vehicle>" details on the "PDP" page
    When I navigate back to previous page
    Then I should see previously selected wheel should be on focus
    When I choose 'Change Vehicle' option on the Wheel Configurator modal window
    And  I choose "Year" to view its options and set the value to "<Year2>"
    And  I choose "Make" to view its options and set the value to "<Make2>"
    And  I choose "Model" to view its options and set the value to "<Model2>"
    And  I choose "Trim" to view its options and set the value to "<Trim2>"
    And  I click on the "See Wheels" button
    Then I should see the wheel results displayed on the Wheel Configurator modal window
    When I choose 'Change Vehicle' option on the Wheel Configurator modal window
    And  I choose "Year" to view its options and set the value to "<Year3>"
    And  I choose "Make" to view its options and set the value to "<Make3>"
    And  I choose "Model" to view its options and set the value to "<Model3>"
    And  I choose "Trim" to view its options and set the value to "<Trim3>"
    And  I choose "Assembly" to view its options and set the value to "<Assembly3>"
    And  I click on the "See Wheels" button
    Then I should see the wheel results displayed on the Wheel Configurator modal window


    Examples:
      | Year | Make  | Model | Trim     | Assembly | Vehicle                   | Year2 | Make2     | Model2   | Trim2      | Year3 | Make3 | Model3 | Trim3       | Assembly3         |
      | 2012 | Honda | Civic | Coupe DX | none     | 2012 Honda Civic Coupe DX | 2010  | Chevrolet | Corvette | Gran Sport | 2014  | BMW   | 640i   | Convertible | F: 245 /45 R18 SL |

  @dt
  @web
  @checkAddedFilter
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify the  previous filters and wheel products displayed in Wheel Configurator modal window on click of back browser from PDP
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose FILTER option
    And  I enter "<Brand>" into the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "text": "<Brand>"
    When I select the "<Brand>" checkbox
    And  I choose filter option "Apply Filters" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I select "View Details" option on "Wheel Configurator modal window" page
    Then I should see "<Vehicle>" details on the "PDP" page
    When I navigate back to previous page
    And  I choose FILTER option
    Then I should see previously added filter "<Brand>" is present

    Examples:
      | Year | Make  | Model | Trim     | Assembly          | Brand | Vehicle                   |
      | 2012 | Honda | Civic | Coupe DX | none              | Rage  | 2012 Honda Civic Coupe DX |
      | 2014 | BMW   | 640i  | Coupe    | F: 245 /45 R18 SL | TSW   | 2014 BMW 640i Coupe       |

  @dt
  @web
  @checkAddedFilter
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: IWS - Verify that when the user clicks on "View Details" the staggered sets PDP page is displayed (4.10_OCW-823_1)
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose FILTER option
    And  I enter "<WheelBrand>" into the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "text": "<WheelBrand>"
    When I select the "<WheelBrand>" checkbox
    And  I choose filter option "Apply Filters" on Wheel Configurator modal window
    And  I choose a wheel "<WheelBrand>" "<WheelName>" on Wheel Configurator modal window
    And  I select "View Details" option on "Wheel Configurator modal window" page
    Then I should see "<Vehicle>" details on the "PDP" page
    And  I should see "<WheelBrand>" on product details page
    And  I should see "<WheelName>" on product details page

    Examples:
      | Year | Make  | Model | Trim     | Assembly          | WheelBrand | WheelName | Vehicle                   |
      | 2012 | Honda | Civic | Coupe DX | none              | MB WHEELS  | VECTOR    | 2012 Honda Civic Coupe DX |
      | 2014 | BMW   | 640i  | Coupe    | F: 245 /35 R20 XL | TSW        | PANORAMA  | 2014 BMW 640i Coupe       |

  @dt
  @web
  @wheelConfigurator
  @staggeredExperience
  @wheelConfiguratorAddToCart
  Scenario Outline: Verify Add to cart in Wheel Configurator modal
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I select "Add To Cart"
    Then I should see Cart Popup has "View shopping Cart" option displayed
    And  I should see Cart Popup has "Continue Shopping" option displayed
    And  I should see Cart Popup has "Item added to cart" option displayed
    And  I should see Cart Popup has "You can choose product add-ons and schedule installation during check out" option displayed

    Examples:
      | Year | Make  | Model | Trim     | Assembly       |
      | 2012 | Honda | Civic | Coupe DX | none           |
      | 2014 | BMW   | 640i  | Coupe    | 245 /35 R20 XL |

  @dt
  @web
  @viewCart
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify View shopping cart in Wheel Configurator
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I add item to my cart and "View shopping Cart"
    Then I verify the header cart item count is "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly       |
      | 2012 | Honda | Civic | Coupe DX | none           |
      | 2014 | BMW   | 640i  | Coupe    | 245 /40 R19 SL |

  @dt
  @web
  @continueShop
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify Fitment Search By Vehicle Option
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I add item to my cart and "Continue Shopping"
    Then I should see vehicle "<Make>" on the Wheel Configurator modal window

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @web
  @share
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify the Sharing options on Wheel Configurator
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    Then I should see the "Share Icon" on the "Wheel Configurator modal window" page
    When I choose a wheel on Wheel Configurator modal window
    And  I select "Share" option on "Wheel Configurator modal window" page
    Then I should see the Social Site "Email" option displayed
    And  I should see the Social Site "Mobile" option displayed
    And  I should see the Social Site "Facebook" option displayed
    And  I should see the Social Site "Twitter" option displayed
    And  I should see the Social Site "Pinterest" option displayed
    And  I should see the Social Site "Google Plus" option displayed

    Examples:
      | Year | Make  | Model | Trim     | Assembly       |
      | 2012 | Honda | Civic | Coupe DX | none           |
      | 2014 | BMW   | 640i  | Coupe    | 245 /40 R19 SL |

  @dt
  @web
  @wheelQuantity
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify default cart quantity and Update the quantity of a wheel
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I add item to my cart and "View shopping Cart"
    Then I should see the cart quantity is set to "<quantity1>"
    When I update the quantity in "Cart" with "<quantity2>"
    Then I should see the cart quantity is set to "<quantity2>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly          | quantity1 | quantity2 |
      | 2012 | Honda | Civic | Coupe DX | none              | 4         | 8         |
      | 2014 | BMW   | 640i  | Coupe    | F: 245 /45 R18 SL | 2         | 4         |

  @dt
  @web
  @closeWCmodal
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify the close(X) functionality on Wheel Configurator
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I select "X" option on "Wheel Configurator modal window" page
    Then I verify the site logo

    Examples:
      | Year | Make  | Model | Trim     | Assembly          |
      | 2012 | Honda | Civic | Coupe DX | none              |
      | 2014 | BMW   | 640i  | Coupe    | F: 245 /45 R18 SL |

  @at
  @dt
  @dtd
  @web
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_IWS - Verify that is a vehicle is selected and the user launch the IWS the default wheel size selected is displayed on the size drop down(4.10_OCW-818_1)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I browse to the "Homepage" page with defaults
    And  I click on the "USE WHEEL VISUALIZER" button
    Then I should see vehicle "<Year> <Make> <Model>" on the Wheel Configurator modal window
    And  I should see the default "Wheel Size" in the Wheel Configurator modal window

    Examples:
      | Year | Make | Model | Trim  | Assembly       | Header  |
      | 2014 | BMW  | 640i  | Coupe | 245 /35 R20 XL | Results |

  @at
  @dt
  @dtd
  @web
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_IWS - Verify that an user can change from a standard vehicle to a staggered one in the IWS(4.10_OCW-820_2)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I browse to the "Homepage" page with defaults
    And  I click on the "USE WHEEL VISUALIZER" button
    Then I should see vehicle "<Make>" on the Wheel Configurator modal window
    When I choose 'Change Vehicle' option on the Wheel Configurator modal window
    And  I do a vehicle search with details "<staggeredYear>" "<staggeredMake>" "<staggeredModel>" "<staggeredTrim>" "<staggeredAssembly>" on Wheel Configurator modal window
    Then I should see vehicle "<staggeredMake>" on the Wheel Configurator modal window

    Examples:
      | Year | Make  | Model | Trim     | Assembly | staggeredYear | staggeredMake | staggeredModel | staggeredTrim | staggeredAssembly |
      | 2012 | Honda | Civic | Coupe DX | none     | 2014          | BMW           | 640i           | Coupe         | 245 /40 R19 SL    |

  @dt
  @web
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify the Sharing options on Wheel Configurator, and navigating to Google Plus, Facebook, Twitter, and Pinterest using Share option
    """ This will only pass locally or on Hybris_Execution_Node for Facebook, Twitter, and Pinterest because
    "Content blocked by your organization" popup displays on other VMs """
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I select "Share" option on "Wheel Configurator modal window" page
    Then I should see the Social Site "Email" option displayed
    And  I should see the Social Site "Mobile" option displayed
    And  I should see the Social Site "<Site1>" option displayed
    And  I should see the Social Site "<Site2>" option displayed
    And  I should see the Social Site "<Site3>" option displayed
    And  I should see the Social Site "<Site4>" option displayed
    And  I select the Social Site "<Site1>"
    Then I verify the current URL contains "<url1>"
    When I close browser with url containing "<url1>"
    And  I select the Social Site "<Site2>"
    Then I verify the current URL contains "<url2>"
    When I close browser with url containing "<url2>"
    And  I select the Social Site "<Site3>"
    Then I verify the current URL contains "<url3>"
    When I close browser with url containing "<url3>"
    And  I select the Social Site "<Site4>"
    Then I verify the current URL contains "<url4>"
    When I close browser with url containing "<url4>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly          | Site1       | url1         | Site2    | url2         | Site3   | url3        | Site4     | url4          |
      | 2012 | Honda | Civic | Coupe DX | none              | Google Plus | google.com   | Facebook | facebook.com | Twitter | twitter.com | Pinterest | pinterest.com |
      | 2014 | BMW   | 640i  | Coupe    | F: 245 /45 R18 SL | Google Plus | google.com   | Facebook | facebook.com | Twitter | twitter.com | Pinterest | pinterest.com |

  @dt
  @web
  @selectEmail
  @wheelConfigurator
  @staggeredExperience
  Scenario Outline: Verify send to Email Address using Share Option
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I select "Share" option on "Wheel Configurator modal window" page
    And  I enter email address "<EmailId>" on Wheel Configurator page
    Then I verify the "SEND" button is "enabled"
    And  I enter phone number "<PhoneNumber>" on Wheel Configurator page
    Then I verify the "SEND" button is "enabled"

    Examples:
      | Year | Make  | Model | Trim     | Assembly          | EmailId               | PhoneNumber |
      | 2012 | Honda | Civic | Coupe DX | none              | Test@discounttire.com | 9206981972  |
      | 2014 | BMW   | 640i  | Coupe    | F: 245 /45 R18 SL | Test@discounttire.com | 9206981972  |

  @dt
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart Verify the user can check fitment for set and a staggered vehicle and proceed to checkout(OCW-815-2 OCW-815-2)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    When I select "FRONT" staggered tab on PLP result page
    And  I add item to my cart and "Continue Shopping"
    And  I select "REAR" staggered tab on PLP result page
    And  I add item to my cart and "View shopping Cart"
    And  I click checkout now button
    And  I click on the "Continue to Checkout" button
    Then I verify "Checkout" page is displayed

    Examples:
      | Year | Make      | Model    | Trim |
      | 2010 | Chevrolet | Corvette | Base |

  @dt
  @dtd
  @web
  @16607
  Scenario Outline: HYBRIS_CUSTOMER_MYACCOUNT_My Vehicle Modal_Recent Vehicle Details_Regular Vehicle(ALM#16607)
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    Then I am brought to the homepage
    When I open the My Vehicles popup
    Then I verify "selected vehicle" element displayed
    And  I verify "Vehicle icon" element displayed
    And  I verify "my vehicles image" element displayed
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Wheel" fitment banner with "<OELabel>" and "<OEWheelSize>"
    And  I verify "edit" is displayed on "MyVehicles Modal"

    Examples:
      | Year | Make  | Model | Trim     | OELabel | OETireSize | OEWheelSize |
      | 2012 | Honda | Civic | Coupe DX | O.E     | 195/65-R15 | 15          |

  @dt
  @dtd
  @16607
  @mobile
  Scenario Outline: HYBRIS_CUSTOMER_MYACCOUNT_My Vehicle Modal_Recent Vehicle Details_Regular Vehicle_on_mobile(ALM#16607)
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    Then I am brought to the homepage
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    Then I verify "selected vehicle" element displayed
    And  I verify "Vehicle icon" element displayed
    And  I verify "my vehicles image" element displayed
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Wheel" fitment banner with "<OELabel>" and "<OEWheelSize>"
    And  I verify "edit" is displayed on "MyVehicles Modal"

    Examples:
      | Year | Make  | Model | Trim     | OELabel | OETireSize | OEWheelSize |
      | 2012 | Honda | Civic | Coupe DX | O.E     | 195/65-R15 | 15          |

  @dt
  @dtd
  @web
  @16612
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal _Verify Recent Vehicle List and clearing Recent searches(ALM#16612)
    When I open the My Vehicles popup
    Then I verify "my vehicles" element displayed
    And  I verify "recently searched vehicles" element displayed
    And  I verify "recent searches" element displayed
    When I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I close popup modal
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "No, Keep These Searches" option
    Then I verify "edit" is displayed on "MyVehicles Modal"
    When I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    Then I verify "no recent vehicles" element displayed

    Examples:
      | Year1 | Make1  | Model1 | Trim1     |
      | 2014  | Toyota | Camry  | Hybrid SE |

  @dt
  @dtd
  @16612
  @mobile
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal _Verify Recent Vehicle List and clearing Recent searches_on_mobile(ALM#16612)
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    Then I verify "my vehicles" element displayed
    And  I verify "recent searches" element displayed
    And  I verify "clear recent searches" element displayed
    When I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I close popup modal
    Then I am brought to the homepage
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select Clear Recent Searches and "No, Keep These Searches" option
    Then I verify "edit" is displayed on "MyVehicles Modal"
    When I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    Then I verify "no recent vehicles" element displayed

    Examples:
      | Year1 | Make1  | Model1 | Trim1     |
      | 2014  | Toyota | Camry  | Hybrid SE |

  @dt
  @dtd
  @web
  @5758
  @12569
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Verify_shop_tires_shop_wheels_standard (ALM#5758,ALM#12569)
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    Then I verify My Vehicle in the header displays as "<Make> <Model>"
    And  I close popup modal
    And  I open the My Vehicles popup
    And  I click on the "shop products" link
    And  I select "shop tires" link
    Then I verify "fitment tires tab" element displayed
    When I close popup modal
    And  I open the My Vehicles popup
    And  I select "shop wheels" link
    Then I verify "fitment wheels tab" element displayed

    Examples:
      | Year | Make      | Model    | Trim      |
      | 2014 | Toyota    | Camry    | Hybrid SE |
      | 2010 | chevrolet | corvette | base      |

  @dt
  @dtd
  @5758
  @12569
  @mobile
  @staggeredExperience
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Verify_shop_tires_shop_wheels_on_mobile(ALM#12569)
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select Add Vehicle
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    And  I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select "shop tires" link
    Then I verify "fitment tires tab" element displayed
    When I close popup modal
    And  I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select "shop wheels" link
    Then I verify "fitment wheels tab" element displayed

    Examples:
      | Year | Make      | Model    | Trim      |
      | 2014 | Toyota    | Camry    | Hybrid SE |
      | 2010 | chevrolet | corvette | base      |

  @at
  @web
  @5543
  @16660
  @mobile
  @treadwell
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify Treadwell RECOMMENDED and TOP RECOMMENDATION branding on product banner PLP and PDP_standard(ALM#5543,16660)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I verify treadwell logo is displayed with title and subtitle on "Fitment Modal"
    And  Treadwell fitment box in "popup page" should be "displayed"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I set "Primary Driving Location" to "<ZipCode>"
    And  I select view recommended tires
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PLP" page
    And  I verify treadwell top recommended product is "displayed" in 'Top 3 Tiles'
    And  I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking>" on "PLP" page
    When I select the "First" product result image on "PLP" page
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PDP" page
    When I navigate back to previous page
    And  I select the "2nd" product result image on "PLP" page
    Then I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking>" on "PDP" page
    When I navigate back to previous page
    And  I select product at position "2" in 'Top 3 Tiles'
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PDP" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ZipCode | TopRanking | RecommendedRanking |
      | 2012 | Honda | Civic | Coupe DX | none     | 86001   | #1         | #2                 |

  @at
  @web
  @5543
  @16660
  @mobile
  @treadwell
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify Treadwell RECOMMENDED and TOP RECOMMENDATION branding on product banner PLP and PDP_staggered (ALM#5543,16660)
    """ Will fail due to OEES-855: Top 3 not loading for Staggered. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I verify treadwell logo is displayed with title and subtitle on "Fitment Modal"
    And  Treadwell fitment box in "popup page" should be "displayed"
    When I select find tires using Treadwell
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I set "Primary Driving Location" to "<ZipCode>"
    And  I select view recommended tires
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PLP" page
    And  I verify treadwell top recommended product is "displayed" in 'Top 3 Tiles'
    And  I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking>" on "PLP" page
    When I select the "First" product result image on "PLP" page
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PDP" page
    When I navigate back to previous page
    And  I select the "2nd" product result image on "PLP" page
    Then I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking>" on "PDP" page
    When I navigate back to previous page
    And  I select product at position "2" in 'Top 3 Tiles'
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PDP" page

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ZipCode | TopRanking | RecommendedRanking |
      | 2010 | Chevrolet | Corvette | Base | none     | 86001   | #1         | #2                 |

  @dt
  @at
  @web
  @6034
  @16659
  @16664
  @treadwell
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify Treadwell RECOMMENDED and TOP RECOMMENDATION branding on product banner PLP and PDP from compare product page_standard(ALM#16659,16664,6034)
    When I change to the store with url "/store/az/scottsdale/s/1874"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I set "Primary Driving Location" to "<ZipCode>"
    And  I select view recommended tires
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PLP" page
    And  I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking2>" on "PLP" page
    When I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "Compare products" page
    And  I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking2>" on "Compare products" page
    And  I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking3>" on "Compare products" page
    And  I verify all categories are present for the "3" products
    And  I verify the current URL contains "/compare/product"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ZipCode | TopRanking | RecommendedRanking2 | RecommendedRanking3 |
      | 2012 | Honda | Civic | Coupe DX | none     | 86001   | #1         | #2                  | #3                  |

  @dt
  @at
  @web
  @core
  @6034
  @16659
  @16664
  @treadwell
  @coreScenarioSearch
  @staggeredExperience
  @coreScenarioTreadwell
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify Treadwell RECOMMENDED and TOP RECOMMENDATION branding on product banner PLP and PDP from compare product page (ALM#16659,16664,6034)
  """ May fail for Staggered product due to OEES-974 (DT) or OEES-975 (DTD): Staggered sets messaging not based off the longest lead time. """
    When I change to the store with url "/store/az/scottsdale/s/1874"
    And  I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I set "Primary Driving Location" to "<ZipCode>"
    And  I select view recommended tires
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PLP" page
    And  I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking2>" on "PLP" page
    When I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "Compare products" page
    And  I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking2>" on "Compare products" page
    And  I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking3>" on "Compare products" page
    And  I verify all categories are present for the "3" products
    And  I verify the current URL contains "/compare/product"

    Examples:
      | VehicleType             | ZipCode | TopRanking | RecommendedRanking2 | RecommendedRanking3 |
      | VEHICLE_NON_STAGGERED_1 | 86001   | #1         | #2                  | #3                  |
      | VEHICLE_STAGGERED_1     | 86001   | #1         | #2                  | #3                  |

  @dt
  @at
  @web
  @16711
  @16712
  @treadwell
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify Treadwell Logo, title and subtitle on Compare Products and PDP_standard(ALM#16711,16712)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I select view recommended tires
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify treadwell logo is displayed with title and subtitle on "Compare Products"
    When I select the "First" product result image on "Compare Products" page
    Then I verify treadwell logo is displayed with title and subtitle on "PDP"

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @at
  @web
  @16711
  @16712
  @treadwell
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify Treadwell Logo, title and subtitle on Compare Products and PDP_staggered(ALM#16711,16712)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I select view recommended tires
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify treadwell logo is displayed with title and subtitle on "Compare Products"
    When I select the "First" product result image on "Compare Sets" page
    Then I verify treadwell logo is displayed with title and subtitle on "PDP"

    Examples:
      | Year | Make | Model | Trim  | Assembly       |
      | 2014 | BMW  | 640i  | Coupe | 245 /45 R18 SL |

  @dt
  @at
  @web
  @16082
  @mobile
  @treadwell
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_FITMENT_Display of Treadwell banner and tagline_Regular and staggered vehicle (ALM#16082)
  """ FAILS: if the treadwell service for the selected fitment is not available"""
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I click on the "cancel" link
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed
    And  I verify the treadwell location display zipcode "<ZipCode>" on "treadwell modal"
    When I set "Miles Driven Per Year" to "<Miles>"
    Then I verify the miles driven per year has a value of "<Miles>" on treadwell customer preference model
    When I select view recommended tires
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify treadwell details section "present"
    And  I verify the treadwell location display zipcode "<ZipCode>" on "PLP"
    And  I should see "<Miles>" as my selected miles per year value on treadwell driving details section on "PLP"
    And  I verify "Everyday" default pdl driving priority order on "PLP"
    When I select edit treadwell driving details on plp page
    And  I select "Performance" as my driving priority
    And  I select view recommended tires
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify treadwell details section "present"
    And  I verify "Performance" default pdl driving priority order on "PLP"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Tire Size  | Miles | ZipCode |
      | 2012 | Honda     | Civic    | Coupe DX | none     | 195/65-R15 | 25    | 86001   |
      | 2010 | Chevrolet | Corvette | Base     | none     | 245/40-R18 | 25    | 86001   |

  @dt
  @at
  @web
  @pdlx
  @16084
  @mobile
  @treadwell
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_PLP_Display Treadwell branding to Preferences Component for regular and staggered vehicle (ALM#16084)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed
    When I set "Miles Driven Per Year" to "<Miles>"
    Then I verify the miles driven per year has a value of "<Miles>" on treadwell customer preference model
    When I select "Performance" as my driving priority
    And  I select view recommended tires
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify treadwell details section "present"
    When I select edit treadwell driving details on plp page
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the miles driven per year has a value of "<Miles>" on treadwell customer preference model
    When I set "Primary Driving Location" to "<ZipCode>"
    And  I set "Miles Driven Per Year" to "<NewMiles>"
    And  I move "Handling" driving priority to position "3"
    And  I select view recommended tires
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify treadwell details section "present"
    And  I verify the treadwell location display zipcode "<ZipCode>" on "PLP"
    And  I should see "<NewMiles>" as my selected miles per year value on treadwell driving details section on "PLP"
    And  I should see "Handling","Stopping","Comfort","Tire Life" are in order on "PLP"
    When I click on the product "<ItemCode>"
    Then I should see "<NewMiles>" as my selected miles per year value on treadwell driving details section on "PDP"

    Examples:
      | ZipCode | Year | Make      | Model    | Trim     | Assembly | Tire Size  | Miles | NewMiles | ItemCode |
      | 85260   | 2012 | Honda     | Civic    | Coupe DX | none     | 195/65-R15 | 25    | 45       | 29935    |
      | 85260   | 2010 | Chevrolet | Corvette | Base     | none     | 245/40-R18 | 25    | 45       | 35524    |

  @dt
  @at
  @web
  @pdlx
  @8189
  @mobile
  @treadwell
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_Validation for View recommendation button based on Primary driving location field (ALM#8189)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I set "Primary Driving Location" to "<InvalidZipCode>"
    Then I should verify that error message is displayed for "invalid" "zipcode"
    And  I verify the edit option is displayed
    And  I verify that the "view recommended tires" button is disabled
    And  I verify that the "view recommended tires" button color is "grey"
    When I set "Primary Driving Location" to " "
    Then I verify that the "view recommended tires" button is disabled
    And  I verify that the "view recommended tires" button color is "grey"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | InvalidZipCode |
      | 2012 | HONDA     | ACCORD   | EX COUPE | none     | 00000          |
      | 2010 | Chevrolet | Corvette | Base     | none     | 00000          |

  @dt
  @at
  @web
  @pdlx
  @7972
  @treadwell
  @TestFlow2
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_add vehicle test flow for regular and staggered vehicle(ALM#7972)
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Tire Size  |
      | 2012 | Honda     | Civic    | Coupe DX | none     | 195/65-R15 |
      | 2010 | Chevrolet | Corvette | Base     | none     | 245/40-R18 |

  @dt
  @at
  @pdlx
  @7972
  @mobile
  @treadwell
  @TestFlow2
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_add vehicle test flow for regular and staggered vehicle_mobile(ALM#7972)
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Tire Size  |
      | 2012 | Honda     | Civic    | Coupe DX | none     | 195/65-R15 |
      | 2010 | Chevrolet | Corvette | Base     | none     | 245/40-R18 |

  @dt
  @at
  @web
  @pdlx
  @7973
  @mobile
  @TestFlow3
  @fitmentPDLX
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_treadwell test flow for regular and staggered vehicle (ALM#7973)
  """Treadwell tag is taken out as the treadwell modal on the homepage is not in QA environment but can be added back
     if it is enabled accordingly """
    When I click on get started on the treadwell model on homepage
    Then Treadwell fitment box in "homepage" should be "displayed"
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed
    When I close popup modal
    And  I click on get started on the treadwell model on homepage
    Then Treadwell fitment box in "homepage" should be "displayed"
    And  I verify my vehicle details: "<Year>, <Make>, <Model>, <Trim>" are "displayed" in the 'My Vehicles' section of the fitment grid
    And  I verify "treadwell" My Vehicles popup displays add vehicle

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Tire Size  |
      | 2012 | Honda     | Civic    | Coupe DX | none     | 195/65-R15 |
      | 2010 | Chevrolet | Corvette | Base     | none     | 245/40-R18 |

  @dt
  @at
  @dtd
  @web
  @pdlx
  @7975
  @mobile
  @defect
  @treadwell
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_PLP - Multiple tires in Top Three should display Appropriate branding
  and display Treadwell branding in quick filters when traversed through treadwell flow and should not be selected when traversed through normal flow (ALM#7975)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed
    When I select view recommended tires
    Then I verify driving priority order on "PLP"
    And  I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify treadwell details section "present"
    And  I verify the "Treadwell" checkbox to be "selected" by default
    And  I verify that the treadwell filter is "present" in the first position in quick filters
    And  I verify that the search refinement filters contain the "single" value(s): "Treadwell Tested"
    And  I verify that the sort by dropdown value is set to "<Value1>"
    And  I verify treadwell top recommended product is "displayed" in 'Top 3 Tiles'
    And  I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PLP" page
    When I extract the product at position "1" from "PLP"
    Then I verify the product is displayed at position "2" in the 'Top 3 Tiles'
    When I click the discount tire logo
    Then I verify my vehicle details: "<Year>, <Make>, <Model>, <Trim>" are "displayed" in the 'My Vehicles' section of the fitment grid
    When I click on the "<Make>" button
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify the "Treadwell" checkbox to be "deselected" by default
    And  I verify that the sort by dropdown value is set to "<Value2>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Tire Size  | Value1      | Value2      | TopRanking | FitmentOption |
      | 2012 | Honda     | Civic    | Coupe EX | none     | 205/55-R16 | Recommended | Best Seller | #1         | tire          |
      | 2010 | Chevrolet | Corvette | Base     | none     | 245/40-R18 | Recommended | Best Seller | #1         | tire          |

  @dt
  @at
  @dtd
  @web
  @pdlx
  @7976
  @mobile
  @treadwell
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_PLP - Multiple tires in Top Three should not display treadwell branding and not display Treadwell branding in quick filters (ALM#7976)
  """ FAILS: If the service is available for the particular fitment """
    When I click on the "Show more" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I select view recommended tires
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify treadwell details section "absent"
    And  I verify treadwell top recommended product is "not displayed" in 'Top 3 Tiles'
    And  I verify if the error message "<Message>" is displayed on the header

    Examples:
      | Year | Make | Model | Trim  | Assembly | Message                                           |
      | 2005 | Ford | GT    | Coupe | none     | Error loading recommended tires for your vehicle. |

  @dt
  @at
  @dtd
  @web
  @pdlx
  @7977
  @mobile
  @treadwell
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_Validation for View recommendation button based on miles driven per year field (ALM#7977)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed
    When I set "Miles Driven Per Year" to " "
    Then I verify that the "view recommended tires" button is disabled
    And  I verify that the "view recommended tires" button color is "grey"
    When I set "Miles Driven Per Year" to "<InvalidMilesPerYear>"
    Then I should verify that error message is displayed for "invalid" "Mile Driven Per Year"
    And  I verify that the "view recommended tires" button is disabled
    And  I verify that the "view recommended tires" button color is "grey"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Tire Size  | InvalidMilesPerYear |
      | 2012 | Honda     | Civic    | Coupe DX | none     | 195/65-R15 | 4                   |
      | 2010 | Chevrolet | Corvette | Base     | none     | 245/40-R18 | 56                  |

  @dt
  @at
  @dtd
  @web
  @pdlx
  @8676
  @mobile
  @defect
  @treadwell
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_Testing for the sequence of items on PLP (ALM#8676)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed
    When I select view recommended tires
    Then I verify the "Treadwell" checkbox to be "selected" by default
    And  I verify that the treadwell filter is "present" in the first position in quick filters
    And  I verify that the search refinement filters contain the "single" value(s): "Treadwell Tested"
    And  I verify that the sort by dropdown value is set to "<Value1>"
    And  I verify the results list is sorted in ascending order by treadwell ranking on PLP

    Examples:
      | Year | Make  | Model | Trim     | Assembly       | Tire Size  | Value1      |
      | 2012 | Honda | Civic | Coupe DX | none           | 195/65-R15 | Recommended |
      | 2014 | BMW   | 640i  | Coupe    | 245 /40 R19 SL | 245/40-R19 | Recommended |

  @dt
  @at
  @dtd
  @web
  @pdlx
  @8063
  @mobile
  @treadwell
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_Top 3 tiles with all 3 as treadwell recommended products (ALM#8063)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed
    When I select view recommended tires
    Then I verify the PLP displays 'Top 3 Tiles' below the PLP header
    And  I verify the PLP displays "<Tile1>","<Tile2>","<Tile3>" in the 'Top 3 Tiles'
    When I extract the product at position "1" from "PLP"
    Then I verify the product is displayed at position "<Tile1Rank>" in the 'Top 3 Tiles'
    When I extract the product at position "2" from "PLP"
    Then I verify the product is displayed at position "<Tile2Rank>" in the 'Top 3 Tiles'
    When I extract the product at position "3" from "PLP"
    Then I verify the product is displayed at position "<Tile3Rank>" in the 'Top 3 Tiles'

    Examples:
      | Year | Make      | Model    | Trim  | Assembly       | Tire Size  | Tile1              | Tile2                       | Tile3                       | Tile1Rank | Tile2Rank | Tile3Rank |
      | 2014 | Audi      | A4       | Sedan | 245 /40 R18 XL | 245/40-R18 | top recommendation | #2 treadwell recommendation | #3 treadwell recommendation | 1         | 2         |  3        |

  @dt
  @at
  @dtd
  @web
  @pdlx
  @6556
  @20590
  @mobile
  @defect
  @treadwell
  @fitmentPDLX
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_PLP_PDL results and TW Quick Filter were to display only on sets tab for staggered vehicle (ALM#6556)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<Optional Size>"
    And  I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Optional Size>" is displayed
    When I select view recommended tires
    Then I verify treadwell details section "present"
    And  I verify the "Treadwell" checkbox to be "selected" by default
    And  I verify that the treadwell filter is "present" in the first position in quick filters
    And  I verify that the search refinement filters contain the "single" value(s): "Treadwell Tested"
    And  I verify that the sort by dropdown value is set to "<Value1>"
    And  I verify treadwell top recommended product is "displayed" in 'Top 3 Tiles'
    And  I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PLP" page
    When I extract the product at position "1" from "PLP"
    Then I verify the product is displayed at position "2" in the 'Top 3 Tiles'
    When I select "FRONT" staggered tab on PLP result page
    Then I verify that the sort by dropdown value is set to "<Value2>"
    And  I verify treadwell details section "absent"
    And  I verify that the treadwell filter is "absent" in the first position in quick filters
    When I select "REAR" staggered tab on PLP result page
    Then I verify that the sort by dropdown value is set to "<Value2>"
    And  I verify treadwell details section "absent"
    And  I verify that the treadwell filter is "absent" in the first position in quick filters
    When I select "SETS" staggered tab on PLP result page
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    Then I verify the current URL contains "/compare/product"
    And  I verify add to cart button is "DISPLAYED"
    And  I should see quantity is set to "<Quantity>" in the cart
    And  I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "Compare products" page
    And  I verify treadwell product banner display "RECOMMENDED" "<TopRanking>" on "Compare products" page

    Examples:
      | Year | Make | Model | Trim        | Assembly       | TopRanking | Value2      | Value1      | Optional Size | Quantity | OptionalSize1 | SizeOption |
      | 2014 | BMW  | 640i  | Convertible | 245 /40 R19 SL | #1         | Best Seller | Recommended | 255/40-19     | 2        | 255/40-R19    | 19         |

  @dt
  @at
  @dtd
  @web
  @pdlx
  @10469
  @mobile
  @treadwell
  @fitmentPDLX
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_TREADWELL_PLP_Product driving details container present for all products on PLP (ALM#10469)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed
    When I select view recommended tires
    Then I verify all the products listed on PLP contains product driving details container
    When I extract the "<Ranking>" product tire life and cost
    And  I select the "<Ranking>" product result image on "PLP" page
    Then I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PDP" page
    And  I verify tire life and cost values of the selected vehicle

    Examples:
      | Year | Make  | Model | Trim     | Assembly       | Tire Size  | TopRanking | Ranking |
      | 2012 | Honda | Civic | Coupe DX | none           | 195/65-R15 | #1         | 1       |
      | 2014 | BMW   | 640i  | Coupe    | 245 /40 R19 SL | 245/40-R19 | #1         | 1       |

  @dt
  @dtd
  @web
  @16543
  Scenario Outline: HYBRIS_CUSTOMER_MYACCOUNTS_My Vehicles Modal_Remove Recent Search(ALM#16543)
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    And  I open the My Vehicles popup
    And  I click on the "delete" link
    And  I click on the "No, keep this vehicle" button
    Then I verify "edit" is displayed on "MyVehicles Modal"
    When I click on the "delete" link
    And  I click on the "Yes, delete this vehicle" button
    Then I verify "no recent vehicles" element displayed

    Examples:
      | Year | Make   | Model | Trim      |
      | 2014 | Toyota | Camry | Hybrid SE |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH Search by Size - Verify that the user is encouraged with messaging to enter in a vehicle fitment for tires (4.10_OCW-798_1)
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY SIZE" menu option
    And  I select the "<Width>" fitment grid option
    And  I select the "<Ratio>" fitment grid option
    And  I select the "<Diameter>" fitment grid option
    And  I click on the "VIEW TIRES" button
    Then I verify the "PDP" banner color is "Yellow"
    And  I verify the message on the "PDP" banner contains "Enter your vehicle to ensure these tires fit"

    Examples:
      | Width | Ratio | Diameter |
      | 235   | 40    | 18       |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH Search by Size - Verify that the user is encouraged with messaging to enter in a vehicle fitment for wheels (4.10_OCW-798_2)
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY SIZE" menu option
    And  I select the "<Diameter>" fitment grid option
    And  I select the "<Wheel Width>" fitment grid option
    And  I select the "<Bolt Pattern>" fitment grid option
    And  I click on the "VIEW WHEELS" button
    Then I verify the "PDP" banner color is "Yellow"
    And  I verify the message on the "PDP" banner contains "Enter your vehicle to ensure these wheels fit"

    Examples:
      | Diameter | Wheel Width | Bolt Pattern |
      | 17       | Any         | 4-100.0 Mm   |

  @at
  @dt
  @dtd
  @web
  @21240
  @staggeredExperience
  @coreScenarioStaggeredExperience
  Scenario Outline: HYBRIS_SEARCH Search by Size - Verify that once the user has deliver a front and rear size by dimension the staggered PLP is presented with results from the fitment service – Tires (4.10_OCW-798_3) (ALM#21240)
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY SIZE" menu option
    And  I select the "<Width>" fitment grid option
    And  I select the "<Ratio>" fitment grid option
    And  I select the "<Diameter>" fitment grid option
    And  I click on the "<link>" link
    And  I select the "<Width2>" fitment grid option
    And  I select the "<Ratio2>" fitment grid option
    And  I select the "<Diameter2>" fitment grid option
    And  I click on the "VIEW TIRES" button
    And  I click on the "continue without vehicle" link
    Then I verify the "PDP" banner color is "Yellow"
    And  I verify the message on the "PDP" banner contains "Enter your vehicle to ensure these tires fit"
    And  I verify "SETS" staggered option tab is displayed on PLP result page
    And  I verify "FRONT" staggered option tab is displayed on PLP result page
    And  I verify "REAR" staggered option tab is displayed on PLP result page

    Examples:
      | Width | Ratio | Diameter | link                   | Width2 | Ratio2 | Diameter2 |
      | 245   | 35    | 20       | + add a rear tire size | 275    | 30     | 20        |

  @at
  @dt
  @dtd
  @web
  @21248
  @staggeredExperience
  @coreScenarioStaggeredExperience
  Scenario Outline: HYBRIS_SEARCH Search by Size - Verify that once the user has deliver a front and rear size by dimension the staggered PLP is presented with results from the fitment service  - Wheels (4.10_OCW-798_4) (ALM#21248)
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY SIZE" menu option
    And  I select the "<Diameter>" fitment grid option
    And  I select the "<Wheel Width>" fitment grid option
    And  I select the "<Bolt Pattern>" fitment grid option
    And  I click on the "<link>" link
    And  I select the "<Diameter2>" fitment grid option
    And  I select the "<Wheel Width>" fitment grid option
    And  I select the "<Bolt Pattern>" fitment grid option
    And  I click on the "VIEW WHEELS" button
    And  I click on the "continue without vehicle" link
    Then I verify "REAR" staggered option tab is displayed on PLP result page
    And  I verify "FRONT" staggered option tab is displayed on PLP result page
    And  I verify the "PDP" banner color is "Yellow"
    And  I verify the message on the "PDP" banner contains "Enter your vehicle to ensure these wheels fit"
    And  I verify the "Brands" filter section(s) is/are displayed
    And  I verify that the sort by dropdown value is set to "Best Seller"

    Examples:
      | Diameter | Wheel Width | Bolt Pattern | link                    | Diameter2 |
      | 20       | Any         | 5-120.0 Mm   | + add a rear wheel size | 20        |

  @at
  @dt
  @dtd
  @web
  @21249
  @staggeredExperience
  @coreScenarioStaggeredExperience
  Scenario Outline: HYBRIS_SEARCH_Verify that after doing a search by size with no Results user is taken to the PLP Error page (4.10_OCW-798_5) (ALM#21249)
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY SIZE" menu option
    And  I select the "<Diameter>" fitment grid option
    And  I select the "<Wheel Width>" fitment grid option
    And  I select the "<Bolt Pattern>" fitment grid option
    And  I click on the "<link>" link
    And  I select the "<Diameter2>" fitment grid option
    And  I select the "<Wheel Width>" fitment grid option
    And  I select the "<Bolt Pattern>" fitment grid option
    And  I click on the "VIEW WHEELS" button
    And  I should see text "There are no sets found for the size entered.  Please try another size." present in the page source

    Examples:
      | Diameter | Wheel Width | Bolt Pattern | link                    | Diameter2 |
      | 17       | Any         | 5-0.0 Mm     | + add a rear wheel size | 20        |

  @at
  @dt
  @dtd
  @web
  @21250
  @staggeredExperience
  @coreScenarioStaggeredExperience
  Scenario Outline: HYBRIS_SEARCH Search by Size - Verify PLP Page Tires - Verify that the size selection  made by the user when searching by size  persisted into each of the Sets, Front, and Rear tabs in the tires PLP(4.10_OCW-807_3) (ALM#21250)
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY SIZE" menu option
    And  I select the "<Width>" fitment grid option
    And  I select the "<Ratio>" fitment grid option
    And  I select the "<Diameter>" fitment grid option
    And  I click on the "<link>" link
    And  I select the "<Width2>" fitment grid option
    And  I select the "<Ratio2>" fitment grid option
    And  I select the "<Diameter2>" fitment grid option
    And  I click on the "VIEW TIRES" button
    And  I click on the "continue without vehicle" link
    Then I verify the "PDP" banner color is "Yellow"
    Then I verify default "<Sortby>" sortby option
    When I select "FRONT" staggered tab on PLP result page
    Then I verify default "<Sortby>" sortby option
    When I select "REAR" staggered tab on PLP result page
    Then I verify default "<Sortby>" sortby option

    Examples:
      | Width | Ratio | Diameter | link                   | Width2 | Ratio2 | Diameter2 | Sortby      |
      | 245   | 35    | 20       | + add a rear tire size | 275    | 30     | 20        | Best Seller |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  @coreScenarioStaggeredExperience
  Scenario Outline: HYBRIS_SEARCH Search by Size - PLP Page Wheels - Verify that the size selection  made by the user when searching by size  persisted into each of the Sets, Front, and Rear tabs in the wheels  PLP(4.10_OCW-807_4)
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY SIZE" menu option
    And  I select the "<Diameter>" fitment grid option
    And  I select the "<Width>" fitment grid option
    And  I select the "<Bolt>" fitment grid option
    And  I click on the "<link>" link
    And  I select the "<Diameter2>" fitment grid option
    And  I select the "<Width2>" fitment grid option
    And  I select the "<Bolt2>" fitment grid option
    And  I click on the "VIEW WHEELS" button
    And  I click on the "continue without vehicle" link
    Then I verify the "PDP" banner color is "Yellow"
    And  I verify default "<Sortby>" sortby option
    When I select "FRONT" staggered tab on PLP result page
    Then I verify default "<Sortby>" sortby option
    When I select "REAR" staggered tab on PLP result page
    Then I verify default "<Sortby>" sortby option

    Examples:
      | Diameter | Width | Bolt       | link                    | Diameter2 | Width2 | Bolt2      | Sortby      |
      | 20       | 8.5   | 5-120.0 mm | + add a rear wheel size | 20        | 9.5    | 5-120.0 mm | Best Seller |

  @at
  @dt
  @dtd
  @web
  @6612
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PLP Page Tires - Verify that when navigating from rear/front tabs to sets tab the sorting it is always default by Best Seller for tires(ALM#6612)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify that the sort by dropdown value is set to "<Value1>"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify that the sort by dropdown value is set to "<Value2>"
    When I select "REAR" staggered tab on PLP result page
    Then I verify that the sort by dropdown value is set to "<Value2>"

    Examples:
      | Year | Make | Model | Trim  | Assembly       | FitmentOption | Value1      | Value2      |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL | tire          | Best Seller | Best Seller |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL | wheel         | Best Match  | Best Seller |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: PDP - Verify If the subtotal for the products shown on the PDP and Shopping Cart is above a certain amount, a message will appear which contains a link to additional financing information – Staggered(4.10_OCW-824_1)
    When I search for a product "<ProductCode>"
    And  I enter "<Quantity>" into the first item quantity text box
    Then I verify payment plan available is "displayed"
    When I click on the "learn more" link
    Then I am brought to the page with header "<Header>"
    When I search for a product "<ProductCode>"
    And  I add item to my cart and "View shopping Cart"
    And  I update the quantity to "<Quantity>"
    Then I verify payment plan available is "displayed"
    When I click on the "learn more" link
    Then I am brought to the page with header "<Header>"
    When I select mini cart and "Clear Cart"
    And  I search for a product "<ProductCode>"
    And  I enter "<Quantity>" into the first item quantity text box
    And  I add item to my cart and "View shopping Cart"
    Then I verify payment plan available is "displayed"
    When I click on the "learn more" link
    Then I am brought to the page with header "<Header>"

    Examples:
      | ProductCode | Quantity | Header                     |
      | 19609       | 99       | Discount Tire Credit Card  |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario: PDP - Verify If the subtotal for the products shown on the PDP is above a certain amount, a message will appear which contains a link to additional financing information – Staggered(4.10_OCW-824_3)
    When I search for a product "29935"
    Then I verify payment plan available is "not displayed"

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_Verify that upon clicking the "View on Vehicle" link the user is presented with a different image for the front and rear wheels (4.10_OCW-802_1)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>"
    Then I verify the PLP header message contains "<Header>"
    When I click on the "View on my vehicle" link
    Then I verify View On My Vehicle Popup page is displayed

    Examples:
      | Year | Make | Model | Trim  | Assembly       | FitmentOption | Header  |
      | 2014 | BMW  | 640i  | Coupe | 245 /35 R20 XL | wheel         | Results |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_Verify that upon clicking the "View on Vehicle" link the user is presented with a different image for the front and rear wheels (4.10_OCW-811_1)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>"
    Then I verify the PLP header message contains "<Header>"
    When I click on the first available product result with view on my vehicle link on PLP page
    Then I verify the "PDP" banner color is "Green"
    When I click on the "View on my vehicle" link
    Then I verify View On My Vehicle Popup page is displayed

    Examples:
      | Year | Make | Model | Trim  | Assembly       | FitmentOption | Header  |
      | 2014 | BMW  | 640i  | Coupe | 245 /35 R20 XL | wheel         | Results |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PLP – Verify that the OE badge is displayed in the PLP when original equipment is selected by a user (4.10_OCW-804_1)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    Then I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OEREarWheelSize>"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OEREarWheelSize>"
    When I select "REAR" staggered tab on PLP result page
    Then I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OEREarWheelSize>"

    Examples:
      | Year | Make | Model | Trim  | Assembly       | Header  | OELabel | OEFrontTireSize | OEFrontWheelSize | OERearTireSize | OEREarWheelSize |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL | Results | O.E     | 245/40/R19      | 19               | 275/35/R19     | 19              |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PLP – Verify that Optional badge is displayed in the PLP when the optional size is selected - staggered (example: +1 or -1) (4.10_OCW-804_2)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"
    And  I verify that the optional tire and wheel size is "collapsed"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"
    When I select "REAR" staggered tab on PLP result page
    Then I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"

    Examples:
      | Year | Make | Model | Trim  | Assembly       | Header  | SizeOption | TireSize  | NonOELabel | WheelSize |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL | Results | 20         | 285/30-20 | +1’’       | 20        |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PLP – Verify that Optional badge is displayed in the PLP when the optional size is selected - non-staggered (example: +1 or -1) (4.10_OCW-804_2)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"
    And  I verify that the optional tire and wheel size is "collapsed"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    Then I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header  | SizeOption | TireSize  | NonOELabel | WheelSize |
      | 2012 | Honda | Civic | Coupe DX | none     | Results | 19         | 225/35-19 | +4’’       | 19        |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PDP – Verify that the OE badge is displayed in the PDP when original equipment is selected by a user (4.10_OCW-809_1)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I click on the product "<ItemCode>"
    Then I verify "Staggered" Vehicle Details Banner on "PDP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"

    Examples:
      | Year | Make | Model | Trim  | Assembly       | ItemCode | OELabel | OEFrontTireSize | Header  |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL | 25991    | O.E     | 245/40/R19      | Results |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PDP – Verify that the OE badge is displayed in the PDP when original equipment is selected by a user (4.10_OCW-809_2)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"
    And  I verify that the optional tire and wheel size is "collapsed"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I click on the product "<ItemCode>"
    Then I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"

    Examples:
      | Year | Make | Model | Trim  | Assembly       | SizeOption | TireSize  | NonOELabel | WheelSize | ItemCode | Header  |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL | 19         | 255/40-19 | +0’’       | 19        | 19220    | Results |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PDP – Verify that when the user return from the PLP to the FVRM then user should land on  the  FVRM  with the previous size selection - Fitment Flow - (4.10_OCW-797_1)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"
    And  I verify that the optional tire and wheel size is "collapsed"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"
    When I click the discount tire logo
    And  I select vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>"
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly       | SizeOption | TireSize  | NonOELabel | WheelSize | Header  |
      | 2014 | BMW   | 640i  | Coupe    | 245 /40 R19 SL | 19         | 255/40-19 | +0’’       | 19        | Results |
      | 2012 | Honda | Civic | Coupe DX | none           | 18         | 225/40-18 | +3’’       | 18        | Results |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PDP – Verify that when the user return from the PDL Modal to the FVRM then user should land on the FVRM with the previous size selection - PDL cancel (4.10_OCW-797_2 )
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    And  I close view on my vehicle modal
    And  I select vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>"
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"

    Examples:
      | Year | Make | Model | Trim  | Assembly       | SizeOption | TireSize  | NonOELabel | WheelSize |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL | 19         | 255/40-19 | +0’’       | 19        |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PDP – Verify that when the user return from the PLP to the FVRM through selecting or adding a new vehicle  then user should land with the OE selection - Fitment Flow - (4.10_OCW-797_4 )
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I open the My Vehicles popup
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "Non-OE Tire" fitment banner with "<NonOELabel>" and "<WheelSize>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly       | SizeOption | TireSize  | NonOELabel | WheelSize |
      | 2012 | Honda | Civic | Coupe DX | none           | 18         | 225/40-18 | +3’’       | 18        |
      | 2014 | BMW   | 640i  | Coupe    | 245 /40 R19 SL | 19         | 255/40-19 | +0’’       | 19        |

  @at
  @dt
  @dtd
  @web
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_PDP – Verify User selected an Optional Size set tabs - Replace the selected Original Equipment - Display applicable links in the modal which navigate to the user to PDL or the PLP for tires (4.10_OCW-790_1  4.10_OCW-790_3)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify "<Vehicle Type>" Vehicle Details Banner on "My Vehicles Modal" contains "Non-OE FRONT Wheel" fitment banner with "<NonOELabel>" and "<WheelSize>"
    When I select a fitment option "<FitmentOption>"
    Then I should see the fitment panel page with fitment options
    When I "expand" "optional tire and wheel size"
    And  I select the "<OESize>" fitment box option
    And  I select a fitment option "O.E"
    Then I should see the fitment panel page with fitment options

    Examples:
      | Year | Make  | Model | Trim     | Assembly       | Vehicle Type | SizeOption | TireSize  | NonOELabel | WheelSize | FitmentOption | OESize |
      | 2012 | Honda | Civic | Coupe DX | none           | Regular      | 18         | 225/40-18 | +3’’       | 18        | tire          | 15     |
      | 2014 | BMW   | 640i  | Coupe    | 245 /40 R19 SL | Staggered    | 19         | 255/40-19 | +0’’       | 19        | tire          | 19     |

  @dt
  @web
  @bvt
  @core
  @21291
  @coreScenarioSearch
  @bvtSearchByFitmentBySizeByBrandTires
  Scenario Outline: Verify the Fitment Match and Search through Shop By Vehicle/Size/Brand for Tires (ALM#21291)
    When I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year1> <Make1>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year1>" "<Make1>" "<Model1>" and "<Trim1>"
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    When I clear all the currently active filters on the PLP page
    And  I do a "my vehicles" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select a fitment option "<SizeOption>"
    And  I select a fitment option "<TireSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" "tire" size search with details "205, 55, 16"
    Then I verify the PLP header message contains "tire result"
    And  I verify the "PLP" results banner message contains "<TireSize1>"
    When I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" "brand" "tire" search with option: "<Brand>"
    And  I select "All-Season tires" from the Product Brand page
    Then I can see "<Brand>" Brand PLP page
    When I do a "my vehicles" vehicle search with details "<Year3>" "<Make3>" "<Model3>" "<Trim3>" "<Assembly>"
    And  I select a fitment option "tire"
    Then I verify Vehicle Details Banner on "PLP" contains "<Year3>" "<Make3>" "<Model3>" and "<Trim3>"
    When I click on the "Shop all Tires" button
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | Year1 | Make1 | Model1 | Trim1    | Assembly | Year2 | Make2 | Model2 | Trim2    | Year3 | Make3  | Model3 | Trim3      | SizeOption | TireSize  | Brand    | TireSize1 | Checkout | Reason                              | Customer                    | Credit Card |
      | 2012  | Honda | Civic  | Coupe EX | none     | 2012  | Honda | Accord | EX Coupe | 2014  | Nissan | 370Z   | Coupe Base | 16         | 205/65-16 | MICHELIN | 205/55R16 | default  | Make an appointment at a later time | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @dt
  @web
  @bvt
  @core
  @21251
  @coreScenarioSearch
  @bvtScenarioSearchByFitmentBySizeByBrandWheels
  Scenario Outline: Verify the Fitment Match and Search through Shop By Vehicle/Size/Brand for Wheels (ALM#21251)
    When I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    Then I should see the fitment panel with vehicle "<Year1> <Make1>"
    When I select a fitment option "wheel"
    Then I verify Vehicle Details Banner on "PLP" contains "<Year1>" "<Make1>" "<Model1>" and "<Trim1>"
    When I do a "my vehicles" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "none"
    And  I "expand" "optional tire and wheel size"
    And  I select a fitment option "<SizeOption>"
    And  I select a fitment option "<OptionalWheelSize>"
    And  I select a fitment option "wheel"
    Then I verify the PLP header message contains "wheel result"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year2>" "<Make2>" "<Model2>" and "<Trim2>"
    When I click the discount tire logo
    And  I do a "my vehicles" "wheel" size search with details "<Diameter>, <WheelWidth>, <BoltPattern>"
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    Then I verify the PLP header message contains "wheel result"
    And  I verify the "PLP" results banner message contains "<WheelSize>"
    When I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" "brand" "wheel" search with option: "<Brand>"
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    And  I select shop all from the Product Brand page
    Then I can see "<Brand>" Brand PLP page
    When I do a "my vehicles" vehicle search with details "<Year3>" "<Make3>" "<Model3>" "<Trim3>" "none"
    And  I select a fitment option "wheel"
    Then I verify the PLP header message contains "wheel result"
    When I add "first available" item of type "front" to my cart and "Continue Shopping"
    And  I add "first available" item of type "rear" to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click continue for appointment customer details page
    Then I verify date and time on the customer details appointment page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | Year1 | Make1 | Model1 | Trim1    | Year2 | Make2 | Model2 | Trim2    | Year3 | Make3  | Model3 | Trim3      | SizeOption | OptionalWheelSize | Diameter | WheelSize | WheelWidth | BoltPattern       | Brand       | Checkout         | Customer                    | Credit Card            |
      | 2012  | Honda | Civic  | Coupe EX | 2012  | Honda | Accord | EX Coupe | 2014  | Nissan | 370Z   | Coupe Base | 16         | 205/60-16         | 15       | 15        | 8.0        | 5-114.3 mm/5-4.5" | DRAG WHEELS | with appointment | DEFAULT_CUSTOMER_BOPIS_AMEX | American Express Bopis |

  @dt
  @web
  @bvt
  @core
  @21252
  @wheelConfigurator
  @staggeredExperience
  @bvtScenarioWheelConfigurator
  @coreScenarioWheelConfigurator
  Scenario Outline: Verify the Wheel Configurator UI2 (ALM#21252)
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    Then I should see "title" "<Title>" on the Wheel Configurator modal window displayed
    And  I should see "subtitle" "<SubTitle>" on the Wheel Configurator modal window displayed
    And  I should see the "X" on the "Wheel Configurator modal window" page
    When I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    Then I should see vehicle "<Make>" on the Wheel Configurator modal window
    And  I should see the "Vehicle Image" on the "Wheel Configurator modal window" page
    And  I should see the wheel results displayed on the Wheel Configurator modal window
    And  I should see the wheel has Image Name Brand Price displayed
    And  I verify the wheel count on the header should match with the displayed wheel results
    And  I should see the default "Wheel Color" in the Wheel Configurator modal window
    And  I should see the default "Wheel Size" in the Wheel Configurator modal window
    When I choose FILTER option
    Then I should see the wheel filter options on Wheel Configurator modal window
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    When I enter "<NewBrand>" into the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "text": "<NewBrand>"
    When I select the "<NewBrand>" checkbox
    And  I select the "Price (Low To High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "<SortValue>"
    When I choose filter option "Apply Filters" on Wheel Configurator modal window
    Then I verify the Wheel Configurator filters are "not displayed"
    When I choose a wheel on Wheel Configurator modal window
    And  I select "Add To Cart"
    Then I should see Cart Popup has "View shopping Cart" option displayed
    And  I should see Cart Popup has "Continue Shopping" option displayed
    And  I should see Cart Popup has "Item added to cart" option displayed
    And  I should see Cart Popup has "You can choose product add-ons and schedule installation during check out" option displayed

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Title              | SubTitle                                                               | NewBrand | SortValue           |
      | 2012 | Honda | Civic | Coupe DX | none     | Enter your vehicle | Please note that not all vehicles are represented in the Wheel Browser | Rage     | Price (Low to High) |

  @smoke
  @search_staggeredresult
  Scenario Outline: HYBRIS_STAGGERED_SEARCH (ALM # NONE)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly |
      | 2010 | Chevrolet | Corvette | Base | none     |

  @dt
  @web
  @20280
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - PLP - TIRES and WHEELS - Product fits(ALM #20280)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"
    And  I verify the message on the "CheckFit" banner contains "VERIFIEDThis product fits your:"
    And  I verify Vehicle Details Banner on "CheckFit" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | FitmentOption |
      | 2012 | Honda     | Civic    | Coupe DX | none     | tire          |
      | 2012 | Honda     | Civic    | Coupe DX | none     | wheel         |
      | 2010 | Chevrolet | Corvette | Base     | none     | tire          |
      | 2010 | Chevrolet | Corvette | Base     | none     | wheel         |

  @dt
  @web
  @20281
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - PDP - TIRES and WHEELS - Product fits(ALM #20281)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I click on the first available product result on PLP page
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"
    And  I verify the message on the "CheckFit" banner contains "VERIFIEDThis product fits your:"
    And  I verify Vehicle Details Banner on "CheckFit" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | FitmentOption |
      | 2012 | Honda     | Civic    | Coupe DX | none     | tire          |
      | 2012 | Honda     | Civic    | Coupe DX | none     | wheel         |
      | 2010 | Chevrolet | Corvette | Base     | none     | tire          |
      | 2010 | Chevrolet | Corvette | Base     | none     | wheel         |

  @dt
  @web
  @20282
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Compare - TIRES - Product fits(ALM #20282)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"
    And  I verify the message on the "CheckFit" banner contains "VERIFIEDThis product fits your:"
    And  I verify Vehicle Details Banner on "CheckFit" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | FitmentOption |
      | 2012 | Honda     | Civic    | Coupe DX | none     | tire          |
      | 2010 | Chevrolet | Corvette | Base     | none     | tire          |

  @dt
  @web
  @20283
  @checkFit
  @coreScenarioCheckFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Wheel Configurator - WHEELS - Product fits(ALM #20283)
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window with "Add to Cart" button
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"
    And  I verify the message on the "CheckFit" banner contains "VERIFIEDThis product fits your:"
    And  I verify Vehicle Details Banner on "CheckFit" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly    |
      | 2012 | Honda | Civic | Coupe DX | none        |
      | 2014 | BMW   | 228i  | Coupe    | 205 /50 R17 |

  @dt
  @web
  @20284
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Browse TIRES by Brand(ALM #20284)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I should see text "What vehicle are you shopping for?" present in the page source
    And  I should see text "Choose from one of your vehicles below to ensure these products fit." present in the page source
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Product Type |
      | 2012 | Honda     | Civic    | Coupe DX | none     | TIRES        |
      | 2012 | Honda     | Civic    | Coupe DX | none     | WHEELS       |
      | 2010 | Chevrolet | Corvette | Base     | none     | TIRES        |
      | 2010 | Chevrolet | Corvette | Base     | none     | WHEELS       |

  @dt
  @web
  @20285
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Browse TIRES by Categoty(ALM #20285)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> SEARCH" menu option
    Then I should see text "What vehicle are you shopping for?" present in the page source
    And  I should see text "Choose from one of your vehicles below to ensure these products fit." present in the page source
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Product Type |
      | 2012 | Honda     | Civic    | Coupe DX | none     | TIRE         |
      | 2010 | Chevrolet | Corvette | Base     | none     | TIRE         |

  @dt
  @web
  @20286
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Browse WHEELS by Style(ALM #20286)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "Wheel Style" View All link in the header
    Then I should see text "What vehicle are you shopping for?" present in the page source
    And  I should see text "Choose from one of your vehicles below to ensure these products fit." present in the page source
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Product Type |
      | 2012 | Honda     | Civic    | Coupe DX | none     | WHEEL        |
      | 2010 | Chevrolet | Corvette | Base     | none     | WHEEL        |

  @dt
  @web
  @20287
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Browse TIRES by Size(ALM #20287)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY SIZE" menu option
    And  I do a "homepage" "tire" size search with details "<Width>, <Ratio>, <Diameter>"
    Then I should see text "What vehicle are you shopping for?" present in the page source
    And  I should see text "Choose from one of your vehicles below to ensure these products fit." present in the page source
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Product Type | Width | Ratio | Diameter |
      | 2012 | Honda     | Civic    | Coupe DX | none     | TIRES        | 195   | 65    | 15       |
      | 2010 | Chevrolet | Corvette | Base     | none     | TIRES        | 285   | 30    | 19       |

  @dt
  @web
  @20288
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Browse WHEELS by Size(ALM #20288)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY SIZE" menu option
    When I do a "homepage" "wheel" size search with details "<Width>, <Wheel Width>, <Diameter>"
    Then I should see text "What vehicle are you shopping for?" present in the page source
    And  I should see text "Choose from one of your vehicles below to ensure these products fit." present in the page source
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Product Type | Width | Wheel Width | Diameter   |
      | 2012 | Honda     | Civic    | Coupe DX | none     | WHEELS       | 15    | 7.0         | 5-100.0 MM |
      | 2010 | Chevrolet | Corvette | Base     | none     | WHEELS       | 15    | 7.0         | 5-100.0 MM |

  @dt
  @web
  @20289
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Browse TIRES by Brand with cleared fitment(ALM #20289)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I search for a product "<SearchText>"
    And  I click on "Brand under Tires" in search results Page
    Then I should see text "What vehicle are you shopping for?" present in the page source
    And  I should see text "Choose from one of your vehicles below to ensure these products fit." present in the page source
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SearchText     |
      | 2012 | Honda | Civic | Coupe DX | none     | NoSearchResult |

  @dt
  @web
  @20290
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Browse WHEELS by Brand with cleared fitment(ALM #20290)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I search for a product "<SearchText>"
    And  I click on "Brand under Wheels" in search results Page
    Then I should see text "What vehicle are you shopping for?" present in the page source
    And  I should see text "Choose from one of your vehicles below to ensure these products fit." present in the page source
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SearchText     |
      | 2012 | Honda | Civic | Coupe DX | none     | NoSearchResult |

  @dt
  @web
  @20291
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - Browse TIRES by Categoty with cleared fitment(ALM #20291)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I search for a product "<SearchText>"
    And  I click on the "All-Season tires" link
    Then I should see text "What vehicle are you shopping for?" present in the page source
    And  I should see text "Choose from one of your vehicles below to ensure these products fit." present in the page source
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SearchText     |
      | 2012 | Honda | Civic | Coupe DX | none     | NoSearchResult |

  @dt
  @web
  @20292
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Cart That Fit - Browse TIRES by Brand(ALM #20292)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "Continental" brand image
    And  I select "All-Season tires" from the Product Brand page
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    Then I am brought to the page with header "All Brands"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | TIRES        | 19661    |

  @dt
  @web
  @20293
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Cart That Does not Fit - Browse WHEELS by STYLE(ALM #20293)
    When I open the "<Product Type>" navigation link
    And  I click the "Wheel Style" View All link in the header
    And  I click on the "continue without vehicle" link
    And  I click on the "CHROME Wheels" link
    And  I select the "CRAGAR" brand image
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I open the "<Product Type>" navigation link
    And  I click the "Wheel Style" View All link in the header
    Then I should see text "Current cart items do not fit this vehicle" present in the page source
    And  I should see text "You must remove previous items from your cart or ignore the selected vehicle below before continuing." present in the page source

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | WHEELS       | 56852    |

  @dt
  @web
  @20294
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Cart That Fit - Browse TIRES by Size (equal to active vehicle size)(ALM #20294)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY SIZE" menu option
    And  I do a "homepage" "tire" size search with details "<Width>, <Radio>, <Diameter>"
    And  I click on the "continue without vehicle" link
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    Then I am brought to the page with header "All Brands"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | Width | Radio | Diameter | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | TIRES        | 195   | 65    | 15       | 29935    |

  @dt
  @web
  @20295
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Cart That Does not Fit - Browse WHEELS by Size (different to active vehicle size)(ALM #20295)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY SIZE" menu option
    And  I do a "homepage" "tire" size search with details "<Width>, <Wheel Width>, <Diameter>"
    And  I click on the "continue without vehicle" link
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    Then I should see text "Current cart items do not fit this vehicle" present in the page source
    And  I should see text "You must remove previous items from your cart or ignore the selected vehicle below before continuing." present in the page source

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | Width | Wheel Width | Diameter | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | TIRES        | 205   | 65          | 15       | 43027    |

  @dt
  @web
  @20296
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Session - “Continue with this vehicle” CTA(ALM #20296)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    Then I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type |
      | 2012 | Honda | Civic | Coupe DX | none     | TIRES        |

  @dt
  @web
  @20297
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Session - “Ignore Vehicle” CTA(ALM #20297)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    Then I verify vehicle "<Year2> <Make2> <Model2>" "not displayed" in "Recent Searches" section
    And  I am brought to the page with header "All Brands"
    When I select the "Continental" brand image
    And  I select "All-Season tires" from the Product Brand page
    Then I verify the "PLP" banner color is "Yellow"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Year2 | Make2 | Model2 | Trim2    | Assembly | Product Type |
      | 2012 | Honda | Civic | Coupe DX | none     | 2012  | Honda | Accord | EX Coupe | none     | TIRES        |

  @dt
  @web
  @20298
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Session - Select a recent vehicle(ALM #20298)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I do a "homepage" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly2>"
    Then I should see the fitment panel with vehicle "<Year2> <Make2>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I select a vehicle with fitment details "<Year>" "<Make>" "<Model>" "<Trim>"
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    Then I am brought to the page with header "All Brands"
    When I select the "Continental" brand image
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Year2 | Make2 | Model2 | Trim2    | Assembly2 | Product Type |
      | 2012 | Honda | Civic | Coupe DX | none     | 2012  | Honda | Accord | EX Coupe | none      | TIRES        |

  @dt
  @web
  @20299
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Session - Add a new vehicle(ALM #20299)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly2>"
    And  I click on the "Update Vehicle" link
    Then I verify that selected vehicle "<Year2> <Make2> <Model2>" as the current vehicle in Checkfit
    When I click on the "CONTINUE WITH THIS VEHICLE" link
    Then I am brought to the page with header "All Brands"
    When I select the "Michelin Tires" brand image
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Vehicle Details Banner on "PLP" contains "<Year2>" "<Make2>" "<Model2>" and "<Trim2>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Year2 | Make2 | Model2 | Trim2    | Assembly2 | Product Type |
      | 2012 | Honda | Civic | Coupe DX | none     | 2012  | Honda | Accord | EX Coupe | none      | TIRES        |

  @dt
  @web
  @20300
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Session - Edit selected vehicle(ALM #20300)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "edit vehicle" link
    And  I do a "CheckFitEdit" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly2>"
    And  I click on the "Update Vehicle" link
    Then I verify that selected vehicle "<Year2> <Make2> <Model2>" as the current vehicle in Checkfit
    When I click on the "CONTINUE WITH THIS VEHICLE" link
    Then I am brought to the page with header "All Brands"
    When I select the "Michelin Tires" brand image
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Vehicle Details Banner on "PLP" contains "<Year2>" "<Make2>" "<Model2>" and "<Trim2>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Year2 | Make2 | Model2 | Trim2    | Assembly2 | Product Type |
      | 2012 | Honda | Civic | Coupe DX | none     | 2012  | Honda | Accord | EX Coupe | none      | TIRES        |

  @dt
  @web
  @20301
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Session Items in Cart that does not Fit new vehicle - “continue and clear current cart”(ALM #20301)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "Continental" brand image
    And  I select "All-Season tires" from the Product Brand page
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    Then I should see text "Current cart items do not fit this vehicle" present in the page source
    And  I should see text "You must remove previous items from your cart or ignore the selected vehicle below before continuing." present in the page source
    When I click on the "Continue & clear current cart" link
    Then I verify the header cart total is "$0.00"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | TIRES        | 26236    |

  @dt
  @web
  @20302
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Session Items in Cart that does not Fit new vehicle - view shopping cart(ALM #20302)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "Continental" brand image
    And  I select "All-Season tires" from the Product Brand page
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    Then I should see text "Current cart items do not fit this vehicle" present in the page source
    And  I should see text "You must remove previous items from your cart or ignore the selected vehicle below before continuing." present in the page source
    When I click on the "View shopping Cart" link
    Then I verify "Shopping cart" page is displayed

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | TIRES        | 26236    |

  @dt
  @web
  @20303
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in Session Items in Cart that does not Fit new vehicle - continue without vehicle(ALM #20303)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "Continental" brand image
    And  I select "All-Season tires" from the Product Brand page
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    Then I should see text "Current cart items do not fit this vehicle" present in the page source
    And  I should see text "You must remove previous items from your cart or ignore the selected vehicle below before continuing." present in the page source
    When I click on the "continue without vehicle" link
    Then I verify the "PDP" banner color is "Yellow"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | TIRES        | 26236    |

  @dt
  @web
  @20304
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - Vehicle in session - Browse Tire by Brand from Compare Page(ALM #20304)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "Continental" brand image
    And  I select "All-Season tires" from the Product Brand page
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    Then I should see text "Current cart items do not fit this vehicle" present in the page source
    And  I should see text "You must remove previous items from your cart or ignore the selected vehicle below before continuing." present in the page source
    When I click on the "continue without vehicle" link
    Then I verify the "PDP" banner color is "Yellow"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | TIRES        | 26236    |

  @dt
  @web
  @20305
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire and Wheel by Brand and to Cart from Compare Page(ALM #20305)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "<Brand>" brand image
    And  I select "<Type>" from the Product Brand page
    Then I verify the PLP page is displayed
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page

    Examples:
      | Product Type | ItemCode | Type             | Brand       |
      | TIRES        | 26236    | All-Season tires | Continental |
      | WHEELS       | 51760    | multi-spoke      | konig       |

  @dt
  @web
  @20306
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire by Brand and to Cart from PDP(ALM #20306)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "Continental" brand image
    And  I select "All-Season tires" from the Product Brand page
    Then I verify the PLP page is displayed
    When I click on the product "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page

    Examples:
      | Product Type | ItemCode | ProductName                 |
      | TIRES        | 19661    | Control Contact Touring A/S |

  @dt
  @web
  @20307
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Wheel by homepage configurator selection and add to Cart from Wheel configurator(ALM #20307)
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    And  I choose a wheel on Wheel Configurator modal window
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @web
  @20308
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire by Category and to Cart from Comparate Page(ALM #20308)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> SEARCH" menu option
    And  I click on the "continue without vehicle" link
    And  I select "All-Season tires" from the Product Brand page
    And  I click on the "SHOP ALL ALL-SEASON BRANDS" link
    Then I verify the PLP page is displayed
    When I select item number(s): "<Items>" from the results list to compare
    And  I click the compare products Compare button
    And  I add item to my cart and "View shopping Cart"
    Then I verify product "<Item>" is "displayed" on the "cart" page

    Examples:
      | Product Type | Items               | Item  |
      | TIRE         | 11248, 31105, 25551 | 11248 |

  @dt
  @web
  @20309
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Wheel by Category and add to Cart from PLP(ALM #20309)
    When I open the "<Product Type>" navigation link
    And  I click the "Wheel Style" View All link in the header
    And  I click on the "continue without vehicle" link
    And  I select "multi-spoke" from the Product Brand page
    And  I click on the "SHOP ALL MULTI-SPOKE BRANDS" link
    Then I verify the PLP page is displayed
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"

    Examples:
      | Product Type | ItemCode |
      | WHEELS       | 52030    |

  @dt
  @web
  @20310
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire by Category and to Cart from PDP(ALM #20310)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "Continental" brand image
    And  I select "All-Season tires" from the Product Brand page
    Then I verify the PLP page is displayed
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page

    Examples:
      | Product Type | ItemCode |
      | TIRES        | 26236    |

  @dt
  @web
  @20311
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire by Size and add to Cart from Compare Page(ALM #20311)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY SIZE" menu option
    And  I do a "homepage" "tire" size search with details "<Width>, <Ratio>, <Diameter>"
    And  I click on the "continue without vehicle" link
    Then I verify the PLP page is displayed
    When I select item number(s): "<Items>" from the results list to compare
    And  I click the compare products Compare button
    And  I add item to my cart and "View shopping Cart"
    Then I verify product "<Item>" is "displayed" on the "cart" page

    Examples:
      | Product Type | Item  | Items               | Width | Ratio | Diameter |
      | TIRES        | 29935 | 29935, 12339, 19661 | 195   | 65    | 15       |

  @dt
  @web
  @20312
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Wheel by Size and to Cart from PLP(ALM #20312)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY SIZE" menu option
    And  I do a "homepage" "wheel" size search with details "<Diameter>"
    And  I click on the "continue without vehicle" link
    Then I verify the PLP page is displayed
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page

    Examples:
      | Product Type | ItemCode | Diameter |
      | WHEELS       | 71434    | 19       |

  @dt
  @web
  @20313
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire by Size and to Cart from (ALM #20313)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY SIZE" menu option
    And  I do a "homepage" "tire" size search with details "<Width>, <Ratio>, <Diameter>"
    And  I click on the "continue without vehicle" link
    And  I click on the product "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page

    Examples:
      | Product Type | ItemCode | Width | Ratio | Diameter | ProductName        |
      | TIRES        | 29935    | 195   | 65    | 15       | Silver Edition III |

  @dt
  @web
  @20314
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire by Global search and to Cart from Comparate Page(ALM #20314)
    When I do a free text search for "<Product Type>" and hit enter
    And  I click on the "continue without vehicle" link
    And  I select "All-Season tires" from the Product Brand page
    And  I click on the "SHOP ALL ALL-SEASON BRANDS" link
    Then I verify the PLP page is displayed
    When I select item number(s): "<Items>" from the results list to compare
    And  I click the compare products Compare button
    And  I add item to my cart and "View shopping Cart"
    Then I verify product "<Item>" is "displayed" on the "cart" page

    Examples:
      | Product Type | Items               | Item  |
      | Tire         | 11248, 31105, 25551 | 11248 |

  @dt
  @web
  @20315
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Wheel by Global search and to Cart from PLP(ALM #20315)
    When I do a free text search for "<Product Type>" and hit enter
    And  I click on the "continue without vehicle" link
    And  I select "multi-spoke" from the Product Brand page
    And  I click on the "SHOP ALL MULTI-SPOKE BRANDS" link
    Then I verify the PLP page is displayed
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page

    Examples:
      | Product Type | ItemCode |
      | Wheel        | 52030    |

  @dt
  @web
  @20316
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire by Global search and to Cart from PDP(ALM #20316)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    And  I add item to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page

    Examples:
      | ItemCode | ProductName |
      | 11248    | UHP         |

  @dt
  @web
  @20317
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire by Brand and to Cart from Compare Page and Select the x button to close modal(ALM #20317)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "<Brand>" brand image
    And  I select "<Type>" from the Product Brand page
    Then I verify the PLP page is displayed
    When I add item "<ItemCode>" of type "none" to my cart and "Close"
    Then I verify the message on the "PLP" banner contains "Enter your vehicle to ensure these tires fit"
    When I select mini cart
    And  I select View Cart on Mini Cart
    Then I should see product "<ItemCode>" on the "cart" page

    Examples:
      | Product Type | ItemCode | Type             | Brand       |
      | TIRES        | 26236    | All-Season tires | Continental |

  @dt
  @web
  @20318
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Wheel by Categoty and add to Cart from PLP and Enter Standard vehicle that fit(ALM #20318)
    When I open the "<Product Type>" navigation link
    And  I click the "Wheel Style" View All link in the header
    And  I click on the "continue without vehicle" link
    And  I select "multi-spoke" from the Product Brand page
    And  I select the "MAXXIM" brand image
    Then I verify the PLP page is displayed
    When I add item "<ItemCode>" of type "none" to my cart and "Verify Vehicle Fitment"
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify the "CheckFit" banner color is "Green"
    When I click on the "View shopping Cart" link
    Then I should see product "<ItemCode>" on the "cart" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Product Type | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | WHEELS       | 23500    |

  @dt
  @web
  @20319
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Tire by Size and to Cart from PDP and Enter Standard vehicle that does not fit(ALM #20319)
    When I open the "<Product Type>" navigation link
    And  I click the "<Product Type> BY SIZE" menu option
    And  I do a "homepage" "tire" size search with details "<Width>, <Ratio>, <Diameter>"
    And  I click on the "continue without vehicle" link
    And  I click on the product "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "Verify Vehicle Fitment"
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify the "CheckFit" banner color is "Red"

    Examples:
      | Product Type | ItemCode | Width | Ratio | Diameter | ProductName | Year | Make  | Model | Trim     | Assembly |
      | TIRES        | 41143    | 225   | 40    | 18       | P ZERO      | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @web
  @20320
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Add to Cart - No vehicle in session - Browse Wheel by Global search and to Cart from PLP and Enter Staggered vehicle that does not fit(ALM #20320)
    When I do a free text search for "<Product Type>" and hit enter
    And  I click on the "continue without vehicle" link
    And  I select "multi-spoke" from the Product Brand page
    And  I click on the "SHOP ALL MULTI-SPOKE BRANDS" link
    Then I verify the PLP page is displayed
    When I add item "<ItemCode>" of type "none" to my cart and "Verify Vehicle Fitment"
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify the "CheckFit" banner color is "Red"

    Examples:
      | Product Type | ItemCode | Year | Make      | Model    | Trim | Assembly |
      | Wheel        | 52030    | 2010 | Chevrolet | Corvette | Base | none     |

  @dt
  @web
  @20321
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - No vehicle associated - Empty cart(ALM #20321)
    When I navigate to the "<CartPath>" url
    Then I should see product has been "removed" in cart message

    Examples:
      | CartPath |
      | /cart    |

  @dtd
  @web
  @20322
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - No vehicle associated - Tire and Wheels - DTD(ALM #20322)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page
    When I click checkout now button
    Then I verify "Checkout" page is displayed

    Examples:
      | ItemCode |
      | 29935    |

  @dt
  @web
  @20323
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - No vehicle associated - Lawn & Garden, Trailer Tire, Golf Tire(ALM #20323)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page
    And  I click checkout now button
    Then I verify "Checkout" page is displayed

    Examples:
      | ItemCode |
      | 17546    |

  @dt
  @web
  @20324
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - No vehicle associated - Tire or Wheel(ALM #20324)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify product "<ItemCode>" is "displayed" on the "cart" page
    And  I click checkout now button
    Then I should see text "A valid vehicle is required to checkout." present in the page source
    And  I should see text "You have no recent vehicle searches" present in the page source

    Examples:
      | ItemCode |
      | 29935    |
      | 51585    |

  @dt
  @web
  @20325
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - No vehicle associated - Tire and Wheel(ALM #20325)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCode2>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I click checkout now button
    Then I should see text "A valid vehicle is required to checkout." present in the page source
    And  I should see text "You have no recent vehicle searches" present in the page source

    Examples:
      | ItemCode | ItemCode2 |
      | 29935    | 51585     |

  @dt
  @web
  @20326
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - No vehicle associated - Tire or Wheel - Select Standard vehicle that fit(ALM #20326)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I click checkout now button
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify "Checkout" page is displayed

    Examples:
      | ItemCode | Year | Make  | Model | Trim     | Assembly |
      | 29935    | 2012 | Honda | Civic | Coupe DX | none     |
      | 51585    | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @web
  @20327
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - Shopping Cart - No vehicle associated - Tire and Wheel - Select Standard vehicle that fit(ALM #20327)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCode2>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I click checkout now button
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify "Checkout" page is displayed

    Examples:
      | ItemCode | ItemCode2 | Year | Make  | Model | Trim     | Assembly |
      | 29935    | 51585     | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @web
  @20328
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - No vehicle associated - Tire or Wheel - Select Standard vehicle that does not fit all products in cart(ALM #20328)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I click checkout now button
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see text "Only one vehicle allowed to checkout." present in the page source
    And  I should see text "Call your store to order these products if you know they'll fit your vehicle" present in the page source
    And  I verify the "CheckFit" banner color is "Red"

    Examples:
      | ItemCode | Year | Make  | Model | Trim     | Assembly |
      | 14203    | 2012 | Honda | Civic | Coupe DX | none     |
      | 16120    | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @web
  @20329
  @checkFit
  @staggeredExperience
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - Shopping Cart - No vehicle associated - Tire and Wheel - Select Standard vehicle that does not fit(ALM #20329)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCode2>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I click checkout now button
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see text "Only one vehicle allowed to checkout." present in the page source
    And  I should see text "Call your store to order these products if you know they'll fit your vehicle" present in the page source
    And  I verify the "CheckFit" banner color is "Red"

    Examples:
      | ItemCode | ItemCode2 | Year | Make  | Model | Trim     | Assembly |
      | 14203    | 16120     | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @web
  @checkFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Shopping Cart - No vehicle associated - Tire/wheel - Select Staggered vehicle that fit
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I click checkout now button
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify "Checkout" page is displayed

    Examples:
      | ItemCode | Year | Make      | Model    | Trim | Assembly |
      | 33537    | 2010 | Chevrolet | Corvette | Base | none     |
      | 55875    | 2010 | Chevrolet | Corvette | Base | none     |

  @at
  @dt
  @dtd
  @web
  @19618
  @mobile
  @regression
  @solrSearch
  @searchRegression
  Scenario Outline: HYBRIS_SEARCH_SEARCH_FREE TEXT SEARCH With misspellings  and common search terms directing the user the right page(ALM #19618)
    When I do a free text search for "<Search Term>" and hit enter
    Then I verify the PLP header message contains "Results for "<Search Term>""
    And  I verify the "<Breadcrumb>" link in the breadcrumb container
    And  I verify the order of filter categories for "<Product Type>"
    And  I verify "<Non-Expected Filter>" does not display in the filters
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I do a free text search for "<Search Term>" and hit enter
    Then I verify the PLP header message contains "Results for "<Search Term>""
    And  I verify the "<Breadcrumb>" link in the breadcrumb container
    And  I verify the order of filter categories for "<Product Type>"
    And  I verify "<Non-Expected Filter>" does not display in the filters

    Examples:
      | Search Term | Breadcrumb                    | Product Type | Non-Expected Filter | Year | Make  | Model | Trim     | Assembly |
      | Weels       | Home,  search for "weels"     | Wheels       | Tires               | 2012 | Honda | Civic | Coupe DX | none     |
      | Tired       | Home,  search for "tired"     | Tires        | Wheels              | 2012 | Honda | Civic | Coupe DX | none     |
      | Michellin   | Home,  search for "Michellin" | Tires        | Wheels              | 2012 | Honda | Civic | Coupe DX | none     |
      | Snow        | Home,  search for "Snow"      | Tires        | Wheels              | 2012 | Honda | Civic | Coupe DX | none     |
      | Rim         | Home,  search for "Rim"       | Wheels       | Tires               | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @at
  @web
  @viewOnMyVehicleScenario
  Scenario Outline: View on my vehicle on PLP and PDP on DT
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "wheel"
    And  I click on the "View on my vehicle" link
    And  I should see vehicle "<Year> <Make>" on the Wheel Configurator modal window
    And  I click on the "Filter" button
    And  I select from the "Wheel Category" filter section, "single" option(s): "<Filter 1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I select from the "Wheel Color" filter section, "single" option(s): "<Filter 2>"
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I click on the "View on my vehicle" link
    And  I click on the "Filter" button
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    And  I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I reset the minimum and maximum price range
    And  I select the "First" product result image on "PLP" page
    And  I click on the "View on my vehicle" link
    And  I should see vehicle "<Year> <Make>" on the Wheel Configurator modal window
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BopisCustomer>"
    And  I place the order for "<BopisCustomer>"
    Then I am brought to the order confirmation page
    When I go to the homepage
    And  I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "wheel"
    And  I click on the "View on my vehicle" link
    And  I should see vehicle "<Year> <Make>" on the Wheel Configurator modal window
    And  I click on the "Filter" button
    And  I select from the "Wheel Category" filter section, "single" option(s): "<Filter 1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I select from the "Wheel Color" filter section, "single" option(s): "<Filter 2>"
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I click on the "View on my vehicle" link
    And  I click on the "Filter" button
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    And  I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    And  I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I reset the minimum and maximum price range
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click next step for customer information
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BopisCustomer>"
    And  I place the order for "<BopisCustomer>"
    Then I am brought to the order confirmation page
    When I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I open the "WHEELS" navigation link
    And  I click the "Drag Wheels" menu option
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    And  I click on the "SHOP ALL DRAG WHEELS" link
    And  I click on the "View on my vehicle" link
    And  I should see vehicle "<Year> <Make>" on the Wheel Configurator modal window
    And  I click on the "Filter" button
    And  I select from the "Wheel Category" filter section, "single" option(s): "<Filter 1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I select from the "Wheel Color" filter section, "single" option(s): "<Filter 2>"
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I click on the "View on my vehicle" link
    And  I click on the "Filter" button
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    And  I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I reset the minimum and maximum price range
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BopisCustomer>"
    And  I place the order for "<BopisCustomer>"
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Filter 1 | Filter 2 | Customer            | BopisCustomer               | Credit Card | Email                | password  | Minimum Price | Maximum Price | Year1 | Make1 | Model1 | Trim1 |
      | 2012 | Honda | Civic | Coupe DX | none     | Painted  | Black    | DEFAULT_CUSTOMER_AZ | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | autouser_b@gmail.com | Discount1 | 75            | 95            | 2014  | Acura | ILX    | 2.0L  |

  @dtd
  @web
  @viewOnMyVehicleScenario
  Scenario Outline: View on my vehicle on PLP and PDP on DTD
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "wheel"
    And  I click on the "View on my vehicle" link
    Then I should see vehicle "<Year> <Make>" on the Wheel Configurator modal window
    When I click on the "Filter" button
    And  I select from the "Wheel Category" filter section, "single" option(s): "<Filter 1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I select from the "Wheel Color" filter section, "single" option(s): "<Filter 2>"
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I click on the "View on my vehicle" link
    And  I click on the "Filter" button
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    And  I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I reset the minimum and maximum price range
    And  I select the "First" product result image on "PLP" page
    And  I click on the "View on my vehicle" link
    And  I should see vehicle "<Year> <Make>" on the Wheel Configurator modal window
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I populate all the required fields for "<Customer>"
    And  I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BopisCustomer>"
    And  I place the order for "<BopisCustomer>"
    Then I am brought to the order confirmation page
    When I go to the homepage
    And  I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I click on the "SHOP ALL MB WHEELS" link
    And  I click on the "View on my vehicle" link
    And  I should see vehicle "<Year> <Make>" on the Wheel Configurator modal window
    And  I click on the "Filter" button
    And  I select from the "Wheel Category" filter section, "single" option(s): "<Filter 1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    When I select from the "Wheel Color" filter section, "single" option(s): "<Filter 2>"
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I select the "Price (Low to High)" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I click on the "View on my vehicle" link
    And  I click on the "Filter" button
    Then I verify that the sort by dropdown value is set to "Price (Low to High)"
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    And  I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I click on the "Apply Filters" link
    And  I close view on my vehicle modal
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter 1>,<Filter 2>"
    When I reset the minimum and maximum price range
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I populate all the required fields for "<Customer>"
    And  I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BopisCustomer>"
    And  I place the order for "<BopisCustomer>"
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Filter 1 | Filter 2 | Customer            | BopisCustomer               | Credit Card |
      | 2012 | Honda | Civic | Coupe DX | none     | Painted  | Black    | DEFAULT_CUSTOMER_AZ | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @dt
  @web
  @21255
  @coreScenarioCheckFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - PLP / PDP / Compare Products - Tire fits (ALM#21255)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"
    And  I verify the message on the "CheckFit" banner contains "VERIFIEDThis product fits your:"
    And  I verify Vehicle Details Banner on "CheckFit" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I close popup modal
    And  I click on the first available product result on PLP page
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"
    And  I verify the message on the "CheckFit" banner contains "VERIFIEDThis product fits your:"
    And  I verify Vehicle Details Banner on "CheckFit" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I close popup modal
    And  I navigate back to previous page
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"
    And  I verify the message on the "CheckFit" banner contains "VERIFIEDThis product fits your:"
    And  I verify Vehicle Details Banner on "CheckFit" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | FitmentOption |
      | 2012 | Honda     | Civic    | Coupe DX | none     | tire          |
      | 2010 | Chevrolet | Corvette | Base     | none     | tire          |

  @dt
  @web
  @21256
  @coreScenarioCheckFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Vehicle in session - PLP / PDP / Compare Products - Wheel fits (ALM#21256)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"
    And  I verify the message on the "CheckFit" banner contains "VERIFIEDThis product fits your:"
    And  I verify Vehicle Details Banner on "CheckFit" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I close popup modal
    And  I click on the first available product result on PLP page
    And  I select "Add To Cart"
    Then I verify the "CheckFit" banner color is "Green"
    And  I verify the message on the "CheckFit" banner contains "VERIFIEDThis product fits your:"
    And  I verify Vehicle Details Banner on "CheckFit" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | FitmentOption |
      | 2012 | Honda     | Civic    | Coupe DX | none     | wheel         |
      | 2010 | Chevrolet | Corvette | Base     | none     | wheel         |

  @dt
  @at
  @web
  @20595
  @staggeredExperience
  Scenario Outline: HYBRIS_UI_CONTENT_Staggered Experience_HYBRIS_UI_CONTENT_PLP Sets tab Filters -  Wheel Sets duallys (ALM#20595)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    Then I verify that the "Sub Header" element is not displayed
    And  I verify that the "treadwell" element is not displayed
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify that the "Tabs" element is not displayed
    When I select edit vehicle link
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify "SETS" staggered option tab is displayed on PLP result page
    And  I verify "FRONT" staggered option tab is displayed on PLP result page
    And  I verify "REAR" staggered option tab is displayed on PLP result page
    And  I verify the "Quick Filters" filter section(s) is/are displayed

    Examples:
      | Year | Make | Model         | Trim | Assembly |
      | 2003 | GMC  | Yukon XL 2500 | 4WD  | None     |

  @dt
  @at
  @web
  @20589
  @staggeredExperience
  @coreScenarioStaggeredExperience
  Scenario Outline: HYBRIS_UI_CONTENT_Staggered Experience_HYBRIS_UI_CONTENT_Staggered Experience_Optional Size Sub-tabs Messaging (ALM#20589)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    Then I verify that the optional size message for "OE size" is displayed
    When I select the "<SizeOption1>" fitment box option
    Then I verify that the optional size message for "optional size" is displayed
    When I select a fitment option "<TireSize>"
    And  I "expand" "optional tire and wheel size"
    And  I select revert to O.E. size
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    When I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify that the sort by dropdown value is set to "Best Match"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify that the sort by dropdown value is set to "Best Seller"
    When I select "REAR" staggered tab on PLP result page
    Then I verify that the sort by dropdown value is set to "Best Seller"
    When I select "SETS" staggered tab on PLP result page
    And  I click on the first available product result on PLP page
    Then I verify "Staggered" Vehicle Details Banner on "PDP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    When I open the My Vehicles popup
    And  I click on the "Shop Products" link
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption1>" fitment box option
    Then I verify that the optional size message for "optional size" is displayed
    When I select a fitment option "<TireSize>"
    And  I "expand" "optional tire and wheel size"
    And  I select revert to O.E. size
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify that the sort by dropdown value is set to "Best Seller"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify that the sort by dropdown value is set to "Best Seller"
    When I select "REAR" staggered tab on PLP result page
    Then I verify that the sort by dropdown value is set to "Best Seller"
    When I select the "Highest Rated" from the Sort By dropdown box
    And  I select "SETS" staggered tab on PLP result page
    Then I verify that the sort by dropdown value is set to "Highest Rated"
    When I click on the first available product result on PLP page
    Then I verify "Staggered" Vehicle Details Banner on "PDP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make | Model | Trim  | Assembly       | SizeOption | SizeOption1 | TireSize  | OELabel | OETireSize |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL | 19”/19”    | 20”/20”     | 245/35-20 | O.E.    | 245/40-R19 |

  @dt
  @at
  @dtd
  @20556
  @20557
  @20558
  @staggeredExperience
  @coreScenarioStaggeredExperience
  Scenario Outline: HYBRIS_UI_CONTENT_Staggered Experience_HYBRIS_UI_CONTENT_Staggered Experience_FVRM Size Selections Persist to PLP_TIRES (ALM#20556, 20557, 20558)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption1>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "<OptionalLabel>" and "<OETireSize>"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify "FRONT" wheel diameter matches with the size of each product on the results page
    When I select "REAR" staggered tab on PLP result page
    Then I verify "REAR" wheel diameter matches with the size of each product on the results page
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "<OptionalLabel>" and "<OETireSize>"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify "FRONT" wheel diameter matches with the size of each product on the results page
    When I select "REAR" staggered tab on PLP result page
    Then I verify "REAR" wheel diameter matches with the size of each product on the results page
    When I go to the homepage
    And  I click on the "<Year> <Make>" button
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE Wheel" fitment banner with "<OptionalLabel>" and "<OETireSize>"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify "FRONT" wheel diameter matches with the size of each product on the results page
    When I select "REAR" staggered tab on PLP result page
    Then I verify "REAR" wheel diameter matches with the size of each product on the results page
    When I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY SIZE" menu option
    And  I click on the "<Width1>" button
    And  I click on the "<Ratio1>" button
    And  I click on the "<Diameter1>" button
    And  I click on the "+ add a rear tire" link
    And  I click on the "<Width2>" button
    And  I click on the "<Ratio2>" button
    And  I click on the "<Diameter2>" button
    And  I click on the "View Tires" button
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    Then I verify the staggered size line displayed the selected size

    Examples:
      | Year | Make | Model | Trim  | Assembly       | SizeOption1 | TireSize  | OptionalLabel | OETireSize | Width1 | Ratio1 | Diameter1 | Width2 | Ratio2 | Diameter2 |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL | 20”/20”     | 245/35-20 | +1’’          | 245/35-R20 | 245    | 40     | 19        | 275    | 35     | 19        |

  @dt
  @at
  @web
  @core
  @20815
  @checkfit
  @coreScenarioCheckFit
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Deletion of Vehicle in session (ALM#20815)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    And  I click on the "continue without vehicle" link
    And  I select the "MB WHEELS" brand image
    And  I click on the "shop MB WHEELS Chrome wheels" link
    And  I click on the "ADD TO CART" button
    Then I should see text "A valid vehicle is required to checkout." present in the page source
    When I click on the "View shopping Cart" link
    And  I click checkout now button
    Then I should see text "A valid vehicle is required to checkout." present in the page source

    Examples:
      | Year | Make | Model | Trim  | Assembly       |
      | 2014 | BMW  | 640i  | Coupe | 245 /40 R19 SL |

  @dt
  @at
  @web
  @20886
    Scenario Outline: HYBRIS_SEARCH_Enhancements_ OEEE-407 PLP_Check nearby store default sort(ALM#20886)
    When I change to the store with url "<StoreUrl>"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    And  I select "All-Season tires" from the Product Brand page
    And  I select the "Check nearby stores" link for item "<ItemCode>" on "PLP" page
    Then I verify that the check availability modal has default sort by option set to "Availability" and verify if the "stock count" is in "descending" order
    When I close popup modal
    And  I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "Check nearby stores" link for item "<ItemCode>" on "PLP" page
    Then I verify that the check availability modal has default sort by option set to "Availability" and verify if the "stock count" is in "descending" order
    When I add item to my cart and "View shopping Cart"
    And  I select the "Check nearby stores" link for item "<ItemCode>" on "Shopping Cart" page
    Then I verify that the check availability modal has default sort by option set to "Availability" and verify if the "stock count" is in "descending" order

    Examples:
      | ItemCode | StoreUrl                 | Year | Make  | Model | Trim     | Assembly |
      | 34302    | /store/az/phoenix/s/1358 | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @at
  @dtd
  @web
  @20897
  Scenario Outline: HYBRIS_SEARCH_Enhancements_ OEEE-211- Add and Edit a vehicle on DCP (ALM#20897)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click on "View all" menu link
    And  I click on the "Enter vehicle" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I select edit icon on "brand page"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "<Tire Link>" menu option
    And  I click on the "Enter vehicle" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires don't fit your vehicle"
    When I select edit icon on "brand page"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption1>" fitment box option
    And  I select a fitment option "<TireSize1>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  | Tire Link    | SizeOption1 | TireSize1 |
      | 2012 | Honda | Civic | Coupe DX | none     | 15”        | 205/60-15 | Winter Tires | 18”         | 225/40-18 |

  @dt
  @at
  @web
  @20900
  Scenario Outline: HYBRIS_SEARCH_Enhancements_ OEEE-211- Add and Edit a vehicle on PLP(ALM#20900)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click on "View all" menu link
    And  I click on the "CONTINUE WITHOUT VEHICLE" link
    And  I select the "MICHELIN TIRES" brand image
    And  I click on the "SHOP ALL" link
    And  I click on the "Enter vehicle" link
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I select edit icon on "PLP"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "<Tire Link>" menu option
    And  I click on the "CONTINUE WITHOUT VEHICLE" link
    And  I click on the "SHOP ALL" link
    And  I click on the "Enter vehicle" link
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires don't fit your vehicle"
    When I select edit icon on "PLP"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption1>" fitment box option
    And  I select a fitment option "<TireSize1>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  | Tire Link    | SizeOption1 | TireSize1 |
      | 2012 | Honda | Civic | Coupe DX | none     | 15”        | 185/65-15 | Winter Tires | 18”         | 225/40-18 |

  @dtd
  @web
  @20900
  Scenario Outline: HYBRIS_SEARCH_Enhancements_ OEEE-211- Add and Edit a vehicle on PLP_dtd(ALM#20900)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click on "View all" menu link
    And  I select the "MICHELIN TIRES" brand image
    And  I click on the "SHOP ALL" link
    And  I click on the "Enter vehicle" link
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I select edit icon on "PLP"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "<Tire Link>" menu option
    And  I click on the "SHOP ALL" link
    And  I click on the "Enter vehicle" link
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires don't fit your vehicle"
    When I select edit icon on "PLP"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption1>" fitment box option
    And  I select a fitment option "<TireSize1>"
    And  I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  | Tire Link    | SizeOption1 | TireSize1 |
      | 2012 | Honda | Civic | Coupe DX | none     | 15”        | 185/65-15 | Winter Tires | 18”         | 225/40-18 |

  @dt
  @at
  @web
  @20901
  Scenario Outline: HYBRIS_SEARCH_Enhancements_ OEEE-211- Add and Edit a vehicle on PDP(ALM#20901)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click on "View all" menu link
    And  I click on the "CONTINUE WITHOUT VEHICLE" link
    And  I select the "Bridgestone" brand image
    And  I click on the "SHOP ALL" link
    And  I select "View Details" for product "<Product Name>" on the PLP page
    And  I click on the "Enter vehicle" link
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    When I select edit icon on "PDP"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I click on the "Continue" button
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "<Tire Link>" menu option
    And  I click on the "CONTINUE WITHOUT VEHICLE" link
    And  I click on the "SHOP ALL" link
    And  I select "View Details" for product "<Product Name1>" on the PLP page
    And  I click on the "Enter vehicle" link
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PDP" banner contains "These tires don't fit your vehicle"
    When I select edit icon on "PDP"
    And  I click on the "Year" button
    And  I do a "default" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I click on the "Continue" button
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | SizeOption | TireSize  | Tire Link        | SizeOption1 | TireSize1 | Product Name   | Product Name1 | Year1 | Make1 | Model1 | Trim1    | Assembly1 |
      | 2010 | Chevrolet | Corvette | Base | None     | 18”        | 245/40-18 | All-Season tires | 16”         | 225/50-16 | POTENZA RE050A | CP662         | 2012  | Honda | Civic  | Coupe DX | none      |

  @dtd
  @web
  @20901
  Scenario Outline: HYBRIS_SEARCH_Enhancements_ OEEE-211- Add and Edit a vehicle on PLP_dtd(ALM#20901)
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click on "View all" menu link
    And  I select the "Bridgestone" brand image
    And  I click on the "SHOP ALL" link
    And  I select "View Details" for product "<Product Name>" on the PLP page
    And  I click on the "Enter vehicle" link
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    When I select edit icon on "PDP"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I click on the "Continue" button
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "<Tire Link>" menu option
    And  I click on the "SHOP ALL" link
    And  I select "View Details" for product "<Product Name1>" on the PLP page
    And  I click on the "Enter vehicle" link
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click on the "Continue" button
    Then I verify the message on the "PDP" banner contains "These tires don't fit your vehicle"
    When I select edit icon on "PDP"
    And  I click on the "Year" button
    And  I do a "default" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I click on the "Continue" button
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | SizeOption | TireSize  | Tire Link        | SizeOption1 | TireSize1 | Product Name   | Product Name1 | Year1 | Make1 | Model1 | Trim1    | Assembly1 |
      | 2010 | Chevrolet | Corvette | Base | None     | 18”        | 245/40-18 | All-Season tires | 16”         | 225/50-16 | POTENZA RE050A | CP662         | 2012  | Honda | Civic  | Coupe DX | none      |

  @dt
  @at
  @web
  @dtd
  @20684
  @suggestedSelling
  Scenario Outline: HYBRIS_SEARCH_SEARCH_SUGGESTED SELLING_Homepage_Verify SS product Carousel for standard vehicle_guest user_First visit (ALM#20684)
    When I go to the Geoip store homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click the discount tire logo
    Then I am brought to the homepage
    And  I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that tooltip icon is present in the carousel for tire product

    Examples:
      | Year | Make  | Model | Trim     | Assembly | title             |
      | 2012 | Honda | Civic | Coupe DX | none     | TRENDING PRODUCTS |

  @dt
  @at
  @dtd
  @web
  @20740
  @suggestedSelling
  Scenario Outline: HYBRIS_SEARCH_SEARCH_SUGGESTED SELLING_Homepage_Verify SS product Carousel for standard vehicle_Guest User_No suggestion for previously removed products (ALM#20740)
    When I go to the Geoip store homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click the discount tire logo
    Then I am brought to the homepage
    And  I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    When I select the "2nd" product result image on "Homepage" page
    Then I verify the "PDP" banner color is "Green"
    When I add item to my cart and "View shopping Cart"
    And  I extract "ProductName" from shopping cart as "product name"
    And  I select mini cart and "Clear Cart"
    And  I click the discount tire logo
    Then I am brought to the homepage
    And  I verify the product in cart is not displayed in the "Homepage" carousel

    Examples:
      | Year | Make   | Model | Trim     | Assembly | title             |
      | 2012 | Honda  | Civic | Coupe DX | none     | TRENDING PRODUCTS |

  @dt
  @at
  @web
  @20772
  @suggestedSelling
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on Items Added to Cart modal when go through Wheel iConfigurator flow(ALM#20772)
    """TODO: @dtd tag should be added once the new code is pulled in to Devfr10 for DTD"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I extract the product at position "1" from "PLP"
    And  I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    Then I should see vehicle "<Year> <Make>" on the Wheel Configurator modal window
    When I choose a wheel on Wheel Configurator modal window
    And  I click on the "Add to Cart" button
    Then I should see Cart Popup has "Item added to cart" option displayed
    And  I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "Add to cart" page
    And  I verify the product is displayed at position "1" in the 'suggested selling carousel'

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Title                    |
      | 2012 | Honda | Civic | Coupe DX | none     | CUSTOMERS ALSO PURCHASED |

  @dt
  @at
  @dtd
  @web
  @20754
  @suggestedSelling
  Scenario Outline: HYBRIS_SEARCH_SEARCH_SUGGESTED SELLING_Homepage_Verify SS product Carousel for standard vehicle_Guest User_Suggestions should match based on size (ALM#20754)
    When I go to the Geoip store homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I extract the "Tire" size on the home page
    And  I extract the "Wheel" size on the home page
    Then I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    When I select the "First" product result image on "Homepage" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify extracted "Tire" size with size on PDP page
    When I navigate back to previous page
    Then I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title2> <Year> <Make> <Model>" with no more than five products and no duplicate products in the carousel
    When I select the "2nd" product result image on "Homepage" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify extracted "Tire" size with size on PDP page
    When I navigate back to previous page
    Then I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title2> <Year> <Make> <Model>" with no more than five products and no duplicate products in the carousel
    When I select the "3nd" product result image on "Homepage" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify extracted "Wheel" size with size on PDP page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | title             | title2   |
      | 2012 | Honda | Civic | Coupe DX | none     | TRENDING PRODUCTS | FOR YOUR |

  @at
  @dt
  @web
  @21259
  @coreScenarioStaggeredExperience
  Scenario Outline: Staggered Experience core test scenario - PLP, Treadwell (ALM#21259)
    """ Will fail due to OEES-855. Top 3 not loading for Staggered. Commenting out those steps until issue is resolved.
    Will fail if tire sets or wheel sets are not loading on PLP. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "20" fitment box option
    And  I select a fitment option "235/40-19"
    Then I verify "Staggered" Vehicle Details on "My Vehicles" page contains "Non-OE FRONT Wheel" fitment banner with "+1’’" and "19"
    When I select a fitment option "tire"
    Then I should see the fitment panel page with fitment options
    When I "expand" "optional tire and wheel size"
    And  I select the "19" fitment box option
    And  I select a fitment option "O.E"
    Then I should see the fitment panel page with fitment options
    When I click on the "SHOP ALL TIRES" button
    Then I verify the PLP header message contains "results"
    And  I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify that the sort by dropdown value is set to "Best Seller"
    And  I verify PLP UI "sorting options"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OEREarWheelSize>"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OEREarWheelSize>"
    When I select "REAR" staggered tab on PLP result page
    Then I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OEREarWheelSize>"
    When I select "SETS" staggered tab on PLP result page
    And  I select the first "2" results to compare
    Then I verify the Add To Cart button is clickable and Red on "PLP" page
    When I enter "0" into the first item quantity text box
    Then I see the Please Enter a Number error message
    And  I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "PLP" page
    When I select from the "Good better best" filter section, "single" option(s): "Best"
    Then I verify that the search refinement filters contain the "single" value(s): "Best"
    When I select "FRONT" staggered tab on PLP result page
    Then I verify that the search refinement filters contain the "single" value(s): "Best"
    When I select "REAR" staggered tab on PLP result page
    Then I verify that the search refinement filters contain the "single" value(s): "Best"
    When I select "SETS" staggered tab on PLP result page
    And  I clear all the currently active filters on the PLP page
    Then I verify no search refinement filters are being applied
    When I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    Then I verify the PLP header message contains "results"
    When I clear all the currently active filters on the PLP page
    Then I verify no search refinement filters are being applied
#    And  I verify the PLP displays 'Top 3 Tiles' below the PLP header
#    And  I verify the PLP displays "Original Equipment","Our Recommendation","Best Seller" in the 'Top 3 Tiles'
#    When I extract the product at position "1" from the "PLP"
#    Then I verify the product is displayed at position "3" in the 'Top 3 Tiles'
#    When I select the "Original Equipment" checkbox
#    Then I verify Original Equipment tire is displayed on "PLP" page
#    When I extract the product at position "1" from the "PLP"
#    Then I verify the product is displayed at position "1" in the 'Top 3 Tiles'
#    And  I verify "<Brand>" is displayed at position "2" in the 'Top 3 Tiles'
#    When I select the "First" product result image on "PLP" page
#    Then I verify the "PDP" banner color is "Green"
#    When I navigate back to previous page
    When I select "FRONT" staggered tab on PLP result page
    And  I add item to my cart and "Continue Shopping"
    And  I select "REAR" staggered tab on PLP result page
    And  I add item to my cart and "View shopping Cart"
    And  I click checkout now button
    And  I click on the "Continue to Checkout" button
    Then I verify "Checkout" page is displayed
    When I click the discount tire logo
    And  I select mini cart and "Clear Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I select view recommended tires
    Then I verify driving priority order on "PLP"
    And  I verify the "Treadwell" checkbox to be "selected" by default
    And  I verify that the treadwell filter is "present" in the first position in quick filters
    And  I verify that the search refinement filters contain the "single" value(s): "Treadwell Tested"
    And  I verify that the sort by dropdown value is set to "Recommended"
    And  I verify the results list is sorted in ascending order by treadwell ranking on PLP
    And  I verify the "Quick Filters, Brands, Price Range, Ratings, Speed Rating, Good Better Best" filter section(s) is/are displayed
    And  I verify the PLP header message contains "results"
    When I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify treadwell logo is displayed with title and subtitle on "Compare Products"
    When I select the "First" product result image on "Compare Products" page
    Then I verify treadwell logo is displayed with title and subtitle on "PDP"
    When I click the discount tire logo
    And  I open the My Vehicles popup
    And  I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I click on the "cancel" link
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the selected treadwell fitment tire size value "<Tire Size>" is displayed
    And  I verify the treadwell location display zipcode "<ZipCode>" on "treadwell modal"
    When I set "Miles Driven Per Year" to "<Miles>"
    Then I verify the miles driven per year has a value of "<Miles>" on treadwell customer preference model
    When I select view recommended tires
    Then I verify driving priority order on "PLP"
    And  I verify "Everyday" default pdl driving priority order on "PLP"
    And  I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify treadwell details section "present"
    And  I verify the treadwell location display zipcode "<ZipCode>" on "PLP"
    And  I should see "<Miles>" as my selected miles per year value on treadwell driving details section on "PLP"
    When I select edit treadwell driving details on plp page
    And  I select "Performance" as my driving priority
    And  I select view recommended tires
    Then I verify driving priority order on "PLP"
    And  I verify "Performance" default pdl driving priority order on "PLP"
    And  I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify treadwell details section "present"
    When I select edit treadwell driving details on plp page
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    And  I verify the miles driven per year has a value of "<Miles>" on treadwell customer preference model
    When I set "Primary Driving Location" to "<NewZipCode>"
    And  I set "Miles Driven Per Year" to "<NewMiles>"
    And  I move "Handling" driving priority to position "3"
    And  I select view recommended tires
    Then I verify driving priority order on "PLP"
    And  I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify treadwell details section "present"
    And  I verify the treadwell location display zipcode "<NewZipCode>" on "PLP"
    And  I should see "<NewMiles>" as my selected miles per year value on treadwell driving details section on "PLP"
    When I select the "1st" product result image on PLP page with "Add to Cart" button
    Then I should see "<NewMiles>" as my selected miles per year value on treadwell driving details section on "PDP"
    When I navigate back to previous page
    And  I select edit treadwell driving details on plp page
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I set "Primary Driving Location" to "<InvalidZipCode>"
    Then I should verify that error message is displayed for "invalid" "zipcode"
    And  I verify the edit option is displayed
    And  I verify that the "view recommended tires" button is disabled
    And  I verify that the "view recommended tires" button color is "grey"
    When I set "Primary Driving Location" to " "
    Then I verify that the "view recommended tires" button is disabled
    And  I verify that the "view recommended tires" button color is "grey"
    When I set "Primary Driving Location" to "<ZipCode>"
    And  I set "Miles Driven Per Year" to " "
    Then I should verify that error message is displayed for "invalid" "Mile Driven Per Year"
    And  I verify that the "view recommended tires" button is disabled
    And  I verify that the "view recommended tires" button color is "grey"
    And  I set "Miles Driven Per Year" to "4"
    Then I should verify that error message is displayed for "invalid" "Mile Driven Per Year"
    And  I verify that the "view recommended tires" button is disabled
    And  I verify that the "view recommended tires" button color is "grey"
    And  I set "Miles Driven Per Year" to "56"
    Then I should verify that error message is displayed for "invalid" "Mile Driven Per Year"
    And  I verify that the "view recommended tires" button is disabled
    And  I verify that the "view recommended tires" button color is "grey"
    And  I close popup modal
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim2>" "<Assembly>"
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the "Brands, Price Range, Wheel Category, Wheel Color, Wheel Width" filter section(s) is/are displayed
    When I select "FRONT" staggered tab on PLP result page
    And  I add item to my cart and "Continue Shopping"
    And  I select "REAR" staggered tab on PLP result page
    And  I add item to my cart and "View shopping Cart"
    And  I click checkout now button

    Examples:
      | Year | Make      | Model    | Trim | Trim2      | Assembly | Tire Size  | Miles | NewMiles | ZipCode | NewZipCode | InvalidZipCode | OELabel | OEFrontTireSize | OEFrontWheelSize | OERearTireSize | OEREarWheelSize |
      | 2010 | Chevrolet | Corvette | Base | Gran Sport | none     | 245/40-R18 | 25    | 45       | 86001   | 85260      | 00000          | O.E.    | 245/40/R18      | 18               | 285/35/R19     | 19              |

  @at
  @dt
  @web
  @21257
  @coreScenarioStaggeredExperience
  Scenario Outline: Staggered Experience core test scenario - Wheel Configurator (ALM#21257)
    When I go to the homepage
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    Then I should see "subtitle" "Please note that not all vehicles are represented in the Wheel Browser" on the Wheel Configurator modal window displayed
    And  I should see the "X" on the "Wheel Configurator modal window" page
    When I do a vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>" on Wheel Configurator modal window
    Then I should see the default "Wheel Color" in the Wheel Configurator modal window
    And  I should see vehicle "<Year> <Make> <Model>" on the Wheel Configurator modal window
    And  I should see the "Vehicle Image" on the "Wheel Configurator modal window" page
    And  I should see the wheel results displayed on the Wheel Configurator modal window
    When I choose 'Change Vehicle' option on the Wheel Configurator modal window
    And  I do a vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>" on Wheel Configurator modal window
    Then I should see vehicle "<Year1> <Make1> <Model1>" on the Wheel Configurator modal window
    When I select "X" option on "Wheel Configurator modal window" page
    Then I verify the site logo
    When I click on the "USE WHEEL VISUALIZER" button
    Then I should see vehicle "<Year1> <Make1> <Model1>" on the Wheel Configurator modal window
    When I select "X" option on "Wheel Configurator modal window" page
    Then I verify the site logo
    When I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "Results"
    When I click the discount tire logo
    And  I click on the "USE WHEEL VISUALIZER" button
    Then I should see vehicle "<Year1> <Make1> <Model1>" on the Wheel Configurator modal window
    And  I should see the default "Wheel Size" in the Wheel Configurator modal window
    When I select "X" option on "Wheel Configurator modal window" page
    Then I verify the site logo
    When I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I do a vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>" on Wheel Configurator modal window
    Then I should see the wheel has Image Name Brand Price displayed
    And  I verify the wheel count on the header should match with the displayed wheel results
    When I choose a wheel on Wheel Configurator modal window
    And  I select "View Details" option on "Wheel Configurator modal window" page
    Then I verify quantity value "4" on PDP page
    And  I should see "<Year1> <Make1> <Model1> <Trim1>" details on the "PDP" page
    When I navigate back to previous page
    Then I should see previously selected wheel should be on focus
    When I go to the homepage
    And  I click on the "USE WHEEL VISUALIZER" button
    And  I choose a wheel on Wheel Configurator modal window
    And  I click on the "Add to Cart" button
    Then I should see Cart Popup has "View shopping Cart" option displayed
    And  I should see Cart Popup has "Continue Shopping" option displayed
    And  I should see Cart Popup has "Item added to cart" option displayed
    And  I should see Cart Popup has "You can choose product add-ons and schedule installation during check out" option displayed
    When I click on the "Continue Shopping" link
    Then I verify the header cart item count is "1"
    And I should see vehicle "<Year1> <Make1> <Model1>" on the Wheel Configurator modal window
    When I choose a wheel on Wheel Configurator modal window
    And  I click on the "Add to Cart" button
    And  I click on the "View shopping Cart" link
    Then I am brought to the page with header "Shopping cart"
    And  I verify the header cart item count is "1"
    And  I should see the cart quantity is set to "8"
    When I update the quantity in "Cart" with "12"
    Then I should see the cart quantity is set to "12"
    When I go to the homepage
    And  I select mini cart and "Clear Cart"
    Then I verify the header cart item count is "0"
    When I click on the "USE WHEEL VISUALIZER" button
    And  I choose a wheel on Wheel Configurator modal window
    And  I select "share" option on wheel configurator window
    Then I should see the Social Site "Email" option displayed
    And  I should see the Social Site "Facebook" option displayed
    And  I should see the Social Site "Twitter" option displayed
    And  I should see the Social Site "Google Plus" option displayed
    And  I should see the Social Site "Mobile" option displayed
    And  I should see the Social Site "pinterest" option displayed
    When I enter email address "Test@discounttire.com" on Wheel Configurator page
    Then I verify the "SEND" button is "enabled"
    When I enter phone number "9206981972" on Wheel Configurator page
    Then I verify the "SEND" button is "enabled"
    When I set fitment type to "<FitmentType>"
    And  I choose FILTER option
    Then I should see the wheel filter options on Wheel Configurator modal window
    And  I verify that the sort by dropdown value is set to "<SortValueDefault>"
    When I select the "<SortByOption1>" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "<SortByOption1>"
    When I enter "<WheelBrand1>" into the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "text": "<WheelBrand1>"
    When I select the "<WheelBrand1>" checkbox
    And  I choose filter option "Apply Filters" on Wheel Configurator modal window
    Then I verify the Wheel Configurator filters are "not displayed"
    When I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I select from the "Wheel Category" filter section, "single" option(s): "<Filter1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter1>"
    When I click on the "Apply Filters" link
    Then I should see the wheel results displayed on the Wheel Configurator modal window
    When I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I select the "<SortByOption2>" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "<SortByOption2>"
    When I reset the minimum and maximum price range
    And  I click on the "Apply Filters" link
    And  I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I select from the "Brands" filter section, "single" option(s): "<Filter2>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter2>"
    When I click on the "Apply Filters" link
    Then I should see the wheel results displayed on the Wheel Configurator modal window
    When I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I enter "<WheelBrand2>" into the 'Brands' search bar
    Then I verify the 'Brands' search bar contains the "text": "<WheelBrand2>"
    When I select from the "Brands" filter section, "single" option(s): "<WheelBrand2>"
    And  I click on the "Apply Filters" link
    Then I should see the wheel results displayed on the Wheel Configurator modal window
    When I choose a wheel "<WheelBrand2>" "<WheelName>" on Wheel Configurator modal window
    And  I select "View Details" option on "Wheel Configurator modal window" page
    Then I should see "<Year1> <Make1> <Model1> <Trim1>" details on the "PDP" page
    And  I should see "<WheelBrand2>" on product details page
    And  I should see "<WheelName>" on product details page
    When I navigate back to previous page
    And  I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    When I click on the "Apply Filters" link
    And  I click on the "Filter" button
    And  I click on the "Clear All" link
    And  I select from the "Wheel Category" filter section, "single" option(s): "<Filter1>"
    And  I select from the "Brands" filter section, "single" option(s): "<Filter2>"
    And  I reset the minimum and maximum price range
    Then I verify that the search refinement filters contain the "multiple" value(s): "<Filter1>,<Filter2>"
    When I click on the "Apply Filters" link
    And  I choose 'Change Vehicle' option on the Wheel Configurator modal window
    And  I choose "Assembly" to view its options and set the value to "<Value>"
    And  I click on the "See Wheels" button
    Then I should see the wheel results displayed on the Wheel Configurator modal window
    When I choose 'Change Vehicle' option on the Wheel Configurator modal window
    And  I choose "Year" to view its options and set the value to "<Year2>"
    And  I choose "Make" to view its options and set the value to "<Make2>"
    And  I choose "Model" to view its options and set the value to "<Model2>"
    And  I choose "Trim" to view its options and set the value to "<Trim2>"
    And  I click on the "See Wheels" button
    Then I should see the wheel results displayed on the Wheel Configurator modal window

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Year1 | Make1 | Model1 | Trim1 | Assembly1      | Value          | Year2 | Make2     | Model2   | Trim2      | SortValueDefault | SortByOption1       | WheelBrand1 | FitmentType   | Filter1 | SortByOption2       | Filter2 | WheelBrand2 | WheelName |
      | 2012 | Honda | Civic | Coupe DX | none     | 2014  | BMW   | 640i   | Coupe | 245 /40 R19 XL | 245 /45 R18 XL | 2010  | Chevrolet | Corvette | Gran Sport | Best Seller      | Price (Low to High) | TSW         | staggered     | Painted | Price (High to Low) | Beyern  | Beyern      | RAPP      |