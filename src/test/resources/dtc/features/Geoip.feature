Feature: Geo Ip Scenarios

  Background:
    Given I go to the Geoip store homepage

  @dtd
  @web
  @9308
  @8753
  @mobile
  @nexusFt
  @nexusTax
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Verify Taxes and Fees applied with Tires/Wheels/Acc having certificate_DTD (ALM#9308)
  """ Nexus P1 covered for State: IL """
  """ Nexus P2 covered for State: Yet to be added """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Sensor" fee for item
    And  I select the optional "Certificates" fee for item
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I take page screenshot
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Rebuild Kits" fee for item "<ItemCodeB>"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I take page screenshot
    And  I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    When I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "checkout" page
    And  I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I take page screenshot
    And  I verify the required fees and add-ons sections are expanded
    Then I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | Customer            | ShippingOption | Credit Card      |
      | 2010 | Chevrolet | Corvette | Base | none     | 36241     | 66319     | DEFAULT_CUSTOMER_LA | Ground         | MasterCard Bopis |

  @dtd
  @web
  @C002
  @16621
  @16601
  @16594
  @mobile
  @nexusTax
  @nexusTaxSit
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Verify Taxes and Fees for Web Order created with NO Legislation Passed State_With Staggered Fitment_DTD (ALM#16621,16601,16594)
  """ Nexus P1 covered for State: CA """
  """ Nexus P2 covered for State: AK, AR, CO, DE, KS, MO, MT, NH, NM, OR, TN, WV, WY """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Sensor" fee for item
    And  I select the optional "Certificates" fee for item
    Then I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Rebuild Kits" fee for item "<ItemCodeB>"
    Then I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "geoCustomer" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "geoCustomer"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    When I take page screenshot
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "geoCustomer"
    And  I place the order for "geoCustomer"
    Then I am brought to the order confirmation page
    When I take page screenshot
    Then I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | FitmentOption | ItemCodeB | ShippingOption | Credit Card      |
      | 2012 | Honda | Civic | Coupe DX | None     | 29935     | wheel         | 69161     | Next Day Air   | MasterCard Bopis |

  @dtd
  @web
  @9633
  @16620
  @mobile
  @nexusFt
  @nexusTax
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Verify Environmental fee charged having tires and wheels addd with certificates for NO sales tax states_DTD (ALM#9633,16620)
  """ Nexus P1 covered for State: FL """
  """ Nexus P2 covered for State: Yet to be added """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "Certificates" fee for item
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Rebuild Kits" fee for item "<ItemCodeB>"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I take page screenshot
    And  I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I take page screenshot
    And  I verify the required fees and add-ons sections are expanded
    Then I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | Year | Make | Model | Trim         | Assembly       | ItemCodeA | SizeOption | ItemCodeB | Customer            | ShippingOption | Credit Card      |
      | 2016 | Ram  | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | 14321     | 20         | 54274     | default_customer_az | Ground         | MasterCard Bopis |

  @dtd
  @web
  @C005
  @11208
  @mobile
  @nexusTax
  @smoketest
  Scenario Outline: HYBRIS_ORDERS_ORDERS_Verify Taxes and Fees for Web Order created with 5-Digit Zip Code_DTD (ALM # 11208)
  """ Nexus P1 covered for State: NV """
  """ Nexus P2 covered for State: Yet to be added """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify the "Estimated Environmental Fee" label displayed on "Shopping cart" page
    And  I verify the "Estimated Taxes" label displayed on "Shopping cart" page
    When I select the checkout option "default"
    Then I verify "Checkout" page is displayed
    When I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Est. Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    When I enter shipping info as "<Customer>"
    And  I set "Address Line 1" to "<NewAddressLine1>"
    And  I set "Zip / Postal Code" to "<NewZip>"
    And  I submit the updated address information and "check for AVS popup"
    Then I verify Tax Estimation Message is displayed for invalid response from "AVS Service" on "Checkout" page
    When I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Est. Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Est. Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    When I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Est. Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify Tax Estimation Message is displayed for invalid response from "AVS Service" on "Order Confirmation" page
    When I take page screenshot
    And  I verify the required fees and add-ons sections are expanded
    Then I verify the "Estimated Environmental Fee" label displayed on "Order Confirmation" page
    And  I verify the "Estimated Taxes" label displayed on "Order Confirmation" page

    Examples:
      | Year | Make   | Model | Trim | Assembly | ItemCode | Customer            | NewAddressLine1 | NewZip | ShippingOption | Credit Card      |
      | 2016 | Toyota | Camry | XLE  | none     | 29888    | default_customer_nv | 401 E. 8th St   | 57103  | Ground         | MasterCard Bopis |

  @dtd
  @nexusTaxUat
  @nexusTaxUat01
  Scenario Outline: Create Web Order for Geo located non-legislation states having CRT,SHP and ACC FL,NY,VA
  (No taxes and No Env Fee displayed with Geo IP)
  """ Nexus P1 covered for State: FL, NY, VA """
  """ Nexus P2 covered for State: Yet to be added """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Sensor" fee for item
    And  I select the optional "Certificates" fee for item
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Rebuild Kits" fee for item "<ItemCodeB>"
    Then I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I zoom the web page by "45" and take Screenshot and save it to "GeoState" folder with a file name of "Hybris Shopping Cart" extracted from examples
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I take page screenshot
    And  I take Screenshots of all items in checkout page and save it to "GeoState" folder with a file name of "Hybris checkoutpage" extracted from examples
    Then I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "GeoCustomer" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "GeoCustomer"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I take Screenshots of all items in checkout page and save it to "GeoState" folder with a file name of "Hybris shipping page" extracted from examples
    Then I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    When I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "GeoCustomer"
    And  I place the order for "GeoCustomer"
    Then I am brought to the order confirmation page
    When I take Screenshot and save it to "GeoState" folder with a file name of "Hybris Order Number" extracted from examples
    And  I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "GeoState" into row number "2" and cell number "2"
    And  I write the total amount into the excel sheet with sheet name "GeoState" into row number "2" and cell number "2"
    And  I write the web order number into the excel sheet with sheet name "GeoState" into row number "2" and cell number "1"
    And  I zoom the web page by "42" and take Screenshot and save it to "GeoState" folder with a file name of "Hybris Order Confirmation" extracted from examples
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | Year | Make   | Model | Trim | Assembly | ItemCodeA | ItemCodeB | ShippingOption | Credit Card      |
      | 2009 | Toyota | Camry | Base | none     | 31846     | 13699     | Next Day Air   | MasterCard Bopis |

  @dtd
  @nexusTaxUat
  @nexusTaxUat03
  Scenario Outline: Create Web Order for Geo Located legislation passed states having SHP,STD,SBL  NC,MS
  (Taxes and Env Fee displayed for NC and MS with Geo Ip)
  """ Nexus P1 covered for State: NC, MS """
  """ Nexus P2 covered for State: Yet to be added """
    When I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    Then I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeB>" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
#    When I select the optional "Heat Cycling" fee for item
    When I select the optional "Studding" fee for item
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I zoom the web page by "42" and take Screenshot and save it to "GeoState" folder with a file name of "Hybris Shopping Cart" extracted from examples
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I take Screenshots of all items in checkout page and save it to "GeoState" folder with a file name of "Hybris checkoutpage" extracted from examples
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "GeoCustomer" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "GeoCustomer"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I take Screenshots of all items in checkout page and save it to "GeoState" folder with a file name of "Hybris shipping page" extracted from examples
    And  I take page screenshot
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "GeoCustomer"
    And  I place the order for "GeoCustomer"
    Then I am brought to the order confirmation page
    When I take Screenshot and save it to "GeoState" folder with a file name of "Hybris Order Number" extracted from examples
    And  I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "GeoState" into row number "2" and cell number "2"
    And  I write the web order number into the excel sheet with sheet name "GeoState" into row number "2" and cell number "1"
    And  I verify the required fees and add-ons sections are expanded
    Then I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Order Confirmation" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeB>" product on "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    When I zoom the web page by "42" and take Screenshot and save it to "GeoState" folder with a file name of "Hybris Order Confirmation" extracted from examples

    Examples:
      | ItemCodeA | ItemCodeB | Checkout | ShippingOption | Credit Card |
      | 11120     | 41123     | default  | Second Day Air | Discover    |

  @dtd
  @nexusTaxUat
  @nexusTaxUat06
  Scenario Outline: Create Web Order for Geo located legislation passed states where Env Fee is NOT charged CT
  (Taxes are Charged and Env. Fee is NOT charged with Geo Ip)
  """ Nexus P1 covered for State: CT """
  """ Nexus P2 covered for State: Yet to be added """
    When I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    When I extract the product price from "Cart" page
    And  I zoom the web page by "42" and take Screenshot and save it to "GeoState" folder with a file name of "Hybris Shopping Cart" extracted from examples
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "Valve Stem" fee for item
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I take Screenshots of all items in checkout page and save it to "GeoState" folder with a file name of "Hybris checkoutpage" extracted from examples
    Then I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "GeoCustomer" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "GeoCustomer"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I take Screenshots of all items in checkout page and save it to "GeoState" folder with a file name of "Hybris shipping page" extracted from examples
    Then I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I take page screenshot
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "GeoCustomer"
    And  I place the order for "GeoCustomer"
    Then I am brought to the order confirmation page
    When I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "GeoState" into row number "2" and cell number "2"
    And  I write the web order number into the excel sheet with sheet name "GeoState" into row number "2" and cell number "1"
    And  I take Screenshot and save it to "GeoState" folder with a file name of "Hybris Order Number" extracted from examples
    And  I verify the required fees and add-ons sections are expanded
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    When I zoom the web page by "42" and take Screenshot and save it to "GeoState" folder with a file name of "Hybris Order Confirmation" extracted from examples

    Examples:
      | ItemCodeA | ItemCodeB | Checkout | ShippingOption | Credit Card |
      | 14279     | 15746     | default  | Ground         | Discover    |

  @dtd
  @web
  @17867
  @mobile
  @nexusFt
  @nexusTax
  Scenario Outline: HYBRIS_PRICING_TAXESANDFEES_Verify Two Env Fee line items displayed when a Studdable and Studded Tire for Washington state(ALM#17867)
  """ Nexus P1 covered for State when DMA service is online: WA """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "Studding" fee for item
    Then I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Studded" is "Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    And  I extract the product price from "Cart" page
    Then I verify "Enviro" is "Charged" for "<ItemCodeB>" product on "Shopping cart" page
    And  I verify "Studded" is "Charged" for "<ItemCodeB>" product on "Shopping cart" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    And  I verify "Studded" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    When I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Studded" is "Charged" for "all" product on "Checkout" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "GeoCustomer" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Studded" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "geoCustomer"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Studded" is "Charged" for "all" product on "Checkout" page
    When I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "GeoCustomer"
    And  I place the order for "GeoCustomer"
    Then I am brought to the order confirmation page
    When I verify the required fees and add-ons sections are expanded
    Then I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Order Confirmation" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeB>" product on "Order Confirmation" page
    And  I verify "Studded" is "Charged" for "<ItemCodeA>" product on "Order Confirmation" page
    And  I verify "Studded" is "Charged" for "<ItemCodeB>" product on "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | Year | Make      | Model          | Trim   | Assembly | ItemCodeB | ItemCodeA | ShippingOption | Credit Card      |
      | 2014 | Chevrolet | Silverado 3500 | LT 4WD | none     | 17409     | 17369     | Ground         | MasterCard Bopis |