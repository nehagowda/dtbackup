@compareProducts
Feature: Compare Products

  Background:
    Given I change to the default store

  @at
  @dt
  @bba
  @web
  @8348
  @9901
  @mobile
  @compareProductsBBA
  @compareProductsWithExpectedCategories
  Scenario: Compare Tire products with product reviews (ALM # 8348,9901)
  """Fails due to known issue in automation verifying the products on PDP page. Fix is in progress."""
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I select from the "Mileage Warranty" filter section, "single" option(s): "60,000-70,000"
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "2" products
    When I remove the first item on the compare product page
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products

  @at
  @dt
  @bba
  @web
  @8348
  @9901
  @mobile
  @compareProductsBBA
  @compareProductsRemoveFromCart
  Scenario Outline: Add and remove tires from shopping cart (ALM # 8348,9901)
  """TC 2 - Remove and re-add product from compare page"""
  """TODO: click Add an Item button is missing in the framework"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the first "<Quantity>" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "<Quantity>" products
    When I add the first item to my cart and click "Continue Shopping" on the Compare Products page
    And  I click the X next to the first product on the compare product page
    And  I click the undo remove product button
    Then I verify all categories are present for the "<Quantity>" products

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Quantity |
      | 2012 | Honda | Civic | Coupe DX | none     | 3        |

  @dt
  @at
  @dtd
  @web
  @bba
  @8348
  @9901
  @core
  @compareProductsBBA
  @coreScenarioCompareProducts
  @compareProductsModifyComparisonWithBrowserBackButton
  Scenario Outline: Modify products to be compared (ALM # 8348,9901)
  """ May fail for Staggered product due to OEES-974 (DT) or OEES-975 (DTD): Staggered sets messaging not based off the longest lead time. """
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products
    When I navigate back to previous page
    Then I verify "3" products selected
    And  I verify compare button text is displayed as "Compare"
    When I "deselect" the first eligible checkbox from the products list page
    Then I verify "2" products selected
    When I "select" the first eligible checkbox from the products list page
    Then I verify "3" products selected
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products
    When I click the X next to the first product on the compare product page
    Then I verify all categories are present for the "2" products
    When I navigate back to previous page
    Then I verify "2" products selected
    When I "select" the first eligible checkbox from the products list page
    Then I verify "3" products selected
    When I click the compare products Compare button
    Then I verify all categories are present for the "3" products
    And  I verify the current URL contains "/compare/product"

    Examples:
      | VehicleType             |
      | VEHICLE_NON_STAGGERED_1 |
      | VEHICLE_STAGGERED_1     |

  @at
  @dt
  @bba
  @8348
  @9901
  @mobile
  @compareProductsBBA
  @compareProductsModifyComparison
  Scenario: Mobile - Modify products to be compared (ALM # 8348,9901)
  """Mobile TC 3 - Modify selection updated on Compare Products page (click x on item, click Add an Item button)"""
    When I click the mobile homepage menu
    And  I click on "Tire" menu link
    And  I click on "Tire Brand" menu link
    And  I click the "Michelin Tires" menu option
    And  I select "passenger tires" from the Product Brand page
    And  I select from the "Mileage Warranty" filter section, "single" option(s): "60,000-70,000"
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "2" products
    When I remove the first item on the compare product page
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products

  @at
  @dt
  @bba
  @web
  @8348
  @9901
  @compareProductsBBA
  @compareProductsRemoveAll
  Scenario: Compare Products and remove all (ALM # 8348,9901)
  """ This will fail on the URL validation due to known defect. Defect# 10555 """
    When I open the "TIRES" navigation link
    And  I click the "Tire Type" View All link in the header
    And  I select "All-Season tires" from the Product Brand page
    And  I select "GT RADIAL" from the Product Brands page
    Then I verify that the search refinement filters contain the "single" value(s): "GT Radial"
    When I select from the "Mileage Warranty" filter section, "single" option(s): "40,000-50,000"
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products
    When I click Remove All
    Then I verify the product list page is displayed having "no" fitment

  @at
  @dt
  @bba
  @8348
  @9901
  @mobile
  @compareProductsBBA
  @compareProductsRemoveAll
  Scenario: Mobile - Compare Products and remove all (ALM # 8348,9901)
  """Mobile TC 4 - Mobile Goodyear tires does not have enough products for Mileage Warranty.More products need to be
    added to filter"""
    When I click the mobile homepage menu
    And  I click on "Tire" menu link
    And  I click on "Tire Type" menu link
    And  I click on "All-Season tires" menu link
    And  I select "GOODYEAR" from the Product Brands page
    Then I verify that the search refinement filters contain the "single" value(s): "Goodyear"
    And  I select from the "Mileage Warranty" filter section, "single" option(s): "40,000-50,000"
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products
    When I click Remove All
    Then I verify the product list page is displayed having "no" fitment

  @at
  @dt
  @bba
  @web
  @8348
  @9901
  @compareProductsBBA
  @compareProductsFromDifferentResultPages
  Scenario: Compare Products from different result pages (ALM # 8348,9901)
  """TC 5"""
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "passenger tires" from the Product Brand page
    And  I select a single product to compare
    And  I go to the "next" page of product list results
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "2" products

  @dt
  @at
  @bba
  @8348
  @9901
  @mobile
  @compareProductsBBA
  @compareProductsFromDifferentResultPages
  Scenario: Mobile - Compare Products from different result pages (ALM # 8348,9901)
  """Mobile TC 5 - Mobile"""
    When I click the mobile homepage menu
    And  I click on "Tire" menu link
    And  I click on "Tire Brand" menu link
    And  I click on "Michelin Tires" menu link
    And  I select "All-Season tires" from the Product Brand page
    And  I select a single product to compare
    And  I go to the "next" page of product list results
    And  I select a single product to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "2" products

  @bba
  @dtd
  @web
  @8856
  @compareProductsBBA
  Scenario Outline: DTD - Compare Tire products with product reviews (ALM # 8856)
    When I do a "homepage" "Brand" "tire" search with option: "<Brand>"
    Then I see the "<Brand>" that I selected
    When I select "<SubCategory>" to shop
    And  I select from the "Mileage Warranty" filter section, "single" option(s): "60,000-70,000"
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products

    Examples:
      | Brand          | SubCategory      |
      | Michelin Tires | All-Season tires |

  @bba
  @dtd
  @8856
  @mobile
  @compareProductsBBA
  Scenario Outline: DTD Mobile - Compare Tire products (ALM # 8856)
    When I click the mobile homepage menu
    And  I click on "Tire" menu link
    And  I click on "Tire Brand" menu link
    And  I click on "View all" menu link
    And  I select the "<Brand>" brand image
    And  I select "<SubCategory>" to shop
    And  I select from the "Mileage Warranty" filter section, "single" option(s): "60,000-70,000"
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products

    Examples:
      | Brand          | SubCategory      |
      | Michelin Tires | All-Season tires |

  @at
  @dt
  @web
  @15703
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify the inventory message for the products in Compare Products page (ALM #15703)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY BRAND" menu option
    And  I select the "<Brand>" brand image
    And  I select shop all from the Product Brand page
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "2" products
    When I select 'Add an item' to compare
    And  I "select" the compare checkbox at position "<Position>" from the products list page
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products

    Examples:
      | ZipCode | Brand          | Position |
      | 85260   | Michelin Tires | 3        |

  @at
  @dt
  @15703
  @mobile
  @extendedAssortment
  Scenario Outline: Mobile - HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify the inventory message for the products in Compare Products page (ALM #15703)
    When I click the mobile homepage menu
    And  I click on "Tire" menu link
    And  I click on "Tire Brand" menu link
    And  I click on "<Brand>" menu link
    And  I select shop all from the Product Brand page
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    Then I verify all categories are present for the "2" products
    When I select 'Add an item' to compare
    And  I "select" the compare checkbox at position "<Position>" from the products list page
    And  I click the compare products Compare button
    Then I verify all categories are present for the "3" products

    Examples:
      | Brand            | Position |
      | Michelin Tires   | 3        |
      | BFGoodrich Tires | 3        |

  @at
  @dt
  @web
  @15748
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify the inventory message for the products in Compare Products page for Regular Vehicle (ALM #15748)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    And  I verify the "<InventoryMessage1>" displayed for "<ItemCode1>" on "PLP" page
    And  I verify the "<InventoryMessage2>" displayed for "<ItemCode2>" on "PLP" page
    When I select "<ItemCode1>" on the product list page to compare
    And  I select "<ItemCode2>" on the product list page to compare
    And  I click the compare products Compare button
    Then I verify the "<InventoryMessage1>" displayed for "<ItemName1>" on "Compare products" page
    And  I verify the "<InventoryMessage2>" displayed for "<ItemName2>" on "Compare products" page
    When I select 'Add an item' to compare
    And  I select "<ItemCode3>" on the product list page to compare
    And  I verify the "<InventoryMessage3>" displayed for "<ItemCode3>" on "PLP" page
    And  I click the compare products Compare button
    Then I verify the "<InventoryMessage3>" displayed for "<ItemName3>" on "Compare products" page

    Examples:
      | ZipCode | Year | Make  | Model | Trim     | Assembly | ItemCode1 | ItemCode2 | ItemCode3 | ItemName1          | ItemName2                   | ItemName3 | InventoryMessage1                                    | InventoryMessage2                                   | InventoryMessage3           |
      | 85260   | 2012 | Honda | Civic | Coupe DX | none     | 29935     | 19661     | 43006     | Silver Edition III | Control Contact Touring A/S | YK580     | Order now, available as soon as tomorrow at My Store | Order now, available as soon as tomorrow at My Store | Available today at My Store |

  @at
  @dt
  @web
  @15749
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_SEARCH_SEARCH_EXTENDEDASSORTMENT_Verify the inventory message for the products in Compare Products page for Staggered Vehicle (ALM #15749)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    And  I verify the "<InventoryMessage1>" for set displayed for "<ItemCode1>" on "PLP" page
    And  I verify the "<InventoryMessage2>" for set displayed for "<ItemCode2>" on "PLP" page
    And  I verify the "<InventoryMessage3>" for set displayed for "<ItemCode3>" on "PLP" page
    And  I verify the "<InventoryMessage4>" for set displayed for "<ItemCode4>" on "PLP" page
    When I select "<ItemCode1>" on the product list page to compare
    And  I select "<ItemCode3>" on the product list page to compare
    And  I click the compare products Compare button
    Then I verify the "<InventoryMessage1>" displayed for "FRONT" set Tire for product "<SetName1>"
    And  I verify the "<InventoryMessage2>" displayed for "REAR" set Tire for product "<SetName1>"
    And  I verify the "<InventoryMessage3>" displayed for "FRONT" set Tire for product "<SetName2>"
    And  I verify the "<InventoryMessage4>" displayed for "REAR" set Tire for product "<SetName2>"

    Examples:
      | ZipCode | Year | Make      | Model    | Trim | Assembly | ItemCode1 | ItemCode2 | ItemCode3 | ItemCode4 | SetName1     | SetName2             | InventoryMessage1                              | InventoryMessage2                              | InventoryMessage3                                    | InventoryMessage4                         |
      | 85260   | 2010 | Chevrolet | Corvette | Base | none     | 30018     | 30019     | 37068     | 14777     | Eagle F1 GS2 | G-Force Sport Comp 2 | Order now, available in 3 - 5 days at My Store | Order now, available in 3 - 5 days at My Store | Order now, available as soon as tomorrow at My Store | Order now, available as soon as tomorrow at My Store |