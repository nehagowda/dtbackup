@bvtdtdOrdersCheckoutAvs
Feature: DtdOrdersCheckoutAVS

  @dtd
  @web
  @bvt
  @core
  @6947
  @bvtOrders
  @coreScenarioOrders
  @bvtScenarioShippingNexusTaxAVS
  Scenario Outline: HYBRIS_ORDERS_CHECKOUT_AVS_Validate the AVS modal displayed when the response from AVS is Verification Required_DTD (ALM #6947,10364)
    When I go to the homepage
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I select the checkout option "default"
    And  I enter shipping info as "<Customer>"
    And  I set "Address Line 1" to "<NewAddressLine1>"
    And  I set "Zip / Postal Code" to "<NewZip>"
    And  I submit the updated address information and "do not handle AVS popup"
    Then I verify the "Address Verification" modal is displayed
    And  I verify USPS corrected address display "<ZipPlus4>" on "checkout" page for "default" contact by customer type "<Customer>"
    When I select "Edit Entered Address" from ADDRESS VERIFICATION modal
    And  I enter shipping info as "<Customer>"
    And  I set "Address Line 1" to "<NewAddressLine1>"
    And  I set "Zip / Postal Code" to "<ZipPlus4>"
    When I submit the updated address information and "check for AVS popup"
    And  I click on the "Continue To Payment" button
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | ItemCode | Customer             | NewAddressLine1      | NewZip  | ZipPlus4   |
      | 29935    | default_customer_az  | W 67 Elwood St       | 85040   | 85040-1025 |
      | 29935    | default_customer_can | 372 Rundleview Drive | T1Y 1H3 | T1Y 1H8    |

  @dtd
  @web
  @bvt
  @launchDtd
  @bvtlaunchDtd
  Scenario: LaunchDTD (ALM #NONE)
    When I go to the homepage