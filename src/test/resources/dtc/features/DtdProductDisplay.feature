@bvtdtdProductDisplay
Feature: DtdProductDisplay

  @dtd
  @web
  @bvt
  @core
  @bvtOrders
  @productDetailPage
  @coreScenarioOrders
  @bvtScenarioCheckout_CanadianCustomer_ShippingBlockMessage
  Scenario Outline: HYBRIS_PRODUCT_DISPLAY RULES Cart Page With Restricted and Unrestricted Products Remove Restricted (ALM #6859)
    When I go to the homepage
    And  I do a free text search for "<Restricted Item Code 1>" and hit enter
    Then I verify the "We cannot ship items to locations" message is "displayed" on the product detail page
    When I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<Restricted Item Code 2>" and hit enter
    Then I verify the "We cannot ship items to locations" message is "displayed" on the product detail page
    When I add item to my cart and "View shopping Cart"
    Then I verify product "<Restricted Item Name 1>" is "displayed" on the "cart" page
    And  I verify product "<Restricted Item Name 2>" is "displayed" on the "cart" page
    When I select the checkout option "none"
    And  I enter shipping info as "<Canadian Customer>" and continue to next page
    Then I verify the "Shipping Restriction" modal messaging as well as controls are "displayed"
    When I select "Close" from the "Shipping Restriction" modal
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify product "<Restricted Item Name 1>" is "displayed" in the "Order Summary" of the Checkout page
    And  I verify product "<Restricted Item Name 2>" is "displayed" in the "Order Summary" of the Checkout page
    When I enter shipping info as "<Canadian Customer>" and continue to next page
    And  I select "Remove Items" from the "Shipping Restriction" modal
    Then I should see product has been "removed" in cart message

    Examples:
      | Restricted Item Code 1 | Restricted Item Name 1 | Restricted Item Code 2 | Restricted Item Name 2 | Canadian Customer    |
      | 19350                  | Bridgestone            | 42355                  | AMERI TRAC 13          | default_customer_can |

  @dtd
  @web
  @bvt
  @core
  @bvtOrders
  @productDetailPage
  @coreScenarioCheckout
  @bvtScenarioShippingBlockRule
  Scenario Outline: HYBRIS_PRODUCT_DISPLAY RULES Shipping Block Rule And Change Address (ALM #6846)
    When I go to the homepage
    And  I do a free text search for "<Item Code>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<Item Name>" on the "cart" page
    When I select the checkout option "none"
    And  I enter shipping info as "<Canadian Customer>" and continue to next page
    Then I verify the "Shipping Restriction" modal messaging as well as controls are "displayed"
    When I select "Change Address" from the "Shipping Restriction" modal
    And  I enter shipping info as "<US Customer>" and continue to next page
    Then I verify the "Shipping Restriction" modal messaging as well as controls are "not displayed"
    And  I confirm the shipping options are: "Ground - Free"

    Examples:
      | Item Code | Item Name   | Canadian Customer    | US Customer         |
      | 19350     | NITTO       | default_customer_can | default_customer_az |