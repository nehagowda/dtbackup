@store
Feature: Store

  Background:
    Given I go to the homepage

  @dt
  @web
  @8550
  @storeDetails
  @myStoreGeopip
  Scenario Outline: Verify store location based on GeopIP (ALM #8550)
    When I change to the default store
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I click on Store details button in My Store popup
    Then I verify the "<StoreTitle>", "<StoreAddress>" of the current store

    Examples:
      | StoreAddress     | StoreTitle          |
      | 1230 S Milton Rd | Discount Tire Store |

  @dt
  @web
  @bba
  @8555
  @mobile
  @storeBBA
  @storeLocator
  @storeLocatorRedirectDtToAtBackToDt
  Scenario Outline: Store Locator redirect from DT to AT back to DT (ALM #8555)
    When I search for store within "<Range>" miles of "<Americas Tire Service Area>"
    And  I select make "<Americas Tire Service Area>" my store
    And  I "continue" the Welcome Popup
    Then I verify site is "America's Tire"
    When I search for store within "<Range>" miles of "<Discount Tire Service Area>"
    And  I select make "<Discount Tire Service Area>" my store
    And  I "continue" the Welcome Popup
    Then I verify site is "Discount Tire"
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I search for store within "<Range>" miles of "<Americas Tire Service Area>"
    And  I select make "<Americas Tire Service Area>" my store
    Then I verify the 'Confirm Store Changed' Modal is displayed with warning message
    When I click on the "USE THIS STORE" button
    And  I "continue" the Welcome Popup
    Then I verify site is "America's Tire"
    When I search for store within "<Range>" miles of "<Discount Tire Service Area>"
    And  I select make "<Discount Tire Service Area>" my store
    Then I verify the 'Confirm Store Changed' Modal is displayed with warning message
    When I click on the "USE THIS STORE" button
    And  I "continue" the Welcome Popup
    Then I verify site is "Discount Tire"

    Examples:
      | Range | Americas Tire Service Area | Discount Tire Service Area | ItemCode |
      | 25    | 96002                      | 86001                      | 29935    |

  @dt
  @web
  @8556
  @9388
  @6402
  @6401
  @mobile
  @storeLocator
  Scenario Outline: Store Locator for shop DTD message redirect from DT to DTD (ALM #8556,9388,6402,6401)
    When I search for store within "<Range>" miles of "<A Non-Discount Tire Service Area>"
    Then I verify Shop Discount Tire Direct message displays "searched result"
    When I select shop discount tire direct
    Then I am brought to the "Discount Tire Direct" site

    Examples:
      | Range | A Non-Discount Tire Service Area |
      | 25    | New York                         |

  @dt
  @at
  @dtd
  @web
  @6272
  @6643
  @mobile
  @storeLocator
  Scenario Outline: Store Locator No search result shop DTD message on DT and DTD (ALM #6272)
    When I search for store within "<Range>" miles of "<Invalid Zip>"
    Then I verify Shop Discount Tire Direct message displays "no search result"
    When I click the "Shop Tires" button link
    Then I verify fitment popup page displays
    When I close popup modal
    And  I click the "Shop Wheels" button link
    Then I verify fitment popup page displays

    Examples:
      | Range | Invalid Zip |
      | 25    | 123456      |
      | 75    | asdwfwfwf   |

  @dt
  @web
  @6387
  @mobile
  @storeLocator
  Scenario Outline: HYBRIS_Store_Store Locator_100+ Miles Display Logic (ALM #6387)
    When I search for store within "<Range>" miles of "<A Non-Discount Tire Service Area>"
    Then I verify discount Tire Stores displayed for hundred plus miles display logic
    And  I verify Shop Discount Tire Direct message displays "searched miles expanded"
    When I select shop discount tire direct
    Then I am brought to the "Discount Tire Direct" site

    Examples:
      | Range | A Non-Discount Tire Service Area |
      | 100   | South Dakota                     |

  @dt
  @web
  @6641
  @6279
  @mobile
  @storeLocator
  Scenario Outline: HYBRIS_Store_Store Locator_Store List_Pagination (ALM #6641,6279)
    When I search for store within "<Range>" miles of "<A Discount Tire Service Area>"
    Then I verify the nearest stores sorted based on miles on "Store Locator" page
    And  I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "Store Locator" page
    When I search for store within "<Range>" miles of "<Zipcode>"
    Then I verify the nearest stores sorted based on miles on "Store Locator" page
    When I select make "<Zipcode>" my store
    Then I verify the "<StoreTitle>", "<StoreAddress>" of the current store

    Examples:
      | Range | A Discount Tire Service Area | Zipcode | StoreTitle          | StoreAddress     |
      | 100   | 48084                        | 86001   | Discount Tire Store | 1230 S Milton Rd |

  @web
  @bba
  @dtd
  @8566
  @mobile
  @storeBBA
  @storeLocator
  Scenario Outline: DTD Installer locator shows non discount tires installer partner list and discount tire stores(ALM #8566)
    When I open the "Installers" navigation link
    Then I am brought to the page with header "Installer Locator"
    When I search for store within "<Range>" miles of "New York"
    Then I should see this partner installer "<Partner Installer>" is present in the displayed stores list
    And  I verify schedule appointment option is not available to users on DTD store locator page
    When I select "Directions" for store #"1" in the location results
    And  I navigate to newly opened next tab
    Then I am brought to the page with path "<Path>"
    When I navigate to previous browser tab
    And  I select "Share" for store #"1" in the location results
    Then I verify the "Share Installer Details" modal is displayed
    When I select send to "Phone" "<PhoneNumber>" on "Installer Locator" page
    Then I verify "Success" message displayed

    Examples:
      | Range | Partner Installer                | Path                      |
      | 75    | Savage Wheels & Things Tire Shop | www.google.com/maps/dir// |

  @at
  @dt
  @web
  @8578
  @mobile
  Scenario Outline: HYBRIS_139_Store_StoreLocator Send Store Adderss to Customer as Text (ALM #8578)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select "Share" for store #"1" in the location results
    Then I verify the "Share Store Details" modal is displayed
    When I select send to "Phone" "<PhoneNumber>" on "Store Details" page
    Then I verify "Success" message displayed

    Examples:
      | ZipCode | PhoneNumber  |
      | 86001   | 555-555-5555 |

  @dt
  @web
  @8565
  @storeLocator
  Scenario Outline: HYBRIS_140_Store_StoreLocator Make Appointment to the Store (ALM #8565)
    When I change to the default store
    And  I select "Appointment" for store #"1" in the location results
    And  I select service option(s): "<ServiceOptions>"
    And  I select default date and time
    And  I click continue for appointment customer details page
    And  I select "Schedule Appointment" after entering customer information for "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOptions>"
    And  I store the order number

    Examples:
      | ServiceOptions            | Customer            |
      | Tire Rotation and Balance | default_customer_az |

  @at
  @dt
  @web
  @5321
  @6367
  @5514
  @5974
  @6569
  @6566
  @6245
  @6254
  @6250
  @mobile
  @storeDetails
  @storeLocator
  Scenario Outline: HYBRIS_STORE EXPERIENCE_STORE DETAILS_Validate copy link on the share modal by clicking on the URL/Share Icon for Not My Store(ALM #5321,6367,5514,5974,6569,6566,6245,6254,6250)
    When I open the Store Locator page
    And  I search for store within "25" miles of "<ZipCode>"
    Then I am brought to the page with header "Store Locator"
    And  I should see the "Share Icon" on the "Store Locator" page
    And  I should see the "Directions" on the "Store Locator" page
    When I select "Share" option on "Store Details" page
    Then I verify the "Share Store Details" modal is displayed
    When I select "<Option>" on Share "Store" Details popup page
    Then I verify "Copied" message displayed
    And  I verify the displayed text matches to the clipboard copied text for "Store Details Link"
    When I select send to "Phone" "<PhoneNumber>" on "Store Details" page
    Then I verify "Success" message displayed
    When I close popup modal
    And  I select "Directions" option on "Store Details" page
    And  I switch to "Newly launched" window
    Then I should see "<GoogleMapFromGeoipStore>" url is launched
    When I navigate to previous browser tab
    And  I click "Schedule appointment" on "Store Locator" page
    Then I am brought to the page with header "Service Appointment"
    When I open the Store Locator page
    And  I search for store within "25" miles of "<ZipCode>"
    And  I select "<ZipCode>" for store details
    Then I am brought to the page with header "<Header>"
    And  I should see the "Share Icon" on the "Store Details" page
    And  I should see the "Directions" on the "Store Details" page
    When I select "Share" option on "Store Details" page
    Then I verify the "Share Store Details" modal is displayed
    When I select "<Option>" on Share "Store" Details popup page
    Then I verify "Copied" message displayed
    And  I verify the displayed text matches to the clipboard copied text for "Store Details Link"
    When I select send to "Phone" "<PhoneNumber>" on "Store Details" page
    Then I verify "Success" message displayed
    When I close popup modal
    And  I select "Directions" option on "Store Details" page
    And  I switch to "Newly launched" window
    Then I should see "<GoogleMapFromFlagstaffStore>" url is launched

    Examples:
    | ZipCode | Header        | Option         | PhoneNumber | GoogleMapFromGeoipStore | GoogleMapFromFlagstaffStore |
    | 86001   | Store Details | link           | 5555555555  | /maps/dir//             | /maps/dir//1230             |
    | 86001   | Store Details | copy link icon | 5555555555  | /maps/dir//             | /maps/dir//1230             |

  @at
  @dt
  @web
  @9382
  @6233
  @6584
  @6245
  @storeDetails
  Scenario Outline: HYBRIS_STORE EXPERIENCE_STORE_STORE DETAILS_Validate Send to phone on the share modal for My store (ALM #9382)
    When I click on "My Store" title
    And  I click on Store details button in My Store popup
    Then I am brought to the page with header "<Header>"
    And  I should see the "Share Icon" on the "Store Details" page
    And  I should see the "Directions" on the "Store Details" page
    When I select "Share" option on "Store Details" page
    Then I verify the "Share Store Details" modal is displayed
    When I select "<Option>" on Share "Store" Details popup page
    Then I verify "Copied" message displayed
    And  I verify the displayed text matches to the clipboard copied text for "Store Details Link"
    When I select send to "Phone" "<PhoneNumber>" on "Store Details" page
    Then I verify "Success" message displayed
    When I close popup modal
    And  I select "Directions" option on "Store Details" page
    And  I switch to "Newly launched" window
    Then I should see "<GoogleMap>" url is launched

    Examples:
      | Header        | Option         | PhoneNumber | GoogleMap  |
      | Store Details | link           | 5555555555  | maps/dir// |
      | Store Details | copy link icon | 5555555555  | maps/dir// |

  @at
  @dt
  @web
  @core
  @6252
  @6264
  @storeDetails
  @storeLocator
  @coreScenarioStore
  Scenario Outline: HYBRIS_STORE EXPERIENCE_STORE DETAILS_Validate Schedule appointment while items in cart (ALM#6252,6264)
    When I change to the default store
    And  I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I click on "My Store" title
    And  I click on Store details button in My Store popup
    And  I click "Schedule appointment" on "Store Details" page
    Then I verify the window with header "Action needed" is displayed
    And  I verify "CHECKOUT WITH APPOINTMENT" option is displayed on appointments 'Action Needed' modal
    And  I verify "APPOINTMENT ONLY" option is displayed on appointments 'Action Needed' modal
    When I select "CHECKOUT WITH APPOINTMENT" button
    Then I am brought to the page with path "<Expected Checkout Path>"
    And  I am brought to the page with header "Checkout"
    When I navigate back to previous page
    And  I close popup modal
    Then I am brought to the page with header "<Header>"
    When I click "Schedule appointment" on "Store Details" page
    Then I verify the window with header "Action needed" is displayed
    When I select "APPOINTMENT ONLY" button
    Then I am brought to the page with path "<Expected Service Appointment Path>"
    And  I am brought to the page with header "Service Appointment"
    And  I verify the header cart total is "<MiniCartTotal>"

    Examples:
      | VehicleType             | ItemCode | Header        | Expected Checkout Path    | Expected Service Appointment Path | MiniCartTotal |
      | VEHICLE_NON_STAGGERED_1 | 29935    | Store Details | checkout/appointment-info | /schedule-appointment             | $0.00         |

  @at
  @dt
  @9393
  @6264
  @mobile
  @storeDetails
  @storeLocator
  Scenario Outline: HYBRIS_STORE EXPERIENCE_STORE DETAILS_Validate Schedule appointment while items in cart for mobile (ALM#9393,6264)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I open the Store Locator page
    And  I click "Schedule appointment" on "Store Locator" page
    Then I verify the window with header "Action needed" is displayed
    And  I verify "CHECKOUT WITH APPOINTMENT" option is displayed on appointments 'Action Needed' modal
    And  I verify "APPOINTMENT ONLY" option is displayed on appointments 'Action Needed' modal
    When I select "CHECKOUT WITH APPOINTMENT" button
    Then I am brought to the page with path "<Expected Checkout Path>"
    And  I am brought to the page with header "Checkout"
    When I navigate back to previous page
    And  I click the mobile homepage menu
    And  I click on "Schedule appointment" menu link
    Then I verify the window with header "Action needed" is displayed
    And  I verify "CHECKOUT WITH APPOINTMENT" option is displayed on appointments 'Action Needed' modal
    And  I verify "APPOINTMENT ONLY" option is displayed on appointments 'Action Needed' modal
    When I select "CHECKOUT WITH APPOINTMENT" button
    Then I am brought to the page with path "<Expected Checkout Path>"
    And  I am brought to the page with header "Checkout"
    When I navigate back to previous page
    And  I click the mobile homepage menu
    And  I click on "Schedule appointment" menu link
    Then I verify the window with header "Action needed" is displayed
    When I select "APPOINTMENT ONLY" button
    Then I am brought to the page with path "<Expected Service Appointment Path>"
    And  I am brought to the page with header "Service Appointment"
    And  I verify the header cart total is "<MiniCartCount>"

    Examples:
      | ItemCode | Expected Checkout Path    | Expected Service Appointment Path | MiniCartCount |
      | 29935    | checkout/appointment-info | /schedule-appointment             | 0             |

  @at
  @dt
  @web
  @6357
  @9366
  @storeDetails
  Scenario Outline: HYBRIS_STORE EXPERIENCE_STORE DETAILS_Verify if stores locations are marked by pins on the map for 3 near by stores(ALM#6357,9366)
    When I click on "My Store" title
    And  I click on Store details button in My Store popup
    Then I am brought to the page with header "<Header>"
    And  I verify three nearest stores displayed for the current store on store details page
    And  I verify the nearest stores sorted based on miles on "Store Details" page

    Examples:
      | Header        |
      | Store Details |

  @at
  @dt
  @web
  @6375
  @mobile
  @storeDetails
  Scenario Outline: HYBRIS_STORE EXPERIENCE_STORE DETAILS_Verify if stores locations are marked by pins on the map for 3 near by stores_Mobile(ALM#6375)
    When I open the Store Locator page
    And  I search for store within "25" miles of "<ZipCode>"
    And  I select "<Store>" for store details
    Then I am brought to the page with header "<Header>"
    And  I verify three nearest stores displayed for the current store on store details page
    And  I verify the nearest stores sorted based on miles on "Store Details" page
    And  I verify the nearest store number "2" is "collapsed"
    When I "expand" number "2" nearest Store on store details page
    Then I verify the nearest store number "2" is "expanded"
    When I "collapse" number "2" nearest Store on store details page
    Then I verify the nearest store number "2" is "collapsed"

    Examples:
      | ZipCode | Store            | Header        |
      | 86001   | 1230 S Milton Rd | Store Details |

  @dt
  @web
  @9385
  @mobile
  @storeLocator
  Scenario Outline: HYBRIS_Store_StoreLocator_Eliminate Clear Cart (ALM#9385)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    Then I verify the 'Confirm Store Changed' Modal is displayed with warning message
    And  I verify the modal popup contains item "<ProductName>" with "Available" message
    And  I verify the "USE THIS STORE" button is "enabled"
    And  I verify the "CANCEL" button is "enabled"
    When I click on the "USE THIS STORE" button
    Then I verify the "<StoreTitle>", "<StoreAddress>" of the current store
    When I select mini cart and "View Cart"
    Then I verify product "<ProductName>" is "displayed" on the "cart" page

    Examples:
      | ItemCode | ZipCode | ProductName        | StoreTitle          | StoreAddress     |
      | 29935    | 86001   | Silver Edition III | Discount Tire Store | 1230 S Milton Rd |

  @at
  @dt
  @web
  @6253
  @7963
  @6958
  @15204
  @19454
  @mobile
  @storeLocator
  Scenario Outline: Hybris_Store_Storelocator_SiteSwap_AppointmentsOnlyAndCheckoutWithAppointment_NO Vehicle No Cart_No Vehicle and With Cart (ALM#6253,7963,7958,15204,19454)
    When I change to the default store
    And  I search for stores within "25" miles of non default zip code
    And  I select "Appointment" for store #"1" in the location results
    Then I am brought to the page with path "<Expected Service Appointment Path>"
    When I change to the default store
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I search for stores within "25" miles of non default zip code
    And  I select "Appointment" for store #"1" in the location results
    Then I verify the window with header "Action needed" is displayed
    When I select "APPOINTMENT ONLY" button
    Then I am brought to the page with path "<Expected Service Appointment Path>"
    When I change to the default store
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I search for stores within "25" miles of non default zip code
    And  I select "Appointment" for store #"1" in the location results
    Then I verify the window with header "Action needed" is displayed
    When I select "CHECKOUT WITH APPOINTMENT" button
    Then I am brought to the page with path "/checkout/appointment-info"
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I search for stores within "25" miles of non default zip code
    And  I select "Appointment" for store #"1" in the location results
    Then I verify the window with header "Action needed" is displayed
    When I select "CHECKOUT WITH APPOINTMENT" button
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    And  I select "Continue to Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page

    Examples:
    | ItemCode | Year | Make  | Model | Trim     | Assembly | Expected Service Appointment Path | FitmentOption | Reason                              | Customer            |
    | 29935    | 2012 | Honda | Civic | Coupe DX | none     | /schedule-appointment             | tire          | Make an appointment at a later time | default_customer_az |

  @dt
  @web
  @6251
  @6256
  @19453
  @mobile
  @storeLocator
  Scenario Outline: HYBRIS_Store_StoreLocator_Eliminate Clear Cart (ALM#6251,6256,19453)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I search for store within "25" miles of "<ZipCode>"
    And  I select "Appointment" for store #"1" in the location results
    Then I verify the window with header "Action needed" is displayed
    And  I verify "CHECKOUT WITH APPOINTMENT" option is displayed on appointments 'Action Needed' modal
    And  I verify "APPOINTMENT ONLY" option is displayed on appointments 'Action Needed' modal
    When I select "CHECKOUT WITH APPOINTMENT" button
    Then I verify site is "America's Tire"
    And  I am brought to the page with header "Checkout"
    And  I verify "<AT StoreAddress>" store on the customer information appointment page
    When I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page

    Examples:
      | ItemCode | ZipCode | Reason                              | Customer            | AT StoreAddress |
      | 29935    | 96002   | Make an appointment at a later time | default_customer_az | 16 Hartnell Ave |