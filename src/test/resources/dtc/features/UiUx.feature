@UiUx
Feature: UI/UX -(Header, Footer, PDP, PLP & Navigation)

  Background:
    Given I change to the default store

  @dt
  @web
  @launchDt
  @bvtlaunchDt
  Scenario: LaunchDT (ALM #NONE)
    When I go to the homepage

  @dt
  @web
  @bvtVerifyBasicFunctionality
  Scenario Outline: LaunchDT and execute some basic functionality - DT - Non-staggered (ALM #NONE)
    """ Bring staggered and non-staggered back together once Suggested Selling is also applied to staggered fitment in Phase 2. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that tooltip icon is present in the carousel for tire product
    And  I click on the "<Year> <Make>" button
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I click on the "shop products" link
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I click the discount tire logo
    And  I click on the "<Year> <Make>" button
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "with appointment"
    And  I select first available appointment date
    And  I click the discount tire logo
    And  I click on the "<Year> <Make>" button
    And  I select "footer" "Customer Care"
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I close popup modal
    And  I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option

    Examples:
      | Year | Make  | Model | Trim     | Assembly | title             |
      | 2012 | Honda | Civic | Coupe DX | none     | TRENDING PRODUCTS |

  @dt
  @web
  @bvtVerifyBasicFunctionality
  Scenario Outline: LaunchDT and execute some basic functionality - DT - Staggered (ALM #NONE)
  """ Bring staggered and non-staggered back together once Suggested Selling is also applied to staggered fitment in Phase 2. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I click on the "shop products" link
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I click the discount tire logo
    And  I click on the "<Year> <Make>" button
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "with appointment"
    And  I select first available appointment date
    And  I click the discount tire logo
    And  I click on the "<Year> <Make>" button
    And  I select "footer" "Customer Care"
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I close popup modal
    And  I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option

    Examples:
      | Year | Make      | Model    | Trim | Assembly |
      | 2010 | Chevrolet | Corvette | Base | none     |

  @dtd
  @web
  @bvtVerifyBasicFunctionality
  Scenario Outline: LaunchDT and execute some basic functionality - DTD - Non-staggered (ALM #NONE)
  """ Bring staggered and non-staggered back together once Suggested Selling is also applied to staggered fitment in Phase 2. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that tooltip icon is present in the carousel for tire product
    And  I click on the "<Year> <Make>" button
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I click on the "shop products" link
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I click on the "shop products" link
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    And  I select "footer" "Customer Care"
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I close popup modal
    And  I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option

    Examples:
      | Year | Make  | Model | Trim     | Assembly | title             |
      | 2012 | Honda | Civic | Coupe DX | none     | TRENDING PRODUCTS |

  @dtd
  @web
  @bvtVerifyBasicFunctionality
  Scenario Outline: LaunchDT and execute some basic functionality - DTD - Staggered (ALM #NONE)
  """ Bring staggered and non-staggered back together once Suggested Selling is also applied to staggered fitment in Phase 2. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I click on the "shop products" link
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I click on the "shop products" link
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    And  I select "footer" "Customer Care"
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I close popup modal
    And  I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option

    Examples:
      | Year | Make      | Model    | Trim | Assembly |
      | 2010 | Chevrolet | Corvette | Base | none     |

  @at
  @dt
  @web
  @13085
  @regression
  @UiUxFooter
  @UiUxRegression
  Scenario: Verify working Apply Now footer form (ALM#13085)
    When I select "footer" "Credit"
    Then I verify the "Home, Finance" link in the breadcrumb container
    When I click on the "Apply Now" button
    Then I should see text "We take great care to protect your information" present in the page source
    When I go to the homepage
    And  I select "footer" "Apply Now"
    Then I should see text "We take great care to protect your information" present in the page source

  @dt
  @web
  @bba
  @dtd
  @5300
  @UiUxBBA
  @UiUxFooter
  @UiUxRegression
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_FOOTER_Validate links(DT & DTD) (ALM #5300)
    When I browse to the "<Page>" page with defaults
    Then I verify the Headline Text of footer is displayed
    And  I verify the footer link launched URLs heading, breadcrumb and url content

    Examples:
      | Page               |
      | Homepage           |
      | PLP                |
      | PDP                |
      | Shopping Cart      |

  @web
  @dt
  @dtd
  @bba
  @5284
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different categories related to WHEELS (ALM#5284)
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY VEHICLE" menu option
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year>" "<Make>"
    When I close popup modal
    And  I remove my "selected vehicle" vehicle
    And  I open the "WHEELS" navigation link
    And  I click the "WHEELS BY SIZE" menu option
    And  I do a "homepage" "wheel" size search with details "15, 7.0, 5-100.0 MM"
    Then I verify the "PLP" results banner message contains "15X7.0 5-100.0 mm"
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    Then I am brought to the page with path "/wheels/brands"
    And  I verify all of the fitment menus are displayed

    Examples:
      | Year | Make  | Model  | Trim     | Assembly |
      | 2012 | HONDA | ACCORD | EX COUPE | none     |


  @web
  @dt
  @dtd
  @bba
  @5284
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different brand categories related to WHEELS (ALM#5284)
  """TODO: Unique Brand - fails in DTD due to Defect #9921"""
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    And  I select the "<Brand>" brand image
    Then I am brought to the page with path "<Path>"
    And  I verify all of the fitment menus are displayed
    When I select shop all from the Product Brand page
    Then I can see "<Brand>" PLP page

    Examples:
      | Brand       | Path                     |
      | MB WHEELS   | /wheels/brands/mb-wheels |
      | UNIQUE      | /wheels/brands/unique    |
      | DRAG WHEELS | /wheels/brands/drag      |
      | KONIG       | /wheels/brands/konig     |

  @web
  @dt
  @dtd
  @bba
  @5284
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different Style and Type categories related to WHEELS (ALM#5284)
    When I open the "WHEELS" navigation link
    And  I click the "<StyleTypeLink>" menu option
    Then I am brought to the page with path "<Path>"
    And  I verify all of the fitment menus are displayed
    When I select shop all from the Product Brand page
    Then I can see "<StyleTypePLPPage>" PLP page

    Examples:
      | StyleTypeLink    | Path              | StyleTypePLPPage |
      | Painted Wheels   | /wheels/painted   | painted          |
      | Machined Wheels  | /wheels/machined  | machined         |
      | Chrome Wheels    | /wheels/chrome    | chrome           |
      | Mesh Wheels      | /wheels/mesh      | mesh             |
      | Trailer Wheels   | /wheels/trailer   | trailer          |
      | Passenger Wheels | /wheels/passenger | passenger        |
      | ATV/UTV Wheels   | /wheels/atv-utv   | atv-utv          |
      | Truck Wheels     | /wheels/truck     | truck            |

  @mobile
  @dt
  @dtd
  @bba
  @5284
  @UiUxBBA
  Scenario Outline: Mobile - HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different categories related to WHEELS (ALM#5284)
    When I click the mobile homepage menu
    And  I click on "<Menu>" menu link
    And  I click on "<Sub Menu>" menu link
    And  I click on "<Link>" menu link
    Then I am brought to the mobile page with header "<Expected Header>"

    Examples:
      | Menu  | Sub Menu     | Link             | Expected Header |
      | Wheel | Wheel Brand  | MB Wheels        | MB WHEELS       |
      | Wheel | Wheel Brand  | Unique Wheels    | UNIQUE          |
      | Wheel | Wheel Brand  | Drag Wheels      | DRAG WHEELS     |
      | Wheel | Wheel Brand  | Konig Wheels     | KONIG           |
      | Wheel | Wheel Brand  | View all         | All Brands      |
      | Wheel | Wheel Style  | Painted Wheels   | Painted         |
      | Wheel | Wheel Style  | Machined Wheels  | Machined        |
      | Wheel | Wheel Style  | Chrome Wheels    | Chrome          |
      | Wheel | Wheel Style  | Mesh Wheels      | Mesh Wheels     |
      | Wheel | Wheel Style  | View all         | Wheels          |
      | Wheel | Vehicle Type | Trailer Wheels   | Trailer         |
      | Wheel | Vehicle Type | Passenger Wheels | Passenger       |
      | Wheel | Vehicle Type | ATV/UTV Wheels   | ATV / UTV       |
      | Wheel | Vehicle Type | Truck Wheels     | Truck           |
      | Wheel | Vehicle Type | View all         | Wheels          |

  @dt
  @dtd
  @bba
  @web
  @5283
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different catagories related to TIRES (ALM#5283) (DT & DTD) - Fitment links
    When I browse to the "<Page>" page with defaults
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I select "2014" from the "Year" dropdown
    And  I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY SIZE" menu option
    And  I select "205" from the "Tire Width" dropdown
    And  I close popup modal

    Examples:
      | Page          |
      | Homepage      |
      | PLP           |
      | PDP           |
      | Shopping Cart |

  @dt
  @web
  @5283
  @verifyTireFitmentLinks
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different catagories related to TIRES (ALM#5283) (DT) - Fitment links
    When I browse to the "<Page>" page with defaults
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I select "2014" from the "Year" dropdown
    And  I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY SIZE" menu option
    And  I select "205" from the "Tire Width" dropdown
    And  I close popup modal

    Examples:
      | Page        |
      | Appointment |

  @dtd
  @web
  @5283
  @bba
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different catagories related to TIRES (ALM#5283) (DTD) - Fitment links
    When I browse to the "<Page>" page with defaults
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I select "2014" from the "Year" dropdown
    And  I close popup modal
    And  I open the "TIRES" navigation link
    And  I click the "TIRES BY SIZE" menu option
    And  I select "205" from the "Tire Width" dropdown
    And  I close popup modal

    Examples:
      | Page               |
      | Order Confirmation |

  @dt
  @web
  @5283
  @verifyTireCenterSectionLinks
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different catagories related to TIRES (ALM#5283) (DT) - Center section links
  """TODO - Data only works in STG as page headers differ between QA and STG;"""
    When I browse to the "<Page>" page with defaults
    And  I open the "TIRES" navigation link
    And  I click the "<Tire Link>" menu option
    Then I am brought to the page with path "<Expected Path>"
    And  I am brought to the page with header "<Expected Header>"

    Examples:
      | Page          | Tire Link         | Expected Path            | Expected Header  |
      | Homepage      | Michelin Tires    | /tires/brands/michelin   | Michelin Tires   |
      | PLP           | Falken Tires      | /tires/brands/falken     | Falken Tires     |
      | PDP           | Goodyear Tires    | /tires/brands/goodyear   | Goodyear Tires   |
      | Shopping Cart | BFGoodrich Tires  | /tires/brands/bfgoodrich | BFGoodrich Tires |
      | Appointment   | All-Season tires  | /tires/all-season        | All-Season       |
      | PDP           | Performance Tires | /tires/performance       | Performance      |
      | PLP           | All-Terrain Tires | /tires/all-terrain       | All-Terrain      |
      | Appointment   | Winter Tires      | /tires/winter            | Winter           |
      | Homepage      | ATV/UTV Tires     | /tires/atv-utv           | ATV / UTV        |
      | Shopping Cart | Passenger Tires   | /tires/passenger         | Passenger        |
      | Homepage      | Trailer Tires     | /tires/trailer           | Trailer          |
      | Appointment   | Truck Tires       | /tires/truck             | Truck            |

  @dtd
  @web
  @bba
  @5283
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different catagories related to TIRES (ALM#5283) (DTD) - Center section links
  """TODO - Data only works in STG as page headers differ between QA and STG;"""
    When I browse to the "<Page>" page with defaults
    And  I open the "TIRES" navigation link
    And  I click the "<Tire Link>" menu option
    Then I am brought to the page with path "<Expected Path>"
    And  I am brought to the page with header "<Expected Header>"

    Examples:
      | Page               | Tire Link         | Expected Path         | Expected Header |
      | Homepage           | Cooper Tires      | /tires/brands/cooper  | Cooper Tires    |
      | PLP                | Nitto Tires       | /tires/brands/nitto   | Nitto Tires     |
      | PDP                | Hankook Tires     | /tires/brands/hankook | Hankook Tires   |
      | Shopping Cart      | Falken Tires      | /tires/brands/falken  | Falken Tires    |
      | PDP                | Performance Tires | /tires/performance    | Performance     |
      | PLP                | All-Terrain Tires | /tires/all-terrain    | All-Terrain     |
      | Order Confirmation | Winter Tires      | /tires/winter         | Winter          |
      | Homepage           | ATV/UTV Tires     | /tires/atv-utv        | ATV / UTV       |
      | Shopping Cart      | Passenger Tires   | /tires/passenger      | Passenger       |
      | Order Confirmation | Trailer Tires     | /tires/trailer        | Trailer         |

  @dt
  @dtd
  @web
  @5283
  @bba
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate different catagories related to TIRES (ALM#5283) (DT & DTD) - Center section View All links
  """TODO: View All link is not showing up for Tires - Vehicle Type in QA. Working with Web Team to get it resolved"""
    When I browse to the "<Page>" page with defaults
    And  I open the "TIRES" navigation link
    And  I click the "<Tire Section>" View All link in the header
    Then I am brought to the page with path "<Expected Path>"
    And  I am brought to the page with header "<Expected Header>"

    Examples:
      | Page          | Tire Section | Expected Path | Expected Header |
      | Homepage      | Tire Brand   | /tires/brands | All Brands      |
      | Shopping Cart | Tire Type    | /tires        | Tires           |
      | PLP           | Vehicle Type | /tires        | Tires           |

  @at
  @dt
  @dtd
  @web
  @5524
  @5308
  @bba
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate logo for American tire (ALM #5524)
    When I browse to the "<Page>" page with defaults
    Then I verify the site logo
    When I click the discount tire logo
    Then I am brought to the homepage
    And  I verify the site logo

    Examples:
      | Page          |
      | Homepage      |
      | PLP           |
      | PDP           |
      | Shopping Cart |
      | Appointment   |

  @at
  @dt
  @web
  @5524
  @5308
  @validateLogoAppointment
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate logo for American tire (ALM #5524)
    When I browse to the "<Page>" page with defaults
    Then I verify the site logo
    When I click the discount tire logo
    Then I am brought to the homepage

    Examples:
      | Page        |
      | Appointment |

  @dtd
  @web
  @5524
  @5308
  @bba
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate logo for American tire (ALM #5524)
    When I browse to the "<Page>" page with defaults
    Then I verify the site logo
    When I click the discount tire logo
    Then I am brought to the homepage

    Examples:
      | Page               |
      | Checkout           |
      | Order Confirmation |

  @dt
  @at
  @9703
  @verifyMobileShopByVehicle
  @mobile
  Scenario Outline: Hybris_Mobile_Fitment_Component_ShopBy_Vehicle_TC01 (ALM#9703)
  """TODO: This fails in iOS 10 due to Appium bug 7868"""
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I go to the homepage
    Then I verify that the "Search" button is disabled
    And  I verify that the "Search" button color is "grey"
    When I click the disabled dropdown with value "Make"
    Then I verify that the dropdown menu "has not" expanded
    When I select "<Year2>" from the "Year" dropdown
    And  I type "H" in the "Make" dropdown
    Then I verify that the dropdown is limited to values that start with "H"
    When I select "<Make2>" from the "Make" dropdown
    And  I select "<Model2>" from the "Model" dropdown
    And  I select "<Trim2>" from the "Trim" dropdown
    Then I verify that the Search button is enabled
    And  I verify that the "Search" button color is "red"
    When I click the "Year" dropdown
    Then I verify that the dropdown menu "has" expanded
    And  I verify that the "<Year2>" value in the dropdown is selected
    When I click the "Year" dropdown
    And  I click the "Make" dropdown
    Then I verify that the dropdown menu "has" expanded
    And  I verify that the "<Make2>" value in the dropdown is selected
    When I click the "Make" dropdown
    And  I click the "Model" dropdown
    Then I verify that the dropdown menu "has" expanded
    And  I verify that the "<Model2>" value in the dropdown is selected
    When I click the "Model" dropdown
    And  I click the "Trim" dropdown
    Then I verify that the dropdown menu "has" expanded
    And  I verify that the "<Trim2>" value in the dropdown is selected
    When I click the "Trim" dropdown
    And  I click on the "Find Products" button
    Then I should see the fitment panel with vehicle "<Year2> <Make2>"
    When I go to the homepage
    And  I click the mobile homepage menu
    And  I select the fitment vehicle "<Year1> <Make1>"
    Then I should see the fitment panel with vehicle "<Year1> <Make1>"

    Examples:
      | StoreUrl                  | Year1 | Make1  | Model1 | Trim1     | Year2 | Make2 | Model2 | Trim2    |
      | /store/il/oak-lawn/s/1702 | 2014  | Toyota | Camry  | Hybrid SE | 2015  | Honda | Accord | Coupe EX |

  @dt
  @at
  @9705
  @verifyMobileShopByBrandTire
  @mobile
  Scenario Outline: Hybris_Mobile_Fitment_Component_ShopBy_Brand_Tire_TC03 (ALM#9705)
  """TODO: This fails in iOS 10 due to Appium bug 7868"""
    When I change to the default store
    And  I click the discount tire logo
    And  I go to the homepage
    Then I verify that the "Search" button is disabled
    And  I verify that the "Search" button color is "grey"
    When I navigate to Shop By "Brand"
    And  I click on the "Tires" link
    Then I verify that the "Tires" radio button is "enabled"
    And  I verify that the "Wheels" radio button is "disabled"
    When I type "MI" in the "brand" dropdown
    Then I verify that the items in the drop down display in alphabetically order
    When I select "<Brand>" from the expanded dropdown
    Then I verify that a dropdown has the value "<Brand>"
    And  I verify that the "Search" button color is "red"
    When I click on the "Find Tires" button
    Then I am brought to the page with header "<Brand>"
    When I navigate to Shop By "Brand"
    And  I click on the "View all brands" link
    Then I am brought to the page with header "All Brands"

    Examples:
      | Brand    |
      | michelin |

  @dt
  @9715
  @verifyMobileShopByVehicleStaggered
  @mobile
  Scenario Outline: Hybris_Mobile_Fitment_Component_ShopBy_Vehicle_Staggered (ALM#9715)
    When I go to the homepage
    Then I verify that "Year" dropdown arrow is pointing "downwards"
    When I click the disabled dropdown with value "Make"
    Then I verify that the dropdown menu "has not" expanded
    And  I verify that the "Search" button is disabled
    And  I verify that the "Search" button color is "grey"
    When I click the "Year" dropdown
    Then I verify that "Year" dropdown arrow is pointing "upwards"
    When I select "<Year>" from the "Year" dropdown
    Then I verify that the "<Year>" value in the dropdown is selected
    And  I verify that "Make" dropdown arrow is pointing "upwards"
    When I select "<Make>" from the "Make" dropdown
    Then I verify that the "<Make>" value in the dropdown is selected
    And  I verify that "Make" dropdown arrow is pointing "downwards"
    And  I verify that "Model" dropdown arrow is pointing "upwards"
    When I select "<Model>" from the "Model" dropdown
    Then I verify that the "<Model>" value in the dropdown is selected
    And  I verify that "Model" dropdown arrow is pointing "downwards"
    And  I verify that "Trim" dropdown arrow is pointing "upwards"
    When I select "<Trim>" from the "Trim" dropdown
    Then I verify that the "<Trim>" value in the dropdown is selected
    And  I verify that "Trim" dropdown arrow is pointing "downwards"
    When I click on the "Select Assembly" button
    And  I click the "Assembly" dropdown
    Then I verify that "Assembly" dropdown arrow is pointing "upwards"
    When I select "<Assembly>" from the "Assembly" dropdown
    Then I verify that "Assembly" dropdown arrow is pointing "downwards"
    And  I verify that the "<Assembly>" value in the dropdown is selected
    And  I verify that the Search button is enabled
    And  I verify that the "Search" button color is "red"
    When I click on the "Find Products" button
    Then I should see the fitment panel with vehicle "<Year> <Make>"

    Examples:
      | Year | Make   | Model | Trim       | Assembly                            |
      | 2010 | Nissan | 370Z  | Coupe Base | F 245 /40 R19 SL - R 275 /35 R19 SL |

  @dt
  @9706
  @mobile
  Scenario Outline: Hybris_Mobile_Fitment Modal_Menu_Shop By Vehicle_TC01 (ALM#9706)
  """TODO: This fails in iOS 10 due to Appium bug 7868"""
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I click the "Add Vehicle" button link
    Then I verify all of the fitment menus are displayed
    When I click the disabled dropdown with value "Make"
    Then I verify that the dropdown menu "has not" expanded
    And  I verify that the "Search" button is disabled
    And  I verify that the "Search" button color is "grey"
    When I click the "Year" dropdown
    Then I verify that "Year" dropdown arrow is pointing "upwards"
    When I select "<Year1>" from the "Year" dropdown
    Then I verify that the "<Year1>" value in the dropdown is selected
    And  I verify that "Make" dropdown arrow is pointing "upwards"
    When I type "T" in the "Make" dropdown
    Then I verify that the dropdown is limited to values that start with "T"
    When I select "<Make1>" from the "Make" dropdown
    Then I verify that the "<Make1>" value in the dropdown is selected
    And  I verify that "Make" dropdown arrow is pointing "downwards"
    And  I verify that "Model" dropdown arrow is pointing "upwards"
    When I select "<Model1>" from the "Model" dropdown
    Then I verify that the "<Model1>" value in the dropdown is selected
    And  I verify that "Model" dropdown arrow is pointing "downwards"
    And  I verify that "Trim" dropdown arrow is pointing "upwards"
    When I select "<Trim1>" from the "Trim" dropdown
    Then I verify that the "<Trim1>" value in the dropdown is selected
    And  I verify that "Trim" dropdown arrow is pointing "downwards"
    And  I verify that the Search button is enabled
    And  I verify that the "Search" button color is "red"
    When I click on the "Find Products" button
    Then I should see the fitment panel with vehicle "<Year1> <Make1>"
    When I go to the homepage
    And  I click the mobile homepage menu
    Then I verify that My Vehicles displays "<Year1> <Make1>" as the current vehicle
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "none"
    And  I go to the homepage
    And  I click the mobile homepage menu
    Then I verify that My Vehicles displays "<Year2> <Make2>" as the current vehicle
    When I open the My Vehicles popup
    Then I should see the fitment panel with vehicle "<Year1> <Make1>"
    When I go to the homepage
    And  I click the mobile homepage menu
    Then I verify that My Vehicles displays "<Year1> <Make1>" as the current vehicle
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year3>" "<Make3>" "<Model3>" "<Trim3>" "none"
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year4>" "<Make4>" "<Model4>" "<Trim4>" "none"
    And  I go to the homepage
    And  I click the mobile homepage menu
    Then I verify that My Vehicles displays the vehicle limit reached message

    Examples:
      | Year1 | Make1  | Model1 | Trim1     | Year2 | Make2 | Model2 | Trim2    | Year3 | Make3     | Model3   | Trim3 | Year4 | Make4 | Model4 | Trim4    |
      | 2014  | Toyota | Camry  | Hybrid SE | 2015  | Honda | Accord | Coupe EX | 2010  | Chevrolet | Corvette | Base  | 2012  | Honda | Civic  | Coupe DX |

  @dt
  @at
  @footerValidationWithoutBack
  @web
  Scenario: Verify working links in the footer
  """TODO: ALM NUMBER PENDING, fails do to Defect ID 5629 - Wheel vs. Wheels link text
    TODO: Verify if this is ever coming back. It existed
    when I select "footer" "Promotions"
    then I am brought to the page with header "Active Promotions""""
    When I select "footer" "Tire Search"
    Then I am brought to the page with header "Tires"
    When I select "footer" "Wheel Search"
    Then I am brought to the page with header "Wheels"
    When I select "footer" "Services"
    Then I am brought to the page with header "Wheel and Tire Services"
    When I select "footer" "Customer Care"
    Then I am brought to the page with header "Customer Service"
    When I select "footer" "Store Locator"
    Then I am brought to the page with header "Store Locator"
    When I select "footer" "Appointments"
    Then I am brought to the page with header "Schedule appointment"
    When I select "footer" "Return Policy"
    Then I am brought to the page with header "Our Return Policy"
    When I select "footer" "Tire Safety"
    Then I am brought to the page with header "Tire Safety"
    When I select "footer" "Tire Size Calculator"
    Then I am brought to the page with header "Tire Size and Conversion Calculator"
    When I select "footer" "Check Tire Pressure"
    Then I verify the window with header "HOW TO CHECK TIRE PRESSURE" is displayed
    When I select "footer" "More Topics..."
    Then I am brought to the page with header "Tips & Guides"
    When I select "footer" "Site Map"
    Then I am brought to the page with header "Site Map"

  @dt
  @at
  @footerWithBack
  @web
  Scenario: Verify working links in the footer - requires navigation 'back'
  """TODO: ALM NUMBER PENDING"""
    When I select "footer" "Apply Now"
    Then I verify "Please Read Before Applying" appears on the Apply Now page
    When I go to the homepage
    And  I select "footer" "Commercial Payments"
    Then I verify "Welcome to E-Bill Express!" appears on the Commercial Payment page

  @dt
  @at
  @footerWithAdditionalLinks
  @web
  Scenario: Verify working links in the footer - and internal page links
  """TODO: ALM NUMBER PENDING
    TODO: About US Content is missing in QA & STG env since 2 weeks
    TODO: Enable the validation step once content available otherwise it's causing cascading failures
    then I am brought to the page with header 'About Discount Tire'"""
    When I select "footer" "About Us"
    And  I select "footer" "Contact Us"
    Then I am brought to the page with header "Contact Us"
    When I select "footer" "Motorsports"
    Then I am brought to the page with header "Discount Tire Motorsports"
    When I select "footer" "Careers"
    Then I am brought to the page with header "Careers at Discount Tire"
    When I select "footer" "Regional Offices"
    Then I am brought to the page with header "Regional Offices"
    When I select "footer" "Our Story"
    Then I am brought to the page with header "Our Story: How We Started"
    When I click on the "Contact Us" link
    Then I am brought to the page with header "Contact Us"
    When I click on the "Motorsports" link
    Then I am brought to the page with header "Discount Tire Motorsports"
    When I click on the "Careers" link
    Then I am brought to the page with header "Careers at Discount Tire"
    When I click on the "Our Story" link
    Then I am brought to the page with header "Our Story: How We Started"
    When I click on the "Regional Offices" link
    Then I am brought to the page with header "Regional Offices"
    When I click on the "Careers" link
    Then I am brought to the page with header "Careers at Discount Tire"
    When I click on the "Career FAQs" link
    Then I am brought to the page with header "Q&A: Working at Discount Tire"

  @dt
  @at
  @contactUs
  @web
  Scenario: Verify elements and working links on the Contact Us page
  """TODO: ALM NUMBER PENDING"""
    When I select "footer" "Contact Us"
    Then I am brought to the page with header "Contact Us"
    And  I verify the window with header "EMAIL" is displayed
    And  I verify the window with header "REGIONAL OFFICES" is displayed
    And  I verify the window with header "SOCIAL MEDIA" is displayed
    When I click on the "Regional Offices" link
    Then I am brought to the page with header "Regional Offices"
    When I click on the "Contact Us" link
    Then I am brought to the page with header "Contact Us"
    When I click on the "Motorsports" link
    Then I am brought to the page with header "Discount Tire Motorsports"
    When I click on the "Careers" link
    Then I am brought to the page with header "Careers at Discount Tire"
    When I click on the "Our Story" link
    Then I am brought to the page with header "Our Story: How We Started"
    When I click on the "Regional Offices" link
    Then I am brought to the page with header "Regional Offices"
    When I click on the "Careers" link
    Then I am brought to the page with header "Careers at Discount Tire"
    When I click on the "Career FAQs" link
    Then I am brought to the page with header "Q&A: Working at Discount Tire"

  @dt
  @dtd
  @web
  @5287
  @bba
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate link for Tips and Guide (DT & DTD) (ALM #5287)
    When I browse to the "<Page>" page with defaults
    And  I open the "TIPS & GUIDES" navigation link
    Then I am brought to the page with header "Tips & Guides"

    Examples:
      | Page          |
      | Homepage      |
      | PLP           |
      | PDP           |
      | Shopping Cart |

  @dt
  @web
  @5287
  @tipsAndGuidesNavigationLinkAppointment
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate link for Tips and Guide (DT & DTD) (ALM #5287)
    When I browse to the "<Page>" page with defaults
    And  I open the "TIPS & GUIDES" navigation link
    Then I am brought to the page with header "Tips & Guides"

    Examples:
      | Page        |
      | Appointment |

  @dtd
  @web
  @5287
  @bba
  @UiUxBBA
  Scenario Outline: HYBRIS_GLOBAL AND NAVIGATION_HEADER_Validate link for Tips and Guide (DT & DTD) (ALM #5287)
    When I browse to the "<Page>" page with defaults
    And  I open the "TIPS & GUIDES" navigation link
    Then I am brought to the page with header "Tips & Guides"

    Examples:
      | Page               |
      | Order Confirmation |

  @dt
  @at
  @9678
  @checkAvailabilityHideInStock
  @web
  Scenario Outline: Check Product Availability Hide InStock (ALM #9678)
  """18012 used for DT + AT. 78157 used for STG"""
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I select the "Check nearby stores" link for item "<ItemCode>" on "PDP" page
    Then I should verify that the Check Availability popup loaded
    And  I should verify that In Stock Label is not displayed on Check Inventory popup
    And  I should verify that default store MY STORE label is at top and visible
    When I close the Check Availability popup
    Then I should see product detail page with "<ProductName>"

    Examples:
      | ProductName   | ItemCode |
      | Sincera SN828 | 18012    |
      | Solution      | 78157    |

  @dt
  @at
  @dtd
  @bba
  @9791
  @9802
  @checkAvailabilityChangeMessage
  @mobile
  @UiUxBBA
  Scenario Outline: Hybris_Mobile_Check_Availability_Page_Functionality_PLP_FreeText Search_TC01 (ALM#9791)
    When I do a free text search for "<ItemCode>"
    And  I select the "Check nearby stores" link for item "<ItemCode>" on "PDP" page
    Then I should verify that the Check Availability popup loaded
    And  I should verify that the Check Inventory headline is "Check Availability"
    And  I verify "Product" image is displayed on "Check Inventory"
    And  I should verify that the Product Info shows "<Brand>", "<Name>", and "<ProductName>" in "Check Availability"
    And  I should verify price is not blank
    When I enter a quantity of "100"
    Then I should verify that a "quantity" error message is displayed
    And  I should verify that the Add To Cart button is disabled
    When I enter a quantity of "F"
    Then I should verify that a "validity" error message is displayed
    And  I should verify that the Add To Cart button is disabled
    When I enter a quantity of "<Quantity>"
    Then I should verify that the show quantity filter is displayed with "<Quantity>"
    When I enter a zipcode of "1234567"
    Then I should verify that a zip code error message is displayed
    When I enter a zipcode of "df787"
    Then I should verify that a zip code error message is displayed
    When I enter a zipcode of "<Zipcode>"
    And  I click go and wait for results to load
    Then the first store listed should be within "75" miles
    When I click the "In stock" filter
    Then I should verify the first store has stock quantity greater than "0"
    When I click the "Show more" filter
    Then I should verify the first store has stock quantity greater than "<Quantity>"
    When I select store "2" to make my store
    Then I should verify that store "2" is now the current store
    When I close the Check Availability popup
    Then I should see "<ProductName>" on the product list page

    Examples:
      | ItemCode | Brand     | TireSize   | ProductName        | Quantity | Zipcode |
      | 29935    | Arizonian | 195/65 R15 | Silver Edition III | 6        | 85250   |


  @dt
  @at
  @dtd
  @bba
  @web
  @9665
  @mobile
  @UiUxBBA
  @vehiclePresentation
  @vehicleDetailsBanner
  Scenario Outline: HYBRIS_UI_CONTENT_PLP Vehicle Presentation banner and fit indicator for Tires with Regular vehicle (ALM#9665)
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "OE Wheel" fitment banner with "<OELabel>" and "<OEWheelSize>"
    When I open the My Vehicles popup
    And  I select "shop tires" link
    And  I verify the fitment box option has a value of "<Size>"
    When I select the "<Size>" fitment box option
    Then I verify the selected fitment box option has a value of "<Size>"
    When I select a fitment option "<TireSize>"
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "<Non-OELabel>" and "<TireSizeBanner>"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE Wheel" fitment banner with "<Non-OELabel>" and "<Size>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | TireSize  | Size | TireSizeBanner | OELabel | OETireSize | OEWheelSize | Non-OELabel |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | 215/45-18 | 18   | 215/45-R18     | O.E     | 195/65-R15 | 15          | +3          |

  @dt
  @at
  @dtd
  @web
  @bba
  @9666
  @mobile
  @UiUxBBA
  @vehiclePresentation
  @vehicleDetailsBanner
  Scenario Outline: HYBRIS_UI_CONTENT_PLP Vehicle Presentation vehicle details banner and fit indicator for  Wheels with Regular vehicle (ALM#9666)
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "OE Wheel" fitment banner with "<OELabel>" and "<OEWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Regular" Vehicle Details Banner on "PLP" contains "Non-OE Wheel" fitment banner with "<Non-OELabel>" and "<Size>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Size | OELabel | OETireSize | OEWheelSize | Non-OELabel |
      | 2012 | Honda | Civic | Coupe DX | none     | wheel         | 18   | O.E     | 195/65-R15 | 15          | +3          |


  @dt
  @at
  @navigationPageUrlsAndBreadcrumbs
  @web
  Scenario Outline: Extra wheel and tire page URL path has a response code under 400 and expected breadcrumbs exist
    When I request the URL with "<Path>"
    Then I should get a response below 400
    And the breadcrumbs "<Breadcrumbs>" exist for path "<Path>"

    Examples:
      | Path                       | Breadcrumbs                        |
      | /tires/all-season-catalog  | Home, Tires, All-Season, Products  |
      | /tires/all-terrain-catalog | Home, Tires, All-Terrain, Products |
      | /tires/winter-catalog      | Home, Tires, Winter, Products      |
      | /tires/performance-catalog | Home, Tires, Performance, Products |
      | /tires/truck-catalog       | Home, Tires, Truck, Products       |
      | /tires/atv-utv-catalog     | Home, Tires, ATV / UTV, Products   |
      | /tires/trailer-catalog     | Home, Tires, Trailer, Products     |
      | /tires/passenger-catalog   | Home, Tires, Passenger, Products   |
      | /wheels/chrome-catalog     | Home, Wheels, Chrome, Products     |
      | /wheels/painted-catalog    | Home, Wheels, Painted, Products    |
      | /wheels/machined-catalog   | Home, Wheels, Machined, Products   |
      | /wheels/mesh-catalog       | Home, Wheels, Mesh, Products       |
      | /wheels/truck-catalog      | Home, Wheels, Truck, Products      |
      | /wheels/atv-utv-catalog    | Home, Wheels, ATV / UTV, Products  |
      | /wheels/trailer-catalog    | Home, Wheels, Trailer, Products    |
      | /wheels/passenger-catalog  | Home, Wheels, Passenger, Products  |

  @dt
  @at
  @tipsAndGuidesBreadcrumbs
  @web
  Scenario Outline: Tips & guides page URL path has a response code under 400 and expected breadcrumbs exist
    When I request the URL with "<Path>"
    Then I should get a response below 400
    And the breadcrumbs "<Breadcrumbs>" exist for path "<Path>"

    Examples:
      | Path                             | Breadcrumbs                                         |
      | /learn                           | Home, Tips & Guides                                 |
      | /learn/air-pressure-fuel-economy | Home, Tips & Guides, Air Pressure & Fuel Efficiency |
      | /learn/buying-tires              | Home, Tips & Guides, Choosing the Right Tire        |
      | /learn/tire-pressure             | Home, Tips & Guides, Correct Air Pressure           |
      | /learn/tire-types                | Home, Tips & Guides, Types of Tires                 |
      | /learn/tire-repair               | Home, Tips & Guides, Tire Repair Guidelines         |
      | /learn/load-range-load-index     | Home, Tips & Guides, Load Range & Load Index        |
      | /learn/plus-sizing               | Home, Tips & Guides, Plus Sizing Tires & Wheels     |
      | /learn/tire-balancing            | Home, Tips & Guides, Tire Balancing                 |
      | /learn/tire-rotations            | Home, Tips & Guides, Tire Rotations                 |
      | /learn/reading-tire-sidewall     | Home, Tips & Guides, Reading Tire Sidewall          |
      | /learn/replacing-2tires          | Home, Tips & Guides, Replacing Two Tires            |
      | /learn/road-force-balancing      | Home, Tips & Guides, Road Force Balancing           |
      | /learn/sidewall-inspection       | Home, Tips & Guides, Sidewall Inspection            |
      | /learn/speed-rating              | Home, Tips & Guides, Speed Ratings                  |
      | /learn/stopping-distance         | Home, Tips & Guides, Stopping Distance              |
      | /learn/tire-aging                | Home, Tips & Guides, The Life of a Tire             |
      | /learn/tire-construction         | Home, Tips & Guides, Tire Construction              |
      | /learn/tire-dimensions           | Home, Tips & Guides, Tire Dimensions                |
      | /learn/tire-ratings              | Home, Tips & Guides, Tire Ratings                   |
      | /learn/tire-safety               | Home, Tips & Guides, Tire Safety                    |
      | /learn/tire-sipes                | Home, Tips & Guides, Tire Sipes                     |
      | /learn/tire-size-calculator      | Home, Tips & Guides, Tire Size Calculator           |
      | /learn/tire-tread-depth          | Home, Tips & Guides, Measuring Tread Depth          |
      | /learn/tires-below-45            | Home, Tips & Guides, Tires Below 45 Degrees         |
      | /learn/trailer-tire-faqs         | Home, Tips & Guides, Trailer Tire FAQs              |
      | /learn/utqg                      | Home, Tips & Guides, UTQG                           |
      | /learn/winter-tire-changeover    | Home, Tips & Guides, Winter Tire Changeover         |
      | /learn/winter-tire-faqs          | Home, Tips & Guides, Winter Tire FAQs               |
      | /learn/change-a-tire             | Home, Tips & Guides, Changing a Tire                |
      | /learn/check-tire-pressure       | Home, Tips & Guides, Checking Air Pressure          |
      | /learn/clean-tires               | Home, Tips & Guides, Cleaning Tires                 |
      | /learn/clean-wheels              | Home, Tips & Guides, Cleaning Wheels                |
      | /learn/bolt-pattern              | Home, Tips & Guides, Bolt Patterns                  |
      | /learn/wheel-alignment           | Home, Tips & Guides, Wheel Alignment                |
      | /learn/wheel-construction        | Home, Tips & Guides, Wheel Construction             |
      | /learn/offset-backspace          | Home, Tips & Guides, Wheel Offset vs. Backspacing   |
      | /learn/wheel-size                | Home, Tips & Guides, Wheel Size                     |
      | /learn/wheel-torque              | Home, Tips & Guides, Wheel Torque                   |
      | /learn/fuel-calculator           | Home, Tips & Guides, Fuel Calculator                |
      | /learn/tire-glossary             | Home, Tips & Guides, Glossary                       |
      | /learn/tpms-facts                | Home, Tips & Guides, TPMS Facts                     |
      | /learn/tpms-rebuild-kits         | Home, Tips & Guides, TPMS Rebuild Kits              |

  @bba
  @scheduleAppointmentCustomerInfoPageUrlValidation
  @web
  @mobile
  @UiUxBBA
  Scenario Outline: Verify element and breadcrumbs on Schedule Appointment Customer Info page
    When I schedule an appointment for my current store
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I click continue for appointment customer details page
    And  I select "Schedule Appointment" after entering customer information for "<Customer>"
    Then I verify the "<Breadcrumbs>" link in the breadcrumb container
    And  I should see text "<ElementText>" present in the page source

    Examples:
      | ServiceOptions                | Customer            | ElementText             | Breadcrumbs                     |
      | New Tires/Wheels Consultation | DEFAULT_CUSTOMER_AZ | Your appointment number | Home, Appointment, Confirmation |

  @bba
  @tireUrlsAndBreadcrumbs
  @web
  @UiUxBBA
  Scenario Outline: Verify Tire breadcrumbs are present and Url's are correct
  """TODO: Currently failes in QA due to Defect ID 6270"""
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY BRAND" menu option
    Then I am brought to the page with path "<Path>"
    And  I verify the "<Breadcrumbs>" link in the breadcrumb container

    Examples:
      | Path          | Breadcrumbs             |
      | /tires/brands | Home, Tires, All Brands |

  @bba
  @tireUrlsAndBreadcrumbs
  @mobile
  @UiUxBBA
  Scenario Outline: Mobile - Verify Tire breadcrumbs are present and Url's are correct
    When I click the mobile homepage menu
    And  I click on "Tire" menu link
    And  I click on "Tire Brand" menu link
    And  I click on "Michelin Tires" menu link
    Then I verify the "<Breadcrumbs>" link in the breadcrumb container

    Examples:
      | Breadcrumbs                       |
      | Home, Tires, All Brands, Michelin |

  @bba
  @wheelsUrlsAndBreadcrumbs
  @web
  @UiUxBBA
  Scenario Outline: Verify Wheel breadcrumbs are present and Url's are correct
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    Then I am brought to the page with path "<Path>"
    And  I verify the "<Breadcrumbs>" link in the breadcrumb container

    Examples:
      | Path           | Breadcrumbs              |
      | /wheels/brands | Home, Wheels, All Brands |

  @bba
  @wheelsUrlsAndBreadcrumbs
  @mobile
  @UiUxBBA
  Scenario Outline: Mobile - Verify Wheel breadcrumbs are present and Url's are correct
    When I click the mobile homepage menu
    And  I click on "Wheel" menu link
    And  I click on "Wheel Brand" menu link
    And  I click on "View all" menu link
    Then I verify the "<Breadcrumbs>" link in the breadcrumb container

    Examples:
      | Breadcrumbs              |
      | Home, Wheels, All Brands |

  @bba
  @serviceUrlAndBreadcrumbs
  @web
  @UiUxBBA
  Scenario Outline: Verify Services page breadcrumbs are present and Url's are correct
    When I open the "SERVICES" navigation link
    And  I click on the "SCHEDULE APPOINTMENT" link
    Then I verify the current URL contains "<Path>"
    And  I verify the "<Breadcrumbs>" link in the breadcrumb container

    Examples:
      | Path                  | Breadcrumbs                 |
      | /schedule-appointment | Home, Appointment, Services |

  @bba
  @serviceUrlAndBreadcrumbs
  @mobile
  @UiUxBBA
  Scenario Outline: Mobile - Verify Services page breadcrumbs are present and Url's are correct
    When I click on "APPOINTMENTS" header link
    And  I click on the "SCHEDULE APPOINTMENT" link
    Then I verify the "<Breadcrumbs>" link in the breadcrumb container

    Examples:
      | Breadcrumbs                 |
      | Home, Appointment, Services |

  @bba
  @footerUrlsAndBreadcrumbs
  @web
  @UiUxBBA
  Scenario Outline: Verify About Us breadcrumbs are present and Url's are correct
  """TODO: Careers page got redesigned with new branding and No Breadcrumbs
    | Careers         | /careers                           | Home, Careers                      |"""
    When I select "footer" "<Link>"
    Then I verify the current URL contains "<Path>"
    And  I verify the "<Breadcrumbs>" link in the breadcrumb container

    Examples:
      | Link             | Path                               | Breadcrumbs                        |
      | About Us         | /about-us                          | Home, About Us                     |
      | Our Story        | /about-us/our-story                | Home, About Us, Our Story          |
      | Motorsports      | /about-us/motorsports              | Home, About Us, Motorsports        |
      | Customer Care    | /customer-service                  | Home, Customer Care                |
      | Store Locator    | /store-locator                     | Home, Store Locator                |
      | Appointments     | /schedule-appointment              | Home, Appointment, Services        |
      | Return Policy    | /customer-service/return-policy    | Home, Customer Care, Return Policy |
      | Regional Offices | /customer-service/regional-offices | Home, About Us, Regional Offices   |
      | Contact Us       | /customer-service/contact-us       | Home, About Us, Contact Us         |
      | Services         | /services                          | Home, Services                     |

  @dt
  @at
  @tireSizeCalcValidateDisplay
  @web
  Scenario: Tire Size Calc validate default display
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Size Calculator"
    Then I verify Tire Size calculator controls and expected sections are displayed

  @dt
  @at
  @tireSizeCalcForCommonSizeUpdatesResultsTable
  @web
  Scenario Outline: Tire Size Calc for common tire size updates the results table
  """TODO: Tire size calculator not updating with IE"""
    When I select "footer" "Tire Size Calculator"
    Then I am brought to the page with header "Tire Size and Conversion Calculator"
    When I enter "current" tire size of "<CurrentTireDiameter>" / "<CurrentTireWidth>" R "<CurrentWheelDiameter>" into the Tire Size Calculator
    Then I verify the Tire Size Calculator results table is updated
    And  I verify the Side by Side Comparison section is updated with a size of "<CurrentTireDiameter>" / "<CurrentTireWidth>" R "<CurrentWheelDiameter>"
    When I enter "new" tire size of "<NewTireDiameter>" / "<NewTireWidth>" R "<NewWheelDiameter>" into the Tire Size Calculator
    Then I verify the Tire Size Calculator results table is updated
    And  I verify the Side by Side Comparison section is updated with a size of "<NewTireDiameter>" / "<NewTireWidth>" R "<NewWheelDiameter>"

    Examples:
      | CurrentTireDiameter | CurrentTireWidth | CurrentWheelDiameter | NewTireDiameter | NewTireWidth | NewWheelDiameter |
      | 215                 | 55               | 17                   | 215             | 60           | 16               |
      | 245                 | 75               | 17                   | 245             | 75           | 16               |

  @dt
  @at
  @tireSizeCalcSpeedometerReadingUpdatesWithNewTireSizes
  @web
  Scenario: Tire Size Calc speedometer reads section updates speed with new tire sizes
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Size Calculator"
    And  I enter "current" tire size of "125" / "70" R "16" into the Tire Size Calculator
    And  I enter "new" tire size of "125" / "80" R "16" into the Tire Size Calculator
    And  I enter a speed of "70" as the speedometer reading
    Then I verify the traveling speed is updated to "73"

  @dt
  @at
  @generalTg
  @web
  Scenario: Verify General section links
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Fuel Calculator"
    Then I am brought to the page with header "Fuel Calculator: The Cost of Low Air Pressure"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Glossary of Terms"
    Then I am brought to the page with header "Tire Glossary"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "TPMS Facts & Troubleshooting"
    Then I am brought to the page with header "Tire Pressure Monitoring Systems (TPMS) Facts"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "TPMS Rebuild Kits"
    Then I am brought to the page with header "TPMS Rebuild Kits"

  @dt
  @at
  @tiresTg
  @web
  Scenario: Verify Tires section links
  """TODO: revisit this when content returns to page (Defect ID 6253)
    then I am brought to the page with header "Correct Air Pressure
    TODO: Verify this list element has been purposefully removed
    and  I open the "TIPS & GUIDES" navigation link
    and  I click the list element "Proper Tire Rotation"
    then I am brought to the page with header 'The Basics of Tire Rotations'"""
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Air Pressure & Fuel Economy"
    Then I am brought to the page with header "Correct Tire Inflation Improves Fuel Economy"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Choosing The Right Tire"
    Then I am brought to the page with header "Choosing the Right Tire"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Air Pressure"
    And  I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Different Types of Tires"
    Then I am brought to the page with header "Different Types of Tires"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Flat Tire Repair"
    Then I am brought to the page with header "Proper Tire Repair"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Load Range vs Load Index"
    Then I am brought to the page with header "Load Range and Load Index"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Plus Sizing Tires and Wheels"
    Then I am brought to the page with header "Plus Sizing Tires & Wheels"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Balancing"
    Then I am brought to the page with header "Tire Balancing"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Reading The Tire Sidewall"
    Then I am brought to the page with header "How to Read a Tire Sidewall"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Replacing Two Tires"
    Then I am brought to the page with header "Replacing Two Tires"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Road Force Balancing"
    Then I am brought to the page with header "Road Force Balancing"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Sidewall Inspection"
    Then I am brought to the page with header "Sidewall Inspection"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Speed Ratings"
    Then I am brought to the page with header "What Are Tire Speed Ratings?"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Stopping Distance"
    Then I am brought to the page with header "Stopping Distance"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "The Life of a Tire"
    Then I am brought to the page with header "The Life of a Tire"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Construction"
    Then I am brought to the page with header "Tire Construction"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Dimensions"
    Then I am brought to the page with header "Tire Dimensions"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Ratings"
    Then I am brought to the page with header "Tire Ratings and Reviews"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Safety"
    Then I am brought to the page with header "Tire Safety"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Sipes"
    Then I am brought to the page with header "Tire Sipes"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Size Calculator"
    Then I am brought to the page with header "Tire Size and Conversion Calculator"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tire Tread Depth"
    Then I am brought to the page with header "Measuring Tread Depth"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Tires Below 45-Degrees"
    Then I am brought to the page with header "Tires Below 45 Degrees"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Trailer Tire FAQs"
    Then I am brought to the page with header "Safely Maintaining Trailer Tires"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "UTQG Standards"
    Then I am brought to the page with header "The UTQG System"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Winter Tire Changeover"
    Then I am brought to the page with header "Winter Tire Changeover & Installation"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Winter Tire FAQs"
    Then I am brought to the page with header "Winter Tire FAQs"

  @dt
  @at
  @wheelsTg
  @web
  Scenario: Verify Wheels section links
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Understanding Bolt Patterns"
    Then I am brought to the page with header "Understanding Bolt Patterns"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Wheel Alignment"
    Then I am brought to the page with header "Wheel Alignment"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Wheel Construction"
    Then I am brought to the page with header "Wheel Construction"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Wheel Offset & Backspacing"
    Then I am brought to the page with header "Understanding Wheel Offset and Backspacing"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Wheel Size"
    Then I am brought to the page with header "Wheel Size Basics"

  @dt
  @at
  @howToGuidesTg
  @web
  Scenario: Verify How-To-Guides section links
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "How to Change a Tire"
    Then I am brought to the page with header "Changing a Tire"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "How to Check Tire Pressure"
    Then I verify the window with header "HOW TO CHECK TIRE PRESSURE" is displayed
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "How To Clean Tires"
    Then I am brought to the page with header "How To Clean Your Tires"
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "How To Clean Wheels"
    Then I am brought to the page with header "Proper Wheel Cleaning"

  @dt
  @at
  @web
  @smokeTest
  @wheelTorqueChart
  Scenario: Verify Wheel Torque Chart brand links
  """TODO: Step is failing due to Defect ID 5648"""
    When I open the "TIPS & GUIDES" navigation link
    And  I click the list element "Wheel Torque Chart"
    Then I am brought to the page with header "Wheel Torque Chart"
    And  I verify the brand links on the Torque Chart page

  @dt
  @at
  @web
  @5279
  @siteMapSectionVerification
  Scenario Outline: Site Map section verification
    When I select "footer" "Site Map"
    Then I am brought to the page with header "Site Map"
    And  I click and verify all the links in the "<Section Name>" section of the Site Map page

    Examples:
      | Section Name      |
      | ABOUT US          |
      | CAREERS           |
      | CONTACT US        |
      | CUSTOMER SERVICES |
      | SERVICES          |
      | LEARN             |
      | TIRES             |
      | TIRE BRANDS       |
      | WHEELS            |
      | WHEEL BRANDS      |

  @dt
  @at
  @8670
  @checkAvailabilityByBrand
  @web
  Scenario Outline: HYBRIS_171_ANALYTICAL AND TAGGING_Check Availability_By Brand (ALM #8670)
  """TODO - This is failing due to defect #10312"""
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY BRAND" menu option
    And  I select the "<Brand>" brand image
    And  I select shop all from the Product Brand page
    And  I select the "In Stock" checkbox
    And  I select the Check Inventory for the first item available
    Then I should verify that the Check Availability popup loaded
    When I close the Check Availability popup
    And  I select the "First" product result image on "PLP" page
    Then I verify "Standard" PDP page is displayed
    When I select the Check Inventory for the first item available
    Then I should verify that the Check Availability popup loaded
    When I close the Check Availability popup
    Then I verify "Standard" PDP page is displayed

    Examples:
      | Brand          |
      | Michelin Tires |

  @dt
  @9231
  @verifyMobileHeaderElements
  @mobile
  Scenario: HYBRIS_HEADER_VALIDATION_Mobile_DT (ALM#9231)
    Given I go to the homepage
    Then I verify all of the mobile header elements are visible

  @dt
  @9709
  @mobileMakeThisMyStoreValidation
  @mobile
  Scenario Outline: Hybris_Mobile_Launching_TCOO (ALM#9707)
    Given I go to the homepage
    When  I search for store within "10" miles of "<Zipcode>"
    And   I select make "<Zipcode>" my store
    Then  I verify the Current Store text color is Blue

    Examples:
      | Zipcode |
      | 60652   |

  @9404
  @dtd
  @web
  @mobile
  @bba
  @UiUxBBA
  Scenario Outline: Hybris_CONTENT_Search_By_Size_Before_PLP_Fitment_Message_Needs_To_Be_Changed (ALM # 9404)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I navigate back to previous page
    And  I select 'Change' on the vehicle fitment panel page
    And  I do a "homepage" vehicle search with details "<NewYear>" "<NewMake>" "<NewModel>" "<NewTrim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<NewYear> <NewMake>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify Vehicle Details Banner on "PLP" contains "<NewYear>" "<NewMake>" "<NewModel>" and "<NewTrim>"

    Examples:
      | Year | Make  | Model  | Trim     | Assembly | FitmentOption | NewYear | NewMake | NewModel | NewTrim   |
      | 2015 | Honda | Accord | Coupe EX | none     | tire          | 2014    | Toyota  | Camry    | Hybrid SE |

  @dt
  @9775
  @mobile
  @plpUiVehicleFitmentNotSelectedTire
  Scenario Outline: Hybris_Mobile_PLP_UI_Vehicle_Fitment_Not_selected_Tire_TC01 (ALM #9775)
    When I go to the homepage
    And  I do a free text search for "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    And  I verify PLP UI "<Aspect>"

    Examples:
      | Aspect                 | ItemCode | ProductName        |
      | banner without vehicle | 29935    | Silver Edition III |
      | basic controls         | 29935    | Silver Edition III |
      | sorting options        | 29935    | Silver Edition III |
      | pagination             | 29935    | Silver Edition III |

  @dt
  @at
  @web
  @9682
  @regression
  @UiUxRegression
  Scenario Outline: PLP In Stock Quick Filter State Check For Tires (ALM #9682)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the "In Stock" checkbox to be "deselected" by default

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          |
      | 2012 | Honda | Civic | Coupe DX | none     | Wheel         |

  @dt
  @at
  @dtd
  @web
  @9227
  @12599
  @regression
  @UiUxRegression
  Scenario Outline: HYBRIS_UI_UI_FIND VEHICLE VALIDATION Desktop DT and DTD(ALM #9227)
    Then I verify the "TIRES" navigation link is displayed
    And  I verify the "WHEELS" navigation link is displayed
    When I navigate to Shop By "Vehicle"
    Then I verify breadcrumbs: "Year, Make, Model, Trim" are displayed on the fitment grid
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I navigate back to previous page
    And  I do a "homepage" "Brand" "tire" search with option: "<Brand>"
    Then I see the "<Brand>" that I selected
    When I select "<SubCategory>" to shop
    Then I can see "<Brand>" PLP page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Brand          | SubCategory      |
      | 2012 | Honda | Civic | Coupe EX | none     | Michelin Tires | All-Season tires |

  @9228
  @12599
  @web
  @dt
  @at
  @dtd
  @regression
  @UiUxRegression
  Scenario Outline: HYBRIS_UI_UI_FOOTER VALIDATION Desktop(ALM #9228, 15299)
    When I browse to the "<Page>" page with defaults
    Then I verify "Tire Search" footer link is displayed
    And  I verify "Wheel Search" footer link is displayed
    And  I verify "Deals And Rebates" footer link is displayed
    And  I verify "Tire Safety" footer link is displayed
    And  I verify "Tire Size Calculator" footer link is displayed
    And  I verify "Check Tire Pressure" footer link is displayed
    And  I verify "More Topics..." footer link is displayed
    And  I verify "Credit" footer link is displayed
    And  I verify "Apply Now" footer link is displayed
    And  I verify "Customer Care" footer link is displayed
    And  I verify "Return Policy" footer link is displayed
    And  I verify "About Us" footer link is displayed
    And  I verify "Contact Us" footer link is displayed
    And  I verify "Commercial Payments" footer link is displayed
    And  I verify "Store Locator" footer link is displayed
    And  I verify "Appointments" footer link is displayed
    And  I verify "Our Story" footer link is displayed
    And  I verify "Motorsports" footer link is displayed
    And  I verify "Careers" footer link is displayed
    And  I verify "Regional Offices" footer link is displayed

    Examples:
      | Page          |
      | Homepage      |
      | Shopping Cart |

  @dt
  @at
  @dtd
  @9346
  @regression
  @UiUxRegression
  @web
  Scenario Outline: UI_Verify page font on Filter search results for In Stock (ALM #9346)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify the "In Stock" checkbox to be "deselected" by default
    When I extract "In Stock" filter font size
    And  I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    Then I verify "In Stock" filter font size value before and after selection

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Header      |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | tire result |

  @dt
  @at
  @9345
  @regression
  @UiUxRegression
  @mobile
  Scenario Outline: HYBRIS_UI_UI_Mobile Verify the Shop by Vehicle functionality (ALM #9345)
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          |

  @dt
  @at
  @dtd
  @web
  @12551
  @12557
  @12560
  @regression
  @UiUxRegression
  Scenario Outline: HYBRIS_UI_GLOBAL AND NAVIGATION_Cart Checkout_GLOBALHEADER_Verify the header in Checkout page (ALM #12551,12557,12560)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify need help option is displayed
    And  I verify the site logo
    When I click on the "<NeedHelp>" link
    Then I verify need help popup values email and phone number are displayed

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | ItemCode | Checkout            | NeedHelp   |
      | 2012 | Honda | CIVIC | COUPE DX | none     | tire          | 29935    | without appointment | Need Help? |

  @dt
  @at
  @web
  @12568
  @12599
  @regression
  @UiUxRegression
  Scenario Outline: HYBRIS_UI_GLOBAL AND NAVIGATION_Cart Checkout_GLOBALHEADER_Verify navigation when Logo in the global header on checkout page is selected (ALM #12568, 15299)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify the site logo
    When I click the discount tire logo
    Then I am brought to the homepage
    And  I verify the "TIRES" navigation link is displayed
    And  I verify the "WHEELS" navigation link is displayed
    And  I verify the "APPOINTMENTS" navigation link is displayed
    And  I verify the "TIPS & GUIDES" navigation link is displayed
    And  I verify the default selection in the fitment component
    And  I verify all of the fitment menus are displayed

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | ItemCode | Checkout            |
      | 2012 | Honda | CIVIC | COUPE DX | none     | tire          | 29935    | without appointment |

  @dt
  @at
  @web
  @12562
  @regression
  @UiUxRegression
  Scenario Outline: HYBRIS_UI_GLOBAL AND NAVIGATION_Cart Checkout_GLOBALFOOTER_Verify the contents in footer displayed in the checkout page (ALM #12562)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify need help option is displayed
    And  I verify the site logo
    And  I verify the Headline Text of footer is displayed
    And  I verify "Return Policy" footer link is displayed
    When I select "footer" "Return Policy"
    And  I navigate to newly opened next tab
    Then I am brought to the page with header "Our Return Policy"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | ItemCode | Checkout            |
      | 2012 | Honda | CIVIC | COUPE DX | none     | tire          | 29935    | without appointment |

  @dt
  @at
  @dtd
  @web
  @15634
  @15635
  @15636
  @regression
  @UiUxRegression
  Scenario Outline: HYBRIS_UI_Finance, Promotions, Tips & guides  links in Header menu (ALM #15634, 15635, 15636)
    When I click the discount tire logo
    Then I am brought to the homepage
    And  I verify the "<Header>" navigation link is displayed
    When I open the "<Header>" navigation link
    Then I verify the "<Breadcrumbs>" link in the breadcrumb container
    And  I verify the current URL contains "<Content>"

    Examples:
      | Header        | Breadcrumbs             | Content                    |
      | FINANCING     | Home, Finance           | customer-service/financing |
      | PROMOTIONS    | Home, Deals and Rebates | promotions                 |
      | TIPS & GUIDES | Home, TIPS & GUIDES     | learn                      |

  @dtd
  @web
  @9230
  @5300
  @mobile
  @nexusTax
  @UiUxFooter
  @UiUxRegression
  Scenario Outline: HYBRIS_UI_UI_Verify Tax Exempt Message throughout the checkout process_DTD (ALM # 9230, 5300)
    When I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify "tax exempt" is displayed on "Shopping cart page"
    When I select "tax exempt" link
    Then I verify the "Tax Policy" modal is displayed
    When I close the tax exempt modal
    Then I verify "Shopping cart" page is displayed
    When I click on the "Checkout Now" button
    And  I click on the "Continue to Checkout" button
    Then I verify "tax exempt" is displayed on "Checkout page"
    When I select "tax exempt" link
    Then I verify the "Tax Policy" modal is displayed
    When I close the tax exempt modal
    Then I verify "Checkout" page is displayed
    When I enter shipping info as "<Customer>"
    And  I click on the "Continue To Shipping Method" button
    Then I verify "tax exempt" is displayed on "Checkout page"
    When I select "tax exempt" link
    Then I verify the "Tax Policy" modal is displayed
    When I close the tax exempt modal
    Then I verify "Checkout" page is displayed
    When I click on the "Continue To Payment" button
    Then I verify "tax exempt" is displayed on "Checkout page"
    When I select "tax exempt" link
    Then I verify the "Tax Policy" modal is displayed
    When I close the tax exempt modal
    Then I verify "Checkout" page is displayed
    When I enter payment info for same billing address with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | ItemCodeA | Customer            | Credit Card |
      | 29935     | default_customer_tx | Visa        |

  @dt
  @at
  @dtd
  @web
  @bba
  @16530
  @mobile
  @UiUxBBA
  @vehiclePresentation
  @vehicleDetailsBanner
  Scenario Outline: HYBRIS_UI_CONTENT_PLP Vehicle Presentation vehicle details banner and fit indicator for Wheels with Staggered vehicle (ALM#16530)
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select the "Front" staggered menu option
    And  I select a fitment option "wheel"
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OERearWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OERearWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE Wheel" fitment banner with "no badge" and "<FrontSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE Wheel" fitment banner with "no badge" and "<RearSize>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | FrontSize | RearSize | OELabel | OEFrontTireSize | OERearTireSize | OEFrontWheelSize | OERearWheelSize |
      | 2010 | Chevrolet | Corvette | Base | none     | 19        | 20       | O.E     | 245/40-R18      | 285/35-R19     | 18               | 19              |

  @dt
  @at
  @dtd
  @web
  @bba
  @16531
  @mobile
  @UiUxBBA
  @vehiclePresentation
  @vehicleDetailsBanner
  Scenario Outline: HYBRIS_UI_CONTENT_PLP Vehicle Presentation vehicle details banner and fit indicator for Tires with Staggered vehicle (ALM#16531)
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OERearWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OERearWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OERearWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE FRONT Wheel" fitment banner with "<Non-OELabel>" and "<FrontSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE REAR Wheel" fitment banner with "<Non-OELabel>" and "<RearSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "no badge" and "<OptionalFrontBanner>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE Wheel" fitment banner with "no badge" and "<FrontSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE Tire" fitment banner with "no badge" and "<OptionalRearBanner>"
    And  I verify "Staggered" Vehicle Details Banner on "PLP" contains "Non-OE Wheel" fitment banner with "no badge" and "<RearSize>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | OELabel | OEFrontTireSize | OERearTireSize | OEFrontWheelSize | OERearWheelSize | Non-OELabel | OptionalFrontTire | OptionalRearTire | FrontSize | RearSize | OptionalFrontBanner | OptionalRearBanner |
      | 2010 | Chevrolet | Corvette | Base | none     | O.E     | 245/40-R18      | 285/35-R19     | 18               | 19              | +1          | 225/35-19         | 245/35-20        | 19        | 20       | 225/35-R19          | 245/35-R20         |

  @dt
  @at
  @dtd
  @web
  @16532
  @mobile
  @vehiclePresentation
  @vehicleDetailsBanner
  Scenario Outline: HYBRIS_UI_CONTENT_PDP Vehicle Presentation vehicle details banner for tires regular vehicles (ALM#16532)
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PDP"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Regular" Vehicle Details Banner on "PDP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify "Regular" Vehicle Details Banner on "PDP" contains "OE Wheel" fitment banner with "<OELabel>" and "<OEWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Regular" Vehicle Details Banner on "PDP" contains "Non-OE Tire" fitment banner with "<Non-OELabel>" and "<TireSizeBanner>"
    And  I verify "Regular" Vehicle Details Banner on "PDP" contains "Non-OE Wheel" fitment banner with "<Non-OELabel>" and "<Size>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | TireSize  | Size | TireSizeBanner | OELabel | OETireSize | OEWheelSize | Non-OELabel |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | 215/45-18 | 18   | 215/45-R18     | O.E     | 195/65-R15 | 15          | +3          |

  @dt
  @at
  @dtd
  @web
  @9022
  @mobile
  @vehiclePresentation
  @vehicleDetailsBanner
  Scenario Outline: HYBRIS_UI_CONTENT_PDP Vehicle Presentation vehicle details banner for tires with staggered vehicles (ALM#9022)
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PDP"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OERearWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PDP"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "FRONT Tire" fitment banner with "no badge" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "FRONT Wheel" fitment banner with "no badge" and "<OEFrontWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PDP"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE Tire" fitment banner with "no badge" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE Wheel" fitment banner with "no badge" and "<OERearWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PDP"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE FRONT Wheel" fitment banner with "<Non-OELabel>" and "<FrontSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE REAR Wheel" fitment banner with "<Non-OELabel>" and "<RearSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE Tire" fitment banner with "no badge" and "<OptionalFrontBanner>"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE Wheel" fitment banner with "no badge" and "<FrontSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE Tire" fitment banner with "no badge" and "<OptionalRearBanner>"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE Wheel" fitment banner with "no badge" and "<RearSize>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | OELabel | OEFrontTireSize | OERearTireSize | OEFrontWheelSize | OERearWheelSize | Non-OELabel | OptionalFrontTire | OptionalRearTire | FrontSize | RearSize | OptionalFrontBanner | OptionalRearBanner |
      | 2010 | Chevrolet | Corvette | Base | none     | O.E     | 245/40-R18      | 285/35-R19     | 18               | 19              | +1          | 225/35-19         | 245/35-20        | 19        | 20       | 225/35-R19          | 245/35-R20         |

  @dt
  @at
  @dtd
  @web
  @9423
  @mobile
  @vehiclePresentation
  @vehicleDetailsBanner
  Scenario Outline: HYBRIS_UI_CONTENT_PDP Vehicle Presentation vehicle details banner for wheels with regular vehicles (ALM#9423)
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PDP"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Regular" Vehicle Details Banner on "PDP" contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify "Regular" Vehicle Details Banner on "PDP" contains "OE Wheel" fitment banner with "<OELabel>" and "<OEWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Regular" Vehicle Details Banner on "PDP" contains "OE Wheel" fitment banner with "<OELabel>" and "<OEWheelSize>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | OELabel | OETireSize | OEWheelSize |
      | 2012 | Honda | Civic | Coupe DX | none     | wheel         | O.E     | 195/65-R15 | 15          |
  @dt
  @at
  @dtd
  @web
  @9021
  @mobile
  @vehiclePresentation
  @vehicleDetailsBanner
  Scenario Outline: HYBRIS_UI_CONTENT_PDP Vehicle Presentation vehicle details banner for wheels with staggered vehicles (ALM#9021)
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select the "Front" staggered menu option
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PDP"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "FRONT Tire" fitment banner with "no badge" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "FRONT Wheel" fitment banner with "no badge" and "<OEFrontWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PDP"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE Wheel" fitment banner with "no badge" and "<OERearWheelSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE Wheel" fitment banner with "no badge" and "<FrontSize>"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify Vehicle Details Banner on "PDP" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "PDP" contains "Non-OE Wheel" fitment banner with "no badge" and "<RearSize>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | FrontSize | RearSize | OEFrontTireSize | OEFrontWheelSize | OERearWheelSize |
      | 2010 | Chevrolet | Corvette | Base | none     | 19        | 20       | 245/40-R18      | 18               | 19              |

  @dt
  @web
  @17796
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_grouping_of_PDP_grouping_products
  Scenario Outline: Verify canonical pdp grouping of products  (ALM#17796)
    When I open the "<Category>" navigation link
    And  I click the "<Menu Option>" menu option
    And  I select "<Brand Link>" from the Product Brand page
    Then I verify PLP results are grouped by brand "<Brand>"
    When I select the first available View Details from the results page
    Then I verify "Canonical" PDP page is displayed
    And  I verify product info on the PDP page
    And  I verify price range on PDP page
    When I set first available size options on PDP page
    Then I verify price range on PDP page

    Examples:
      | Category | Menu Option    | Brand     | Brand Link       |
      | TIRES    | Michelin Tires | MICHELIN  | All-Season tires |
      | WHEELS   | MB Wheels      | MB WHEELS | Chrome wheels    |

  @dt
  @web
  @17797
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verify_canonical_pdp_attributes
  Scenario Outline: Verify canonical pdp product attributes (ALM#17797)
    """TO Do: The commented step is not valid and there will be a upcoming story to have Best Selling on latest fitment popup modal"""
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the first available View Details from the results page
    Then I verify canonical product attributes on PDP "TIRES"
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    And  I select edit vehicle link
    And  I remove all vehicles on session from fitment section
    And  I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
#    And  I select a fitment option "Best selling"
    And  I select a fitment option "wheel"
    And  I select the first available View Details from the results page
    Then I verify canonical product attributes on PDP "WHEELS"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe EX |

  @dt
  @web
  @17798
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyEnterVehicleLinkOnPDP
  Scenario Outline: Verify enter vehicle link and text on canonical PDP  (ALM#17798)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I click on the "continue without vehicle" link
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    When I select the first available View Details from the results page
    Then I verify "Canonical" PDP page is displayed
    And  I verify the message on the "PDP" banner contains "Enter your vehicle to ensure these tires fit"
    When I click on the "Enter vehicle" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I verify price range on PDP page
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I click on the "continue without vehicle" link
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "passenger wheels" from the Product Brand page
    And  I select the first available View Details from the results page
    Then I verify "Canonical" PDP page is displayed
    And  I verify the message on the "PDP" banner contains "Enter your vehicle to ensure these wheels fit"
    When I click on the "Enter vehicle" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I verify price range on PDP page

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @web
  @18268
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyEnterVehicleLinkOnPDP
  Scenario Outline: Verify enter vehicle text and link on canonical PDP (ALM#TBA)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    When I select the first available View Details from the results page
    Then I verify "Canonical" PDP page is displayed
    And  I verify the message on the "PDP" banner contains "Enter your vehicle to ensure these tires fit"
    When I click on the "Enter vehicle" link
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I verify price range on PDP page
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    And  I select the first available View Details from the results page
    Then I verify "Canonical" PDP page is displayed
    And  I verify the message on the "PDP" banner contains "Enter your vehicle to ensure these wheels fit"
    When I click on the "Enter vehicle" link
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I verify price range on PDP page

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @web
  @17800
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyReviewRatingsOnCanonicalPDP
  Scenario Outline: Verify rating and review for canonical PDP products (ALM#17800)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    When I select the first available View Details from the results page
    And  I click on the "Enter vehicle" link
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    Then I verify review ratings on Canonical PDP page
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    And  I click on the "Enter vehicle" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "CONTINUE" button
    And  I click the "<Menu Option>" menu option
    And  I select the first available View Details from the results page
    Then I verify "Read Reviews" text not present on PDP page
    And  I verify "Ratings" text not present on PDP page

    Examples:
      | Year | Make  | Model | Trim     | Menu Option |
      | 2012 | Honda | Civic | Coupe DX | MB WHEELS   |

  @dt
  @web
  @17801
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyReviewRatingsOnNonCanonicalPdp
  Scenario: Verify rating and review for non canonical products (ALM#17801)
    When I open the "TIRES" navigation link
    And  I click the "Goodyear Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    When I select the first available Standard product to "Add To Cart" on PLP page
    Then I verify review ratings on Canonical PDP page

  @dt
  @web
  @17802
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyPromotionOrRebateOnCanonicalPdp
  Scenario: Verify promotion or rebate for canonical PDP products (ALM#17802)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify "Promotions" exists in PLP
    When I click "Promotions" element in PLP
    Then I verify "Promotions" exists in PDP
    When I click "Mail In Rebate" element in PDP
    Then I verify "Deals and Rebates" exists in PDP

  @dt
  @web
  @17803
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifySubtotalAndQuantityOnStandartPdp
  Scenario: Verify subtotal and quantity on Standard PDP products (ALM#17803)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I select the first available Standard product to "Add To Cart" on PLP page
    Then I verify quantity value "4" on PDP page
    When I click "Sub total tool tip" element in PDP
    Then I verify Sub Total Tool Tip has the required text in PDP
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    And  I select the first available Standard product to "Add To Cart" on PLP page
    Then I verify quantity value "4" on PDP page
    When I click "Sub total tool tip" element in PDP
    Then I verify Sub Total Tool Tip has the required text in PDP

  @dt
  @web
  @17804
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifySubtotalAndQuantityOnCanonicalPdp
  Scenario: Verify subtotal and quantity on Canonical PDP products (ALM#17804)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I select the first available View Details from the results page
    Then I verify "Subtotal" text not present on PDP page

  @dt
  @web
  @18164
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyEditQuantityOnCanonicalPdp
  Scenario Outline: Verify edit quantity on Canonical PDP products (ALM#18164)
    When I open the "TIRES" navigation link
    And  I do a "homepage" "tire" size search with details "<TireWidth>, <AspectRatio>, <TireDiameter>"
    And  I select the first available View Details from the results page
    Then I verify element "Tire Size" has text "<TireWidth>" in PDP
    And  I verify element "Tire Size" has text "<AspectRatio>" in PDP
    And  I verify element "Tire Size" has text "<TireDiameter>" in PDP
    Then I verify PDP has single price
    And  I verify the product availability section is displayed
    When I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY SIZE" menu option
    And  I do a "homepage" "wheel" size search with details "<WheelSize>"
    And  I select the first available view details from the results page for product with image and extract product info
    Then I verify element "Wheel Size" has text "<WheelSize>" in PDP
    And  I verify selected wheel color matches with displayed wheel image on PDP
    And  I verify price range on PDP page
    When I set first available size options on PDP page
    And  I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP

    Examples:
      | TireWidth | AspectRatio | TireDiameter | WheelSize |
      | 285       | 70          | 18           | 16        |

  @dt
  @web
  @18165
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyEditQuantityOnPdpThroughWheelsAdvanceSearch
  Scenario Outline: Verify edit quantity on PDP through advance search for wheels (ALM#18165)
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY SIZE" menu option
    And  I select the "Advanced Search" subheader on the fitment grid
    And  I select the "<Diameter>" fitment grid option
    And  I select the "<Wheel Width>" fitment grid option
    And  I select the "<Bolt Pattern>" fitment grid option
    And  I click on the "VIEW WHEELS" button
    And  I select the first available View Details from the results page
    Then I verify element "Wheel Size" has text "12" in PDP
    And  I verify PDP has single price
    And  I verify the product availability section is displayed
    When I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP

    Examples:
      | Diameter | Wheel Width | Bolt Pattern |
      | 12       | 8.0         | 4-156.0 mm   |

  @dt
  @web
  @18166
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyEditQuantityOnPdpByCategory
  Scenario Outline: Verify edit quantity on PDP when search type is by category (ALM#18166)
    When I open the "<Category>" navigation link
    And  I click the "<Menu Option>" menu option
    And  I select "<Brand>" from the Product Brands page
    Then I verify bread crumb section contains the text "<Brand>"
    When I select the first available view details from the results page for product with image and extract product info
    Then I verify price range on PDP page
    When I set first available size options on PDP page
    And  I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP

    Examples:
      | Category | Menu Option       | Brand      |
      | TIRES    | All-Terrain Tires | BFGoodrich |
      | WHEELS   | Passenger Wheels  | MB WHEELS  |

  @dt
  @web
  @18167
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyEditQuantityOnPdpByVehicleType
  Scenario: Verify edit quantity on PDP when search type is vehicle type (ALM#18167)
    When I open the "TIRES" navigation link
    And  I click the "Passenger Tires" menu option
    And  I select "BFGoodrich" from the Product Brands page
    Then I verify bread crumb section contains the text "BFGoodrich"
    When I select the first available View Details from the results page
    And  I set first available size options on PDP page
    Then I verify PDP has single price
    And  I verify the product availability section is displayed
    When I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP
    When I open the "WHEELS" navigation link
    And  I click the "Passenger Wheels" menu option
    And  I select "MB WHEELS" from the Product Brands page
    And  I select the first available view details from the results page for product with image and extract product info
    Then I verify selected wheel color matches with displayed wheel image on PDP
    And  I verify price range on PDP page
    When I set first available size options on PDP page
    And  I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP

  @dt
  @web
  @18168
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyAddProductToCartAndEditQuantityOnPdpByBrand
  Scenario Outline: Verify add product to cart for check out process and edit quantity when search type is by brand (ALM#18168)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify bread crumb section contains the text "All-Season"
    When I select the first available View Details from the results page
    And  I set first available size options on PDP page
    And  I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP
    And  I verify add to cart button is "DISPLAYED"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    And  I select the first available View Details from the results page
    And  I set first available size options on PDP page
    And  I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded

    Examples:
      | Customer            | Reason                              |
      | default_customer_az | Make an appointment at a later time |

  @dt
  @web
  @18169
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyAddToCartDisabledIfDropDownsNotSelected
  Scenario:Verify Add to Cart button disabled if user do not fill the required dropdown (ALM#18169)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify bread crumb section contains the text "All-Season"
    When I select the first available View Details from the results page
    Then I verify add to cart button is "DISABLED"
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    And  I select the first available View Details from the results page
    Then I verify selected wheel color matches with displayed wheel image on PDP
    And  I verify add to cart button is "DISABLED"

  @dt
  @web
  @18170
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyVisibilityOfSpecsDataOnPdp
  Scenario: Verify Specs data when user drills down to article level (ALM#18170)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I select the first available View Details from the results page
    And  I set first available size options on PDP page
    Then I verify expected "Tire" attributes are displayed for product detail page section "SIZE"
    And  I verify expected "Tire" attributes are displayed for product detail page section "TREAD & TRACTION"
    And  I verify expected "Tire" attributes are displayed for product detail page section "SAFETY & PERFORMANCE"
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    And  I select the first available View Details from the results page
    Then I verify selected wheel color matches with displayed wheel image on PDP
    When I set first available size options on PDP page
    Then I verify expected "Wheel" attributes are displayed for product detail page section "SIZE"
    And  I verify expected "Wheel" attributes are displayed for product detail page section "STYLE & CONSTRUCTION"
    And  I verify expected "Wheel" attributes are displayed for product detail page section "SAFETY & PERFORMANCE"

  @dt
  @web
  @18171
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyTreadwellDisplay
  Scenario Outline:Verify treadwell on Canonical PDP (ALM#18171)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select enter driving details for recommended tires
    And  I select "Performance" as my driving priority
    And  I select view recommended tires
    Then I verify products displayed contain treadwell details on PLP
    And  I verify the PLP page is displayed
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Passenger wheels" from the Product Brand page
    Then I verify "ESTIMATED TIRE LIFE & COST" text not present on PLP page

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @web
  @18172
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyMileageWarrantyOfProduct
  Scenario Outline: Verify the mileage warranty displayed on PDP (ALM#18172)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I select "View Details" for product "<Product Name>" on the PLP page
    Then I verify product has mileage warranty "With range"
    When I set first available size options on PDP page
    Then I verify product has mileage warranty "Without range"
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    And  I select the first available View Details from the results page
    Then I verify "mile warranty" text not present on PDP page

    Examples:
      | Product Name     |
      | LATITUDE TOUR HP |

  @dt
  @web
  @18173
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyCategoryPageFitment
  Scenario: Verify category page fitment for product (ALM#18173)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I click "Enter vehicle" element in PLP
    And  I do a "homepage" vehicle search with details "2012" "Honda" "Civic" "Coupe DX" "none"
    And  I select the "First" product result image on "PLP" page
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Passenger wheels" from the Product Brand page
    And  I select the "First" product result image on "PLP" page
    Then I verify the message on the "PDP" banner contains "These wheels fit your vehicle"

  @dt
  @web
  @18174
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyIconographyOfProducts
  Scenario: Verify iconography for the products displayed (ALM#18174)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I select the first available View Details from the results page
    Then I verify tooltip and tooltip text for each icon on PDP

  @dt
  @web
  @18175
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyWidth,RatioAndDiameterIsDisplayed
  Scenario:Verify User is able to see the Width,Ratio and Diameter drop downs on the PDP (ALM#18175)
    When I open the "TIRES" navigation link
    And  I click the "TIRE SEARCH" menu option
    And  I select "all-terrain tires" from the Product Brand page
    And  I select "BFGoodrich" from the Product Brands page
    Then I verify bread crumb section contains the text "BFGoodrich"
    And  I verify Canonical products are "displayed" on PLP page
    And  I verify PLP results are grouped by brand "BFGoodrich"
    When I select the first available View Details from the results page
    And  I set first available size options on PDP page
    Then I verify the "help me choose" Link color and help text

  @dt
  @web
  @18176
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifySelectionOFWidthRatioAndDiameterInAnyOrder
  Scenario:Verify user can select Width ratio and Diameter in any order (ALM#18176)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "passenger tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify PLP results are grouped by brand "MICHELIN"
    When I select the first available View Details from the results page
    And  I select "First" value from "Diameter" drop down on PDP page for "Tire"
    And  I select "First" value from "Width" drop down on PDP page for "Tire"
    And  I select "First" value from "Diameter" drop down on PDP page for "Tire"
    Then I verify add to cart button is "ENABLED"
    When I navigate back to previous page
    And  I select the first available View Details from the results page
    And  I select "First" value from "Ratio" drop down on PDP page for "Tire"
    And  I select "First" value from "Width" drop down on PDP page for "Tire"
    And  I select "First" value from "Diameter" drop down on PDP page for "Tire"
    Then I verify add to cart button is "ENABLED"
    When I navigate back to previous page
    And  I select the first available View Details from the results page
    And  I select "First" value from "Width" drop down on PDP page for "Tire"
    And  I select "First" value from "Diameter" drop down on PDP page for "Tire"
    And  I select "First" value from "Diameter" drop down on PDP page for "Tire"

  @dt
  @web
  @18177
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyDropDownValuesBasedOnFirstDropDownValueSelection
  Scenario Outline:Verify filtering of dropdown values should be based on first drop down selected (ALM#18177)
    When I open the "<Menu Link>" navigation link
    And  I click the "<Submenu Link>" menu option
    And  I select "<Category Link>" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify PLP results are grouped by brand "<Brand>"
    When I select the first available View Details from the results page
    And  I select "First" value from "<Dropdown>" drop down on PDP page for "<Product Type>"
    Then I verify the tire size dropdowns are populated on the PDP page

    Examples:
      | Menu Link | Submenu Link   | Category Link   | Brand    | Product Type | Dropdown |
      | TIRES     | Michelin Tires | passenger tires | MICHELIN | Tire         | Ratio    |
      | TIRES     | Michelin Tires | passenger tires | MICHELIN | Tire         | Diameter |
      | TIRES     | Michelin Tires | passenger tires | MICHELIN | Tire         | Width    |

  @dt
  @web
  @18178
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyDropDownValuesBasedOnDiameterAndRatioSelection
  Scenario Outline:Verify filtering of width dropdown value should be based on diameter and ratio dropdown selected (ALM#18178)
    When I open the "TIRES" navigation link
    And  I click the "All-Terrain Tires" menu option
    And  I select "BFGoodrich" from the Product Brands page
    Then I verify bread crumb section contains the text "BFGoodrich"
    And  I verify Canonical products are "displayed" on PLP page
    And  I verify PLP results are grouped by brand "BFGoodrich"
    When I select the first available View Details from the results page
    And  I select "<Diameter>" value from "Diameter" drop down on PDP page for "Tire"
    And  I select "<Ratio>" value from "Ratio" drop down on PDP page for "Tire"
    And  I select "<Width>" value from "Width" drop down on PDP page for "Tire"
    Then I verify the values of "<Width>" "<Ratio>" "<Diameter>" dropdown

    Examples:
      | Width | Ratio | Diameter |
      | 275   | 65    | 20       |

  @dt
  @web
  @18180
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifySelectionOfStaticDropDownsInAnyOrder
  Scenario Outline:Verify User can select the static drop downs Diameter, Width for wheels in any order on the PDP (ALM#18180)
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" to shop
    Then I verify Canonical products are "displayed" on PLP page
    When I select product "OLD SCHOOL" from PLP page
    Then I should see product detail page with "OLD SCHOOL"
    When I select "<Diameter>" value from "Diameter" drop down on PDP page for "Wheel"
    And  I select "<Width>" value from "Width" drop down on PDP page for "Wheel"
    Then I verify the values in "<Width>" "<Diameter>" dropdown for wheels
    When I navigate back to previous page
    And  I select product "OLD SCHOOL" from PLP page
    And  I select "<Width>" value from "Width" drop down on PDP page for "Wheel"
    And  I select "<Diameter>" value from "Diameter" drop down on PDP page for "Wheel"
    Then I verify the values in "<Width>" "<Diameter>" dropdown for wheels

    Examples:
      | Width | Diameter |
      | 7.0   | 18       |

  @dt
  @web
  @18181
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyWidthValueRemainsSameWhenDiameterIsChanged
  Scenario Outline:Verify if dropdown values in Width remain same when user changes value in Diameter dropdown (ALM#18181)
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" to shop
    Then I verify Canonical products are "displayed" on PLP page
    When I select product "OLD SCHOOL" from PLP page
    And  I select "<Width>" value from "Width" drop down on PDP page for "Wheel"
    And  I select "<Diameter>" value from "Diameter" drop down on PDP page for "Wheel"
    Then I verify the values in "<Width>" "<Diameter>" dropdown for wheels
    When I select "<Diameter2>" value from "Diameter" drop down on PDP page for "Wheel"
    Then I verify the values in "<Width>" "<Diameter2>" dropdown for wheels

    Examples:
      | Width | Diameter | Diameter2 |
      | 8.0   | 17       | 18        |

  @dt
  @web
  @18182
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyDropDownValueWrtUserSelectedDropDown
  Scenario Outline:Verify dropdown value with respect to user selected dropdown (ALM#18182)
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" to shop
    Then I verify Canonical products are "displayed" on PLP page
    When I select product "Vector" from PLP page
    Then I should see product detail page with "Vector"
    When I select "<Diameter>" value from "Diameter" drop down on PDP page for "Wheel"
    Then I verify the values in "<Width>" "<Diameter>" dropdown for wheels
    When I navigate back to previous page
    And  I select product "Vector" from PLP page
    And  I select "<Width>" value from "Width" drop down on PDP page for "Wheel"
    Then I verify the values in "<Width>" "<Diameter>" dropdown for wheels

    Examples:
      | Width | Diameter |
      | 6.5   | 16       |

  @dt
  @web
  @18183
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyTwoDropDownsWhenThirdDropDownIsChanged
  Scenario Outline:Verify if two dropdown values remain same when user changes value in the third dropdown for Tires (ALM#18183)
    When I open the "TIRES" navigation link
    And  I click the "All-Terrain Tires" menu option
    And  I select "BFGoodrich" from the Product Brands page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify PLP results are grouped by brand "BFGoodrich"
    When I select product "KO2" from PLP page
    And  I select "<Diameter>" value from "Diameter" drop down on PDP page for "Tire"
    And  I select "<Ratio>" value from "Ratio" drop down on PDP page for "Tire"
    And  I select "<Width>" value from "Width" drop down on PDP page for "Tire"
    Then I verify the values of "<Width>" "<Ratio>" "<Diameter>" dropdown
    When I select "<Width2>" value from "Width" drop down on PDP page for "Tire"
    Then I verify the values of "<Width2>" "<Ratio>" "<Diameter>" dropdown

    Examples:
      | Width | Ratio | Diameter | Width2 |
      | 275   | 70    | 18       | 265    |

  @dt
  @web
  @18184
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyResetOfValuesInDropDownWhenOneOfTheDropDownValueChangedInTires
  Scenario Outline:Verify if dropdown values resets when user changes value from one of the dropdown for Tires (ALM#18184)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "passenger tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify PLP results are grouped by brand "MICHELIN"
    When I select product "PILOT SUPER SPORT" from PLP page
    And  I select "<Width>" value from "Width" drop down on PDP page for "Tire"
    Then I verify the values of "<Width>" "<Ratio>" "<Diameter>" dropdown
    When I select "<Width2>" value from "Width" drop down on PDP page for "Tire"
    Then I verify the values of "<Width2>" "" "" dropdown

    Examples:
      | Width | Ratio | Diameter | Width2 |
      | 195   | 45    | 17       | 255    |

  @dt
  @web
  @18185
  @canonicalPDP
  @canonicalPLPPDP
  @PDP_verifyResetOfValuesInDropDownWhenOneOfTheDropDownValueChangedInWheels
  Scenario Outline:Verify if dropdown values resets when user changes value from one of the dropdown for Wheels (ALM#18185)
    When I open the "WHEELS" navigation link
    And  I click the "Drag Wheels" menu option
    And  I click the "Passenger wheels" menu option
    Then I verify Canonical products are "displayed" on PLP page
    When I select product "DR-37" from PLP page
    And  I select "<Diameter>" value from "Diameter" drop down on PDP page for "Wheel"
    Then I verify the values in "<Width>" "<Diameter>" dropdown for wheels
    When I select "<Diameter2>" value from "Diameter" drop down on PDP page for "Wheel"
    Then I verify the values in "" "<Diameter2>" dropdown for wheels

    Examples:
      | Width | Diameter | Diameter2 |
      | 7.5   | 17       | 19        |

  @dt
  @web
  @17696
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_groupingOfTiresByBrand
  Scenario Outline: Verify canonical plp products grouping of tires by Brand (ALM#17696)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify PLP results are grouped by brand "<Brand>"
    And  I verify price range displayed on canonical product
    And  I verify default "<Sortby>" sortby option

    Examples:
      | Brand    | Sortby    |
      | Michelin | Relevance |

  @dt
  @web
  @17697
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_groupingOfTiresBySize
  Scenario Outline: Verify canonical plp products grouping of tires by Size (ALM#17697)
    When I open the "TIRES" navigation link
    And  I do a "homepage" "tire" size search with details "<TireWidth>, <AspectRatio>, <TireDiameter>"
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify price range displayed on canonical product
    And  I verify default "<Sortby>" sortby option

    Examples:
      | TireWidth | AspectRatio | TireDiameter | Sortby              |
      | 265       | 40          | 19           | Price (Low to High) |

  @dt
  @web
  @17698
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_groupingOfTiresByVehicleType
  Scenario Outline: Verify canonical plp products grouping of tires by Vehicle Type (ALM#17698)
    When I open the "TIRES" navigation link
    And  I click the "<Tire Link>" menu option
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify default "<Sortby>" sortby option

    Examples:
      | Tire Link       | Year | Make  | Model | Trim     | FitmentOption | Sortby      |
      | Passenger Tires | 2012 | Honda | Civic | Coupe EX | tire          | Best Seller |

  @dt
  @web
  @17699
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_groupingOfWheelsByBrand
  Scenario Outline: Verify canonical plp products grouping of wheels by brand (ALM#17699)
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify PLP results are grouped by brand "<Brand>"
    And  I verify price range displayed on canonical product
    And  I verify default "<Sortby>" sortby option

    Examples:
      | Brand     | Sortby    |
      | MB Wheels | Relevance |

  @dt
  @web
  @17700
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_groupingOfWheelsBySize
  Scenario Outline: Verify canonical plp products grouping of wheels by size (ALM#17700)
    When I open the "WHEELS" navigation link
    And  I do a "homepage" "wheel" size search with details "<Diameter>, <WheelWidth>, <BoltPattern>"
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify default "<Sortby>" sortby option

    Examples:
      | Diameter | WheelWidth | BoltPattern       | Sortby              |
      | 15       | 6.5        | 5-114.3 MM/5-4.5" | Price (Low to High) |

  @dt
  @web
  @17701
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_groupingOfWheelsByVehicleType
  Scenario Outline: Verify canonical plp products grouping of wheels by vehicle type (ALM#17701)
    When I open the "WHEELS" navigation link
    And  I click the "Passenger Wheels" menu option
    And  I click on the "SHOP ALL" link
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify default "<Sortby>" sortby option

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption | Sortby              |
      | 2012 | Honda | Civic | Coupe DX | wheel         | Price (Low to High) |

  @dt
  @web
  @17702
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_customerRatingAndNavigation
  Scenario: Verify customer rating for PLP and ratings on PDP (ALM#17702)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Customer Rating and reviews are displayed for listed products on PLP result page
    When I select the 'Read Reviews' link on the PLP
    Then I am brought to the page with header "Customer reviews"
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    Then I verify "Read Reviews" text not present on PLP page
    And  I verify "Ratings" text not present on PLP page

  @dt
  @web
  @17703
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_promotionsAndSavingsAvailableTagVerification
  Scenario: Verify the promotions on canonical PLP (ALM#17703)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify "Promotions" exists in PLP
    And  I verify "Promotions" has the text of "Savings available" in PLP
    When I click "Promotions" element in PLP
    Then I verify "Canonical" PDP page is displayed

  @dt
  @web
  @17704
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_iconographyVerification
  Scenario: Verify iconography on listed canonical plp products (ALM#17704)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify non canonical product details contain Item#
    And  I verify tooltip and tooltip text for each icon

  @dt
  @web
  @17705
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_thumbnailImageDisplayForTires
  Scenario: Verify the thumbnail image displayed on PLP (ALM#17705)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify the thumbnail images are displayed for the listed products
    When I select the first available View Details from the results page
    Then I verify "Canonical" PDP page is displayed
    When I go to the homepage
    And  I do a "homepage" "wheel" size search with details "15, 7.0, 5-100.0 MM"
    Then I verify the thumbnail images are displayed for the listed products
    When I select the first available View Details from the results page
    Then I verify "Canonical" PDP page is displayed

  @dt
  @web
  @17706
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_standardPlpReDesignWithAddToCartFunctionality
  Scenario: Verify standard plp re-design with add to cart functionality  (ALM#17706)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify Add To Cart button is displayed on PLP page
    When I select the first available Standard product to "Add To Cart" on PLP page
    Then I verify "Standard" PDP page is displayed
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify Add To Cart button is displayed on PLP page
    When I select the first available Standard product to "Add To Cart" on PLP page
    Then I verify "Standard" PDP page is displayed

  @dt
  @web
  @17707
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_mileageWarrantyVerification
  Scenario Outline: Verify the mileage warranty displayed on PLP (ALM#17707)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify "Mile Warranty" exists in PLP
    And  I verify mile warranty range for PLP products
    When I select from the "Mileage Warranty" filter section, "single" option(s): "<Filter 1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    And  I verify mileage warranty is within filter range "<Minimum>" "<Maximum>"
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify "mile warranty" text not present on PLP page

    Examples:
      | Filter 1      | Minimum | Maximum |
      | 40,000-50,000 | 40,000  | 50,000  |

  @dt
  @web
  @17708
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyMileageWarrantyForStaggeredTires
  Scenario Outline: Verify mileage warranty for staggered Tires (ALM#17708)
    Then I verify "Fitment popup" exists in search results page
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select "FRONT" staggered tab on PLP result page
    And  I select from the "Mileage Warranty" filter section, "single" option(s): "<Filter 1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    And  I verify mileage warranty is within filter range "<Minimum>" "<Maximum>"
    When I select "REAR" staggered tab on PLP result page
    And  I select from the "Mileage Warranty" filter section, "single" option(s): "<Filter 1>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Filter 1>"
    And  I verify mileage warranty is within filter range "<Minimum>" "<Maximum>"

    Examples:
      | Year | Make      | Model    | Trim | Filter 1      | Minimum | Maximum |
      | 2010 | Chevrolet | Corvette | Base | 40,000-50,000 | 40,000  | 50,000  |

  @dt
  @web
  @17709
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifySubtotalFieldAndAddtocartNon-staggeredTires
  Scenario Outline: Verify the subtotal and add to cart field for non-staggered Tires (ALM#17709)
    Then I verify "Fitment popup" exists in search results page
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify calculation of subtotal on qty and retail price
    And  I verify subtotal with tooltip exist for all non canonical products
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I add item to my cart and "View shopping Cart"
    Then I verify selected items on cart page
    And  I verify the required fees and add-ons sections are expanded

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | tire          |

  @dt
  @web
  @17710
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifySubtotalFieldAndAddtocartNon-staggeredWheels
  Scenario Outline: Verify the subtotal field and add to cart field for non-staggered Wheels (ALM#17710)
    Then I verify "Fitment popup" exists in search results page
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "wheel"
    Then I verify calculation of subtotal on qty and retail price
    And  I verify subtotal with tooltip exist for all non canonical products
    And  I verify Add To Cart button is displayed on PLP page
    When I add item to my cart and "View shopping Cart"
    Then I verify selected items on cart page
    And  I verify the required fees and add-ons sections are expanded

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @web
  @17711
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifySubtotalFieldAndAddtocartStaggeredTires
  Scenario Outline: Verify the subtotal field for Tires (ALM#17711)
    Then I verify "Fitment popup" exists in search results page
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify calculation of subtotal on qty and retail price for 'SETS' staggered tab
    And  I verify subtotal with tooltip exist for all non canonical products
    When I select "FRONT" staggered tab on PLP result page
    Then I verify quantity for front and rear tabs plp
    And  I verify Add To Cart button is displayed on PLP page
    When I select "REAR" staggered tab on PLP result page
    Then I verify quantity for front and rear tabs plp
    And  I verify Add To Cart button is displayed on PLP page
    When I add item to my cart and "View shopping Cart"
    Then I verify selected items on cart page
    And  I verify the required fees and add-ons sections are expanded

    Examples:
      | Year | Make      | Model    | Trim | FitmentOption |
      | 2010 | Chevrolet | Corvette | Base | tire          |

  @dt
  @web
  @18159
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifySubtotalFieldAndAddtocartStaggeredwheels
  Scenario Outline: Verify the subtotal and add to cart functionality for Wheels (ALM#18159)
    Then I verify "Fitment popup" exists in search results page
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "wheel"
    Then I verify calculation of subtotal on qty and retail price
    And  I verify quantity for front and rear tabs plp
    And  I verify subtotal with tooltip exist for all non canonical products
    And  I verify quantity for front and rear tabs plp
    And  I verify Add To Cart button is displayed on PLP page
    When I select "REAR" staggered tab on PLP result page
    Then I verify quantity for front and rear tabs plp
    When I add item to my cart and "View shopping Cart"
    Then I verify selected items on cart page
    And  I verify the required fees and add-ons sections are expanded

    Examples:
      | Year | Make      | Model    | Trim |
      | 2010 | Chevrolet | Corvette | Base |

  @dt
  @web
  @17713
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyCompareButtonFunctionality
  Scenario Outline: Verify the compare button functionality for Tires (ALM#17713)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP page is displayed
    When I select the first "3" results to compare
    And  I click the compare products Compare button
    And  I add the first item to my cart and click "Continue Shopping" on the Compare Products page
    And  I click Remove All
    Then I verify the PLP page is displayed

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | tire          |

  @dt
  @web
  @17714
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyTiresizeDisplayedOnResultBarAndNo-reviewMessage
  Scenario Outline: Verify tire size on result bar and no review message (ALM#17714)
    When I do a "homepage" "tire" size search with details "<Width>, <Ratio>, <Diameter>"
    Then I verify the PLP header message contains "Shop 155/70R17 Tires"
    And  I verify Customer Rating and reviews are displayed for listed products on PLP result page
    When I select the "First" product result image on "PLP" page
    Then I verify Customer review portion on PDP contains "There are currently no reviews for this product" text

    Examples:
      | Width | Ratio | Diameter |
      | 155   | 70    | 17       |

  @dt
  @web
  @17715
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifySet_Front_RearFunctionalitiesStaggeredTires
  Scenario Outline: Verify sets-front-rear functionalities for staggered Tires (ALM#17715)
    Then I verify "Fitment popup" exists in search results page
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire"
    Then I verify "SETS" staggered option tab is displayed on PLP result page
    And  I verify Customer Rating and reviews are displayed for listed products on PLP result page
    When I select the 'Read Reviews' link on the PLP
    Then I am brought to the page with header "Customer reviews"
    When I navigate back to previous page
    Then I verify "FRONT" staggered option tab is displayed on PLP result page
    When I select "FRONT" staggered tab on PLP result page
    Then I verify the order of filter categories for "<Product Type>"
    And  I verify the "<Facet1>" filter section is expanded by default
    And  I verify the "<Facet2>" filter section is expanded by default
    And  I verify the "<Facet3>" filter section is expanded by default
    And  I verify the "<Facet4>" filter section is expanded by default
    When I select from the "Brands" filter section, "single" option(s): "Goodyear"
    Then I verify that the search refinement filters contain the "single" value(s): "Goodyear"
    And  I verify PLP results are grouped by brand "<Brand>"
    And  I verify Customer Rating and reviews are displayed for listed products on PLP result page
    When I select the 'Read Reviews' link on the PLP
    Then I am brought to the page with header "Customer reviews"
    When I navigate back to previous page
    Then I verify "REAR" staggered option tab is displayed on PLP result page
    When I select "REAR" staggered tab on PLP result page
    Then I verify the order of filter categories for "<Product Type>"
    And  I verify the "<Facet1>" filter section is expanded by default
    And  I verify the "<Facet2>" filter section is expanded by default
    And  I verify the "<Facet3>" filter section is expanded by default
    And  I verify the "<Facet4>" filter section is expanded by default
    And  I verify Customer Rating and reviews are displayed for listed products on PLP result page
    When I select the 'Read Reviews' link on the PLP
    Then I am brought to the page with header "Customer reviews"

    Examples:
      | Year | Make      | Model    | Trim | Facet1      | Facet2        | Facet3  | Facet4 | Product Type | Brand    |
      | 2010 | Chevrolet | Corvette | Base | Price Range | Quick Filters | Ratings | Brands | Tires        | Goodyear |

  @dt
  @web
  @17716
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyFacetFiltersForTires
  Scenario Outline: Verify facet filters for plp Tires (ALM#17716)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP page is displayed
    And  I verify the order of filter categories for "<Product Type>"
    And  I verify the "<Facet1>" filter section is expanded by default
    And  I verify the "<Facet2>" filter section is expanded by default
    And  I verify the "<Facet3>" filter section is expanded by default
    And  I verify the "<Facet4>" filter section is expanded by default
    When I select from the "Brands" filter section, "single" option(s): "<Brand>"
    Then I verify that the search refinement filters contain the "single" value(s): "<Brand>"
    And  I verify PLP results are grouped by brand "<Brand>"

    Examples:
      | Year | Make  | Model | Trim     | Facet1      | Facet2        | Facet3  | Facet4 | Product Type | Brand    |
      | 2012 | Honda | Civic | Coupe DX | Price Range | Quick Filters | Ratings | Brands | Tires        | Michelin |

  @dt
  @web
  @17717
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyCanonicalProductsWhenUserChooseFitmentModal
  Scenario Outline:Verify canonical products via fitment modal and facet filters for plp Wheels (ALM#17717)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP page is displayed
    When I navigate back to previous page
    And  I select a fitment option "wheel"
    Then I verify the PLP page is displayed
    And  I verify the order of filter categories for "<Product Type>"
    And  I verify the "<Facet1>" filter section is expanded by default
    And  I verify the "<Facet2>" filter section is expanded by default
    And  I verify the "<Facet3>" filter section is expanded by default
    When I select from the "Brands" filter section, "single" option(s): "MB Wheels"
    Then I verify that the search refinement filters contain the "single" value(s): "MB Wheels"
    And  I verify PLP results are grouped by brand "<Brand>"

    Examples:
      | Year | Make  | Model | Trim     | Facet1      | Facet2        | Facet3 | Product Type | Brand     |
      | 2012 | Honda | Civic | Coupe DX | Price Range | Quick Filters | Brands | Wheels       | MB Wheels |

  @dt
  @web
  @17718
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyCanonicalProductsNotDisplayedInPDLFlow
  Scenario Outline:Verify canonical products not displayed via PDL flow (ALM#17718)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire"
    And  I select "Treadwell" option
    And  I select "Performance" as my driving priority
    And  I select view recommended tires
    Then I verify the PLP page is displayed
    And  I verify Canonical products are "not displayed" on PLP page

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @web
  @17719
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyCategoryPageFitmentForPlp
  Scenario Outline:Verify category page fitment for canonical PLP (ALM#17719)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I open the My Vehicles popup
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify message displayed in search no results page with "no results"
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Passenger wheels" from the Product Brand page
    Then I verify the message on the "PLP" banner contains "These wheels fit your vehicle"

    Examples:
      | Year | Make  | Model | Trim     | FitmentOption |
      | 2012 | Honda | Civic | Coupe DX | tire          |

  @dt
  @web
  @18160
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyEnterVehicleLinkOnCanonicalPLP
  Scenario Outline:Verify enter vehicle link (ALM#18160)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify the message on the "PLP" banner contains "Enter your vehicle to ensure these tires fit"
    When I click "Enter vehicle" element in PLP
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    Then I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify "<Tire Brand>" and "<Tire Size>" on PLP product details when vehicle on session
    When I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select edit vehicle link
    And  I remove all vehicles on session from fitment section
    And  I open the "WHEELS" navigation link
    And  I click the "MB Wheels" menu option
    And  I select "Chrome wheels" from the Product Brand page
    Then I verify the message on the "PLP" banner contains "Enter your vehicle to ensure these wheels fit"
    When I click "Enter vehicle" element in PLP
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    Then I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify PLP results are grouped by brand "<Wheel Brand>"

    Examples:
      | Year | Make  | Model | Trim     | Tire Brand | Tire Size | Wheel Brand |
      | 2012 | Honda | Civic | Coupe DX | Michelin   | 195 /65   | MB Wheels   |

  @dt
  @web
  @18161
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyLinkAndTextForCanonicalProducts
  Scenario Outline:Verify enter vehicle link and text for canonical products (ALM#18161)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    Then I verify price and inventory text for canonical products when "VEHICLE_NOT_ON_SESSION"
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify price and inventory text for canonical products when "VEHICLE_ON_SESSION"

    Examples:
      | Year | Make  | Model | Trim     |
      | 2014 | Honda | Civic | Coupe EX |

  @dt
  @web
  @18162
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyProductAttributesOnStandardPlpWhenVehicleInSession
  Scenario Outline:Verify the product attributes on Standard PLP page when  vehicle in session (ALM#18162)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP page is displayed
    And  I verify Size,Item,Price and Inventory displayed for the products in Standard PLP

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @web
  @18163
  @canonicalPLP
  @canonicalPLPPDP
  @PLP_verifyProductAttributesForStaggeredProductWhenVehicleInSession
  Scenario Outline:Verify the product attributes for Staggered Product on Standard PLP page (ALM#18163)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire"
    Then I verify the PLP page is displayed
    And  I verify "SETS" staggered option tab is displayed on PLP result page
    And  I verify Size,Item,Price and Inventory displayed for the products in Standard PLP

    Examples:
      | Year | Make      | Model    | Trim |
      | 2012 | Chevrolet | Corvette | Base |


  @dt
  @web
  @core
  @21236
  @coreUiUx
  @coreScenarioPLPPDP
  @coreScenarioCanonicalPDPverifyPDPDetailsCategorySearch
  Scenario Outline: Verify Canonical pdp page details and functionality (ALM#21236)
  """ Will fail on Wheel logo due to differences in logo definition. Also with deployment to QA1,
  the logo is not displaying in majority of cases. Defect 11505 has been raised regarding logos. """
    When I open the "<Category>" navigation link
    And  I click the "<Menu Option>" menu option
    And  I select "<Brand Link>" from the Product Brand page
    Then I verify Canonical products are "displayed" on PLP page
    And  I verify PLP results are grouped by brand "<Brand>"
    When I select the first available View Details from the results page
    Then I verify "Canonical" PDP page is displayed
    And  I verify product info on the PDP page
    And  I verify price range on PDP page
    When I set first available size options on PDP page
    Then I verify price range on PDP page
    When I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP

    Examples:
      | Category | Menu Option    | Brand     | Brand Link       |
      | TIRES    | Michelin Tires | MICHELIN  | All-Season tires |
      | WHEELS   | MB Wheels      | MB WHEELS | Chrome wheels    |



  @dt
  @at
  @web
  @mobile
  @19771
  @canonicalEnhancements
  @storeAddressOnPLDPDP
  Scenario Outline: Store Address on PLP/PDP by My Store (ALM #19771)
    When I do a "homepage" "brand" "tire" search with option: "<Brand>"
    And  I select shop all from the Product Brand page
    And  I select the "In Stock" checkbox
    Then I verify saved store details in "PLP" page
    When I select the first available View Details from the results page
    And  I select "First" value from "Width" drop down on PDP page for "Tire"
    And  I select "First" value from "Ratio" drop down on PDP page for "Tire"
    And  I select "First" value from "Diameter" drop down on PDP page for "Tire"
    Then I verify saved store details in "PDP" page

    Examples:
      | Brand  |
      | FALKEN |

  @dt
  @at
  @web
  @mobile
  @19772
  @canonicalEnhancements
  @storeAddressOnPLDPDPWithProductCompare
  Scenario Outline: Store Address on compare page (ALM #19772)
    When I do a "homepage" "brand" "tire" search with option: "<Brand>"
    And  I select shop all from the Product Brand page
    And  I select the "In Stock" checkbox
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    Then I verify saved store details in "compare" page

    Examples:
      | Brand  |
      | FALKEN |

  @dt
  @at
  @dtd
  @web
  @19770
  @mobile
  @canonicalEnhancements
  @noResultsPageMessageUpdate
  Scenario Outline: Messaging update on No Results Page (ALM #19770)
    When I do a free text search for "<Search text>" and hit enter
    Then I verify message displayed in search no results page with "no vehicle in session"
    When I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I do a free text search for "<Search text>" and hit enter
    Then I verify message displayed in search no results page with "vehicle in session"

    Examples:
      | Search text | Year | Make      | Model    | Trim |
      | dfghdfghdfg | 2012 | Chevrolet | Corvette | Base |

  @dt
  @at
  @dtd
  @web
  @19776
  @canonicalEnhancements
  @expandModelsByDefault
  Scenario Outline: Dynamic Category Page - Show/Expand Models by default (ALM #19776)
    When I do a "homepage" "brand" "tire" search with option: "<Brand>"
    Then I verify all the models in brand page are expanded

    Examples:
      | Brand  |
      | FALKEN |

  @dt
  @at
  @dtd
  @web
  @mobile
  @19773
  @canonicalEnhancements
  @productReviewRedesign
  Scenario Outline: Product Review Redesign (ALM #19773)
    When I do a "homepage" "brand" "tire" search with option: "<Brand>"
    And  I select shop all from the Product Brand page
    And  I select the "Highest Rated" from the Sort By dropdown box
    Then I verify that the sort by dropdown value is set to "Highest Rated"
    When I select the "First" product result image on "PLP" page
    Then I am brought to the page with header "customer reviews"
    And  I verify element "review summary" has text "Ratings By Review" in PDP
    And  I verify element "review summary" has text "Average Customer Ratings" in PDP
    And  I verify categories present in all the review blocks in PDP page
      | Was this review helpful? |
      | Driving Conditions       |
      | Driving Style            |
      | Miles Driven On Tires    |

    Examples:
      | Brand  |
      | FALKEN |

  @dt
  @at
  @dtd
  @web
  @19778
  @canonicalEnhancements
  @vpnDropDownOnPdp
  Scenario Outline: Show new "No OE Designation" Value  and VPN dropdown on PDP (ALM #19778)
    When I open the "TIRES" navigation link
    And  I do a "homepage" "tire" size search with details "275, 45, 20"
    And  I select "View Details" for product "<Product Name>" on the PLP page
    And  I select "110V" value from "Load Index With Rating Key" drop down on PDP page for "Tire"
    And  I take page screenshot
    Then I verify element "OE Designation" has text "No OE Designation" in PDP
    When I select "16361" value from "manufacturer AID" drop down on PDP page for "Tire"
    Then I should see text "34082" present in the page source
    When I take page screenshot
    And  I select "31229" value from "manufacturer AID" drop down on PDP page for "Tire"
    Then I should see text "14345" present in the page source

    Examples:
      | Product Name     |
      | LATITUDE TOUR HP |

  @dtd
  @web
  @mobile
  @19777
  @canonicalEnhancements
  @disabledAddToCartButton
  Scenario Outline: Update Disabled Add to Cart Button (ALM #19777)
    When I do a "homepage" "brand" "tire" search with option: "<Brand>"
    And  I select shop all from the Product Brand page
    And  I take page screenshot
    Then I verify add to cart button disabled for item code "38867" in PLP page

    Examples:
      | Brand      |
      | BFGOODRICH |

  @dt
  @at
  @web
  @mobile
  @canonicalEnhancements
  @changeStorePlpPdp
  Scenario Outline: Change store on search results page - PLP and PDP
    When I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I click on the "change store" link
    Then I am brought to the page with header "Change Store"
    When I save new store address in store popup
    Then I verify current store details in store popup
    When I click on element name "Make my store" in change store popup
    Then I am brought to the page with header "STORE CHANGED"
    When I click on continue for successful change of store
    Then I verify saved store details in "PLP" page
    When I select the "First" product result image on "PLP" page
    Then I verify saved store details in "PDP" page
    When I click on the "change store" link
    Then I am brought to the page with header "Change Store"
    When I save new store address in store popup
    Then I verify current store details in store popup
    When I click on element name "Make my store" in change store popup
    And  I click on continue for successful change of store
    Then I verify saved store details in "PDP" page

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |

  @dt
  @at
  @web
  @mobile
  @canonicalEnhancements
  @changeStoreCompare
  Scenario Outline: Change store on search results page - Compare page
    When I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    And  I click on the "change store" link
    Then I am brought to the page with header "Change Store"
    When I save new store address in store popup
    Then I verify current store details in store popup
    When I click on element name "Make my store" in change store popup
    And  I click on continue for successful change of store
    Then I verify saved store details in "compare" page

    Examples:
      | Year | Make  | Model | Trim     |
      | 2012 | Honda | Civic | Coupe DX |