@webService
Feature: WebService response validation

  @dev
  @get
  Scenario Outline: Get Request Calls in QA Environment
    When I verify if this URL "<URL>" is up or not
    Examples:
      | URL                                                                          |
      | http://carhanld1.trtc.com:8000/ExtAssrtmnt/api/relativeInventory/1020/10986/ |

  @qa
  @post
  Scenario Outline: post Request Calls in QA Environment
    When I verify the post method for this URI "<URI>" with this endpoint "<endpoint>" and request file with name "<filename>"
    Examples:
      | URI                                         | endpoint       | filename          |
      | http://fithanld01.trtc.com:8000/Fitment_2_0 | /Logic/backup/ | fitment_POST.json |

  @qa
  @post_getYear
  Scenario Outline: post Request Calls in QA Environment for getYear PDL
    When I verify the post method for this URI "<URI>" with this endpoint "<endpoint>" and request file with name "<filename>"
    Examples:
      | URI                                 | endpoint  | filename          |
      | http://fithanlq01.trtc.com:8000/PDL | /getYear/ | getYear_POST.json |

  @qa
  @post_getMakeModel
  Scenario Outline: post Request Calls in QA Environment for getMakeModel PDL
    When I verify the post method for this URI "<URI>" with this endpoint "<endpoint>" and request file with name "<filename>"
    Examples:
      | URI                                 | endpoint       | filename          |
      | http://fithanlq01.trtc.com:8000/PDL | /getMakeModel/ | getYear_POST.json |

  @qa
  @post_getTrims
  Scenario Outline: post Request Calls in QA Environment for getTrims PDL
    When I verify the post method for this URI "<URI>" with this endpoint "<endpoint>" and request file with name "<filename>"
    Examples:
      | URI                                 | endpoint   | filename           |
      | http://fithanlq01.trtc.com:8000/PDL | /getTrims/ | getTrims_POST.json |

  @qa
  @post_getInventory
  Scenario Outline: post Request Calls in QA Environment for getInventory PDL
    When I verify the post method for this URI "<URI>" with this endpoint "<endpoint>" and request file with name "<filename>"
    Examples:
      | URI                                                                | endpoint | filename               |
      | http://carhanlq1.car.trtc.com:8040/Inventory/api/getInventory.xsjs |          | getInventory_POST.json |

  @qa
  @post_getFitment
  Scenario Outline: post Request Calls in QA Environment for getFitment PDL
    When I verify the post method for this URI "<URI>" with this endpoint "<endpoint>" and request file with name "<filename>"
    Examples:
      | URI                                            | endpoint | filename             |
      | http://fithanlq01:8000/PDL/api/getFitment.xsjs |          | getFitment_POST.json |

  @stg
  @post_getFitment
  Scenario Outline: post Request Calls in STG Environment for getFitment PDL
    When I verify the post method for this URI "<URI>" with this endpoint "<endpoint>" and request file with name "<filename>"
    Examples:
      | URI                                                          | endpoint | filename             |
      | https://stg.fitment.discounttire.com/PDL/api/getFitment.xsjs |          | getFitment_POST.json |


  @qa
  @solr
  Scenario Outline: post Request Calls in QA Environment for solr
    When I verify the post method for this URI "<URI>" with this endpoint "<endpoint>" and request file with name "<filename>"
    Examples:
      | URI                                                                              | endpoint | filename       |
      | https://dtqa1.epic.discounttire.com/extovcocc/v2/discounttire/pdl/products/query |          | solr_POST.json |

  @stg
  @solr
  Scenario Outline: post Request Calls in STG Environment for solr
    When I verify the post method for this URI "<URI>" with this endpoint "<endpoint>" and request file with name "<filename>"
    Examples:
      | URI                                                                         | endpoint | filename       |
      | https://ccstg.discounttire.com/extovcocc/v2/discounttire/pdl/products/query |          | solr_POST.json |
    
    @vtv
    Scenario Outline: VTV Get Service
      When I make a get request with vin "<VIN>" to vtv car service and return response
      Examples:
      |VIN|
      |WBA3A5G53ENP27445|