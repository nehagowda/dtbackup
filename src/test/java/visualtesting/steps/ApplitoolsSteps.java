package visualtesting.steps;

import cucumber.api.java.en.Then;
import visualtesting.pages.Applitools;
import utilities.Driver;

/**
 * Created by aaronbriel on 5/3/17.
 */
public class ApplitoolsSteps {

    private Applitools applitools;

    public ApplitoolsSteps(Driver driver) {
        applitools = new Applitools(driver);
    }

    @Then("^I verify the Window \"(.*?)\" for Batch \"(.*?)\", App \"(.*?)\" and Test \"(.*?)\"$")
    public void i_verify_the_ui_window_in_applitools(String window, String batch, String app, String test)
            throws Throwable {
        applitools.verifyWindowApplitools(window, batch, app, test);
    }

    @Then("^I verify the Region \"(.*?)\" for Batch \"(.*?)\", App \"(.*?)\" and Test \"(.*?)\"$")
    public void i_verify_the_ui_region_in_applitools(String region, String batch, String app, String test)
            throws Throwable {
        applitools.verifyRegionApplitools(region, batch, app, test);
    }

    @Then("^I specify visual test batch id \"([^\"]*)\" for test name \"([^\"]*)\" for \"([^\"]*)\"$")
    public void i_specify_visual_test_batch_id_for_test_name_for(String batch, String testName, String application) throws Throwable {
        applitools.specifyApplitoolsBatchId(batch, testName, application);
    }

    @Then("^I close the connection for visual test$")
    public void i_close_the_connection_for_visual_test() throws Throwable {
        applitools.closeApplitoolsConnection();
    }

    @Then("^I use Eyes to verify the Page \"([^\"]*)\" displays$")
    public void i_use_eyes_to_verify_the_page_displays(String page) throws Throwable {
        applitools.verifyPageApplitools(page);
    }

    @Then("^I use Eyes to verify the entire Page \"([^\"]*)\" displays$")
    public void i_use_eyes_to_verify_entire_the_page_displays(String page) throws Throwable {
        applitools.enableFullPageApplitools(page);
    }

    @Then("^I use Eyes to verify the \"([^\"]*)\" region of Page \"([^\"]*)\" displays$")
    public void i_use_eyes_to_verify_the_region_of_page_displays(String text,String page) throws Throwable {
        applitools.verifyRegionApplitools(text, page);
    }

    @Then("^I use Eyes to verify the storybook components displayed$")
    public void i_use_eyes_to_verify_storybook_components_displays() throws Throwable {
        applitools.verifyStorybookComponents();
    }
}