package pdl.data;

import common.Constants;
import dtc.data.Customer;

import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 6/28/17.
 */
public class Tire {

    private final Logger LOGGER = Logger.getLogger(Tire.class.getName());
    public String itemId;
    public String vendor;
    public String model;
    public String size;
    public String price;
    public final static String ITEM = "ITEM_";

    private enum TireEnum {
        ITEM_19600,
        ITEM_32491,
        ITEM_36251,
        ITEM_26043,
        ITEM_28449;

        public static TireEnum tireForName(String tire) throws IllegalArgumentException {
            for (TireEnum c : values()) {
                if (c.toString().equalsIgnoreCase(tire)) {
                    return c;
                }
            }
            throw tireNotFound(tire);
        }

        private static IllegalArgumentException tireNotFound(String outcome) {
            return new IllegalArgumentException(("Invalid customer [" + outcome + "]"));
        }
    }

    public Tire() {
    }


    /**
     * Stores and returns specific product (tire) data based on the item ID passed in
     *
     * @param tireEnum Tire based on Item ID
     * @return Tire values
     */
    public Tire getTire(String tireEnum) {
        TireEnum productEnum = TireEnum.tireForName(tireEnum);
        Tire tire = new Tire();

        switch (productEnum) {
            case ITEM_19600:
                tire.itemId = "19600";
                tire.vendor = "Michelin";
                tire.model = "Premier A/S";
                tire.size = "195/65 R15";
                tire.price = "106";
                break;
            case ITEM_32491:
                tire.itemId = "32491";
                tire.vendor = "Michelin";
                tire.model = "Defender T + H";
                tire.size = "195/65 R15";
                tire.price = "95";
                break;
            case ITEM_36251:
                tire.itemId = "36251";
                tire.vendor = "Bridgestone";
                tire.model = "Potenza S-04 Pole Position";
                tire.size = "265 /35 R18 97Y XL BSW";
                tire.price = "204";
                break;
            case ITEM_26043:
                tire.itemId = "26043";
                tire.vendor = "Michelin";
                tire.model = "PILOT SPORT A/S 3 PLUS";
                tire.size = "265 /35 R18 97Y XL BSW";
                tire.price = "218";
                break;
            case ITEM_28449:
                tire.itemId = "28449";
                tire.vendor = "Falken";
                tire.model = "Pro G4 A/S";
                tire.size = "195 /65 R15 91H SL BSW";
                tire.price = "93";
                break;
        }
        return tire;
    }

    /**
     * Returns tire data based on item id
     *
     * @param tire tire to get data string for
     * @return String of product data
     */
    public String getProductDataString(Tire tire) {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty(Constants.LINE_SEPARATOR_SYSTEM_PROPERTY);
        Field[] fields = tire.getClass().getDeclaredFields();
        result.append(newLine);

        //print field names paired with their values if not null
        for (Field field : fields) {
            try {
                if (field.get(tire) != null) {
                    result.append("  ");
                    result.append(field.getName());
                    result.append(": ");
                    result.append(field.get(tire));
                    result.append(newLine);
                }
            } catch (IllegalAccessException ex) {
                LOGGER.info(ex.toString());
            }
        }

        return result.toString();
    }

}
