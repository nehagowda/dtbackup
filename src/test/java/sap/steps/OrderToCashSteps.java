package sap.steps;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sap.pages.CommonActions;
import sap.pages.OrderToCash;
import sap.pages.Vehicle;
import utilities.Driver;

public class OrderToCashSteps {

    private Driver driver;
    private OrderToCash orderToCash;
    private CommonActions commonActions;
    private Scenario scenario;

    public OrderToCashSteps(Driver driver) {
        this.driver = driver;
        orderToCash = new OrderToCash(driver);
        commonActions = new CommonActions(driver);
    }

    @When("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in order to Cash page$")
    public void i_enter_information_on_in_order_to_cash_page(String value, String element) throws Throwable {
        commonActions.sendKeys(orderToCash.returnElement(element), value);
    }

    @When("^I click on \"([^\"]*)\" in order to cash page$")
    public void i_click_on_element(String elementName) throws Throwable {
        orderToCash.clickInOrderToCashPage(elementName);
    }

    @Then("^I verify \"([^\"]*)\" is displayed in order to cash page$")
    public void i_verify_is_element_displayed(String elementName) throws Throwable {
        driver.isElementDisplayed(orderToCash.returnElement(elementName));
    }

    @When("^I check for an empty field and enter \"([^\"]*)\" information on \"([^\"]*)\" in order to Cash page where flag \"([^\"]*)\"$")
    public void i_check_for_an_empty_field_and_enter_information_on_in_order_to_cash_page(String value, String elementName, String set) throws Throwable {
        orderToCash.clickClearAndInputKeysWhenEmpty(elementName, value,set);
    }

    @When("^I select \"([^\"]*)\" information on \"([^\"]*)\" on order to Cash page$")
    public void i_select_information_on_on_order_to_Cash_page(String value, String elementName) throws Throwable {
        commonActions.selectsValueFromDropDown(value, orderToCash.returnElement(elementName));
    }

    @When("^I double click \"([^\"]*)\"$")
    public void i_double_click(String elementName) throws Throwable {
        commonActions.doubleClickOnElement(orderToCash.returnElement(elementName));
    }

    @Then("^I verify \"([^\"]*)\" matches \"([^\"]*)\" displayed in order to cash page$")
    public void i_verify_matches_displayed_in_order_to_cash_page(String elementName, String text) throws Throwable {
        orderToCash.assertTextDisplayedInOrderToCash(elementName, text);
    }

    @When("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in New Customer Page$")
    public void i_enter_information_on_in_new_customer_page(String value, String elementName) throws Throwable {
        commonActions.sendKeysWithLabelName(elementName, commonActions.createUniqueValue(value));
    }

    @When("^I save the \"([^\"]*)\" number$")
    public void i_save_the_number(String empID) throws Throwable {
        orderToCash.saveEmpID(empID);
    }

    @When("^I enter previously saved Emp ID in \"([^\"]*)\" field$")
    public void i_enter_previously_saved_emp_id_in_field(String element) throws Throwable {
        String value = orderToCash.getEmployeeID();
        commonActions.sendKeysWithLabelName(element, value);
    }

    @When("^I input \"([^\"]*)\" as name$")
    public void i_input_name(String name) throws Throwable {
        orderToCash.inputName(name);
    }
}