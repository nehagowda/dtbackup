package sap.steps;

import common.Config;
import common.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sap.pages.CommonActions;
import sap.pages.IDocPage;
import sap.pages.PosDm;
import utilities.Driver;

public class PosDmSteps {

    private Driver driver;
    private PosDm posDm;
    private CommonActions commonActions;
    private IDocPage iDocPage;

    public PosDmSteps(Driver driver) {
        this.driver = driver;
        posDm = new PosDm(driver);
        iDocPage = new IDocPage(driver);
        commonActions = new CommonActions(driver);
    }

    @When("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in Posdm page$")
    public void i_enter_information_on_in_order_to_posDm_page(String value, String element) throws Throwable {
        commonActions.sendKeys(posDm.returnElement(element), value);
    }

    @When("^I enter the Transaction Number generated in legacy system in \"([^\"]*)\" field in Posdm$")
    public void i_enter_the_transaction_number_generated_in_legacy_system_in_posdm(String elementName) throws Throwable {
        String input = driver.scenarioData.getData(IDocPage.IDOC_NUMBER_FROM_EXCEL).replaceAll("[^0-9]", "");
        commonActions.sendKeys(posDm.returnElement(elementName), input);
    }

    @When("^I enter the web order number into \"([^\"]*)\" field in Posdm$")
    public void enter_web_order_number_into_field(String elementName) throws Throwable {
        commonActions.sendKeys(posDm.returnElement(elementName), ""+driver.scenarioData.getCurrentOrderNumber()+"");
    }

    @When("^I enter the Transaction Number for layaway generated in legacy system in \"([^\"]*)\" field in PosdM$")
    public void i_enter_the_transaction_number_with_generated_in_legacy_system_in_posdm(String elementName) throws Throwable {
        commonActions.sendKeys(posDm.returnElement(elementName), "*"+driver.scenarioData.getData(IDocPage.IDOC_NUMBER_FROM_EXCEL)+"*");
    }

    @When("^I click on \"([^\"]*)\" in posdm page$")
    public void i_click_on_element(String elementName) throws Throwable {
        posDm.clickInPosDmPage(elementName);
    }

    @Then("^I verify \"([^\"]*)\" is displayed in posdm page$")
    public void i_verify_is_element_displayed(String elementName) throws Throwable {
        driver.isElementDisplayed(posDm.returnElement(elementName));
    }

    @Then("^I verify all the transactions are successful$")
    public void i_verify_all_the_transactions_are_successful() throws Throwable {
        posDm.verifyIfTransactionsAreSuccessful();
    }

    @Then("^I verify Article Identifier and Sales Price for Line items in sales data$")
    public void i_verify_article_identifier_and_sales_price_for_line_in_sales_data() throws Throwable {
        posDm.assertArticleAndSalesPriceOnSalesItemDataInPosdm();
    }

    @When("^I double click on Transaction Number$")
    public void i_double_click_on_transaction_number() throws Throwable {
        commonActions.doubleClickOnPartialText(driver.scenarioData.getData(IDocPage.IDOC_NUMBER_FROM_EXCEL));
    }

    @Then("^I verify \"([^\"]*)\" field has the text of \"([^\"]*)\" in posdm$")
    public void i_verify_field_has_the_text_of_in_posdm(String elementName, String validation) throws Throwable {
        driver.jsScrollToElement(posDm.returnElement(elementName));
        driver.verifyTextDisplayedCaseInsensitive(posDm.returnElement(elementName), validation);
    }

    @Then("^I verify \"([^\"]*)\" field has the text of \"(Address Line|City|State|ZIP|Screenshot folder|Sheet name|Last Name|Layaway Amount|Transaction Type)\" in posdm/posdt$")
    public void i_verify_field_has_the_text_of_in_posdm_posdt(String elementName, String dataType) throws Throwable {
        driver.verifyTextDisplayedCaseInsensitive(posDm.returnElement(elementName), commonActions.returnDataFromEnvironmentVariables(dataType));
    }

    @Then("^I verify customer data for field \"([^\"]*)\"$")
    public void i_verify_customer_data_for_field(String elementName) throws Throwable {
        driver.verifyTextDisplayedCaseInsensitive(posDm.returnElement(elementName), driver.scenarioData.getData(elementName));
    }

    @When("^I click \"([^\"]*)\" \"([0-9]*)\" times in posdm$")
    public void i_click_element_times( String elementName, int numberOfTimes) throws Throwable {
        posDm.clickMultipleTimes(numberOfTimes,elementName);
    }

    @When("^I click \"([^\"]*)\" till \"([^\"]*)\" is visible in posdm$")
    public void i_click_element_till_visible( String scrollElementName, String visibleElementName) throws Throwable {
            posDm.scrollTillElementIsVisible(visibleElementName, scrollElementName);
    }

    @When("^I clear text from \"([^\"]*)\" in SAP$")
    public void i_clear_text_from_field_in_SAP(String elementName) throws Throwable {
        commonActions.clearInput(posDm.returnElement(elementName));
    }

    @When("^I double click \"([^\"]*)\" in posdm$")
    public void i_double_click(String elementName) throws Throwable {
        commonActions.doubleClickOnElement(posDm.returnElement(elementName));
    }

    @When("^I select \"([^\"]*)\" and enter \"([^\"]*)\" in PosDm page$")
    public void i_select_and_enter_in_vehicle_page(String elementName, String input) throws Throwable {
        posDm.sendKeysInPosDmPage(elementName, input);
    }

    @And("^I scroll to following \"([^\"]*)\" element in PosDm page$")
    public void i_scroll_to_following_element(String elementName) throws Throwable {
        driver.jsScrollToElementClick(posDm.returnElement(elementName));
    }

    @When("^I save the \"([^\"]*)\" element text value in PosDm page$")
    public void i_save_the_element_text_value_in_posdm_page(String elementName) throws Throwable {
        posDm.saveElementValueFromText(elementName);
    }

    @When("^I save the \"([^\"]*)\" element text value for web$")
    public void i_save_the_element_text_value_in_posDm_page_value_attribute(String elementName) throws Throwable {
        posDm.saveElementValueFromValueAttribute(elementName);
    }

    @When("^I save the Idoc Number and assign it to WPUUMS/WPUTAB key in se16n$")
    public void i_save_element_value_attribute_in_se16n() throws Throwable {
        driver.scenarioData.setData(posDm.returnElement(PosDm.MESSAGE_TYPE_SE16N).getText(),
                posDm.returnElement(PosDm.IDOC_ROW_SE16N).getText());
    }

    @When("^I save the \"([^\"]*)\" field's Idoc Number and assign it to \"([^\"]*)\" key$")
    public void i_save_idoc_number_and_assign_it_to_key(String elementName, String key) throws Throwable {
        driver.scenarioData.setData(key, posDm.returnElement(elementName).getText());
    }

    @When("^I enter saved \"([^\"]*)\" element value into following element \"([^\"]*)\"$")
    public void i_enter_saved_element_value_into_following_element(String key, String elementName) throws Throwable {
        posDm.enterSavedValuesIntoElement(key, elementName);
    }

    @When("^I confirm the job is complete$")
    public void i_confirm_the_job_is_complete() throws Throwable {
        posDm.aggregateJobConfirmation();
    }

    @Then("^I verify total transaction amount in posdm/posdt$")
    public void i_verify_total_transaction_amount_in_posdm_posdt() throws Throwable {
        posDm.verifyTotalTransactionAmount(commonActions.
                returnDataFromEnvironmentVariables(CommonActions.SHEET_NAME), Constants.fullPathFileNames.get(Constants.INVOICE_SAP));
    }

    @Then("^I verify total transaction amount in posdm/posdt from sheet name \"([^\"]*)\"$")
    public void i_verify_total_transaction_amount_in_posdm_posdt_from_examples(String sheetName) throws Throwable {
        posDm.verifyTotalTransactionAmount(sheetName, Constants.fullPathFileNames.get(Constants.INVOICE_SAP));
    }

    @Then("^I verify total transaction amount in posdm/posdt from sheet name \"([^\"]*)\" and file name \"([^\"]*)\"$")
    public void i_verify_total_transaction_amount_in_posdm_posdt_from_sheet_name_and_file_name(String sheetName, String fileName) throws Throwable {
        posDm.verifyTotalTransactionAmount(sheetName, Constants.DATA_STORAGE_FOLDER + fileName);
    }

    @When("^I expand date column in posdm$")
    public void i_expand_date_column_in_posdm() throws Throwable {
        posDm.expandDateColumn();
    }

    @When("^I expand first store node in posdm$")
    public void i_expand_first_store_node_in_posdm() throws Throwable {
        posDm.expandFirstStoreNode();
    }

    @When("^I expand date column for date from variant in posdm$")
    public void i_expand_date_column_for_date_in_posdm() throws Throwable {
        posDm.expandDateColumnNoDate();
    }

    @Then("^I verify Article Identifier for Line items in sales data in posdm$")
    public void i_verify_article_identifier_for_line_in_sales_data() throws Throwable {
        posDm.assertArticlesItemDataInPosDt();
    }

    @When("^I wait for execution to be completed$")
    public void i_wait_for_execution_to_be_completed() throws Throwable {
        posDm.waitForBwOrCarToExecute();
    }

    @When("^I extract \"([^\"]*)\" from excel sheet which is in \"([0-9]*)\"th row and \"([0-9]*)\"th cell from sheet name \"([^\"]*)\" and excel name \"([^\"]*)\"$")
    public void i_extract_invoice_number_from_excel_sheet(String key, int rowNumber, int cellNumber, String sheetName, String excelName) throws Throwable {
        iDocPage.readFromExcelAndReturnSingleValue(PosDm.POSDM_SCRIPTS_DATA_LOCATION + "\\" + excelName, sheetName, rowNumber, cellNumber, key);
    }

    @When("^I extract all values from excel from sheet name \"([^\"]*)\" and excel name \"([^\"]*)\"$")
    public void i_extract_all_values_from_excel_sheet(String sheetName, String excelName) throws Throwable {
        iDocPage.readAllInvoiceDetailsFromExcel(PosDm.POSDM_SCRIPTS_DATA_LOCATION + "\\" + excelName, sheetName);
    }

    @When("^I search for hybris order number and save it to scenario data$")
    public void i_search_for_hybris_order_number_and_save_it_to_scenario_data() throws Throwable {
        posDm.searchForhybrisOrderRelatedTransaction();
    }

    @When("^I enter start time and end time in posdm$")
    public void i_enter_start_time_and_end_time_in_posdm() throws Throwable {
        posDm.enterStartAndEndDateInPosDm(PosDm.TIME_STAMP_BEFORE_HASP_MAP_KEY, PosDm.TIME_STAMP_AFTER_HASP_MAP_KEY);
    }

    @When("^I enter start time and end time from environment variables in posdm$")
    public void i_enter_start_time_and_end_time_from_environment_variables_in_posdm() throws Throwable {
        posDm.enterStartAndEndDateInPosDm();
    }

    @Then("^I verify articles and values in posdm for 1014, 1015 and 1003 transaction types$")
    public void i_verify_articles_and_values_in_posdm_for_transaction_types() throws Throwable {
        posDm.assertArticlesAndAmountForSalesOrderIntegrationInPosDm();
    }

    @When("^I enter site numbers for DTD in posdm$")
    public void i_enter_site_numbers_for_dtd_in_posdm() throws Throwable {
        posDm.enterSiteNumberForDTD();
    }

    @When("^I enter store number for DT orders in posdm$")
    public void i_enter_store_number_for_dt_orders_in_posdm() throws Throwable {
        posDm.enterStoreNumberFromJsonResponse();
    }

    @When("^I enter store number from environment variables$")
    public void i_enter_store_number_from_environmental_variables() throws Throwable {
        posDm.enterStoreNumberFromEnvironmentVariables();
    }

    @When("^I enter BOPIS web order number into field name with label \"([^\"]*)\"$")
    public void i_enter_bopis_web_order_number_into_field_name_with_label(String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, driver.scenarioData.getCurrentOrderNumber());
    }

    @When("^I enter BOPIS web order number from environmental variables into field name with label \"([^\"]*)\"$")
    public void i_enter_bopis_web_order_number_from_environmental_variables_into_field_name_with_label(String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, Config.getHybrisWebOrderId());
    }

    @When("^I enter Date into field name with label \"([^\"]*)\"$")
    public void i_enter_date_into_field_name_with_label(String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, driver.scenarioData.getData(Constants.DATE));
    }

    @When("^I enter Date from Environment Variables into field name with label \"([^\"]*)\"$")
    public void i_enter_date_from_environment_variables_into_field_name_with_label(String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, Config.getTranDate());
    }

    @When("^I enter web order number into field name with label \"([^\"]*)\"$")
    public void i_enter_web_order_number_into_field_name_with_label(String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, driver.scenarioData.getCurrentOrderNumber());
    }

    @When("^I extract \"([^\"]*)\" Bopis orders from excel sheet which is in \"([0-9]*)\"th row and \"([0-9]*)\"th cell from sheet name \"([^\"]*)\" and excel name \"([^\"]*)\"$")
    public void i_extract_invoice_bopis_orders_number_from_excel_sheet(String key, int rowNumber, int cellNumber, String sheetName, String excelName) throws Throwable {
        iDocPage.readFromExcelAndReturnSingleValue(PosDm.BOPIS_SCRIPTS_DATA_LOCATION + "\\" + "SAP-" + Config.getDataSet() + "-" + excelName, sheetName, rowNumber, cellNumber, key);
    }

    @Then("^I verify if \"([^\"]*)\" field in Order History is empty$")
    public void i_verify_if_field_in_order_history_is_empty(String fieldNameKey) throws Throwable {
        posDm.assertOrderHistoryFieldValueIsEmpty(fieldNameKey);
    }

    @When("^I select variant \"([^\"]*)\"$")
    public void i_select_variant(String variant) throws Throwable {
        posDm.selectVariant(variant);
    }

    @When("^I check, analyze and fix errors for \"([^\"]*)\" type Total Record Transaction$")
    public void i_check_analyze_and_fix_errors_for_type_total_record_transaction(String transactionType) throws Throwable {
        posDm.checkAnalyzeAndCorrectErrorsFor1603(transactionType);
    }

    @When("^I check, analyze and fix errors for 1602 type Total Record Transaction$")
    public void i_check_analyze_and_fix_errors_for_1602_type_total_record_transaction() throws Throwable {
        posDm.checkAnalyzeAndCorrectErrorsFor1602();
    }

    @When("^I check, analyze and fix errors for 1601 type Total Record Transaction$")
    public void i_check_analyze_and_fix_errors_for_1601_type_total_record_transaction() throws Throwable {
        posDm.checkAnalyzeAndCorrectErrorsFor1601();
    }

    @When("^I check, analyze and fix errors for 1009 type layaway transaction$")
    public void i_check_analyze_and_fix_errors_for_1009_type_layaway_transaction() throws Throwable {
        posDm.checkAnalyzeAndCorrectErrorsFor1009();
    }
}