package sap.steps;

import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sap.pages.CommonActions;
import sap.pages.CommonTCodes;
import sap.pages.NetWeaverBusinessClient;
import sap.pages.PurchaseOrder;
import utilities.Driver;
import wm.pages.TransactionSearch;

/**
 * Created by mnabizadeh on 5/14/18.
 */
public class PurchaseOrderSteps {

    private Driver driver;
    private PurchaseOrder purchaseOrder;
    private CommonActions commonActions;
    private CommonTCodes commonTCodes;
    private NetWeaverBusinessClient netWeaverBusinessClient;
    private TransactionSearch transactionSearch;
    private Scenario scenario;

    public PurchaseOrderSteps(Driver driver) {
        this.driver = driver;
        purchaseOrder = new PurchaseOrder(driver);
        commonActions = new CommonActions(driver);
        commonTCodes = new CommonTCodes(driver);
        netWeaverBusinessClient = new NetWeaverBusinessClient(driver);
        transactionSearch = new TransactionSearch(driver);
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @Then("^I am brought to the page with the title \"([^\"]*)\"$")
    public void i_am_brought_to_the_page_with_the_title(String text) throws Throwable {
        purchaseOrder.verifyPageTitle(text);
    }

    @Then("^I verify the dropdown boxes are defaulted to \"(.*?)\" and \"(.*?)\"$")
    public void i_verify_the_drop_down_boxes_are_defaulted_to_defaults(String dropDownDefaultAction,
                                                                       String dropDownDefaultOrderType) throws Throwable {
        purchaseOrder.verifyDefaultDropDowns(dropDownDefaultAction, dropDownDefaultOrderType);
    }

    @When("^I enter the purchase order number in the entry field$")
    public void i_enter_the_purchase_order_number_in_the_entry_field() throws Throwable {
        purchaseOrder.enterPurchaseOrderNumber();
    }

    @When("^I enter the purchase order number from environment variables in the entry field$")
    public void i_enter_the_purchase_order_number_from_environement_variables_in_the_entry_field() throws Throwable {
        driver.scenarioData.setCurrentPurchaseOrderNumber(Config.getPo());
        purchaseOrder.enterPurchaseOrderNumber();
    }

    @Then("^I verify the purchase order number is in the page title$")
    public void i_verify_the_purchase_order_number_is_in_the_page_title() throws Throwable {
        purchaseOrder.verifyPageTitle(driver.scenarioData.getCurrentPurchaseOrderNumber());
    }

    @Then("^I verify the alert is displayed for the article document being posted$")
    public void i_verify_alert_displayed_for_article_document_posted() throws Throwable {
        purchaseOrder.verifyArticleDocumentPosted();
    }

    @And("^I save the article document number to scenario data$")
    public void i_save_the_article_document_number_to_scenario_data() throws Throwable {
        purchaseOrder.saveArticleDocumentNumberToScenarioData();
    }

    @And("^I send the shortcut for the \"Other Purchase Order\" page$")
    public void i_send_shortcut_for_other_purchase_order_page() throws Throwable {
        purchaseOrder.sendOtherPOShortcut();
    }

    @And("^I enter the purchase order number into the popup search field$")
    public void i_enter_purchase_order_number_into_popup_search_field() throws Throwable {
        String orderNumber = purchaseOrder.getPONumber();
        purchaseOrder.enterPurchaseOrderNumberInPopupSearch(orderNumber);
    }

    @Then("^I verify the alert is displayed for the store merch po being changed$")
    public void i_verify_alert_displayed_for_store_merch_po_change() throws Throwable {
        purchaseOrder.verifyStoreMerchPOChanged();
    }

    @When("^I loop through each article and fill out the Confirmation Header and Order Acknowledgement$")
    public void i_loop_each_article_and_fill_confirmation_heard_and_order_acknowledgement() throws Throwable {
        purchaseOrder.fillConfirmationHeaderAndOrderAcknowledgement();
    }

    @When("^I enter the purchase order value for \"(.*?)\" to \"(.*?)\" from preferred data source$")
    public void i_enter_purchase_order_value_for_field_to_value_from_preferred_data_source(String field, String value) throws Throwable {
        if(!Config.getSapDataSourceFlag().equalsIgnoreCase(CommonActions.DEFAULT)){
            value = commonActions.returnDataFromExcel(field);
        }
        purchaseOrder.enterValueIntoPOField(field, value);
    }

    @When("^I enter the purchase order value at row \"(.*?)\" for \"([^\"]*)\" to \"([^\"]*)\" from preferred data source$")
    public void i_enter_purchase_order_value_for_field_to_value_and_row_from_preferred_data_source(int rowNumber, String field, String value) throws Throwable {
        if (!Config.getSapDataSourceFlag().equalsIgnoreCase(CommonActions.DEFAULT)) {
            value = commonActions.returnDataFromExcel(field);
        }
        purchaseOrder.enterValuesIntoItemTableForPoCreation(field, value, rowNumber);
    }

    @When("^I enter the purchase order value for \"(.*?)\" to \"(.*?)\"$")
    public void i_enter_purchase_order_value_for_field_to_value(String field, String value) throws Throwable {
        purchaseOrder.enterValueIntoPOField(field, value);
    }

    @When("^I set the input for \"(.*?)\" at row number \"(.*?)\" to \"(.*?)\"$")
    public void i_enter_inputs_for_field_to_value_at_row_number(String field, int rowNumber, String value) throws Throwable {
        if (field.equalsIgnoreCase(CommonActions.SITE)) {
            if (value.equalsIgnoreCase(Constants.STORE)) {
                if (Config.getDataSet().equalsIgnoreCase(Constants.QA)) {
                    value = Constants.NWBC_RTK_SITE;
                } else if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    value = Constants.NWBC_RTQ_SITE;
                }
            }
        }
        purchaseOrder.enterValuesIntoItemTableForPoCreation(field, value, rowNumber);
    }

    @When("^I click \"(.*?)\" at row number \"(.*?)\"$")
    public void i_click_element_at_row_number(String field, int rowNumber) throws Throwable {
        purchaseOrder.clickElementOnItemTableForPoCreation(field, rowNumber);
    }

    @When("^I enter value for \"(.*?)\" to \"(.*?)\" using special function$")
    public void i_enter_value_for_field_to_value_using_special_function(String field, String value) throws Throwable {
        driver.performSendKeysWithActions(purchaseOrder.getPOInputField(field), value);
    }

    @When("^I select the PO type \"([^\"]*)\"$")
    public void i_select_the_PO_type(String poType) throws Throwable {
        purchaseOrder.inputPOTypeByText(poType);
    }

    @And("^I search the desired PO number$")
    public void i_search_the_desired_po_number() throws Throwable {
        String orderNumber = purchaseOrder.getPONumber();
        purchaseOrder.searchOtherPurchaseOrderNumber(orderNumber);
    }

    @Then("^I verify the Receipt Validation Purchase Order input defaults to the previously saved PO number$")
    public void i_verify_receipt_validation_purchase_order_default() throws Throwable {
        String purchaseOrderNumber = purchaseOrder.getPONumber();
        purchaseOrder.orderReceiptValidationPOSearchDefault(purchaseOrderNumber);
    }

    @And("^I select the Purchase Order History Tab for Receipt Validation$")
    public void i_select_purchase_order_history_tab() throws Throwable {
        purchaseOrder.selectPurchaseOrderHistoryTab();
    }

    @Then("^I verify the article document matches the previously saved number$")
    public void i_verify_the_article_document_matches_previous_saved_number() throws Throwable {
        String articleDocumentNumber = purchaseOrder.getArticleDocumentNumber();
        purchaseOrder.verifyArticleDocumentMatchesSavedNumber(articleDocumentNumber);
    }

    @When("^I select the article document link$")
    public void i_select_article_document_link() throws Throwable {
        purchaseOrder.selectArticleDocumentNumber();
    }

    @When("^I select the Display outputs button on the Outputs tab$")
    public void i_select_display_outputs_button_on_outputs_tab() throws Throwable {
        purchaseOrder.selectDisplayOutputsOnOutputsTab();
    }

    @When("^I select the email output process code row and the processing log button$")
    public void i_select_email_output_process_code_row_and_processing_log_button() throws Throwable {
        purchaseOrder.selectEmailLogOutputRowHeader();
    }

    @Then("^I verify the Goods Receipt price$")
    public void i_verify_goods_receipt_price() throws Throwable {
        String salesTotal = purchaseOrder.getSalesTotalNumber();
        purchaseOrder.verifyGoodsReceiptSaleTotal(salesTotal);
    }

    @Then("^I verify \"([^\"]*)\" matches text displayed$")
    public void i_verify_matches_text_displayed(String text) throws Throwable {
        driver.verifyTextDisplayed(purchaseOrder.textDisplayed, text);
    }

    @Then("^I verify correct vendor address is displayed$")
    public void i_verify_correct_vendor_address_is_displayed() throws Throwable {
        purchaseOrder.verifyVendorAddress();
    }

    @Then("^I verify \"([^\"]*)\" matches the \"([^\"]*)\" displayed information$")
    public void i_verify_matches_the_displayed_information(String value, String poLevelInfo) throws Throwable {
        purchaseOrder.assertPoLineItemValue(value, poLevelInfo);
    }

    @Then("^I extract the email validation IDoc number$")
    public void i_extract_the_email_validation_idoc_number() throws Throwable {
        purchaseOrder.extractIDocEmailTriggerNumber();
    }

    @Then("^I verify the automated purchase order number for \"([^\"]*)\" is in the page title$")
    public void i_verify_the_automated_purchase_order_number_for_is_in_the_page_title(String vendor) throws Throwable {
        String orderNumber = driver.scenarioData.getPOVendorData(vendor);
        purchaseOrder.verifyPageTitle(orderNumber);
    }

    @Then("^I verify \"([^\"]*)\" is listed in Partner function$")
    public void i_verify_is_listed_in_partner_function(String value) throws Throwable {
        purchaseOrder.assertPartnerFunctionValue(value);
    }

    @Then("^I verify reference matches the web order number$")
    public void i_verify_reference_matches_the_web_order_number() throws Throwable {
        purchaseOrder.verifyReferenceWebOrder();
    }

    @Then("^I verify ZNFY processing log$")
    public void i_verify_znfy_processing_log() throws Throwable {
        purchaseOrder.verifyZnfyProcessingLog();
    }

    @And("^I click on the \"(.*?)\" element in po page to navigate$")
    public void i_click_on_element_in_po_page(String element) throws Throwable {
        purchaseOrder.clickReturnedElementOnPurchaseOrderPage(element);
    }

    @Then("^I verify Detail data tab is open$")
    public void i_verify_detail_data_tab_is_open() throws Throwable {
        purchaseOrder.detailDataHeaderCollapse();
    }

    @Then("^I verify Head data tab is open$")
    public void i_verify_head_data_tab_is_open() throws Throwable {
        purchaseOrder.headerExpand();
    }

    @Then("^I verify Item detail tab is open$")
    public void i_verify_item_detail_tab_is_open() throws Throwable {
        purchaseOrder.itemDetailsExpand();
    }

    @And("^I enter purchase order number in the entry field with \"([^\"]*)\"$")
    public void i_enter_purchase_order_number_in_the_entry_field(String brand) throws Throwable {
        purchaseOrder.enterPurchaseOrderNumberWithBrand(brand);
    }

    @When("^I select \"([^\"]*)\" choice in Confirmations$")
    public void i_click_on_choice_in_confirmations(String choice) throws Throwable {
        purchaseOrder.choiceOfConfirmation(choice);
    }

    @When("^I save the updated document$")
    public void i_save_the_updated_document() {
        purchaseOrder.saveDocument();
    }

    @Then("^I verify the \"([^\"]*)\" field has \"([^\"]*)\" value in ME21N page$")
    public void i_verify_field_has_value_in_me21n_page(String elementName, String validation) throws Throwable {
        purchaseOrder.verifyValueAttributeInMe21n(elementName, validation);
    }

    @When("^I click on \"([^\"]*)\" in po page$")
    public void i_click_on_element(String elementName) throws Throwable {
        purchaseOrder.clickInPoPages(elementName);
    }

    @Then("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in Purchase order page$")
    public void i_enter_information_on_in_purchase_order_page(String value, String element) throws Throwable {
        commonActions.sendKeys(purchaseOrder.getPOInputField(element), value);
    }

    @Then("^I verify \"([^\"]*)\" element is displayed in Purchase order page$")
    public void i_verify_element_is_displayed_in_purchase_order_page(String elementName) throws Throwable {
        purchaseOrder.returnElementToClick(elementName).isDisplayed();
    }

    @Then("^I verify \"([^\"]*)\" element contains PO number in Purchase order page$")
    public void i_verify_element_contains_po_number_in_purchase_order_page(String elementName) throws Throwable {
        purchaseOrder.returnElementToClick(elementName).getText().contains(driver.scenarioData.getCurrentPurchaseOrderNumber());
    }

    @Then("^I verify if item Overview is open$")
    public void i_verify_if_item_overview_is_open() throws Throwable {
        purchaseOrder.itemOverviewHeaderCollapse();
    }

    @When("^I click on the confirmation tab in ME21N$")
    public void i_click_on_the_confirmation_tab() throws Throwable {
        purchaseOrder.clickOnConfirmationTabInMe21n();
    }

    @When("^I enter the PO value for \"([^\"]*)\" to \"([^\"]*)\", \"([^\"]*)\" to \"([^\"]*)\", \"([^\"]*)\" to \"([^\"]*)\" from preferred data source$")
    public void i_enter_the_po_value_at_row_for_to_to_to_from_preferred_data_source(String field1, String value1, String field2, String value2, String field3, String value3) throws Throwable {
        if (!Config.getSapDataSourceFlag().equalsIgnoreCase(CommonActions.DEFAULT)) {
            purchaseOrder.enterMultipleValuesInExcel(field1, field2, field3);
        } else {
            purchaseOrder.enterValueIntoPOField(field1, value1);
            purchaseOrder.enterValueIntoPOField(field2, value2);
            purchaseOrder.enterValueIntoPOField(field3, value3);
        }
    }

    @And("^I enter the Asset ID based on the environment$")
    public void i_enter_asset_id_based_on_the_environment() throws Throwable {
        purchaseOrder.setAssetId();
    }

    @When("^I save the \"([^\"]*)\" value from the page$")
    public void i_save_the_value_from_the_page(String elementName) throws Throwable {
        purchaseOrder.saveValueFromPage(elementName);
    }

    @When("^I enter saved \"([^\"]*)\" element value into following element \"([^\"]*)\" in the page$")
    public void i_enter_saved_element_value_into_following_element_in_page(String key, String elementName) throws Throwable {
        purchaseOrder.enterSavedValuesIntoElement(key, elementName);
    }

    @When("^I enter saved \"([^\"]*)\" element value into following table element \"([^\"]*)\" in the page$")
    public void i_enter_saved_element_value_into_following_table_element_in_page(String key, String elementName) throws Throwable {
        purchaseOrder.enterSavedValuesIntoTableElement(key, elementName);
    }

    @Then("^I validate \"([^\"]*)\" entered matches original \"([^\"]*)\" in order output")
    public void i_validate_entered_matches_original(String elementName, String value) throws Throwable {
        if (Config.getDataSet().equalsIgnoreCase(Constants.QA)) {
            commonActions.verifyTextAttribute(purchaseOrder.returnElementToClick(elementName),value);
        } else if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            commonActions.verifyValueAttribute(purchaseOrder.returnElementToClick(elementName),value);
        }

    }

    @Then("^I verify \"([^\"]*)\" element is checked in Purchase order page$")
    public void i_verify_element_is_checked_in_purchase_order_page(String elementName) throws Throwable {
        purchaseOrder.returnElementToClick(elementName).isDisplayed();
    }

    @Then("^I verify delivery completion$")
    public void iVerifyDeliveryCompletion() throws Throwable {
        purchaseOrder.deliveryCompleteCheck();
    }

    @Then("^I verify \"([^\"]*)\" element has same value as \"([^\"]*)\" element$")
    public void i_verify_element_has_same_value_as_element(String elementName, String key) throws Throwable {
        commonActions.verifyValueAttribute(purchaseOrder.getPOInputField(elementName), key);
    }

    @Then("^I verify \"([^\"]*)\" element has same value as \"([^\"]*)\"$")
    public void i_verify_element_has_same_value_as(String elementName, String key) throws Throwable {
        commonActions.verifySimilarDataFromTwoElements(purchaseOrder.getPOInputField(elementName), key);
    }

    @Then("^I verify \"([^\"]*)\" field is not empty$")
    public void i_verify_field_is_not_empty(String elementName) throws Throwable {
        commonActions.assertElementTextPopulated(purchaseOrder.getPOInputField(elementName));
    }

    @And("^I enter \"(past|present|future)\" date with \"([^\"]*)\" months into \"([^\"]*)\" element$")
    public void i_enter_date_with_months_into_element(String tense, int months, String elementName) throws Throwable {
        purchaseOrder.getsAndEntersDate(elementName, months, tense);
    }

    @When("^I enter the unique 5 digit identification code in f110$")
    public void i_enter_the_unique_5_digit_identification_code_in_f110() throws Throwable {
        purchaseOrder.generateAndEnterUniqueIdentificationCode();
    }

    @When("^I save the document number to the scenario data$")
    public void i_save_the_document_number_to_the_scenario_date() throws Throwable {
        purchaseOrder.findTheRightDataForAmount();
    }

    @When("^I enter the unique printout job name in \"([^\"]*)\" in f110$")
    public void i_enter_the_unique_printout_job_name_in_f110(String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, purchaseOrder.JOB_NAME_PART_1 + purchaseOrder.randomNumber);
    }

    @When("^I enter the document number into \"([^\"]*)\"$")
    public void i_enter_the_document_number_into_element(String elementName) throws Throwable {
        purchaseOrder.getAndEnterTheDocumentNumberWhichHasTransactionTotal(elementName);
    }

    @When("^I enter the Article document number into \"([^\"]*)\"$")
    public void i_enter_the_article_document_number_into_element(String elementName) throws Throwable {
        purchaseOrder.getAndEnterTheArticleDocumentNumber(elementName);
    }

    @When("^I double click on \"([^\"]*)\" element$")
    public void i_double_click_on_element(String elementName) throws Throwable {
        commonTCodes.scrollAndDoubleClickInSap(purchaseOrder.returnElementToClick(elementName));
    }

    @And("^I select both Lastchange columns using control key$")
    public void i_select_both_lastchange_columns_using_control_key() throws Throwable {
        purchaseOrder.selectTwoColumnsUsingControlKey();
    }

    @And("^I enter saved timestamps \"([^\"]*)\" file creation in \"([^\"]*)\" field$")
    public void i_enter_saved_time_stamps_file_creation_in_field(String timeStamp, String labelName) throws Throwable {
        purchaseOrder.enterSavedTimesBeforeAndAfterFileCreation(timeStamp, labelName);
    }

    @When("^I scroll and enter the purchase order value for \"(.*?)\" to \"(.*?)\" from preferred data source$")
    public void i_scroll_and_enter_purchase_order_value_for_field_to_value_from_preferred_data_source(String field, String value) throws Throwable {
        if(!Config.getSapDataSourceFlag().equalsIgnoreCase(CommonActions.DEFAULT)){
            value = commonActions.returnDataFromExcel(field);
        }
        purchaseOrder.scrollAndEnterValueIntoPOField(field, value);
    }

    @Then("^I collapse header tab$")
    public void i_collapse_header_tab() throws Throwable {
        purchaseOrder.headerCollapse();
    }

    @When("^I save the time \"([^\"]*)\" generating the file$")
    public void i_save_the_time_generating_the_file(String timeStamp) throws Throwable {
        commonActions.savesTimeStamp(timeStamp);
    }
}

