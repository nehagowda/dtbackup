package sap.steps;

import bo.pages.PriceCheck;
import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import sap.pages.CommonActions;
import sap.pages.CommonTCodes;
import sap.pages.IDocPage;
import sap.pages.MerchandiseCycle;
import utilities.CommonUtils;
import utilities.Driver;

public class MerchandiseCycleSteps {

    private Driver driver;
    private WebDriver webDriver;
    private MerchandiseCycle merchandiseCycle;
    private CommonActions commonActions;
    private CommonTCodes commonTCodes;
    private IDocPage iDocPage;
    private CommonUtils commonUtils;
    private Scenario scenario;
    private PriceCheck priceCheck;
    
    public MerchandiseCycleSteps(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        merchandiseCycle = new MerchandiseCycle(driver);
        commonActions = new CommonActions(driver);
        commonTCodes = new CommonTCodes(driver);
        iDocPage = new IDocPage(driver);
        commonUtils = new CommonUtils();
        priceCheck = new PriceCheck(driver);
    }

    @When("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in merchandise cycle page$")
    public void i_enter_information_on_in_merchandise_cycle_page(String value, String element) throws Throwable {
        commonActions.sendKeys(merchandiseCycle.returnElement(element), value);
    }

    @When("^I enter site information extracted from environment variables on \"([^\"]*)\" in merchandise cycle page$")
    public void i_enter_information_extracted_from_environment_variables_on_in_merchandise_cycle_page(String element) throws Throwable {
        commonActions.sendKeys(commonTCodes.returnInputElementBasedOnFieldName(Constants.SITE, Constants.FROM), Config.getSiteNumber());
    }

    @When("^I click on \"([^\"]*)\" in merchandise cycle page$")
    public void i_click_on_element_in_merchandise_cycle_page(String elementName) throws Throwable {
        merchandiseCycle.clickInMerchandiseCyclePage(elementName);
    }

    @Then("^I verify \"([^\"]*)\" is displayed in merchandise cycle page$")
    public void i_verify_is_element_displayed_in_merchandise_cycle_page(String elementName) throws Throwable {
        driver.isElementDisplayed(merchandiseCycle.returnElement(elementName));
    }

    @When("^I select \"([^\"]*)\" Parameter$")
    public void i_select_parameter(String option) throws Throwable {
        merchandiseCycle.clickOnDesiredParameterInMerchandiseCyclePage(option);
    }
    @Then("^I verify \"([^\"]*)\" matches \"([^\"]*)\" displayed in merchandise cycle page$")
    public void i_verify_matches_displayed_in_merchandise_cycle_page(String elementName, String text) throws Throwable {
        merchandiseCycle.assertTextDisplayedInMerchandiseCyclePage(elementName, text);
    }
    @Then("^I validate \"([^\"]*)\" entered matches original \"([^\"]*)\" in merchandise cycle page$")
    public void i_validate_entered_matches_original_in_merchandise_cycle_page(String elementName, String value) throws Throwable {
        merchandiseCycle.assertValDisplayedMatchesValExpectedInMerchandiseCyclePage(elementName, value);
    }

    @When("^I enter random number between \"([0-9]*)\" and \"([0-9]*)\" into \"([^\"]*)\" in merchandise cycle page$")
    public void i_enter_the_information_in_merchandise_cycle_page(int min,int max,String elementName ) throws Throwable {
        commonActions.sendKeys(merchandiseCycle.returnElement(elementName), Integer.toString(CommonUtils.getRandomNumber(min, max)));
    }

    @When("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in calculate competitor pricing page$")
    public void i_enter_information_on_in_calculate_competitor_pricing_page(String value, String element) throws Throwable {
        commonActions.sendKeys(merchandiseCycle.returnElement(element), value);
    }

    @When("^I verify \"([^\"]*)\" checkbox is \"([^\"]*)\"$")
    public void i_verify_checkbox_is(String element, String value) throws Throwable {
        commonActions.verifyClassAttribute(merchandiseCycle.returnElement(element) ,value);
    }

    @When("^I deselect \"([^\"]*)\" checkbox$")
    public void i_deselect_checkbox(String element) throws Throwable {
        commonActions.click(merchandiseCycle.returnElement(element));
    }

    @When("^I verify \"([^\"]*)\" has \"([^\"]*)\"$")
    public void i_verify_has(String element, String value) throws Throwable {
        commonActions.verifyValueAttribute(merchandiseCycle.returnElement(element) ,value);
    }

    @When("^I verify \"([^\"]*)\" is displayed in \"([^\"]*)\" field$")
    public void i_verify_is_displayed_in_field(String value, String element) throws Throwable {
        commonActions.verifyValueAttribute(merchandiseCycle.returnElement(element) ,value);
    }
    @When("^I verify \"([^\"]*)\" displayed yellow triangle$")
    public void i_verify_displayed_yellow_triangle(String element) throws Throwable {
        commonActions.verifyElementDisplays(merchandiseCycle.returnElement(element));
    }

    @When("^I enter \"([^\"]*)\" to be added to todays date in \"([^\"]*)\" in calculate competitor pricing page$")
    public void i_enter_to_be_added_to_todays_date_in_in_calculate_competitor_pricing_page(int days, String element) throws Throwable {
        commonActions.sendKeysWithLabelName(element, driver.getFutureDate(days));
    }

    @Then("^I save the order number to the \"([^\"]*)\" of the \"([^\"]*)\" tab of the \"([^\"]*)\" excel file for updated price$")
    public void i_save_the_order_number_to_the_of_the_tab_of_the_excel_file_for_updated_price(String location, String tab, String file) throws Throwable {
        file = commonActions.getFullPathFileNameSap(file);
        boolean append = false;
        if (location.equalsIgnoreCase("end")) {
            append = true;
        }
        merchandiseCycle.storePriceToExcel(file, tab, append);
    }

    @When("^I enter new price information for \"([^\"]*)\"$")
    public void i_enter_information_on_in_merchandise_cycle_page_for(String element) throws Throwable {
        merchandiseCycle.sendKeysPriceWithType(merchandiseCycle.returnElement(element));
    }

    @Then("^I save the new price for \"([^\"]*)\"$")
    public void i_save_the_new_price_for(String priceType) throws Throwable {
        String updatedPrice = merchandiseCycle.returnPrice(priceType);
        driver.scenarioData.setCurrentPrice(updatedPrice);
    }

    @Then("^I verify new price is displayed in \"([^\"]*)\" field$")
    public void i_verify_new_price_is_displayed_in_field(String element) throws Throwable {
        String newPrice = merchandiseCycle.getNewPrice();
        merchandiseCycle.scroll(merchandiseCycle.returnElement(element), MerchandiseCycle.scrollRight);
        commonActions.verifyTextAttribute(merchandiseCycle.returnElement(element), newPrice);
    }

    @When("^I extract the values from excel and input all articles in MARD table in se16n$")
    public void i_extract_the_values_from_excel_and_input_all_articles_in_mard_table_in_se1n() throws Throwable {
        merchandiseCycle.keyInAllArticlesInMardTable();
    }

    @When("^I check and store the value of inventory which should be added to a specific article$")
    public void i_store_the_value_of_inventory_which_should_be_added_to_a_specific_article() throws Throwable {
        merchandiseCycle.checkAndStoreInventory();
    }

    @When("^I enter all articles and values in the table$")
    public void i_enter_all_articles_and_values_in_the_table() throws Throwable {
        merchandiseCycle.inputArticlesToUpdateInventory();
    }

    @Then("^I verify new price is displayed in \"([^\"]*)\" field in the backoffice page$")
    public void i_verify_new_price_is_displayed_in_field_in_the_backoffice_page(String element) throws Throwable {
        String newPrice = merchandiseCycle.getNewPrice();
        commonActions.verifyValueAttribute(priceCheck.returnElement(element), newPrice);
    }

    @When("^I enter \"([^\"]*)\" for the \"([^\"]*)\" subordinate article$")
    public void i_enter_for_the_subordinate_article(String groupId, String position) throws Throwable {
        driver.performSendKeysWithActions(merchandiseCycle.returnElement(position), groupId);
    }

    @When("^I select the checkbox for the \"([^\"]*)\" subordinate article$")
    public void i_select_the_checkbox_for_the_subordinate_article(String position) throws Throwable {
        commonActions.click(merchandiseCycle.returnElement(position));
    }

    @When("^I enter price relationship as \"([^\"]*)\" for the \"([^\"]*)\" subordinate article$")
    public void i_enter_price_relationship_as_for_the_subordinate_article(String priceRelationship, String position) throws Throwable {
        driver.performSendKeysWithActions(merchandiseCycle.returnElement(position),priceRelationship);
    }

    @Then("^I compare the updated price from SAP to price in Hybris$")
    public void i_compare_the_updated_price_from_sap_to_price_in_hybris() throws Throwable {
        merchandiseCycle.compareSapToHybrisPrice();
    }

    @When("^I click on \"([^\"]*)\" in Competitor Pricing page$")
    public void i_click_on_in_competitor_pricing_page(String elementName) throws Throwable {
        merchandiseCycle.clickInMerchandiseCyclePage(elementName);
    }

    @When("^I set the input for single value at row number \"([^\"]*)\" to \"([^\"]*)\"$")
    public void i_set_the_input_for_at_row_number_to(String rowNumber, String value) throws Throwable {
        commonActions.enterValuesIntoItemTable(value, rowNumber);
    }

    @When("^I save matched competitor$")
    public void i_save_matched_competitor() throws Throwable {
        String matchedCompetitorValue = merchandiseCycle.saveMatchedCompetitor();
        driver.scenarioData.setMatchedCompetitor(matchedCompetitorValue);
    }

    @When("^I click the box at \"([^\"]*)\" row number \"([^\"]*)\"$")
    public void i_click_the_box_at_row_number(String field, String row) throws Throwable {
        commonActions.selectDeselectFields(field, row);
    }

    @When("^I select the new competitor$")
    public void i_select_the_new_competitor() throws Throwable {
        String currentMatchedCompetitor = driver.scenarioData.getMatchedCompetitor();
        merchandiseCycle.setNewMatchedCompetitor(currentMatchedCompetitor);

    }
}