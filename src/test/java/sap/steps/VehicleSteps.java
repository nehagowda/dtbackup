package sap.steps;

import common.Constants;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.Keys;
import sap.pages.CommonActions;
import sap.pages.Vehicle;
import utilities.Driver;

public class VehicleSteps {
    private Vehicle vehicle;
    private Driver driver;
    private CommonActions commonActions;

    public VehicleSteps(Driver driver) {
        this.driver = driver;
        vehicle = new Vehicle(driver);
        commonActions = new CommonActions(driver);
    }

    @When("^I click on \"([^\"]*)\" in vehicle page$")
    public void i_click_on_element_in_vehicle_page(String elementName) throws Throwable {
        vehicle.clickInVehiclePage(elementName);
    }

    @When("^I hit F4 action")
    public void i_hit_f4_action() throws Throwable {
        driver.performKeyAction(Keys.F4);
    }

    @When("^I scroll to following \"([^\"]*)\" element and click")
    public void i_scroll_to_following_element_and_click(String elementName) throws Throwable {
        vehicle.scrollToElementAndClick(elementName);
    }

    @Then("^I verify \"([^\"]*)\" matches \"([^\"]*)\" displayed in vehicle page$")
    public void i_verify_matches_displayed_in_vehicle_page(String elementName, String text) throws Throwable {
        vehicle.assertTextDisplayedInVehicle(elementName, text);
    }

    @When("^I select \"([^\"]*)\" and enter \"([^\"]*)\" in vehicle page$")
    public void i_select_and_enter_in_vehicle_page(String elementName, String input) throws Throwable {
        vehicle.sendKeysInVehiclePage(elementName, input);
    }

    @Then("^I validate \"([^\"]*)\" entered matches original \"([^\"]*)\" in vehicle page$")
    public void i_validate_entered_matches_original_in_vehicle_page(String elementName, String expectedText) throws Throwable {
        vehicle.assertTextDisplayedMatchesValExpectedInVehiclePage(elementName, expectedText);
    }

    @Then("^I select the \"([^\"]*)\" row in \"([^\"]*)\" table$")
    public void i_select_the_new_row(String elementName, String tableScrollBar) throws Throwable {
        vehicle.getLastRow(elementName, tableScrollBar).click();
    }

    @Then("^I validate \"([^\"]*)\" entered matches with saved data \"([^\"]*)\" in vehicle page$")
    public void i_validate_entered_matches_with_saved_data_in_vehicle_page(String elementName, String key) throws Throwable {
        vehicle.assertValueOnVehiclePage(elementName, key);
    }

    @When("^I save the \"([^\"]*)\" chassis number from \"([^\"]*)\" field$")
    public void i_save_the_chasis_number_from_field(String key, String field) throws Throwable {
        vehicle.saveChassisNumbers(key, field);
    }

    @Then("^I verify if \"([^\"]*)\" saved number and \"([^\"]*)\" saved number are in right sequence$")
    public void i_verify_if_saved_number_are_in_right_sequence(String key1, String key2) throws Throwable {
        vehicle.verifyCountDifference(key1, key2);
    }

    @Then("^I select the following \"([^\"]*)\" index in \"([^\"]*)\"$")
    public void i_select_the_following_index_in(int index, String elementName) throws Throwable {
        vehicle.selectIndexRowByElement(index, elementName);
    }

    @When("^I save the \"([^\"]*)\" notes number from \"([^\"]*)\" field$")
    public void i_save_the_notes_number_from_field(String key, String elementName) throws Throwable {
        vehicle.saveNumbers(key, elementName);
    }

    @When("^I save the \"([^\"]*)\" value in vehicle page$")
    public void i_save_the_value_in_vehicle_list_page(String elementName) throws Throwable {
        vehicle.saveValueFromVehiclePage(elementName);
    }

    @When("^I enter saved \"([^\"]*)\" element value into following element \"([^\"]*)\" in vehicle page$")
    public void i_enter_saved_element_value_into_following_element_in_vehicle_page(String key, String elementName) throws Throwable {
        vehicle.enterSavedValuesIntoElement(key, elementName);
    }

    @Then("^I check if the \"([^\"]*)\" value and \"([^\"]*)\" value is the same$")
    public void i_check_if_the_value_and_value_is_the_same(String elementName1, String elementName2) throws Throwable {
        vehicle.compareVehicleTrimSubModel(elementName1, elementName2);
    }

    @Then("^I check if the \"([^\"]*)\" numeric value and \"([^\"]*)\" numeric value is the same$")
    public void i_check_if_the_numeric_value_and_numeric_value_is_the_same(String elementName1, String elementName2) throws Throwable {
        vehicle.compareVehicleSegmentCategoryIDS(elementName1, elementName2);
    }

    @When("^I save the \"([^\"]*)\" text in vehicle page$")
    public void i_save_the_text_in_vehicle_age(String elementName) throws Throwable {
        vehicle.saveTextFromVehiclePage(elementName);
    }

    @When("^I double click on \"([^\"]*)\" in vehicle page$")
    public void i_double_click_on_vehicle_page(String elementName) throws Throwable {
        commonActions.doubleClickStaleElement(vehicle.returnElement(elementName));
    }

    @Then("^I compare if the \"([^\"]*)\" value and \"([^\"]*)\" value are the same$")
    public void i_compare_if_the_value_and_value_are_the_same(String elementName, String elementName1) throws Throwable {
        vehicle.compareVehicleTrimSubModel(elementName, elementName1);
    }

    @When("^I send \"([^\"]*)\" text into input element \"([^\"]*)\" without clear$")
    public void i_send_text_into_input_element_without_clear(String text, String elementName) throws Throwable {
        vehicle.sendKeysWithOutClear(text, elementName);
    }

    @Then("^I validate the element \"([^\"]*)\" status with \"([^\"]*)\" image$")
    public void i_validate_the_element_status_with_image(String elementName, String colorImage) throws Throwable {
        vehicle.returnElement(elementName).getAttribute(Constants.ATTRIBUTE_SRC).contains(colorImage);
    }

    @When("^I save the \"([^\"]*)\" number from field with key \"([^\"]*)\"$")
    public void i_save_the_value_number_from_field_with_key(String elementName, String key) throws Throwable {
        vehicle.savesTitleValueIntoHashMap(elementName, key);
    }

    @Then("^I add different attributes of two number of element \"([^\"]*)\" and element \"([^\"]*)\"$")
    public void i_add_different_attributes_of_two_number_of_element_and_element(String elementName1, String elementName2) throws Throwable {
        vehicle.addTwoDifferentAttributes(elementName1, elementName2);
    }

    @When("^I enter \"([^\"]*)\" value into \"([^\"]*)\" table in \"([^\"]*)\" column in vehicle page$")
    public void i_enter_value_into_table_in_column(String value, String tableName, String columnNumber) throws Throwable {
        vehicle.enterDataIntoTablelastRow(value, tableName, columnNumber);
    }

    @When("^I \"([^\"]*)\" on row \"([^\"]*)\" into \"([^\"]*)\" table in \"([^\"]*)\" column in vehicle page$")
    public void i_double_click_on_row_into_table_in_column(String action, int row, String tableName, String columnNumber) throws Throwable {
        vehicle.doubleClickOrSaveTableSpecifiedRowRow(action, row, tableName, columnNumber);
    }

    @When("^I select on row \"([^\"]*)\" into \"([^\"]*)\" table in vehicle page$")
    public void i_select_on_row_into_table(String row , String tableName) throws Throwable {
        vehicle.selectsRowInATable(row, tableName);
    }

    @Then("^I verify if correct URL for the current environment is up or not$")
    public void i_verify_if_correct_url_for_the_current_environment_is_up_or_not() throws Throwable {
        vehicle.checkURL();
    }

    @Then("^I save the response body node \"([^\"]*)\" count from request GET call$")
    public void i_save_the_response_body_node_count_from_request_get_call(String key) throws Throwable {
        vehicle.getBodyAndSaveData(key);
    }
}