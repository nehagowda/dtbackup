package sap.pages;

import common.Constants;
import org.junit.Assert;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by mnabizadeh on 7/09/18.
 */
public class NetWeaverBusinessClient {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(NetWeaverBusinessClient.class.getName());

    public NetWeaverBusinessClient(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//*[@id=\"ul_nav_1\"]/li[2]/a")
    private static WebElement discountTireMIMAdmin;

    @FindBy(xpath = "//*[@id=\"ul_nav_1\"]/li[1]/a")
    private static WebElement generalAccess;

    @FindBy(xpath = "//input[@id=\"M0:U:::0:34\"]")
    private static WebElement userNameInput;

    @FindBy(xpath = "//input[@id=\"M0:U:::1:34\"]")
    private static WebElement siteUserInput;

    @FindBy(id = "URLSPW-0")
    private static WebElement savePopupiFrame;

    @FindBy(className = "urPgHTTxtSmall")
    public static WebElement createDTDTransfer;

    @FindBy(xpath = "//*[contains(text(),'Purchasing Document')]//following::input[1]")
    private static WebElement goodsMovementPoInput;

    @FindBy(xpath = "//*[contains(text(),'STO #')]//following::input[1]")
    private static WebElement mimAdjustmentCorrectiveSTO;

    @FindBy(id = "M0:D:13::btn[8]-img")
    private static WebElement userSearchExecuteButton;

    @FindBy(id = "M0:D:10::btn[11]-img")
    private static WebElement userSearchSaveButton;

    @FindBy(className = "urPgHTTxtSmall")
    public static WebElement confirmDTDTransfer;

    @FindBy(id = "M0:D:13::btn[17]-r")
    public static WebElement otherPurchaseOrder;

    @FindBy(id = "M1:U:::0:5")
    private static WebElement siteSuccessfulSaveTextContainer;

    @FindBy(id = "M1:D:13::btn[0]-img")
    private static WebElement siteSuccessfulSaveDismissInput;

    @FindBy(xpath = "//*[@id=\"URLSPW-0\"]")
    public static WebElement nwbcSelectDociFrame;

    @FindBy(id = "M0:D-title")
    public static WebElement nwbcPageTitle;

    @FindBy(id = "WD4D-r")
    private static WebElement checkGoodsIssueButton;

    @FindBy(id = "WD010E-r")
    private static WebElement checkGoodsIssueSaveButton;

    @FindBy(id = "WD4B-r")
    private static WebElement checkGoodsReceiptSaveButton;

    @FindBy(xpath = "//*[contains(@title,'Continue')]")
    private static WebElement goodsMovementContinue;

    @FindBy(xpath = "//div[contains(@id,'scrollV-Nxt')]")
    private static WebElement scrollNext;

    @FindBy(xpath = "//td[contains(text(), 'Confirm DTD Transfer')]")
    private static WebElement transferConfirmationTextContainer;

    @FindBy(xpath = "//*[contains(@data-unique-id,'~PJ1ECC_AZO_CJ_M_IT-MIM_ADMIN:3')]")
    private static WebElement mimadministrator;

    @FindBy(xpath = "//a[contains(@data-unique-id,'~PJ1ECC_AZO_CJ_M_IT-MIM_ADMIN:53')]")
    private static WebElement returns;

    @FindBy(xpath = "//a[text()='Open Returns']")
    private static WebElement openreturns;

    @FindBy(id = "WDCC")
    private static WebElement editQuantity;

    @FindBy(id = "WDD2")
    private static WebElement reasonDropDownButton;

    @FindBy(xpath = "//span[contains(text(),'Items')]/following::input[14]")
    private static WebElement reasonDropDownCreateReturnToVendor2;

    @FindBy(xpath = "//input[@value='FedEx/UPS']")
    private static WebElement methodOfReturn;

    @FindBy(id = "content_frame")
    public static WebElement rtviFrame;

    @FindBy(id = "URLSPW-0")
    private static WebElement createReturnPurchaseOrderIframe;

    @FindBy(xpath = "//a[@title = 'Display Filter Row ']")
    private static WebElement filter;

    @FindBy(xpath = "//a[contains(@title,'Filter')]")
    private static WebElement filterClose;

    @FindBy(xpath = "//a[@title='Display Filter Row ']")
    private static WebElement filterOpen;

    @FindBy(xpath = "//*[contains(@src,'SuccessMessage')]/following::*[1]")
    private static WebElement rtvConfirmation;

    @FindBy(xpath = "//input[@title='Purchasing Document Number']")
    private static WebElement purchasingDocument;

    @FindBy(xpath = "//span[text()='ADJUSTMENTS']")
    private static WebElement adjustmentMenu;

    @FindBy(xpath = "//input[@title=\"Enter Article Document Number\"]")
    private static WebElement articleDocumentInput;

    @FindBy(xpath = "//input[@title='Enter Customer Invoice Number']")
    private static WebElement customerInvoiceInput;

    @FindBy(xpath = "//span[contains(text(),'Return to Vendor')]")
    private static WebElement returnToVendor;

    @FindBy(xpath = "//table[@class='urST3BdBrd urST3Bd urFontStd']//tr[4]/td[12]//input")
    private static WebElement deptOfTrans;

    @FindBy(xpath = "//a[@title='Edit Data ']")
    private static WebElement edit;

    @FindBy(xpath = "//img[[@id=\"WD0505-img\"]")
    private static WebElement save;

    @FindBy(xpath = "//span[contains(text(),'RTV PO created')]")
    private static WebElement mimAdjustmentPO;

    @FindBy(xpath = "//span[contains(text(),'DTD Transfer')]")
    private static WebElement dtdTransferPo;

    @FindBy(xpath = "//span[contains(text(),'RTV PO')]")
    private static WebElement rtvPO;

    @FindBy(xpath = "//span[contains(text(),'Adjustment Transfer created')]")
    private static WebElement adjustmentTransferPO;

    @FindBy(id = "n0000000165")
    private static WebElement correctiveActions;

    @FindBy(id = "M0:U:1:1:1::0:0")
    private static WebElement poOrderInput;

    @FindBy(xpath = "(//table[@class='urMatrixLayout'])[3]//input")
    private static WebElement mimRtvPOInput;

    @FindBy(xpath = "//iframe[contains(@title,'Process Adjustment')]")
    private static WebElement mimAdjustmentIFrame;

    @FindBy(xpath = "//*[@title='Deleted ']")
    private static WebElement deleteIndicator;

    @FindBy(xpath = "//span[contains(text(),'RTV Purchase Order')]")
    private static WebElement rtvPOSuccessMessage;

    @FindBy(xpath = "(//*[@class='urST4RowFirstVisible'])[5]/td[3]")
    private static WebElement articleDocumentNumber1;

    @FindBy(xpath = "(//tr[contains(@udat,'RowIdNumeric:2')])[1]/td[3]")
    private static WebElement articleDocumentNumber2;

    @FindBy(id = "n0000000084")
    private static WebElement createReturnToVendor;

    @FindBy(xpath = "//span[contains(text(),'Items')]/following::input[10]")
    private static WebElement articleInput2;

    @FindBy(xpath = "//span[contains(text(),'Items')]/following::input[12]")
    private static WebElement quantityInput2;

    @FindBy(xpath = "//span[contains(text(),'Items')]/following::input[13]")
    private static WebElement unitPriceInput2;

    @FindBy(xpath = "//iframe[contains(@title,'Create Return to Vendor')]")
    private static WebElement createReturnPurchaseIframe;

    @FindBy(id = "WD021B")
    private static WebElement trackingAlertBanner;

    @FindBy(xpath = "//span[text()='Net Value']/following::td[6]")
    private static WebElement unitPriceInputFieldInNwbc;

    @FindBy(xpath = "//*[text()='Refresh']")
    private static WebElement refreshInIframe;

    @FindBy(xpath = "//a[text()='Create Store Transfer']")
    private static WebElement createStoreTransfer;

    @FindBy(xpath = "//span[text()='ORDERING']")
    private static WebElement orderingSubMenu;

    @FindBy(xpath = "//div[text()='Purchase Order Header Text']/following::input[1]")
    private static WebElement purchaseOrderPopupOption;

    @FindBy(xpath = "//td[text()='PO Comments']")
    private static WebElement poCommentPopupOption;

    @FindBy(xpath = "//body[@acf='editor']")
    private static WebElement purchaseOrderHeaderPopupCommentInput;

    @FindBy(xpath = "//*[contains(text(),'Supplying Site')]/following::input[1]")
    private static WebElement supplyingSiteFieldInNwbc;

    @FindBy(xpath = "//input[@id=\"WD68\"]")
    private static WebElement deliveryDate;

    @FindBy(xpath = "//span[text()='Quantity']/following::td[3]")
    private static WebElement articleFieldForStoreTransfer;

    @FindBy(xpath = "//span[text()='Quantity']/following::td[5]")
    private static WebElement quantityFieldForStoreTransfer;

    @FindBy(xpath = "//iframe[@class='urBdyStd']")
    private static WebElement innerIFrame;

    @FindBy(xpath = "//iframe[contains(@title,'Create Store Transfer')]")
    private static WebElement createStoreTransferIframe;

    @FindBy(xpath = "//*[contains(text(),'has been created')]")
    private static WebElement nwbcPurchaseOrder;

    @FindBy(xpath = "//span[contains(text(),'GOODS MOVEMENTS')]")
    private static WebElement goodsMovement;

    @FindBy(xpath = "//a[@id='a_50000000034']")
    private static WebElement postGoodsIssue;

    @FindBy(xpath = "//span[contains(text(),'REPORTS')]")
    private static WebElement reports;

    @FindBy(xpath = "//a[contains(@data-unique-id,'PJ1ECC_AZO_CJ_M_IT-MIM_ADMIN:73')]")
    private static WebElement changeSite;

    @FindBy(xpath = "//*[text()='Site']/following::input[1]")
    private static WebElement siteInputField;

    @FindBy(xpath = "//span[contains(text(),'RECEIVING')]")
    private static WebElement receiving;

    @FindBy(xpath = "//img[contains(@src,'Error')]")
    private static WebElement errorImageForSelectSite;

    @FindBy(xpath = "//img[contains(@src,'Success')]")
    private static WebElement successImageForAllTransactions;

    @FindBy(xpath = "//input[@title=\"Load View\"]")
    private static WebElement viewMimAdjustment;

    @FindBy(xpath = "//input[@title=\"Load View\"][@value=\"RTV Pending\"]")
    private static WebElement viewRtvPending;

    @FindBy(xpath = "//input[@title=\"Load View\"][@value=\"AutomationRTV\"]")
    private static WebElement viewAutomationRtv;

    @FindBy(xpath = "//*[text()='RTV Pending']")
    private static WebElement rtvPendingDropDownOption;

    @FindBy(xpath = "//*[text()='AutomationRTV']")
    private static WebElement automationRtvDropDownOption;

    @FindBy(id = "WDBD")
    private static WebElement returnToVendorInAdjustmentsTable;

    @FindBy(xpath = "//a[@title='Item Details ']")
    private static WebElement detailsButtonInAdjustmentsTable;

    @FindBy(id = "WDE1-scrollV-Nxt")
    private static WebElement scrollBarDown;

    @FindBy(xpath = "//label[@title='BOL/PRO #']")
    private static WebElement bolProLabelField;

    @FindBy(xpath = "//div[contains(@title,'Sorted in ascending order')]")
    private static WebElement sortedInAscendingOrderIndicator;

    @FindBy(xpath = "//div[contains(@title,'Sorted in descending order')]")
    private static WebElement sortedInDescendingOrderIndicator;

    @FindBy(id = "M0:U:::2:21")
    private static WebElement transactionCodeInput;

    @FindBy(xpath = "//a[text()='Create Non Merchandise Order']")
    private static WebElement createNonMerchandiseOrder;

    @FindBy(xpath = "//span[contains(text(),'Vendor')]/following::input[1]")
    private static WebElement vendorFieldInNwbc;

    @FindBy(xpath = "//label[contains(text(),\"Vendor\")]/following::input[1]")
    private static WebElement nonMerchandisevendor;

    @FindBy(xpath = "//input[@id=\"WDD5\"]")
    private static WebElement nonMerchandiseArticleNumber;

    @FindBy(xpath = "//input[@id=\"WDDA\"]")
    private static WebElement nonMerchandiseArticleQuantity;

    @FindBy(xpath = "//td[text()='Back Room Supplies']")
    private static WebElement backRoomSuppliesOption;

    @FindBy(xpath = "//*[text()=\"Merch. Category:\"]/following::*[1]")
    private static WebElement merchCategoryDropdown;

    @FindBy(xpath = "//*[contains(@id,'[1,3]_c') and not (contains(@id,'[1,3]_c-r'))]")
    private static WebElement accountAssignment;

    @FindBy(xpath = "//a[contains(@title,'Settings')]")
    private static WebElement settingsButton;

    @FindBy(id = "WD04EA-btn")
    private static WebElement filterColumn;

    @FindBy(xpath = "//img[contains(@alt,'RTW')]")
    private static WebElement filterRtwBoolean;

    @FindBy(xpath = "//img[contains(@src,'BGNEQU')]")
    private static WebElement unequal;

    @FindBy(xpath = "//iframe[contains(@id,'blindlayer')][@scrolling=\"no\"][contains(@style,'50px')]")
    private static WebElement loadBar;

    @FindBy(xpath = "//a[text()='Open Orders']")
    private static WebElement openOrders;

    @FindBy(xpath = "//td[@class=\"urSTC urST3TD\"][1]")
    private static WebElement itemID;

    @FindBy(xpath = "//td[@class=\"urSTC urST3TD\"][2]")
    private static WebElement articleNumber;

    @FindBy(xpath = "//*[text()='235/45R17  94Y B MCH PLTSP AS35R']")
    private static WebElement articleDescription;

    @FindBy(xpath = "//td[@class=\"urSTC urST3TD\"][4]")
    private static WebElement lineItemQuantity;

    @FindBy(xpath = "//td[@class=\"urSTC urST3TD\"][5]")
    private static WebElement siteNumber;

    @FindBy(xpath = "//input[contains(@id,WD)][contains(@lsevents,'Field')]")
    private static WebElement scrappedArticleDocNumberInputField;

    @FindBy(xpath = "//input[contains(@lsdata,'0000')]")
    private static WebElement scrappedArticleDocItemNumber;

    @FindBy(xpath = "//input[@value=''][@size='4']")
    private static WebElement scrappedArticleDocYearNumber;

    @FindBy(xpath = "//iframe[contains(@id,'Post Goods Return')]")
    private static WebElement postGoodsIframe;

    @FindBy(xpath = "//input[contains(@value,'Z142')]")
    private static WebElement z142;

    @FindBy(xpath = "//input[contains(@value,'Z348')]")
    private static WebElement z348;

    @FindBy(xpath = "//span[contains(@title,'WOSE')]")
    private static WebElement postGoodsDocumentNumber;

    @FindBy(xpath = "//td[contains(@id,'grid#11') and contains(@id,'#1,0')]")
    private static WebElement printServiceClaimsPOLine;

    @FindBy(xpath = "//a[contains(@title,\"Subtotals\")]/following::a[contains(@title,'Print')]")
    private static WebElement printButton;

    @FindBy(id = "M1:U:::0:20")
    private static WebElement outputDevice;

    @FindBy(xpath = "//div[contains(text(),'Title')]/following::img[contains(@src,'s_otfdoc.png')][1]")
    private static WebElement firstSpool;

    @FindBy(xpath = "//a[@title=\"Continue (Shift+F1)\"]")
    private static WebElement continueButtonPrintservices;

    @FindBy(xpath = "//a[@title=\"Continue (Enter)\"]")
    private static WebElement continueButtonPrintservicesNextIframe;

    @FindBy(xpath = "//span[contains(text(),'Article')]/following::input[1]")
    private static WebElement articleInputDtdTrasferInNwbc;

    @FindBy(xpath = "//span[contains(text(),'Quantity')]/following::input[2]")
    private static WebElement quantityInputInDtdTransferInNwbc;

    @FindBy(id = "WD42-img")
    public static WebElement undoReturnToWarehouseCheckBox;

    @FindBy(id = "WD65")
    private static WebElement stoArticleDocumentInput;

    @FindBy(xpath = "//*[text()='Document Year']/following::input[1]")
    private static WebElement documentYear;

    @FindBy(xpath = "//a[text()='Create DTD Transfer']")
    private static WebElement createDtdTransferMenu;

    @FindBy(xpath = "//*[contains(text(),'Deliv. Compl.')]/following::span[1]")
    private static WebElement checkDeliveryComplete;

    private static final By trackingNumbersInputInReturnInformationBy = By.xpath("//*[text()='Tracking Numbers']/following::input");

    private static final By tableContentsInMimAdjusmentTableBy = By.xpath("//input[contains(@id,'WD')][contains(@lsevents,'Change')]");

    private static final By nwbcPageTitleBy = By.xpath("//td[@id='M0:D-title']");

    private static final By itemsTableColumnHeadingsBy = By.xpath("//tbody[contains(@id,'contentTBody')]/child::tr[1]/child::th");

    private static final By itemsTableRowBy = By.xpath("//tbody[contains(@id,'contentTBody')]/child::tr");

    private static final By goodsButtonContainerBy = By.className("urPgHITrans");

    private static final By goodsMovementButtonBy = By.className("urTbarItmBtn");

    private static final By nwbPurchOrderInputBy = By.id("M1:U:1::0:21");

    private static final By confirmDTDTransferBy = By.className("urPgHTTxtSmall");

    private static final By nwbcSaveButtonBy = By.xpath("//span[@class='lsToolbarButton'][contains(.,'Save')]");

    private static final By nwbcCheckButtonBy = By.xpath("//span[@class='lsToolbarButton'][contains(.,'Check')]");

    private static final By openReturnsTabFilterBy = By.className("lsTblEdf3");

    private static final By firstTableRowSelectBy = By.className("urST5SCMetricInner");

    private static final By deptOfTransInvalidMessage = By.xpath("//span[contains(text(),'Dept of Trans# is invalid')]");

    private static final By AdjustmentTransferConfirmationMessage = By.xpath("//span[contains(text(),'Adjustment Transfer created')]");

    private static final By mimAdjustmentTableDetailsContentBy = By.xpath("//div[contains(text(),'Article Document')]/following::input");

    private static final By tableRowSelectBy = By.xpath("//div[@class='urST5SCMetricInner'][text()]");

    private static final By scrapPopUpInputTagElementsBy = By.xpath("//input[contains(@id,'WD')]");

    private static final By correctiveActionsRadioButtonsBy = By.xpath("//img[contains(@id,'WD')]");

    private static final String USERNAME = "AUTOUSER01";
    private static final String SAVE_SUCCESSFUL = "Save successful";
    private static final String ADJUSTMENTS = "ADJUSTMENTS";
    private static final String RETURN_TO_VENDOR = "Return To Vendor";
    private static final String RTV_MIM_ADJUSTMENT_TABLE = "Return to Vendor";
    private static final String DEPT_OF_TRANS = "DEPT OF TRANS";
    private static final String EDIT_BUTTON_TEXT = "EDIT";
    private static final String EDIT_BUTTON_TEXT_2 = "Edit";
    private static final String SAVE_BUTTON_TEXT = "SAVE";
    private static final String SAVE_BUTTON_TEXT1 = "Save";
    private static final String MIM_ADJUSTMENT_PO = "mim adjustment po";
    private static final String DTD_TRANSFER_PO = "DTD Transfer po";
    private static final String RETURN_TO_VENDOR_PO = "Return To Vendor Purchase Order Number";
    private static final String MIM_RTV_PO_INPUT = "mim rtv input";
    private static final String MIM_ADJUSTMENT_FRAME = "MIM Adjustment Frame";
    private static final String MIM_ADJUSTMENT_CORR_STO = "STO #";
    private static final String DELETE_INDICATOR = "DELETE INDICATOR";
    private static final String METHOD_OF_RETURN = "Method of Return";
    private static final String REFRESH = "Refresh";
    private static final String MIM_ADMINISTRATOR = "MIM ADMINISTRATOR";
    private static final String OPEN_RETURNS = "Open Returns";
    private static final String OPEN_ORDERS = "Open Orders";
    private static final String RETURNS = "RETURNS";
    private static final String EDIT_QUANTITY = "quantity";
    private static final String REASON_DROP_DOWN = "reason";
    private static final String REASON_CREATE_RETURN_2 = "Second Reason";
    private static final String CREATE_RETURN_TO_VENDOR = "Create Return to Vendor";
    private static final String ARTICLE_INPUT_2 = "Second Article";
    private static final String QUANTITY_INPUT_2 = "Second Quantity";
    private static final String UNIT_PRICE_INPUT_2 = "Second Unit Price";
    private static final String UNIT_PRICE_FIELD_NON_MERCH_PO = "Unit Price Input Field Non Merch";
    private static final String CREATE_RETURN_ORDER = "Create Return Order";
    private static final String TRACKING_NUMBER_1 = "Autouser01";
    private static final String TRACKING_NUMBER_2 = "Autouser02";
    private static final String TRACKING_NUMBER_4 = "Autouser03";
    private static final String DRIVER = "DRIVER 1";
    private static final String RGA = "RGA 1";
    private static final String BOL = "BOL 1";
    private static final String OP = "OP 1";
    private static final String TRACKING_ASSERT_MESSAGE = "FAIL: Tracking number is not present!";
    private static final String CREATE_STORE_TRANSFER = "Create Store Transfer";
    private static final String STORE_TRANSFER_PO = "Store Transfer PO";
    private static final String ORDERING = "Ordering";
    private static final String PO_COMMENT_OPTION = "PO comment";
    private static final String SUPPLYING_SITE_INPUT_FIELD_NWBC = "Supplying Site";
    private static final String Delivery_DATE = "Delivery Date";
    private static final String QUANTITY_FIELD_STORE_TRANSFER = "Store Transfer Quantity";
    private static final String ARTICLE_FIELD_STORE_TRANSFER = "Store Transfer Article";
    private static final String INNER_IFRAME = "Inner IFrame";
    private static final String CREATE_STORE_TRANSFER_IFRAME = "Create Store Transfer Iframe";
    private static final String GOODS_MOVEMENTS = "Goods Movements";
    private static final String POST_GOODS_ISSUE = "Post Goods Issue";
    private static final String REPORTS_TAB = "Reports";
    private static final String CHANGE_SITE = "Change Site";
    private static final String SITE_INPUT_FIELD = "Site Input Field";
    private static final String POST_GOODS_ISSUE_PO = "Post Goods Issue PO";
    private static final String POST_GOODS_RECEIPT_PO = "Post Goods Receipt PO";
    private static final String RECEIVING = "Receiving";
    private static final String REPORTS = "REPORTS";
    private static final String THIRTY_TWO_NDS = "8";
    private static final String MILES_ON = "56000";
    private static final String MILES_OFF = "60000";
    private static final String DETAILS = "Details";
    private static final String PURCHASE_ORDER_POPUP_DROPDOWN = "Purchase Order dropdown";
    private static final String CREATE_NON_MERCHANDISE_ORDER = "Create Non Merchandise Order";
    private static final String VENDOR_NUMBER_FIELD = "Vendor Field";
    private static final String MERCH_CATEGORY_DROPDOWN = "Merch Category dropdown";
    private static final String NON_MERCH_PO = "Non Merchant";
    private static final String ACCOUNT_ASSIGNMENT = "Account Assignment";
    private static final String ITEM_ID = "Item Id";
    private static final String ARTICLE_NUMBER = "Article Number";
    private static final String ARTICLE_DESCRIPTION = "Article Description";
    private static final String ITEM_QUANTITY = "Item Quantity";
    private static final String SITE_NUMBER = "Site Number";
    private static final String AUTOMATION_RTV_VIEW = "AutomationRTV";
    private static final String RTV_PENDING_VIEW = "RTV Pending";
    private static final String ADJUSTMENT_TRANSFER_PO = "Adjustment Transfer created";
    private static final String UNEVEN_WEAR = "Uneven Wear";
    private static final String PURCHASING_DOCUMENT_SE16N = "Purchasing Doc.";
    private static final String RETURN_TYPE = "Return Type";
    private static final String POST_GOODS_IFRAME = "Post Goods";
    private static final String POST_GOODS_DOCUMENT_NUMBER = "Post Goods Document Number";
    private static final String CREATE_DTD_TRANSFER = "Create DTD Transfer";
    private static final String Z348 = "Z348";
    private static final String Z142 = "Z142";
    public static int counter = 3;
    //    private static final String PRINT_SERVICE_CLAIMS_PURCHASE_ORDER_NUMBER = "Print Service claims Purchase Order Number";
    private static final String PRINT_SERVICE_CLAIMS_PO_LINE = "Print Service Claims PO Line";
    private static final String PRINT_BUTTON = "Print Button";
    private static final String OUTPUT_DEVICE = "Output Device";
    private static final String FIRST_SPOOL = "First Spool";
    private static final String CONTINUE_BUTTON_PRINT_SERVICES = "Continue Button In Print Services";
    private static final String CONTINUE_BUTTON_PRINT_SERVICES_NEXT_IFRAME = "Continue Button Print Services Next Iframe";
    private static final String STO_ARTICLE_DOCUMENT_NUMBER_INPUT = "Sto Article Document Number Input";
    private static final String UNDO_RETURN_TO_WAREHOUSE_CHECK_BOX = "Undo Return To Warehouse Check Box";
    private static final String ARTICLE_INPUT_IN_DTD_TRANSFER_IN_NWBC = "Article Input in DTD";
    private static final String QUANTITY_INPUT_IN_DTD_TRANSFER_IN_NWBC = "Quantity Input in DTD";
    private static final String TRACKING = "tracking";
    private static final String AUTOUSER0 = "Autouser0";
    private static final String ARTICLE_DOCUMENT_LABEL = "Article Document";
    public static final String ARTICLE_DOCUMENT_NUMBER_DETAILS_KEY = "Article Document Num";
    public static final String CHECK_DELIVERY_COMPLETE = "Check delivery complete";
    private static int counter_2 = 3;
    public static int mimAdjustmentTableIndex = 2;
    public static ArrayList<String> mimScenarioData = new ArrayList<>();

    /**
     * goodsMovementPoInput
     * returns the WebElement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case MIM_ADJUSTMENT_CORR_STO:
                return mimAdjustmentCorrectiveSTO;
            case MIM_ADMINISTRATOR:
                return mimadministrator;
            case OPEN_RETURNS:
                return openreturns;
            case OPEN_ORDERS:
                return openOrders;
            case RETURNS:
                return returns;
            case EDIT_QUANTITY:
                return editQuantity;
            case REASON_DROP_DOWN:
                return reasonDropDownButton;
            case REASON_CREATE_RETURN_2:
                return reasonDropDownCreateReturnToVendor2;
            case METHOD_OF_RETURN:
                return methodOfReturn;
            case ADJUSTMENTS:
                return adjustmentMenu;
            case RETURN_TO_VENDOR:
                return returnToVendor;
            case DEPT_OF_TRANS:
                return deptOfTrans;
            case EDIT_BUTTON_TEXT:
                return edit;
            case SAVE_BUTTON_TEXT:
                return save;
            case MIM_RTV_PO_INPUT:
                return mimRtvPOInput;
            case MIM_ADJUSTMENT_PO:
                return mimAdjustmentPO;
            case DTD_TRANSFER_PO:
                return dtdTransferPo;
            case MIM_ADJUSTMENT_FRAME:
                return mimAdjustmentIFrame;
            case DELETE_INDICATOR:
                return deleteIndicator;
            case CREATE_RETURN_TO_VENDOR:
                return createReturnToVendor;
            case ARTICLE_INPUT_2:
                return articleInput2;
            case QUANTITY_INPUT_2:
                return quantityInput2;
            case UNIT_PRICE_FIELD_NON_MERCH_PO:
                return unitPriceInputFieldInNwbc;
            case UNIT_PRICE_INPUT_2:
                return unitPriceInput2;
            case CREATE_RETURN_ORDER:
                return createReturnPurchaseIframe;
            case CREATE_NON_MERCHANDISE_ORDER:
                return createNonMerchandiseOrder;
            case VENDOR_NUMBER_FIELD:
                return vendorFieldInNwbc;
            case MERCH_CATEGORY_DROPDOWN:
                return merchCategoryDropdown;
            case ORDERING:
                return orderingSubMenu;
            case NON_MERCH_PO:
                return nwbcPurchaseOrder;
            case ACCOUNT_ASSIGNMENT:
                return accountAssignment;
            case RTV_MIM_ADJUSTMENT_TABLE:
                return returnToVendorInAdjustmentsTable;
            case DETAILS:
                return detailsButtonInAdjustmentsTable;
            case CREATE_STORE_TRANSFER:
                return createStoreTransfer;
            case SUPPLYING_SITE_INPUT_FIELD_NWBC:
                return supplyingSiteFieldInNwbc;
            case Delivery_DATE:
                return deliveryDate;
            case ARTICLE_FIELD_STORE_TRANSFER:
                return articleFieldForStoreTransfer;
            case QUANTITY_FIELD_STORE_TRANSFER:
                return quantityFieldForStoreTransfer;
            case INNER_IFRAME:
                return innerIFrame;
            case PO_COMMENT_OPTION:
                return poCommentPopupOption;
            case CREATE_STORE_TRANSFER_IFRAME:
                return createStoreTransferIframe;
            case STORE_TRANSFER_PO:
                return nwbcPurchaseOrder;
            case GOODS_MOVEMENTS:
                return goodsMovement;
            case POST_GOODS_ISSUE:
                return postGoodsIssue;
            case REPORTS_TAB:
                return reports;
            case CHANGE_SITE:
                return changeSite;
            case SITE_INPUT_FIELD:
                return siteInputField;
            case POST_GOODS_ISSUE_PO:
                return goodsMovementPoInput;
            case POST_GOODS_RECEIPT_PO:
                return goodsMovementPoInput;
            case RECEIVING:
                return receiving;
            case PURCHASE_ORDER_POPUP_DROPDOWN:
                return purchaseOrderPopupOption;
            case ARTICLE_NUMBER:
                return articleNumber;
            case ITEM_ID:
                return itemID;
            case ARTICLE_DESCRIPTION:
                return articleDescription;
            case ITEM_QUANTITY:
                return lineItemQuantity;
            case SITE_NUMBER:
                return siteNumber;
            case AUTOMATION_RTV_VIEW:
                return viewAutomationRtv;
            case RTV_PENDING_VIEW:
                return viewRtvPending;
            case ADJUSTMENT_TRANSFER_PO:
                return adjustmentTransferPO;
            case RETURN_TYPE:
                return webDriver.findElements(CommonActions.inputTagBy).get(4);
            case POST_GOODS_IFRAME:
                return postGoodsIframe;
            case Z142:
                return z142;
            case Z348:
                return z348;
            case POST_GOODS_DOCUMENT_NUMBER:
                return postGoodsDocumentNumber;
            case PRINT_SERVICE_CLAIMS_PO_LINE:
                return printServiceClaimsPOLine;
            case PRINT_BUTTON:
                return printButton;
            case OUTPUT_DEVICE:
                return outputDevice;
            case FIRST_SPOOL:
                return firstSpool;
            case CONTINUE_BUTTON_PRINT_SERVICES:
                return continueButtonPrintservices;
            case CONTINUE_BUTTON_PRINT_SERVICES_NEXT_IFRAME:
                return continueButtonPrintservicesNextIframe;
            case STO_ARTICLE_DOCUMENT_NUMBER_INPUT:
                return stoArticleDocumentInput;
            case UNDO_RETURN_TO_WAREHOUSE_CHECK_BOX:
                return undoReturnToWarehouseCheckBox;
            case RETURN_TO_VENDOR_PO:
                return rtvPO;
            case CREATE_DTD_TRANSFER:
                return createDtdTransferMenu;
            case ARTICLE_INPUT_IN_DTD_TRANSFER_IN_NWBC:
                return articleInputDtdTrasferInNwbc;
            case QUANTITY_INPUT_IN_DTD_TRANSFER_IN_NWBC:
                return quantityInputInDtdTransferInNwbc;
            case CHECK_DELIVERY_COMPLETE:
                return checkDeliveryComplete;
            default:
                Assert.fail("FAIL: Could not find element that matched string passed from step");
                return null;
        }
    }

    /**
     * Selects tab and option to click on
     *
     * @param tab    tab to select
     * @param option option to select
     */
    public void selectTabOption(String tab, String option) {
        LOGGER.info("selectTabOption started");
        driver.waitForPageToLoad();
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        driver.waitForPageToLoad();
        WebElement tabToClick = commonActions.returnNetWeaverNavTab(tab);
        tabToClick.click();
        WebElement optionToClick = commonActions.returnNetWeaverNavTab(option);
        optionToClick.click();
        LOGGER.info("selectTabOption completed");
    }

    /**
     * Selects option to click on
     *
     * @param option option to click on
     */
    public void selectOption(String option) {
        LOGGER.info("selectOption started");
        driver.waitForPageToLoad();
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        commonActions.returnNetWeaverNavTab(option).click();
        LOGGER.info("selectOption completed");
    }

    /**
     * Selects tab to click on
     *
     * @param tab tab to select
     */
    public void selectTab(String tab) {
        LOGGER.info("selectTab started");
        driver.waitForPageToLoad();
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        driver.waitForPageToLoad();
        WebElement tabToClick = commonActions.returnNetWeaverNavTab(tab);
        tabToClick.click();
        LOGGER.info("selectTab completed");
    }

    /**
     * Selects Save and Print Preview for NWBC Create DTD Transfer
     */
    public void saveAndPrintPreview() {
        LOGGER.info("saveAndPrintPreview started");
        driver.waitForElementClickable(webDriver.findElement(nwbcSaveButtonBy));
        webDriver.findElement(nwbcSaveButtonBy).click();
        LOGGER.info("saveAndPrintPreview completed");
    }

    /**
     * Verifies DTD transfer is created
     *
     * @param type type of PO to validate
     */
    public void verifyDTDSTOTransfer(String type) {
        LOGGER.info("verifyDTDSTOTransfer started");
        driver.waitForElementVisible(confirmDTDTransferBy);
        driver.waitForPageToLoad();
        driver.waitForMilliseconds(Constants.SEVEN_THOUSAND);
        String transferMessage = webDriver.findElement(confirmDTDTransferBy).getText();

        if (type.equalsIgnoreCase(Constants.NWBC_STO_PO_TRANSFER)) {
            String confirmMessage = transferMessage.substring(0, 22);
            Assert.assertTrue("FAIL: Message displayed:-> " + confirmMessage + " Message expected:  " +
                    Constants.CONFIRM_STO_TRANSFER + ")", confirmMessage.equals(Constants.CONFIRM_STO_TRANSFER));
        } else if (type.equalsIgnoreCase(Constants.NWBC_DTD_PO_TRANSFER)) {
            String confirmMessage = transferMessage.substring(0, 20);
            Assert.assertTrue("FAIL: Message displayed:-> " + confirmMessage + " Message expected:  " +
                    Constants.CONFIRM_DTD_TRANSFER + ")", confirmMessage.equals(Constants.CONFIRM_DTD_TRANSFER));
        } else if (type.equalsIgnoreCase(Constants.NWBC_GOODS_ISSUE_PO_TRANSFER)) {
            String confirmMessage = transferConfirmationTextContainer.getText();
            Assert.assertTrue("FAIL: Messaged displayed was: " + confirmMessage + " but expected " +
                    Constants.CONFIRM_GOODS_MOVEMENT_TRANSFER, confirmMessage.contains(Constants.CONFIRM_GOODS_MOVEMENT_TRANSFER));
        }
        LOGGER.info("verifyDTDSTOTransfer completed");
    }

    /**
     * Saves NWBC PO number to Scenario Data
     */
    public void saveNWBCPONumberToScenarioData() {
        LOGGER.info("saveNWBCPONumberToScenarioData started");
        driver.waitForElementVisible(transferConfirmationTextContainer);
        String cleanedNWBCPONumber = transferConfirmationTextContainer.getText().replaceAll("[^\\d.]", "");
        driver.scenarioData.setNWBCCurrentPONumber(cleanedNWBCPONumber);
        LOGGER.info("saveNWBCPONumberToScenarioData completed");
    }


    /**
     * Selects NWBC Navigation options
     *
     * @param option option to select
     */
    public void selectNWBCNavigationOption(String option) {
        LOGGER.info("selectNWBCNavigationOption started" + option);

        if (option.equalsIgnoreCase(Constants.GENERAL_ACCESS)) {
            driver.waitForElementClickable(generalAccess);
            generalAccess.click();
            driver.waitForPageToLoad();
        } else {
            driver.waitForElementClickable(discountTireMIMAdmin);
            discountTireMIMAdmin.click();
        }
    }

    /**
     * Click icon on NWBC to open the 'Other Purchase Order' search popup
     */
    public void clickNWBCOtherPurchaseOrderIcon() {
        LOGGER.info("clickNWBCOtherPurchaseOrderIcon started");
        driver.waitForElementVisible(otherPurchaseOrder);
        otherPurchaseOrder.click();
        LOGGER.info("clickNWBCOtherPurchaseOrderIcon completed");
    }

    /**
     * Gets Scenario data for PO Number
     */
    public String getPoNumber(String type) {
        LOGGER.info("getPoNumber started");
        driver.waitForMilliseconds();
        String nwbcPONumber = null;

        if (type.equalsIgnoreCase(Constants.EXTENDED_ASSORTMENT)) {
            try {
                return nwbcPONumber = driver.scenarioData.getCurrentPurchaseOrderNumber();
            } catch (Exception e) {
                Assert.fail("FAIL: Was unable to get PO Number from scenario data");
            }
        } else {
            return nwbcPONumber = driver.scenarioData.getNWBCCurrentPONumber();
        }
        LOGGER.info("getNWBCPoNumber completed");
        return nwbcPONumber;
    }

    /**
     * Searches NWBC PO Number based
     *
     * @param PONumber from scenario data that was created before
     */
    public void searchForPONumber(String PONumber) {
        LOGGER.info("searchForPONumber started" + PONumber);
        driver.waitForPageToLoad();
        webDriver.switchTo().frame(nwbcSelectDociFrame);
        WebElement purOrder = webDriver.findElement(nwbPurchOrderInputBy);
        purOrder.click();
        purOrder.clear();
        purOrder.sendKeys(PONumber);
        LOGGER.info("searchForPONumber completed");
    }

    /**
     * Asserts text displayed as page title on NWBC
     *
     * @param text to verify text is present in the title
     */
    public void verifyNWBCTitle(String text) {
        LOGGER.info("verifyNWBCTitle started");
        driver.waitForPageToLoad();
        WebElement textEl = webDriver.findElement(nwbcPageTitleBy);
        String titleString = textEl.getText();
        Assert.assertTrue("FAIL: Page title was not found!", titleString.contains(text));
        LOGGER.info("verifyNWBCTitle completed");
    }

    /**
     * Searches for the previously created PO number through Goods Movement
     */
    public void searchGoodsMovementPO() {
        LOGGER.info("searchGoodsMovementPO started");
        driver.waitForElementClickable(goodsMovementPoInput);
        String purchaseOrderNumber = driver.scenarioData.getNWBCCurrentPONumber();
        goodsMovementPoInput.click();
        goodsMovementPoInput.clear();
        goodsMovementPoInput.sendKeys(purchaseOrderNumber);
        goodsMovementContinue.click();
        LOGGER.info("searchGoodsMovementPO completed");
    }

    /**
     * Fills out the employee user name and executes the search.
     */
    public void executeEmployeeUserNameSearch() {
        LOGGER.info("executeEmployeeUserNameSearch started");
        driver.waitForElementClickable(userNameInput);
        userNameInput.click();
        userNameInput.clear();
        userNameInput.sendKeys(USERNAME);
        userSearchExecuteButton.click();
        LOGGER.info("executeEmployeeUserNameSearch completed");
    }

    /**
     * Fills out the user site information and clicks the save button.
     *
     * @param site passed from feature file to send to input.
     */
    public void setSiteForUser(String site) {
        LOGGER.info("setSiteForUser started");
        driver.waitForElementClickable(siteUserInput);
        siteUserInput.click();
        siteUserInput.clear();
        siteUserInput.sendKeys(site);
        userSearchSaveButton.click();
        LOGGER.info("setSiteForUser completed");
    }

    /**
     * Verifies that the site update for user has been saved successfully.
     */
    public void verifySiteUpdateSaved() {
        LOGGER.info("verifySiteUpdateSaved started");
        driver.waitForElementVisible(siteSuccessfulSaveTextContainer);
        Assert.assertTrue("FAIL: did not find the expected of : " + SAVE_SUCCESSFUL +
                        " text and instead found: " + siteSuccessfulSaveTextContainer.getText(),
                siteSuccessfulSaveTextContainer.getText().contains(SAVE_SUCCESSFUL));
        LOGGER.info("verifySiteUpdateSaved completed");
    }

    /**
     * Closes the save alert popup after updating site.
     */
    public void closeSiteUpdatePopup() {
        LOGGER.info("closeSiteUpdatePopup started");
        driver.waitForElementClickable(siteSuccessfulSaveDismissInput);
        siteSuccessfulSaveDismissInput.click();
        LOGGER.info("closeSiteUpdatePopup completed");
    }

    /**
     * Switches iFrame to save confirmation popup
     */
    public void switchToSavePopupiFrame() {
        LOGGER.info("switchToSavePopupiFrame started");
        driver.waitForElementClickable(savePopupiFrame);
        driver.switchFrameContext(savePopupiFrame);
        LOGGER.info("switchToSavePopupiFrame completed");
    }

    /**
     * Selects the Check Goods Issue button
     */
    public void selectCheckGoodsIssue() {
        LOGGER.info("selectCheckGoodsIssue started");
        driver.waitForElementClickable(nwbcCheckButtonBy);
        webDriver.findElement(nwbcCheckButtonBy).click();
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        driver.waitForElementClickable(nwbcSaveButtonBy);
        webDriver.findElement(nwbcSaveButtonBy).click();
        driver.waitForPageToLoad();
        LOGGER.info("selectCheckGoodsIssue completed");
    }

    /**
     * Send keys for elements in NWBC page
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     * @param inputText   - value to enter
     */
    public void sendKeysInNwbcPage(String elementName, String inputText) {
        LOGGER.info("sendKeysInNwbcPage started");
        try {
            driver.clearInputAndSendKeys(returnElement(elementName), inputText);
        } catch (InvalidElementStateException iese) {
            Assert.fail("Unclean Data, Method is not able to edit. Please scrap the line item manually");
        }
        LOGGER.info("sendKeysInNwbcPage completed");
    }

    /**
     * Click method for elements in NWBC page
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void clickInNwbcPage(String elementName) {
        LOGGER.info("clickInNwbcPage started");
        commonActions.click(returnElement(elementName));
        LOGGER.info("clickInNwbcPage completed");
    }

    /**
     * verifies if Purchase Order number is present in the RTV Confirmation page
     */
    public void verifyRtvConfirmationInNwbcPage() {
        LOGGER.info("verifyRtvConfirmationInNwbcPage started");
        Assert.assertTrue(driver.isElementDisplayed(commonActions.returnElementWithExactText
                ("RTV PO " + driver.scenarioData.getCurrentPurchaseOrderNumber() + " has been changed")));
        LOGGER.info("verifyRtvConfirmationInNwbcPage completed");
    }

    /**
     * verifies if Article Document Customer Invoice Document Year Fields are empty
     */
    public void clearTextFromArticleDocumentCustomerInvoiceDocumentYearField() {
        LOGGER.info("clearTextFromArticleDocumentCustomerInvoiceDocumentYearField started");
        articleDocumentInput.clear();
        customerInvoiceInput.clear();
        documentYear.clear();
        commonActions.clickOnExactText("Continue");
        LOGGER.info("clearTextFromArticleDocumentCustomerInvoiceDocumentYearField Completed");
    }

    /**
     * Save the purchase order number from NWBC Page
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void savePurchaseOrderNumbersInNWBCPage(String elementName) {
        LOGGER.info("savePurchaseOrderNumbersInNWBCPage started");
        commonActions.savePurchaseOrderNumber(returnElement(elementName));
        LOGGER.info("savePurchaseOrderNumbersInNWBCPage completed");
    }

    /**
     * Enters the saved return to vendor purchase order.
     *
     * @param element - send in elementName and gets the WebElement from returnElement method.
     */
    public void getAndEnterPurchaseOrderNumberInNwbc(String element) {
        LOGGER.info("getAndEnterPurchaseOrderNumberInNwbc started");
        commonActions.getAndEnterPurchaseOrderNumber(returnElement(element));
        LOGGER.info("getAndEnterPurchaseOrderNumberInNwbc Completed");
    }

    /**
     * Checks if the delete indicator is visible or not
     */
    public void verifyDeleteIndicatorIsVisible() {
        LOGGER.info("verifyDeleteIndicatorIsVisible started");
        Assert.assertTrue("FAIL: Delete indicator not visible", driver.isElementDisplayed(deleteIndicator));
        LOGGER.info("verifyDeleteIndicatorIsVisible completed");
    }

    /**
     * Checks for the RTV PO reversed success message.
     */
    public void checkReversedRTVPOSuccessMessage() {
        LOGGER.info("checkReversedRTVPOSuccessMessage started");
        Assert.assertTrue(" Fail:RTV Po was not reversed", rtvPOSuccessMessage.getText().contains("RTV Purchase Order "
                + driver.scenarioData.getCurrentPurchaseOrderNumber() + " reversed successfully"));
        LOGGER.info("checkReversedRTVPOSuccessMessage completed");
    }

    /**
     * Asserts Article Document Numbers are visible on the Reversal Movement Item details.
     */
    public void verifyArticleNumberInPurchaseOrderHistoryTab() {
        LOGGER.info("verifyArticleNumberInPurchaseOrderHistoryTab started");
        Assert.assertTrue("Fail:articleDocumentNumber is not displayed", driver.isElementDisplayed(articleDocumentNumber1));
        Assert.assertTrue("Fail:articleDocumentNumber is not displayed", driver.isElementDisplayed(articleDocumentNumber2));
        LOGGER.info("verifyArticleNumberInPurchaseOrderHistoryTab completed");
    }

    /**
     * inputs tracking numbers into return information iframe
     */
    public void inputTrackingNumbersInReturnInformation(int quantity) {
        LOGGER.info("inputTrackingNumbersInReturnInformation started");
        List<WebElement> inputFields = webDriver.findElements(trackingNumbersInputInReturnInformationBy);
        for (int i = 0; i < quantity; i++) {
            driver.scenarioData.setData(TRACKING + (i + 1), AUTOUSER0 + (i + 1));
            driver.performSendKeysWithActions(inputFields.get(i), driver.scenarioData.getData(TRACKING + (i + 1)));
            driver.performKeyAction(Keys.TAB);
        }
        LOGGER.info("inputTrackingNumbersInReturnInformation completed");
    }

    /**
     * Verifies the values entered into return information using Driver assertElementAttributeString class based on the
     * elements value property.
     */
    public void verifyReturnInformationTabValues(String driverName, String rga, String bolpro, String originalPo) {
        LOGGER.info("verifyReturnInformationTabValues started");
        List<WebElement> input = webDriver.findElements(CommonActions.inputTagBy);
        driver.assertElementAttributeString(input.get(0), Constants.VALUE, driverName);
        driver.assertElementAttributeString(input.get(1), Constants.VALUE, rga);
        driver.assertElementAttributeString(input.get(2), Constants.VALUE, bolpro);
        driver.assertElementAttributeString(input.get(3), Constants.VALUE, originalPo);
        driver.assertElementAttributeString(input.get(4), Constants.VALUE, TRACKING_NUMBER_1);
        driver.assertElementAttributeString(input.get(5), Constants.VALUE, TRACKING_NUMBER_2);
        driver.assertElementAttributeString(input.get(6), Constants.VALUE, TRACKING_NUMBER_4);
        LOGGER.info("verifyReturnInformationTabValues completed");
    }

    /**
     * This method will clear the 9th field in tracking number input list
     *
     * @param rowNumber - Row number in which tracking number should be cleared
     */
    public void clearTrackingNumber(int rowNumber) {
        LOGGER.info("clearTrackingNumber started");
        List<WebElement> input = webDriver.findElements(trackingNumbersInputInReturnInformationBy);
        input.get(rowNumber - 1).clear();
        LOGGER.info("clearTrackingNumber completed");
    }

    /**
     * Saves Return Purchase order when create from NWBC under "Returns-->create return to vendor"
     */
    public void saveReturnPurchaseOrder() {
        LOGGER.info("saveReturnPurchaseOrder started");
        driver.waitForElementVisible(rtvConfirmation);
        String purchaseOrderNumber = rtvConfirmation.getText().replaceAll("[^0-9]", "");
        driver.scenarioData.setCurrentPurchaseOrderNumber(purchaseOrderNumber);
        if (purchaseOrderNumber.isEmpty()) {
            Assert.fail("Purchase Order Number is Empty");
        }
        LOGGER.info("Saved Purchase Order: "+driver.scenarioData.getCurrentPurchaseOrderNumber());
        LOGGER.info("saveReturnPurchaseOrder completed");
    }

    /**
     * switches to Iframe in NWBC page with NWBC's WebElement
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void switchToIframeInNwbc(String elementName) {
        LOGGER.info("switchToIframeInNwbc started");
        commonActions.switchFrameContext(returnElement(elementName));
        LOGGER.info("switchToIframeInNwbc completed");
    }

    /**
     * Verifies the value attribute of Individual WebElements in NWBC
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     * @param validation  - value to verify
     */
    public void verifyValueAttributeInNwbc(String elementName, String validation) {
        LOGGER.info("verifyValueAttributeInNwbc started");
        driver.assertElementAttributeString(returnElement(elementName), Constants.VALUE, validation);
        LOGGER.info("verifyValueAttributeInNwbc completed");
    }

    /**
     * validates the Tracking numbers in SE16N
     */
    public void validateTrackingNumbersInSe16n() {
        LOGGER.info("validateTrackingNumbersInSe16n started");
        try {
            Assert.assertTrue(TRACKING_ASSERT_MESSAGE,
                    driver.isElementDisplayed(commonActions.returnElementWithExactText(TRACKING_NUMBER_1)));
            Assert.assertTrue(TRACKING_ASSERT_MESSAGE,
                    driver.isElementDisplayed(commonActions.returnElementWithExactText(TRACKING_NUMBER_2)));
            Assert.assertTrue(TRACKING_ASSERT_MESSAGE,
                    driver.isElementDisplayed(commonActions.returnElementWithExactText(TRACKING_NUMBER_4)));
        } catch (NoSuchElementException ne) {
            Assert.assertTrue(TRACKING_ASSERT_MESSAGE,
                    driver.isElementDisplayed(webDriver.findElement(By.xpath(CommonActions.VALUE_XPATH +
                            TRACKING_NUMBER_1 + "')]"))));
            Assert.assertTrue(TRACKING_ASSERT_MESSAGE,
                    driver.isElementDisplayed(webDriver.findElement(By.xpath(CommonActions.VALUE_XPATH +
                            TRACKING_NUMBER_1 + "')]"))));
            Assert.assertTrue(TRACKING_ASSERT_MESSAGE,
                    driver.isElementDisplayed(webDriver.findElement(By.xpath(CommonActions.VALUE_XPATH +
                            TRACKING_NUMBER_1 + "')]"))));
        }
        LOGGER.info("validateTrackingNumbersInSe16n completed");
    }

    /**
     * Enter comment in purchase Order popup
     *
     * @Param commonText- Text to be entered into the Purchase Order Comment Text Box
     */
    public void enterCommentInPurchaseOrderPopup(String commentText) {
        LOGGER.info("enterCommentInPurchaseOrderPopup Started");
        Actions actions = new Actions(webDriver);
        actions.doubleClick(purchaseOrderHeaderPopupCommentInput).sendKeys(commentText).build().perform();
        LOGGER.info("enterCommentInPurchaseOrderPopup Completed");
    }

    /**
     * This method includes all the steps required to change the site to desired site location
     *
     * @param site - site number
     */
    public void setTheSiteLocationInNwbc(String site) {
        LOGGER.info("setTheSiteLocationInNwbc started");
        commonActions.clickOnExactText(REPORTS);
        commonActions.clickOnExactText(CHANGE_SITE);
        commonActions.switchFrameContext(0);
        driver.clearInputAndSendKeys(siteInputField, site);
        commonActions.clickOnExactText(SAVE_BUTTON_TEXT1);
        if (driver.isElementDisplayed(errorImageForSelectSite)) {
            Assert.fail("Error Message appeared while entering the Site Number");
        }
        commonActions.switchFrameContext(Constants.DEFAULT_CONTENT);
        LOGGER.info("setTheSiteLocationInNwbc completed");
    }

    /**
     * Selects the article document Number which is not in transit and will edit the 32nds, Miles On and Miles Off
     * in mim adjustment's "RTV Pending" table
     */
    public void editWareHouseMimAdjustmentArticles(String elementName) {
        LOGGER.info("editWareHouseMimAdjustmentArticles started");
        driver.waitForPageToLoad();
        if (driver.isElementDisplayed(returnElement(elementName))) {
            while (counter != 0) {
                selectRowInTable(mimAdjustmentTableIndex);
                commonActions.clickOnExactText(EDIT_BUTTON_TEXT_2);
                if (driver.isElementDisplayed(errorImageForSelectSite)) {
                    mimAdjustmentTableIndex++;
                    editWareHouseMimAdjustmentArticles(elementName);
                } else {
                    counter = 0;
                }
            }
            sendKeysIntoInputFieldsInItemsTableInNwbc("Dept of Trans#", mimAdjustmentTableIndex + 1, Constants.DEPT_OF_TRANS);
            sendKeysIntoInputFieldsInItemsTableInNwbc("32nds", mimAdjustmentTableIndex + 1, THIRTY_TWO_NDS);
            sendKeysIntoInputFieldsInItemsTableInNwbc("Miles On", mimAdjustmentTableIndex + 1, MILES_ON);
            sendKeysIntoInputFieldsInItemsTableInNwbc("Miles Off", mimAdjustmentTableIndex + 1, MILES_OFF);
            commonActions.clickOnExactText(SAVE_BUTTON_TEXT1);
            if (driver.isElementDisplayed(errorImageForSelectSite)) {
                Assert.fail("Edit was not succesful");
            }
            selectRowInTable(mimAdjustmentTableIndex);


        } else {
            commonActions.click(viewMimAdjustment);
            if (elementName.equalsIgnoreCase(AUTOMATION_RTV_VIEW)) {
                if (driver.isElementDisplayed(automationRtvDropDownOption)) {
                    commonActions.click(automationRtvDropDownOption);
                    editWareHouseMimAdjustmentArticles(elementName);
                } else {
                    Assert.fail("AutomationRTV Drop Down Option Is not Present");
                }
            } else if (elementName.equalsIgnoreCase(RTV_PENDING_VIEW)) {
                if (driver.isElementDisplayed(rtvPendingDropDownOption)) {
                    commonActions.click(rtvPendingDropDownOption);
                    editWareHouseMimAdjustmentArticles(elementName);
                } else {
                    Assert.fail("RTV pending Drop Down Option Is not Present");
                }
            }
        }
        counter = 3;
        mimAdjustmentTableIndex = 2;
        LOGGER.info("editWareHouseMimAdjustmentArticles completed");
    }

    /**
     * Enters a value into BOL/PRO field in mim adjustment's return to vendor iframe.
     */
    public void enterBolProNumber() {
        LOGGER.info("enterBolProNumber started");
        if (driver.isElementDisplayed(bolProLabelField)) {
            commonActions.sendKeys(PurchaseOrder.bolPro, TRACKING_NUMBER_2);
        } else {
            Assert.fail("Field verification failed. There is no BOL/PRO# field present");
        }
        LOGGER.info("enterBolProNumber completed");
    }

    /**
     * This is a Generic method to verify if the success image(Green Check) is displayed
     * at the end of transactions.
     */
    public void verifySuccessStatus() {
        LOGGER.info("verifySuccessStatus started");
        driver.waitForPageToLoad();
        if (!driver.isElementDisplayed(successImageForAllTransactions, null,Constants.THIRTY)) {
            Assert.fail("Transaction Unsuccessful!!");
        }
        LOGGER.info("verifySuccessStatus completed");
    }

    /**
     * This is a Generic method to verify if the error image(red exclamation) is displayed
     * at the end of transactions.
     */
    public void verifyErrorStatus() {
        LOGGER.info("verifyErrorStatus started");
        driver.waitForElementVisible(errorImageForSelectSite);
        if (!driver.isElementDisplayed(errorImageForSelectSite)) {
            Assert.fail("Success message appeared!!");
        }
        LOGGER.info("verifyErrorStatus completed");
    }

    /**
     * Saves the Article document number, Article Item number and Vendor Number from mim adjustments details tab
     */
    public void saveDetailsFromMimAdjustmentTable() {
        LOGGER.info("saveDetailsFromMimAdjustmentTable started");
        mimScenarioData.add(0, commonActions.returnElementWithLabelName("Article Document Num").
                getAttribute(Constants.VALUE).replaceAll("[^0-9]", ""));
        mimScenarioData.add(1,commonActions.returnElementWithLabelName("Art. Doc.Item").
                getAttribute(Constants.VALUE).replaceAll("[^0-9]", ""));
        mimScenarioData.add(2, commonActions.returnElementWithLabelName("Vendor").
                getAttribute(Constants.VALUE).replaceAll("[^0-9]", ""));
        mimScenarioData.add(3, mimScenarioData.get(0) + mimScenarioData.get(1));
        mimScenarioData.add(4, commonActions.returnElementWithLabelName("Mat. Doc. Year").
                getAttribute(Constants.VALUE).replaceAll("[^0-9]", ""));
        LOGGER.info("saveDetailsFromMimAdjustmentTable completed");
    }

    /**
     * saves the details of a line item in adjustment table in NWBC
     *
     * @param elementName - Name of the detail
     */
    public void saveDetailsFromMimAdjustmentTable(String elementName) {
        LOGGER.info("saveDetailsFromMimAdjustmentTable started");
        WebElement detailsTabElement = webDriver.findElement(By.xpath("//*[text()='" + elementName + "']/following::input[1]"));
        driver.scenarioData.setData(elementName, detailsTabElement.getAttribute(Constants.VALUE).replaceAll("[^0-9]", ""));
        LOGGER.info("saveDetailsFromMimAdjustmentTable completed");
    }

    /**
     * Verifies line item is displayed on NWBC page
     *
     * @param elementName string to verify the value of
     * @param message     Message to check on NWBC page
     */
    public void assertTextDisplayedInNWBC(String elementName, String message) {
        LOGGER.info("assertTextDisplayedInNWBC started");
        driver.waitForElementVisible(returnElement(elementName));
        driver.verifyTextDisplayed(returnElement(elementName), message);
        LOGGER.info("assertTextDisplayedInNWBC completed");
    }

    /**
     * This method will enter purchase order/Article Document number in mim adjustment table's Filter and searches for
     * order number and selects the first row of the search results.
     */
    public void enterPurchaseOrderNumberIntoMimAdjustmentFilter() {
        LOGGER.info("enterPurchaseOrderNumberIntoMimAdjustmentFilter started");
        commonActions.sendKeysWithLabelName(ARTICLE_DOCUMENT_LABEL, driver.scenarioData.getData(ARTICLE_DOCUMENT_NUMBER_DETAILS_KEY));
        commonActions.clickOnExactText(CommonActions.CONTINUE);
        WebElement lineItem = webDriver.findElement(By.xpath("//*[text()='" + driver.scenarioData.getData(ARTICLE_DOCUMENT_NUMBER_DETAILS_KEY) + "']/preceding::div[1]"));
        commonActions.click(lineItem);
        LOGGER.info("enterPurchaseOrderNumberIntoMimAdjustmentFilter completed");
    }

    /**
     * enters Article Document Number, Article Item Code and Article Year in the input fields which will appear when
     * trying to do Undo Scrap.
     */
    public void enterDetailsToUndoScrap() {
        LOGGER.info("enterDetailsToUndoScrap started");
        commonActions.click(webDriver.findElements(correctiveActionsRadioButtonsBy).get(5));
        driver.waitForMilliseconds(Constants.THREE_THOUSAND);
        driver.clearInputAndSendKeys(scrappedArticleDocNumberInputField, mimScenarioData.get(0));
        driver.clearInputAndSendKeys(scrappedArticleDocYearNumber, mimScenarioData.get(4));
        driver.clearInputAndSendKeys(scrappedArticleDocItemNumber, mimScenarioData.get(1));
        LOGGER.info("enterDetailsToUndoScrap completed");
    }

    /**
     * This methods include the steps to set the reason for goods movement to a particular value when
     * trying to do a Scrap transaction.
     */
    public void setReasonForGoodsMovement() {
        LOGGER.info("setReasonForGoodsMovement started");
        List<WebElement> element = webDriver.findElements(scrapPopUpInputTagElementsBy);
        commonActions.click(element.get(3));
        commonActions.clickOnExactText(UNEVEN_WEAR);
        commonActions.clickOnExactText(SAVE_BUTTON_TEXT);
        LOGGER.info("setReasonForGoodsMovement completed");
    }

    /**
     * This method will input the article document number which is stored in "mimScenarioData" Array list in
     * NetWeaverBusinessClient.java into the element passed
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void enterArticleDocumentNumberInNwbc(String elementName) {
        LOGGER.info("enterArticleDocumentNumberInNwbc started");
        driver.clearInputAndSendKeys(returnElement(elementName), mimScenarioData.get(0));
        LOGGER.info("enterArticleDocumentNumberInNwbc completed");
    }

    /**
     * This method is to check if an element is displayed
     *
     * @param elementName send in the name of element and returnElement method will return the WebElement
     */
    public void isElementDisplayedInNwbc(String elementName) {
        LOGGER.info("isElementDisplayedInNwbc started");
        driver.isElementDisplayed(returnElement(elementName));
        LOGGER.info("isElementDisplayedInNwbc completed");
    }

    /**
     * Checks if the System ClipBoard Contains the Article document number String
     */
    public void verifyArticleNumberInPDF() {
        LOGGER.info("verifyArticleNumberInPDF started");
        String articleDocumentNumber = driver.scenarioData.getData("Article Document Num");
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        try {
            String data = (String) clipboard.getData(DataFlavor.stringFlavor);
            data = data.replaceAll("\\D+", "").trim();
            Assert.assertTrue("FAIL: The data you copied is: " + data.toLowerCase() +
                            " And the text being validated is: " + articleDocumentNumber,
                    data.toLowerCase().contains(articleDocumentNumber.toLowerCase()));
        } catch (HeadlessException e) {
            e.printStackTrace();
        } catch (UnsupportedFlavorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.info("verifyArticleNumberInPDF completed");
    }

    /**
     * method will return web element related to a specific column name and row number
     *
     * @param columnName - Name of the column in NWBC
     * @param rowNumber  - Row number
     * @return
     */
    private WebElement inputElementItemsTableInNwbc(String columnName, int rowNumber) {
        LOGGER.info("inputElementItemsTableInNwbc started");
        WebElement inputField = null;
        List<WebElement> itemsTableColumnHeadings = webDriver.findElements(itemsTableColumnHeadingsBy);
        for (int i = 1; i < itemsTableColumnHeadings.size(); i++) {
            if (itemsTableColumnHeadings.get(i).getText().toLowerCase().contains(columnName.toLowerCase())) {

                webDriver.findElements(By.xpath("//tbody[contains(@id,'contentTBody')]/child::tr[" + (rowNumber + 1) + "]/child::td")).size();
                inputField = webDriver.findElements(By.xpath("//tbody[contains(@id,'contentTBody')]/child::tr[" + (rowNumber + 1) + "]/child::td")).get(i);
                driver.waitForMilliseconds();
                break;
            }
        }
        LOGGER.info("inputElementItemsTableInNwbc completed");
        return inputField;
    }

    /**
     * method to input the value into the web element into the table in NWBC
     *
     * @param columnName - Name of the column
     * @param rowNumber  - Row number
     * @param value      - value to enter
     */
    public void sendKeysIntoInputFieldsInItemsTableInNwbc(String columnName, int rowNumber, String value) {
        LOGGER.info("sendKeysIntoInputFieldsInItemsTableInNwbc started");
        try {
            driver.performSendKeysWithActions(inputElementItemsTableInNwbc(columnName, rowNumber), value);
        } catch (StaleElementReferenceException s) {
            driver.performSendKeysWithActions(inputElementItemsTableInNwbc(columnName, rowNumber), value);
        }
        LOGGER.info("sendKeysIntoInputFieldsInItemsTableInNwbc completed");
    }

    /**
     * selects the row with its number
     *
     * @param rowNumber - Row number which has to be selected
     */
    public void selectRowInTable(int rowNumber) {
        LOGGER.info("selectRowInTable started");
        driver.waitForMilliseconds();
        commonActions.click(webDriver.findElements(tableRowSelectBy).get(rowNumber - 1));
        LOGGER.info("selectRowInTable completed");
    }

    public void getPurchaseOrderFromOpenReturns() {
        LOGGER.info("getPurchaseOrderFromOpenReturns started");
        boolean isFound = false;
        commonActions.clickOnExactText("Refresh");
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        for (int i = 0; i <= 10; i++) {
            commonActions.clickOnExactText("Purchasing Document");
            commonActions.clickOnPartialText("User-Defined");
            commonActions.switchToMainWindow();
            commonActions.switchFrameContext(0);
            commonActions.sendKeysWithLabelName("Purchasing Document",
                    driver.scenarioData.getCurrentPurchaseOrderNumber());
            driver.performKeyAction(Keys.ENTER);
            commonActions.switchToMainWindow();
            commonActions.switchFrameContext(1);
            if (isOpenPurchaseOrderFound()) {
                commonActions.click(webDriver.findElement(By.xpath("//*[text()='" +
                        driver.scenarioData.getCurrentPurchaseOrderNumber() + "']/preceding::div[3]")));
                isFound = true;
                break;
            } else {
                commonActions.clickOnExactText(Constants.REFRESH);
                driver.waitForMilliseconds(Constants.SEVEN_THOUSAND);
            }
        }
        Assert.assertTrue("FAIL: Open Purchase order number is not found.", isFound);
        LOGGER.info("getPurchaseOrderFromOpenReturns completed");
    }

    public boolean isOpenPurchaseOrderFound() {
        LOGGER.info("isOpenPurchaseOrderFound started");
        boolean isFound = false;
        List<WebElement> elements = webDriver.findElements(By.xpath("//*[contains(@id,'contentTBody')]//following::tr"));
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).getText().contains(driver.scenarioData.getCurrentPurchaseOrderNumber() )) {
                isFound = true;
                break;
            }
        }
        LOGGER.info("isOpenPurchaseOrderFound completed");
        return isFound;
    }
}