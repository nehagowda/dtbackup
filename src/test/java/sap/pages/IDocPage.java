package sap.pages;


import common.Constants;
import dtc.data.Customer;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtils;
import utilities.Driver;
import common.Config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class IDocPage {


    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private Customer customer;
    private final Logger LOGGER = Logger.getLogger(NetWeaverBusinessClient.class.getName());

    public IDocPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        customer = new Customer();
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "M0:U:1:2B257::6:34")
    private static WebElement iDocNumberInputField;

    @FindBy(id = "M0:D:13::btn[8]")
    private static WebElement execute;

    @FindBy(id = "M0:U:::2:24-img")
    public static WebElement greenLight;

    @FindBy(id = "M0:U:::3:20")
    private static WebElement iDocBasicType;

    @FindBy(id = "M1:D:13::btn[12]-img")
    private static WebElement close;

    @FindBy(xpath = "//input[contains(@id, \"[5,2]_c\")]")
    private static WebElement vehicleMake;

    @FindBy(xpath = "//input[contains(@id, \"[6,2]_c\")]")
    private static WebElement vehicleModel;

    @FindBy(xpath = "//input[contains(@id, \"[7,2]_c\")]")
    private static WebElement vehicleYear;

    @FindBy(xpath = "//input[@id='tbl12607[9,1]_c']")
    private static WebElement itemQuantity;

    @FindBy(xpath = "//input[contains(@id, \"[5,2]_c\")]")
    private static WebElement customerFirstName;

    @FindBy(xpath = "//input[contains(@id, \"[6,2]_c\")]")
    private static WebElement customerLastName;

    @FindBy(xpath = "//input[contains(@id, \"[7,2]_c\")]")
    private static WebElement customerAddress;

    @FindBy(xpath = "//input[contains(@id, \"[8,2]_c\")]")
    private static WebElement customerCity;

    @FindBy(xpath = "//input[contains(@id, \"[1,2]_c\")]")
    private static WebElement communicationTypeHDR;

    @FindBy(xpath = "//input[contains(@id, \"[2,2]_c\")]")
    private static WebElement orderNumberHDR;

    @FindBy(linkText = "Z1CUST_NOTIFY_HDR")
    private static WebElement customerNotifyHeaderLink;

    @FindBy(linkText = "Z1CUST_NOTIFY_ITM")
    private static WebElement customerNotifyItemLink;

    @FindBy(xpath = "//td[contains(@id,'Datarecords')]//td[@class = 'lsSTHierarchicalSpace']//span[@title = 'Expand Node']")
    private static WebElement customerNotifyHeaderButton;

    @FindBy(xpath = "//td[contains(@id,'0000')]//td[@class = 'lsSTHierarchicalSpace']//span[@title = 'Expand Node']")
    private static WebElement customerNotifyItemButton;

    @FindBy(xpath = "//*[text()='Existing IDoc']//following::input[1]")
    private static WebElement existingIDocNumber;

    @FindBy(xpath = "//*[contains(text(),'RS') and contains (@class, 'lsAbapList__')][1]")
    private static WebElement vendorInvoiceIDoc;

    @FindBy(id = "M1:U:1::1:21")
    private static WebElement partnerID;

    @FindBy(xpath = "//*[contains(text(),'E1EDK02')]/following::*[2]")
    private static WebElement invoiceIDocNumber;

    @FindBy(id = "M1:U:1::1:21")
    private static WebElement invoiceIDocNumberField;

    @FindBy(xpath = "//*[@id='M1:37::btn[0]']")
    private static WebElement continueEnter;

    @FindBy(xpath = "//*[contains(text(),'Standard Inbound')]")
    private static WebElement standardInbound;

    @FindBy(id = "M0:U:::27:5")
    private static WebElement articleOneInitialExpand;

    @FindBy(id = "M0:U:::36:5")
    private static WebElement articleOneExpand;

    @FindBy(id = "M0:U:::37:5")
    private static WebElement articleTwoExpand;

    @FindBy(id = "M0:U:::31:40_l")
    private static WebElement poUpdateArticleOneSection;

    @FindBy(xpath = "//*[contains(text(),'Information')]//following::*[contains(@lsdata,'IDoc')]")
    private static WebElement idocNumberOutput;

    @FindBy(xpath = "//*[text()='IDOC']//following::input[1]")
    private static WebElement idocInputFieldInfoCenter;

    @FindBy(id = "M2:D:13::btn[0]")
    private static WebElement checkOk;

    @FindBy(id = "M0:D:13::btn[8]-img")
    private static WebElement userSearchExecuteButton;

    @FindBy(xpath = "//*[contains(@id,'1,2#if')][not(contains(@id,'1,2#if-r'))]")
    private static WebElement recordId;

    @FindBy(xpath = "//*[contains(@lsdata,'Overview')]")
    private static WebElement sapDocumentHeader;

    @FindBy(xpath = "//*[@title='Invoice details ']")
    private static WebElement invoiceNumber;

    @FindBy(xpath = "//iframe[@id = 'URLSPW-1']")
    private static WebElement iDocGeneratoriframe;

    @FindBy(xpath = "//iframe[@id = 'URLSPW-0']")
    private static WebElement firstIframe;

    @FindBy(xpath = "//*[text()='100']")
    private static WebElement iDocRecordStatus;

    @FindBy(id = "M1:U:1::5:21")
    private static WebElement costCenterInput;

    @FindBy(xpath = "//input[@id='M1:U:1::13:21']")
    private static WebElement refDocNo;

    @FindBy(id = "M1:U:1::7:21")
    private static WebElement docDate;

    @FindBy(id = "M1:U:1::8:21")
    private static WebElement postingDate;

    @FindBy(id = "M0:U:::5:36_l")
    private static WebElement vendorNumberInputPopUp;

    @FindBy(xpath = "//*[contains(text(),'EDIDC')]/following::*[2]")
    private static WebElement ediVendorPopUp;

    @FindBy(id = "M1:U:::2:58")
    private static WebElement ediPopUpVendorInputField;

    @FindBy(className = "lsField__inputcontainer")
    private static WebElement programInputField;

    @FindBy(className = "lsField__inputcontainer")
    private static WebElement backgroundExecutionResultPageTitle;

    @FindBy(className = "lsField__inputcontainer")
    private static WebElement backgroundFirstPage;

    @FindBy(xpath = "//div[@class='urST5SCMetricInner']")
    private static WebElement selectRecordLineItemInDol;

    @FindBy(id = "M0:U:2:1:1:2B256::6:20")
    private static WebElement companyCodeInputInPosting;

    @FindBy(id = "M0:U:2:1:1:2B256::17:21-img")
    private static WebElement preApprovalCheckBox;

    @FindBy(xpath = "//*[contains(text(),'Coding')]/following::*[contains(@id,'1,1#if-r')]")
    private static WebElement glEditBox;

    @FindBy(xpath = "//img[@action='close']")
    private static WebElement closePopUp;

    @FindBy(id = "M0:U:1:2B257::6:34")
    private static WebElement dateInDolAp2n;

    @FindBy(id = "M0:D:13::btn[17]-img")
    private static WebElement copySapNavigation;

    @FindBy(id = "M0:D:13::btn[18]-img")
    private static WebElement insertSapNavigation;

    @FindBy(id = "M0:U:::4:8_l")
    private static WebElement e1bpacgl09;

    @FindBy(id = "M0:U:::8:8_l")
    private static WebElement e1bpaccr09;

    @FindBy(id = "M1:U:1::0:21")
    private static WebElement itemNoAccount;

    @FindBy(xpath = "//*[contains(text(),'E1EDK03')]/following::*[2]")
    private static WebElement creditEditField;

    @FindBy(id = "M1:U:1::4:21")
    private static WebElement amtOccur;

    @FindBy(xpath = "//*[contains(@id,'2,1#if-r')]")
    private static WebElement glEditBox2;

    @FindBy(xpath = "//*[contains(@id,'3,1#if-r')]")
    private static WebElement glEditBox3;

    @FindBy(xpath = "//*[contains(@id,'4,1#if-r')]")
    private static WebElement glEditBox4;

    @FindBy(id = "M1:U:1::2:21")
    private static WebElement itemTextFirstPopUp;

    @FindBy(xpath = "//*[contains(@id,'1,9#if')][not(contains(@id,'1,9#if-r'))]")
    private static WebElement poItem1InPTSHeader;

    @FindBy(xpath = "//*[contains(@id,'2,9#if')][not(contains(@id,'2,9#if-r'))]")
    private static WebElement poItem2InPTSHeader;

    @FindBy(xpath = "//*[contains(@id,'2,10#if')][not(contains(@id,'2,10#if-r'))]")
    private static WebElement gdsInvRcvdQa;

    @FindBy(xpath = "//*[contains(@id,'2,9#if')][not(contains(@id,'2,9#if-r'))]")
    private static WebElement gdsInvRcvdStg;

    @FindBy(xpath = "//*[text()='Tax Amount']//following::input[1]")
    private static WebElement taxAmount;

    @FindBy(xpath = "//*[text()='Tax Amount']//following::input[2]")
    private static WebElement taxCode;

    @FindBy(xpath = "//span[@id=\"M0:U:1:4:2:1:2:1::0:2-title\"]")
    private static WebElement articleData;

    @FindBy(xpath = "//input[@id=\"M0:U:1:4:2:1:2:1:2B258:1::1:18\"]")
    private static WebElement vendorArticleNumber;

    @FindBy(id = "M0:U:1:4:2:1:1::0:14-btn")
    private static WebElement itemDropDown;

    @FindBy(id = "M1:U:1::10:21")
    private static WebElement taxAmountInputField;

    @FindBy(id = "M0:U:1:2B264::0:16")
    private static WebElement vendorInFb60;

    @FindBy(xpath = "//*[text()='Invoice date']/following::input[1]")
    private static WebElement invoiceDateFb60;

    @FindBy(id = "M0:U:1:2B264::4:16")
    private static WebElement amountFb60;

    @FindBy(id = "M0:U:1:2B264::6:16")
    private static WebElement textFb60;

    @FindBy(id = "M0:U:1:2B264::1:47")
    private static WebElement referenceFb60;

    @FindBy(xpath = "//*[(contains(@id,'tb') and contains(@id,'[1,2]_c')) and not (contains(@id,'[1,2]_c-r'))]")
    private static WebElement glAccountFb60;

    @FindBy(xpath = "//*[(contains(@id,'tb') and contains(@id,'[1,18]_c')) and not (contains(@id,'[1,18]_c-r'))]")
    private static WebElement costCenterFb60;

    @FindBy(xpath = "//*[(contains(@id,'tb') and contains(@id,'[1,5]_c')) and not (contains(@id,'[1,5]_c-r'))]")
    private static WebElement amountInDocFb60;

    @FindBy(xpath = "//td[contains(@id,'tb') and contains(@id,'[1,1]')]")
    private static WebElement statusLineCheckFb60;

    @FindBy(xpath = "//span[contains(@id,'grid') and contains(@id,'1,1#if')]")
    private static WebElement verifyVendorFb60;

    @FindBy(xpath = "//span[contains(@id,'grid') and contains(@id,'2,1#if')]")
    private static WebElement verifyGlFb60;

    @FindBy(id = "M0:36::btn[11]-img")
    private static WebElement saveFb60;

    @FindBy(xpath = "//span[@id='wnd[0]/sbar_msg-txt']")
    private static WebElement docNumber;

    @FindBy(id = "M0:46:::1:17")
    private static WebElement verifyDocumentNumber;

    @FindBy(id = "M1:U:::3:9_l")
    private static WebElement displayMessage;

    @FindBy(xpath = "//input[contains(@id,',13#if')]")
    private static WebElement getDocumentValue;

    @FindBy(id = "M0:U:::2:25")
    private static WebElement documentNumberInputFb03;

    @FindBy(id = "M0:U:::4:25")
    private static WebElement fiscalYearInputFb03;

    @FindBy(xpath = "//*[contains(@id,'grid#603#2,1')]/child::div")
    private static WebElement accountNrTab;

    @FindBy(xpath = "//*[contains(@id,'2,8#if-r')]/child::*")
    private static WebElement costCenterNrTab;

    @FindBy(xpath = "//*[contains(@id,'grid#603#2,8#if-r')]/child::*")
    private static WebElement profitCtrTab;

    @FindBy(xpath = "//*[contains(@id,'603#3,1')]/child::*")
    private static WebElement accountNrTab2;

    @FindBy(xpath = "//*[contains(@id,'3,8#if-r')]/child::*")
    private static WebElement costCenterNrTab2;

    @FindBy(xpath = "//*[contains(@id,'grid#603#3,8#if-r')]/child::*")
    private static WebElement profitCtrTab2;

    @FindBy(id = "M0:U:::14:5")
    private static WebElement inboundProcessingInWper;

    @FindBy(id = "M0:U:::17:9")
    private static WebElement documentsProcessedInWper;

    @FindBy(id = "M0:U:::20:13")
    private static WebElement azf01Store;

    @FindBy(xpath = "//div[contains(text(),'WDAZ')]/preceding::div[4]")
    private static WebElement wdaz0Store;

    @FindBy(xpath = "//div[contains(text(),'WDTX')]/preceding::div[4]")
    private static WebElement wdtx0Store;

    @FindBy(xpath = "//div[contains(text(),'WPUBON')]/preceding::div[contains(@lsdata,'2_4.png')][1]")
    private static WebElement salesDataNotAggregated;

    @FindBy(xpath = "//div[contains(text(),'WPUTAB')]/preceding::div[contains(@lsdata,'2_4.png')][1]")
    private static WebElement wputab;

    @FindBy(xpath = "//div[contains(text(),'WPUUMS')]/preceding::div[contains(@lsdata,'2_4.png')][1]")
    private static WebElement wpuums;

    @FindBy(id = "M0:D:13::btn[6]")
    private static WebElement expanddSubTreeButton;

    @FindBy(xpath = "//div[contains(text(),'STR')][1]/preceding::div[2]")
    private static WebElement accountFieldForStr;

    @FindBy(xpath = "//div[contains(text(),'Sales Tax')][1]/preceding::div[2]")
    private static WebElement salesTaxAccountField;

    @FindBy(xpath = "//div[contains(text(),'Environmental')][1]/preceding::div[2]")
    private static WebElement environmentalFeesAccountField;

    @FindBy(xpath = "//div[contains(text(),'Sales Rev')][1]/preceding::div[2]")
    private static WebElement salesRevAccountField;

    @FindBy(xpath = "//input[contains(@id,'[1,10]_c')]")
    private static WebElement movementType;

    @FindBy(id = "M0:D:13::btn[17]")
    private static WebElement getVariant;

    @FindBy(xpath = "//input[contains(@value,'QA_IDOC_PROC')][contains(@id,',2')]")
    private static WebElement qaidocProc;

    @FindBy(id = "M0:U:::1:34")
    private static WebElement creationTime;

    @FindBy(xpath = "//div[contains(text(),'Billing')]/preceding::div[2]")
    private static WebElement billingDocument;

    @FindBy(xpath = "//*[contains(text(),'Net Value')]/following::input[1]")
    private static WebElement totalInvoiceAmount;

    @FindBy(xpath = "//a[contains(@title,'Views')]")
    private static WebElement views;

    @FindBy(xpath = "//*[text()='Inbound']/following::*[contains(text(),'IDoc number')]/following::input[1]")
    public static WebElement iDocFieldInWper;

    @FindBy(xpath = "//div[text()='G/L account document'][2]/preceding::div[2]")
    public static WebElement glAccountForWpufib;

    @FindBy(xpath = "//input[contains(@value,'STR')][1]/preceding::input[1]")
    public static WebElement strDepositClearingInFb03;

    @FindBy(xpath = "//input[contains(@value,'Layaway')][1]/preceding::input[1]")
    public static WebElement layawayAccountInFb03;

    @FindBy(xpath = "//div[text()='WPUFIB']")
    public static WebElement wpufib;

    @FindBy(xpath = "//span[@title='Expand Node'][1]")
    private static WebElement expandNode;

    @FindBy(xpath = "(//*[contains(@id,'tb') and contains(@id,'[7,2]_c')])[2]")
    private static WebElement lVormText;

    @FindBy(xpath = "//div[contains(@id,'tree#') and contains(@id,'vscroll-Nxt')]")
    private static WebElement scrollInIdocPage;

    @FindBy(xpath = "//div[contains(@id,'vscroll-Nxt')]")
    private static WebElement scrollInDol;

    @FindBy(xpath = "//span[@title = 'Expand Node']//following::span[2]")
    private static WebElement firstLineEntryUnderZFC_APINVS;

    @FindBy(xpath = "//div[@title=\"Data Structure\"]//following::span[3]")
    private static WebElement edidcLineItem;

    @FindBy(xpath = "//*[text()='Z1BPDCHASSIS']")
    private static WebElement martRow;

    @FindBy(xpath = "(//*[contains(text(),'IDoc Number')]/following::div[2])[1]")
    private static WebElement idocForVehicleOutbound;

    @FindBy(xpath = "//*[contains(text(),'IDoc Number')]/following::input[1]")
    private static WebElement idocList;

    @FindBy(xpath = "//input[contains(@id,',3#if')][1]")
    private static WebElement getDateField;

    @FindBy(xpath = "//*[contains(text(),'Account') ] /following::input[2]")
    private static WebElement accountNrWper;

    @FindBy(xpath = "//*[contains(text(),'BOPIS Deposits')]/preceding::div[2]")
    private static WebElement bopisDepositValue;

    @FindBy(xpath = "//*[contains(@value,'BOPIS Deposits')]/following::input[4]")
    private static WebElement bopisAssignmentValue;

    @FindBy(xpath = "//*[contains(text(),'Assignment') ] / following::input[7]")
    private static WebElement assignmentNrWper;

    @FindBy(xpath = "//*[contains(text(),'Site') ] / following::input[6]")
    private static WebElement glSiteNrfb03;

    @FindBy(xpath = "//*[contains(text(),'Profit Ctr') ] / following::input[22]")
    private static WebElement glProfitNrfb03;

    @FindBy(xpath = "//*[contains(@title,'Create')]")
    private static WebElement createButtonInWe19;

    @FindBy(xpath = "//*[contains(@id,'2,6]_c')][not (contains(@id,'2,6]_c-r'))]")
    private static WebElement shortTextInputInRow2;

    @FindBy(xpath = "//*[contains(@id,'2,11]_c')][not (contains(@id,'2,11]_c-r'))]")
    private static WebElement netPriceInputInRow2;

    @FindBy(xpath = "//*[contains(@id,'2,14]_c')][not (contains(@id,'2,14]_c-r'))]")
    private static WebElement orderPerUnitInputInRow2;

    @FindBy(xpath = "//*[contains(@id,'2,15]_c')][not (contains(@id,'2,15]_c-r'))]")
    private static WebElement merchCategoryInputInRow2;

    @FindBy(xpath = "//*[contains(@id,'2,8]_c')][not (contains(@id,'2,8]_c-r'))]")
    private static WebElement orderUnitInputInRow2;

    @FindBy(xpath = "//*[contains(text(),'BKPFF')]/preceding::div[2]")
    private static WebElement prodIssuetab;

    private static final By netAmountValuesInTableBy = By.xpath("//input[contains(@id,',5]_c')]");
    private static final By assemblyRowBy = By.xpath("//*[text()='Z1BPDVEHOEM']");
    private static final By articleValuesInTableBy = By.xpath("//input[contains(@id,',6]_c')]");
    private static final By expandFolderE1edp01InWe19 = By.xpath("//*[contains(text(),'E1EDP01')]//preceding::img[1]");
    private static final By purchaseOrderBlocksInWe19 = By.xpath("//*[contains(text(),'E1EDP02')]/following::div[2]");
    private static final By errorInPostingMessageBy = By.xpath("//*[contains(text(),'messages')] //following::*[contains(@lsdata,'Error')][1]");
    private static final By subStatusSelectionBy = By.xpath("//*[@value='210']/preceding::td[2]");
    private static final By subStatusSelectionByText = By.xpath("//*[contains(text(),'210')]/preceding::td[2]");
    private static final By errorPostingMessageAtTheBottomBy = By.xpath("//*[contains(@lsdata,'ERROR')]");
    private static final By warningPostingMessageAtTheBottomBy = By.xpath("//*[contains(@lsdata,'WARNING')]");
    private static final By messageInPtsPopUpBy = By.xpath("//*[contains(text(),'messages')]//following::*[contains(@lsdata,'ledg.png')][1]");
    private static final By expandNodeBy = By.xpath("//span[@title='Expand Node'][1]");
    private static final By firstLineUnderZfcApInvsBy = By.xpath("//span[@title = 'Expand Node']//following::span[2]");
    private static final By glLine1By = By.xpath("//*[contains(text(),'Coding')]/following::*[contains(@id,'1,1#if-r')]/child::input");
    private static final By glLine2By = By.xpath("//*[contains(text(),'Coding')]/following::*[contains(@id,'2,1#if-r')]/child::input");
    private static final By glLine3By = By.xpath("//*[contains(text(),'Coding')]/following::*[contains(@id,'3,1#if-r')]/child::input");
    private static final By glLine4By = By.xpath("//*[contains(text(),'Coding')]/following::*[contains(@id,'4,1#if-r')]/child::input");
    private static final By taxColumnBy = By.xpath("//*[contains(@id,',9#if-r')]/child::*");

    private static final String WPUFIB = "WPUFIB";
    private static final String STR_FB03 = "STR FB03";
    private static final String LAYAWAY_FB03 = "Layaway fb03";
    private static final String IDOC_EXPAND = "Idoc Expand";
    private final String PARTIAL_TEXT_XPATH = "//div[contains(text(),'";
    private static final String ACCOUNT_FIELD_FOR_STR = "STR Account";
    private static final String ACCOUNT_FIELD_FOR_SALES_TAX = "Sales Tax Account";
    private static final String ACCOUNT_FIELD_FOR_ENVIRONMENT_FEE = "Environment Fee Account";
    private static final String ACCOUNT_FIELD_FOR_SALES_REVENUE = "Sales Revenue Account";
    private static final String MOVEMENT_TYPE = "Movement Type";
    private static final String INBOUND_PROCESSING_IN_WPER = "Inbound processing";
    private static final String DOCUMENTS_PROCESSED_IN_WPER = "Documents Processed";
    private static final String AZF_STORE = "AZF 01";
    private static final String WDAZ_STORE = "WDAZ";
    private static final String WDTX_STORE = "WDTX";
    private static final String SALES_DATA_NOT_AGGREGATED = "Sales data not aggregated";
    private static final String IDOC_SUBSET = "Idoc Subset";
    private static final String EXPAND_SUB_TREE = "Expand Sub Tree";
    private static final String ARTICLE_DOCUMENT_SUBSET = "Article";
    private static final String ARTICLE_DOCUMENT_AGGR_WPUUMS = "AGGR Article WPUUMS";
    private static final String ARTICLE_DOCUMENT_AGGR_WPUTAB = "AGGR Article WPUTAB";
    private static final String BILLING_DOCCUMENT_SUBSET = "Billing";
    private static final String BILLING_DOCUMENT_AGGR_WPUUMS = "AGGR Billing WPUUMS";
    private static final String BILLING_DOCUMENT_AGGR_WPUTAB = "AGGR Billing WPUTAB";
    private static final By expandImageBy = By.xpath("//img[@class='lsAbapList--image']");
    public final String IDOC_STATUS_TEXT = "Green Light: IDoc Processed Successfully";
    private static final String IDOC_INPUT_FIELD = "idoc input field";
    private static final String IDOC_MESSAGE_TYPE = "basic type";
    private static final String VEHICLE_MAKE = "vehicle make";
    private static final String VEHICLE_MODEL = "vehicle model";
    private static final String VEHICLE_YEAR = "vehicle year";
    private static final String ITEM_QUANTITY = "item quantity";
    private static final String Z1CUST_NOTIFY_HDR_BUTTON = "expand Z1CUST NOTIFY header button";
    private static final String Z1CUST_NOTIFY_ITM_BUTTON = "expand Z1CUST NOTIFY item button";
    private static final String Z1CUST_NOTIFY_HDR_LINK = "Z1CUST NOTIFY header link";
    private static final String Z1CUST_NOTIFY_ITM_LINK = "Z1CUST NOTIFY item link";
    private static final String FIRST_NAME = "First name";
    private static final String LAST_NAME = "Last name";
    private static final String COMMUNICATION_TYPE = "Communication type";
    private static final String ORDER_NUMBER = "order number";
    private static final String IDOC_PREFIX = "AUTO";
    private static final String CONTINUE_ENTER = "Continue (Enter)";
    private static final String IDOC_EXTRACT = "IDOC";
    private static final String IDOC_INPUT_FIELD_INFO_CENTER = "IDOC input field info center";
    private static final String CHECK_OK = "Check Ok";
    private static final String EXECUTE = "Execute";
    private static final String RECORD_ID = "Record Id";
    private static final String SAP_DOCUMENT_HEADER = "SAP Document Header";
    private static final String INVOICE_NUMBER = "Invoice Number";
    private static final String TEXT_XPATH = "//*[text()='";
    private static final String IDOC_GENERATOR = "Idoc Generator";
    private static final String STATUS = "Status 100";
    private static final String EXISTING_IDOC = "Existing Idoc";
    private static final String COST_CENTER = "Cost Center";
    private static final String VENDOR_NUMBER = "Vendor Number";
    private static final String REFERENCE_DOC_NO = "Ref Doc No";
    private static final String DOC_DATE = "Doc Date";
    private static final String PSTNG_DATE = "Posting Date";
    private static final String VENDOR_INPUT_POPUP = "Vendor number input pop up";
    private static final String EDI_VENDOR_POPUP = "Edi Vendor Pop up";
    private static final String EDI_VENDOR_INPUT = "Edi Partner number";
    private static final String PROGRAM = "Program";
    private static final String BACKGROUND_EXECUTION_RESULT_PAGE = "Background execution result page";
    private static final String BACKGOUND_EXECUTION_FIRST_PAGE = "Background execution first page";
    private static final String LINE_ITEM_IN_DOL_PAGE = "Line Item";
    private static final String COMPANY_CODE_INPUT_POSTING = "Company Code";
    private static final String PRE_APPROVAL_CHECK_BOX = "Pre-Approval Check box";
    private static final String GL = "GL";
    private static final String GL_2 = "GL line 2";
    private static final String GL_3 = "GL line 3";
    private static final String GL_4 = "GL line 4";
    private static final String CLOSE_POPUP = "Close popup";
    private static final String DATE_IN_DOL_AP2N = "Date in Dol/Ap2n";
    private static final String PO_ITEM_1_IN_PTS_HEADER = "PO Item 1 In PTS Header";
    private static final String PO_ITEM_2_IN_PTS_HEADER = "PO Item 2 In PTS Header";
    private static final String GDS_INV_RCVD = "Gds/Inv Rcvd";
    private static final String TAX_AMOUNT = "Tax amount";
    private static final String TAX_CODE = "Tax code";
    private static final String ARTICLE_DATA = "Article Data";
    private static final String VENDOR_ARTICLE_NUMBER = "Vendor Article Number";
    private static final String ITEM_DROPDOWN = "Item Dropdown";
    private static final String VENDOR_FB60 = "Vendor";
    private static final String INVOICE_DATE = "Invoice Date";
    private static final String AMOUNT_FB60 = "Amount FB60";
    private static final String TEXT_FB60 = "Text FB60";
    private static final String REFERENCE_FB60 = "Reference FB60";
    private static final String GL_ACCONT_FB60 = "G/L account FB60";
    private static final String COST_CENTER_FB60 = "Cost center FB60";
    private static final String AMOUNT_IN_DOC = "Amount in doc";
    private static final String STATUS_LINE_CHECK_1 = "Status line check 1";
    private static final String SAVE_FB60 = "Save FB60";
    private static final String DOC_NUMBER = "Doc number";
    private static final String DOCUMENT_NUMBER_FB60 = "Document number FB60";
    private static final String VENDOR_NUMBER_FB60 = "Vendor number FB60";
    private static final String GL_NUMBER = "G/L number";
    private static final String DISPLAY_MESSAGE = "Display message";
    private static final String CUSTOMER = "Customer";
    private static final String DOCUMENT_VALUE = "Document number FB03";
    private static final String DOCUMENT_NUMBER_INPUT_FB03 = "Document number input FB03";
    private static final String FISCAL_YEAR_INPUT_Fb03 = "Fiscal year";
    private static final String ACCOUNT_NUMBER_TAB = "The account number";
    private static final String COST_CENTER_TAB = "The cost center number";
    private static final String PROFIT_CTR_TAB = "The profit ctr";
    private static final String ACCOUNT_NUMBER_TAB_2 = "The account number 2";
    private static final String COST_CENTER_TAB_2 = "The cost center number 2";
    private static final String PROFIT_CTR_TAB_2 = "The profit ctr 2";
    private static final String ARTICLE = "Article";
    private static final String Billing = "Billing";
    private static final String WPUTAB = "WPUTAB";
    private static final String WPUUMS = "WPUUMS";
    private static final String GET_VARIANT = "Get Variant";
    private static final String CREATION_TIME = "Creation Time";
    private static final String BILLING_DOCUMENT_WPUTAB = "Billing document WPUTAB";
    private static final String VIEWS = "Views";
    private final int MIN = 1;
    private final int MAX = 90000;
    public static int rowCount = 0;
    public static final String IDOC_NUMBER_FROM_EXCEL = "invoice";
    public static final String IDOC_VALUE_1 = "WPUUMS IDOC";
    public static final String IDOC_VALUE_2 = "WPUTAB IDOC";
    private static final String QA_IDOC_PROC = "QA INBOUND";
    private static final String EXPAND_NODE = "Expand Node";
    private static final String LVORM = "LVORM";
    private static final String MART_ROW = "First Mart Row";
    private static final String SCROLL_IN_IDOC_PAGE = "Scroll In Idoc Page";
    private static final String IDOC_FOR_VEHICLE_OUTBOUND = "Idoc For Vehicle Outbound";
    private static final String IDOC_LIST = "Idoc List";
    private static final String IFRAME_IN_SE93 = "Iframe in SE93";
    private static final String FIRST_LINE_ENTRY_UNDER_ZFC_APINVS = "First line item in Zfc_Apinvs";
    private static final String EDIDC_LINE_ITEM = "Edidc line Item";
    private static final String ACCOUNT_NR_WPER = "Account Nr in Wper";
    private static final String BOPIS_DEPOSIT_VALUE = "Bopis Deposit";
    private static final String BOPIS_ASSIGNMENT_VALUE = "Bopis Assignment";
    private static final String ASSIGNMENT_NR_WPER = "Assignment Nr in Wper";
    private static final String GL_SITE_NR_GB03 = "Site Number FB03";
    private static final String GL_PROFIT_NR_GB03 = "Profit Number FB03";
    private static final String CREATE_BUTTON_IN_WE19 = "Create button in we19";
    private static final String SHORT_TEXT = "Short Text input in row 2";
    private static final String NET_PRICE = "Net price input in row 2";
    private static final String ORDER_PER_UNIT = "Order price unit input in row 2";
    private static final String MERCH_CATEGORY = "Merch Category input in row 2";
    private static final String ORDER_UNIT = "Order unit input in row 2";
    private static final String DOCUMENT_DISPLAY_MESSAGES = "Document lines:: Display messages";
    private static final String MAXIMUM_NUMBER_OF_EXTERNAL_SESSIONS_REACHED = "Maximum number of external sessions reached";
    private static final String PTS_INVOICE_DETAIL_AND_POSTING_SCREEN = "PTS Invoice Detail and Posting Screen";
    private static final String INFORMATION = "Information";
    private static final String ATTEMPT_POSTING = "Attempt Posting";
    private static final String PTS = "pts";
    private static final String WARNING = "warning";
    private static final String DOLPHIN_HEADER = "Dolphin Process Tracking System (PTS)-AP - InfoCenter";
    private static final String SUCCESFULLY_POSTED = "Succesfully Posted";
    private static final String RESUBMITTED = "Resubmitted";
    private static final String UPDATE_PTS_RECORD = "Update PTS Record";
    private static final String POSTING_MESSAGE = "Posting Message: Display messages";
    private static final String SAVE = "Save";
    private static final String RESUBMIT = "Resubmit";
    private static final String FINAL_OUTPUT_PTS_FOLDER_NAME = "finalOutPutForEachPts";
    private static final String DOLPHIN_PTS_SUBSTATUS = "Dolphin PTS - SubStatus Selection";
    private static final String SUCCESS = "Success";
    private static final String SEL2BGCOLOR = "Sel2BgColor";
    private static final String CREATED_BY = "Created By";
    private static final String VARIANT = "Variant";
    private static final String GET_VARIANT_TEXT = "Get Variant... (Shift+F5)";
    private static int count = 1;
    private static boolean isPosted = false;
    private static boolean isScreenShotFlagPosted = false;
    private static boolean expectMaximumSessionsPopUpVisible = true;

    /**
     * Method to return By Element
     *
     * @param elementName - Name of the element
     * @return - By element
     */
    public By returnByElement(String elementName) {
        LOGGER.info("returnByElement started");
        switch (elementName) {
            case GL:
                return glLine1By;
            case GL_2:
                return glLine2By;
            case GL_3:
                return glLine3By;
            case GL_4:
                return glLine4By;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case IDOC_INPUT_FIELD:
                return iDocNumberInputField;
            case IDOC_MESSAGE_TYPE:
                return iDocBasicType;
            case Constants.EXECUTE:
                return execute;
            case Constants.CLOSE:
                return close;
            case VEHICLE_MAKE:
                return vehicleMake;
            case VEHICLE_MODEL:
                return vehicleModel;
            case VEHICLE_YEAR:
                return vehicleYear;
            case ITEM_QUANTITY:
                return itemQuantity;
            case Z1CUST_NOTIFY_HDR_BUTTON:
                return customerNotifyHeaderButton;
            case Z1CUST_NOTIFY_ITM_BUTTON:
                return customerNotifyItemButton;
            case Z1CUST_NOTIFY_HDR_LINK:
                return customerNotifyHeaderLink;
            case Z1CUST_NOTIFY_ITM_LINK:
                return customerNotifyItemLink;
            case COMMUNICATION_TYPE:
                return communicationTypeHDR;
            case ORDER_NUMBER:
                return orderNumberHDR;
            case FIRST_NAME:
                return customerFirstName;
            case LAST_NAME:
                return customerLastName;
            case Constants.ADDRESS:
                return customerAddress;
            case Constants.CITY:
                return customerCity;
            case CONTINUE_ENTER:
                return continueEnter;
            case CHECK_OK:
                return checkOk;
            case RECORD_ID:
                return recordId;
            case SAP_DOCUMENT_HEADER:
                return sapDocumentHeader;
            case INVOICE_NUMBER:
                return invoiceNumber;
            case IDOC_GENERATOR:
                return iDocGeneratoriframe;
            case STATUS:
                return iDocRecordStatus;
            case EXISTING_IDOC:
                return existingIDocNumber;
            case COST_CENTER:
                return costCenterInput;
            case VENDOR_NUMBER:
                return partnerID;
            case REFERENCE_DOC_NO:
                return refDocNo;
            case DOC_DATE:
                return docDate;
            case PSTNG_DATE:
                return postingDate;
            case VENDOR_INPUT_POPUP:
                return vendorNumberInputPopUp;
            case EDI_VENDOR_POPUP:
                return ediVendorPopUp;
            case EDI_VENDOR_INPUT:
                return ediPopUpVendorInputField;
            case PROGRAM:
                return programInputField;
            case BACKGROUND_EXECUTION_RESULT_PAGE:
                return backgroundExecutionResultPageTitle;
            case BACKGOUND_EXECUTION_FIRST_PAGE:
                return backgroundFirstPage;
            case LINE_ITEM_IN_DOL_PAGE:
                return selectRecordLineItemInDol;
            case COMPANY_CODE_INPUT_POSTING:
                return companyCodeInputInPosting;
            case PRE_APPROVAL_CHECK_BOX:
                return preApprovalCheckBox;
            case GL:
                return glEditBox;
            case CLOSE_POPUP:
                return closePopUp;
            case DATE_IN_DOL_AP2N:
                return dateInDolAp2n;
            case GL_2:
                return glEditBox2;
            case PO_ITEM_1_IN_PTS_HEADER:
                return poItem1InPTSHeader;
            case PO_ITEM_2_IN_PTS_HEADER:
                return poItem2InPTSHeader;
            case GDS_INV_RCVD:
                if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    return gdsInvRcvdStg;
                } else {
                    return gdsInvRcvdQa;
                }
            case TAX_AMOUNT:
                return taxAmount;
            case TAX_CODE:
                return taxCode;
            case GL_3:
                return glEditBox3;
            case GL_4:
                return glEditBox4;
            case DISPLAY_MESSAGE:
                return displayMessage;
            case ARTICLE_DATA:
                return articleData;
            case VENDOR_ARTICLE_NUMBER:
                return vendorArticleNumber;
            case ITEM_DROPDOWN:
                return itemDropDown;
            case INBOUND_PROCESSING_IN_WPER:
                return inboundProcessingInWper;
            case DOCUMENTS_PROCESSED_IN_WPER:
                return documentsProcessedInWper;
            case AZF_STORE:
                return azf01Store;
            case WDAZ_STORE:
                return wdaz0Store;
            case WDTX_STORE:
                return wdtx0Store;
            case SALES_DATA_NOT_AGGREGATED:
                return salesDataNotAggregated;
            case ARTICLE_DOCUMENT_SUBSET:
                return webDriver.findElement(returnTheByLocatorForBillingAndArticleForASpecificidoc(driver.scenarioData.getData(IDOC_NUMBER_FROM_EXCEL), ARTICLE));
            case ARTICLE_DOCUMENT_AGGR_WPUUMS:
                return webDriver.findElement(returnTheByLocatorForBillingAndArticleForASpecificidoc(driver.scenarioData.getData(PosDm.WPUUMS_ROW), ARTICLE));
            case ARTICLE_DOCUMENT_AGGR_WPUTAB:
                return webDriver.findElement(returnTheByLocatorForBillingAndArticleForASpecificidoc(driver.scenarioData.getData(PosDm.WPUTAB_ROW), ARTICLE));
            case BILLING_DOCCUMENT_SUBSET:
                return webDriver.findElement(returnTheByLocatorForBillingAndArticleForASpecificidoc(driver.scenarioData.getData(IDOC_NUMBER_FROM_EXCEL), Billing));
            case BILLING_DOCUMENT_AGGR_WPUUMS:
                return webDriver.findElement(returnTheByLocatorForBillingAndArticleForASpecificidoc(driver.scenarioData.getData(PosDm.WPUUMS_ROW), Billing));
            case BILLING_DOCUMENT_AGGR_WPUTAB:
                return webDriver.findElement(returnTheByLocatorForBillingAndArticleForASpecificidoc(driver.scenarioData.getData(PosDm.WPUTAB_ROW), Billing));
            case IDOC_VALUE_2:
                return webDriver.findElement(returnTheByLocatorForBillingAndArticleForASpecificidoc(driver.scenarioData.getData(PosDm.WPUTAB_ROW), IDOC_EXPAND));
            case IDOC_VALUE_1:
                return webDriver.findElement(returnTheByLocatorForBillingAndArticleForASpecificidoc(driver.scenarioData.getData(PosDm.WPUUMS_ROW), IDOC_EXPAND));
            case EXPAND_SUB_TREE:
                return expanddSubTreeButton;
            case VENDOR_FB60:
                return vendorInFb60;
            case AMOUNT_FB60:
                return amountFb60;
            case INVOICE_DATE:
                return invoiceDateFb60;
            case TEXT_FB60:
                return textFb60;
            case REFERENCE_FB60:
                return referenceFb60;
            case GL_ACCONT_FB60:
                return glAccountFb60;
            case COST_CENTER_FB60:
                return costCenterFb60;
            case AMOUNT_IN_DOC:
                return amountInDocFb60;
            case STATUS_LINE_CHECK_1:
                return statusLineCheckFb60;
            case SAVE_FB60:
                return saveFb60;
            case DOC_NUMBER:
                return docNumber;
            case DOCUMENT_NUMBER_FB60:
                return verifyDocumentNumber;
            case VENDOR_NUMBER_FB60:
                return verifyVendorFb60;
            case GL_NUMBER:
                return verifyGlFb60;
            case CUSTOMER:
                return firstIframe;
            case DOCUMENT_VALUE:
                return getDocumentValue;
            case DOCUMENT_NUMBER_INPUT_FB03:
                return documentNumberInputFb03;
            case FISCAL_YEAR_INPUT_Fb03:
                return fiscalYearInputFb03;
            case ACCOUNT_NUMBER_TAB:
                return accountNrTab;
            case COST_CENTER_TAB:
                return costCenterNrTab;
            case PROFIT_CTR_TAB:
                return profitCtrTab;
            case ACCOUNT_NUMBER_TAB_2:
                return accountNrTab2;
            case COST_CENTER_TAB_2:
                return costCenterNrTab2;
            case PROFIT_CTR_TAB_2:
                return profitCtrTab2;
            case ACCOUNT_FIELD_FOR_STR:
                return accountFieldForStr;
            case ACCOUNT_FIELD_FOR_SALES_TAX:
                return salesTaxAccountField;
            case ACCOUNT_FIELD_FOR_SALES_REVENUE:
                return salesRevAccountField;
            case ACCOUNT_FIELD_FOR_ENVIRONMENT_FEE:
                return environmentalFeesAccountField;
            case MOVEMENT_TYPE:
                return movementType;
            case WPUTAB:
                return wputab;
            case WPUUMS:
                return wpuums;
            case GET_VARIANT:
                return getVariant;
            case QA_IDOC_PROC:
                return qaidocProc;
            case CREATION_TIME:
                return creationTime;
            case BILLING_DOCUMENT_WPUTAB:
                return billingDocument;
            case VIEWS:
                return views;
            case LAYAWAY_FB03:
                return layawayAccountInFb03;
            case STR_FB03:
                return strDepositClearingInFb03;
            case WPUFIB:
                return wpufib;
            case IDOC_EXTRACT:
                return idocNumberOutput;
            case IDOC_INPUT_FIELD_INFO_CENTER:
                return idocInputFieldInfoCenter;
            case EXPAND_NODE:
                return webDriver.findElements(expandNodeBy).get(0);
            case LVORM:
                return lVormText;
            case MART_ROW:
                return martRow;
            case SCROLL_IN_IDOC_PAGE:
                return scrollInIdocPage;
            case IDOC_FOR_VEHICLE_OUTBOUND:
                return idocForVehicleOutbound;
            case IDOC_LIST:
                return idocList;
            case IFRAME_IN_SE93:
                return firstIframe;
            case FIRST_LINE_ENTRY_UNDER_ZFC_APINVS:
                return webDriver.findElements(firstLineUnderZfcApInvsBy).get(0);
            case EDIDC_LINE_ITEM:
                return edidcLineItem;
            case ACCOUNT_NR_WPER:
                return accountNrWper;
            case BOPIS_DEPOSIT_VALUE:
                return bopisDepositValue;
            case BOPIS_ASSIGNMENT_VALUE:
                return bopisAssignmentValue;
            case ASSIGNMENT_NR_WPER:
                return assignmentNrWper;
            case GL_SITE_NR_GB03:
                return glSiteNrfb03;
            case GL_PROFIT_NR_GB03:
                return glProfitNrfb03;
            case CREATE_BUTTON_IN_WE19:
                return createButtonInWe19;
            case SHORT_TEXT:
                return shortTextInputInRow2;
            case NET_PRICE:
                return netPriceInputInRow2;
            case ORDER_PER_UNIT:
                return orderPerUnitInputInRow2;
            case MERCH_CATEGORY:
                return merchCategoryInputInRow2;
            case ORDER_UNIT:
                return orderUnitInputInRow2;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * This method is to input iDoc num into input field.
     */
    public void inputIniDocPage() {
        LOGGER.info("inputIniDocPage started");
        driver.clearInputAndSendKeys(iDocNumberInputField, driver.scenarioData.getCurrentIDocNumber());
        LOGGER.info("inputIniDocPage completed");
    }

    /**
     * This Method is click any element in iDoc page.
     *
     * @param elementName - send in elementName and gets the WebElement from returnElement method.
     */
    public void clickIniDocPage(String elementName) {
        LOGGER.info("clickIniDocPage started");
        commonActions.click(returnElement(elementName));
        LOGGER.info("clickIniDocPage completed");
    }


    /**
     * verifies the Idoc status if it is Green.
     */
    public void verifyIdocStatusSuccessful() {
        LOGGER.info("verifyIdocStatusSuccessful started");
        driver.assertElementAttributeString(greenLight, Constants.TITLE, IDOC_STATUS_TEXT);
        LOGGER.info("verifyIdocStatusSuccessful completed");
    }

    /**
     * verifies the Vehicle information.
     *
     * @param vehicleField         Vehicle field who data has to be validated
     * @param vehicleExpectedValue Expected vehicle. Value gets from step definition
     */
    public void verifyVehicleDetails(String vehicleField, String vehicleExpectedValue) {
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        switch (vehicleField) {
            case VEHICLE_MAKE:
                driver.assertElementAttributeString(vehicleMake, Constants.VALUE, vehicleExpectedValue);
                break;
            case VEHICLE_MODEL:
                driver.assertElementAttributeString(vehicleModel, Constants.VALUE, vehicleExpectedValue);
                break;
            case VEHICLE_YEAR:
                driver.assertElementAttributeString(vehicleYear, Constants.VALUE, vehicleExpectedValue);
                break;
            case ITEM_QUANTITY:
                driver.assertElementAttributeString(itemQuantity, Constants.VALUE, vehicleExpectedValue);
                break;
            default:
                Assert.fail("FAIL: Could not find the Key that matched string parameter passed from step definition");
        }
    }

    /**
     * verifies the customer informaiton.
     *
     * @param customerType  Type of customer who data has to get retrieved from customer class
     * @param customerField Customer filed (First name, last name .. etc)
     */
    public void verifyCustomerData(String customerType, String customerField) {
        Customer apptCust = customer.getCustomer(customerType);
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        switch (customerField) {
            case FIRST_NAME:
                driver.assertElementAttributeString(customerFirstName, Constants.VALUE, apptCust.firstName);
                break;
            case LAST_NAME:
                driver.assertElementAttributeString(customerLastName, Constants.VALUE, apptCust.lastName);
                break;
            case Constants.ADDRESS:
                driver.assertElementAttributeString(customerAddress, Constants.VALUE, apptCust.address1);
                break;
            case Constants.CITY:
                driver.assertElementAttributeString(customerCity, Constants.VALUE, apptCust.city);
                break;
            case COMMUNICATION_TYPE:
                driver.assertElementAttributeString(communicationTypeHDR, Constants.VALUE, Constants.EMAIL);
                break;
            case ORDER_NUMBER:
                driver.assertElementAttributeString(orderNumberHDR, Constants.VALUE, driver.scenarioData.getCurrentOrderNumber());
                break;
            default:
                Assert.fail("FAIL: Could not find the Key that matched string parameter passed from step definition");
        }
    }

    /**
     * Enters an existing idoc number to copy from based on the environment
     */
    public void enterTestIdocFromExcel() throws Throwable {
        LOGGER.info("enterTestIdocFromExcel started");
        driver.waitForPageToLoad();
        driver.clearInputAndSendKeys(existingIDocNumber, commonActions.returnDataFromExcel(Config.getDataSet())
                .replace(".", ""));
        LOGGER.info("enterTestIdocFromExcel completed");
    }

    /**
     * Enters an existing idoc number to copy from based on the environment
     *
     * @param labelName name of the label by the input
     */
    public void enterExistingIdocInLabel(String labelName) {
        LOGGER.info("enterExistingIdocInLabel started");
        driver.waitForPageToLoad();
        commonActions.sendKeysWithLabelName(labelName, Config.getSapIdoc());
        LOGGER.info("enterExistingIdocInLabel completed");
    }

    /**
     * Update Values for the respective IdocType
     *
     * @param : type  type of idoc to be updated
     */
    public void updateIdocValues(String type, String vendor) {
        LOGGER.info("updateIdocValues started");
        String iDoc = IDOC_PREFIX + CommonUtils.getRandomNumber(MIN, MAX);
        creditEditField.click();
        commonActions.switchToRespectiveiFrame();
        driver.waitForMilliseconds(100);
        commonActions.sendKeysTodaysDateWithLabelName(CommonActions.DATE);
        driver.performKeyAction(Keys.ENTER);
        commonActions.switchToMainWindow();
        commonActions.click(invoiceIDocNumber);
        commonActions.switchToRespectiveiFrame();
        commonActions.sendKeysWithLabelName("Document", iDoc);
        driver.performKeyAction(Keys.ENTER);
        commonActions.switchToMainWindow();
        commonActions.click(webDriver.findElements(expandFolderE1edp01InWe19).get(0));
        driver.waitForMilliseconds(Constants.ONE_THOUSAND);
        commonActions.click(webDriver.findElement(purchaseOrderBlocksInWe19));
        commonActions.switchToRespectiveiFrame();
        commonActions.sendKeysWithLabelName("Document", driver.scenarioData.getCurrentPurchaseOrderNumber());
        driver.performKeyAction(Keys.ENTER);
        commonActions.switchToMainWindow();
        commonActions.click(webDriver.findElements(expandFolderE1edp01InWe19).get(1));
        driver.waitForMilliseconds(Constants.ONE_THOUSAND);
        commonActions.click(webDriver.findElements(purchaseOrderBlocksInWe19).get(1));
        commonActions.switchToRespectiveiFrame();
        commonActions.sendKeysWithLabelName("Document", driver.scenarioData.getCurrentPurchaseOrderNumber());
        driver.performKeyAction(Keys.ENTER);
        commonActions.switchToMainWindow();

        if (type.equalsIgnoreCase("1 Article")) {
            //TODO - Update one Articles
        } else if (type.equalsIgnoreCase("2 Articles")) {
            //TODO - Update two Articles
        } else {

        }
        commonActions.click(standardInbound);
        commonActions.switchToRespectiveiFrame();
        driver.performKeyAction(Keys.ENTER);
        LOGGER.info("updateIdocValues completed");
    }

    /**
     * Save the idoc number from iDoc Page
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void saveIdocNumberInIdocPage(String elementName) {
        LOGGER.info("saveIdocNumbersInIdocPage started");
        commonActions.savePurchaseOrderNumber(returnElement(elementName));
        String iDoc = "";

        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            iDoc = driver.scenarioData.getCurrentPurchaseOrderNumber().substring(7, 16);
        } else if (Config.getDataSet().equalsIgnoreCase(Constants.DEV)) {
            iDoc = driver.scenarioData.getCurrentPurchaseOrderNumber().substring(9, 16);
        } else {
            iDoc = driver.scenarioData.getCurrentPurchaseOrderNumber().substring(8, 16);
        }
        driver.scenarioData.setCurrentIDocNumber(iDoc);
        LOGGER.info("saveIdocNumbersInIdocPage completed");
    }

    /**
     * Enters the saved return to idoc number.
     *
     * @param element - send in elementName and gets the WebElement from returnElement method.
     */
    public void getAndEnterIdocNumberInIdoc(String element) {
        LOGGER.info("getAndEnterIdocNumberInIdoc started");
        driver.clearInputAndSendKeys(returnElement(element), driver.scenarioData.getCurrentIDocNumber());
        LOGGER.info("getAndEnterIdocNumberInIdoc Completed");
    }

    /**
     * Verifies tax code text count IDOC
     *
     * @param taxCode- Tax code of the transaction.
     * @param count    - counts the number of I0 and I1.
     */
    public void verifyTaxCodeTextCountInIDOC(String taxCode, String count) {
        LOGGER.info("verifyTaxCodeTextCountInIDOC started");
        driver.waitForPageToLoad();
        Assert.assertEquals(Integer.parseInt(count), webDriver.findElements(By.xpath(TEXT_XPATH + taxCode + "']")).size());
        LOGGER.info("verifyTaxCodeTextCountInIDOC completed");
    }

    /**
     * switches to Iframe in NWBC page with Idoc page's WebElement
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void switchToIframeInIdoc(String elementName) {
        LOGGER.info("switchToIframeInIdoc started");
        commonActions.switchFrameContext(returnElement(elementName));
        LOGGER.info("switchToIframeInIdoc completed");
    }

    /**
     * This method will enter the today's date in to the element in the format YYYYMMDD
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void getAndEnterTodayDateInIdocPage(String elementName) {
        LOGGER.info("getAndEnterTodayDateInIdocPage started");
        String todayDate = driver.getTodayDate("yyyy/MM/dd").replaceAll("[^0-9]", "");
        driver.clearInputAndSendKeys(returnElement(elementName), todayDate);
        LOGGER.info("getAndEnterTodayDateInIdocPage completed");
    }

    /**
     * This method will enter the today's date in to the element in the format YYYYMMDD
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void getAndEnterTodayDateInFieldWithLabelName(String elementName) {
        LOGGER.info("getAndEnterTodayDateInFieldWithLabelName started");
        String todayDate = driver.getTodayDate("yyyy/MM/dd").replaceAll("[^0-9]", "");
        commonActions.sendKeysWithLabelName(elementName, todayDate);
        LOGGER.info("getAndEnterTodayDateInFieldWithLabelName completed");
    }

    /**
     * Verifies displayed value matches expected value in Idoc page
     *
     * @param elementName string to verify the value of
     * @param value       expected value of the field to compare to
     */
    public void assertValDisplayedMatchesValExpectedinIdocPage(String elementName, String value) {
        LOGGER.info("assertValDisplayedMatchesValExpectedinIdocPage started");
        driver.waitForPageToLoad();
        driver.assertElementAttributeString(returnElement(elementName), Constants.VALUE, value);
        LOGGER.info("assertValDisplayedMatchesValExpectedinIdocPage completed");
    }

    /**
     * Enters date into input field
     *
     * @param elementName :  String , Name of the input field.
     */
    public void enterTodaydateInFb60(String elementName) {
        LOGGER.info("getAndEnterTodayDateInFb60 started");
        String todayDate = driver.getTodayDate("MM/dd/yyyy");
        driver.clearInputAndSendKeys(returnElement(elementName), todayDate);
        LOGGER.info("getAndEnterTodayDateInFb60 completed");
    }

    /**
     * Saves document number in idoc page
     *
     * @param elementName :  String , Name of the input field.
     */
    public void saveDocNumber(String elementName) {
        LOGGER.info("saveDocNumber started");
        driver.waitForPageToLoad();
        String documentNumber = returnElement(elementName).getText().replaceAll("[^0-9]", "").substring(0,10);
        driver.scenarioData.setCurrentPurchaseOrderNumber(documentNumber);
        if (documentNumber.isEmpty()) {
            Assert.fail("FAIL: Document Number is Empty");
        }
        LOGGER.info("saveDocNumber completed");
    }

    /**
     * Verifies displayed value matches saved document number
     *
     * @param elementName string to verify the value of
     */
    public void assertsSavedDocumentNumber(String elementName) {
        LOGGER.info("assertsSavedDocumentNumber started");
        driver.waitForPageToLoad();
        driver.assertElementAttributeString(returnElement(elementName), Constants.VALUE, driver.scenarioData.getCurrentPurchaseOrderNumber());
        LOGGER.info("assertsSavedDocumentNumber completed");
    }

    /**
     * Verifies Message is displayed on Idoc page
     *
     * @param elementName string to verify the value of
     * @param message     Message to check on vehicle page
     */
    public void assertTextDisplayedInIdoc(String elementName, String message) {
        LOGGER.info("assertTextDisplayedInIdoc started");
        driver.verifyTextDisplayed(returnElement(elementName), message);
        LOGGER.info("assertTextDisplayedInIdoc completed");
    }

    /**
     * Saves the document number from Idoc page
     *
     * @param elementName
     */

    public void saveArticleDocumentNumbersInIDocPage(String elementName) {
        LOGGER.info("savePurchaseOrderNumbersInIDocCPage started");
        commonActions.saveArticleDocumentNumberFromValueAttribute(returnElement(elementName));
        LOGGER.info("savePurchaseOrderNumbersInIDocPage completed");
    }

    /**
     * Gets the document number from the above method on Idoc page
     *
     * @param elementName
     */

    public void getAndEnterArticleDocumentNumberInIdocPage(String elementName) {
        LOGGER.info("getAndEnterArticleDocumentNumberInIDocPage started");
        commonActions.getArticleDocumentNumberFromValueAttribute(returnElement(elementName));
        LOGGER.info("getAndEnterArticleDocumentNumberInIDocPage completed");
    }

    /**
     * Method used only for returning the web elements to locate Article Document and Billing Document of a specific
     * Idoc when idoc number is passed to the method.
     *
     * @param idocNumber     - input Idoc number
     * @param typeOfDocument - Type of document Only two values(Article or billing)
     * @return - By locator for Either Article Document or billing document.
     */
    private By returnTheByLocatorForBillingAndArticleForASpecificidoc(String idocNumber, String typeOfDocument) {
        LOGGER.info("returnTheByLocatorForBillingAndArticleForASpecificidoc started");
        if (typeOfDocument.equalsIgnoreCase(ARTICLE)) {
            By by = By.xpath(PARTIAL_TEXT_XPATH + idocNumber + "')]/following::div[contains(text(),'Article Document')]/preceding::div[contains(text(),'49')][1]");
            return by;
        } else if (typeOfDocument.equalsIgnoreCase(Billing)) {
            By by = By.xpath(PARTIAL_TEXT_XPATH + idocNumber + "')]/following::div[contains(text(),'Billing Document')]/preceding::div[contains(text(),'97')][1]");
            return by;
        } else if (typeOfDocument.equalsIgnoreCase(IDOC_EXPAND)) {
            By by = By.xpath(PARTIAL_TEXT_XPATH + idocNumber + "')]/preceding::div[contains(@lsdata,'2_4.png')][1]");
            return by;
        }
        LOGGER.info("returnTheByLocatorForBillingAndArticleForASpecificidoc completed");
        return null;
    }

    /**
     * Double click method for IDoc Page which will double click the element
     *
     * @param elementName - String , Name of the input field.
     */
    public void doubleClick(String elementName) {
        LOGGER.info("doubleClick started");
        driver.waitForPageToLoad();
        try {
            driver.doubleClick(returnElement(elementName));
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = returnElement(elementName);
            driver.doubleClick(staleElement);
        }
        LOGGER.info("doubleClick completed");
    }

    /**
     * This method will read from excel values and loads it into the hash map with scenario data
     *
     * @param filepath   - path to the excel file
     * @param sheetname  - sheet name inside excel file
     * @param rowNumberm - Row number
     * @param cellCount  -  cell number
     * @param key        - Desired key to save the value
     * @throws IOException
     */
    public void readFromExcelAndReturnSingleValue(String filepath, String sheetname, int rowNumberm, int cellCount, String key) throws IOException {
        LOGGER.info("readFromExcelAndReturnSingleValue started");
        String value = driver.readFromExcelAndReturnSingleValue(filepath, sheetname, rowNumberm, cellCount);
        if (key.equalsIgnoreCase("date")) {
            try {
                Date javaDate = DateUtil.getJavaDate(Double.valueOf(value));
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
                value = simpleDateFormat.format(javaDate);
            } catch (NumberFormatException ne) {
                ne.printStackTrace();
            }
        }
        driver.scenarioData.setData(key, value);
        LOGGER.info("readFromExcelAndReturnSingleValue completed");
    }

    /**
     * This method will click on the idoc number which is the invocie number stored in the output excel sheet from legacy
     */
    public void clickOnIdocExtractedFromExcel() {
        LOGGER.info("clickOnIdocExtractedFromExcel started");
        commonActions.clickOnPartialText(driver.scenarioData.getData(IDOC_NUMBER_FROM_EXCEL));
        LOGGER.info("clickOnIdocExtractedFromExcel completed");
    }

    /**
     * This method will read the article numbers and net amount values from excel sheet and stores into
     * driver.scenarioData.setData().
     *
     * @param filepath  - excel file path
     * @param sheetname - excel sheet name
     * @throws IOException
     */
    public void readAllInvoiceDetailsFromExcel(String filepath, String sheetname) throws IOException {
        LOGGER.info("readAllInvoiceDetailsFromExcel started");
        File file = new File(filepath);
        FileInputStream inputStream = new FileInputStream(file);
        Sheet sheet;
        if (filepath.toLowerCase().contains(Constants.XLSX.toLowerCase())) {
            XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xssfsheet = xssfworkbook.getSheet(sheetname);
            sheet = xssfsheet;
        } else {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(inputStream);
            HSSFSheet hssfsheet = hssfworkbook.getSheet(sheetname);
            sheet = hssfsheet;
        }
        rowCount = sheet.getLastRowNum();
        for (int i = 1; i <= rowCount; i++) {
            Row articleRows = sheet.getRow(i);
            try {
                Double cellValue = articleRows.getCell(0).getNumericCellValue();
                if (cellValue != null) {
                    driver.scenarioData.setData(CommonActions.ARTICLE_FROM_LINE + i, cellValue.toString().substring(0, cellValue.toString().length() - 2));
                }
            } catch (IllegalStateException ise) {
                String cellValue = articleRows.getCell(0).getStringCellValue();
                if (cellValue != null) {
                    driver.scenarioData.setData(CommonActions.ARTICLE_FROM_LINE + i, cellValue);
                }
            }

            try {
                Double cellValue = articleRows.getCell(2).getNumericCellValue();
                if (cellValue != null) {
                    driver.scenarioData.setData(CommonActions.AMOUNT_FROM_LINE + i, cellValue.toString());
                }
            } catch (IllegalStateException ise) {
                String cellValue = articleRows.getCell(2).getStringCellValue();
                if (cellValue != null) {
                    driver.scenarioData.setData(CommonActions.AMOUNT_FROM_LINE + i, cellValue);
                }
            }
        }
        LOGGER.info("readAllInvoiceDetailsFromExcel completed");
    }

    /**
     * Verifies all the articles present in WPER's billing document
     */
    public void verifyBillingDocumentNetAmountWithArticles() {
        LOGGER.info("verifyBillingDocumentNetAmountWithArticles started");
        List<WebElement> articles = webDriver.findElements(articleValuesInTableBy);
        for (int i = 0; i < rowCount; i++) {
            int hashMapKeyIncrement = i + 1;
            Assert.assertTrue("FAIL: Article should be: " + driver.scenarioData.getData(
                    CommonActions.ARTICLE_FROM_LINE + hashMapKeyIncrement) + " But Article displayed is: " +
                    articles.get(i).getAttribute(Constants.VALUE), articles.get(i).getAttribute(Constants.VALUE).toLowerCase().
                    contains(driver.scenarioData.getData(CommonActions.ARTICLE_FROM_LINE + hashMapKeyIncrement)));
        }
        LOGGER.info("verifyBillingDocumentNetAmountWithArticles completed");
    }

    /**
     * This method will valdiate the aggregated idoc's values with invoice created on legacy
     */
    public void verifyBillingDocumentNetAmountWithArticlesAggregate() {
        LOGGER.info("verifyBillingDocumentNetAmountWithArticlesAggregate started");
        String attribute;
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            attribute = "@value";
        } else {
            attribute = "text()";
        }
        for (int i = 0; i < rowCount; i++) {
            int hashMapKeyIncrement = i + 1;
            WebElement article = webDriver.findElement(By.xpath("//*[contains(" + attribute + ",'" + driver.scenarioData.getData(
                    CommonActions.ARTICLE_FROM_LINE + hashMapKeyIncrement) + "')]"));
            Assert.assertTrue("FAIL: Article Not Found: " + driver.scenarioData.getData(
                    CommonActions.ARTICLE_FROM_LINE + hashMapKeyIncrement), driver.isElementDisplayed(article));
            LOGGER.info("verifyBillingDocumentNetAmountWithArticlesAggregate completed");
        }
    }

    /**
     * This method will click the Idoc in WPER in WPUUMS and WPUTAB which is Inbound and stored in EDIDS table
     * <p>
     * This method needs to call clickOnPartial Text twice because of behavior of the application.
     *
     * @param key - Hash map Key to access the idoc number saved in driver.scenariodata.getData() hash map from EDIDS table
     */
    public void clickOnIdocExtractedFromEDIDS(String key) {
        LOGGER.info("clickOnIdocExtractedFromExcel started");
        commonActions.clickOnPartialText(driver.scenarioData.getData(key));
        commonActions.clickOnPartialText(driver.scenarioData.getData(key));
        LOGGER.info("clickOnIdocExtractedFromExcel completed");
    }

    /**
     * This method will validate Total amount of invoice to SAP Billing document. Checks both negative and positive amounts.
     *
     * @throws Exception
     */
    public void verifyTotalAmountInBillingDocumentInWper() throws Exception {
        LOGGER.info("verifyTotalAmountInBillingDocumentInWper started");
        readFromExcelAndReturnSingleValue(Constants.fullPathFileNames.get(Constants.INVOICE_SAP), commonActions.
                returnDataFromEnvironmentVariables(commonActions.SHEET_NAME), rowCount + 1, 6, "Total Amount");
        if (driver.scenarioData.getData("Total Amount").contains("-")) {

            Assert.assertTrue("Fail: WebElement text is: " + totalInvoiceAmount.getAttribute(Constants.VALUE).replace(",",
                    "") + " And validation text is: " + driver.scenarioData.getData
                    ("Total Amount").replace("-", ""), totalInvoiceAmount.getAttribute(Constants.VALUE).replace(
                    ",", "").contains(driver.scenarioData.getData
                    ("Total Amount").replace("-", "")));

            driver.assertElementAttributeString(totalInvoiceAmount, Constants.VALUE, "-");
        } else {
            Assert.assertTrue("Fail: WebElement text is: " + totalInvoiceAmount.getAttribute(Constants.VALUE).replace(",",
                    "") + " And validation text is: " + driver.scenarioData.getData
                    ("Total Amount").replace("-", ""), totalInvoiceAmount.getAttribute(Constants.VALUE).replace(
                    ",", "").contains(driver.scenarioData.getData
                    ("Total Amount").replace("-", "")));
        }
        LOGGER.info("verifyTotalAmountInBillingDocumentInWper completed");
    }

    /**
     * Selects nth row from the list of assembly's displayed in WE02
     */
    public void selectAssemblyRow() {
        LOGGER.info("selectAssemblyRow started");
        driver.waitForPageToLoad();
        List<WebElement> rows = webDriver.findElements(assemblyRowBy);
        int size = rows.size();
        rows.get(size - 5).click();
        LOGGER.info("selectAssemblyRow completed");
    }

    /**
     * This method will click the element multiple times.
     *
     * @param scrollElementName - scroll until a particular element
     * @Param visibleElementName - Element you want to click
     */
    public void scrollTillElementIsVisible(String visibleElementName, String scrollElementName) {
        LOGGER.info("scrollTillElementIsVisible started");
        if (driver.isElementDisplayed(returnElement(scrollElementName))) {
            while (!returnElement(visibleElementName).isDisplayed()) {
                try {
                    driver.waitForElementVisible(returnElement(scrollElementName));
                    commonActions.click(returnElement(scrollElementName));

                } catch (StaleElementReferenceException s) {
                    WebElement element = webDriver.findElement(commonActions.extractByLocatorFromWebElement(returnElement(scrollElementName)));
                    commonActions.click(element);
                }
            }
        } else {

        }
        LOGGER.info("scrollTillElementIsVisible completed");
    }

    /**
     * This method will validate the date format for a line item in PTS records
     */
    public void validateTheDateFormatInPts() {
        LOGGER.info("validateTheDateFormatInPts started");
        String dateWithExpectedFormat = driver.getTodayDate(commonActions.MM_DD_YYYY_FORMAT);
        String actualDataOnPts = getDateField.getAttribute(Constants.VALUE);
        Assert.assertEquals("FAIL: The date format expected is: " + dateWithExpectedFormat +
                ", But date format found is: " + actualDataOnPts, dateWithExpectedFormat, actualDataOnPts);
        LOGGER.info("validateTheDateFormatInPts completed");
    }

    /**
     * This method will validate the dexecution status of the idoc created
     */
    public void validateExecutionStatus() {
        LOGGER.info("validateExecutionStatus started");
        driver.waitForPageToLoad(2000);
        List<WebElement> element = webDriver.findElements(By.cssSelector("[id*='1,1#icp'] img:nth-child(1)"));
        driver.assertElementAttributeString(element.get(1), Constants.TITLE, SUCCESS);
        LOGGER.info("validateExecutionStatus completed");
    }

    public void loopThroughIdocAndCreateStandardInBound(int numberOfRecords) {
        while (numberOfRecords > 0) {
            LOGGER.info("loopThroughIdocAndCreateStandardInBound started for Record Number: " + numberOfRecords);
            commonActions.doubleClickOnElement(prodIssuetab);
            commonActions.switchFrameContext(0);
            Actions actions = new Actions(webDriver);
            actions.moveToElement(refDocNo).build().perform();
            commonActions.sendKeys(refDocNo, String.valueOf(CommonUtils.getRandomNumber(10, 99999)));
            driver.performKeyAction(Keys.ENTER);
            commonActions.switchToMainWindow();
            driver.performKeyAction(Keys.F8);
            commonActions.switchFrameContext(0);
            commonActions.click(continueEnter);
            commonActions.switchToMainWindow();
            driver.performKeyAction(Keys.ENTER);
            numberOfRecords--;
            LOGGER.info("loopThroughIdocAndCreateStandardInBound completed");
        }
    }

    /**
     * Method will start processing each PTS record
     *
     * @throws Throwable
     */
    public void processAllSelectedPtsRecords() throws Throwable {
        LOGGER.info("processAllSelectedPtsRecords started");
        LOGGER.info("expectMaximumSessionsPopUpVisible flag value: " + expectMaximumSessionsPopUpVisible);
        if (!commonActions.isTextAvailableInFirstIframe(DOCUMENT_DISPLAY_MESSAGES)) {
            if (expectMaximumSessionsPopUpVisible) {
                commonActions.waitForFirstIframeWithTextInDolAp2n(MAXIMUM_NUMBER_OF_EXTERNAL_SESSIONS_REACHED);
                while (commonActions.isTextAvailableInFirstIframe(INFORMATION)) {
                    driver.performKeyAction(Keys.ENTER);
                }
            }
            commonActions.waitForElementWithPartialText(PTS_INVOICE_DETAIL_AND_POSTING_SCREEN, Constants.ONE_HUNDRED);
            LOGGER.info("PTS Record number: " + count + " : " + commonActions.returnElementWithPartialText
                    (PTS_INVOICE_DETAIL_AND_POSTING_SCREEN).getText().replaceAll("[^0-9]", ""));
            driver.scenarioData.setData("pts", commonActions.returnElementWithPartialText
                    (PTS_INVOICE_DETAIL_AND_POSTING_SCREEN).getText().replaceAll("[^0-9]", ""));

            if (commonActions.returnElementWithPartialText(ATTEMPT_POSTING).isDisplayed()) {
                driver.waitForPageToLoad();
                commonActions.takePtsScreenShotsAtIntervals(FINAL_OUTPUT_PTS_FOLDER_NAME, driver.scenarioData.getData(PTS) + " Initial Screenshot for the record before processing");
                if (!recordPosted()) {
                    LOGGER.info("PTS TEST STEP: Attemp to Post Failed");
                    commonActions.takePtsScreenShotsAtIntervals(FINAL_OUTPUT_PTS_FOLDER_NAME, driver.scenarioData.getData(PTS) + " " + ATTEMPT_POSTING + " Failed");
                    resubmitProcess();
                }
            } else {
                LOGGER.info("PTS TEST STEP: No Attemp to Post Button");
                resubmitProcess();
            }
            if (!commonActions.isTextAvailableInFirstIframe(DOCUMENT_DISPLAY_MESSAGES)) {
                if (commonActions.isTextAvailableInFirstIframe(INFORMATION, 30) ||
                        commonActions.isTextAvailableInFirstIframe(DOCUMENT_DISPLAY_MESSAGES, 10)) {
                    LOGGER.info("PTS Record Number: " + driver.scenarioData.getData(PTS) + " is " + determineStatus());
                    count++;
                    expectMaximumSessionsPopUpVisible = true;
                    LOGGER.info("PTS RECORD COUNT: " + count);
                    processAllSelectedPtsRecords();
                } else if (webDriver.getPageSource().contains(PTS_INVOICE_DETAIL_AND_POSTING_SCREEN)) {
                    LOGGER.info("PTS Record Number(No Max Session pop Up): " + driver.scenarioData.getData("pts") + " is " + determineStatus());
                    count++;
                    expectMaximumSessionsPopUpVisible = false;
                    LOGGER.info("PTS RECORD COUNT(No Max Session pop Up): " + count);
                    processAllSelectedPtsRecords();
                }
                Assert.assertTrue(Integer.valueOf(commonActions.returnElementWithPartialText(DOLPHIN_HEADER)
                        .getText().substring(50).replaceAll("[^0-9]", "")) == 0);
            } else {
                driver.performKeyAction(Keys.ENTER);
                driver.waitForPageToLoad();
                Assert.assertTrue(Integer.valueOf(commonActions.returnElementWithPartialText(DOLPHIN_HEADER)
                        .getText().substring(50).replaceAll("[^0-9]", "")) == 0);
            }
        } else {
            driver.performKeyAction(Keys.ENTER);
            driver.waitForMilliseconds();
            Assert.assertTrue(Integer.valueOf(commonActions.returnElementWithPartialText(DOLPHIN_HEADER)
                    .getText().substring(50).replaceAll("[^0-9]", "")) == 0);
        }
        count = 1;
        LOGGER.info("processAllSelectedPtsRecords completed");
    }

    /**
     * Method determines the status
     *
     * @return
     */
    private String determineStatus() {
        LOGGER.info("determineStatus started");
        if (isPosted) {
            isPosted = false;
            LOGGER.info("determineStatus completed with message: " + SUCCESFULLY_POSTED);
            return SUCCESFULLY_POSTED;
        } else {
            LOGGER.info("determineStatus completed with message: " + RESUBMITTED);
            return RESUBMITTED;
        }
    }

    /**
     * Determine status
     *
     * @return
     */
    private String determineStatusForScreenShot() {
        LOGGER.info("determineStatusForScreenShot started");
        if (isScreenShotFlagPosted) {
            isScreenShotFlagPosted = false;
            LOGGER.info("determineStatusForScreenShot completed with message: " + SUCCESFULLY_POSTED);
            return SUCCESFULLY_POSTED;
        } else {
            LOGGER.info("determineStatusForScreenShot completed with message: " + RESUBMITTED);
            return RESUBMITTED;
        }
    }

    /**
     * Handles warnings
     *
     * @return - Warnings validation status
     * @throws Throwable
     */
    private boolean isWarning() throws Throwable {
        LOGGER.info("isWarning started");
        boolean isWarning = false;
        boolean isValidated = false;
        int enterCount = 10;
        while (driver.isElementDisplayed(warningPostingMessageAtTheBottomBy, 2) && enterCount != 0) {
            LOGGER.info("PTS TEST STEP: recordposted method got the warning");
            commonActions.takePtsScreenShotsAtIntervals(driver.scenarioData.getData(PTS), WARNING);
            driver.waitForMilliseconds();
            driver.performKeyAction(Keys.ENTER);
            isWarning = true;
            enterCount--;
        }
        if (isWarning) {
            if (!commonActions.isTextAvailableInFirstIframe(UPDATE_PTS_RECORD, 2) ||
                    !commonActions.isTextAvailableInFirstIframe(POSTING_MESSAGE, 2)) {
                LOGGER.info("PTS TEST STEP: recordposted method got the warning and no pop up after warning was resolved");
                commonActions.takePtsScreenShotsAtIntervals(driver.scenarioData.getData(PTS), "warning after checking for two pop ups");

                isValidated = true;
            }
        }
        LOGGER.info("isWarning completed");
        return isValidated;
    }

    /**
     * Attempt To post the PTS record and handle all Warnings, errors and success cases.
     *
     * @throws Throwable
     */
    private boolean recordPosted() throws Throwable {
        LOGGER.info("recordPosted started");
        boolean isSuccess = false;
        boolean isValidated = false;
        boolean isWarningBeforeSave;
        boolean isWarningAfterSave;
        commonActions.clickOnPartialText(ATTEMPT_POSTING);
        LOGGER.info("PTS TEST STEP: recordposted method clicked attempt posting button");
        if (!driver.isElementDisplayed(errorPostingMessageAtTheBottomBy, 2)) {
            isWarningBeforeSave = isWarning();
            if (isWarningBeforeSave) {
                isValidated = true;
            }
            LOGGER.info("PTS TEST STEP: recordposted method checked warning before save");
            if (commonActions.isTextAvailableInFirstIframe(UPDATE_PTS_RECORD)) {
                if (!commonActions.isTextAvailableInFirstIframe(POSTING_MESSAGE)) {
                    driver.switchFrameContext(0);
                    try {
                        commonActions.clickOnPartialText(SAVE);
                    } catch (NoSuchElementException nse) {
                        LOGGER.info("Entered the exception, POP Issue found for PTS: " + driver.scenarioData.getData(PTS));
                        webDriver.switchTo().defaultContent();
                    }
                    webDriver.switchTo().defaultContent();

                }
            }
            waitTillPopUpWithTextCloses(UPDATE_PTS_RECORD);
            isWarningAfterSave = isWarning();
            if (isWarningAfterSave) {
                isValidated = true;
            }
            LOGGER.info("PTS TEST STEP: recordposted method checked warning after save");
            if (commonActions.isTextAvailableInFirstIframe(POSTING_MESSAGE)) {
                isSuccess = returnErrorOrSuccessForARecordPosting();
                closeErrorOrSuccessMessagePopUp();
                isValidated = true;
                LOGGER.info("PTS TEST STEP: isSuccess flag: " + isSuccess);
                LOGGER.info("PTS TEST STEP: recordposted method error or success");
            }
            waitTillPopUpWithTextCloses(POSTING_MESSAGE);
        } else {
            isValidated = true;
            LOGGER.info("PTS TEST STEP: recordposted method captured direct error and skipped other steps");
        }
        if (!isValidated) {
            isSuccess = false;
            LOGGER.info("PTS TEST STEP: No clue what to do now! sending resubmit request for PTS record number: " + driver.scenarioData.getData(PTS));
        }
        LOGGER.info("recordPosted completed");
        return isSuccess;
    }

    /**
     * Checks record posting status
     *
     * @return - posting status
     * @throws Throwable
     */
    private boolean returnErrorOrSuccessForARecordPosting() throws Throwable {
        LOGGER.info("returnErrorOrSuccessForARecordPosting started");
        boolean isSuccess = false;
        while (driver.isElementDisplayed(webDriver.findElement(CommonActions.firstIframeElementBy), 2)
                && commonActions.isTextAvailableInFirstIframe(POSTING_MESSAGE)) {
            driver.switchFrameContext(0);
            if (webDriver.findElements(messageInPtsPopUpBy).get(0).getAttribute(Constants.TITLE).equalsIgnoreCase(INFORMATION)) {
                isSuccess = true;
                isPosted = true;
                isScreenShotFlagPosted = true;
                commonActions.takePtsScreenShotsAtIntervals(FINAL_OUTPUT_PTS_FOLDER_NAME,
                        driver.scenarioData.getData(PTS) + " " + determineStatusForScreenShot());
            } else {
                isSuccess = false;
            }
            driver.performKeyAction(Keys.ENTER);
            driver.waitForPageToLoad();
            webDriver.switchTo().defaultContent();
            if (isSuccess) {
                for (int i = 0; i < 20; i++) {
                    if (!commonActions.isTextAvailableInFirstIframe(INFORMATION)) {
                        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
                    }
                }
            }
        }
        LOGGER.info("returnErrorOrSuccessForARecordPosting completed");
        return isSuccess;
    }

    private void closeErrorOrSuccessMessagePopUp() {
        LOGGER.info("closeErrorOrSuccessMessagePopUp started");
        while (driver.isElementDisplayed(webDriver.findElement(CommonActions.firstIframeElementBy), 2)
                && commonActions.isTextAvailableInFirstIframe(POSTING_MESSAGE)) {
            driver.performKeyAction(Keys.ENTER);
            driver.waitForPageToLoad();
        }
        LOGGER.info("closeErrorOrSuccessMessagePopUp completed");
    }

    /**
     * Resubmits the PTS record and handle all Warnings, errors and success cases.
     *
     * @throws Throwable
     */
    private void resubmitProcess() throws Throwable {
        LOGGER.info("resubmitProcess started");
        String classAttribute = null;
        int enterCount = 10;
        while ((driver.isElementDisplayed(errorPostingMessageAtTheBottomBy, 1) ||
                driver.isElementDisplayed(warningPostingMessageAtTheBottomBy, 1)) && enterCount != 0) {
            driver.waitForMilliseconds();
            driver.performKeyAction(Keys.ENTER);
            enterCount--;
        }
        if (enterCount == 0) {
            LOGGER.info("PTS TEST STEP: Hard Warning appeared");
        }
        LOGGER.info("PTS TEST STEP: resubmitProcess method check warnings");
        commonActions.clickOnPartialText(RESUBMIT);
        LOGGER.info("PTS TEST STEP: resubmitProcess click on resubmit");
        commonActions.takePtsScreenShotsAtIntervals(FINAL_OUTPUT_PTS_FOLDER_NAME, driver.scenarioData.getData(PTS) + " resubmitProcess click on resubmit");
        while (driver.isElementDisplayed(errorPostingMessageAtTheBottomBy, 2)) {
            driver.performKeyAction(Keys.ENTER);
        }
        while (driver.isElementDisplayed(warningPostingMessageAtTheBottomBy, 2)) {
            driver.waitForMilliseconds();
            driver.performKeyAction(Keys.ENTER);
        }
        LOGGER.info("PTS TEST STEP: resubmitProcess method checked warnings after click");
        if (commonActions.isTextAvailableInFirstIframe(UPDATE_PTS_RECORD)) {
            driver.switchFrameContext(0);
            commonActions.clickOnPartialText(SAVE);
            webDriver.switchTo().defaultContent();
        }
        waitTillPopUpWithTextCloses(UPDATE_PTS_RECORD);
        LOGGER.info("PTS TEST STEP: resubmitProcess method passed Update PTS Record method");
        driver.waitForPageToLoad();
        closeErrorOrSuccessMessagePopUp();
        LOGGER.info("PTS TEST STEP: resubmitProcess method passed closed errors/warnings after save if any");
        if (commonActions.isTextAvailableInFirstIframe(DOLPHIN_PTS_SUBSTATUS)) {
            driver.switchFrameContext(0);
            for (int i = 0; i < 3; i++) {
                commonActions.click(scrollInDol);
            }
            do {
                if (driver.isElementDisplayed(subStatusSelectionBy, 2)) {
                    commonActions.click(webDriver.findElement(subStatusSelectionBy));
                    classAttribute = webDriver.findElement(subStatusSelectionBy).getAttribute(Constants.CLASS);
                } else {
                    commonActions.click(webDriver.findElement(subStatusSelectionByText));
                    classAttribute = webDriver.findElement(subStatusSelectionByText).getAttribute(Constants.CLASS);
                }
            } while (!classAttribute.contains(SEL2BGCOLOR));
            webDriver.switchTo().defaultContent();
            while (commonActions.isTextAvailableInFirstIframe(DOLPHIN_PTS_SUBSTATUS)) {
                driver.waitForMilliseconds();
                commonActions.takePtsScreenShotsAtIntervals(FINAL_OUTPUT_PTS_FOLDER_NAME,
                        driver.scenarioData.getData(PTS) + " " + determineStatusForScreenShot());
                driver.performKeyAction(Keys.F6);
            }
        }
        waitTillPopUpWithTextCloses(DOLPHIN_PTS_SUBSTATUS);
        LOGGER.info("PTS TEST STEP: resubmitProcess method passed and resubmitted");
        LOGGER.info("resubmitProcess completed");
    }

    /**
     * Waits till iframe with text closes
     *
     * @param text
     */
    private void waitTillPopUpWithTextCloses(String text) {
        LOGGER.info("waitTillPopUpWithTextCloses started");
        while (commonActions.isTextAvailableInFirstIframe(text)) {
            driver.waitForMilliseconds();
        }
        LOGGER.info("waitTillPopUpWithTextCloses completed");
    }

    /**
     * Method to input GL account number in Dolphin
     *
     * @param elementName - Name of the element
     * @param glNumber - GL number
     */
    public void inputGlAccountInDol(String elementName, String glNumber) {
        LOGGER.info("inputGlAccountInDol started");
        commonActions.sendKeys(returnElement(elementName), glNumber);
        driver.performKeyAction(Keys.ENTER);
        driver.waitForPageToLoad();
        if (!webDriver.findElement(returnByElement(elementName)).getAttribute(Constants.VALUE).contains(glNumber)) {
            commonActions.sendKeys(returnElement(elementName), glNumber);
            driver.performKeyAction(Keys.ENTER);
        }
        LOGGER.info("inputGlAccountInDol completed");
    }

    /**
     * Method to verify Tax fields in Dolphin
     *
     * @param columnNumber - Column number
     * @param rowNumber - Row number
     * @param taxCode - Tax Code
     */
    public void verifyTaxFields(String columnNumber, int rowNumber, String taxCode) {
        LOGGER.info("verifyTaxFields started");
        String validationText = webDriver.findElements(By.xpath("//*[contains(@id,'," + columnNumber + "#if-r')]/child::*")).get(rowNumber - 1).getText();
        if (validationText.equalsIgnoreCase("")) {
            validationText = webDriver.findElements(By.xpath("//*[contains(@id,'," + columnNumber + "#if-r')]/child::*")).get(rowNumber - 1).getAttribute(Constants.VALUE);
        }
        Assert.assertTrue("FAIL: ", validationText.equalsIgnoreCase(taxCode));
        LOGGER.info("verifyTaxFields completed");
    }
    /**
     * Selects variant in dolphin
     *
     * @param variantName - Exact name of the variant
     */
    public void selectVariantInDol(String variantName) {
        LOGGER.info("selectVariantInDol started");
        commonActions.selectElementWithTitle(GET_VARIANT_TEXT);
        commonActions.switchFrameContext(0);
        commonActions.returnElementWithLabelName(CREATED_BY).click();
        commonActions.returnElementWithLabelName(CREATED_BY).clear();
        commonActions.sendKeysWithLabelName(VARIANT, variantName.toUpperCase());
        driver.waitForMilliseconds();
        driver.performKeyAction(Keys.F8);
        commonActions.switchToMainWindow();
        LOGGER.info("selectVariantInDol completed");
    }
}