package sap.pages;


import common.Constants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.List;
import java.util.logging.Logger;

public class OrderToCash {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(OrderToCash.class.getName());

    public OrderToCash(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "M1:U:::0:24")
    private static WebElement customerId;

    @FindBy(id = "M1:U:::2:24")
    private static WebElement companyCode;

    @FindBy(id = "M1:U:::5:24")
    private static WebElement salesOrganisation;

    @FindBy(id = "M1:U:::6:24")
    private static WebElement distributionChannel;

    @FindBy(id = "M1:U:::7:24")
    private static WebElement createCustomerDistributionChannel;

    @FindBy(id = "M1:U:::7:24")
    private static WebElement division;

    @FindBy(id = "M1:U:::8:24")
    private static WebElement createCustomerDivision;

    @FindBy(xpath = "//img[contains(@src,'okay')]")
    private static WebElement continueButton;

    @FindBy(xpath = "//div[@title='Certificate']")
    private static WebElement certificateOptions;

    @FindBy(id = "M1:U:::4:17")
    private static WebElement certificateField;

    @FindBy(id = "M0:U:2:1:2B262:1::11:1-img")
    private static WebElement moreDetails;

    @FindBy(id = "M0:U:1::14:17")
    private static WebElement phoneNumber;

    @FindBy(id = "M0:U:1::16:17")
    private static WebElement email;

    @FindBy(id = "M0:D:10::btn[3]")
    private static WebElement backButton;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::26:22")
    private static WebElement emailInGeneralData;

    @FindBy(id = "M1:U:::5:17")
    private static WebElement invoiceCopyOption;

    @FindBy(id = "M1:U:::6:17")
    private static WebElement poRequiredField;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::23:22")
    private static WebElement telephoneNumber;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::10:30")
    private static WebElement streetAddess;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::10:22")
    private static WebElement houseNumber;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::11:22")
    private static WebElement district;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::12:22")
    private static WebElement city;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::12:63")
    private static WebElement state;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::12:67")
    private static WebElement zipCode;

    @FindBy(xpath = "//input[contains(@id,'[1,2]_c')][contains(@lsevents,'history')]")
    private static WebElement firstFieldValueInClassification;

    @FindBy(xpath = "//input[contains(@id,'[1,4]_c')]")
    private static WebElement posNotesComment;

    @FindBy(id = "M0:D:13::btn[5]-img")
    private static WebElement displayChangeButtonOnNavigationBar;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::14:22")
    private static WebElement timeZoneInAddress;

    @FindBy(id = "M1:U:::0:24")
    private static WebElement accountGroup;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::3:22")
    private static WebElement arName;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::10:22")
    private static WebElement arStreetNumber;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::10:30")
    private static WebElement arAddress;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::12:22")
    private static WebElement arCountry;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::11:22")
    private static WebElement arCity;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::11:63")
    private static WebElement arState;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::11:67")
    private static WebElement arPostalCode;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::22:22")
    private static WebElement arPhoneNumber;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::24:22")
    private static WebElement arFax;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::25:22")
    private static WebElement arEmail;

    @FindBy(id = "M0:U:2:1:2B257:2::2:17")
    private static WebElement arIndustry;

    @FindBy(id = "M0:U:2:1:2B257:3::2:17")
    private static WebElement arTaxNumber2;

    @FindBy(xpath = "//input[contains(@id,'[1,2]_c')]")
    private static WebElement arCustomerName;

    @FindBy(xpath = "//input[contains(@id,'[1,5]_c')]")
    private static WebElement arCustomerDepartment;

    @FindBy(xpath = "//input[contains(@id,'[1,7]_c')]")
    private static WebElement arCustomerFunction;

    @FindBy(id = "wnd[0]/sbar_msg")
    private static WebElement arCustomerExtensionMessage;

    @FindBy(id = "M0:U:1::15:17")
    private static WebElement arCustomerFax;

    @FindBy(id = "M0:U:2:1:2B256:1::1:17")
    private static WebElement arReconAccount;

    @FindBy(id = "M0:U:2:1:2B256:1::3:52")
    private static WebElement arCashMgmtGroup;

    @FindBy(id = "M0:U:::2:23")
    private static WebElement arClassType;

    @FindBy(xpath = "//input[contains(@id,'[1,1]_c')]")
    private static WebElement arClassAssignments;

    @FindBy(id = "ls-inputfieldhelpbutton")
    private static WebElement arCustomerValueField;

    @FindBy(xpath = "//img[contains(@id, '[1,1]_c-img')]")
    private static WebElement arCustomerSelectEntryField;

    @FindBy(id = "M1:D:13::btn[8]-img")
    private static WebElement arCustomerSelectGreenCheck;

    @FindBy(id = "M0:U:2:1:2B257:1::1:25")
    private static WebElement termsOfPayment;

    @FindBy(id = "M0:U:2:1:2B257:1::2:25")
    private static WebElement creditMemoPayment;

    @FindBy(id = "M0:U:2:1:2B258:2::1:17")
    private static WebElement acctgClerk;

    @FindBy(id = "M0:U:2:1:2B258:2::2:17")
    private static WebElement acctgAtCust;

    @FindBy(id = "M0:U:2:1:2B258:2::1:63")
    private static WebElement acctgStatement;

    @FindBy(xpath = "//*[text()='Payment history record']/preceding::*[1]")
    private static WebElement paymentHistoryRecord;

    @FindBy(id = "M0:U:2:1:2B256:2::2:17")
    private static WebElement CustPriceProc;

    @FindBy(id = "M0:U:2:1:2B257:1::2:22")
    private static WebElement ShippingConditions;

    @FindBy(xpath = "//input[contains(@id,'[1,5]')]")
    private static WebElement TaxClass;

    @FindBy(id = "wnd[0]/sbar_msg-txt")
    private static WebElement successMessage;

    @FindBy(id = "M0:U:2:1:2B256:1:1:1::7:22")
    private static WebElement employeeID;

    @FindBy(id = "M0:U:2:1:2B256:3::1:17")
    private static WebElement prevAcctNo;

    private static final By firstColumnInAllTablesBy = By.xpath("//input[contains(@id,'1]_c')]");

    private static final By thirdColumnInAllTablesBy = By.xpath("//input[contains(@id,'3]_c')]");

    private static final By fourthColumnInAllTablesBy = By.xpath("//input[contains(@id,'4]_c')]");

    private static final By secondRowInAllTablesBy = By.xpath("//input[contains(@id,'2]_c')]");

    private static final By fifthRowInAllTablesBy = By.xpath("//input[contains(@id,'5]_c')]");

    private static final By seventhRowInAllTablesBy = By.xpath("//input[contains(@id,'7]_c')]");

    private static int index = 0;
    private static final String ACCOUNT_GROUP = "Account Group";
    private static final String CUSTOMER_ID = "Customer Id";
    private static final String NAME = "Name";
    private static final String COMPANY_CODE = "Company Code";
    private static final String SALES_ORGANISATION = "Sales Organisation";
    private static final String DISTRIBUTION_CHANNEL = "Distribution Channel";
    private static final String CREATE_CUSTOMER_DISTRIBUTION_CHANNEL = "Create Customer Distribution Channel";
    private static final String CERTIFICATE_FIELD = "Certificate Field";
    private static final String CERTIFICATE_OPTIONS = "Certificate Options";
    private static final String SECOND_ROW = "Second Row";
    private static final String FIFTH_ROW = "Fifth Row";
    private static final String SEVENTH_ROW = "Seventh Row";
    private static final String FIRST_ROW = "First Row";
    private static final String THIRD_ROW = "Third Row";
    private static final String FOURTH_ROW = "Fourth Row";
    private static final String DETAILS_IMAGE = "More Details";
    private static final String PHONE_NUMBER = "Phone Number";
    private static final String EMAIL = "Email";
    private static final String BACK_BUTTON = "Back Button";
    private static final String DIVISION = "Division";
    private static final String CREATE_CUSTOMER_DIVISION = "Create Customer";
    private static final String CONTINUE_BUTTON = "CONTINUE";
    private static final String EMAIL_IN_GENERAL_DATA = "Email in General Data";
    private static final String INVOICE_COPY_OPTION = "Invoice Copy";
    private static final String PO_REQUIRED = "Po Required Field";
    private static final String TELEPHONE = "Telephone";
    private static final String STREET_ADDRESS = "Street";
    private static final String HOUSE_ADDRESS = "House no.";
    private static final String ZIP_CODE = "Zip Code";
    private static final String DISTRICT = "District";
    private static final String STATE = "State";
    private static final String CITY = "City";
    private static final String FIRST_FIELD_VALUE = "First Field Value";
    private static final String POS_NOTES_COMMENT = "POS Notes";
    private static final String DISPLAY_CHANGE = "Display Change";
    private static final String TIME_ZONE = "Time Zone";
    private static final String AR_NAME = "AR Name";
    private static final String AR_STREET_NUMBER = "AR Street Number";
    private static final String AR_ADDRESS = "AR Address";
    private static final String AR_CITY = "AR City";
    private static final String AR_STATE = "AR State";
    private static final String AR_INDUSTRY = "AR Industry";
    private static final String AR_TAX_NUMBER2 = "AR Tax Number 2";
    private static final String AR_COUNTRY = "AR Country";
    private static final String AR_POSTAL_CODE = "AR Postal Code";
    private static final String AR_PHONE_NUMBER = "AR Phone Number";
    private static final String AR_FAX = "AR Fax";
    private static final String AR_EMAIL = "AR Email";
    private static final String AR_CUSTOMER_NAME = "AR Customer Name";
    private static final String AR_CUSTOMER_DEPARTMENT = "AR Customer Department";
    private static final String AR_CUSTOMER_FUNCTION = "AR Customer Function";
    private static final String AR_CUSTOMER_PHONE_EXT_MESSAGE = "Message";
    private static final String AR_CUSTOMER_FAX = "AR Customer Fax";
    private static final String AR_RECON_ACCOUNT = "Recon. Account";
    private static final String AR_CASH_MGMT_GROUP = "Cash mgmt group";
    private static final String AR_CLASS_ASSIGNMENT = "Class Assignments";
    private static final String AR_CLASS_TYPE = "Class Type";
    private static final String AR_CUSTOMER_INV_VALUE = "Value";
    private static final String AR_CUSTOMER_SELECT_ENTRY_FIELD = "Customer Inv Req field";
    private static final String AR_CUSTOMER_SELECT_GREEN_CHECK = "Green Check";
    private static final String TERMS_OF_PAYMENT = "Terms Of Payments";
    private static final String CREDIT_MEMO_PAYMENT = "Credit Memo payt term";
    private static final String ACCOUNT_G_CLERK = "Acctg clerk";
    private static final String ACCOUNT_G_AT_CUST = "Acct at cust.";
    private static final String ACCOUNT_G_STATEMENT = "Account Statement";
    private static final String PAYMENT_HISTORY_RECORD = "Payment history record";
    private static final String CUST_PRICE_PROC = "Cust price proc";
    private static final String SHIPPING_CONDITIONS = "Shipping Conditions";
    private static final String TAX_CLASS = "Tax Class";
    private static final String SUCCESS_MESSAGE = "message text";
    private static final String EMPLOYEE_ID = "Search term 1/2";
    private static final String PREV_ACCT_NO = "Prev Acct No";

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case ACCOUNT_GROUP:
                return accountGroup;
            case CUSTOMER_ID:
                return customerId;
            case COMPANY_CODE:
                return companyCode;
            case SALES_ORGANISATION:
                return salesOrganisation;
            case DISTRIBUTION_CHANNEL:
                return distributionChannel;
            case CREATE_CUSTOMER_DISTRIBUTION_CHANNEL:
                return createCustomerDistributionChannel;
            case DIVISION:
                return division;
            case CREATE_CUSTOMER_DIVISION:
                return createCustomerDivision;
            case CONTINUE_BUTTON:
                return continueButton;
            case CERTIFICATE_FIELD:
                return certificateField;
            case CERTIFICATE_OPTIONS:
                return certificateOptions;
            case DETAILS_IMAGE:
                return moreDetails;
            case PHONE_NUMBER:
                return phoneNumber;
            case EMAIL:
                return email;
            case BACK_BUTTON:
                return backButton;
            case EMAIL_IN_GENERAL_DATA:
                return emailInGeneralData;
            case INVOICE_COPY_OPTION:
                return invoiceCopyOption;
            case PO_REQUIRED:
                return poRequiredField;
            case TELEPHONE:
                return telephoneNumber;
            case STREET_ADDRESS:
                return streetAddess;
            case HOUSE_ADDRESS:
                return houseNumber;
            case ZIP_CODE:
                return zipCode;
            case DISTRICT:
                return district;
            case STATE:
                return state;
            case CITY:
                return city;
            case FIRST_FIELD_VALUE:
                return firstFieldValueInClassification;
            case POS_NOTES_COMMENT:
                return posNotesComment;
            case DISPLAY_CHANGE:
                return displayChangeButtonOnNavigationBar;
            case TIME_ZONE:
                return timeZoneInAddress;
            case AR_NAME:
                return arName;
            case AR_STREET_NUMBER:
                return arStreetNumber;
            case AR_ADDRESS:
                return arAddress;
            case AR_CITY:
                return arCity;
            case AR_COUNTRY:
                return arCountry;
            case AR_POSTAL_CODE:
                return arPostalCode;
            case AR_PHONE_NUMBER:
                return arPhoneNumber;
            case AR_FAX:
                return arFax;
            case AR_EMAIL:
                return arEmail;
            case AR_STATE:
                return arState;
            case AR_INDUSTRY:
                return arIndustry;
            case AR_TAX_NUMBER2:
                return arTaxNumber2;
            case AR_CUSTOMER_NAME:
                return arCustomerName;
            case AR_CUSTOMER_DEPARTMENT:
                return arCustomerDepartment;
            case AR_CUSTOMER_FUNCTION:
                return arCustomerFunction;
            case AR_CUSTOMER_PHONE_EXT_MESSAGE:
                return arCustomerExtensionMessage;
            case AR_CUSTOMER_FAX:
                return arCustomerFax;
            case AR_RECON_ACCOUNT:
                return arReconAccount;
            case AR_CASH_MGMT_GROUP:
                return arCashMgmtGroup;
            case AR_CLASS_ASSIGNMENT:
                return arClassAssignments;
            case AR_CLASS_TYPE:
                return arClassType;
            case AR_CUSTOMER_INV_VALUE:
                return arCustomerValueField;
            case AR_CUSTOMER_SELECT_ENTRY_FIELD:
                return arCustomerSelectEntryField;
            case AR_CUSTOMER_SELECT_GREEN_CHECK:
                return arCustomerSelectGreenCheck;
            case TERMS_OF_PAYMENT:
                return termsOfPayment;
            case CREDIT_MEMO_PAYMENT:
                return creditMemoPayment;
            case ACCOUNT_G_CLERK:
                return acctgClerk;
            case ACCOUNT_G_AT_CUST:
                return acctgAtCust;
            case ACCOUNT_G_STATEMENT:
                return acctgStatement;
            case PAYMENT_HISTORY_RECORD:
                return paymentHistoryRecord;
            case CUST_PRICE_PROC:
                return CustPriceProc;
            case SHIPPING_CONDITIONS:
                return ShippingConditions;
            case TAX_CLASS:
                return TaxClass;
            case SUCCESS_MESSAGE:
                return successMessage;
            case EMPLOYEE_ID:
                return employeeID;
            case PREV_ACCT_NO:
                return prevAcctNo;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * return the By Element.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public By returnByElement(String elementName) {
        LOGGER.info("returnByElement started");
        switch (elementName) {
            case SECOND_ROW:
                return secondRowInAllTablesBy;
            case FIFTH_ROW:
                return fifthRowInAllTablesBy;
            case SEVENTH_ROW:
                return seventhRowInAllTablesBy;
            case FIRST_ROW:
                return firstColumnInAllTablesBy;
            case THIRD_ROW:
                return thirdColumnInAllTablesBy;
            case FOURTH_ROW:
                return fourthColumnInAllTablesBy;
            default:
                Assert.fail("FAIL: Could not find By element that matched string passed from step");
                return null;
        }
    }

    /**
     * This Method is click any element in order to cash page.
     *
     * @param elementName - send in elementName and gets the WebElement from returnElement method.
     */
    public void clickInOrderToCashPage(String elementName) {
        LOGGER.info("clickInOrderToCashPage started");
        commonActions.click(returnElement(elementName));
        LOGGER.info("clickInOrderToCashPage completed");
    }

    /**
     * This method will find the empty column in the row and inserts the input given to this method
     *
     * @param byElementName - returnByElement method will return the By element with reference to the input string
     * @param input         - input value to sendKeys
     * @param set           - once a set of values are entered, use "Reset" for first step step definition for next set of data.
     */
    public void clickClearAndInputKeysWhenEmpty(String byElementName, String input, String set) {
        LOGGER.info("clickClearAndInputKeysWhenEmpty started");
        if (set.equalsIgnoreCase("Reset")) {
            index = 0;
        }
        List<WebElement> elements = webDriver.findElements(returnByElement(byElementName));
        for (int i = index; i <= elements.size() - 1; i++) {
            try {
                if (!elements.get(i).getAttribute("lsdata").contains("11:")) {
                    driver.clearInputAndSendKeys(elements.get(i), input);
                    index = i;
                    break;
                }

            } catch (StaleElementReferenceException s) {
                WebElement staleElement = webDriver.findElements(returnByElement(byElementName)).get(i);
                driver.clearInputAndSendKeys(staleElement, input);
            }
        }
        LOGGER.info("clickClearAndInputKeysWhenEmpty completed");
    }

    /**
     * Verifies Message is displayed on order to cash page
     *
     * @param elementName string to verify the value of
     * @param message     Message to check on order to cash page
     */
    public void assertTextDisplayedInOrderToCash(String elementName, String message) {
        LOGGER.info("assertTextDisplayedInOrderToCash started");
        driver.verifyTextDisplayed(returnElement(elementName), message);
        LOGGER.info("assertTextDisplayedInOrderToCash completed");
    }

    /**
     * Save the Emp ID number
     *
     * @param empID - send in elementName and gets the WebELement from returnElement method.
     */
    public void saveEmpID(String empID) {
        LOGGER.info("saveEmpID started");
        commonActions.saveCurrentEmpID(returnElement(empID));
        LOGGER.info("saveEmpID completed");
    }

    /**
     * Gets Scenario data for Emp ID
     */
    public String getEmployeeID() {
        LOGGER.info("getEmployeeId started");
        String EmpID = driver.scenarioData.getCurrentEmpID();
        LOGGER.info("getEmployeeId completed");
        return EmpID;
    }

    /**
     * Function to input name in order to cash module
     *
     * @param name - Name of employee or customer
     */
    public void inputName(String name) {
        LOGGER.info("inputName started");
        WebElement element = commonActions.returnElementWithLabelNameAndPosition(NAME,"2");
        commonActions.sendKeys(element, commonActions.createUniqueValue(name));
        LOGGER.info("inputName completed");
    }
}
