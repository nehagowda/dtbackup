package sap.pages;

import common.Config;
import common.Constants;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import utilities.Driver;
import utilities.CommonUtils;

import java.awt.Robot;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class CommonActions {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(CommonActions.class.getName());

    public CommonActions(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "sysid")
    private static WebElement sapSystem;

    @FindBy(id = "sap-client")
    private static WebElement sapClient;

    @FindBy(id = "sap-user")
    private static WebElement sapUser;

    @FindBy(id = "sap-password")
    private static WebElement sapPassword;

    @FindBy(xpath = "//*[contains(text(),'Log On')]")
    private static WebElement submitButton;

    @FindBy(id = "delete-session-cb-img")
    private static WebElement deleteSession;

    @FindBy(id = "SESSION_QUERY_CONTINUE_BUTTON")
    private static WebElement continueButton;

    @FindBy(xpath = "//span[text()='Menu']/following::input[1]")
    private static WebElement commandField;

    @FindBy(id = "URLSPW-0")
    public static WebElement selectDocumentiFrame;

    @FindBy(xpath = "//*[@id=\"ul_nav_2\"]/li[2]/a")
    private static WebElement orderingTab;

    @FindBy(id = "a_30000000183")
    private static WebElement createDTDTransfer;

    @FindBy(id = "a_10000000183")
    private static WebElement createStoreTransfer;

    @FindBy(id = "a_50000000034")
    private static WebElement postGoodsIssue;

    @FindBy(id = "a_40000000037")
    private static WebElement postGoodsReceipt;

    @FindBy(id = "a_30000000100")
    private static WebElement changeEmployeeSite;

    @FindBy(id = "wnd[0]/sbar_msg-txt")
    private static WebElement purchaseOrderNumberAlertContainer;

    @FindBy(xpath = ("//*[@id=\"ul_nav_2\"]/li[1]/a"))
    private static WebElement receivingTab;

    @FindBy(xpath = ("//*[@id=\"ul_nav_2\"]/li[3]/a"))
    private static WebElement goodsMovementTab;

    @FindBy(xpath = ("//*[@id=\"ul_nav_2\"]/li[7]/a"))
    private static WebElement reportingTab;

    @FindBy(xpath = "//img[contains(@class,'Ok')]")
    private static WebElement successInSap;

    @FindBy(xpath = "//*[contains(@id,'1,3#if')and not (contains(@id,'1,3#if-r'))]//preceding::img[1]")
    public static WebElement paymentProposalSuccess;

    @FindBy(xpath = "//img[contains(@class,'Error')]")
    private static WebElement errorInSap;

    @FindBy(xpath = "//img[@title='Tasks with Errors Exist']")
    public static WebElement errorsInTasksExist;

    @FindBy(xpath = "//iframe[contains(@id,'URLSPW-0')]")
    private static WebElement iframeWithCheckOk;

    @FindBy(xpath = "//h2[text()='SAP NetWeaver']/following::span[contains(@id,'txt')][1]")
    private static WebElement logonAttemptsText;

    @FindBy(xpath = "//*[text()='You are already logged on to the system in the following sessions:']")
    private static WebElement nwbcSessionsMessage;

    @FindBy(xpath = "//*[contains(text(),'Cancel all existing logons')]")
    private static WebElement cancelAllLoginsNwbc;

    public static final By spanTagBy = By.tagName("span");

    public static final By inputTagBy = By.tagName("input");

    public static final By customerPriceListBy = By.xpath("//span[@class='z-comboitem-text'][contains(text(),'ug-0001')]");

    public static final By articleFromDropDownBy = By.xpath("//div[contains(@id='-cave'][contains(@class,'z-listcell-content')]");

    public static final By articleNumberInputRowBy = By.xpath("//*[contains(@id,',2]_c'])[not (contains(@id,',2]_c-r'))]");

    private static final By itemsTableColumnHeadingsBy = By.xpath("//tbody[contains(@id,'contentTBody')]/child::tr[1]/child::th");

    private static final By tCodeForUpgradedCaq = By.id("ToolbarOkCode");

    public static final By firstIframeElementBy = By.xpath("//iframe[@id='URLSPW-0']");

    private static final By iframeTitleHeaderBy = By.xpath("//div[@class='urPWTitleText']");

    private static By commandFieldBy = By.xpath("//span[text()='Menu']/following::input[1]");

    public static final By verticleScrollUpBy = By.xpath("//*[contains(@id,'vscroll-Prev')]");

    public static final By verticalScrollDownBy = By.xpath("//*[contains(@id,'vscroll-Nxt')]");

    public final String TEXT_XPATH = "//*[text()='";
    public static final String VALUE_XPATH = "//*[contains(@value,'";
    private final String PARTIAL_TEXT_XPATH = "//*[contains(text(),'";
    private final String PARTIAL_VALUE_XPATH = "//*[contains(@value,'";
    private final String PARTIAL_TITLE_XPATH = "//*[contains(@title,'";
    public static final String ARTICLE_FROM_LINE = "Article from line";
    public static final String AMOUNT_FROM_LINE = "Amount from line";
    private static final String TITLE_XPATH = "//*[contains(@title,'";
    private static final String ADDRESS_LINE_1 = "Address Line";
    private static final String TRANSACTION_TYPE = "Transaction Type";
    private static final String LAST_NAME = "Last Name";
    private static final String LEGACY_LAYWAY_AMOUNT = "Layaway Amount";
    private static final String DISTRIBUTION_CENTER = "DC";
    private static final String CITY = "City";
    private static final String STATE = "State";
    private static final String ZIP = "ZIP";
    private static final String SCREENSHOT_FOLDER = "Screenshot folder";
    public static final String SHEET_NAME = "Sheet name";
    private static final String MOVEMENT_TYPE = "Movement type";
    public static final String ENVIRONMENT_FEE = "Environment Fee Account";
    public static final String SALES_TAX = "Sales Tax Account";
    public static final String MM_DD_YYYY_FORMAT = "MM/dd/YYYY";
    public static final String CONTINUE = "Continue";
    public static final String VENDOR = "Vendor";
    public static final String SITE = "site";
    public static final String UP = "up";
    public static final String DOWN = "down";
    private static final String UPGRADED_CAR_URL = "carnwcaq";
    public static final String SAP_PASSWORD_KEY = "sappassword";
    public static final String FOLDER_NAME = "foldername";
    public static final String EXCEL_FILE_NAME = "excelfilename";
    public static final String SHEET_NAME_1 = "sheetname";
    public static final String ROW_NUMBER = "rownumber";
    public static final String USERS = "users";
    public static final String DEFAULT = "default";
    public static final String ITEM = "item";
    public static final String TOOLBAROKCODE = "toolbarokcode";
    public static final String TRUE = "true";
    public static final String DATE = "Date";
    public boolean cancelSession = false;
    private static final String CHAR_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * Returns NWBC element to click on
     *
     * @param tab tab to click on
     */
    public WebElement returnNetWeaverNavTab(String tab) {
        LOGGER.info("returnNetWeaverNavTab started");
        WebElement tabToClick = null;
        if (tab.equalsIgnoreCase(Constants.ORDERING)) {
            tabToClick = orderingTab;
        } else if (tab.equalsIgnoreCase(Constants.CREATE_DTD_TRANSFER)) {
            tabToClick = createDTDTransfer;
        } else if (tab.equalsIgnoreCase(Constants.CREATE_STORE_TRANSFER)) {
            tabToClick = createStoreTransfer;
        } else if (tab.equalsIgnoreCase(Constants.NWBC_RECEIVING_TAB)) {
            tabToClick = receivingTab;
        } else if (tab.equalsIgnoreCase(Constants.GOODS_MOVEMENT)) {
            tabToClick = goodsMovementTab;
        } else if (tab.equalsIgnoreCase(Constants.REPORTS)) {
            tabToClick = reportingTab;
        } else if (tab.equalsIgnoreCase(Constants.CHANGE_EMPLOYEE_SITE)) {
            tabToClick = changeEmployeeSite;
        } else if (tab.equalsIgnoreCase(Constants.POST_GOODS_ISSUE)) {
            tabToClick = postGoodsIssue;
        } else if (tab.equalsIgnoreCase(Constants.POST_GOODS_RECEIPT)) {
            tabToClick = postGoodsReceipt;
        }
        LOGGER.info("returnNetWeaverNavTab completed");
        return tabToClick;
    }

    /**
     * Switches frame to in order to access fields
     */
    public void switchToRespectiveiFrame() {
        LOGGER.info("switchToRespectiveiFrame started");
        driver.waitForPageToLoad();
        webDriver.switchTo().frame(0);
        LOGGER.info("switchToRespectiveiFrame completed");
    }

    /**
     * Switches frame back to the main window
     */
    public void switchToMainWindow() {
        LOGGER.info("switchToMainWindow started");
        webDriver.switchTo().defaultContent();
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        LOGGER.info("switchToMainWindow completed");
    }

    /**
     * Switches to the second frame on the page in order to access fields
     */
    public void switchToParentiFrame() {
        LOGGER.info("switchToRespectiveiFrame started");
        driver.waitForPageToLoad();
        webDriver.switchTo().frame(1);
        LOGGER.info("switchToRespectiveiFrame completed");
    }

    /**
     * Sends input
     *
     * @param element - WebELement
     * @param input   - Input text
     */
    public void sendKeys(WebElement element, String input) {
        LOGGER.info("sendKeys started");
        driver.waitForPageToLoad();
        try {
            element.clear();
            driver.waitForMilliseconds(Constants.TEN);
            driver.clickInputAndSendKeys(element, input);
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(element));
            driver.clearInputAndSendKeys(staleElement, input);
        } catch (InvalidElementStateException i) {
            driver.performSendKeysWithActions(element, input);
        } catch (WebDriverException w) {
            commandField.click();
            driver.clearInputAndSendKeys(element, input);
        }
        LOGGER.info("sendKeys completed");
    }

    /**
     * This method will double click and will handle the stale element reference exception and click the element again.
     * We have a double click method in driver class which also can be used but exception is not handled.
     *
     * @param element - WebElement which needs to be double clicked.
     */
    public void doubleClickStaleElement(WebElement element) {
        LOGGER.info("doubleClickStaleElement started");
        driver.waitForPageToLoad();
        try {
            driver.doubleClick(element);
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(element));
            driver.doubleClick(staleElement);
        }
        LOGGER.info("doubleClickStaleElement completed");
    }

    /**
     * CLicks the element
     *
     * @param element WebElement to be clicked
     */
    public void click(WebElement element) {
        LOGGER.info("click started");
        driver.waitForPageToLoad();
        try {
            driver.waitForElementClickable(element);
            element.click();
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(element));
            staleElement.click();
        } catch (WebDriverException w) {
            driver.clickWithActions(element);
        }
        LOGGER.info("click completed");
    }

    /**
     * This Method Verifies the text of the element with input.
     *
     * @param element    - WebElement
     * @param validation - text to verify
     */
    public void verifyTextAttribute(WebElement element, String validation) {
        LOGGER.info("verifyTextAttribute started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(element, Constants.ONE_HUNDRED_TWENTY);
        String text = element.getText();
        Assert.assertTrue("FAIL: The text found of : " + text + " did not match the expected validation of: " + validation + ".", text.contains(validation));
        LOGGER.info("verifyTextAttribute completed");
    }

    /**
     * This Method Verifies the text of the web element with input.
     *
     * @param element    - WebElement
     * @param validation - value to verify
     */
    public void verifyValueAttribute(WebElement element, String validation) {
        LOGGER.info("verifyValueAttribute started");
        driver.jsScrollToElement(element);
        driver.waitForElementVisible(element);
        String value = element.getAttribute(Constants.VALUE);
        Assert.assertTrue("FAIL: The value found of : " + value + " did not match the expected validation of: " + validation + ".", value.equalsIgnoreCase(validation));
        LOGGER.info("verifyValueAttribute completed");
    }

    /**
     * This Method Verifies if data saved from previous element is similar to the one being checked.
     *
     * @param element    - WebElement
     * @param validation - key used to retrieve stored value from element saved
     */
    public void verifySimilarDataFromTwoElements(WebElement element, String validation) {
        LOGGER.info("verifySimilarDataFromTwoElements started");
        driver.jsScrollToElement(element);
        driver.waitForElementVisible(element);
        String value = element.getAttribute(Constants.VALUE);
        String storedValue = driver.scenarioData.getData(validation);
        Assert.assertTrue("FAIL: The value found of : " + value +
                " did not match the expected validation of: " + storedValue + ".", value.equalsIgnoreCase(storedValue));
        LOGGER.info("Displayed Value = " + value + " and Stored Value = " + storedValue);
        LOGGER.info("verifySimilarDataFromTwoElements completed");
    }

    /**
     * This Method Verifies the title of the web element with input.
     *
     * @param element    - WebElement
     * @param validation - value to verify
     */
    public void verifyTitleAttribute(WebElement element, String validation) {
        LOGGER.info("verifyTitleAttribute started");
        driver.waitForElementVisible(element);
        String title = element.getAttribute(Constants.TITLE);
        Assert.assertTrue("FAIL: The title found of : " + title + " did not match the expected validation of: " + validation + ".", title.equalsIgnoreCase(validation));
        LOGGER.info("verifyTitleAttribute completed");
    }

    /**
     * This method can be used to click on element with exact text as input text.
     * avoid using this if you think the element text is not unique
     *
     * @param text - text of the element.
     */
    public void clickOnExactText(String text) {
        LOGGER.info("clickOnExactText started");
        driver.waitForPageToLoad();
        try {
            driver.waitForElementClickable(returnElementWithExactText(text));
            driver.clickWithActions(returnElementWithExactText(text));
        } catch (StaleElementReferenceException s) {
            driver.clickWithActions(returnElementWithExactText(text));
            returnElementWithExactText(text).click();
        } catch (WebDriverException we) {
            Actions actions = new Actions(webDriver);
            actions.click(returnElementWithExactText(text));
        }
        LOGGER.info("clickOnExactText completed");
    }

    /**
     * Method will return web element with exact text for any tag
     *
     * @param text - text of the element.
     * @return WebElement - element matching the xpath requirements
     */
    public WebElement returnElementWithExactText(String text) {
        LOGGER.info("returnElementWithExactText started");
        return webDriver.findElement(By.xpath(TEXT_XPATH + text + "']"));
    }

    /**
     * Method will return web element with exact text for any tag
     *
     * @param text - text of the element.
     * @return
     */
    public WebElement returnElementWithPartialText(String text) {
        LOGGER.info("returnElementWithPartialText started");
        return webDriver.findElement(By.xpath(PARTIAL_TEXT_XPATH + text + "')]"));
    }

    /**
     * Method will return web element with exact or partial value for any tag
     *
     * @param text - title attribute of the element.
     * @return
     */
    public WebElement returnElementWithPartialTitle(String text) {
        LOGGER.info("returnElementWithPartialTitle started");
        return webDriver.findElement(By.xpath(PARTIAL_TITLE_XPATH + text + "')]"));
    }

    /**
     * Method will return web element with exact or partial value for any tag
     *
     * @param text - value attribute of the element.
     * @return
     */
    public WebElement returnElementWithPartialValue(String text) {
        LOGGER.info("returnElementWithPartialValue started");
        return webDriver.findElement(By.xpath(PARTIAL_VALUE_XPATH + text + "')]"));
    }

    /**
     * Clicks on Element with partial value attribute
     *
     * @param value
     */
    public void clickOnElementWithPartialValueAttribute(String value) {
        LOGGER.info("clickOnElementWithPartialValueAttribute started");
        click(returnElementWithPartialValue(value));
        LOGGER.info("clickOnElementWithPartialValueAttribute started");
    }

    /**
     * This method can be used to double click on element with exact text as input text.
     * Avoid using this if you think the element text is not unique.
     *
     * @param text - text of the element.
     */
    public void doubleClickOnExactText(String text) {
        LOGGER.info("doubleClickOnExactText started");
        driver.waitForPageToLoad();
        Actions action = new Actions(webDriver);
        try {
            driver.waitForElementClickable(webDriver.findElement(By.xpath(TEXT_XPATH + text + "']")));
            action.moveToElement(webDriver.findElement(By.xpath(TEXT_XPATH + text + "']"))).doubleClick().perform();
        } catch (NoSuchElementException nse) {
            driver.waitForElementClickable(webDriver.findElement(By.xpath(VALUE_XPATH + text + "']")));
            driver.doubleClickControl(webDriver.findElement(By.xpath(VALUE_XPATH + text + "']")));
        }
        LOGGER.info("doubleClickOnExactText completed");
    }

    /**
     * This method can be used to double click on element with exact text as input text or value attribute.
     * avoid using this if you think the element text is not unique
     *
     * @param text - text of the element.
     */
    public void doubleClickValueOrTextAttribute(String text) {
        LOGGER.info("doubleClickValueOrTextAttribute started");
        driver.waitForPageToLoad();
        try {
            driver.waitForElementClickable(webDriver.findElement(By.xpath(TEXT_XPATH + text + "']")));
            driver.doubleClickControl(webDriver.findElement(By.xpath(TEXT_XPATH + text + "']")));
        } catch (NoSuchElementException nsee) {
            driver.doubleClickControl(webDriver.findElement(By.xpath(VALUE_XPATH + text + "']")));
        }
        LOGGER.info("doubleClickValueOrTextAttribute completed");
    }

    /**
     * Switches to the pop up window
     */
    public void switchToNewWindowHandle() {
        String mainHandle = webDriver.getWindowHandle();
        Set allHandles = webDriver.getWindowHandles();
        Iterator<String> iter = allHandles.iterator();
        while (iter.hasNext()) {
            String popupHandle = iter.next();
            if (!popupHandle.contains(mainHandle)) {
                webDriver.switchTo().window(popupHandle);
            }
        }
    }

    /**
     * Switches to the specific iframe declared as a Web Element
     *
     * @param element - Iframe Web Element
     */
    public void switchToSpecificiFrame(WebElement element) {
        LOGGER.info("switchToSelectDocPopupFrame started");
        driver.waitForPageToLoad();
        webDriver.switchTo().frame(element);
        LOGGER.info("switchToSelectDocPopupFrame completed");
    }

    /**
     * Deletes the browser cookies
     */
    public void deleteCookies() {
        LOGGER.info("deleteCookies started");
        webDriver.manage().deleteAllCookies();
        LOGGER.info("deleteCookies completed");
    }

    /**
     * To return boolean upon title attribute verification
     *
     * @param element  - Web Element
     * @param contains - input Text
     * @return - True if contains
     */
    public boolean titleAttributeContains(WebElement element, String contains) {
        LOGGER.info("titleAttributeContains started");
        driver.waitForElementVisible(element);
        if (element.getAttribute(Constants.TITLE).contains(contains)) {
            return true;
        }
        LOGGER.info("titleAttributeContains completed");
        return false;
    }

    /**
     * Saves the extracted Purchase Order number
     *
     * @param- Web Element
     */
    public void savePurchaseOrderNumber(WebElement element) {
        LOGGER.info("savePurchaseOrderNumber started");
        String purchaseOrderNumber = element.getText().replaceAll("[^0-9]", "");
        driver.scenarioData.setCurrentPurchaseOrderNumber(purchaseOrderNumber);
        if (purchaseOrderNumber.isEmpty()) {
            Assert.fail("Purchase Order Number is Empty");
        }
        LOGGER.info("savePurchaseOrderNumber Completed");
    }

    /**
     * Saves the document number from Value Attribute
     *
     * @param element
     */

    public void saveArticleDocumentNumberFromValueAttribute(WebElement element) {
        LOGGER.info("saveArticleDocumentNumberFromValueAttribute Started");
        String articleDocumentNumber = element.getAttribute(Constants.VALUE);
        driver.scenarioData.setArticleDocumentNumber(articleDocumentNumber);
        if (articleDocumentNumber.isEmpty()) {
            Assert.fail("Article document Number is Empty");
        }
        LOGGER.info("saveArticleDocumentNumberFromValueAttribute Completed");
    }

    /**
     * Gets the document number from Value Attribute stored in the method above
     *
     * @param element
     */

    public void getArticleDocumentNumberFromValueAttribute(WebElement element) {
        LOGGER.info("getArticleDocumentNumberFromValueAttribute started");
        sendKeys(element, driver.scenarioData.getCurrentArticleDocumentNumber());
        LOGGER.info("getArticleDocumentNumberFromValueAttribute completed");
    }

    /**
     * Enters the saved Purchase Order number
     *
     * @param- Web Element
     */
    public void getAndEnterPurchaseOrderNumber(WebElement element) {
        LOGGER.info("getAndEnterPurchaseOrderNumber started");
        driver.clearInputAndSendKeys(element, driver.scenarioData.getCurrentPurchaseOrderNumber());
        LOGGER.info("getAndEnterPurchaseOrderNumber completed");
    }

    /**
     * Switches the iFrame selection context with passed in WebElement for the Frame
     *
     * @param frame WebElement of iFrame to switch to
     */
    public void switchFrameContext(WebElement frame) {
        LOGGER.info("switchFrameContext started");
        driver.waitForPageToLoad();
        driver.switchFrameContext(frame);
        LOGGER.info("switchFrameContext completed");
    }

    /**
     * Switches the iFrame selection context with passed in Integer of frame in DOM
     *
     * @param frame integer value of frame in DOM
     */
    public void switchFrameContext(int frame) {
        LOGGER.info("switchFrameContext started");
        driver.waitForPageToLoad();
        driver.switchFrameContext(frame);
        LOGGER.info("switchFrameContext completed");
    }

    /**
     * Switches the Iframe Selection context with Passed in String Value matched to default, parent or other sting value
     *
     * @param context String value matched to context selection
     */
    public void switchFrameContext(String context) {
        LOGGER.info("switchFrameContext started");
        driver.waitForPageToLoad();
        driver.switchFrameContext(context);
        LOGGER.info("switchFrameContext Completed");
    }

    /**
     * This method can be used to click on element with partial text as input text.
     * Avoid using this if you think the element text is not unique.
     *
     * @param text - text of the element.
     */
    public void clickOnPartialText(String text) {
        LOGGER.info("clickOnPartialText started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(webDriver.findElement(By.xpath(PARTIAL_TEXT_XPATH + text + "')]")));
        click(webDriver.findElement(By.xpath(PARTIAL_TEXT_XPATH + text + "')]")));
        LOGGER.info("clickOnPartialText completed");
    }

    /**
     * This method can be used to click on element with partial text as input text.
     * Avoid using this if you think the element text is not unique.
     *
     * @param text - text of the element.
     */
    public void clickOnRequiredPartialText(String text) {
        LOGGER.info("clickOnRequiredPartialText started");
        driver.waitForPageToLoad();
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            driver.waitForElementClickable(webDriver.findElement(By.xpath(PARTIAL_VALUE_XPATH + text + "')]")));
            click(webDriver.findElement(By.xpath(PARTIAL_VALUE_XPATH + text + "')]")));
        } else {
            clickOnPartialText(text);
        }
        LOGGER.info("clickOnRequiredPartialText completed");
    }

    /**
     * Waits for the element, when the text attribute of the element is passed.
     *
     * @param text - text attribute of the element
     */
    public void waitForElementWithText(String text) {
        LOGGER.info("waitForElementWithText started");
        driver.waitForElementVisibleByLocator(By.xpath(TEXT_XPATH + text + "']"), Constants.FIVE_HUNDRED);
        LOGGER.info("waitForElementWithText completed");
    }

    /**
     * Asserts element with text
     *
     * @param text - text attribute of the element
     */
    public void assertElementDisplayedWithText(String text) {
        LOGGER.info("assertElementDisplayedWithText started");
        driver.waitForPageToLoad();
        try {
            Assert.assertTrue("FAIL: Element not found with text: " + text,
                    driver.isElementDisplayed(returnElementWithPartialText(text)));
        } catch (NoSuchElementException ne) {
            Assert.assertTrue("FAIL: Element not found with value: " + text,
                    driver.isElementDisplayed(returnElementWithPartialValue(text)));
        } catch (AssertionError ae) {
            Assert.assertTrue("FAIL: Element not found with value: " + text,
                    driver.isElementDisplayed(returnElementWithPartialValue(text)));
        }
        LOGGER.info("assertElementDisplayedWithText completed");
    }

    /**
     * Asserts element with value
     *
     * @param value - value attribute of the element
     */
    public void assertElementDisplayedWithValueAttribute(String value) {
        LOGGER.info("assertElementDisplayedWithValueAttribute started");
        driver.waitForPageToLoad();
        try {
            Assert.assertTrue("FAIL: Element not found with value: " + value,
                    driver.isElementDisplayed(returnElementWithPartialValue(value)));
        } catch (NoSuchElementException ne) {
            Assert.assertTrue("FAIL: Element not found with text: " + value,
                    driver.isElementDisplayed(returnElementWithPartialText(value)));
        } catch (AssertionError ae) {
            Assert.assertTrue("FAIL: Element not found with text: " + value,
                    driver.isElementDisplayed(returnElementWithPartialText(value)));
        }
        LOGGER.info("assertElementDisplayedWithValueAttribute completed");
    }

    /**
     * Asserts element with title
     *
     * @param title - title attribute of the element
     */
    public void assertElementDisplayedWithTitleAttribute(String title) {
        LOGGER.info("assertElementDisplayedWithTitleAttribute started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: Element not found with title: " + title,
                driver.isElementDisplayed(returnElementWithPartialTitle(title)));
        LOGGER.info("assertElementDisplayedWithTitleAttribute completed");
    }

    /**
     * verifies the OK status when a transaction is done in SAP
     */
    public void verifySucessStatusInSap() {
        LOGGER.info("verifySucessStatusInSap started");
        driver.isElementDisplayed(successInSap);
        LOGGER.info("verifySucessStatusInSap completed");
    }

    /**
     * verifies the error status when a transaction is done in SAP
     */
    public void verifyErrorStatusInSap() {
        LOGGER.info("verifyErrorStatusInSap started");
        driver.isElementDisplayed(errorInSap);
        LOGGER.info("verifyErrorStatusInSap completed");
    }

    /**
     * This method will extract the locator from an element and returns as By object
     *
     * @param element - WebElement
     * @return - By object
     */
    public By extractByLocatorFromWebElement(WebElement element) {
        LOGGER.info("extractByLocatorFromWebElement started");
        String elementContent = element.toString();
        if (elementContent.toLowerCase().contains("by.id")) {
            String idOfElement = elementContent.substring(elementContent.indexOf("By.id:") + 6).replaceAll(" ", "").replaceAll("'", "");
            return By.id(idOfElement);
        } else if (elementContent.toLowerCase().contains("xpath")) {
            String uncleanXpath = elementContent.substring(elementContent.indexOf("xpath:") + 6).replaceAll(" ", "");
            String cleanXpath = uncleanXpath.substring(0, uncleanXpath.length() - 1);
            return By.xpath(cleanXpath);
        } else if (elementContent.toLowerCase().contains(TOOLBAROKCODE)) {
            return tCodeForUpgradedCaq;
        }
        LOGGER.info("extractByLocatorFromWebElement completed");
        return null;
    }

    /**
     * This method can be used to click on element with value attribute same as input text.
     * avoid using this if you think the element's value attribute is not unique
     *
     * @param valueAttributeValue - text of the element's value attribute.
     */
    public void clickOnExactValueAttribute(String valueAttributeValue) {
        LOGGER.info("clickOnExactValueAttribute started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(webDriver.findElement(By.xpath(VALUE_XPATH + valueAttributeValue + "')]")));
        webDriver.findElement(By.xpath(VALUE_XPATH + valueAttributeValue + "')]")).click();
        LOGGER.info("clickOnExactValueAttribute completed");
    }

    /**
     * This method can be used to double click on element  with value attribute same as input text.
     * avoid using this if you think the element's value attribute is not unique
     *
     * @param valueAttributeValue - text of the element's value attribute.
     */
    public void doubleClickOnExactValueAttribute(String valueAttributeValue) {
        LOGGER.info("doubleClickOnExactValueAttribute started");
        driver.waitForPageToLoad();
        try {
            driver.waitForElementClickable(webDriver.findElement(By.xpath(VALUE_XPATH + valueAttributeValue + "')]")));
            driver.doubleClickControl(webDriver.findElement(By.xpath(VALUE_XPATH + valueAttributeValue + "')]")));
        } catch (StaleElementReferenceException s) {
            WebElement element = webDriver.findElement(By.xpath(VALUE_XPATH + valueAttributeValue + "')]"));
            driver.doubleClickControl(element);
        }
        LOGGER.info("doubleClickOnExactValueAttribute completed");
    }

    /**
     * Method verifies the hybris Order is the same order which was processed by legacy
     *
     * @throws Exception
     */
    public void verifyWebOrderReceivedIsSameAsOrderProcessedFromLegacy(String sheetNameSap, String sheetNameWeb) throws Exception {
        LOGGER.info("verifyWebOrderReceivedIsSameAsorderProcessedFromlegacy started");
        driver.scenarioData.setData("Web Order Processed", driver.readFromExcelAndReturnSingleValue(Constants.
                fullPathFileNames.get(Constants.INVOICE_SAP), sheetNameSap, 2, 12));
        driver.scenarioData.setData("Web Order Received", driver.readFromExcelAndReturnSingleValue(Constants.
                fullPathFileNames.get(Constants.WEBORDERS_LEGACY_STOREPOS_EXCEL), sheetNameWeb, 2, 1));
        Assert.assertTrue("FAIL: Web Order is not same as invoice processed for",
                driver.scenarioData.getData("Web Order Processed").equalsIgnoreCase(
                        driver.scenarioData.getData("Web Order Received")));
        LOGGER.info("verifyWebOrderReceivedIsSameAsorderProcessedFromlegacy completed");
    }

    /**
     * Selects value from dropdown
     *
     * @param input   - Input text
     * @param element - WebELement
     */
    public void selectsValueFromDropDown(String input, WebElement element) {
        LOGGER.info("selectsValueFromDropDown started");
        driver.waitForPageToLoad();
        driver.clickInputAndSendKeys(element, input);
        LOGGER.info("selectsValueFromDropDown completed");
    }

    /**
     * This method can be used to double click on element.
     *
     * @param element - text of the element.
     */
    public void doubleClickOnElement(WebElement element) {
        LOGGER.info("doubleClickOnElement started");
        driver.waitForPageToLoad();
        try {
            driver.waitForElementClickable(element);
            driver.doubleClickControl(element);
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(element));
            driver.doubleClickControl(staleElement);
        }
        LOGGER.info("doubleClickOnElement completed");
    }

    /**
     * This method can be used to create a unique value
     *
     * @param value - value to return
     */
    public String createUniqueValue(String value) {
        LOGGER.info("createUniqueValue started");
        String valueName = "";
        if (value.equalsIgnoreCase(Constants.EMPLOYEE_AUTOMATION)) {
            valueName = Constants.EMPLOYEE_AUTOMATION + CommonUtils.getRandomNumber(Constants.ONE,
                    Constants.THREE_THOUSAND);
        } else if (value.equalsIgnoreCase(Constants.AR_AUTOMATION)) {
            valueName = Constants.AR_AUTOMATION + CommonUtils.getRandomNumber(Constants.ONE, Constants.THREE_THOUSAND);
        } else {
            valueName = Integer.toString(CommonUtils.getRandomNumber(Constants.ONE, Constants.THREE_THOUSAND));
            driver.scenarioData.setCurrentEmpID(valueName);
        }

        LOGGER.info("createUniqueValue completed");
        return valueName;
    }

    /**
     * Saves the extracted Employee ID
     *
     * @param- Web Element element of the field to get the emp ID from
     */
    public void saveCurrentEmpID(WebElement element) {
        LOGGER.info("saveCurrentEmpID started");
        String empID = element.getAttribute(Constants.VALUE);
        driver.scenarioData.setCurrentEmpID(empID);
        if (empID.isEmpty()) {
            Assert.fail("Emp ID Number is Empty");
        }
        LOGGER.info("saveCurrentEmpID Completed");
    }

    /**
     * Takes a WebElement to click and  clear's input in SAP .
     *
     * @param element WebElement to interact with.
     */
    public void clearInput(WebElement element) {
        LOGGER.info("clearInput started");
        driver.waitForElementClickable(element);
        element.click();
        element.clear();
        LOGGER.info("clearInput completed");
    }

    /**
     * This Method Verifies class the web element with input.
     *
     * @param element    - WebElement
     * @param validation - value to verify
     */
    public void verifyClassAttribute(WebElement element, String validation) {
        LOGGER.info("verifyClassAttribute started");
        driver.waitForElementVisible(element);
        String value = element.getAttribute(Constants.CLASS);
        Assert.assertTrue("FAIL: class found of : " + value + " did not match the expected validation of: " + validation + ".", value.contains(validation));
        LOGGER.info("verifyClassAttribute completed");
    }

    /**
     * This Method Verifies the element displays
     *
     * @param element - WebElement to be displayed
     */
    public void verifyElementDisplays(WebElement element) {
        LOGGER.info("verifyElementDisplays started");
        Assert.assertTrue("FAIL: Yellow Triangle not present", element.isDisplayed());
        LOGGER.info("verifyElementDisplays completed");
    }

    public String returnDataFromEnvironmentVariables(String dataType) {
        LOGGER.info("returnDataFromEnvironmentVariables started");
        switch (dataType) {
            case ADDRESS_LINE_1:
                return Config.getAddressLine1();
            case CITY:
                return Config.getCity();
            case STATE:
                return Config.getState();
            case ZIP:
                return Config.getZip();
            case SCREENSHOT_FOLDER:
                return Config.getScreenshotFolder();
            case SHEET_NAME:
                try {
                    return Config.getExcelSheetName();
                } catch (NullPointerException n) {
                    return Config.getStateCode().toLowerCase();
                }
            case MOVEMENT_TYPE:
                return Config.getMovementType();
            case ENVIRONMENT_FEE:
                return Config.getEnvironmentFeeGlAccount();
            case SALES_TAX:
                return Config.getSalesTaxGlAccount();
            case LAST_NAME:
                return Config.getLastName();
            case LEGACY_LAYWAY_AMOUNT:
                return Config.getLegacyLayawayAmount();
            case DISTRIBUTION_CENTER:
                return Config.getDistributionCenter();
            case TRANSACTION_TYPE:
                return Config.getTransactionType();
            case "geoState":
                return Config.getStateCode();
            default:
                Assert.fail("FAIL: Could not find data that matched string passed from step");
                return null;
        }
    }

    /**
     * This method can be used to double click on element with partial text as input text.
     * avoid using this if you think the element text is not unique
     *
     * @param text - text of the element.
     */
    public void doubleClickOnPartialText(String text) {
        LOGGER.info("doubleClickOnPartialText started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(webDriver.findElement(By.xpath(PARTIAL_TEXT_XPATH + text + "')]")));
        driver.doubleClickControl(webDriver.findElement(By.xpath(PARTIAL_TEXT_XPATH + text + "')]")));
        LOGGER.info("doubleClickOnPartialText completed");
    }

    /**
     * Verify the specified file is valid
     *
     * @param fileAlias Text string representing a file name
     * @return file        File path and file name
     */
    public String getFullPathFileNameSap(String fileAlias) {
        LOGGER.info("getFullPathFileName started for file text string '" + fileAlias + "'");
        String file = Constants.fullPathFileNames.get(fileAlias);

        if (file == null) {
            Assert.fail("FAIL: Invalid file, '" + fileAlias + "'");
        }
        LOGGER.info("getFullPathFileName completed for '" + file + "'");
        return file;
    }

    /**
     * Clicks on PDF page, selects and copies all content
     */
    public void selectAndCopyPdfContent() throws Throwable {
        LOGGER.info("selectAndCopyPdfContent started");
        Actions actionObj = new Actions(webDriver);
        Robot robot = new Robot();
        robot.mouseMove(600, 250);
        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
        Thread.sleep(Constants.TWO_THOUSAND);
        actionObj.keyDown(Keys.CONTROL)
                .sendKeys(Keys.chord("a"))
                .perform();
        Thread.sleep(Constants.ONE_THOUSAND);
        actionObj.keyDown(Keys.CONTROL)
                .sendKeys(Keys.chord("c"))
                .perform();
        actionObj.keyUp(Keys.CONTROL).perform();
        Thread.sleep(Constants.TWO_THOUSAND);
        LOGGER.info("selectAndCopyPdfContent completed");
    }

    /**
     * Checks if the System ClipBoard Contains the input String
     *
     * @param reqTextInPdf Input to validate
     */
    public void verifyPdfContent(String reqTextInPdf) {
        LOGGER.info("VerifyPdfContent started");
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        try {
            String data = (String) clipboard.getData(DataFlavor.stringFlavor);
            Assert.assertTrue("FAIL: The data you copied is: " + data.toLowerCase() +
                    " And the text being validated is: " +
                    reqTextInPdf, data.toLowerCase().contains(reqTextInPdf.toLowerCase()));
        } catch (HeadlessException e) {
            e.printStackTrace();
        } catch (UnsupportedFlavorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.info("VerifyPdfContent completed");
    }

    /**
     * This method clicks on text using span tag by element
     *
     * @param text to click on
     */
    public void clickOnText(String text) {
        LOGGER.info("clickOnText started");
        WebElement myEle = driver.getElementWithText(spanTagBy, text);

        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        myEle.click();
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        LOGGER.info("clickOnText completed");

    }

    /**
     * This method can be used to click on element with exact text as input text.
     * avoid using this if you think the element text is not unique
     *
     * @param text    - text of the element.
     * @param element - element to find
     */
    public void clickAndSelectOnExactText(String text, WebElement element) {
        LOGGER.info("clickAndSelectOnExactText started");
        WebElement costumerPriceListFound = webDriver.findElement(customerPriceListBy);
        costumerPriceListFound.click();
        LOGGER.info("clickAndSelectOnExactText completed");
    }

    /**
     * Sends input and click on the exact text from dropdown
     *
     * @param element         - WebELement
     * @param input           - Input text
     * @param dropDownElement - element of the dropdown to click on
     */
    public void sendKeysDropDown(WebElement element, String input, WebElement dropDownElement) {
        LOGGER.info("sendKeysDropDown started");
        driver.waitForPageToLoad();

        try {
            driver.clearInputAndSendKeys(element, input);
            element.click();
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(element));
            staleElement.click();
        }
        try {
            dropDownElement.click();
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(dropDownElement));
            staleElement.click();
        }
        LOGGER.info("sendKeysDropDown completed");
    }

    /**
     * Verifies element value is not empty
     *
     * @param elementName The element to check
     */
    public void assertElementTextPopulated(WebElement elementName) {
        LOGGER.info("assertElementTextPopulated started");
        driver.waitForElementVisible(elementName);
        driver.jsScrollToElement(elementName);
        try {
            Assert.assertTrue("FAIL: Element text was empty.", !elementName.getAttribute(Constants.VALUE).isEmpty());
        } catch (AssertionError ae) {
            Assert.assertTrue("FAIL: Element text was empty.", !elementName.getText().isEmpty());
        }
        LOGGER.info("assertElementTextPopulated completed");
    }

    /**
     * This method will locate the input field with the field's name and will send keys into the input field
     *
     * @param labelName - Name of the input field
     * @param value     - Input value
     */
    public void sendKeysWithLabelName(String labelName, String value) {
        LOGGER.info("sendKeysWithLabelName started");
        sendKeys(returnInputFieldWithLabel(labelName), value);
        LOGGER.info("sendKeysWithLabelName completed");
    }

    /**
     * This method will return the input field with the field's name
     *
     * @param labelName - Name of the input field
     * @return input WebElement
     */
    public WebElement returnElementWithLabelName(String labelName) {
        LOGGER.info("returnElementWithLabelName started");
        return webDriver.findElement(By.xpath("//*[text()='" + labelName + "']/following::input[1]"));
    }

    /**
     * This method will locate the input field with the field's name and will send keys into the input field
     *
     * @param labelName      - Name of the input field
     * @param inputBoxNumber - Position of input box
     * @param value          - Input value
     */
    public void sendKeysWithLabelNameForMultipleInputFields(String labelName, String inputBoxNumber, String value) {
        LOGGER.info("sendKeysWithLabelNameForMultipleInputFields started");
        sendKeys(returnElementWithLabelNameAndPosition(labelName, inputBoxNumber), value);
        LOGGER.info("sendKeysWithLabelNameForMultipleInputFields completed");
    }

    /**
     * This method will return the input field with the field's name
     *
     * @param labelName - Name of the input field
     * @param position  - position of the input field
     * @return input WebElement
     */
    public WebElement returnElementWithLabelNameAndPosition(String labelName, String position) {
        LOGGER.info("returnElementWithLabelNameAndPosition started");
        return webDriver.findElement(By.xpath("//*[text()='" + labelName + "']/following::input[" + position + "]"));
    }

    /**
     * This method will locate the input field with the field's name and will send keys into the input field at a given
     * position
     *
     * @param labelName - Name of the input field
     * @param value     - Input value
     * @param position  - position of the input field among input fields with similar label names
     */
    public void sendKeysWithLabelName(String labelName, String value, int position) {
        LOGGER.info("sendKeysWithLabelName started for element at position " + position);
        sendKeys(returnInputFieldWithLabel(labelName, position), value);
        LOGGER.info("sendKeysWithLabelName completed for element at position " + position);
    }


    /**
     * Logs in to SAP using system, client, UN and PW
     */
    public void login() {
        LOGGER.info("login started");
        driver.waitForElementVisible(sapUser);
        sapUser.sendKeys(Config.getSapUserName());
        sapPassword.sendKeys(Config.getSapPassword());
        driver.waitForElementClickable(submitButton);
        click(submitButton);
        if (driver.isElementDisplayed(logonAttemptsText)) {
            if (logonAttemptsText.getText().contains(Constants.FAILED_LOGON_MESSAGE))
                clickOnExactText(Constants.CONTINUE);
        }
        if (webDriver.getTitle().equalsIgnoreCase(Constants.LOGON_STATUS_CHECK)) {
            if (deleteSession.isEnabled() == false) {
                deleteSession.click();
            }
            continueButton.click();
        }
        if (driver.isElementDisplayed(nwbcSessionsMessage)) {
            if (!cancelSession) {
                click(cancelAllLoginsNwbc);
            }
            clickOnExactText(CONTINUE);
        }
        driver.waitForPageToLoad();
        LOGGER.info("login completed");
    }

    /**
     * Logs in to SAP using system, client, UN and PW
     */
    public void login(String module) throws Throwable {
        LOGGER.info("login started for module: " + module);
        login(module, Config.getSapUserName(), Config.getDataSet());
        LOGGER.info("login completed for module: " + module);
    }

    /**
     * Logs in to SAP using system, client, UN and PW
     */
    public void login(String module, String username, String dataSet) throws Throwable {
        LOGGER.info("login started for Module: " + module + ", User name: " + username + ", Data set: " + dataSet);
        if (module.equalsIgnoreCase(Constants.NWBC)) {
            module = Constants.SAP_ECC;
        }
        driver.waitForElementVisible(sapUser);
        sendKeys(sapUser, username);
        driver.waitForMilliseconds();
        sendKeys(sapPassword, returnPassword(username, module.toUpperCase(), dataSet));
        driver.scenarioData.setData(SAP_PASSWORD_KEY, returnPassword(username, module.toUpperCase(), dataSet));
        driver.waitForElementClickable(submitButton);
        click(submitButton);
        if (driver.isElementDisplayed(logonAttemptsText)) {
            if (logonAttemptsText.getText().contains(Constants.FAILED_LOGON_MESSAGE))
                clickOnExactText(Constants.CONTINUE);
        }
        if (webDriver.getTitle().equalsIgnoreCase(Constants.LOGON_STATUS_CHECK) && !Config.getDataSet().equalsIgnoreCase(Constants.DEV)) {
            if (deleteSession.isEnabled() == false) {
                deleteSession.click();
            }
            clickOnExactText(Constants.CONTINUE);
        }
        if (driver.isElementDisplayed(nwbcSessionsMessage)) {
            if (!cancelSession) {
                click(cancelAllLoginsNwbc);
            }
            clickOnExactText(CONTINUE);
        }
        driver.waitForPageToLoad();
        LOGGER.info("login completed for Module: " + module + ", User name: " + username + ", Data set: " + dataSet);
    }

    /**
     * Enters t-code into the Command Field
     *
     * @param tCode Transaction Code to search for
     */
    public void enterTCode(String tCode) {
        LOGGER.info("enterTCode started");
        if (webDriver.getCurrentUrl().contains(UPGRADED_CAR_URL)) {
            commandFieldBy = tCodeForUpgradedCaq;
        }
        WebElement element = webDriver.findElement(commandFieldBy);
        sendKeys(element, tCode);
        commandField.sendKeys(Keys.ENTER);
        LOGGER.info("enterTCode completed");
    }

    /**
     * Waits for the element, when the text attribute of the element is passed.
     *
     * @param text          - text attribute of the element
     * @param timeInSeconds - Time to wait in seconds
     */
    public void waitForElementWithText(String text, int timeInSeconds) {
        LOGGER.info("waitForElementWithText started");
        driver.waitForElementVisibleByLocator(By.xpath(TEXT_XPATH + text + "']"), timeInSeconds);
        LOGGER.info("waitForElementWithText completed");
    }

    /**
     * Waits for the element, when the text attribute of the element is passed.
     *
     * @param text          - text attribute of the element
     * @param timeInSeconds - Time to wait in seconds
     */
    public void waitForElementWithPartialText(String text, int timeInSeconds) {
        LOGGER.info("waitForElementWithText started");
        driver.waitForElementVisibleByLocator(By.xpath(PARTIAL_TEXT_XPATH + text + "')]"), timeInSeconds);
        LOGGER.info("waitForElementWithText completed");
    }

    /**
     * Waits for the iframe with information and check ok button
     */
    public void waitForIframeToAppear() {
        LOGGER.info("waitForIframeToAppear started");
        driver.waitForElementVisible(iframeWithCheckOk, Constants.ONE_HUNDRED_TWENTY);
        LOGGER.info("waitForIframeToAppear completed");
    }

    /**
     * Method to perform KeyUp and KeyDown
     *
     * @param keys     - Keys.Object
     * @param downOrUp - Key down or key up
     */
    public void performHoldAndReleaseKeyAction(Keys keys, String downOrUp) {
        Actions actions = new Actions(webDriver);
        if (downOrUp.equalsIgnoreCase(DOWN))
            actions.keyDown(keys).build().perform();
        else if (downOrUp.equalsIgnoreCase(UP))
            actions.keyUp(keys).build().perform();
        else
            Assert.fail("Please mention 'up' or 'down'");
    }

    /**
     * This method returns the web element of the input field with label name passed
     *
     * @param labelName - Name of the input field
     */
    public WebElement returnInputFieldWithLabel(String labelName) {
        return webDriver.findElement(By.xpath("//*[text()='" + labelName + "']/following::input[1]"));
    }

    /**
     * This method returns the web element of the input field and position with label name passed
     *
     * @param labelName - Name of the input field
     * @param position  -  position of the input field among input fields with similar label names
     */
    public WebElement returnInputFieldWithLabel(String labelName, int position) {
        return webDriver.findElements(By.xpath("//*[text()='" + labelName + "']/following::input[1]")).get(position - 1);
    }

    /**
     * This method will locate the input field with the field's name and will send keys todya's date into the input field
     *
     * @param labelName - Name of the input field
     */
    public void sendKeysTodaysDateWithLabelName(String labelName) {
        LOGGER.info("sendKeysTodaysDateWithLabelName started");
        String todayDate = driver.getTodayDate("yyyy/MM/dd").replace("/", "");
        sendKeysWithLabelName(labelName, todayDate);
        LOGGER.info("sendKeysTodaysDateWithLabelName completed");
    }

    /**
     * This method saves time before generating the file after payment process
     *
     * @param timeStamp saves the time before/after creating a file
     */
    public void savesTimeStamp(String timeStamp) {
        LOGGER.info("saveTimeBeforeCreatingTheFile started");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.HOURS_MINUTES_SECONDS);
        driver.scenarioData.genericData.put("Time stamp " + timeStamp + " creating a file", sdf.format(cal.getTime()));
        LOGGER.info("saveTimeBeforeCreatingTheFile completed");
    }

    /**
     * accept alert message.
     */
    public void acceptTheAlert() {
        LOGGER.info("acceptTheAlert started");
        webDriver.switchTo().alert().accept();
        LOGGER.info("acceptTheAlert completed");
    }

    /**
     * Method clicks on element with title specified
     *
     * @param elementTitle Element title
     */
    public void selectElementWithTitle(String elementTitle) {
        LOGGER.info("selectElementWithTitle started");
        driver.waitForPageToLoad();
        click(webDriver.findElement(By.xpath(TITLE_XPATH + elementTitle + "')]")));
        LOGGER.info("selectElementWithTitle completed");
    }

    /**
     * Double click element with title attribute
     *
     * @param elementTitle - Title attribute of the element
     */
    public void doubleClickElementWithTitle(String elementTitle) {
        LOGGER.info("selectElementWithTitle started");
        driver.waitForPageToLoad();
        doubleClickOnElement(webDriver.findElement(By.xpath(TITLE_XPATH + elementTitle + "')]")));
        LOGGER.info("selectElementWithTitle completed");
    }

    /**
     * Method enters PO in element with title specified
     *
     * @param elementTitle Element title
     */
    public void enterPOInElementWithTitle(String elementTitle) {
        LOGGER.info("enterPOInElementWithTitle started");
        driver.waitForPageToLoad();
        sendKeys(webDriver.findElement(By.xpath(TITLE_XPATH + elementTitle + "')]")), driver.scenarioData.getCurrentPurchaseOrderNumber());
        LOGGER.info("enterPOInElementWithTitle completed");
    }

    /**
     * Method refresh current browser.
     */
    public void refreshBrowser() {
        LOGGER.info("refreshBrowser started");
        webDriver.navigate().refresh();
        LOGGER.info("refreshBrowser completed");
    }

    /**
     * Method to return Time Zone based on state
     *
     * @param state - State
     * @return - String time zone
     */
    public String returnTimeZoneBasedOnState(String state) {
        //Todo: More States will be added as dt sales order integration scenarios grow
        LOGGER.info("returnTimeZoneBasedOnState started");
        switch (state) {
            case Constants.STATE_TX:
            case Constants.STATE_CO:
                return Constants.TIME_ZONE_CST;
            case Constants.STATE_AZ:
            case Constants.DTD:
                return Constants.TIME_ZONE_MST;
            default:
                Assert.fail("Time zone for: " + state + " is not configured.");
                return null;
        }
    }

    /**
     * Enters the saved PO in the field with specified label name
     *
     * @param labelName - Name of the input field
     */
    public void enterSavedPONumberFromScenarioDataWithLabelName(String labelName) {
        LOGGER.info("enterSavedPONumberFromScenarioDataWithLabelName started");
        sendKeysWithLabelName(labelName, driver.scenarioData.getCurrentPurchaseOrderNumber());
        LOGGER.info("enterSavedPONumberFromScenarioDataWithLabelName completed");
    }

    /**
     * This method will return the past/present/future date in the format MM/DD/YYYY
     *
     * @param format - send the format desired. ex: YYYY/MM/DD or MM/DD/YYYY
     * @param months - include number of months needed to be added or subtracted.
     * @param tense  - phase of date to be entered(past|present|future)
     * @return returns a String date in desired format.
     */
    public String getDate(String format, int months, String tense) {
        LOGGER.info("getDate started");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        LocalDate localDate = LocalDate.now();
        String actualDate = "";
        if (tense.equalsIgnoreCase(Constants.PRESENT)) {
            actualDate = dateTimeFormatter.format(localDate);
        } else if (tense.equalsIgnoreCase(Constants.PAST)) {
            actualDate = dateTimeFormatter.format(localDate.now().minusMonths(months));
        } else if (tense.equalsIgnoreCase(Constants.FUTURE)) {
            actualDate = dateTimeFormatter.format(localDate.now().plusMonths(months));
        }
        LOGGER.info("getDate completed");
        return actualDate;
    }

    /**
     * Grabs purchase order number after PO creation from the alert banner displayed
     *
     * @return String purchase order number
     */
    public String returnPurchaseOrderNumber() {
        LOGGER.info("returnPurchaseOrderNumber started");
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        String purchaseOrderAlertText = purchaseOrderNumberAlertContainer.getText();
        String cleanPurchaseOrderNumber = purchaseOrderAlertText.replaceAll("\\D+", "").trim();
        LOGGER.info("returnPurchaseOrderNumber completed");
        return cleanPurchaseOrderNumber;
    }

    /**
     * enters values into the items table in any PO related page
     *
     * @param value     - value to enter
     * @param rowNumber - row number for entry
     */
    public void enterValuesIntoItemTable(String value, String rowNumber) {
        LOGGER.info("enterValuesIntoItemTable started");
        driver.waitForPageToLoad();
        By by = By.xpath("//*[contains(@id,'[" + rowNumber + ",2]_c')][not (contains(@id,'[" + rowNumber + ",2]_c-r'))]");
        WebElement inputField = webDriver.findElement(by);
        try {
            driver.performSendKeysWithActions(inputField, value);
            driver.performKeyAction(Keys.ENTER);
        } catch (StaleElementReferenceException s) {
            WebElement staleInputField = webDriver.findElement(by);
            driver.performSendKeysWithActions(staleInputField, value);
            driver.performKeyAction(Keys.ENTER);
        }
        LOGGER.info("enterValuesIntoItemTable completed");
    }

    /**
     * enters values into the items table in any PO related page
     *
     * @param value     - value to enter
     * @param rowNumber - row number for entry
     */
    public void selectDeselectFields(String value, String rowNumber) {
        LOGGER.info("selectDeselectFields started");
        By by = By.xpath("//span[contains(@id,'" + rowNumber + ",2#icp')]");
        WebElement element = webDriver.findElement(by);
        try {
            driver.waitForPageToLoad();
            element.click();
        } catch (StaleElementReferenceException s) {
            WebElement staleInputField = webDriver.findElement(by);
            staleInputField.click();
        }
        LOGGER.info("selectDeselectFields completed");
    }

    /**
     * method will return web element related to a specific column name and row number
     *
     * @param columnName - Name of the column in NWBC
     * @param rowNumber  - Row number
     * @return input WebElement Field
     */
    private WebElement inputElementItemsTableInNwbc(String columnName, int rowNumber) {
        LOGGER.info("inputElementItemsTableInNwbc started");
        WebElement inputField = null;
        List<WebElement> itemsTableColumnHeadings = webDriver.findElements(itemsTableColumnHeadingsBy);
        for (int i = 1; i < itemsTableColumnHeadings.size(); i++) {
            if (itemsTableColumnHeadings.get(i).getText().toLowerCase().contains(columnName.toLowerCase())) {

                webDriver.findElements(By.xpath("//tbody[contains(@id,'contentTBody')]/child::tr[" + (rowNumber + 1) + "]/child::td")).size();
                inputField = webDriver.findElements(By.xpath("//tbody[contains(@id,'contentTBody')]/child::tr[" + (rowNumber + 1) + "]/child::td")).get(i);
                driver.waitForMilliseconds();
                break;
            }
        }
        LOGGER.info("inputElementItemsTableInNwbc completed");
        return inputField;
    }

    /**
     * method will return web element related to a specific column name and row number
     *
     * @param columnName - Name of the column in NWBC
     * @param rowNumber  - Row number
     * @return input WebElement Field
     */
    private WebElement returnAssertElementItemsTableInNwbc(String columnName, int rowNumber) {
        LOGGER.info("inputElementItemsTableInNwbc started");
        WebElement inputField = null;
        List<WebElement> itemsTableColumnHeadings = webDriver.findElements(itemsTableColumnHeadingsBy);
        for (int i = 1; i < itemsTableColumnHeadings.size(); i++) {
            if (itemsTableColumnHeadings.get(i).getText().toLowerCase().contains(columnName.toLowerCase())) {

                webDriver.findElements(By.xpath("//tbody[contains(@id,'contentTBody')]/child::tr[" + (rowNumber + 1) + "]/child::td")).size();
                inputField = webDriver.findElements(By.xpath("//tbody[contains(@id,'contentTBody')]/child::tr[" + (rowNumber + 1) + "]/child::td//following::input")).get(i - 1);
                driver.waitForMilliseconds();
                break;
            }
        }
        LOGGER.info("inputElementItemsTableInNwbc completed");
        return inputField;
    }

    /**
     * method to input the value into the web element into the table in NWBC
     *
     * @param columnName - Name of the column
     * @param rowNumber  - Row number
     * @param value      - value to enter
     */
    public void sendKeysIntoInputFieldsInItemsTableInNwbc(String columnName, int rowNumber, String value) {
        LOGGER.info("sendKeysIntoInputFieldsInItemsTableInNwbc started");
        try {
            driver.performSendKeysWithActions(inputElementItemsTableInNwbc(columnName, rowNumber), value);
        } catch (StaleElementReferenceException s) {
            driver.performSendKeysWithActions(inputElementItemsTableInNwbc(columnName, rowNumber), value);
        }
        LOGGER.info("sendKeysIntoInputFieldsInItemsTableInNwbc completed");
    }

    /**
     * method to input the previously saved value into the web element into the table in NWBC
     *
     * @param columnName - Name of the column
     * @param rowNumber  - Row number
     * @param value      - key to retirve value
     */
    public void sendSavedKeysIntoInputFieldsInItemsTableInNwbc(String columnName, int rowNumber, String value) {
        LOGGER.info("sendSavedKeysIntoInputFieldsInItemsTableInNwbc started");
        try {
            driver.performSendKeysWithActions(inputElementItemsTableInNwbc(columnName, rowNumber), driver.scenarioData.getData(value));
        } catch (StaleElementReferenceException s) {
            driver.performSendKeysWithActions(inputElementItemsTableInNwbc(columnName, rowNumber), driver.scenarioData.getData(value));
        }
        LOGGER.info("sendSavedKeysIntoInputFieldsInItemsTableInNwbc completed");
    }

    /**
     * method to input the value into the web element into the table in NWBC
     *
     * @param columnName - Name of the column
     * @param rowNumber  - Row number
     * @param value      - value to enter
     */
    public void assertTextForInputFieldsInItemsTableInNwbc(String columnName, int rowNumber, String value) {
        LOGGER.info("assertTextForInputFieldsInItemsTableInNwbc started");
        try {
            driver.waitForMilliseconds();
            driver.assertElementAttributeString(returnAssertElementItemsTableInNwbc(columnName, rowNumber), Constants.VALUE, value);
        } catch (StaleElementReferenceException s) {
            driver.assertElementAttributeString(returnAssertElementItemsTableInNwbc(columnName, rowNumber), Constants.VALUE, value);
        }
        LOGGER.info("assertTextForInputFieldsInItemsTableInNwbc completed");
    }

    public String returnDataFromExcel(String columnName) throws Throwable {
        return driver.readFromExcelAndReturnSingleValue(Constants.SAP_DATA_SOURCE_LOCATION +
                        driver.scenarioData.getData(FOLDER_NAME) + "\\" +
                        driver.scenarioData.getData(EXCEL_FILE_NAME),
                driver.scenarioData.getData(SHEET_NAME_1),
                Integer.valueOf(driver.scenarioData.getData(ROW_NUMBER)),
                columnName);

    }

    /**
     * opens a protected .xls excel sheet and returns password for SAP user from excel based on SAp module and environment
     *
     * @param userName - SAP user name
     * @param module   - SAP module
     * @return Password for input user name
     * @throws Throwable
     */
    public String returnPassword(String userName, String module, String dataSet) throws Throwable {
        LOGGER.info("returnPassword started for username: " + userName + " Module: " + module
                + " and Data set: " + dataSet);
        String cellValue;
        String password = null;
        File file = new File(Constants.SAP_USERS_LOCATION);
        NPOIFSFileSystem fileSystem = new NPOIFSFileSystem(file, true);
        EncryptionInfo info = new EncryptionInfo(fileSystem);
        Decryptor decryptor = Decryptor.getInstance(info);
        if (!decryptor.verifyPassword(Config.getSapPassword())) {
            System.out.println("Unable to process: document is encrypted.");
        }
        InputStream dataStream = decryptor.getDataStream(fileSystem);
        XSSFWorkbook xssfworkbook = new XSSFWorkbook(dataStream);
        XSSFSheet xssfsheet = xssfworkbook.getSheet(USERS);
        int totalRows = xssfsheet.getLastRowNum();
        for (int i = 0; i <= totalRows; i++) {
            Row row = xssfsheet.getRow(i);
            cellValue = row.getCell(0).getStringCellValue();
            if (cellValue.contains(userName.toUpperCase()) && cellValue.contains(module.toUpperCase()) &&
                    cellValue.contains(dataSet.toUpperCase())) {
                password = row.getCell(1).getStringCellValue();
                break;
            }
            if (i == totalRows) {
                Assert.fail("The username passed: " + userName + " for module: " + module + " and environment: " +
                        dataSet + " does not exist in the excel file ");
            }
        }
        LOGGER.info("returnPassword completed for username: " + userName + " Module: " + module
                + " and Data set: " + dataSet);
        return password;
    }

    /**
     * Waits for iFrame with text in /n/dol/ap2n
     *
     * @param text - Text in Iframe
     */
    public void waitForFirstIframeWithTextInDolAp2n(String text) {
        LOGGER.info("waitForFirstIframeWithTextInDolAp2n Started");
        if (driver.isElementDisplayed(firstIframeElementBy)) {
            driver.switchFrameContext(0);
            waitForElementWithText(text, Constants.ONE_HUNDRED);
            webDriver.switchTo().defaultContent();
        }
        LOGGER.info("waitForFirstIframeWithTextInDolAp2n Completed");
    }

    /**
     * Checks if Text is available in iframe
     *
     * @param text - Text in Iframe
     * @return - Bolean
     */
    public boolean isTextAvailableInFirstIframe(String text) {
        return isTextAvailableInFirstIframe(text, 5);
    }

    /**
     * Checks if Text is available in iframe
     *
     * @param text    - Text in Iframe
     * @param seconds - seconds
     * @return True if text present
     */
    public boolean isTextAvailableInFirstIframe(String text, int seconds) {
        LOGGER.info("isTextAvailableInFirstIframe Started");
        boolean isAvailable = false;
        if (driver.isElementDisplayed(firstIframeElementBy, seconds)) {
            driver.switchFrameContext(0);
            try {
                if (webDriver.findElement(iframeTitleHeaderBy).getText().equalsIgnoreCase(text)) {
                    isAvailable = true;
                }
            } catch (StaleElementReferenceException se) {
                if (webDriver.findElement(iframeTitleHeaderBy).getText().equalsIgnoreCase(text)) {
                    isAvailable = true;
                }
            }
            webDriver.switchTo().defaultContent();
        }
        LOGGER.info("isTextAvailableInFirstIframe Completed");
        return isAvailable;
    }

    /**
     * Takes PTS screenshot for debugging
     * (method will be removed post prod release)
     *
     * @param ptsNumberAsFolderName - pts record number
     * @param imageName             - image name(PTS Status)
     * @throws Exception - I/O
     */
    public void takePtsScreenShotsAtIntervals(String ptsNumberAsFolderName, String imageName) throws Exception {
        LOGGER.info("takePtsScreenShotsAtIntervals started");
        //TODO: Remove Method post prod release.
        try {
            TakesScreenshot screenshot = ((TakesScreenshot) webDriver);
            File staticScreenshotFile = screenshot.getScreenshotAs(OutputType.FILE);
            String imageNameForPts = "//" + imageName;
            FileUtils.copyFile(staticScreenshotFile, new File(Constants.SAP_PTS_DEBUG_LOCATION
                    + ptsNumberAsFolderName + imageNameForPts + ".png"));
        } catch (Exception e) {

        }
        LOGGER.info("takePtsScreenShotsAtIntervals completed");
    }

    /**
     * Short cut function to access get variant option on SAP Nav bar
     */
    public void getVariantShortCut() {
        LOGGER.info("getVariantShortCut started");
        performHoldAndReleaseKeyAction(Keys.SHIFT, "DOWN");
        driver.waitForMilliseconds();
        driver.performKeyAction(Keys.F5);
        driver.waitForMilliseconds();
        performHoldAndReleaseKeyAction(Keys.SHIFT, "UP");
        LOGGER.info("getVariantShortCut completed");
    }

    /**
     * Closes the Item detail in purchase order page if it is open to allow visibility of item overview table for
     * more than 4 rows
     **/
    public void collapseItemDetails() {
        LOGGER.info("collapseItemDetails started");
        performHoldAndReleaseKeyAction(Keys.CONTROL, "DOWN");
        driver.performKeyAction(Keys.F7);
        performHoldAndReleaseKeyAction(Keys.CONTROL, "UP");
        LOGGER.info("collapseItemDetails completed");
    }

    /**
     * Method used to retrieve no of total rows between first non-null row to last non-null row
     *
     * @param filepath   - full file path
     * @param sheetname  - the name of the excel sheet
     * @param columnName - Name of the column
     * @return int representing index of row found between first non-null and last non-null row.
     * @throws IOException
     **/
    public int getNumberOfRows(String filepath, String sheetname, String columnName) throws IOException {
        LOGGER.info("getNumberOfRows started.");
        File file = new File(filepath);
        FileInputStream inputStream = new FileInputStream(file);
        int cellNumber = 0;
        boolean found = false;
        Sheet sheet;
        if (filepath.toLowerCase().contains(Constants.XLSX.toLowerCase())) {
            XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xssfsheet = xssfworkbook.getSheet(sheetname);
            sheet = xssfsheet;
        } else {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(inputStream);
            HSSFSheet hssfsheet = hssfworkbook.getSheet(sheetname);
            sheet = hssfsheet;
        }
        Row firstRow = sheet.getRow(0);
        for (int i = 0; i <= firstRow.getLastCellNum(); i++) {
            String columnOutput = firstRow.getCell(i).getStringCellValue();
            if (columnName.equalsIgnoreCase(columnOutput)) {
                cellNumber = i;
                break;
            }
        }
        int lastRowIndex = sheet.getLastRowNum();
        Row lastRow = sheet.getRow(lastRowIndex);
        for (int i = lastRowIndex; i >= 0; i--) {
            if (StringUtils.isEmpty(String.valueOf(lastRow.getCell(cellNumber))) ||
                    StringUtils.isWhitespace(String.valueOf(lastRow.getCell(cellNumber))) ||
                    StringUtils.isBlank(String.valueOf(lastRow.getCell(cellNumber))) ||
                    String.valueOf(lastRow.getCell(cellNumber)).length() == 0 ||
                    lastRow.getCell(cellNumber) == null) {
                found = true;
                lastRow = sheet.getRow(i);
                lastRowIndex = i;
            }
        }
        LOGGER.info("White Spaces or empty cells where found? = " + found);
        LOGGER.info("Number of Rows: " + lastRowIndex);
        LOGGER.info("getNumberOfRows completed.");
        return lastRowIndex;
    }

    /**
     * Method created to retrieve multiple rows of data at once provided the data provided each article Id have a quantity
     * and site it belongs to.
     *
     * @param filepath   - full file path
     * @param sheetname  - the name of the excel sheet
     * @param columnName - Name of the column
     * @throws IOException
     */
    public void getExcelDataByColumn(String filepath, String sheetname, String columnName) throws IOException {
        LOGGER.info("getExcelDataByColumn started");
        File file = new File(filepath);
        FileInputStream inputStream = new FileInputStream(file);
        int articleNumber = getNumberOfRows(filepath, sheetname, "Article Number");
        int cellNumber = 0;
        Boolean validity = true;
        Sheet sheet;
        if (filepath.toLowerCase().contains(Constants.XLSX.toLowerCase())) {
            XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xssfsheet = xssfworkbook.getSheet(sheetname);
            sheet = xssfsheet;
        } else {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(inputStream);
            HSSFSheet hssfsheet = hssfworkbook.getSheet(sheetname);
            sheet = hssfsheet;
        }
        Row firstRow = sheet.getRow(0);
        for (int i = 0; i <= firstRow.getLastCellNum(); i++) {
            String columnOutput = firstRow.getCell(i).getStringCellValue();
            if (columnName.equalsIgnoreCase(columnOutput)) {
                cellNumber = i;
                break;
            }
        }

        int rowCount = getNumberOfRows(filepath, sheetname, columnName);
        for (int i = 1; i <= rowCount; i++) {
            Row C_Row = sheet.getRow(i);
            if (StringUtils.isEmpty(String.valueOf(C_Row.getCell(cellNumber))) ||
                    StringUtils.isWhitespace(String.valueOf(C_Row.getCell(cellNumber))) ||
                    StringUtils.isBlank(String.valueOf(C_Row.getCell(cellNumber))) ||
                    String.valueOf(C_Row.getCell(cellNumber)).length() == 0 ||
                    C_Row.getCell(cellNumber) == null || rowCount != articleNumber) {
                LOGGER.info("invalid data");
                Assert.fail("FAIL: The data entered is invalid data");
                validity = false;
                break;
            }
        }
        String cellValue = "";
        if (validity) {
            try {
                for (int i = 1; i <= rowCount; i++) {
                    Row C_Row = sheet.getRow(i);
                    try {
                        Double cell_Value = C_Row.getCell(cellNumber).getNumericCellValue();
                        String Output = String.valueOf(cell_Value);
                        cellValue = Output.substring(0, Output.length() - 2);
                        driver.scenarioData.setData(columnName + i, cellValue);
                        LOGGER.info((columnName + " " + i) + " - " + driver.scenarioData.getData(columnName + i));
                    } catch (IllegalStateException ise) {
                        cellValue = C_Row.getCell(cellNumber).getStringCellValue();
                        driver.scenarioData.setData(columnName + i, cellValue);
                        LOGGER.info((columnName + " " + i) + " - " + driver.scenarioData.getData(columnName + i));
                    }
                }
            } catch (IllegalStateException ise) {
                for (int i = 1; i <= rowCount; i++) {
                    Row C_Row = sheet.getRow(i);
                    try {
                        cellValue = C_Row.getCell(cellNumber).getStringCellValue();
                        driver.scenarioData.setData(columnName + i, cellValue);
                        LOGGER.info((columnName + " " + i) + " - " + driver.scenarioData.getData(columnName + i));
                    } catch (IllegalStateException isE) {
                        Double cell_Value = C_Row.getCell(cellNumber).getNumericCellValue();
                        String Output = String.valueOf(cell_Value);
                        cellValue = Output.substring(0, Output.length() - 2);
                        driver.scenarioData.setData(columnName + i, cellValue);
                        LOGGER.info((columnName + " " + i) + " - " + driver.scenarioData.getData(columnName + i));
                    }
                }
            }
        }
        LOGGER.info("getExcelDataByColumn completed.");
    }

    /**
     * Method to click on radio or check box with label
     *
     * @param labelName - Label Name
     */
    public void clickOnRadioOrCheckBoxWithLabel(String labelName) {
        LOGGER.info("clickOnRadioOrCheckBoxWithLabel started");
        String elementNumber;
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            elementNumber = "3";
        } else {
            elementNumber = "1";
        }
        WebElement element = webDriver.findElement(By.xpath("//*[text()='" + labelName + "']/preceding::*[" + elementNumber + "]"));
        click(element);
        LOGGER.info("clickOnRadioOrCheckBoxWithLabel completed");
    }

    /**
     * Scrolls up and down
     *
     * @param direction Up or Down
     */
    public void scrollUpOrDown(String direction) {
        LOGGER.info("scrollUpOrDown started");
        if (direction.equalsIgnoreCase(UP)) {
            webDriver.findElement(verticleScrollUpBy).click();
        } else {
            webDriver.findElement(verticalScrollDownBy).click();
        }
        LOGGER.info("scrollUpOrDown completed");
    }

    /**
     * Enters random string of specified length
     *
     * @param length length of random string that is required
     * @return random string with specified length
     */
    public String enterRandomString(int length) {
        LOGGER.info("enterRandomString started");
        SecureRandom random = new SecureRandom();

        if (length < 1) {
            Assert.fail("Please enter correct string");
        }

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int rndCharAt = random.nextInt(CHAR_UPPER.length());
            char rndChar = CHAR_UPPER.charAt(rndCharAt);
            sb.append(rndChar);
            LOGGER.info(sb.toString());
        }
        LOGGER.info("enterRandomString completed");
        return sb.toString();
    }
}