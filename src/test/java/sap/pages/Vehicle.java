package sap.pages;

import common.Config;
import common.Constants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;
import java.util.List;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class Vehicle {
    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(Vehicle.class.getName());

    public Vehicle(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }


    @FindBy(xpath = "//*[text()='Vehicle class:']/following::input[1]")
    private static WebElement vehicleClassDropDown;

    @FindBy(xpath = "//*[text()='Vehicle class:']/following::input[2]")
    private static WebElement yearDropDown;

    @FindBy(xpath = "//*[text()='Vehicle class:']/following::input[3]")
    private static WebElement makeDropDown;

    @FindBy(xpath = "//*[text()='Vehicle class:']/following::input[4]")
    private static WebElement modelDropDown;

    @FindBy(xpath = "(//*[text()='Trim/Sub-Model Description builder']/following::input)[1]")
    private static WebElement inputBox1;

    @FindBy(xpath = "(//*[text()='Trim/Sub-Model Description builder']/following::input)[2]")
    private static WebElement inputBox2;

    @FindBy(xpath = "(//*[text()='Trim/Sub-Model Description builder']/following::input)[3]")
    private static WebElement inputBox3;

    @FindBy(xpath = "(//*[text()='Trim/Sub-Model Description builder']/following::input)[4]")
    private static WebElement inputBox4;

    @FindBy(xpath = "(//*[text()='Trim/Sub-Model Description builder']/following::input)[5]")
    private static WebElement inputBox5;

    @FindBy(xpath = "(//*[text()='Trim/Sub-Model Description builder']/following::input)[6]")
    private static WebElement inputBox6;

    @FindBy(xpath = "//*[contains(@lsdata,'Transmission')]")
    private static WebElement transmission;

    @FindBy(id = "SearchHelpPage12-title")
    private static WebElement cabStyle;

    @FindBy(id = "SearchHelpPage13-title")
    private static WebElement trimAlphaNumeric;

    @FindBy(id = "SearchHelpPage14-title")
    private static WebElement trimLetters;

    @FindBy(id = "SearchHelpPage15-title")
    private static WebElement trimNames;

    @FindBy(id = "SearchHelpPage16-title")
    private static WebElement driveTrimNames;

    @FindBy(xpath = "(//*[contains(@title,'Delete an assembly')])[1]")
    private static WebElement deleteATrim;

    @FindBy(xpath = "(//*[contains(@title,'Delete an assembly')])[2]")
    private static WebElement deleteAnAssembly;

    @FindBy(xpath = "//*[contains(@title,'Color 2 (Dark Gray)')][2]")
    private static WebElement savedMessage;

    @FindBy(xpath = "//*[not(contains(@title,'System')) and contains(@title,'Close')]")
    private static WebElement closeButton;

    @FindBy(xpath = "(//*[contains(@title,'Char 20')]/span)[1]")
    private static WebElement inputbox1Text;

    @FindBy(xpath = "(//*[contains(@title,'Char 20')]/span)[2]")
    private static WebElement inputbox2Text;

    @FindBy(xpath = "(//*[contains(@title,'Char 20')]/span)[3]")
    private static WebElement inputbox3Text;

    @FindBy(xpath = "(//*[contains(@title,'Char 20')]/span)[4]")
    private static WebElement inputbox4Text;

    @FindBy(xpath = "(//*[contains(@title,'Char 20')]/span)[5]")
    private static WebElement inputbox5Text;

    @FindBy(xpath = "(//*[contains(@title,'Char 20')]/span)[6]")
    private static WebElement inputbox6Text;

    @FindBy(xpath = "//div[contains(@id,'_vscroll-Nxt')]")
    private static WebElement scrollNext;

    @FindBy(xpath = "//textarea[contains(@id,'TEC_cnt')]")
    private static WebElement inputTextBox;

    @FindBy(id = "wnd[0]/sbar_msg-txt")
    private static WebElement saveMessage;

    @FindBy(xpath = "(//*[contains(@id,'TEC_cnt')])[2]")
    private static WebElement textNotes;

    @FindBy(xpath = "//*[text()='Special notes']/following::input[1]")
    private static WebElement inputSpecialNotes;

    @FindBy(xpath = "//*[text()='TPMS notes']/following::input[1]")
    private static WebElement inputTpmsNotes;

    @FindBy(xpath = "//*[text()='Lifting notes']/following::input[1]")
    private static WebElement inputLiftNotes;

    @FindBy(xpath = "(//*[contains(@title,'Append Row ')])[1]")
    private static WebElement createNewRearAxile;

    @FindBy(xpath = "(//*[contains(@title,'Append Row ')])[2]")
    private static WebElement createNewOverSizedTire;

    @FindBy(id = "//div[contains(@id, 'grid#23') and contains(@id, ',mrss-cont-none div')]")
    private static WebElement tableClick;

    @FindBy(xpath = "(//*[contains(@title,'Delete Row ')])[1]")
    private static WebElement deleteRearAxle;

    @FindBy(xpath = "(//*[contains(@title,'Delete Row ')])[2]")
    private static WebElement deleteOverSizedTire;

    @FindBy(xpath = "(//*[contains(@title,'Chassis Number')])[4]")
    private static WebElement chassisNumber;

    @FindBy(xpath = "(//*[contains(@title,'View global Special Notes')])[1]")
    private static WebElement specialNotesLink;

    @FindBy(xpath = "((//*[contains(@title,'View global Special Notes')])[1]/following::span)[3]")
    private static WebElement specialNotesNumber;

    @FindBy(xpath = "//*[contains(@id,'2,3#dd-btn')]")
    private static WebElement trimDropdown;

    @FindBy(xpath = "//span[contains(text(),'SPECIAL')]/following::tr[@rr='1'][1]")
    private static WebElement selectFirstRow;

    @FindBy(xpath = "//td[contains(text(),'List of Global Notes')]/following::tr[@rr='1'][1]")
    private static WebElement selectFirstRowOfGlobalNotes;

    @FindBy(xpath = "//span[contains(@id, '#4,1')]//input[contains(@id,'4,1#if')]")
    private static WebElement addedGlobalNotes;

    @FindBy(xpath = "//img[contains(@id,'34') and contains(@id,'_toolbar_btn9')]")
    private static WebElement removeAddedGlobalNotes2;

    @FindBy(id = "M1:D:13::btn[0]")
    private static WebElement saveButtonInIframe;

    @FindBy(xpath = "(//*[contains(@id,'1,3#if')])[2]")
    private static WebElement vSegCatId;

    @FindBy(xpath = "(//*[contains(@id,'9#if')])[2]")
    private static WebElement vehSegCatID;

    @FindBy(id = "M0:U:::2:21")
    private static WebElement tableName;

    @FindBy(xpath = "(//*[contains(@title,'Vehicle ID')])[1]")
    private static WebElement vehicleID;

    @FindBy(id = "M0:U:::1:34")
    private static WebElement vehicleClass;

    @FindBy(id = "M0:U:::2:34")
    private static WebElement vehicleMake;

    @FindBy(id = "M0:U:::3:34")
    private static WebElement vehicleModel;

    @FindBy(id = "M0:U:::4:34")
    private static WebElement year;

    @FindBy(id = "M0:D:10::btn[71]")
    private static WebElement findButton;

    @FindBy(id = "M1:U:::0:17")
    private static WebElement searchTerm;

    @FindBy(xpath = "(//span[contains(text(),'Vehicle ID')]/following::*[contains(@id,'1,1#if')])[2]")
    private static WebElement vehicleIDFromVehList;

    @FindBy(xpath = "//*[contains(text(),'TPMS Data')]/following::*[contains(@id, '_vscroll-Nxt')]")
    private static WebElement tpmsDataTableScrollBar;

    @FindBy(xpath = "//*[contains(@title, 'Create a TPMS record ')]")
    private static WebElement tpmsDataRowCreate;

    @FindBy(xpath = "//*[contains(text(),'TPMS data')]/following::*[contains(@title,'Delete an assembly')]")
    private static WebElement tpmsDeleteButton;

    @FindBy(xpath = "//a[contains(@id,'22') and contains(@id,'_sh') and contains(@title,'Copy')]")
    private static WebElement copyButton;

    @FindBy(xpath = "//img[contains(@id,'@CREATE')]")
    private static WebElement createAssemblies;

    @FindBy(xpath = "//*[contains(@id, 'inputfieldhelpbutton')]")
    private static WebElement inputFieldHelpButton;

    @FindBy(xpath = "(//*[contains(text(),'Restrictions')]/following::div/span/span)[14]")
    private static WebElement tpmsEntry;

    @FindBy(xpath = "(//*[contains(text(),'OEM Tire / Wheel (Assemblies)')]/following::*[contains(@id,',3#if') and not (contains(@id,',3#if-r'))])[2]")
    private static WebElement widthOfFrontWheel;

    @FindBy(xpath = "(//*[contains(text(),'OEM Tire / Wheel (Assemblies)')]/following::*[contains(@id,',4#if') and not (contains(@id,',4#if-r'))])[2]")
    private static WebElement aspectRatioOfFrontWheel;

    @FindBy(xpath = "(//*[contains(text(),'OEM Tire / Wheel (Assemblies)')]/following::*[contains(@id,',5#if') and not (contains(@id,',5#if-r'))])[2]")
    private static WebElement tireDiameterOfFrontWheel;

    @FindBy(xpath = "(//*[contains(text(),'OEM Tire / Wheel (Assemblies)')]/following::*[contains(@id,',7#if') and not (contains(@id,',7#if-r'))])[2]")
    private static WebElement loadIndexOfFrontWheel;

    @FindBy(xpath = "(//*[contains(text(),'OEM Tire / Wheel (Assemblies)')]/following::*[contains(@id,',3#if') and not (contains(@id,',3#if-r'))])[3]")
    private static WebElement widthOfRearWheel;

    @FindBy(xpath = "(//*[contains(text(),'OEM Tire / Wheel (Assemblies)')]/following::*[contains(@id,',4#if') and not (contains(@id,',4#if-r'))])[3]")
    private static WebElement aspectRatioOfRearWheel;

    @FindBy(xpath = "(//*[contains(text(),'OEM Tire / Wheel (Assemblies)')]/following::*[contains(@id,',5#if') and not (contains(@id,',5#if-r'))])[3]")
    private static WebElement tireDiameterOfRearWheel;

    @FindBy(xpath = "(//*[contains(text(),'OEM Tire / Wheel (Assemblies)')]/following::*[contains(@id,',7#if') and not (contains(@id,',7#if-r'))])[3]")
    private static WebElement loadIndexOfRearWheel;

    @FindBy(xpath = "//label[contains(@lsdata,'125')]")
    private static WebElement crossSectionValue;

    @FindBy(xpath = "//label[contains(@lsdata,'12.50')]")
    private static WebElement aspectRatioValue;

    @FindBy(xpath = "//label[contains(@lsdata,'12')]")
    private static WebElement wheelDiameterValue;

    @FindBy(xpath = "//label[contains(@lsdata,'109')]")
    private static WebElement loadIndexValue;

    @FindBy(xpath = "//*[contains(@id,'1,2#if') and not (contains(@id,'1,2#if-r'))]")
    private static WebElement trimSubModel;

    @FindBy(xpath = "//span[contains(text(),'Trim/Sub')]/following::input[contains(@id,'2,2#if')]")
    private static WebElement trimSubModelTwo;

    @FindBy(xpath = "//*[contains(@id,'75,7#if') and not (contains(@id,'75,7#if-r'))]")
    private static WebElement trimSubModelDescription;

    @FindBy(xpath = "(//*[contains(@id,',7#if') and not (contains(@id,',7#if-r'))])[1]")
    private static WebElement trimSubModelDescriptionInStage;

    @FindBy(xpath = "//span[contains(text(),'Category')]/following::*[contains(@id,',9#if')][1]")
    private static WebElement vehSegCatIdReport;

    @FindBy(xpath = "//span[contains(text(),'Category')]/following::*[contains(@id,',9#if')][2]")
    private static WebElement vehSegCatIdReportInStage;

    @FindBy(xpath = "//input[@id='__field0-I']")
    private static WebElement inputInfoSheetSearch;

    @FindBy(xpath = "//div[contains(@id,'__item0-idMaster--detailList-0-titleText-inner')]")
    private static WebElement infoSheetSearchResults;

    @FindBy(id = "__button0-img")
    private static WebElement mailButton;

    @FindBy(id = "EmpName-inner")
    private static WebElement empName;

    @FindBy(id = "storeId-inner")
    private static WebElement storeId;

    @FindBy(id = "M0:U:::6:34")
    private static WebElement searchString;

    @FindBy(id = "M0:U:::2:14")
    private static WebElement abapProgramName;

    @FindBy(id = "M0:U:::7:2-txt")
    private static WebElement initialization;

    @FindBy(xpath = "//*[(contains(@id,'grid#11') and contains(@id,'#1,2#if')) and not (contains(@id,'#1,2#if-r'))]")
    private static WebElement keywordSearchResults;

    @FindBy(xpath = "//img[contains(@id,'20') and contains(@id, '_toolbar_btn3-img')]")
    private static WebElement tpmsCreateRecordButton;

    @FindBy(xpath = "//span[contains(text(),'Article')]/following::input[contains(@id,'1,1#if')][2]")
    private static WebElement typeTpmsArticleNr;

    @FindBy(xpath = "//span[text()='TPMS Data']/following::div[@class='urST5SCMetricInner'][1]")
    private static WebElement selectFirstTpmsRow;

    @FindBy(xpath = "(//*[contains(@title,'Delete an assembly')])[3]")
    private static WebElement tpmsDeleteRow;

    @FindBy(xpath = "//div[contains(@id,'14.') and contains(@id,'_vscroll-Nxt')]")
    private static WebElement assemblyScrollBar;

    @FindBy(xpath = "(//div[contains(@id,'_hscroll-Nxt')])[2]")
    private static WebElement assemblyHorizontalScrollBar;

    @FindBy(xpath = "//*[text()='Restrictions']/following::input[9]")
    private static WebElement searchInputBox;

    @FindBy(xpath = "//*[text()='OEM Articles']/following::*[contains(@id,'#1,1')]")
    private static WebElement articleListSelection;

    @FindBy(xpath = "//*[text()='OEM Tire / Wheel (Assemblies)']/following::*/img[contains(@id,'2,14')]")
    private static WebElement addedLastArticle;

    @FindBy(id = "M0:U:::3:34")
    private static WebElement tdidInput;

    @FindBy(id = "M0:U:::1:66")
    private static WebElement fixedColumnsNr;

    @FindBy(xpath = "//span[contains(text(), 'OEM Tire')]/following::img[contains(@id,'1,7')]")
    private static WebElement oemTireStatus;

    @FindBy(xpath = "//span[contains(text(),'Chas.Confg')]/following::img[contains(@id,'1,6')]")
    private static WebElement chassisConfigStatus;

    @FindBy(xpath = "//span[contains(text(), 'Chassis')]/following::img[contains(@id,'1,8')]")
    private static WebElement chassisStatus;

    @FindBy(id = "M0:U:::25:89")
    private static WebElement threadedLength;

    @FindBy(xpath = "//*[contains(@id,'-title')]")
    private static WebElement dataBrowserTitle;

    @FindBy(xpath = "(//*[contains(@title,'Chassis Number')])[6]")
    private static WebElement chasisCountNumber;

    @FindBy(xpath = "((//*[contains(@title,'View global TPMS Notes')])[1]/following::span)[3]")
    private static WebElement tpmsCountNumber;

    @FindBy(xpath = "(//*[contains(text(),'(Assemblies)')]/following::div[@class='urST5SCMetricInner'])[2]")
    private static WebElement assemblyRow2;

    @FindBy(xpath = "//*[contains(@id,'menuOpener')]")
    private static WebElement menuButton;

    @FindBy(xpath = "//*[contains(@title,'Display / change special notes ')]")
    private static WebElement displayChangeSpecialNotes;

    @FindBy(xpath = "//*[contains(@title,'Display / change TPMS notes ')]")
    private static WebElement displayChangeTpmsNotes;

    @FindBy(xpath = "//*[contains(@title, 'Display / change lift notes ')]")
    private static WebElement displayChangeLiftNotes;

    @FindBy(xpath = "//*[contains(@id, '125#2,3#if') and not (contains(@id, '125#2,3#if-r'))]")
    private static WebElement oemTableWidth;

    @FindBy(xpath = "//*[contains(@id, '125#2,4#if') and not (contains(@id, '125#2,4#if-r'))]")
    private static WebElement oemTableAspectRatio;

    @FindBy(xpath = "//*[contains(@id, '125#2,5#if') and not (contains(@id, '125#2,5#if-r'))]")
    private static WebElement oemTableTireDiameter;


    private static final By lastRowToDeleteBy = By.xpath("//*[contains(@title,'Table selection menu')]/following::*[contains(@id,',0')]");
    private static final By lastNameRowBy = By.xpath("//*[text()='List of Global Notes']/following::*[not (contains(@title,'Name')) and contains(@id,',1#') and not(contains(@id,'if-r'))]");
    private static final By globalNotesSelectionCountBy = By.xpath("//div[@class='urST5SCMetricInner']");
    private static final By listOfGlobalNotesSelectionCountBy = By.xpath("//td[contains (@id,'grid#115') and contains(@id,',0')]");
    private static final By listOfChassisConfigSelectionCountBy = By.xpath("//td[contains (@id,'grid#115') and contains(@id,',0')]");
    private static final By globalNotesAddedCountBy = By.xpath("//td[contains (@id,'grid#263') and contains(@id,',0')]");
    private static final By tpmsDataRowCountBy = By.xpath("//*[contains(text(),'TPMS data')]/following::span[contains(@id,'1#if-r')]");
    private static final By tpmsRowDeleteBy = By.xpath("//*[contains(text(),'TPMS data')]/following::div[@class='urST5SCMetricInner']");
    private static final By globalNotesSpecialNotesCountBy = By.xpath("//td[contains(@id,'grid#29') and contains(@id, ',0')]");
    private static final By globalNotesAddedCount2By = By.xpath("//td[contains(@id,'grid#34') and contains(@id, ',0')]");
    private static final By articleListBy = By.xpath("//*[text()='OEM Tire / Wheel (Assemblies)']/following::*/img[contains(@id,',14')]");
    private static final By lastAssemblyRowBy = By.xpath("//td[contains(@id,'grid#14') and contains(@id,',0')]");

    private static final String VEHICLE_DROP_DOWN_BUTTON = "Vehicle drop down button";
    private static final String YEAR_DROP_DOWN_BUTTON = "Year drop down button";
    private static final String MAKE_DROP_DOWN_BUTTON = "Make drop down button";
    private static final String MODEL_DROP_DOWN_BUTTON = "Model drop down button";
    private static final String INPUT_BOX_1 = "input box1";
    private static final String INPUT_BOX_2 = "input box2";
    private static final String INPUT_BOX_3 = "input box3";
    private static final String INPUT_BOX_4 = "input box4";
    private static final String INPUT_BOX_5 = "input box5";
    private static final String INPUT_BOX_6 = "input box6";
    private static final String CAB_STYLE = "Cab Style";
    private static final String TRIM_ALPHA_NUMERIC = "TRIM Alpha Numeric";
    private static final String TRIM_LETTERS = "Trim Letters";
    private static final String TRIM_NAMES = "Trim Names";
    private static final String DRIVE_TRIM_NAMES = "Drive Trim Names";
    private static final String TRANSMISSION = "Transmission";
    private static final String TRIM_DROPDOWN = "Trim Dropdown";
    private static final String DELETE_A_TRIM = "Delete a Trim";
    private static final String DELETE_AN_ASSEMBLY = "Delete an Assembly";
    private static final String CLOSE_BUTTON = "Close button";
    private static final String INPUT_BOX_1_TEXT = "input box1 text";
    private static final String INPUT_BOX_2_TEXT = "input box2 text";
    private static final String INPUT_BOX_3_TEXT = "input box3 text";
    private static final String INPUT_BOX_4_TEXT = "input box4 text";
    private static final String INPUT_BOX_5_TEXT = "input box5 text";
    private static final String INPUT_BOX_6_TEXT = "input box6 text";
    private static final String SAVED_MESSAGE = "saved message";
    private static final String INPUT_TEXT_BOX = "Input text box";
    private static final String SAVE_MESSAGE = "Save message";
    private static final String LAST_ROW = "Last row";
    private static final String DELETE_LAST_ROW = "newly added row to delete";
    private static final String TEXT_NOTES = "text notes";
    private static final String INPUT_SPECIAL_NOTES = "input special notes";
    private static final String INPUT_TPMS_NOTES = "input TPMS notes";
    private static final String INPUT_LIFT_NOTES = "input lift notes";
    private static final String RANDOM = "Random";
    private static final String CREATE_NEW_REAR_AXILE_ROW = "Create new rear axle row";
    private static final String CREATE_NEW_OVER_SIZED_ROW = "Create new over sized row";
    private static final String DELETE_REAR_AXLE = "Delete axle";
    private static final String DELETE_OVER_SIZED_TIRE = "Delete over sized tire";
    private static final String CHASSIS_NUMBER = "Chassis Number";
    private static final String SPECIAL_NOTES_LINK = "Special Notes link";
    private static final String SPECIAL_NOTES_NUMBER = "Special notes number";
    private static final String GLOBAL_NOTES_SELECTION = "Global notes selection";
    private static final String LIST_GLOBAL_NOTES_SELECTION = "List of Global notes";
    private static final String CHASSIS_CONFIG_SELECTION = "Chassis Config Table";
    private static final String GLOBAL_NOTES_ADDED = "Global notes added";
    private static final String SELECT_FIRST_ROW = "Select First Row";
    private static final String SELECT_FIRST_ROW_OF_GLOBAL_NOTES = "Select first row of global notes";
    private static final String ADDED_GLOBAL_NOTES = "Added global notes";
    private static final String SPECIAL_NOTES_GLOBAL_SELECTION = "Special notes global selection";
    private static final String GLOBAL_NOTES_ADDED_2 = "Global notes added 2";
    private static final String REMOVE_GLOBAL_NOTES_SELECTION_2 = "Remove Global notes selection 2";
    private static final String SAVE_BUTTON_IN_IFRAME = "Save Button In Iframe";
    private static final String V_SEG_CAT_ID = "VSegCatId";
    private static final String TABLE_NAME = "Table Name";
    private static final String VEHICLE_ID = "Vehicle ID";
    private static final String VEHICLE_CLASS = "Vehicle Class";
    private static final String VEHICLE_MAKE = "Vehicle Make";
    private static final String VEHICLE_MODEL = "Vehicle Model";
    private static final String VEHICLE_ID_FROM_VEHLIST = "Vehicle ID From Vehlist";
    private static final String YEAR = "Year";
    private static final String VEH_SEG_CAT_ID = "Veh Seg Cat ID";
    private static final String GLOBAL_SCROLL_BAR = "Global scroll bar";
    private static final String TPMS_LAST_ROW = "TPMS last row";
    private static final String TPMS_DATA_SCROLL_BAR = "TPMS scroll bar";
    private static final String TPMS_DATA_CREATE_RECORD = "Create new TPMS record";
    private static final String COPY_BUTTON = "Copy button";
    private static final String TPMS_ROW_DELETE = "TPMS row delete";
    private static final String TPMS_DELETE_BUTTON = "TPMS delete button";
    private static final String CREATE_ASSEMBLIES = "Creates Assemblies";
    private static final String WIDTH_OF_FRONT_WHEEL = "Width Of Front Wheel";
    private static final String WIDTH_OF_REAR_WHEEL = "Width Of Rear Wheel";
    private static final String ASPECT_RATIO_OF_FRONT_WHEEL = "Aspect Ratio Of Front Wheel";
    private static final String ASPECT_RATIO_OF_REAR_WHEEL = "Aspect Ratio Of Rear Wheel";
    private static final String TIRE_DIAMETER_OF_FRONT_WHEEL = "Tire Diameter Of Front Wheel";
    private static final String TIRE_DIAMETER_OF_REAR_WHEEL = "Tire Diameter Of Rear Wheel";
    private static final String LOAD_INDEX_OF_FRONT_WHEEL = "Load Index Of Front Wheel";
    private static final String LOAD_INDEX_OF_REAR_WHEEL = "Load index Of Rear Wheel";
    private static final String CROSS_SECTION_VALUE = "Cross Section Value";
    private static final String ASPECT_RATIO_VALUE = "Aspect Ratio Value";
    private static final String WHEEL_DIAMETER_VALUE = "Wheel Diameter Value";
    private static final String LOAD_INDEX_VALUE = "Load Index Value";
    private static final String FIND_BUTTON = "Find Button";
    private static final String TRIM_SUB_MODEL = "Trim Sub Model";
    private static final String TRIM_SUB_MODEL_TWO = "Trim Sub Model Two";
    private static final String SEARCH_TERM = "Search Term";
    private static final String TRIM_SUB_MODEL_DESCRIPTION = "Trim Sub Model Description";
    private static final String VEH_SEG_CAT_ID_REPORT = "Veh Seg Cat Id Report";
    private static final String INPUT_INFO_SHEET_SEARCH = "Input info sheet search";
    private static final String INFO_SHEET_SEARCH_RESULT = "Info sheet search result";
    private static final String MAIL_BUTTON = "Mail button";
    private static final String EMPLOYEE_NAME = "Employee name";
    private static final String STORE_ID = "Store id";
    private static final String SEARCH_STRING = "Search string";
    private static final String KEYWORD_SEARCH_RESULT = "Keyword Search result";
    private static final String ABAP_PROGRAM_NAME = "ABAP Program Name";
    private static final String INITIALIZATION = "Initialization";
    private static final String TPMS_CREATE_RECORD_BUTTON = "Tpms Create Record Button";
    private static final String TYPE_TPMS_ARTICLE_NR = "Type Tpms Article Nr";
    private static final String SELECT_FIRST_TPMS_ROW = "Select First Tpms row";
    private static final String TPMS_DELETE_ROW = "Tpms Delete Row";
    private static final String ASSEMBLY_SCROLL_BAR = "Assembly scroll bar";
    private static final String SEARCH_INPUT_BOX = "Search input box";
    private static final String ARTICLE_LIST_LAST_ROW = "Article list";
    private static final String ASSEMBLY_HORIZONTAL_SCROLL_BAR = "Assembly horizontal scroll bar";
    private static final String ARTICLE_LIST_SELECTION = "Article list selection";
    private static final String LAST_ASSEMBLY_ROW = "Last assembly row";
    private static final String ADDED_LAST_ARTICLE = "Added last article";
    private static final String TDID_INPUT = "Tdid Input";
    private static final String FIXED_COLUMNS_NR = "Fixed Columns Nr";
    private static final String OEM_TIRE_STATUS = "OEM status";
    private static final String CHASSIS_CONFIG_STATUS = "Chassis config status";
    private static final String CHASSIS_STATUS = "Chassis status";
    private static final String THREADED_LENGTH = "Threaded length";
    private static final String DATA_BROWSER_TITLE = "Data Browser page";
    private static final String CHASIS_COUNT_NUMBER = "Chasis count number";
    private static final String TPMS_COUNT_NUMBER = "TPMS Count number";
    private static final String SUM_OF_NOTES = "Sum of Notes";
    private static final String ASSEMBLY_ROW_2 = "Assembly row 2";
    private static final String DOUBLE_CLICK = "double click";
    private static final String SAVE = "save";
    private static final String MENU_BUTTON = "Menu Button";
    private static final String DISPLAY_CHANGE_SPECIAL_NOTES = "Display/change special notes";
    private static final String DISPLAY_CHANGE_TPMS_NOTES = "Display/change TPMS notes";
    private static final String DISPLAY_CHANGE_LIFT_NOTES = "Display/change lift notes";
    private static final String OEM_TABLE_WIDTH = "OEM Tire / Wheel (Assemblies) Width";
    private static final String OEM_TABLE_ASPECT_RATIO = "OEM Tire / Wheel (Assemblies) Aspect Ratio";
    private static final String OEM_TABLE_TIRE_DIAMETER = "OEM Tire / Wheel (Assemblies) Tire Diameter";
    private static final String INPUT_FIELD_HELP_BUTTON ="input button";
    private static final String TPMS_ENTRY ="TPMS Entry";
    private static final String LIST = "list";
    private static final int MIN = 1;
    private static final int MAX = 5000;
    private static final int GET_STATUS_CODE = 200;

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case TPMS_ENTRY:
                return tpmsEntry;
            case INPUT_FIELD_HELP_BUTTON:
                return inputFieldHelpButton;
            case OEM_TABLE_TIRE_DIAMETER:
                return oemTableTireDiameter;
            case OEM_TABLE_ASPECT_RATIO:
                return oemTableAspectRatio;
            case OEM_TABLE_WIDTH:
                return oemTableWidth;
            case DISPLAY_CHANGE_LIFT_NOTES:
                return displayChangeLiftNotes;
            case DISPLAY_CHANGE_TPMS_NOTES:
                return displayChangeTpmsNotes;
            case DISPLAY_CHANGE_SPECIAL_NOTES:
                return displayChangeSpecialNotes;
            case MENU_BUTTON:
                return menuButton;
            case VEHICLE_DROP_DOWN_BUTTON:
                return vehicleClassDropDown;
            case YEAR_DROP_DOWN_BUTTON:
                return yearDropDown;
            case MAKE_DROP_DOWN_BUTTON:
                return makeDropDown;
            case MODEL_DROP_DOWN_BUTTON:
                return modelDropDown;
            case INPUT_BOX_1:
                return inputBox1;
            case INPUT_BOX_2:
                return inputBox2;
            case INPUT_BOX_3:
                return inputBox3;
            case INPUT_BOX_4:
                return inputBox4;
            case INPUT_BOX_5:
                return inputBox5;
            case INPUT_BOX_6:
                return inputBox6;
            case CAB_STYLE:
                return cabStyle;
            case TRIM_ALPHA_NUMERIC:
                return trimAlphaNumeric;
            case TRIM_LETTERS:
                return trimLetters;
            case TRIM_NAMES:
                return trimNames;
            case DRIVE_TRIM_NAMES:
                return driveTrimNames;
            case TRANSMISSION:
                return transmission;
            case DELETE_A_TRIM:
                return deleteATrim;
            case DELETE_AN_ASSEMBLY:
                return deleteAnAssembly;
            case TRIM_DROPDOWN:
                return trimDropdown;
            case CLOSE_BUTTON:
                return closeButton;
            case INPUT_BOX_1_TEXT:
                return inputbox1Text;
            case INPUT_BOX_2_TEXT:
                return inputbox2Text;
            case INPUT_BOX_3_TEXT:
                return inputbox3Text;
            case INPUT_BOX_4_TEXT:
                return inputbox4Text;
            case INPUT_BOX_5_TEXT:
                return inputbox5Text;
            case INPUT_BOX_6_TEXT:
                return inputbox6Text;
            case SAVED_MESSAGE:
                return savedMessage;
            case INPUT_TEXT_BOX:
                return inputTextBox;
            case SAVE_MESSAGE:
                return saveMessage;
            case TEXT_NOTES:
                return textNotes;
            case INPUT_SPECIAL_NOTES:
                return inputSpecialNotes;
            case INPUT_TPMS_NOTES:
                return inputTpmsNotes;
            case INPUT_LIFT_NOTES:
                return inputLiftNotes;
            case CREATE_NEW_REAR_AXILE_ROW:
                return createNewRearAxile;
            case CREATE_NEW_OVER_SIZED_ROW:
                return createNewOverSizedTire;
            case DELETE_REAR_AXLE:
                return deleteRearAxle;
            case DELETE_OVER_SIZED_TIRE:
                return deleteOverSizedTire;
            case CHASSIS_NUMBER:
                return chassisNumber;
            case SPECIAL_NOTES_NUMBER:
                return specialNotesNumber;
            case SPECIAL_NOTES_LINK:
                return specialNotesLink;
            case SELECT_FIRST_ROW:
                return selectFirstRow;
            case SELECT_FIRST_ROW_OF_GLOBAL_NOTES:
                return selectFirstRowOfGlobalNotes;
            case ADDED_GLOBAL_NOTES:
                return addedGlobalNotes;
            case REMOVE_GLOBAL_NOTES_SELECTION_2:
                return removeAddedGlobalNotes2;
            case SAVE_BUTTON_IN_IFRAME:
                return saveButtonInIframe;
            case V_SEG_CAT_ID:
                return vSegCatId;
            case TABLE_NAME:
                return tableName;
            case VEHICLE_ID:
                return vehicleID;
            case VEHICLE_MAKE:
                return vehicleMake;
            case VEHICLE_CLASS:
                return vehicleClass;
            case YEAR:
                return year;
            case VEHICLE_MODEL:
                return vehicleModel;
            case VEHICLE_ID_FROM_VEHLIST:
                return vehicleIDFromVehList;
            case VEH_SEG_CAT_ID:
                return vehSegCatID;
            case GLOBAL_SCROLL_BAR:
                return scrollNext;
            case TPMS_DATA_SCROLL_BAR:
                return tpmsDataTableScrollBar;
            case TPMS_DATA_CREATE_RECORD:
                return tpmsDataRowCreate;
            case COPY_BUTTON:
                return copyButton;
            case TPMS_DELETE_BUTTON:
                return tpmsDeleteButton;
            case CREATE_ASSEMBLIES:
                return createAssemblies;
            case WIDTH_OF_FRONT_WHEEL:
                return widthOfFrontWheel;
            case WIDTH_OF_REAR_WHEEL:
                return widthOfRearWheel;
            case ASPECT_RATIO_OF_FRONT_WHEEL:
                return aspectRatioOfFrontWheel;
            case ASPECT_RATIO_OF_REAR_WHEEL:
                return aspectRatioOfRearWheel;
            case TIRE_DIAMETER_OF_FRONT_WHEEL:
                return tireDiameterOfFrontWheel;
            case TIRE_DIAMETER_OF_REAR_WHEEL:
                return tireDiameterOfRearWheel;
            case LOAD_INDEX_OF_FRONT_WHEEL:
                return loadIndexOfFrontWheel;
            case LOAD_INDEX_OF_REAR_WHEEL:
                return loadIndexOfRearWheel;
            case CROSS_SECTION_VALUE:
                return crossSectionValue;
            case ASPECT_RATIO_VALUE:
                return aspectRatioValue;
            case WHEEL_DIAMETER_VALUE:
                return wheelDiameterValue;
            case LOAD_INDEX_VALUE:
                return loadIndexValue;
            case FIND_BUTTON:
                return findButton;
            case TRIM_SUB_MODEL:
                return trimSubModel;
            case TRIM_SUB_MODEL_TWO:
                return trimSubModelTwo;
            case SEARCH_TERM:
                return searchTerm;
            case TRIM_SUB_MODEL_DESCRIPTION:
                if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    return trimSubModelDescriptionInStage;
                } else {
                    return trimSubModelDescription;
                }
            case VEH_SEG_CAT_ID_REPORT:
                if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    return vehSegCatIdReportInStage;
                } else {
                    return vehSegCatIdReport;
                }
            case INPUT_INFO_SHEET_SEARCH:
                return inputInfoSheetSearch;
            case INFO_SHEET_SEARCH_RESULT:
                return infoSheetSearchResults;
            case MAIL_BUTTON:
                return mailButton;
            case EMPLOYEE_NAME:
                return empName;
            case STORE_ID:
                return storeId;
            case SEARCH_STRING:
                return searchString;
            case KEYWORD_SEARCH_RESULT:
                return keywordSearchResults;
            case ABAP_PROGRAM_NAME:
                return abapProgramName;
            case INITIALIZATION:
                return initialization;
            case TPMS_CREATE_RECORD_BUTTON:
                return tpmsCreateRecordButton;
            case TYPE_TPMS_ARTICLE_NR:
                return typeTpmsArticleNr;
            case SELECT_FIRST_TPMS_ROW:
                return selectFirstTpmsRow;
            case TPMS_DELETE_ROW:
                return tpmsDeleteRow;
            case ASSEMBLY_SCROLL_BAR:
                return assemblyScrollBar;
            case SEARCH_INPUT_BOX:
                return searchInputBox;
            case ASSEMBLY_HORIZONTAL_SCROLL_BAR:
                return assemblyHorizontalScrollBar;
            case ARTICLE_LIST_SELECTION:
                return articleListSelection;
            case ADDED_LAST_ARTICLE:
                return addedLastArticle;
            case TDID_INPUT:
                return tdidInput;
            case FIXED_COLUMNS_NR:
                return fixedColumnsNr;
            case OEM_TIRE_STATUS:
                return oemTireStatus;
            case CHASSIS_CONFIG_STATUS:
                return chassisConfigStatus;
            case CHASSIS_STATUS:
                return chassisStatus;
            case THREADED_LENGTH:
                return threadedLength;
            case DATA_BROWSER_TITLE:
                return dataBrowserTitle;
            case CHASIS_COUNT_NUMBER:
                return chasisCountNumber;
            case TPMS_COUNT_NUMBER:
                return tpmsCountNumber;
            case ASSEMBLY_ROW_2:
                return assemblyRow2;
            default:
                Assert.fail("FAIL: Could not find element that matched string passed from step");
                return null;
        }
    }

    /**
     * return the By Element.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public By returnByElement(String elementName) {
        LOGGER.info("returnByElement started");
        switch (elementName) {
            case CHASSIS_CONFIG_SELECTION:
                return listOfChassisConfigSelectionCountBy;
            case LIST_GLOBAL_NOTES_SELECTION:
                return listOfGlobalNotesSelectionCountBy;
            case GLOBAL_NOTES_SELECTION:
                return globalNotesSelectionCountBy;
            case GLOBAL_NOTES_ADDED:
                return globalNotesAddedCountBy;
            case LAST_ROW:
                return lastNameRowBy;
            case DELETE_LAST_ROW:
                return lastRowToDeleteBy;
            case TPMS_LAST_ROW:
                return tpmsDataRowCountBy;
            case TPMS_ROW_DELETE:
                return tpmsRowDeleteBy;
            case SPECIAL_NOTES_GLOBAL_SELECTION:
                return globalNotesSpecialNotesCountBy;
            case GLOBAL_NOTES_ADDED_2:
                return globalNotesAddedCount2By;
            case ARTICLE_LIST_LAST_ROW:
                return articleListBy;
            case LAST_ASSEMBLY_ROW:
                return lastAssemblyRowBy;
            default:
                Assert.fail("FAIL: Could not find By element that matched string passed from step");
                return null;
        }
    }

    /**
     * Click elements in Vehicle page
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void clickInVehiclePage(String elementName) {
        LOGGER.info("clickInVehiclePage started");
        commonActions.click(returnElement(elementName));

        LOGGER.info("clickInVehiclePage completed");
    }

    /**
     * Function scrolls to element and clicks
     *
     * @param elementName - Name of the Web Element
     */
    public void scrollToElementAndClick(String elementName) {
        LOGGER.info("scrolltoElementAndClick started");
        driver.jsScrollToElementClick(returnElement(elementName));
        LOGGER.info("scrolltoElementAndClick completed");
    }

    /**
     * Verifies Message is displayed on vehicle page
     *
     * @param elementName string to verify the value of
     * @param message     Message to check on vehicle page
     */
    public void assertTextDisplayedInVehicle(String elementName, String message) {
        LOGGER.info("assertTextDisplayedInVehicle started");
        driver.verifyTextDisplayed(returnElement(elementName), message);
        LOGGER.info("assertTextDisplayedInVehicle completed");
    }

    /**
     * Send keys and random keys into elements in Vehicle page
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     * @param inputText   - value to enter
     */
    public void sendKeysInVehiclePage(String elementName, String inputText) {
        LOGGER.info("sendKeysInVehiclePage started");
        driver.waitForPageToLoad();
        if (inputText.contains(RANDOM)) {
            inputText = inputText + CommonUtils.getRandomNumber(MIN, MAX);
            driver.scenarioData.setData(inputText.substring(0, inputText.indexOf("_") - 1), inputText);
        }
        commonActions.sendKeys(returnElement(elementName), inputText);
        LOGGER.info("sendKeysInVehiclePage completed");
    }

    /**
     * Method returns to last row of the table
     *
     * @param elementName        string to verify the value of
     * @param tableScrollbarName table element which we want to scroll down
     * @return WebElement
     */
    public WebElement getLastRow(String elementName, String tableScrollbarName) {
        LOGGER.info("getLastRow started");
        driver.waitForPageToLoad();
        List<WebElement> rowCount;
        int lastRow;
        rowCount = webDriver.findElements(returnByElement(elementName));
        lastRow = rowCount.size() - 1;
        while (!rowCount.get(lastRow).isDisplayed()) {
            returnElement(tableScrollbarName).click();
            if (rowCount.get(lastRow).isDisplayed()) {
                break;
            }
        }
        LOGGER.info("getLastRow completed");
        return rowCount.get(lastRow);
    }

    /**
     * Verifies displayed value matches expected value in vehicle page
     *
     * @param elementName string to verify the value of
     */
    public void assertTextDisplayedMatchesValExpectedInVehiclePage(String elementName, String expectedText) {
        LOGGER.info("assertValDisplayedMatchesValExpectedInVehiclePage started");
        driver.waitForPageToLoad();
        if (elementName.equalsIgnoreCase(LAST_ROW)) {
            if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                Assert.assertEquals("FAIL: Text not found as per required. ", expectedText, getLastRow(LAST_ROW, GLOBAL_SCROLL_BAR).getAttribute(Constants.VALUE));
            } else {
                Assert.assertEquals("FAIL: Text not found as per required. ", expectedText, getLastRow(LAST_ROW, GLOBAL_SCROLL_BAR).getText());
            }
        } else {
            driver.checkIfElementContainsText(returnElement(elementName), expectedText);
        }
        LOGGER.info("assertValDisplayedMatchesValExpectedInVehiclePage completed");
    }

    /**
     * Method verifies displayed value matches saved expected value in vehicle page
     *
     * @param elementName string to verify the value of
     * @param key         String of saved primary name in HashMap
     */
    public void assertValueOnVehiclePage(String elementName, String key) {
        LOGGER.info("assertValueOnVehiclePage started");
        driver.waitForPageToLoad();
        int length = key.length();
        driver.assertElementAttributeString(returnElement(elementName), Constants.VALUE, driver.scenarioData.getData(key.substring(key.indexOf("_") + 1, length)));
        LOGGER.info("assertValueOnVehiclePage completed");
    }

    /**
     * Saves the chasis number in Vehicle page
     *
     * @param key         String of saved primary name in HashMap
     * @param elementName string to verify the value of
     */
    public void saveChassisNumbers(String key, String elementName) {
        LOGGER.info("saveChassisNumbers started");
        driver.scenarioData.genericData.put(key, returnElement(elementName).getAttribute(Constants.VALUE).replaceAll("[^0-9]", ""));
        LOGGER.info("saveChassisNumbers completed");
    }

    /**
     * Verifies the difference in value between the two chassis numbers in vehicle page
     *
     * @param key1 String of saved primary name in HashMap with first chassis number
     * @param key2 String of saved primary name in HashMap with second chassis number
     */
    public void verifyCountDifference(String key1, String key2) {
        LOGGER.info("verifyCountDifference started");
        int previousNumber1 = Integer.parseInt(driver.scenarioData.genericData.get(key1));
        int updatedNumber2 = Integer.parseInt(driver.scenarioData.genericData.get(key2));
        Assert.assertEquals("FAIL: Number is not in Sequence ", 1, updatedNumber2 - previousNumber1);
        LOGGER.info("verifyCountDifference completed");

    }

    /**
     * Method selects the row based on user specified row index value
     *
     * @param elementName String to get the value byElement
     * @param index       Integer of which row you want to select
     */
    public void selectIndexRowByElement(int index, String elementName) {
        LOGGER.info("selectIndexRowByElement started");
        driver.waitForPageToLoad();
        List<WebElement> selection = webDriver.findElements(returnByElement(elementName));
        selection.get(index).click();
        LOGGER.info("selectIndexRowByElement completed");
    }

    /**
     * Saves the notes number in Vehicle page
     *
     * @param key         String of saved primary name in HashMap
     * @param elementName string to verify the value of
     */
    public void saveNumbers(String key, String elementName) {
        LOGGER.info("saveNumbers started");
        driver.scenarioData.genericData.put(key, returnElement(elementName).getText().replaceAll("[^0-9]", ""));
        LOGGER.info("saveNumbers completed");
    }

    /**
     * Saves the element's value. Key is the element name.
     *
     * @param elementName String who's value should be saved into HashMap
     */
    public void saveValueFromVehiclePage(String elementName) {
        LOGGER.info("saveValueFromVehiclePage started");
        driver.jsScrollToElement(returnElement(elementName));
        driver.scenarioData.genericData.put(elementName, returnElement(elementName).getAttribute(Constants.VALUE));
        LOGGER.info("saveValueFromVehiclePage completed");
    }

    /**
     * Saves the element's text. Key is the element name.
     *
     * @param elementName String who's text should be saved into HashMap
     */
    public void saveTextFromVehiclePage(String elementName) {
        LOGGER.info("saveTextFromVehiclePage started");
        driver.scenarioData.genericData.put(elementName, returnElement(elementName).getText());
        LOGGER.info("saveTextFromVehiclePage completed");
    }

    /**
     * Sends saved vales into element provided.
     *
     * @param elementName string where the saved hashmap value has to be entered
     * @param key         String key from hasmap
     */
    public void enterSavedValuesIntoElement(String key, String elementName) {
        LOGGER.info("enterSavedValuesIntoElement started");
        driver.performSendKeysWithActions(returnElement(elementName), driver.scenarioData.genericData.get(key));
        LOGGER.info("enterSavedValuesIntoElement completed");
    }

    /**
     * Verifies the difference in value between the two chassis numbers in vehicle page
     *
     * @param key1 String of saved primary name in HashMap with first vehicle segment ID
     * @param key2 String of saved primary name in HashMap with second vehicle segment ID
     */
    public void compareVehicleSegmentCategoryIDS(String key1, String key2) {
        LOGGER.info("compareVehicleSegmentCategoryIDS started");
        try {
            int vehicleSegmentCategoryID1 = Integer.parseInt(driver.scenarioData.genericData.get(key1));
            int vehicleSegmentCategoryID2 = Integer.parseInt(driver.scenarioData.genericData.get(key2));
            Assert.assertEquals("FAIL:Values are not equal ", vehicleSegmentCategoryID1, vehicleSegmentCategoryID2);
        } catch (NumberFormatException nfe) {
            compareVehicleTrimSubModel(key1, key2);
        }
        LOGGER.info("compareVehicleSegmentCategoryIDS completed");
    }

    /**
     * Compares the values between the two trim/sub numbers in vehicle page and vehicle segment category list
     *
     * @param key1 String of saved value in HashMap with first vehicle trim model
     * @param key2 String of saved value in HashMap with second vehicle trim model
     */
    public void compareVehicleTrimSubModel(String key1, String key2) {
        LOGGER.info("compareVehicleTrimSubModel started");
        String firstValue = driver.scenarioData.genericData.get(key1);
        String secondValue = driver.scenarioData.genericData.get(key2);
        Assert.assertEquals("FAIL:Values are not equal ", firstValue, secondValue);
        LOGGER.info("compareVehicleTrimSubModel completed");
    }

    /**
     * Sends text into field with out clear
     *
     * @param elementName Name of the element where data has to enter
     * @param text String of text which we want to send into input box
     */
    public void sendKeysWithOutClear(String text, String elementName) {
        LOGGER.info("sendKeysWithOutClear started");
        driver.waitForPageToLoad();
        driver.performSendKeysWithActions(returnElement(elementName), text);
        LOGGER.info("sendKeysWithOutClear completed");
    }

    /**
     * Converting elements to integers to add them together
     *
     * @param elementName1 String to be converted for addition
     * @param elementName2 Value to be converted for addition
     */
    public void addTwoDifferentAttributes(String elementName1, String elementName2) {
        LOGGER.info("addTwoDifferentAttributes started");
        int sum = Integer.valueOf(returnElement(elementName1).getText())+
        Integer.valueOf(returnElement(elementName2).getAttribute(Constants.VALUE));
        driver.scenarioData.genericData.put(SUM_OF_NOTES ,Integer.toString(sum));
        LOGGER.info("addTwoDifferentAttributes completed");
    }

    /**
     * Saves the Title contains number into hashmap
     *
     * @param key - String to retrieve the data from HashMap
     * @param elementName element name to get the value.
     */
    public void savesTitleValueIntoHashMap(String elementName, String key) {
        LOGGER.info("savesTitleValueIntoHashMap started");
        driver.waitForPageToLoad();
        String title = returnElement(elementName).getText();
        driver.scenarioData.genericData.put(key, title.replaceAll("[^0-9]", ""));
        LOGGER.info("savesTitleValueIntoHashMap completed");
    }

    /**
     * Method enters data into table last row in vehicle page
     *
     * @param value data which needs to entered
     * @param columnNumber postion of column number
     * @param tableName Name of the table where data to be entered
     */
    public void enterDataIntoTablelastRow(String value, String tableName, String columnNumber ){
        LOGGER.info("enterDataIntoTablelastRow started");
        driver.waitForPageToLoad();
        List<WebElement> rowCount = webDriver.findElements(By.xpath(commonActions.TEXT_XPATH + tableName + "']/following::*[contains(@id, '," + columnNumber +"#if-r')]"));
        commonActions.sendKeys(rowCount.get(rowCount.size() - 1), value);
        LOGGER.info("enterDataIntoTablelastRow completed");
    }

    /**
     * Method enters data into table last row in vehicle page
     *
     * @param row integer which row we want to select
     * @param columnNumber postion of column number
     * @param tableName Name of the table where data to be entered
     */
    public void doubleClickOrSaveTableSpecifiedRowRow(String action, int row, String tableName, String columnNumber ){
        LOGGER.info("doubleClickOrSaveTableSpecifiedRowRow started");
        driver.waitForPageToLoad();
        List<WebElement> rowCount = webDriver.findElements(By.xpath(commonActions.TEXT_XPATH + tableName + "']/following::*[contains(@id, '," + columnNumber +"#if-r')]"));
        switch (action){
            case DOUBLE_CLICK:
                driver.doubleClick(rowCount.get(row));
                break;
            case SAVE:
                driver.scenarioData.genericData.put(tableName, rowCount.get(row).getText());
                break;
            default:
                Assert.fail("FAIL: Could not find action that matched string passed from step");
        }
        LOGGER.info("doubleClickOrSaveTableSpecifiedRowRow completed");
    }

    /**
     * Selects row in a table in vehicle page
     *
     * @param row integer which row we want to select
     * @param tableName Name of the table where data to be entered
     */
    public void selectsRowInATable(String row, String tableName){
        LOGGER.info("selectsRowInATable started");
        driver.waitForPageToLoad();
        List<WebElement> rowCount = webDriver.findElements(By.xpath(commonActions.TEXT_XPATH + tableName + "']/following::*[contains(@id,'#"+row+",0')]"));
        commonActions.click(rowCount.get(Constants.ZERO));
        LOGGER.info("selectsRowInATable completed");
    }

    /**
     * Returns URL based on the environment.
     *
     * @return : Url of required Web-Application
     */
    public String getUrlForCurrentEnvironment() {
        LOGGER.info("getUrlForCurrentEnvironment started");
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            return "http://stg.fitment.trtc.com/vehicle_notes/api/veh_notes_srch.xsjs?id='ZGBL'";
        } else {
            return "http://fithanlq01:8000/vehicle_notes/api/veh_notes_srch.xsjs?id='ZGBL'";
        }
    }

    /**
     * Asserts the response code for Get call
     */
    public void checkURL() {
        LOGGER.info("checkURL started");
        given().get(getUrlForCurrentEnvironment()).then().statusCode(GET_STATUS_CODE);
        LOGGER.info("checkURL completed");
    }

    /**
     * Gets the response body and count of node from request call
     *
     * @param key - Key name on which value is retrived
     */
    public void getBodyAndSaveData(String key) {
        LOGGER.info("getBodyAndSaveData started");
        driver.scenarioData.genericData.put(key, String.valueOf(given().get(getUrlForCurrentEnvironment()).getBody().jsonPath().getList(LIST).size()));
        LOGGER.info("getBodyAndSaveData completed");
    }
}
