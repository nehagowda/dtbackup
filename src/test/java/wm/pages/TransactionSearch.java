package wm.pages;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.WebElement;
import common.Constants;
import commonUtils.ExcelUtils;
import common.Config;
import utilities.Driver;
import org.junit.Assert;
import wm.utilities.WmExcel;


/**
 * Created by mnabizadeh on 5/18/18.
 */
public class TransactionSearch {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(TransactionSearch.class.getName());
    private ExcelUtils excelUtils;
    private WmExcel wmExcel;
    private String orderNumber;

    public TransactionSearch(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        wmExcel = new WmExcel();
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "wm_login-username")
    private static WebElement wmUser;

    @FindBy(id = "wm_login-password")
    private static WebElement wmPassword;

    @FindBy(id = "submit_login")
    private static WebElement loginButton;

    @FindBy(xpath = "//input[contains(@id,':searchBarForm:searchBarControl:RFP:htmlInputText4')]")
    private static WebElement functionId;

    @FindBy(id = "jsfwmp16507:searchBarForm:searchBarControl:RFP:groupNameInput__text")
    private static WebElement groupNameInput;

    @FindBy(id = "jsfwmp16507:searchBarForm:searchBarControl:RFP:interfaceNameInput__text")
    private static WebElement interfaceNameInput;

    @FindBy(xpath = "//select[contains(@id,'searchBarForm:searchBarControl:RFP:dateRangeInput__range')]")
    private static WebElement dateRangeDropdown;

    @FindBy(xpath = "//*[contains(@id, 'htmlGraphicImage6')]")
    private static WebElement completedStatus;

    @FindBy(xpath = "//button[contains(@id,':searchBarForm:searchBarControl:refinedSearchGoButton')]")
    private static WebElement searchButton;

    @FindBy(className = "caf-table")
    private static WebElement transactionSearchResultTable;

    @FindBy(xpath = "//input[@id='jsfwmp16507:searchBarForm:searchBarControl:RFP:adminModeBooleanCheckbox']")
    private static WebElement verboseCheckbox;

    @FindBy(xpath = "//span[text()='No matches found']")
    private static WebElement noMatchesFound;

    @FindBy(id = "jsfwmp16507:searchBarForm:searchBarControl:RFP:functionalAreaInput__text")
    private static WebElement functionalAreaInput;

    private static final By logMessageXpathBy = By.xpath("//span[contains(@title, 'Successfully created SAP PO ')]");

    private static final By tBodyBy = By.tagName("tbody");

    private static final By searchButtonBy = By.xpath("//button[contains(@id,':searchBarForm:searchBarControl:refinedSearchGoButton')]");

    private static final String USING_PO = "SAP PO";

    private static final String SALES_ORDER = "Sales Order";

    private static final String NOT_DISPLAYED = "display:none;";

    /**
     * Goes to WM home page
     */
    public void goWMHome() {
        LOGGER.info("goWMHome started");
        driver.getUrl(Config.getBaseUrl());
        driver.waitForPageToLoad();
        LOGGER.info("goWMHome completed");
    }

    /**
     * Logs in to WM using UN and PW
     */
    public void login() {
        LOGGER.info("login started");
        driver.waitForElementVisible(wmUser);
        wmUser.sendKeys(Config.getWMUserName());
        wmPassword.sendKeys(Config.getWMPassword());
        driver.waitForElementClickable(loginButton);
        loginButton.click();
        LOGGER.info("login completed");
    }

    /**
     * Creates excelUtils object
     *
     * @param file Specifies the excel file to find the location for
     */
    private ExcelUtils setupExcel(String file) {
        LOGGER.info("setupExcel started");
        String location = Constants.EXTEND_ASSORT_ORDER_DATA_FILE_IN + file + Constants.EXTEND_ASSORT_EXCEL_FILE_EXTENSION;
        excelUtils = new ExcelUtils(location);
        LOGGER.info("setupExcel completed");
        return excelUtils;
    }

    /**
     * Pulls hybris order number from excel and returns for use by the next step
     *
     * @param file Specifies the excel file to read from
     */
    public void hybrisOrderNumberExcel(String file) throws Exception {
        LOGGER.info("hybrisOrderNumberExcel started");
        ExcelUtils myOrder = setupExcel(file);
        orderNumber = myOrder.getCellValue(1, 2, 0);
        LOGGER.info("hybrisOrderNumberExcel completed");
    }

    /**
     * Looks up order number/transaction number on Interface Monitor page
     */
    public void wmFunctionIdSearch() {
        LOGGER.info("wmFunctionIdSearch started");
        driver.waitForElementVisible(functionId);
        String orderNumber = driver.scenarioData.getCurrentOrderNumber();
        functionId.sendKeys(orderNumber);
        LOGGER.info("wmFunctionIdSearch completed");
    }

    /**
     * This will send keys into function id element in Web Method
     */
    public void wmFunctionIdSearchForPosDm() {
        LOGGER.info("wmFunctionIdSearchForPosDm started");
        driver.waitForElementVisible(functionId);
        functionId.sendKeys(driver.scenarioData.getData("invoice"));
        LOGGER.info("wmFunctionIdSearchForPosDm completed");
    }

    /**
     * Selects a dropdown value from a Date Range dropdown
     *
     * @param dateRange dropdown value to select
     */
    public void selectDateRange(String dateRange) {
        LOGGER.info("selectDateRange started");
        driver.waitForElementClickable(dateRangeDropdown);
        driver.selectFromDropdownByVisibleText(dateRangeDropdown, dateRange);
        LOGGER.info("selectDateRange completed");
    }

    /**
     * Clicks Search button
     */
    public void searchButton() {
        LOGGER.info("searchButton started");
        driver.waitForElementClickable(searchButton);
        searchButton.click();
        driver.waitForPageToLoad();
        LOGGER.info("searchButton completed");
    }

    /**
     * Asserts that the completed status icon is present
     */
    public void assertCompletedStatus() {
        LOGGER.info("assertCompletedStatus started");
        driver.waitForElementVisible(completedStatus);
        Assert.assertTrue("FAIL: Completed Icon status not present", completedStatus.isDisplayed());
        LOGGER.info("assertCompletedStatus completed");
    }

    /**
     * Takes parameter from step and sends to Group Name input.
     *
     * @param selection String of input value
     */
    public void fillGroupNameInput(String selection) {
        LOGGER.info("fillGroupNameInput started");
        driver.waitForElementClickable(groupNameInput);
        groupNameInput.clear();
        groupNameInput.sendKeys(selection);
        groupNameInput.click();
        driver.waitForMilliseconds();
        LOGGER.info("fillGroupNameInput completed");
    }

    /**
     * Takes parameter from step and sends to Interface Name input.
     *
     * @param selection String of input value
     */
    public void fillInterfaceNameInput(String selection) {
        LOGGER.info("fillInterfaceNameInput started");
        driver.waitForElementClickable(interfaceNameInput);
        interfaceNameInput.clear();
        interfaceNameInput.sendKeys(selection);
        driver.waitForMilliseconds();
        interfaceNameInput.click();
        LOGGER.info("fillInterfaceNameInput completed");
    }

    /**
     * Pulls the Purchase Order numbers for each returned search item and saves them to the extended assortment excel.
     */
    public void extractAndSavePONumberToExcel() throws Exception {
        LOGGER.info("extractAndSavePONumberToExcel started");
        driver.waitForElementVisible(transactionSearchResultTable);
        WebElement tableBody = transactionSearchResultTable.findElement(tBodyBy);
        List<WebElement> rows = tableBody.findElements(logMessageXpathBy);
        int index = 1;
        for (WebElement row : rows) {
            if (row.getAttribute(Constants.STYLE).equalsIgnoreCase(NOT_DISPLAYED)) {
                continue;
            } else {
                String logMessage = row.getAttribute(Constants.TITLE);
                if (logMessage.contains(USING_PO) && row.isDisplayed()) {
                    String poNumber = logMessage.split("#")[2].replaceAll("[^\\d.]", "");
                    wmExcel.savePONumberToExcel(Constants.PURCHASE_ORDER_EXCEL, poNumber, index);
                    index++;
                }
            }
        }
        LOGGER.info("extractAndSavePONumberToExcel completed");
    }

    /**
     * Pulls the purchase order number from the returned search result.
     */
    public void extractAndSavePONumberToScenarioData() {
        LOGGER.info("extractAndSavePONumberToScenarioData started");
        driver.waitForElementVisible(transactionSearchResultTable);
        WebElement tableBody = transactionSearchResultTable.findElement(tBodyBy);
        List<WebElement> rows = tableBody.findElements(logMessageXpathBy);
        for (WebElement row : rows) {
            if (row.getAttribute(Constants.STYLE).equalsIgnoreCase(NOT_DISPLAYED)) {
                continue;
            } else {
                String logMessage = row.getAttribute(Constants.TITLE);
                if (logMessage.contains(USING_PO)) {
                    String poNumber = (logMessage.replaceAll("[^\\d]", ""));
                    Assert.assertTrue("FAIL: Could not find automated PO Number ", poNumber.length() > 1);
                    driver.scenarioData.setCurrentPurchaseOrderNumber(poNumber);
                    break;
                }
            }
        }
        LOGGER.info("extractAndSavePONumberToScenarioData completed");
    }

    /**
     * Verifies that when a search in web method is done that the result is a transaction that doesn't pass through to
     * SAP.
     */
    public void assertTransactionResultsReturn() {
        LOGGER.info("assertTransactionResultsReturn started");
        driver.waitForElementVisible(transactionSearchResultTable);
        List<WebElement> rows = transactionSearchResultTable.findElements(tBodyBy);
        for (WebElement row : rows) {
            if (row.getAttribute(Constants.STYLE).equalsIgnoreCase(NOT_DISPLAYED)) {
                continue;
            } else {
                String logMessage = row.getAttribute(Constants.TITLE);
                if (logMessage.contains(SALES_ORDER) && row.isDisplayed()) {
                    Assert.assertTrue("FAIL: the messaging found was not as expected, and couldn't determine" +
                            " the status", logMessage.contains(driver.scenarioData.getCurrentOrderNumber()));
                    break;
                }
            }
        }
        LOGGER.info("assertTransactionResultsReturn completed");
    }

    /**
     * Checks the verbose logging checkbox which ensures automated PO is displayed
     */
    public void selectVerboseLogging() {
        LOGGER.info("selectVerboseLogging started");
        driver.waitForElementClickable(interfaceNameInput);
        verboseCheckbox.click();
        LOGGER.info("selectVerboseLogging completed");
    }

    /**
     *This method will enter the functional area in which we want to search our order number
     *
     * @param text - Text which Specify the functional Area. Example: POSDM Processing or Sales Order
     */
    public void selectFunctionalAreaInputAndEnter(String text) {
        LOGGER.info("selectFunctionalAreaInputAndEnter started");
        driver.waitForElementVisible(functionalAreaInput);
        functionalAreaInput.sendKeys(text);
        LOGGER.info("selectFunctionalAreaInputAndEnter completed");
    }

    /**
     * Pulls the purchase order numbers from the returned search for each article
     */
    public void extractAndSavePONumbersToScenarioData() {
        LOGGER.info("extractAndSavePONumbersToScenarioData started");
        driver.waitForElementVisible(transactionSearchResultTable);
        WebElement tableBody = transactionSearchResultTable.findElement(tBodyBy);
        List<WebElement> rows = tableBody.findElements(logMessageXpathBy);
        for (WebElement row : rows) {
            if (row.getAttribute(Constants.STYLE).equalsIgnoreCase(NOT_DISPLAYED)) {
                continue;
            } else {
                String logMessage = row.getAttribute(Constants.TITLE);
                if (logMessage.contains(USING_PO)) {
                    String poNumber = (logMessage.replaceAll("[^\\d]", ""));
                    String vendor = logMessage.substring(0, 19);
                    vendor = vendor.substring(vendor.length() - 3);
                    driver.scenarioData.setPOVendorData(vendor, poNumber);
                    break;
                }
            }
        }
        LOGGER.info("extractAndSavePONumbersToScenarioData completed");
    }

    public void waitForStatusToBeVisible() {
        LOGGER.info("waitForStatusToBeVisible started");
        int counter = 10;
        while (counter != 0) {
            if(noMatchesFound.isDisplayed()==false){
                driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
                try {
                    searchButton.click();
                }catch (StaleElementReferenceException s) {
                    WebElement element = webDriver.findElement(searchButtonBy);
                    element.click();
                }
                break;
            }
            if (noMatchesFound.isDisplayed()) {
                driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
               try {
                   searchButton.click();
               }catch (StaleElementReferenceException s) {
                   WebElement element = webDriver.findElement(searchButtonBy);
                   element.click();
               }

            }
            counter--;
        }
        LOGGER.info("waitForStatusToBeVisible completed");
    }
}