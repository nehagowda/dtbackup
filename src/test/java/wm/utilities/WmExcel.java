package wm.utilities;

import common.Constants;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.logging.Logger;

public class WmExcel {

    private final static Logger LOGGER = Logger.getLogger(WmExcel.class.getName());

    private static final String TEST = "test";

    /**
     * Saves purchaseOrderData to external excel file
     *
     * @param purchaseOrderData Passed through from extracted WebMethod search
     */
    public void savePONumberToExcel(String filePath, String purchaseOrderData, int rowIndex) throws Exception {
        LOGGER.info("savePONumberToExcel started");
        try {
            File excel = new File(filePath);
            FileInputStream file = new FileInputStream(excel);
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheet(TEST);
            Row orderRow = sheet.getRow(rowIndex);
            orderRow.createCell(4).setCellValue(purchaseOrderData);

            if (excel.canWrite()) {
                FileOutputStream fileOut = new FileOutputStream(filePath);
                FileChannel channel = fileOut.getChannel();
                FileLock lock = channel.lock();
                workbook.write(fileOut);
                lock.release();
                fileOut.close();
                workbook.close();
                file.close();
            } else {
                FileLock lock = file.getChannel().tryLock();
                lock.release();
            }
            LOGGER.info("Purchase order number: " + purchaseOrderData + " was successfully saved to excel");

        } catch (IOException err) {
            Thread.sleep(Constants.FIVE); //wait for other process to unlock file

            try {
                FileLock lock = new RandomAccessFile(new File(filePath), "rw").getChannel().tryLock();
                lock.release();
                savePONumberToExcel(filePath, purchaseOrderData, rowIndex);
            } catch (Exception e) {
                Assert.fail("FAIL: File located at '" + filePath +
                        "' is currently locked by another process and cannot be accessed!");
            }
        }
        LOGGER.info("savePONumberToExcel completed");
    }
}
