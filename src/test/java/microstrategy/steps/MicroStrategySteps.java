package microstrategy.steps;

import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.en.When;
import microstrategy.pages.MicroStrategy;
import org.openqa.selenium.WebDriver;
import microstrategy.pages.CommonActions;
import utilities.Driver;

/**
 * Created by mnabizadeh on 04/09/19.
 */

public class MicroStrategySteps {

    private MicroStrategy microStrategy;
    private CommonActions commonActions;

    public MicroStrategySteps(Driver driver){

        microStrategy = new MicroStrategy(driver);
        commonActions = new CommonActions(driver);
    }

    @When("^I click on \"([^\"]*)\" on MicroStrategy homepage$")
    public void i_click_on_on_microstrategy_homepage(String elementName) throws Throwable {
        microStrategy.clickOnText(elementName);
    }

    @When("^I click on exact \"([^\"]*)\" on MicroStrategy page$")
    public void i_click_on_exact_on_microstrategy_page(String text) throws Throwable {
        commonActions.clickOnExactText(text);
    }

    @When("^I enter \"([^\"]*)\" into the field under \"([^\"]*)\" on MicroStrategy page$")
    public void i_enter_into_the_field_under_on_microStrategy_page(String value, String element) throws Throwable {
        commonActions.sendKeys(microStrategy.returnElement(element), value);
    }

    @When("^I double click on \"([^\"]*)\" on MicroStrategy homepage$")
    public void i_double_click_on_on_microstrategy_homepage(String elementName) throws Throwable {
        commonActions.doubleClickOnElement(microStrategy.returnElement(elementName));
    }

    @When("^I click on \"([^\"]*)\" arrow$")
    public void i_click_on_previous_month_arrow(String elementName) throws Throwable {
        microStrategy.clickOnText(elementName);
    }

    @When("^I select \"([^\"]*)\" day of the month$")
    public void i_select_day_of_the_month(String elementName) throws Throwable {
        microStrategy.clickOnText(elementName);
    }
}