package microstrategy.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.logging.Logger;


/**
 * Created by mnabizadeh on 04/09/19.
 */

public class CommonActions {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(sap.pages.CommonActions.class.getName());

    public CommonActions(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
    }

    /**
     * Clicks the element
     *
     * @param element WebElement to be clicked
     */
    public void click(WebElement element) {
        LOGGER.info("click started " + element);
        driver.waitForPageToLoad();
        try {
            element.click();
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(element));
            staleElement.click();
        }
        LOGGER.info("click completed");
    }

    /**
     * This method will extract the locator from an element and returns as By object
     *
     * @param element - WebElement
     * @return - By object
     */
    public By extractByLocatorFromWebElement(WebElement element) {
        LOGGER.info("extractByLocatorFromWebElement started");
        String elementContent = element.toString();
        if (elementContent.toLowerCase().contains("by.id")) {
            String idOfElement = elementContent.substring(elementContent.indexOf("By.id:") + 6).replaceAll(" ", "").replaceAll("'", "");
            return By.id(idOfElement);
        } else if (elementContent.toLowerCase().contains("xpath")) {
            String uncleanXpath = elementContent.substring(elementContent.indexOf("xpath:") + 6).replaceAll(" ", "");
            String cleanXpath = uncleanXpath.substring(0, uncleanXpath.length() - 1);
            return By.xpath(cleanXpath);
        }
        LOGGER.info("extractByLocatorFromWebElement completed");
        return null;
    }

    /**
     * Method to Click on WebElement with Text
     * @param text Text of the WebElement
     */
    public void clickOnExactText(String text){
        LOGGER.info("clickOnExactText started");
        driver.waitForPageToLoad();
        driver.waitForMilliseconds();
        webDriver.findElement(By.xpath("//*[text()='" + text + "']")).click();
        LOGGER.info("clickOnExactText completed");
    }

    /**
     * This method returns the web element of the input field with label name passed
     *
     * @param labelName - Name of the input field
     */
    public WebElement returnInputFieldWithLabel(String labelName) {
        return webDriver.findElement(By.xpath("//*[text()='" + labelName + "']/following::input[1]"));
    }

    /**
     * Sends input
     *
     * @param element - WebELement
     * @param input   - Input text
     */
    public void sendKeys(WebElement element, String input) {
        LOGGER.info("sendKeys started");
        driver.waitForPageToLoad();
        try {
            driver.clearInputAndSendKeys(element, input);
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(element));
            driver.clearInputAndSendKeys(staleElement, input);
        } catch (InvalidElementStateException i) {
            driver.performSendKeysWithActions(element, input);
        }
        LOGGER.info("sendKeys completed");
    }

    /**
     * Locates the input field with the field's name and will send keys into the input field
     *
     * @param labelName - Name of the input field
     * @param value     - Input value
     */
    public void sendKeysWithLabelName(String labelName, String value) {
        LOGGER.info("sendKeysWithLabelName started");
        driver.waitForPageToLoad();
        sendKeys(returnInputFieldWithLabel(labelName), value);
        LOGGER.info("sendKeysWithLabelName completed");
    }

    /**
     * Double click on element.
     *
     * @param element - text of the element.
     */
    public void doubleClickOnElement(WebElement element) {
        LOGGER.info("doubleClickOnElement started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(element);
        driver.doubleClickControl(element);
        LOGGER.info("doubleClickOnElement completed");
    }

}