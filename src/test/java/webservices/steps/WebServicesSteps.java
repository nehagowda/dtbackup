package webservices.steps;

import common.Config;
import common.Constants;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.data.Product;
import dtc.pages.CommonActions;
import utilities.CommonUtils;
import utilities.Driver;
import webservices.pages.WebServices;

public class WebServicesSteps {
    private WebServices webService;
    private Driver driver;
    private Scenario scenario;
    private Product product;
    private CommonActions commonActions;

    public WebServicesSteps(Driver driver) {
        this.driver = driver;
        webService = new WebServices(driver);
        product = new Product();
        commonActions = new CommonActions(driver);
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @Then("^I verify if this URL \"([^\"]*)\" is up or not$")
    public void i_verify_if_this_uri_is_up_or_not(String URI) throws Throwable {
        webService.assertGetResponseForWebServiceUrl(URI);
    }

    @Then("^I verify the post method for this URI \"([^\"]*)\" with this endpoint \"([^\"]*)\" and request file with name \"([^\"]*)\"$")
    public void i_verify_the_post_method_for_this_URI_with_this_endpoint_and_request_file_with_name(String URI, String endPoint, String fileName) throws Throwable {
        webService.assertPostResponseForWebServiceUrl(URI, endPoint, fileName);
    }

    @Then("^I save the response body node \"([^\"]*)\" count from request \"([^\"]*)\" GET call$")
    public void i_save_the_response_body_node_count_from_request_get_call(String key, String URI) throws Throwable {
        webService.getBodyAndSaveData(key, URI);
    }

    @When("^I make a Post call to 'oauth/token' endpoint and capture the token$")
    public void i_make_a_post_call_to_endpoint_and_capture_the_token() throws Throwable {
        webService.postResponseForHttpsSecureTokenWebServiceUrl();
    }

    @When("^I do a Get call to \"([^\"]*)\" endpoint for article \"([^\"]*)\" and customer price list \"([^\"]*)\" and catalog version \"([^\"]*)\" and the previously saved token$")
    public void i_do_a_get_call_to_endpoint_for_article_and_customer_price_list_and_catalog_version_and_the_previously_saved_token(String endpoint, String article, String customerPriceList, String catalogVersion) throws Throwable {
        webService.getResponseForPricingEndpoint(endpoint, article, customerPriceList, catalogVersion);
    }

    @Then("^I request a get response to the order look up url for site id \"([^\"]*)\"$")
    public void i_request_a_get_response_to_the_order_look_up_url_for_site_id(String siteId) throws Throwable {
        webService.assertResponseForOrderLookUpService(siteId);
    }

    @When("^I save Total Price, Article Codes, Quantities, and line item amount from the request body$")
    public void i_save_total_price_article_codes_quantities_and_line_item_amount_from_the_request_body() throws Throwable {
        webService.parseJsonAndSaveValues();
    }

    @When("I set appointment Flag to true")
    public void i_set_appointment_flag_to_true() throws Throwable {
        WebServices.isAppointment = true;
    }

    @Then("^I verify the \"(service order|order)\" created successfully with \"([^\"]*)\" entry$")
    public void i_verify_the_order_created_successfully_with_selected_entry(String orderType, String orderEntry) throws Throwable {
        i_make_a_post_call_to_endpoint_and_capture_the_token();
        String siteId;
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            siteId = Constants.SITE_ID_DT;
        } else {
            siteId = Constants.SITE_ID_DTD;
        }
        webService.assertResponseForOrderLookUpService(siteId);
        webService.assertEntriesInOrderLookUpService(orderType, orderEntry);
    }

    @Then("^I verify below attributes and values \"(exist|doesnot exist|exist notEqual)\" in response body json$")
    public void i_verify_below_attributes_and_values_in_response_body_json(String presence, DataTable table) throws Throwable {
        webService.verifyFollowingAttributesInResponse(presence, table);
    }

    @Then("^I verify the schema file \"([^\"]*)\" with response data in .xml \"([^\"]*)\"$")
    public void i_verify_the_schema_file_with_response_data_in_xml(String schemaFile, String responseDataFile) throws Throwable {
        webService.xmlSchemaValidation(schemaFile, responseDataFile);
    }

    @When("^I request a GET call to customer service$")
    public void i_request_a_getcall_to_customer_service() throws Throwable {
        webService.getCustomerService();
    }

    @Then("^I verify below attributes and values exist in response body .xml$")
    public void i_verify_below_attributes_and_values_exist_in_response_body_xml(DataTable table) throws Throwable {
        webService.makeSureFollowingAttributesInResponseXml(table);
    }

    @When("^I do Get call to My Account customer service with \"([^\"]*)\" email and token passed$")
    public void i_do_get_call_to_my_account_customer_service_with_email_and_token_passed(String emailValue) throws Throwable {
        webService.getResponseForMyAccountCustomerService(emailValue);
    }

    @When("^I add a json request body using the file \"([^\"]*)\" to AVS service for customer type \"([^\"]*)\" and address type \"([^\"]*)\"$")
    public void i_add_a_json_request_body_using_the_file_to_avs_service_for_customer_type_and_address_type(String dataFileByState, String customerType, String addressType) throws Throwable {
        webService.addAJsonRequestDataFromFile(dataFileByState, customerType, addressType);
    }

    @When("^I do GET call to AVS service and return complete address for address type \"([^\"]*)\"$")
    public void i_do_get_call_to_avs_service_and_return_complete_address_for_address_type(String addressType) throws Throwable {
        webService.getAvsResponseBody(addressType);
    }

    @When("^I make a post call to car item header service with field name \"([^\"]*)\"$")
    public void i_make_a_post_call_to_car_item_header_service_with_field_name(String fieldName) throws Throwable {
        if (!Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            webService.returnJsonResponseForItemLevelOrderHistoryServiceRequestBody(fieldName);
            webService.setDataForCarItemOrderHistoryValidation(fieldName);
        }
    }

    @When("^I make a post call to car header service for Customer with Email/Customer ID \"([^\"]*)\"$")
    public void i_make_a_post_call_to_car_header_service_with_field_name(String emailOrCustomerId) throws Throwable {
        if (!Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            webService.assertDataForHybrisCustomerCarHeaderOrderHistoryValidation(emailOrCustomerId);
        }
    }

    @When("^I make a get request with vin \"([^\"]*)\" to vtv car service and return response$")
    public void i_make_a_get_request_with_vin_to_vtv_car_service_and_return_response(String vin) throws Throwable {
        webService.getServiceResponseForVtv(vin);
    }
}