package fleet.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fleet.pages.AutoIntegrate;
import fleet.pages.Fleet;
import sap.pages.CommonActions;
import utilities.Driver;

/**
 * Created by schandra on 04/19/19.
 */

public class AutoIntegrateSteps {
    private Driver driver;
    private Fleet fleet;
    private AutoIntegrate autoIntegrate;
    private CommonActions commonActions;

    public AutoIntegrateSteps(Driver driver) {
        this.driver = driver;
        fleet = new Fleet(driver);
        autoIntegrate = new AutoIntegrate(driver);
        commonActions = new CommonActions(driver);
    }

    @When("I enter username and password to login into Auto Integrate")
    public void i_enter_username_and_password_to_login_into_auto_integrate() throws Throwable {
        autoIntegrate.loginToAutoIntegrate();
    }

    @When("I search for repair order")
    public void i_search_for_repair_order() throws Throwable {
        autoIntegrate.searchForRepairOrderId();
    }

    @When("I click on \"([^\"]*)\" on AutoIntegrate app")
    public void i_click_on_autointegrate_app(String elementName) throws Throwable {
        fleet.click(autoIntegrate.returnElement(elementName));
    }

    @When("I click on VIEW on AutoIntegrate app")
    public void i_click_on_view_on_autointegrate_app() throws Throwable {
        autoIntegrate.clickViewElementForRepairOrder();
    }

    @When("I close multiple orders pop up when appears")
    public void i_close_multiple_orders_pop_up_appears() throws Throwable {
        autoIntegrate.closePopUpIfAppears();
    }

    @Then("I verify if the transaction is approved and save authorization number to scenario data")
    public void i_verify_if_the_transaction_is_approved_and_save_authorizaion_number_to_scenario_data() throws Throwable {
        autoIntegrate.verifyApprovalAndSaveAuthorizationId();
    }

    @Then("I verify if the transaction is rejected")
    public void i_verify_if_the_transaction_is_rejected() throws Throwable {
        autoIntegrate.verifyRejection();
    }

    @When("I select \"([^\"]*)\" as reject reason on AutoIntegrate App")
    public void i_select_as_reject_reason_on_autointegrate_app(String text) throws Throwable {
        autoIntegrate.selectOptionWithVisibleText(text);
    }

    @When("^I enter \"([^\"]*)\" in \"([^\"]*)\" field in AutoIntegrate App$")
    public void i_enter_in_field(String value, String element) throws Throwable {
        fleet.sendKeys(autoIntegrate.returnElement(element), value);
    }

    @Then("^I verify line item attributes from fleet app is being displayed in autointegrate$")
    public void i_verify_line_item_attributes_from_fleet_app_is_being_displayed_in_autointegrate() throws Throwable {
        autoIntegrate.assertLineItemDetails();
    }

    @Then("I verify Tread depth values for Tire at location \"([^\"]*)\" in AutoIntegrate")
    public void i_verify_tread_depths_for_tire_at_location_in_autointegrate(String tireLocation) throws Throwable {
        autoIntegrate.assertTreadDepthValuesInAutoIntegrate(Fleet.O,tireLocation);
        autoIntegrate.assertTreadDepthValuesInAutoIntegrate(Fleet.M,tireLocation);
        autoIntegrate.assertTreadDepthValuesInAutoIntegrate(Fleet.I,tireLocation);
    }
}