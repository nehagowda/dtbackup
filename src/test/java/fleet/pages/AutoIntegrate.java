package fleet.pages;

import common.Config;
import common.Constants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import sap.pages.CommonActions;
import utilities.Driver;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by schandra on 04/19/19.
 */

public class AutoIntegrate {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private Actions actions;
    private final Logger LOGGER = Logger.getLogger(AutoIntegrate.class.getName());

    public AutoIntegrate(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        actions = new Actions(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//*[text()='RO ID']/following::input[not(contains(@type,'hidden'))][1]")
    private static WebElement repairOrderIdField;

    @FindBy(id = "MainContent_btnSearch")
    private static WebElement searchButton;

    @FindBy(xpath = "//*[contains(@value,'Approve Remaining')]")
    private static WebElement approveRemaining;

    @FindBy(xpath = "//*[contains(@value,'Save')]")
    private static WebElement save;

    @FindBy(xpath = "//*[contains(@value,'Reject Remaining')]")
    private static WebElement rejectRemaining;

    @FindBy(xpath = "//*[contains(@value,'Validate And Submit')]")
    private static WebElement validateAndSubmitButton;

    @FindBy(xpath = "//*[@value='Close']")
    private static WebElement close;

    @FindBy(xpath = "//*[@value='Submit To Shop']")
    private static WebElement submitToShop;

    @FindBy(id = "MainContent_ucRepairOrderDetails_ucMultipleRepairOrderInformationPopup_ucPopup_pnlPopup")
    private static WebElement multipleRepairOrderInformationPopUp;

    @FindBy(id = "MainContent_ucRepairOrderStatus_liGeneralMessage")
    private static WebElement generalMessage;

    @FindBy(xpath = "//*[@class='ro-header']")
    private static WebElement roHeader;

    @FindBy(id = "MainContent_ucRepairOrderStatus_lblAuthorizationNumber")
    private static WebElement authNumber;

    @FindBy(id = "MainContent_ucRepairOrderDetails_ucRepairOrderItemRejection_ucPopup_ctl01_ddlRejectionReason")
    private static WebElement rejectReasonDropDown;

    @FindBy(xpath = "//*[text()='Notes']/following::textarea[1]")
    private static WebElement notes;

    @FindBy(xpath = "//*[contains(@value,'Cancel')]")
    private static WebElement cancel;

    private static By lineItemContainerBy = By.xpath("//td[@class='detail-col']");
    private static By codeBy = By.xpath("//dt[contains(text(),'Code') and not (text()='Service Code')]/following::dd[1]");
    private static By lineItemTotalBy = By.xpath("//td[@class='total-col']");
    private static By quantityBy = By.xpath("//td[@class='qty-col']");
    private static By unitCostBy = By.xpath("//td[@class='unit-cost-col']");

    private static final String SEARCH = "Search";
    private static final String APPROVE_REMAINING = "Approve Remaining";
    private static final String REJECT_REMAINING = "Reject Remaining";
    private static final String VALIDATE_AND_SUBMIT = "Validate and Submit";
    private static final String SUBMIT_TO_SHOP = "Submit To Shop";
    private static final String AI_REFERENCE_KEY = "AI Reference";
    private static final String SUBMIT_MESSAGE = "has been submitted";
    private static final String APPROVED = "Approved";
    private static final String AUTH_ID_KEY = "Authorization number";
    private static final String USERNAME = "Username";
    private static final String PASSWORD = "Password";
    private static final String NOTES = "Notes";
    private static final String SAVE = "Save";
    private static final String PLACEHOLDER = "Placeholder";
    private static final String USED = "used";
    private static final String LF = "LF";
    private static final String RF = "RF";
    private static final String RR = "RR";
    private static final String LR = "LR";
    private static final String RRI = "RRI";
    private static final String LRI = "LRI";
    private static final String SP = "SP";
    private static final String FRONT_RIGHT = "FrontRight";
    private static final String FRONT_LEFT = "FrontLeft";
    private static final String REAR_RIGHT = "RearRight";
    private static final String REAR_LEFT = "RearLeft";
    private static final String REAR_RIGHT_INSIDE = "RearRightInside";
    private static final String REAR_LEFT_INSIDE = "RearLeftInside";
    private static final String SPARE = "Spare";
    private static final String OUTER = "Outer";
    private static final String MIDDLE = "Middle";
    private static final String INNER = "Inner";
    private static final String CANCEL = "Cancel";


    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case SEARCH:
                return searchButton;
            case APPROVE_REMAINING:
                return approveRemaining;
            case REJECT_REMAINING:
                return rejectRemaining;
            case VALIDATE_AND_SUBMIT:
                return validateAndSubmitButton;
            case SUBMIT_TO_SHOP:
                return submitToShop;
            case NOTES:
                return notes;
            case SAVE:
                return save;
            case CANCEL:
                return cancel;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * login to AutoIntegrate application
     */
    public void loginToAutoIntegrate() {
        LOGGER.info("loginToAutoIntegrate started");
        commonActions.sendKeysWithLabelName(USERNAME, Config.getAutoIntegrateUser());
        commonActions.sendKeysWithLabelName(PASSWORD, Config.getAutoIntegratePassword());
        driver.performKeyAction(Keys.ENTER);
        LOGGER.info("loginToAutoIntegrate completed");
    }

    /**
     * Enter AI Reference number in repair order manager and search for the record.
     */
    public void searchForRepairOrderId() {
        LOGGER.info("searchForRepairOrderId started");
        driver.clearInputAndSendKeys(repairOrderIdField, driver.scenarioData.getData(AI_REFERENCE_KEY));
        commonActions.click(searchButton);
        LOGGER.info("searchForRepairOrderId completed");
    }

    /**
     * Selects the repair order
     */
    public void clickViewElementForRepairOrder() {
        commonActions.click(webDriver.findElement(By.xpath("//*[text()='" + driver.scenarioData.getData
                (AI_REFERENCE_KEY) + "']/following::a[text()='View'][1]")));
    }

    /**
     * Closes the multiple order or duplicate order popup
     */
    public void closePopUpIfAppears() {
        LOGGER.info("closePopUpIfAppears started");
        driver.waitForPageToLoad();
        if (driver.isElementDisplayed(multipleRepairOrderInformationPopUp)) {
            actions.moveToElement(close).click().build().perform();
        }
        LOGGER.info("closePopUpIfAppears completed");
    }

    /**
     * Verify if the approval was successful and save authorization id.
     */
    public void verifyApprovalAndSaveAuthorizationId() {
        LOGGER.info("verifyApprovalAndSaveAuthorizationId started");
        Assert.assertTrue("FAIL: Transaction was not approved for repair order number: " +
                        driver.scenarioData.getData(AI_REFERENCE_KEY),
                generalMessage.getText().contains(driver.scenarioData.getData(AI_REFERENCE_KEY)));
        Assert.assertTrue("FAIL: Transaction was not approved for repair order number: " +
                        driver.scenarioData.getData(AI_REFERENCE_KEY),
                roHeader.getText().contains(driver.scenarioData.getData(AI_REFERENCE_KEY)));
        Assert.assertTrue("FAIL: Transaction was not approved for repair order number: " +
                        driver.scenarioData.getData(AI_REFERENCE_KEY),
                generalMessage.getText().contains(SUBMIT_MESSAGE));
        Assert.assertTrue("FAIL: Transaction was not approved for repair order number: " +
                        driver.scenarioData.getData(AI_REFERENCE_KEY),
                roHeader.getText().contains(APPROVED));
        saveAuthorizationNumber();
        LOGGER.info("verifyApprovalAndSaveAuthorizationId completed");
    }

    /**
     * Code to verify if the approval was successful and save authorization id.
     */
    public void verifyRejection() {
        LOGGER.info("verifyRejection started");
        Assert.assertTrue("FAIL: Transaction was not approved for repair order number: " +
                        driver.scenarioData.getData(AI_REFERENCE_KEY),
                generalMessage.getText().contains(driver.scenarioData.getData(AI_REFERENCE_KEY)));
        Assert.assertTrue("FAIL: Transaction was not approved for repair order number: " +
                        driver.scenarioData.getData(AI_REFERENCE_KEY),
                roHeader.getText().contains(driver.scenarioData.getData(AI_REFERENCE_KEY)));
        Assert.assertTrue("FAIL: Transaction was not approved for repair order number: " +
                        driver.scenarioData.getData(AI_REFERENCE_KEY),
                generalMessage.getText().contains(SUBMIT_MESSAGE));
        Assert.assertTrue("FAIL: Transaction was not approved for repair order number: " +
                        driver.scenarioData.getData(AI_REFERENCE_KEY),
                roHeader.getText().contains("Awaiting"));
        LOGGER.info("verifyRejection completed");
    }

    /**
     * saves Authorization Number
     */
    private void saveAuthorizationNumber() {
        LOGGER.info("saveAuthorizationNumber started");
        driver.waitForPageToLoad();
        driver.scenarioData.setData(AUTH_ID_KEY, authNumber.getText().replaceAll("[^\\d]", ""));
        LOGGER.info("saveAuthorizationNumber completed");
    }

    /**
     * Selects the options in drop down with visible text
     *
     * @param text - Visible text of the element
     */
    public void selectOptionWithVisibleText(String text) {
        LOGGER.info("selectOptionWithVisibleText started");
        driver.waitForPageToLoad();
        Select rejectDropDown = new Select(rejectReasonDropDown);
        rejectDropDown.selectByVisibleText(text);
        LOGGER.info("selectOptionWithVisibleText completed");
    }

    /**
     * Asserts Line items in AutoIntegrate
     */
    public void assertLineItemDetails() {
        List<WebElement> lineItemContainers = webDriver.findElements(lineItemContainerBy);
        HashMap<Integer, String> indexMap = new HashMap<>();

        for (Integer k = 1; k <= Fleet.totalLineItems; k++) {
            indexMap.put(k, PLACEHOLDER);
        }
        Assert.assertEquals(lineItemContainers.size(), Fleet.totalLineItems);
        for (int i = 1; i <= Fleet.totalLineItems; i++) {
            for (int j = 0; j < lineItemContainers.size(); j++) {
                actions.moveToElement(lineItemContainers.get(j)).build().perform();
                if (indexMap.get(i).equalsIgnoreCase(PLACEHOLDER)) {
                    if (lineItemContainers.get(j).getText().contains
                            (driver.scenarioData.getData(Fleet.PRODUCT + (i)))) {
                        Assert.assertTrue("FAIL: Expected: " +
                                        driver.scenarioData.getData(Fleet.SIZE + (i))
                                        + ", Actual: " + lineItemContainers.get(j).getText(),
                                lineItemContainers.get(j).getText().contains(driver.scenarioData.
                                        getData(Fleet.SIZE + (i))));
                        Assert.assertTrue("FAIL: Expected: " + driver.scenarioData.
                                        getData(Fleet.DESCRIPTION + (i)) + ", Actual: " +
                                        lineItemContainers.get(j).getText(),
                                lineItemContainers.get(j).getText().contains(driver.scenarioData.
                                        getData(Fleet.DESCRIPTION + (i))));
                        Assert.assertTrue("FAIL: Expected: " + driver.scenarioData.
                                        getData(Fleet.TOTAL + (i)) + ", Actual: " + webDriver.
                                        findElements(lineItemTotalBy).get(j).getText(),
                                webDriver.findElements(lineItemTotalBy).get(j).getText().
                                        contains(driver.scenarioData.getData(Fleet.TOTAL + (i))));
                        Assert.assertTrue("FAIL: Expected: " + driver.scenarioData.
                                        getData(Fleet.QUANTITY + (i)) + ", Actual: " + webDriver.findElements
                                        (quantityBy).get(j).getText(),
                                webDriver.findElements(quantityBy).get(j).getText().
                                        contains(driver.scenarioData.getData(Fleet.QUANTITY + (i))));
                        Assert.assertTrue("FAIL: Expected: " + driver.scenarioData.
                                        getData(Fleet.QUANTITY + (i)) + ", Actual: " + webDriver.
                                        findElements(unitCostBy).get(j).getText(),
                                webDriver.findElements(unitCostBy).get(j).getText().
                                        contains(driver.scenarioData.getData(Fleet.PRICE + (i))));
                        indexMap.put(i, USED);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Returns the AutoIntegrate wording for Fleet App's Tread Depth tire location and Tread depth words.
     */
    public String returnElementNameForTreadDepth(String input) {
        LOGGER.info("returnElementNameForTreadDepth started");
        switch (input) {
            case LF:
                return FRONT_LEFT;
            case RF:
                return FRONT_RIGHT;
            case LR:
                return REAR_LEFT;
            case RR:
                return REAR_RIGHT;
            case LRI:
                return REAR_LEFT_INSIDE;
            case RRI:
                return REAR_RIGHT_INSIDE;
            case SP:
                return SPARE;
            case Fleet.O:
                return OUTER;
            case Fleet.M:
                return MIDDLE;
            case Fleet.I:
                return INNER;
            default:
                Assert.fail("Please check the input. Input specified has no logic in " +
                        "returnElementNameForTreadDepth method");
                return null;
        }
    }

    /**
     * returns Web Element for Tread Depth fields in AutoIntegrate.
     *
     * @param omi - Outer, Middle and Inner.
     * @param tirelocation - LF, RF etc
     * @return WebElement
     */
    private WebElement returnTreadDepthElements(String omi, String tirelocation) {
        LOGGER.info("returnTreadDepthElements started");
        return webDriver.findElement(By.xpath("//*[contains(@name,'" + returnElementNameForTreadDepth(tirelocation) +
                returnElementNameForTreadDepth(omi) + "')]"));
    }

    /**
     * Asserts Texts in TreadDepth fields.
     *
     * @param omi
     * @param tireLocation
     */
    public void assertTreadDepthValuesInAutoIntegrate(String omi, String tireLocation) {
        LOGGER.info("assertTreadDepthValuesInAutoIntegrate started");
        Assert.assertTrue("FAIL: Tread Depth for Tire at location: " + tireLocation + " is not as expected: "
                + omi + " value of: " + driver.scenarioData.getData(omi + tireLocation), driver.scenarioData.
                getData(omi + tireLocation).equalsIgnoreCase(returnTreadDepthElements(omi, tireLocation)
                .getAttribute(Constants.VALUE)));
        LOGGER.info("assertTreadDepthValuesInAutoIntegrate completed");
    }
}