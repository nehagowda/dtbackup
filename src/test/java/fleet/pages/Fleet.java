package fleet.pages;


import common.Config;
import common.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;
import org.openqa.selenium.support.FindBy;
import org.junit.Assert;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

/**
 * Created by mnabizadeh on 04/02/19.
 */

public class Fleet {

    private Driver driver;
    private WebDriver webDriver;
    private Actions actions;
    private final Logger LOGGER = Logger.getLogger(Fleet.class.getName());

    public Fleet(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        actions = new Actions(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//input[@placeholder='AZP 20']")
    public static WebElement storeCode;

    @FindBy(xpath = "//*[text()='VIN']//following::input[1]")
    private static WebElement vinEntry;

    @FindBy(id = "auto-isdriverwaiting")
    private static WebElement isDriverWaitingCheckBox;

    @FindBy(id = "auto-mileage")
    private static WebElement mileage;

    @FindBy(id = "auto-start-time")
    private static WebElement startTime;

    @FindBy(id = "auto-estimated-completion-time")
    private static WebElement completionTime;

    @FindBy(xpath = "//*[text()='Refresh']/preceding::span[1]")
    private static WebElement rejectReason;

    @FindBy(xpath = "//*[@title='clear']")
    private static WebElement clear;

    @FindBy(xpath = "//*[text()='VIN']/following::div[1]")
    private static WebElement vinEntryErrorElement;

    @FindBy(xpath = "//*[text()='Repair Status']/following::div[1]")
    private static WebElement vehicleDetailErrorMessage;

    @FindBy(id = "auto-isdually")
    private static WebElement duallyCheckBox;

    @FindBy(xpath = "//*[text()='Product']/parent::div")
    private static WebElement salesLineItemContainer;

    @FindBy(xpath = "//*[text()='Tax']/following::span[3]")
    private static WebElement total;

    @FindBy(xpath = "//*[text()='Subtotal']/parent::div")
    private static WebElement finalValuesContainer;

    public static final String STORE_CODE = "Store Code";
    public static String REPAIR_ID = "";
    private static final String VIN = "VIN";
    private static final String DRIVER_WAITING = "Driver Waiting";
    private static final String MILEAGE = "Mileage";
    private static final String START_TIME = "Start time";
    private static final String END_TIME = "End Time";
    public static final String FLEET_REPAIR_ID_KEY = "RepairNumber";
    private static final String REJECT_REASON = "Reject Reason";
    private static final String FLEET_URL_STORE_DEV = "http://dev.fleet.trtc.com/?storeCode=";
    private static final String FLEET_URL_STORE_QA = "http://qa.fleet.trtc.com/?storeCode=";
    private static final String FLEET_URL_REPAIR_ORDER = "&repairNumber=";
    public static String STORE_ID = "Store";
    public static final String INVALID_VIN_ENTRY_ELEMENT = "VIN Error Message";
    public static final String VEHICLE_DETAIL_ERROR_ELEMENT = "Vehicle Details error message";
    public static final String DUALLY_CHECK_BOX = "Dually?";
    private static final String DROP_DOWN_REASON_ID = "auto-tire-replace-reason-";
    public static final String PRODUCT = "Product";
    public static final String SIZE = "Size";
    public static final String DESCRIPTION = "Description";
    public static final String AXLE = "Axle";
    public static final String PRICE = "Price";
    public static final String QUANTITY = "Qty";
    public static final String TOTAL = "Total";
    public static final String SUBTOTAL = "Subtotal";
    public static final String TAX = "Tax";
    public static int totalLineItems = 0;
    public static final String O = "O";
    public static final String M = "M";
    public static final String I = "I";

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case STORE_CODE:
                return storeCode;
            case VIN:
                return vinEntry;
            case DRIVER_WAITING:
                return isDriverWaitingCheckBox;
            case MILEAGE:
                return mileage;
            case START_TIME:
                return startTime;
            case END_TIME:
                return completionTime;
            case REJECT_REASON:
                return rejectReason;
            case INVALID_VIN_ENTRY_ELEMENT:
                return vinEntryErrorElement;
            case VEHICLE_DETAIL_ERROR_ELEMENT:
                return vehicleDetailErrorMessage;
            case DUALLY_CHECK_BOX:
                return duallyCheckBox;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * This method will extract the locator from an element and returns as By object
     *
     * @param element - WebElement
     * @return - By object
     */
    private By extractByLocatorFromWebElement(WebElement element) {
        LOGGER.info("extractByLocatorFromWebElement started");
        String elementContent = element.toString();
        if (elementContent.toLowerCase().contains("by.id")) {
            String idOfElement = elementContent.substring(elementContent.indexOf("By.id:") + 6).replaceAll(" ",
                    "").replaceAll("'", "");
            return By.id(idOfElement);
        } else if (elementContent.toLowerCase().contains("xpath")) {
            String uncleanXpath = elementContent.substring(elementContent.indexOf("xpath:") + 6).replaceAll(" ",
                    "");
            String cleanXpath = uncleanXpath.substring(0, uncleanXpath.length() - 1);
            return By.xpath(cleanXpath);
        }
        LOGGER.info("extractByLocatorFromWebElement completed");
        return null;
    }

    /**
     * Sends input
     *
     * @param element - WebElement
     * @param input   - Input text
     */
    public void sendKeys(WebElement element, String input) {
        LOGGER.info("sendKeys started");
        driver.waitForPageToLoad();
        try {
            driver.clearInputAndSendKeys(element, input);
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(element));
            driver.clearInputAndSendKeys(staleElement, input);
        } catch (InvalidElementStateException i) {
            driver.performSendKeysWithActions(element, input);
        }
        LOGGER.info("sendKeys completed");
    }


    /**
     * Asserts the response of post call
     *
     * @param fileName - FileName of the json file in saved in resources directory
     * @param store    - Store code
     */
    public void getFleetRepairNumber(String fileName, String store) {
        LOGGER.info("getFleetRepairNumber started");
        String filepath;
        filepath = System.getProperty(Constants.USER_DIR) + Constants.PATH_TO_JSON_OBJECTS + fileName;
        File file = new File(filepath);
        STORE_ID = store;
        REPAIR_ID =
                given()
                        .headers(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON).body(file).
                        when()
                        .post(Config.getGraphQLUrl() + Constants.GRAPHQL_ENDPOINT).
                        getBody().asString().replaceAll("\\D+", "").trim();

        driver.scenarioData.setData(FLEET_REPAIR_ID_KEY, REPAIR_ID);
        driver.scenarioData.setData(STORE_CODE, store);
        LOGGER.info("getFleetRepairNumber completed. Repair Number : " + REPAIR_ID);
    }

    /**
     * Clicks the element
     *
     * @param element WebElement to be clicked
     */
    public void click(WebElement element) {
        LOGGER.info("click started");
        driver.waitForPageToLoad();
        try {
            driver.waitForElementClickable(element);
            driver.moveToElementClick(element);
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(extractByLocatorFromWebElement(element));
            staleElement.click();
        } catch (UnhandledAlertException uae) {
            webDriver.switchTo().alert().accept();
        }
        LOGGER.info("click completed");
    }

    /**
     * Code to return the fleet application url with repair ID
     *
     * @return - Fleet Url with repair ID
     */
    public static String getFleetUrl() {
        if (Config.getDataSet().equalsIgnoreCase(Constants.DEV)) {
            return FLEET_URL_STORE_DEV + STORE_ID + FLEET_URL_REPAIR_ORDER + REPAIR_ID;
        } else if (Config.getDataSet().equalsIgnoreCase(Constants.QA)) {
            return FLEET_URL_STORE_QA + STORE_ID + FLEET_URL_REPAIR_ORDER + REPAIR_ID;
        } else {
            Assert.fail("FAIL: Please specify the valid -DdataSet in command line argument or add implementation " +
                    "for given -DdataSet in getFleetUrl() method");
            return null;
        }
    }

    /**
     * Code to return the Tread Depth Element.
     *
     * @param tireLocation - LF, RF, RF, RR, SP
     * @param omi          - O, M, I field
     * @return - WebElement for a specific tread depth element
     */
    private WebElement returnTreadDepthElement(String tireLocation, String omi) {
        return webDriver.findElement(By.id("auto-tread-depth-" + tireLocation.toUpperCase() + "-" + omi.toUpperCase()));
    }

    /**
     * Code to return the Web Element for Replace check box for a specific tire
     *
     * @param tireLocation - LF, RF, RF, RR, SP
     * @return - WebElement for a specific Replace check box Web Element
     */
    public WebElement returnReplaceTireElement(String tireLocation) {
        return webDriver.findElement(By.id("auto-tire-replace-" + tireLocation.toUpperCase()));
    }

    /**
     * Code to SendKeys Tread depth values for specific OMI fields and Tire.
     *
     * @param tireLocation - LF, RF, RF, RR, SP
     * @param omi          - O or M or I values
     * @param value        - Tread depth value
     */
    public void sendTreadDepthValues(String tireLocation, String omi, String value) {
        driver.clearInputAndSendKeys(returnTreadDepthElement(tireLocation, omi), value);
    }

    /**
     * Generates a random number between 1 to 32 and saves it. Enters the generated number.
     *
     * @param omi - Outer, Middle and Inner
     * @param tireLocation - LF, RF etc
     */
    public void generateSaveAndEnterRandomValuesForTreadDepth(String omi, String tireLocation) {
        LOGGER.info("generateSaveAndEnterRandomValuesForTreadDepth started");
        driver.scenarioData.setData(omi + tireLocation, String.valueOf(CommonUtils.getRandomNumber(1, 32)));
        driver.clearInputAndSendKeys(returnTreadDepthElement(tireLocation, omi), driver.scenarioData.
                getData(omi + tireLocation));
        LOGGER.info("generateSaveAndEnterRandomValuesForTreadDepth completed");
    }

    /**
     * Code to return a specific element's value
     *
     * @param detailItemName - Line item name exactly it appears on the screen
     * @return - Value of the line item
     */
    public WebElement returnVehicleOrInvoiceDetailsElement(String detailItemName) {
        return webDriver.findElement(By.xpath("//*[text()='" + detailItemName + "']/following::*[1]"));
    }

    /**
     * Save any field's data to scenario data
     *
     * @param fieldName - name of the field for which the value have to be saved.
     */
    public void saveFieldDataToScenarioData(String fieldName) {
        LOGGER.info("saveFieldDataToScenarioData started");
        driver.waitForPageToLoad();
        driver.scenarioData.setData(fieldName, returnVehicleOrInvoiceDetailsElement(fieldName).getText());
        LOGGER.info("saveFieldDataToScenarioData completed");
    }

    /**
     * Selects the reason for replacement from the drop down.
     *
     * @param tireLocation - LF, RF, LR, RR, RRI, LRI
     * @param reason       - Reason for Replacement
     */
    public void selectReasonForReplacement(String tireLocation, String reason) {
        LOGGER.info("selectReasonForReplacement started");
        String id = DROP_DOWN_REASON_ID + tireLocation.toUpperCase();
        WebElement dropDown = webDriver.findElement(By.id(id));
        dropDown.click();
        WebElement dropDownOption = webDriver.findElement(By.xpath("//*[text()='" + reason + "']"));
        actions.moveToElement(dropDownOption).click().build().perform();
        LOGGER.info("selectReasonForReplacement completed");
    }

    /**
     * returns line item elements from sales summary table
     *
     * @param rowName      - Name of row
     * @param columnNumber - Column Number of a particular row
     * @return Line Item WebElement
     */
    private WebElement returnSaleLineItemTableRowElements(String rowName, int columnNumber) {
        LOGGER.info("returnSaleLineItemTableRowElements started");
        By by = By.xpath("//*[text()='" + rowName + "']/following::span[" + columnNumber * 7 + "]");
        return webDriver.findElement(by);
    }

    /**
     * Method to find number of line items and capture attributes of each line item
     */
    public void determineNumberOfLineItemsAndSaveTheAttributes() {
        LOGGER.info("determineNumberOfLineItemsAndSaveTheAttributes started");
        List<WebElement> numberOfLineItems = salesLineItemContainer.findElements(By.tagName(Constants.SPAN));
        int numberOfColumns = numberOfLineItems.size() / 7;
        for (int eachColumn = 1; eachColumn < numberOfColumns; eachColumn++) {
            driver.scenarioData.setData(PRODUCT + eachColumn, returnSaleLineItemTableRowElements(PRODUCT, eachColumn)
                    .getText());
            driver.scenarioData.setData(SIZE + eachColumn, returnSaleLineItemTableRowElements(SIZE, eachColumn)
                    .getText());
            driver.scenarioData.setData(DESCRIPTION + eachColumn, returnSaleLineItemTableRowElements(DESCRIPTION, eachColumn)
                    .getText());
            driver.scenarioData.setData(AXLE + eachColumn, returnSaleLineItemTableRowElements(AXLE, eachColumn)
                    .getText());
            driver.scenarioData.setData(PRICE + eachColumn, returnSaleLineItemTableRowElements(PRICE, eachColumn)
                    .getText());
            driver.scenarioData.setData(QUANTITY + eachColumn, returnSaleLineItemTableRowElements(QUANTITY, eachColumn)
                    .getText());
            driver.scenarioData.setData(TOTAL + eachColumn, returnSaleLineItemTableRowElements(TOTAL, eachColumn)
                    .getText());
        }
        totalLineItems = numberOfColumns - 1;
        LOGGER.info("determineNumberOfLineItemsAndSaveTheAttributes completed");
    }

    /**
     * returns By object for Total values
     *
     * @param valueName - name of the field
     * @return By
     */
    public By returnFinalValueBy(String valueName) {
        return By.xpath("//span[text()='" + valueName + "']/following::span[1]");
    }

    /**
     * To save Subtotal, Tax and Total amount
     */
    public void saveFinalInvoiceValues() {
        LOGGER.info("saveFinalInvoiceValues started");
        driver.scenarioData.setData(SUBTOTAL, finalValuesContainer.findElement(returnFinalValueBy(SUBTOTAL)).getText());
        driver.scenarioData.setData(TAX, finalValuesContainer.findElement(returnFinalValueBy(TAX)).getText());
        driver.scenarioData.setData(TOTAL, total.getText());
        LOGGER.info("saveFinalInvoiceValues completed");
    }
}