package utilities;

/**
 * Created by aaronbriel on 9/21/16.
 */

import common.Config;
import common.Constants;
import common.ScenarioData;
import cucumber.api.Scenario;
import dtc.pages.AppointmentPage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.HashSet;
import java.util.Set;


public class Driver {

    private WebDriver webDriver;

    protected static WebDriver driver;
    public ScenarioData scenarioData;
    private static final String BROWSER = Config.getBrowser();
    private final Logger LOGGER = Logger.getLogger(AppointmentPage.class.getName());

    public Driver() {
        scenarioData = new ScenarioData();
    }

    public void initialize() {
        if (driver == null)
            createNewDriverInstance();
    }

    private static final String HEADER = "header";
    private static final String FOOTER = "footer";
    private static final String FITMENT = "fitment";
    private static final String AUTO_HEADER = "auto-header-";
    private static final String AUTO_FOOTER = "auto-footer-";
    private static final String AUTO_FITMENT = "auto-fitment-";
    private String xpathClassLocator = "//*[contains(@class, '%s')]";
    private static String PASSED = "passed";

    /**
     * Creates a new driver instance
     */
    private void createNewDriverInstance() {
        LOGGER.info("createNewDriverInstance started");
        driver = DriverFactory.getDriver(BROWSER);
        LOGGER.info("createNewDriverInstance completed");
    }

    /**
     * Returns the current driver
     *
     * @return driver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Quits the current driver session
     */
    public void destroyDriver() {
        LOGGER.info("destroyDriver started");
        try {
            driver.quit();
            driver = null;
        } catch (Exception e) {
            LOGGER.info("Exception: " + e.getMessage());
        }
        LOGGER.info("destroyDriver completed");
    }

    /**
     * Returns the remote session ID
     *
     * @return String
     */
    public String getRemoteSessionId() {
        return ((RemoteWebDriver) driver).getSessionId().toString();
    }

    /**
     * Sleeps for set time to allow for animation to complete.
     */
    public void waitForMilliseconds() {
        waitForMilliseconds(Constants.ONE_THOUSAND);
    }

    /**
     * Sleeps for specified number of milliseconds
     *
     * @param milliseconds Number of milliseconds
     */
    public void waitForMilliseconds(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            LOGGER.info(e.toString());
        }
    }

    /**
     * Waits until the element is clickable.
     * Note: Uses a max wait time of 30 seconds
     *
     * @param byElement The element to wait for
     */
    public void waitForElementClickable(By byElement) {
        waitForElementClickable(driver.findElement(byElement));
    }

    /**
     * Waits until the element is clickable.
     * Note: Uses a variable max wait time
     *
     * @param byElement     The element to wait for
     * @param timeInSeconds Integer representing the max wait time in seconds
     */
    public void waitForElementClickable(By byElement, int timeInSeconds) {
        waitForElementClickable(driver.findElement(byElement), timeInSeconds);
    }

    /**
     * Waits until the element is clickable.
     * Note: Uses a max wait time of 30 seconds
     *
     * @param element The element to wait for
     */
    public void waitForElementClickable(WebElement element) {
        waitForElementClickable(element, Constants.THIRTY);
    }

    /**
     * Waits specified number of seconds until the element is clickable.
     *
     * @param element       The element to wait for
     * @param timeInSeconds Integer representing the max wait time in seconds
     */
    public void waitForElementClickable(WebElement element, int timeInSeconds) {
        LOGGER.info("waitForElementClickable started for element '" + element +
                "' waiting up to " + timeInSeconds + " seconds");
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);

        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException e) {
            Assert.fail("FAIL: Element \"" + element + "\" NOT found in \"" + timeInSeconds
                    + "\" seconds! " + "(Full Stack Trace: " + e.toString() + ")");
        }

        resetImplicitWaitToDefault();
        LOGGER.info("waitForElementClickable completed and found element '" + element + "'");
    }

    /**
     * Determines whether an element is clickable.
     *
     * @param element       The element to evaluate
     * @param timeInSeconds The max wait time in seconds
     * @return              true or false whether the element is clickable
     */
    public boolean isElementClickable(WebElement element, int timeInSeconds) {
        LOGGER.info("isElementClickable started for element '" + element +
                "' waiting up to " + timeInSeconds + " seconds");
        boolean returnVal = true;
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);

        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException e) {
            returnVal = false;
        }

        resetImplicitWaitToDefault();
        LOGGER.info("isElementClickable completed and found element '" + element + "'");
        return returnVal;
    }

    /**
     * Waits until the by element is visible.
     *
     * @param elementBy The by element to wait for
     */
    public void waitForElementVisible(By elementBy) {
        waitForElementVisible(elementBy, Constants.ZERO);
    }

    /**
     * Waits until the by element is visible.
     *
     * @param elementBy     The by element to wait for
     * @param timeInSeconds Preceding wait time in seconds before attempting to find the element(s)
     */
    public void waitForElementVisible(By elementBy, int timeInSeconds) {
        LOGGER.info("waitForElementVisible started looking for element.");
        boolean found = false;

        waitSeconds(timeInSeconds);
        List<WebElement> elements = driver.findElements(elementBy);

        for (WebElement element : elements) {
            if (isElementDisplayed(element, Constants.ZERO)) {
                found = true;
                break;
            }
        }
        if (!found) {
            Assert.fail("FAIL: Element \"" + elementBy + "\" NOT found in \"" + timeInSeconds
                    + "\" seconds!");
        }
        LOGGER.info("waitForElementVisible completed and found element \"" + elementBy + "\"");
    }

    /**
     * Waits until the element is visible.
     * Note: Uses default 30 second max wait time
     *
     * @param element The element to wait for
     */
    public void waitForElementVisible(WebElement element) {
        waitForElementVisible(element, Constants.THIRTY);
    }

    /**
     * Waits specified time in seconds for the element to be visible.
     *
     * @param element       The element to wait for
     * @param timeInSeconds The amount of time to wait
     */
    public void waitForElementVisible(WebElement element, int timeInSeconds) {
        LOGGER.info("waitForElementVisible started looking for element: " + element +
                " with " + timeInSeconds + " wait time");
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        try {
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (TimeoutException e) {
            Assert.fail("FAIL: Element \"" + element + "\" NOT found in \"" + timeInSeconds
                    + "\" seconds! " + "(Full Stack Trace: " + e.toString() + ")");
        }
        resetImplicitWaitToDefault();
        LOGGER.info("waitForElementVisible completed and found element '" + element + "'");
    }

    /**
     * Waits until the element is not visible.
     *
     * @param element The element to wait for
     */
    public void waitForElementNotVisible(By element) {
        waitForElementNotVisible(element, Constants.THIRTY);
    }

    /**
     * Waits for a specified length of time (in seconds)until the element is not visible.
     *
     * @param element       The element to wait for
     * @param timeInSeconds Length of time in seconds to wait
     */
    public void waitForElementNotVisible(By element, int timeInSeconds) {
        LOGGER.info("waitForElementNotVisible started with wait time of '" + timeInSeconds + "' milliseconds");
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);

        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        try {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
        } catch (TimeoutException e) {
            Assert.fail("FAIL: Element '" + element + "' still visible after '" + timeInSeconds + "' seconds! " +
                    "(Full Stack Trace: " + e.toString() + ")");
        }

        resetImplicitWaitToDefault();
        LOGGER.info("waitForElementNotVisible completed looking for element NOT visible '"
                + element + "' with wait time of '" + timeInSeconds + "' milliseconds");
    }

    /**
     * Waits for a specified length of time (in seconds)until the element is visible and
     * returns a boolean indicating if the check was successful
     *
     * @param element       The element to wait for
     * @param timeInSeconds Length of time in seconds to wait
     * @return boolean Whether element was visible
     */
    public boolean isElementVisible(WebElement element, int timeInSeconds) {
        LOGGER.info("isElementVisible started for element '" + element + "'");
        boolean visible = true;
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            LOGGER.info("isElementVisible completed and found element \"" + element + "\"");
        } catch (Exception e) {
            visible = false;
        }
        resetImplicitWaitToDefault();

        return visible;
    }

    /**
     * Waits for a specified length of time (in seconds)until the element is not visible and
     * returns a boolean indicating if the check was successful
     *
     * @param element       The element to wait for
     * @param timeInSeconds Length of time in seconds to wait
     * @return boolean Whether element was not visible
     */
    public boolean isElementNotVisible(By element, int timeInSeconds) {
        LOGGER.info("isElementNotVisible started with wait time of \"" + timeInSeconds + "\" milliseconds");
        boolean notVisible = true;
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);

        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        try {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
            LOGGER.info("isElementNotVisible completed looking for element NOT visible \""
                    + element + "\" with wait time of \"" + timeInSeconds + "\" milliseconds");
        } catch (TimeoutException e) {
            notVisible = false;
        }

        resetImplicitWaitToDefault();

        return notVisible;
    }

    /**
     * Waits until the execution of the javascript returns with true.
     *
     * @param javaScript       The javascript to be executed
     * @param timeOutInSeconds The time in seconds to wait until returning a failure
     * @return boolean
     */
    public boolean pollUntil(final String javaScript, int timeOutInSeconds) {
        LOGGER.info("pollUntil started with a max wait of \"" + timeOutInSeconds + "\" seconds");
        boolean jsCondition = false;
        try {
            driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
            (new WebDriverWait(driver, timeOutInSeconds)).until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driverObject) {
                    return (Boolean) ((JavascriptExecutor) driverObject).executeScript(javaScript);
                }
            });
            jsCondition = (Boolean) ((JavascriptExecutor) driver).executeScript(javaScript);
            resetImplicitWaitToDefault();
            return jsCondition;
        } catch (Exception e) {
            LOGGER.info("pollUntil error condition caught: " + e.toString());
        }
        LOGGER.info("pollUntil completed with a max wait of \"" + timeOutInSeconds + "\" seconds");
        return jsCondition;
    }

    /**
     * Wait for the Text to be present in the given element, regardless of being displayed or not.
     *
     * @param by            Selector of the given element, which should contain the text
     * @param text          The text we are looking for
     * @param timeInSeconds The time in seconds to wait until returning a failure
     * @return boolean
     */
    public boolean waitForTextPresent(final By by, final String text, int timeInSeconds) {
        LOGGER.info("waitForTextPresent started with max wait of \"" + timeInSeconds + "\" seconds");

        boolean isPresent = false;
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        try {
            wait.until(ExpectedConditions.textToBePresentInElementLocated(by, text));
            isPresent = true;
        } catch (TimeoutException e) {
            //TODO: Leaving in place in case we wish to refactor to move the assertion here
            //Assert.fail("FAIL: Element \"" + getByValue(by) + "\" did not contain \"" + text + "\" in \"" + time
            //        + "\" seconds! " + "(Full Stack Trace: " + e.toString() + ")");
        }
        resetImplicitWaitToDefault();
        LOGGER.info("waitForTextPresent completed - '" + text + "' was " + isPresent);
        return isPresent;
    }


    /**
     * Wait for the Text to be present in the given element. Text case is ignored.
     *
     * @param by   Selector of the given element, which should contain the text
     * @param text The text we are looking for
     * @param time The time in seconds to wait until returning a failure
     * @return boolean
     */
    public boolean waitForTextPresentIgnoreCase(final By by, final String text, int time) {
        LOGGER.info("waitForTextPresentIgnoreCase started with max wait of \"" + time + "\" seconds");
        int counter = 0;
        boolean isPresent = false;
        try {
            do {
                waitForMilliseconds();
                isPresent = StringUtils.containsIgnoreCase(driver.findElement(by).getText(), text);
                counter++;
            } while (counter < time && !isPresent);
        } catch (TimeoutException e) {
            //TODO: Leaving in place in case we wish to refactor to move the assertion here
            //Assert.fail("FAIL: Element \"" + getByValue(by) + "\" did not contain \"" + text + "\" in \"" + time
            //        + "\" seconds! " + "(Full Stack Trace: " + e.toString() + ")");
        }
        LOGGER.info("waitForTextPresentIgnoreCase completed - '" + text + "' was " + isPresent);
        return isPresent;
    }

    /**
     * Waits until the element contains the specified attribute value
     *
     * @param element   The element to wait for
     * @param attribute The attribute to check
     * @param value     The attribute value to check for
     */
    public void waitForElementAttribute(WebElement element, String attribute, String value) {
        LOGGER.info("waitForElementAttribute started");
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, Constants.THIRTY);

        try {
            wait.until(ExpectedConditions.attributeContains(element, attribute, value));
        } catch (TimeoutException e) {
            Assert.fail("FAIL: Element \"" + element + "\" did not contain attribute \"" + attribute +
                    "\" value \"" + value + "\" in \"" + Constants.THIRTY
                    + "\" seconds! " + "(Full Stack Trace: " + e.toString() + ")");
        }

        driver.manage().timeouts().implicitlyWait(Constants.THIRTY, TimeUnit.SECONDS);
        LOGGER.info("waitForElementAttribute completed and found element \"" + element + "\"");
    }

    /**
     * Grabs a list of elements and returns the one with the specific attribute text
     *
     * @param element   By selector of element
     * @param attribute Attribute of element
     * @param text      Text element should contain
     * @return WebElement The WebElement to return
     */
    public WebElement getElementWithAttribute(By element, String attribute, String text) {
        LOGGER.info("getElementWithAttribute started");

        List<WebElement> objects = driver.findElements(element);
        WebElement returnElement = null;

        for (WebElement object : objects) {
            try {
                if (object.getAttribute(attribute).equalsIgnoreCase(text)) {
                    LOGGER.info("String '" + text + "' matched with rendered  ==>"
                            + object.getAttribute(attribute));
                    returnElement = object;
                }
            } catch (NullPointerException e) {
                LOGGER.info("Object \"" + getByValue(element) + "\" did not contain attribute \"" +
                        attribute + "\".");
            }
        }

        LOGGER.info("getElementWithAttribute completed");
        return returnElement;
    }

    /**
     * Returns element among multiple that contains text substring, if substring not found
     * returns null
     *
     * @param elementBy The By element to build list with
     * @param value     The value to search for with element
     * @return WebElement
     */
    public WebElement getElementWithText(By elementBy, String value) {
        return getElementWithText(elementBy, value, Constants.ONE);
    }

    /**
     * Returns element among multiple that contains text substring, if substring not found
     * returns null
     *
     * @param elementBy The By element to build list with
     * @param value     The value to search for with element
     * @param attempts  The number of attempts (seconds) to wait before attempting to find the element with text
     * @return          WebElement containing specified text or null if not containing the text
     */
    public WebElement getElementWithText(By elementBy, String value, int attempts) {
        LOGGER.info("getElementWithText started looking for element with text '" + value + "'");
        WebElement returnElement = null;
        setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        for (int i = 0; i < attempts; i++) {
            List<WebElement> elements = driver.findElements(elementBy);
            for (WebElement element : elements) {
                if (element.getText().toLowerCase().contains(value.toLowerCase())) {
                    returnElement = element;
                    break;
                }
            }
            if (returnElement != null)
                break;
        }
        resetImplicitWaitToDefault();
        LOGGER.info("getElementWithText completed looking for element with text '" + value + "'");
        return returnElement;
    }

    /**
     * Returns element among multiple that contains the numeric values within a text substring, if substring not found
     * returns null
     *
     * @param elementBy The By element to build list with
     * @param value     The value to search for with element
     * @param attempts  The number of attempts (seconds) to wait before attempting to find the element with text
     * @return          WebElement containing specified text or null if not containing the text
     */
    public WebElement getElementWithTextNumericOnly(By elementBy, String value, int attempts) {
        LOGGER.info("getElementWithTextNumericOnly started for value '" + value + "'");
        WebElement returnElement = null;
        setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        for (int i = 0; i < attempts; i++) {
            List<WebElement> elements = driver.findElements(elementBy);
            for (WebElement element : elements) {
                if (element.getText().replaceAll("[^\\d.]", "").contains(value.replaceAll("[^\\d.]", ""))) {
                    returnElement = element;
                    break;
                }
            }

            if (returnElement != null)
                break;
        }
        resetImplicitWaitToDefault();
        LOGGER.info("getElementWithTextNumericOnly completed for value '" + value + "'");
        return returnElement;
    }

    /**
     * Returns list of elements that contain text substring, if substring not found
     * returns list size will be equal to 0
     *
     * @param elementBy The By element to build list with
     * @param value     The value to search for with element
     * @param attempts  The number of attempts (seconds) to wait before attempting to find the element with text
     * @return          WebElement containing specified text or null if not containing the text
     */
    public List<WebElement> getElementsWithText(By elementBy, String value, int attempts) {
        LOGGER.info("getElementsWithText started looking for element \"" + elementBy + "\" with value \"" + value
                + "\"");
        List<WebElement> returnElements = new ArrayList<>();

        for (int i = 0; i < attempts; i++) {
            List<WebElement> elements = driver.findElements(elementBy);
            for (WebElement element : elements) {
                if (element.getText().toLowerCase().contains(value.toLowerCase())) {
                    returnElements.add(element);
                }
            }

            if (returnElements.size() > 0)
                break;

            waitForMilliseconds();
        }

        LOGGER.info("getElementsWithText completed looking for element \"" + elementBy + "\" with value \""
                + value + "\"");
        return returnElements;
    }

    /**
     * Returns list of elements that contain text substring, if substring not found
     * returns list size will be equal to 0
     *
     * @param elementBy The By element to build list with
     * @param value     The value to search for with element
     * @return          List of WebElements containing specified text
     */
    public List<WebElement> getElementsWithText(By elementBy, String value) {
        return getElementsWithText(elementBy, value, Constants.ONE);
    }

    /**
     * Returns element among multiple that contains text substring1 and substring2, if substrings not found
     * returns null
     *
     * @param elementBy The By element to build list with
     * @param value1    The value to search for with element
     * @param value2    The value to search for with element
     * @return WebElement
     */
    public List<WebElement> getElementsWithText(By elementBy, String value1, String value2) {
        LOGGER.info("getElementsWithText started looking for element \"" +
                elementBy + "\" with value \"" + value1 + "\" and value \" + value2");
        List<WebElement> returnElements = new ArrayList<>();
        try {
            List<WebElement> elements = driver.findElements(elementBy);
            for (WebElement element : elements) {
                if (element.getText().toLowerCase().contains(value1.toLowerCase()) &&
                        element.getText().toLowerCase().contains(value2.toLowerCase())) {
                    returnElements.add(element);
                }
            }
        } catch (NoSuchElementException e) {
            returnElements = null;
        }
        LOGGER.info("getElementsWithText completed looking for element \"" +
                elementBy + "\" with value \"" + value1 + "\" and value \" + value2");
        return returnElements;
    }

    /**
     * Verify the specified element exists and contains the specified text
     *
     * @param element The name of the element
     * @param text The text to verify the element contains
     */
    public WebElement getElementWithText(WebElement element, String text) {
        return getElementWithText(element, text, true);
    }

    /**
     * Verify the specified element exists and contains the specified text
     *
     * @param element       The name of the element
     * @param text          The text to verify the element contains
     * @param ignoreCase    true or false weather to ignore case in the verification
     * @return WebElement   containing the specified text
     */
    public WebElement getElementWithText(WebElement element, String text, boolean ignoreCase) {
        LOGGER.info("getElementWithText started for '" + element + "' element with text = '" + text +
                "' and ignore case = " + ignoreCase);
        Assert.assertTrue("FAIL : Element '" + element + "' is not displayed.", isElementDisplayed(element));
        String elementText = element.getText();
        boolean containsText;
        if (ignoreCase)
            containsText = CommonUtils.containsIgnoreCase(elementText, text);
        else
            containsText = elementText.contains(text);
        Assert.assertTrue("FAIL: Text is incorrect for '" + element + "'. Actual text, '" + elementText +
                "', expected to contain '" + text +"'", containsText);
        LOGGER.info("getElementWithText started for '" + element + "' element with text = '" + text +
                "' and ignore cse = " + ignoreCase);
        return  element;
    }

    /**
     * Returns element among multiple that contains matching text
     * returns null
     *
     * @param elementBy The By element to build list with
     * @param value     The text to search for with element
     * @return WebElement
     */
    public WebElement getElementWithMatchingText(By elementBy, String value) {
        LOGGER.info("getElementWithMatchingText started looking for element with value \"" + value);
        WebElement returnElement = null;
        try {
            List<WebElement> elements = driver.findElements(elementBy);
            for (WebElement element : elements) {
                if (element.getText().toLowerCase().equalsIgnoreCase(value.toLowerCase())) {
                    returnElement = element;
                }
            }
        } catch (NoSuchElementException e) {
            returnElement = null;
        }
        LOGGER.info("getElementWithMatchingText completed looking for element \"" + elementBy +
                "\" with value \"" + value + "\"");
        return returnElement;
    }

    /**
     * Clicks an element via javascript executor
     *
     * @param element The element to click
     */
    public void jsClick(WebElement element) {
        LOGGER.info("jsClick started with element '" + element + "'");
        String mouseOverScript = "arguments[0].click();";
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript(mouseOverScript, element);
        } catch (Exception e) {
            Assert.fail("FAIL: Clicking an element with JS executor FAILED with error: " + e);
        }
        LOGGER.info("jsClick completed"); }

    /***
     * Scrolls an element into view (bottom of element will align with bottom of page) via javascript executor
     * @param element The element to scroll into view
     */
    public void jsScrollToElement(WebElement element) {
        LOGGER.info("jsScrollToElement started with element \"" + element + "\"");
        String mouseOverScript = "arguments[0].scrollIntoView(false);";
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript(mouseOverScript, element);
        } catch (Exception e) {
            LOGGER.info("jsScrollToElement encountered exception: " + e);
        }
        LOGGER.info("jsScrollToElement completed with element \"" + element + "\"");
    }

    /**
     * move to and click on a specified WebElement
     *
     * @param element The element to move to and click
     */
    public void moveToElementClick(WebElement element) {
        LOGGER.info("moveToElementClick started with element \"" + element + "\"");
        Actions actions = new Actions(driver);
        try {
            actions.moveToElement(element).click().build().perform();
        } catch (Exception e) {
            jsScrollToElement(element);
            actions.moveToElement(element).click().build().perform();
        }
        LOGGER.info("moveToElementClick completed with element \"" + element + "\"");
    }

    /**
     * Asserts text displayed contains text expected
     *
     * @param element The WebElement to check
     * @param text    Text to verify
     */
    public void verifyTextDisplayed(WebElement element, String text) {
        LOGGER.info("verifyTextDisplayed started for '" + text + "'");
        waitForElementVisible(element);
        String textVisible = element.getText();
        Assert.assertTrue("FAIL: Text expected is not displayed! '" + text + "' is not present in '" + textVisible + "'",
                textVisible.contains(text));
        LOGGER.info("verifyTextDisplayed completed  for '" + text + "'");
    }

    /**
     * Asserts text not displayed in given element
     *
     * @param element The WebElement to check
     * @param text    Text to verify
     */
    public void verifyTextNotDisplayed(WebElement element, String text) {
        LOGGER.info("verifyTextNotDisplayed started for '" + text + "'");
        waitForElementVisible(element);
        String textVisible = element.getText();
        Assert.assertFalse("FAIL: Text expected is displayed! '" + text + "' is present in '" + textVisible + "'",
                textVisible.contains(text));
        LOGGER.info("verifyTextNotDisplayed completed for '" + text + "'");
    }

    /**
     * Asserts text displayed contains text expected. The comparison is case-insensitive.
     *
     * @param element The WebElement to check
     * @param text    Text to verify
     */
    public void verifyTextDisplayedCaseInsensitive(WebElement element, String text) {
        LOGGER.info("verifyTextDisplayedCaseInsensitive started for '" + text + "'");
        waitForElementVisible(element);
        String textVisible = element.getText();
        Assert.assertTrue("FAIL: Text expected is not displayed! '" + text + "' is not present in '" + textVisible + "'",
                CommonUtils.containsIgnoreCase(textVisible, text));
        LOGGER.info("verifyTextDisplayedCaseInsensitive completed for '" + text + "'");
    }

    /**
     * Clicks an element with By selector and text
     *
     * @param by   By selector of element with text
     * @param text Text of element to click on
     */
    public void clickElementWithText(By by, String text) {
        clickElementWithText(by, text, true);
    }

    /**
     * Clicks an element with By selector and text
     *
     * @param by                  By selector of element with text
     * @param text                Text of element to click on
     * @param assertLinkIsPresent true or false whether to assert the link is present
     */
    public boolean clickElementWithText(By by, String text, boolean assertLinkIsPresent) {
        LOGGER.info("clickElementWithText started with text '" + text + "'");
        boolean found = false;
        try {
            if (Config.isMobile())
                waitForMilliseconds();

            setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
            List<WebElement> elements = driver.findElements(by);
            resetImplicitWaitToDefault();
            for (WebElement element : elements) {
                if (element.getText().toLowerCase().contains(text.toLowerCase())) {
                    found = true;
                    jsScrollToElementClick(element);
                    break;
                }
            }
        } catch (NoSuchElementException e) {
            Assert.fail("FAIL: Element \"" + by.toString() + "\" NOT found! (Full Stack Trace: " + e.toString() + ")");
            if (assertLinkIsPresent)
                Assert.fail("FAIL: Element \"" + by.toString() + "\" NOT found! (Full Stack Trace: " + e.toString() + ")");
        }

        Assert.assertTrue("FAIL: Element \"" + by.toString() + "\" with text \"" + text + "\" NOT found!", found);
        if (assertLinkIsPresent)
            Assert.assertTrue("FAIL: Element \"" + by.toString() + "\" with text \"" + text + "\" NOT found!", found);
        LOGGER.info("clickElementWithText completed with text '" + text + "'");
        return found;
    }

    /**
     * Clicks an element within an element using By selector and text
     *
     * @param baseElement   The element within which the By selectors are contained
     * @param by            By selector of element with text
     * @param text          Text of element to click on
     */
    public void clickElementWithText(WebElement baseElement, By by, String text) {
        LOGGER.info("clickElementWithText started for '" + text + "' within the '" + baseElement + "' element");
        boolean found = false;
        try {
            if (Config.isMobile())
                waitForMilliseconds();

            List<WebElement> elements = baseElement.findElements(by);
            for (WebElement element : elements) {
                if (element.getText().toLowerCase().contains(text.toLowerCase())) {
                    found = true;
                    webElementClick(element);
                    break;
                }
            }
        } catch (NoSuchElementException e) {
            Assert.fail("FAIL: Element \"" + by.toString() + "\" NOT found! (Full Stack Trace: " + e.toString() + ")");
        }

        if (!found)
            Assert.fail("FAIL: Element \"" + by.toString() + "\" with text \"" + text + "\" NOT found!");
        LOGGER.info("clickElementWithText completed for '" + text + "' within the '" + baseElement + "' element");
    }

    /**
     * Clicks a web element with text
     *
     * @param element    WebElement with text
     * @param text       Text of element to click on
     */
    public void clickElementWithText(WebElement element, String text) {
        getElementWithText(element, text, true).click();
    }

    /**
     * Clicks an element with text
     *
     * @param element    Web element with text
     * @param text       Text of element to click on
     * @param ignoreCase true/false whether to ignore case
     */
    public void clickElementWithText(WebElement element, String text, boolean ignoreCase) {
        LOGGER.info("clickElementWithText started with " + element + " element with text '" + text +
                "' and ignore case = " + ignoreCase);
        getElementWithText(element, text, ignoreCase).click();
        LOGGER.info("clickElementWithText completed with " + element + " element with text '" + text +
                "' and ignore case = " + ignoreCase);
    }

    /**
     * Finds an element with By selector and exact text (case sensitive) and clicks on it
     *
     * @param by   By selector of element with text
     * @param text Text of element to click on
     */
    public void clickElementWithExactText(By by, String text) {
        clickElementWithExactText(by, text, false);
    }

    /**
     * Finds an element with By selector and exact text and clicks on it
     *
     * @param by         By selector of element with text
     * @param text       Text of element to click on
     * @param ignoreCase 'true' or 'false' whether to ignore case
     */
    public void clickElementWithExactText(By by, String text, boolean ignoreCase) {
        LOGGER.info("clickElementWithExactText started");
        boolean found = false;
        try {
            if (Config.isMobile())
                waitForMilliseconds();

            List<WebElement> elements = driver.findElements(by);
            for (WebElement element : elements) {
                if (ignoreCase) {
                    found = element.getText().equalsIgnoreCase(text);
                } else {
                    found = element.getText().equals(text);
                }
                if (found) {
                    webElementClick(element);
                    break;
                }
            }
        } catch (NoSuchElementException e) {
            Assert.fail("FAIL: Element \"" + by.toString() + "\" NOT found! (Full Stack Trace: " + e.toString() + ")");
        }

        if (!found)
            Assert.fail("FAIL: Element \"" + by.toString() + "\" with exact text \"" + text + "\" NOT found!");
        LOGGER.info("clickElementWithExactText completed");
    }

    /**
     * Finds and clicks on a link using the text value
     *
     * @param linkText Text value of link to click
     */
    public void clickElementWithLinkText(String linkText) {
        clickElementWithLinkText(linkText, true);
    }

    /**
     * Finds and clicks on a link using the text value
     *
     * @param linkText              Text value of link to click
     * @param assertElementFound    true/false whether to assert that the element is found
     */
    public void clickElementWithLinkText(String linkText, boolean assertElementFound) {
        LOGGER.info("clickElementWithLinkText started with linkText \"" + linkText + "\"");
        WebElement linkElement = findElementWithLinkText(linkText);
        if (assertElementFound)
            Assert.assertNotNull("FAIL: Could not find link with text '" + linkText + "'", linkElement);
        jsScrollToElementClick(linkElement);
        LOGGER.info("clickElementWithLinkText completed with linkText \"" + linkText + "\"");
    }

    /**
     * Finds a link with specified text
     *
     * @param linkText Text value of link
     */
    public WebElement findElementWithLinkText(String linkText) {
        LOGGER.info("findElementWithLinkText started with linkText \"" + linkText + "\"");
        WebElement linkElement = null;
        setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
        try {
            linkElement = driver.findElement(By.linkText(linkText));
        } catch (NoSuchElementException nse){
            try {
                linkElement = driver.findElement(By.partialLinkText(linkText));
            } catch (NoSuchElementException nse2) {
                List<String> commonTags = new ArrayList<String>(Arrays.asList("a", "li", "h1", "div"));
                for (String tag : commonTags) {
                    try {
                        setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
                        linkElement = getElementWithText(By.tagName(tag), linkText);
                        if (linkElement != null) {
                            break;
                        }
                    } catch (Exception e) {
                        //continue
                    }
                }
            }
        }
        resetImplicitWaitToDefault();
        LOGGER.info("findElementWithLinkText completed with linkText \"" + linkText + "\"");
        return linkElement;
    }

    /**
     * Finds and clicks on a link using part of the text value
     *
     * @param linkText Partial text value of link to click
     */
    public void clickElementByPartialText(String linkText) {
        LOGGER.info("clickElementByPartialText started with linkText \"" + linkText + "\"");
        WebElement linkElement = driver.findElement(By.partialLinkText(linkText));
        waitForElementClickable(linkElement);
        linkElement.click();
        LOGGER.info("clickElementByPartialText started with linkText \"" + linkText + "\"");
    }

    /**
     * Scrolls an element into view before firing the click event. Useful when a modal or menu has options
     * listed outside current view such that they may be behind another element. This method will scroll them into
     * view and make a click collision between elements much less likely to occur.
     *
     * @param element The element to scroll into view on the page
     */
    public void jsScrollToElementClick(WebElement element) {
        jsScrollToElementClick(element, true);
    }

    /**
     * Scrolls an element into view before firing the click event.
     *
     * @param element The element to scroll into view on the page
     */
    public void jsScrollToElementClick(WebElement element, boolean waitForClickable) {
        LOGGER.info("jsScrollToElementClick started");
        setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        if (element == null) {
            return;
        }
        jsScrollToElement(element);
        if (waitForClickable) {
            waitForElementClickable(element);
        }
        webElementClick(element);
        resetImplicitWaitToDefault();
        LOGGER.info("jsScrollToElementClick completed");
    }


    /**
     * Finds an element via a By parameter, calculates height and clicks 10 pixels below on screen
     *
     * @param by By selector to find element
     */
    public void clickElementYCoordinateOffset(By by) {
        LOGGER.info("clickElementYCoordinateOffset started");
        WebElement element = driver.findElement(by);
        int y = element.getSize().getHeight();
        y = y - 10;
        Actions builder = new Actions(driver);
        builder.moveToElement(element, 10, y).click().build().perform();
        LOGGER.info("clickElementYCoordinateOffset completed");
    }

    /**
     * Returns the last substring of a WebElement's text with delimiter
     *
     * @param element   The WebElement's text to pull the string from
     * @param delimiter The character preceding the string to extract
     * @return String
     */
    public String getLastSubstring(WebElement element, String delimiter) {
        return element.getText().substring(element.getText().lastIndexOf(delimiter) + 1);
    }

    /**
     * Gets the URL passed in
     *
     * @param url The url to go to
     */
    public void getUrl(String url) {
        LOGGER.info("getUrl started with url: " + url);
        try {
            driver.get(url);
        } catch (TimeoutException e) {
            LOGGER.info("Page load timed out, attempting refresh...");
            driver.navigate().to(url);
            driver.navigate().refresh();
            waitForPageToLoad();
            driver.get(url);
        }

        if (Config.isDtc()) {
            LOGGER.info("Setting session ID...");
            this.scenarioData.setDtSessionId(driver);
            LOGGER.info("JSESSIONID:" + this.scenarioData.getDtSessionId());
        }
        LOGGER.info("getUrl completed with url: " + url);
    }

    /**
     * Checks if element (WebElement) is displayed
     *
     * @param element The element to check
     * @return boolean
     */
    public boolean isElementDisplayed(WebElement element) {
        return isElementDisplayed(element, null, Constants.FIVE);
    }

    /**
     * Checks if element (WebElement) is displayed
     *
     * @param element       The element to check
     * @param timeInSeconds The timeInSeconds to wait in checking for the element
     * @return boolean
     */
    public boolean isElementDisplayed(WebElement element, int timeInSeconds) {
        return isElementDisplayed(element, null, timeInSeconds);
    }

    /**
     * Checks if element (By Object) is displayed
     *
     * @param element The element to check
     * @return boolean
     */
    public boolean isElementDisplayed(By element) {
        return isElementDisplayed(null, element, Constants.FIVE);
    }

    /**
     * Checks if element (By Object) is displayed
     *
     * @param element       The element to check
     * @param timeInSeconds The timeInSeconds to wait in checking for the element
     * @return boolean
     */
    public boolean isElementDisplayed(By element, int timeInSeconds) {
        return isElementDisplayed(null, element, timeInSeconds);
    }

    /***
     * Checks if element (web or by) is displayed
     * @param webElement The web element to check
     * @param byElement The by element to check
     * @param timeInSeconds Time to wait for element to display
     * @return Boolean on whether element is displayed
     */
    public boolean isElementDisplayed(WebElement webElement, By byElement, int timeInSeconds) {
        LOGGER.info("isElementDisplayed started");
        if (webElement == null && byElement == null)
            return false;
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
        boolean returnVal = true;
        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        if (webElement != null) {
            try {
                wait.until(ExpectedConditions.visibilityOf(webElement));
                LOGGER.info("The WebElement: \"" + webElement + "\" was displayed");
            } catch (Exception e) {
                LOGGER.info("The WebElement: \"" + webElement + "\" was NOT displayed");
                returnVal = false;
            }
        } else {
            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(byElement));
                LOGGER.info("The By element locator: \"" + byElement + "\" was displayed");
            } catch (Exception e) {
                LOGGER.info("The By element locator: \"" + byElement + "\" was NOT displayed");
                returnVal = false;
            }
        }
        resetImplicitWaitToDefault();
        LOGGER.info("isElementDisplayed completed");
        return returnVal;
    }

    /**
     * Embeds a screenshot into a scenario
     *
     * @param scenario Scenario status - Passed or Failed
     * @throws IOException General exception caught to allow for graceful failure
     */
    public void embedScreenshot(Scenario scenario) throws IOException {
        LOGGER.info("embedScreenshot started with scenario '" + scenario + "' " + scenario.getStatus());
        byte[] screenshot;
        zoomInOrOutInWebPage(Config.getScreenshotZoom());
        try {
            if (Config.isIphone() || Config.isIpad()) {
                //TODO: AB 3/8/17 implement screenshots for iphone ipad, may need to convert
                //TODO (cont) File to Bytes to allow for embedding
                LOGGER.info("Screenshots not yet implemented for iPhone/iPad!");
                //WebDriver driver1 = new Augmenter().augment(driver);
                //File file  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
                //FileUtils.copyFile(file, new File(scenario.getName() + ".jpg"));
            } else {
                screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
                if (Config.getAddBottomScreenshot()) {
                    scrollToBottom();
                    screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                    scenario.embed(screenshot, "image/png");
                }
            }
        } catch (WebDriverException somePlatformsDontSupportScreenshots) {
            LOGGER.info(somePlatformsDontSupportScreenshots.getMessage());
        }
        LOGGER.info("embedScreenshot completed with scenario '" + scenario + "' " + scenario.getStatus());
    }

    /**
     * Returns element with constructed autoClass name
     *
     * @param linkName The element to check
     * @param type     Page location used to determine auto class name
     * @return webElement
     */
    public WebElement constructAutoTagClassName(String linkName, String type) {
        LOGGER.info("constructAutoTagClassName started");
        waitForPageToLoad();
        WebElement autoClassName = null;
        String constructedLink = cleanStringOfSpaces(linkName);

        try {
            if (type.equalsIgnoreCase(HEADER)) {
                autoClassName = driver.findElement(By.className(AUTO_HEADER + constructedLink));
            } else if (type.equalsIgnoreCase(FOOTER)) {
                autoClassName = driver.findElement(By.className(AUTO_FOOTER + constructedLink));
            } else if (type.equalsIgnoreCase(FITMENT)) {
                autoClassName = driver.findElement(By.className(AUTO_FITMENT + constructedLink));
            }
        } catch (Exception e) {
            Assert.fail("FAIL: Constructing auto tag class name for element \"" + linkName + "\" of type \"" + type
                    + "\" FAILED with error: " + e);
        }
        LOGGER.info("constructAutoTagClassName completed");
        return autoClassName;
    }

    /**
     * Takes a string and removes any blank spaces inside
     *
     * @param stringToClean Text to eliminate spaces from
     * @return String
     */
    public String cleanStringOfSpaces(String stringToClean) {
        LOGGER.info("cleanStringOfSpaces started with string \"" + stringToClean + "\"");
        stringToClean = stringToClean.replaceAll("[\\s]", "-");
        LOGGER.info("cleanStringOfSpaces completed; string is now \"" + stringToClean.toLowerCase() + "\"");
        return stringToClean.toLowerCase();
    }

    /**
     * Takes a WebElement to click, clear and send keys to after waiting for it to be interactable.
     *
     * @param element               WebElement to interact with
     * @param inputString           String of keys you want sent to the WebElement
     * @param clickElement          true/false whether to click element
     * @param waitForClickableTime  The amount of time to wait for an item to be clickable
     */
    public void clearInputAndSendKeys(WebElement element, String inputString, boolean clickElement,
                                      int waitForClickableTime) {
        LOGGER.info("clearInputAndSendKeys started");
        if (waitForClickableTime != Constants.ZERO)
            waitForElementClickable(element, waitForClickableTime);
        if (clickElement)
            element.click();
        if (Config.isIe())
            waitForMilliseconds();
        element.clear();
        element.sendKeys(inputString);
        LOGGER.info("clearInputAndSendKeys completed");
    }

    /**
     * Takes a WebElement to click, clear and send keys to after waiting for it to be interactable.
     *
     * @param element     WebElement to interact with.
     * @param inputString String of keys you want sent to the WebElement.
     */
    public void clearInputAndSendKeys(WebElement element, String inputString) {
        clearInputAndSendKeys(element, inputString, true, Constants.THIRTY);
    }

    /**
     * Takes a WebElement to click, clear and send keys to after waiting for it to be interactable.
     *
     * @param element     WebElement to interact with.
     * @param inputString String of keys you want sent to the WebElement.
     */
    public void clickInputAndSendKeys(WebElement element, String inputString) {
        LOGGER.info("clickInputAndSendKeys started");
        waitForElementClickable(element);
        element.click();
        element.sendKeys(inputString);
        LOGGER.info("clickInputAndSendKeys completed");
    }

    /**
     * Input text value in a specified field
     *
     * @param element               The field to enter text
     * @param text                  The text to enter
     * @param clickElement          true/false whether to click element
     * @param waitForClickableTime  The amount of time to wait for an item to be clickable
     */
    public void enterTextIntoField(WebElement element, String text, boolean clickElement, int waitForClickableTime) {
        LOGGER.info("enterTextIntoField started");
        waitForPageToLoad(Constants.FIVE_HUNDRED);
        clearInputAndSendKeys(element, text, clickElement, waitForClickableTime);
        LOGGER.info("enterTextIntoField completed");
    }

    /**
     * Input a text value in a specified field
     *
     * @param element   The field to enter text
     * @param text      The text to enter
     */
    public void enterTextIntoField(WebElement element, String text) {
        enterTextIntoField(element, text, true, Constants.THIRTY);
    }


    /**
     * Uses string type and link text to find element
     * Clicks on found element
     *
     * @param type     What section of the page the link belongs to (header, footer, fitment)
     * @param linkText Text value of link to click
     */
    public void clickElementByAutoClassName(String type, String linkText) {
        LOGGER.info("clickElementByAutoClassName started");

        //TODO: Better solution would be to refactor constructAutoTagClassName to return By then do
        //TODO (cont): waitForClassPresent(getByValue(byElement));
        waitForMilliseconds(Constants.TWO_THOUSAND);

        WebElement element = constructAutoTagClassName(linkText, type);

        //Handing intermittent 'cannot determine loading status' failures
        try {
            jsScrollToElement(element);
            waitForElementClickable(element);
            element.click();
        } catch (Exception e) {
            waitForElementClickable(element);
            element.click();
        }
        LOGGER.info("AutoClassName " + linkText + " Clicked");
        LOGGER.info("clickElementByAutoClassName completed");
    }

    /**
     * Checks if text is displayed
     *
     * @param text The string text to check
     * @return boolean
     */
    public boolean isTextPresentInPageSource(String text) {
        return driver.getPageSource().contains(text);
    }

    /**
     * Simulates clicking the 'back' browser navigation button
     */
    public void browserNavigateBackAction() {
        LOGGER.info("browserNavigateBackAction started");
        try {
            ((JavascriptExecutor) driver).executeScript("history.go(-1)");
        } catch (Exception e) {
            driver.navigate().back();
        }
        waitForPageToLoad();
        LOGGER.info("browserNavigateBackAction completed");
    }

    /**
     * Checks if element is found on page with provided attributes and text
     *
     * @param element   By selector of element
     * @param attribute Attribute of element
     * @param text      Text element should contain
     */
    public void assertElementAttributeString(By element, String attribute, String text) {
        LOGGER.info("assertElementAttributeString started");
        boolean StringFound = false;
        List<WebElement> objects = driver.findElements(element);
        int i = 0;
        for (WebElement object : objects) {
            if (object.getAttribute(attribute).contains(text)) {
                LOGGER.info("String '" + text + "' matched with rendered  ==>"
                        + object.getAttribute(attribute));
                StringFound = true;
                break;
            } else {
                i++;
            }
        }

        if (!StringFound) {
            Assert.fail("FAIL: String \"" + text + "\" NOT found!");
        }
        LOGGER.info("assertElementAttributeString completed");
    }

    /**
     * Checks if element on page matches with provided text for specified attribute
     *
     * @param element   WebElement with attribute to be found
     * @param attribute Attribute of element
     * @param text      Text element should contain
     */
    public void assertElementAttributeString(WebElement element, String attribute, String text) {
        LOGGER.info("assertElementAttributeString started");
        waitForElementVisible(element);
        String foundString = element.getAttribute(attribute);
        Assert.assertTrue("FAIL: expected string of: " + text + " for attribute: " + attribute +
                " did not match the found string of: " + foundString, foundString.toLowerCase().contains(text.toLowerCase()));
        LOGGER.info("assertElementAttributeString completed");
    }

    /**
     * Checks if element contains text string
     *
     * @param element By selector for element
     * @param text    Text element should contain
     * @return Boolean
     */
    public boolean checkIfElementContainsText(By element, String text) {
        LOGGER.info("checkIfElementContainsText started for By Element with text: '" + text + "'");
        boolean found = false;
        List<WebElement> webElements = driver.findElements(element);
        for (WebElement webElement : webElements) {
            if (webElement.getText().toLowerCase().contains(text.toLowerCase())) {
                LOGGER.info("Confirmed that the element contains '" + text + "'.");
                found = true;
                break;
            }
        }
        LOGGER.info("checkIfElementContainsText completed for By Element with text: '" + text + "'");
        return found;
    }

    /**
     * Checks if element contains text string
     *
     * @param element WebElement to check
     * @param text    Text element should contain
     * @return Boolean
     */
    public boolean checkIfElementContainsText(WebElement element, String text) {
        return checkIfElementContainsText(element, text, false);
    }

    /**
     * Checks if element contains text string
     *
     * @param element       WebElement to check
     * @param text          Text element should contain
     * @param ignoreCase    true/false whether to ignore case of the search string
     * @return Boolean
     */
    public boolean checkIfElementContainsText(WebElement element, String text, boolean ignoreCase) {
        LOGGER.info("checkIfElementContainsText started for Web Element with text: '" + text + "'");
        boolean found = false;
        if (ignoreCase)
            found = CommonUtils.containsIgnoreCase(element.getText(), text);
        else
            found = element.getText().contains(text);
        if (found)
            LOGGER.info("Confirmed that the element contains '" + text + "'.");
        LOGGER.info("checkIfElementContainsText completed for Web Element with text: '" + text + "'");
        return found;
    }

    /**
     * Returns By object
     *
     * @param byType  Type of By selector
     * @param element The element to get the By for
     * @return By
     */
    public By getBy(String byType, String element) {
        LOGGER.info("getBy started");
        By by = null;

        if (byType.equalsIgnoreCase(Constants.CLASS_NAME)) {
            by = By.className(element);
        } else if (byType.equalsIgnoreCase(Constants.CSS)) {
            by = By.cssSelector(element);
        } else if (byType.equalsIgnoreCase(Constants.ID)) {
            by = By.id(element);
        } else if (byType.equalsIgnoreCase(Constants.NAME)) {
            by = By.name(element);
        } else if (byType.equalsIgnoreCase(Constants.TAG_NAME)) {
            by = By.tagName(element);
        } else if (byType.equalsIgnoreCase(Constants.XPATH)) {
            by = By.xpath(element);
        } else if (byType.equalsIgnoreCase(Constants.LINK_TEXT)) {
            by = By.linkText(element);
        } else if (byType.equalsIgnoreCase(Constants.PARTIAL_LINK_TEXT)) {
            by = By.partialLinkText(element);
        }

        LOGGER.info("getBy completed");
        return by;
    }

    /**
     * Scrolls to bottom of page
     */
    public void scrollToBottom() {
        LOGGER.info("scrollToBottom started");
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        LOGGER.info("scrollToBottom completed");
    }

    /**
     * Selects visible text value from provided dropdown By element
     *
     * @param by         The dropdown element with values
     * @param optionText Values to select from dropdown (By) element
     */
    public void selectFromDropdownByVisibleText(By by, String optionText) {
        LOGGER.info("selectFromDropdownByVisibleText started");
        WebElement selectEle = driver.findElement(by);
        Select mySelect = new Select(selectEle);
        jsScrollToElement(selectEle);
        mySelect.selectByVisibleText(optionText);
        LOGGER.info("selectFromDropdownByVisibleText completed");
    }

    /**
     * Selects visible text value from provided dropdown element
     *
     * @param dropDownEle The dropdown element with values
     * @param optionText  Values to select from dropdown element
     */
    public void selectFromDropdownByVisibleText(WebElement dropDownEle, String optionText) {
        LOGGER.info("selectFromDropdownByVisibleText started");
        Select mySelect = new Select(dropDownEle);
        jsScrollToElement(dropDownEle);
        mySelect.selectByVisibleText(optionText);
        LOGGER.info("selectFromDropdownByVisibleText completed");
    }

    /***
     * Double-clicks the By element control
     * @param byElement control to receive the double-click
     */
    public void doubleClickControl(By byElement) {
        doubleClickControl(driver.findElement(byElement));
    }

    /***
     * Double-clicks either a non-null By element or else a WebElement control
     * @param webElement null OR control to receive the double-click
     */
    public void doubleClickControl(WebElement webElement) {
        LOGGER.info("doubleClickControl started");
        Actions action = new Actions(driver);
        action.doubleClick(webElement).build().perform();
        LOGGER.info("doubleClickControl completed");
    }

    /**
     * Double Click Method Without Scrolling to the element.
     *
     * @param element - Web Element
     */
    public void doubleClick(WebElement element) {
        LOGGER.info("doubleClick started");
        Actions action = new Actions(driver);
        action.doubleClick(element).build().perform();
        LOGGER.info("doubleClick completed");
    }

    /***
     * Resets the implicit wait to the default value of the framework i.e. 30 seconds
     */
    public void resetImplicitWaitToDefault() {
        driver.manage().timeouts().implicitlyWait(Constants.THIRTY, TimeUnit.SECONDS);
    }

    /***
     * Set the implicit wait
     *
     * @param time : Specify time in (Integer) number how long driver has to wait based on unit parameter
     * @param unit : unit can be Milliseconds, Microseconds, seconds or can be anything.
     */
    public void setImplicitWait(int time, TimeUnit unit) {
        driver.manage().timeouts().implicitlyWait(time, unit);
    }

    /**
     * Returns the displayed element when there are multiple with the same by value
     *
     * @param parentElement The parent element to search within
     * @param by            The by to create a list of elements with
     * @param timeInSeconds The amount of timeInSeconds to wait for trying to get the element
     * @return WebElement  The WebElement being displayed
     */
    public WebElement getDisplayedElement(WebElement parentElement, By by, int timeInSeconds) {
        LOGGER.info("getDisplayedElement started for By element with specified parent element");
        setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> webElements = parentElement.findElements(by);
        resetImplicitWaitToDefault();
        WebElement returnElement = null;
        if (webElements.size() > 0) {
            returnElement = getDisplayedElement(webElements, timeInSeconds);
        }
        LOGGER.info("getDisplayedElement completed for By element with specified parent element");
        return returnElement;
    }

    /**
     * Returns the displayed element when there are multiple with the same by value
     *
     * @param by The by to create a list of elements with
     * @return WebElement  The WebElement being displayed
     */
    public WebElement getDisplayedElement(By by) {
        return getDisplayedElement(by, Constants.FIVE);
    }

    /**
     * Returns the displayed element when there are multiple with the same by value
     *
     * @param by            The by to create a list of elements with
     * @param timeInSeconds The amount of timeInSeconds to wait for trying to get the element
     * @return WebElement  The WebElement being displayed
     */
    public WebElement getDisplayedElement(By by, int timeInSeconds) {
        LOGGER.info("getDisplayedElement started for By element");
        setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
        List<WebElement> webElements = driver.findElements(by);
        resetImplicitWaitToDefault();
        WebElement returnElement = null;
        if (webElements.size() > 0) {
            returnElement = getDisplayedElement(webElements, timeInSeconds);
        }
        LOGGER.info("getDisplayedElement completed for By element");
        return returnElement;
    }

    /**
     * Returns the displayed element when there are multiple with the same by value
     *
     * @param webElements The list of WebElements to check
     * @return WebElement  The WebElement being displayed
     */
    public WebElement getDisplayedElement(List<WebElement> webElements) {
        return getDisplayedElement(webElements, Constants.FIVE);
    }

    /**
     * Returns the displayed element when there are multiple with the same by value
     *
     * @param webElements   The list of WebElements to check
     * @param timeInSeconds The amount of timeInSeconds to wait for trying to get the element
     * @return WebElement  The WebElement being displayed
     */
    public WebElement getDisplayedElement(List<WebElement> webElements, int timeInSeconds) {
        LOGGER.info("getDisplayedElement started with a wait time of '" + timeInSeconds + "' seconds");
        for (WebElement webElement : webElements) {
            if (isElementDisplayed(webElement, timeInSeconds)) {
                LOGGER.info("getDisplayedElement completed with a displayed element");
                return webElement;
            }
        }
        LOGGER.info("getDisplayedElement completed but no displayed element found! Returning null");
        return null;
    }

    /**
     * Waits for a specific class to be present on the page, using pollUntil
     *
     * @param className The name of the class to poll for
     * @param time      The time to wait
     */
    public void waitForClassPresent(String className, int time) {
        String script = "return document.getElementsByClassName('" + className + "')[0] != null;";
        if (pollUntil(script, time)) {
            LOGGER.info("className " + className + " found.");
        } else {
            Assert.fail("FAIL: className " + className + " NOT found.");
        }
    }

    /**
     * Pulls the text value from a By element
     *
     * @param by The by element to pull the value from
     * @return String  The extracted value
     */
    public String getByValue(By by) {
        String byString = by.toString();
        return byString.substring(byString.lastIndexOf(": ") + 2);
    }

    /**
     * Waits for page to load within default timeout
     */
    public void waitForPageToLoad() {
        waitForPageToLoad(Constants.TWO_THOUSAND);
    }

    /**
     * Initially wait for specified number of milliseconds. Then run javascript to poll for ready state until
     * page is loaded.
     *
     * @param milliseconds Number of milliseconds to wait before beginning poll for wait state.
     */
    public void waitForPageToLoad(int milliseconds) {
        LOGGER.info("waitForPageToLoad started with preceding wait time of " + milliseconds + " milliseconds");
        waitForMilliseconds(milliseconds);
        pollUntil("return document.readyState.indexOf('complete')>-1;", Constants.THIRTY);
        LOGGER.info("waitForPageToLoad completed with preceding wait time of " + milliseconds + " milliseconds");
    }

    /**
     * Checks if an attribute is present in an element
     *
     * @param element   The by element to check
     * @param attribute The attribute to look for
     * @return boolean     Whether the attribute was present or not
     */
    public boolean isAttributePresent(WebElement element, String attribute) {
        LOGGER.info("isAttributePresent started");
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
        Boolean result = false;
        try {
            String value = element.getAttribute(attribute);
            if (value != null) {
                resetImplicitWaitToDefault();
                result = true;
            }
        } catch (Exception e) {
            LOGGER.info("isAttributePresent threw error:" + e);
        }
        LOGGER.info("isAttributePresent started");
        resetImplicitWaitToDefault();
        return result;
    }

    /**
     * Gets the number of WebElements of the type specified
     *
     * @param by The By to create a list of WebElements from
     * @return int The number of WebElements counted
     */
    public int getElementCount(By by) {
        waitForElementVisible(by);
        List<WebElement> webElements = driver.findElements(by);
        return webElements.size();
    }

    /**
     * Switches the iFrame selection context with passed in WebElement for the Frame
     *
     * @param frame WebElement of iFrame to switch to
     */
    public void switchFrameContext(WebElement frame) {
        LOGGER.info("switchFrameContext started");
        driver.switchTo().frame(frame);
        LOGGER.info("switchFrameContext completed");
    }

    /**
     * Switches the iFrame selection context with passed in Integer of frame in DOM
     *
     * @param frame integer value of frame in DOM
     */
    public void switchFrameContext(int frame) {
        LOGGER.info("switchFrameContext started");
        driver.switchTo().frame(frame);
        LOGGER.info("switchFrameContext completed");
    }

    /**
     * Switches the Iframe Selection context with Passed in String Value matched to default, parent or other sting value
     *
     * @param context String value matched to context selection
     */
    public void switchFrameContext(String context) {
        LOGGER.info("switchFrameContext started");
        if (context.equalsIgnoreCase(Constants.DEFAULT_CONTENT)) {
            driver.switchTo().defaultContent();
        } else if (context.equalsIgnoreCase(Constants.PARENT)) {
            driver.switchTo().parentFrame();
        } else {
            driver.switchTo().frame(context);
        }
        LOGGER.info("switchFrameContext Completed");
    }

    /**
     * Performs a sendKeys action for passed in Keys
     *
     * @param key Keys datatype of key to be sent.
     */
    public void performKeyAction(Keys key) {
        LOGGER.info("performKeyAction started with Key " + key);
        Actions action = new Actions(driver);
        action.sendKeys(key).perform();
        LOGGER.info("performKeyAction completed with Key " + key);
    }

    /**
     * Moves the mouse to hover over an element
     *
     * @param by By element that you want to be hovered over
     */
    public void mouseHoverOverElement(By by) {
        LOGGER.info("mouseHoverOverElement started");
        Actions builder = new Actions(driver);
        WebElement hoverElement = driver.findElement(by);
        builder.moveToElement(hoverElement).build().perform();
        LOGGER.info("mouseHoverOverElement completed");
    }

    /**
     * Returns the parent of the child element
     *
     * @param childElement The child element
     * @return WebElement Parent Element of childElement
     */
    public WebElement getParentElement(WebElement childElement) {
        return childElement.findElement(By.xpath("./.."));
    }

    /**
     * Returns the nth parent of the child element
     *
     * @param childElement The child element
     * @param parentNode parent node type(e.g div etc)
     * @param number nth parent number
     * @return WebElement Parent Element of childElement
     */
    public WebElement getNthParentElement(WebElement childElement, String parentNode, String number) {
        LOGGER.info("getNthParentElement started for " + childElement + " to get " + number
                + " number parent of type " + parentNode);
        return childElement.findElement(By.xpath("./ancestor::" + parentNode + "[" + number + "]"));
    }

    /**
     * Returns a list of displayed elements when there are multiple with the same by value
     *
     * @param elementBy   The By element to check
     * @param elementList The list of WebElements to check
     * @param time        The amount of time to wait for trying to get the element
     * @return List of WebElements that have been verified as being displayed
     */
    public List<WebElement> getDisplayedElementsList(By elementBy, List<WebElement> elementList, int time) {
        LOGGER.info("getDisplayedElementsList started");
        List<WebElement> duplicateElementsList;
        List<WebElement> displayedElementsList = new ArrayList<>();

        if (elementBy == null) {
            duplicateElementsList = elementList;
        } else {
            duplicateElementsList = driver.findElements(elementBy);
        }

        for (WebElement webElement : duplicateElementsList) {
            if (isElementDisplayed(webElement, time)) {
                displayedElementsList.add(webElement);
            }
        }
        LOGGER.info("getDisplayedElementsList completed");
        return displayedElementsList;
    }

    /**
     * Returns a list of displayed elements when there are multiple with the same by value. Overload for a WebElement
     * list with a default wait of 2 seconds
     *
     * @param elementList The list of WebElements to check
     * @return List of WebElements that have been verified as being displayed
     */
    public List<WebElement> getDisplayedElementsList(List<WebElement> elementList) {
        return getDisplayedElementsList(null, elementList, Constants.TWO);
    }

    /**
     * Returns a list of displayed elements when there are multiple with the same by value. Overload for a By object
     * with a default wait of 2 seconds
     *
     * @param elementBy The By element to check
     * @return List of WebElements that have been verified as being displayed
     */
    public List<WebElement> getDisplayedElementsList(By elementBy) {
        return getDisplayedElementsList(elementBy, null, Constants.TWO);
    }

    /**
     * Returns the WebElement where the class contains specified text. In the event of multiple elements, method returns
     * the first element found. Should only be used in situations where the class is semi-unique but has dynamic
     * portions e.g. "fitment-options__option-list___1Q3YE" so the text passed to this method would be
     * "fitment-options__option-list"
     *
     * @param text Text that should be contained in the class attribute of an element
     * @return WebElement with class attribute containing specified text (if any are found) ELSE null
     */
    public WebElement getElementWithClassContainingText(String text) {
        LOGGER.info("getElementWithClassContainingText started with text: '" + text + "'");
        List<WebElement> webElements = getElementsWithClassContainingText(text);

        if (webElements.size() > Constants.ZERO) {
            LOGGER.info("getElementWithClassContainingText completed with text: '" + text + "'");
            return webElements.get(Constants.ZERO);
        }

        LOGGER.info("Could NOT find element with class attribute containing text: '" + text + "'");
        return null;
    }

    /**
     * Returns a list of WebElements that have a class attribute containing the specified text
     *
     * @param text Text that should be contained in the class attribute of the desired elements
     * @return List WebElements with class attributes containing the specified text
     */
    public List<WebElement> getElementsWithClassContainingText(String text) {
        LOGGER.info("getElementsWithClassContainingText started with text '" + text + "'");
        driver.manage().timeouts().implicitlyWait(Constants.ONE, TimeUnit.SECONDS);
        List<WebElement> elementList;
        elementList = driver.findElements(By.xpath(String.format(xpathClassLocator, text)));
        resetImplicitWaitToDefault();
        LOGGER.info("getElementsWithClassContainingText completed with text '" + text + "'");
        return elementList;
    }

    /**
     * Returns the displayed element where the @class attribute contains the specified text (xpath based)
     *
     * @param classText Text that should be contained within the class of the displayed element
     * @return Displayed element with class containing the specified text
     */
    public WebElement getDisplayedElementWithClassContainingText(String classText) {
        LOGGER.info("getDisplayedElementWithClassContainingText started with '" + classText + "' @class text");
        WebElement displayedElement = getDisplayedElement(getElementsWithClassContainingText(classText));
        Assert.assertNotNull("FAIL: Could NOT find the element with @class containing '" + classText + "' text!",
                displayedElement);
        LOGGER.info("getDisplayedElementWithClassContainingText completed with '" + classText + "' @class text");
        return displayedElement;
    }

    /**
     * Wait one second
     */
    public void waitOneSecond() {
        LOGGER.info("waitOneSecond started");
        waitForMilliseconds();
        LOGGER.info("waitOneSecond completed");
    }

    /**
     * Wait specified number of seconds
     *
     * @param seconds - Number of seconds
     */
    public void waitSeconds(int seconds) {
        LOGGER.info("waitSeconds started waiting " + seconds + " second(s)");
        waitForMilliseconds(seconds * Constants.ONE_THOUSAND);
        LOGGER.info("waitSeconds completed waiting " + seconds + " second(s)");
    }

    /**
     * This method will send keys into fields when the regular selenium method of send keys does not work.
     *
     * @param element pass the WebElement
     * @param input pass the input String which has to be entered
     */
    public void performSendKeysWithActions(WebElement element, String input) {
        LOGGER.info("performSendKeysWithActions started");
        waitForElementClickable(element);
        Actions actions = new Actions(driver);
        actions.sendKeys(element,input).build().perform();
        LOGGER.info("performSendKeysWithActions completed");
    }

    /**
     * This method will return the today's date in the format YYYY/MM/DD
     */
    public String getTodayDate(String format) {
        LOGGER.info("getTodayDate started");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        LocalDate localDate = LocalDate.now();
        String todayDate = dateTimeFormatter.format(localDate);
        LOGGER.info("getTodayDate completed");
        return todayDate;
    }

    /**
     * This method will return the future date in the format YYYY/MM/DD
     *
     * @param format - send the format desired. ex: YYYY/MM/DD or MM/DD/YYYY
     * @param plusMonths - include number of months needed to be added
     * @return returns a String of future date in desired format.
     */
    public String getFutureDate(String format, int plusMonths) {
        LOGGER.info("getFutureDate started");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        LocalDate localDate = LocalDate.now().plusMonths(plusMonths);
        String futureDate = dateTimeFormatter.format(localDate);
        LOGGER.info("getFutureDate completed");
        return futureDate;
    }

    /**
     * Waits specified time in seconds for the element to be visible.
     *
     * @param locator       The locator for the element to wait for
     * @param timeInSeconds The amount of time to wait
     */
    public void waitForElementVisibleByLocator(By locator, int timeInSeconds) {
        LOGGER.info("waitForElementVisibleByLocator started looking for element.");
        driver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            Assert.fail("FAIL: Element \"" + locator + "\" NOT found in \"" + timeInSeconds
                    + "\" seconds! " + "(Full Stack Trace: " + e.toString() + ")");
        }
        resetImplicitWaitToDefault();
        LOGGER.info("waitForElementVisibleByLocator completed and found element \"" + locator + "\"");
    }

    /**
     * Waits specified time in seconds for the element to be invisible.
     *
     * @param element       The WebElement for the element to wait for
     * @param timeInSeconds The amount of time to wait
     */
    public void waitForElementToBeInvisible(WebElement element, int timeInSeconds) {
        LOGGER.info("waitForElementToBeInvisible started ");
        driver.manage().timeouts().implicitlyWait(Constants.THREE, TimeUnit.SECONDS);
        Wait wait = new FluentWait(driver)
                .withTimeout(timeInSeconds, TimeUnit.SECONDS)
                .pollingEvery(Constants.ONE, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
        if(isElementDisplayed(element)){
            wait.until(ExpectedConditions.invisibilityOf(element));
        }
        resetImplicitWaitToDefault();
        LOGGER.info("waitForElementToBeInvisible completed");
    }

    /**
     * Take a screenshot and save it to a specified folder
     *
     * @param parentFolderPath - file path
     * @param childFolderName - folder name
     * @param imageName - image name
     * @param extension - the extension for the image file
     *
     * @throws Exception
     */
    public void takeScreenshotAndSaveToALocation(String parentFolderPath, String childFolderName, String imageName, String extension) throws Exception {
        LOGGER.info("takeScreenshotAndSaveToALocation started");
        try {
            TakesScreenshot screenshot = ((TakesScreenshot) driver);
            File staticScreenshotFile = screenshot.getScreenshotAs(OutputType.FILE);
            Date dateAndTime = new Date();
            String timeStamp = dateAndTime.toString().replace(":", "");
            String imageNameWithTimeStamp = "//" + imageName + " " + timeStamp;
            FileUtils.copyFile(staticScreenshotFile, new File(parentFolderPath + childFolderName + imageNameWithTimeStamp + extension));
        } catch (Exception e) {

        }
        LOGGER.info("takeScreenshotAndSaveToALocation completed");
    }

    /**
     * Get the value from a cell in an excel sheet
     *
     * @param filepath   - full file path
     * @param sheetname  - the name of the excel sheet
     * @param rowNumber  - row number
     * @param cellNumber - cell number
     * @return String representing the cell value
     * @throws IOException
     */
    public String readFromExcelAndReturnSingleValue(String filepath, String sheetname, int rowNumber, int cellNumber) throws IOException {
        LOGGER.info("readFromExcelAndReturnSingleValue started");
        File file = new File(filepath);
        FileInputStream inputStream = new FileInputStream(file);

        Sheet sheet;
        if (filepath.toLowerCase().contains(Constants.XLSX.toLowerCase())) {
            XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xssfsheet = xssfworkbook.getSheet(sheetname);
            sheet = xssfsheet;
        } else {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(inputStream);
            HSSFSheet hssfsheet = hssfworkbook.getSheet(sheetname);
            sheet = hssfsheet;
        }

        Row row = sheet.getRow(rowNumber - 1);
        try {
            Double cellValue = row.getCell(cellNumber - 1).getNumericCellValue();
            String output = String.valueOf(cellValue);
            return output.substring(0, output.length() - 2);
        } catch (IllegalStateException ise) {
            String cellValue = row.getCell(cellNumber - 1).getStringCellValue();
            return cellValue;
        }
    }

    /**
     * This method will write value of String Data type into desired row(1 to n) and column (1 to n i.e, A to ABCD..)
     * <p>
     * This method will write into .xls and .xlsx type of excel sheet
     *
     * @param filepath   - full file path
     * @param sheetName  - the name of the excel sheet
     * @param inputValue - Value to be inserted
     * @param rowNumber  - row number
     * @param cellNumber - cell number
     * @return String representing the cell value
     * @throws IOException
     */
    public void writeValuesIntoExcel(String filepath, String sheetName, String inputValue, int rowNumber, int cellNumber) throws IOException {
        LOGGER.info("writeValuesIntoExcel started with file path: " + filepath + ", " + sheetName + ", inputValue: "
                + inputValue + ", rowNumber: " + rowNumber + ", cellNumber: " + cellNumber);
        if (sheetName.length() > 30) {
            Assert.fail("Please limit the sheet name to 30 Chars");
        }
        File file = new File(filepath);
        FileInputStream inputStream = new FileInputStream(file);
        Sheet sheet;
        Workbook workbook;
        if (filepath.toLowerCase().contains(Constants.XLSX.toLowerCase())) {
            XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xssfsheet = xssfworkbook.getSheet(sheetName);
            workbook = xssfworkbook;
            sheet = xssfsheet;
        } else {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(inputStream);
            HSSFSheet hssfsheet = hssfworkbook.getSheet(sheetName);
            workbook = hssfworkbook;
            sheet = hssfsheet;
        }
        if (sheet == null) {
            sheet = workbook.createSheet(sheetName);
        }
        Row orderRow = sheet.getRow(rowNumber - 1);
        if (orderRow == null) {
            orderRow = sheet.createRow(rowNumber - 1);
        }
        Cell cell = orderRow.getCell(cellNumber - 1);
        if (cell != null) {
            cell.setCellValue(inputValue);
        } else {
            cell = orderRow.createCell(cellNumber - 1);
            cell.setCellValue(inputValue);
        }
        FileOutputStream outputStream = new FileOutputStream(file);
        workbook.write(outputStream);
        outputStream.close();
        LOGGER.info("writeValuesIntoExcel completed with file path:" + filepath + ", " + sheetName + ", inputValue: "
                + inputValue + ", rowNumber: " + rowNumber + ", cellNumber: " + cellNumber);
    }

    /**
     * get future's date
     *
     * @param days - to be added to todays date to get the future date
     * @return - future's date
     */
    public String getFutureDate(int days) {
        LOGGER.info("getFutureDate started");
        LocalDate today = LocalDate.now();
        DateTimeFormatter mdyFormat = DateTimeFormatter.ofPattern("MM/dd/uuuu");
        LocalDate futureDate = today.plusDays(days);
        String strFuture = mdyFormat.format(futureDate);
        LOGGER.info("getFutureDate completed" + strFuture);
        return strFuture;
    }

    /**
     * This method will Zoom in and Zoom out based on the percentage being passed to this method.
     *
     * @param percentage - Percentage of Zoom.
     */
    public void zoomInOrOutInWebPage(float percentage) {
        LOGGER.info("zoomInOrOutInWebPage started");
        float convertedPercentage = percentage / 100;
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("document.body.style.zoom = '" + convertedPercentage + "'");
        LOGGER.info("zoomInOrOutInWebPage completed");
    }

    /**
     * Clicks element using actions
     *
     * @param element - element to click
     */
    public void clickWithActions(WebElement element) {
        LOGGER.info("clickWithActions started");
        waitForElementClickable(element);
        Actions actions = new Actions(driver);
        actions.click(element).build().perform();
        LOGGER.info("clickWithActions completed");
    }

    /**
     * Determines whether an element exists without throwing an error
     *
     * @param baseElement   - The base element within which to check whether the element exists
     * @param elementBy     - The By element being evaluated
     * @return              - true/false whether the element exists
     */
    public boolean elementExists(WebElement baseElement, By elementBy) {
        LOGGER.info("elementExists started");
        boolean elementExists = true;
        setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        try {
            baseElement.findElement(elementBy);
        } catch (Exception e) {
            elementExists = false;
        }
        resetImplicitWaitToDefault();
        LOGGER.info("elementExists completed");
        return elementExists;
    }

    /**
     * Determines whether an element exists without throwing an error
     *
     * @param elementBy     - The By element being evaluated
     * @return              - true/false whether the element exists
     */
    public boolean elementExists(By elementBy) {
        LOGGER.info("elementExists started");
        boolean elementExists = true;
        setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        try {
            webDriver.findElement(elementBy);
        } catch (Exception e) {
            elementExists = false;
        }
        resetImplicitWaitToDefault();
        LOGGER.info("elementExists completed");
        return elementExists;
    }

    /**
     * Get the value from a cell in an excel sheet
     *
     * @param filepath   - full file path
     * @param sheetname  - the name of the excel sheet
     * @param rowNumber  - row number
     * @param columnName - Name of the column
     * @return String representing the cell value
     * @throws IOException
     */
    public String readFromExcelAndReturnSingleValue(String filepath, String sheetname, int rowNumber, String columnName) throws IOException {
        LOGGER.info("readFromExcelAndReturnSingleValue started");
        File file = new File(filepath);
        FileInputStream inputStream = new FileInputStream(file);

        Sheet sheet;
        int cellNumber = 0;
        if (filepath.toLowerCase().contains(Constants.XLSX.toLowerCase())) {
            XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xssfsheet = xssfworkbook.getSheet(sheetname);
            sheet = xssfsheet;
        } else {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(inputStream);
            HSSFSheet hssfsheet = hssfworkbook.getSheet(sheetname);
            sheet = hssfsheet;
        }
        Row firstRow = sheet.getRow(0);
        for (int i = 0; i <= firstRow.getLastCellNum(); i++) {
            String columnOutput = firstRow.getCell(i).getStringCellValue();
            if (columnName.equalsIgnoreCase(columnOutput)) {
                cellNumber = i;
                break;
            }
        }
        Row row = sheet.getRow(rowNumber - 1);
        try {
            Double cellValue = row.getCell(cellNumber).getNumericCellValue();
            String output = String.valueOf(cellValue);
            return output.substring(0, output.length() - 2);
        } catch (IllegalStateException ise) {
            String cellValue = row.getCell(cellNumber).getStringCellValue();
            return cellValue;
        }
    }

    /**
     * Try to click an element and if it fails, use jsClick
     *
     * @param webElement The WebElement to click
     */
    public void webElementClick(WebElement webElement) {
        LOGGER.info("webElementClick started");
        setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        if (webElement == null) {
            return;
        }
        try {
            webElement.click();
        } catch (Exception e) {
            jsClick(webElement);
        }
        resetImplicitWaitToDefault();
        LOGGER.info("webElementClick completed");
    }

    /**
     * Verifies if the list of string contains duplicates
     *
     * @param stringList list of string
     * @return boolean Duplicates found, True or False
     */
    public boolean arrayListHasDuplicates(List<String> stringList) {
        LOGGER.info("arrayListHasDuplicates started");
        boolean returnValue = true;
        List<String> duplicateList = getArrayListDuplicates(stringList);
        if (duplicateList.isEmpty()) {
            returnValue = false;
        }
        LOGGER.info("arrayListHasDuplicates started");
        return returnValue;
    }

    /**
     * Returns a list of duplicate values from a String list
     *
     * @param stringList list of string
     * @return String list of duplicate values
     */
    public List<String> getArrayListDuplicates(List<String> stringList) {
        LOGGER.info("getArrayListDuplicates started");
        Set<String> listOfString = new HashSet<>();
        List<String> returnList = new ArrayList<>();
        boolean duplicateFound = false;
        for (String value : stringList) {
            if (!listOfString.add(value)) {
                returnList.add(value);
                LOGGER.info("Duplicate found for string with text: " + value);
                break;
            }
        }
        LOGGER.info("getArrayListDuplicates completed");
        return returnList;
    }
}
