package crossapplication.steps;

import common.Config;
import common.Constants;
import crossapplication.data.CrossApplicationData;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fleet.pages.Fleet;
import sap.pages.CommonActions;
import sap.pages.PosDm;
import utilities.Driver;

public class CrossApplicationSteps {
    private Driver driver;
    private Scenario scenario;
    private CommonActions commonActions;
    private CrossApplicationData crossApplicationData;
    private PosDm posDm;

    public CrossApplicationSteps(Driver driver) {
        this.driver = driver;
        commonActions = new CommonActions(driver);
        crossApplicationData = new CrossApplicationData();
        posDm = new PosDm(driver);
    }

    private static String baseUrl;

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @And("^I set baseUrl to \"(hybris|Web Method|SAP|NWBC|SAP POSDM|SAP POSDT|SAP ECC|SAP web content|Backoffice|Treadwell Application Status|Fleet|AutoIntegrate|MicroStrategy)\"$")
    public void i_set_base_url(String product) throws Throwable {
        if (product.equalsIgnoreCase(Constants.HYBRIS)) {
            baseUrl = Config.getHybrisUrl();
        } else if (product.equalsIgnoreCase(Constants.WEB_METHOD)) {
            baseUrl = Config.getWMUrl();
        } else if (product.equalsIgnoreCase(Constants.PATCH_TESTING)) {
            baseUrl = Config.getPATCHUrl();
        } else if (product.equalsIgnoreCase(Constants.NWBC)) {
            baseUrl = Config.getNWBCUrl();
        } else if (product.equalsIgnoreCase(Constants.SAP_POSDM)) {
            baseUrl = Config.getPOSDMUrl();
        } else if (product.equalsIgnoreCase(Constants.SAP_POSDT)) {
            baseUrl = Config.getPOSDTUrl();
        } else if (product.equalsIgnoreCase(Constants.SAP_ECC)) {
            baseUrl = Config.getSapEccUrl();
        } else if (product.equalsIgnoreCase(Constants.SAP_WEB_CONTENT)) {
            baseUrl = Config.getSapWebContentUrl();
        } else if (product.equalsIgnoreCase(Constants.BACKOFFICE)) {
            baseUrl = Config.getBackofficeUrl();
        } else if (product.equalsIgnoreCase(Constants.TREADWELL_APPLICATION_STATUS)) {
            baseUrl = Config.getTreadwellApplicationStatus();
        } else if (product.equalsIgnoreCase(Constants.FLEET)) {
            baseUrl = Fleet.getFleetUrl();
        } else if (product.equalsIgnoreCase(Constants.AUTOINTEGRATE)) {
            baseUrl = Config.getAutoIntegrateUrl();
        } else if (product.equalsIgnoreCase(Constants.MICROSTRATEGY)) {
            baseUrl = Config.getMicroStrategyUrl();
        } else {
            Config.setBaseUrl(baseUrl);
        }
    }

    @And("^I set Module baseUrl to \"([^\"]*)\" in \"([^\"]*)\" Environment$")
    public void i_set_base_url_to(String product, String environment) throws Throwable {
        if (product.equalsIgnoreCase(Constants.SAP_POSDM)) {
            baseUrl = Config.getPOSDMUrl(environment);
        } else if (product.equalsIgnoreCase(Constants.SAP_POSDT)) {
            baseUrl = Config.getPOSDTUrl(environment);
        } else if (product.equalsIgnoreCase(Constants.SAP_ECC)) {
            baseUrl = Config.getSapEccUrl(environment);
        } else {
            Config.setBaseUrl(baseUrl);
        }
    }

    @And("^I navigate to the stored Base URL$")
    public void i_navigate_to_stored_based_url() throws Throwable {
        driver.getUrl(baseUrl);
    }

    @When("^I wait for \"([^\"]*)\" seconds$")
    public void i_wait_for_milliseconds(int waitTime) {
        driver.waitSeconds(waitTime);
    }

    @Then("^I switch back to the main window$")
    public void i_switch_back_to_the_main_window() throws Throwable {
        driver.switchFrameContext(Constants.DEFAULT_CONTENT);
    }

    @When("^I switch to the iFrame at Index \"([0-9]*)\"$")
    public void i_switch_to_iframe_at_index(int index) throws Throwable {
        driver.switchFrameContext(index);
    }

    @When("^I switch to the first iFrame$")
    public void i_switch_to_the_first_iFrame() throws Throwable {
        driver.switchFrameContext(0);
    }

    @When("^I switch to the parent iFrame$")
    public void i_switch_to_the_parent_iframe() throws Throwable {
        driver.switchFrameContext(Constants.PARENT);
    }

    @When("^I take Screenshot and save it to \"([^\"]*)\" folder with a file name of \"([^\"]*)\"$")
    public void i_take_screenshot_and_save_it_to_folder_with_a_file_name(String childFolderName, String imageName) throws Throwable {
        crossApplicationData.deleteSpecificFileWhichContainsName(commonActions.returnDataFromEnvironmentVariables(childFolderName), imageName);
        driver.takeScreenshotAndSaveToALocation(Constants.PARENT_FOLDER_NAME_SCREENSHOT, commonActions.returnDataFromEnvironmentVariables(childFolderName), imageName, ".png");
    }

    @When("^I take Screenshot and save it to \"([^\"]*)\" folder with a file name of \"([^\"]*)\" extracted from examples$")
    public void i_take_screenshot_and_save_it_to_folder_with_a_file_name_from_examples(String childFolderName, String imageName) throws Throwable {
        if (childFolderName.toLowerCase().contains(Constants.GEO)) {
            childFolderName = Config.getStateCode().toLowerCase();
        }
        crossApplicationData.deleteSpecificFileWhichContainsName(childFolderName, imageName);
        driver.takeScreenshotAndSaveToALocation(Constants.PARENT_FOLDER_NAME_SCREENSHOT, childFolderName, imageName, ".png");
    }

    @When("^I delete all nexus screenshots created$")
    public void i_delete_all_nexus_screenshots_created() throws Throwable {
        crossApplicationData.deleteNexusScreenshots();
    }

    @Then("^I take page screenshot$")
    public void i_take_page_screenshot() throws Throwable {
        driver.embedScreenshot(scenario);
    }

    @When("^I zoom the web page by \"([0-9]*)\" and take Screenshot and save it to \"([^\"]*)\" folder with a file name of \"([^\"]*)\" extracted from examples$")
    public void i_zoom_the_web_page_by_and_take_screenshot_and_save_it_to_folder_with_a_file_name_from_examples(float percentage, String childFolderName, String imageName) throws Throwable {
        driver.zoomInOrOutInWebPage(Constants.THIRTY);
        driver.zoomInOrOutInWebPage(percentage);
        if (childFolderName.toLowerCase().contains(Constants.GEO)) {
            childFolderName = Config.getStateCode().toLowerCase();
        }
        crossApplicationData.deleteSpecificFileWhichContainsName(childFolderName, imageName);
        driver.takeScreenshotAndSaveToALocation(Constants.PARENT_FOLDER_NAME_SCREENSHOT, childFolderName, imageName, Constants.PNG_EXTENSION);
        driver.embedScreenshot(scenario);
        driver.zoomInOrOutInWebPage(Constants.ONE_HUNDRED);
    }

    @When("^I write the web order number into the excel sheet with sheet name \"([^\"]*)\" into row number \"([0-9]*)\" and cell number \"([0-9]*)\"$")
    public void I_write_the_web_order_number_into_the_excel_sheet(String sheetName, int rowNumber, int cellnumber) throws Throwable {
        if (sheetName.toLowerCase().contains(Constants.GEO)) {
            sheetName = Config.getStateCode().toLowerCase();
        }
        driver.writeValuesIntoExcel(Constants.WEBORDER_DTD_SHEET, sheetName, Constants.WEB_ORDER, Constants.ONE, Constants.ONE);
        driver.writeValuesIntoExcel(Constants.WEBORDER_DTD_SHEET, sheetName, driver.scenarioData.getCurrentOrderNumber(), rowNumber, cellnumber);
    }

    @When("^I write the web order number into the excel sheet with sheet name \"([^\"]*)\" into row number \"([0-9]*)\" and cell number \"([0-9]*)\" in excel file with name \"([^\"]*)\"$")
    public void I_write_the_web_order_number_into_the_excel_sheet_at_location(String sheetName, int rowNumber, int cellnumber, String excelFileName) throws Throwable {
        if (!Config.getIntegrationFlag().isEmpty()) {
            if (sheetName.toLowerCase().contains(Constants.GEO)) {
                sheetName = Config.getStateCode().toLowerCase();
            }
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, Constants.WEB_ORDER, Constants.ONE, Constants.ONE);
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, driver.scenarioData.getCurrentOrderNumber(), rowNumber, cellnumber);
        }
    }

    @When("^I write the date into the excel sheet with sheet name \"([^\"]*)\" into row number \"([0-9]*)\" and cell number \"([0-9]*)\" in excel file with name \"([^\"]*)\"$")
    public void I_write_date_into_the_excel_sheet_at_location(String sheetName, int rowNumber, int cellnumber, String excelFileName) throws Throwable {
        if (!Config.getIntegrationFlag().isEmpty()) {
            if (sheetName.toLowerCase().contains(Constants.GEO)) {
                sheetName = Config.getStateCode().toLowerCase();
            }
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, Constants.DATE, Constants.ONE, cellnumber);
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, driver.getTodayDate("MM/dd/yyyy"), rowNumber, cellnumber);
        }
    }

    @When("^I write the timestamp before placing the order into the excel sheet with sheet name \"([^\"]*)\" into row number \"([0-9]*)\" and cell number \"([0-9]*)\" in excel file with name \"([^\"]*)\"$")
    public void I_write_before_timestamp_into_the_excel_sheet_at_location(String sheetName, int rowNumber, int cellnumber, String excelFileName) throws Throwable {
        if (!Config.getIntegrationFlag().isEmpty()) {
            if (sheetName.toLowerCase().contains(Constants.GEO)) {
                sheetName = Config.getStateCode().toLowerCase();
            }
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, Constants.BEFORE, Constants.ONE, cellnumber);
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, driver.scenarioData.getData(Constants.BEFORE), rowNumber, cellnumber);
        }
    }

    @When("^I write the timestamp after placing the order into the excel sheet with sheet name \"([^\"]*)\" into row number \"([0-9]*)\" and cell number \"([0-9]*)\" in excel file with name \"([^\"]*)\"$")
    public void I_write_date_and_timestamp_into_the_excel_sheet_at_location(String sheetName, int rowNumber, int cellnumber, String excelFileName) throws Throwable {
        if (!Config.getIntegrationFlag().isEmpty()) {
            if (sheetName.toLowerCase().contains(Constants.GEO)) {
                sheetName = Config.getStateCode().toLowerCase();
            }
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, Constants.AFTER, Constants.ONE, cellnumber);
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, driver.scenarioData.getData(Constants.AFTER), rowNumber, cellnumber);
        }
    }

    @When("^I zoom the web page by \"([0-9]*)\" percentage$")
    public void i_zoom_in_out_web_page(float percentage) throws Throwable {
        driver.zoomInOrOutInWebPage(percentage);
    }

    @When("^I write the total amount into the excel sheet with sheet name \"([^\"]*)\" into row number \"([0-9]*)\" and cell number \"([0-9]*)\"$")
    public void I_write_the_total_amount_into_the_excel_sheet(String sheetName, int rowNumber, int cellnumber) throws Throwable {
        if (sheetName.toLowerCase().contains(Constants.GEO)) {
            sheetName = Config.getStateCode().toLowerCase();
        }
        String rowName = Constants.TOTAL_AMOUNT_EXCEL_COLUMN_LABEL;
        driver.writeValuesIntoExcel(Constants.WEBORDER_DTD_SHEET, sheetName, rowName, Constants.ONE, Constants.TWO);
        driver.writeValuesIntoExcel(Constants.WEBORDER_DTD_SHEET, sheetName, driver.scenarioData.getData(rowName), rowNumber, cellnumber);
    }

    @When("^I write the total amount into the excel sheet with sheet name \"([^\"]*)\" into row number \"([0-9]*)\" and cell number \"([0-9]*)\" in excel file with name \"([^\"]*)\"$")
    public void I_write_the_total_amount_into_the_excel_sheet(String sheetName, int rowNumber, int cellnumber, String excelFileName) throws Throwable {
        if (!Config.getIntegrationFlag().isEmpty()) {
            if (sheetName.toLowerCase().contains(Constants.GEO)) {
                sheetName = Config.getStateCode().toLowerCase();
            }
            String rowName = Constants.TOTAL_AMOUNT_EXCEL_COLUMN_LABEL;
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, rowName, Constants.ONE, Constants.TWO);
            driver.writeValuesIntoExcel(Constants.DATA_STORAGE_FOLDER + excelFileName, sheetName, driver.scenarioData.getData(rowName), rowNumber, cellnumber);
        }
    }

    @When("^I record the time stamp \"([^\"]*)\" creating the order in YYYYMMDDHHMMSS format$")
    public void i_record_the_time_stamp_in_format(String key) throws Throwable {
        String state = Config.getStateCode().toLowerCase();
        if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD))
            state = Constants.DTD;
        posDm.recordTimeStampInPosDmFormat(state, key, 0);
    }

    @When("^I record the time stamp \"([^\"]*)\" creating the dt order in YYYYMMDDHHMMSS format for customer \"([^\"]*)\"$")
    public void i_record_the_time_stamp_creating_the_dt_order_in_format_for_customer(String key, String customer) throws Throwable {
        String state = customer.substring(customer.length() - 2);
        posDm.recordTimeStampInPosDmFormat(state.toLowerCase(), key, 0);
    }

    @When("^I record the time stamp plus \"([0-9]*)\" minutes \"([^\"]*)\" creating the web order order in YYYYMMDDHHMMSS format for customer \"([^\"]*)\"$")
    public void i_record_the_time_stamp_plus_minutes_creating_the_web_order_in_format_for_customer(int additionalMinutes, String key, String customer) throws Throwable {
        String state = customer.substring(customer.length() - 2);
        if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD))
            state = Constants.DTD;
        posDm.recordTimeStampInPosDmFormat(state.toLowerCase(), key, additionalMinutes);
    }

}