package common;

import dtc.data.ConstantsDtc;
import org.junit.Assert;
import pdl.data.ConstantsPdl;

/**
 * Created by aaronbriel on 9/17/16.
 */

public class Config {

    private static final String BROWSER = System.getProperty("browser", Constants.CHROME_BROWSER);
    private static final String IS_REMOTING = System.getProperty("isRemoting", "false");
    private static final String IS_SAUCELABS = System.getProperty("isSaucelabs", "false");
    private static final String IS_APPLITOOLS = System.getProperty("isApplitools", "false");
    private static final String IS_MOBILE = System.getProperty("isMobile", "false");
    private static final String WINDOW_SIZE = System.getProperty("windowSize", Constants.MAXIMIZED);
    private static final String HTTP = "http://";
    private static final String HTTPS = "https://";
    private static final String SCREENSHOT_ZOOM = System.getProperty("screenshotZoom",
            String.valueOf(Constants.ONE_HUNDRED));
    private static final String ADD_BOTTOM_SCREENSHOT = System.getProperty("addBottomScreenshot", "false");
    private static final String SITE_REGION = System.getProperty("siteRegion", ConstantsDtc.DT);
    private static final String SAP_DATA_SOURCE_FLAG = System.getProperty("sapDataSourceFlag", "default");
    private static final String SAP_TEST_STABILITY_METRIC_FLAG = System.getProperty("sapTestStabilityMetricFlag", "off");
    private static final String STATE_CODE = System.getProperty("geoState", Constants.STATE_AZ);
    private static String BASE_URL = System.getProperty("baseUrl", "localhost");
    private static final String DATA_SET = System.getProperty("dataSet", Constants.QA);
    private static final String INTEGRATION_FLAG = System.getProperty("integrationFlag", "");
    //NOTE: Android version 5.1 and before use "Browser", for above use "Chrome"
    private static final String ANDROID_BROWSER = System.getProperty("androidBrowser", "Chrome");
    private static final String EMULATED_DEVICE = System.getProperty("emulatedDevice", "Nexus 5");
    //NOTE: Device name for droid can be found by running ../android_sdk/platform_tools/abd devices
    private static final String DEVICE_NAME = System.getProperty("deviceName", "emulator-5554");
    private static final String UDID = System.getProperty("udid", "PLEASE SPECIFIY -Dudid");
    private static final String SAUCE_USERNAME = System.getenv("SAUCE_USERNAME");
    private static final String SAUCE_ACCESS_KEY = System.getenv("SAUCE_ACCESS_KEY");
    private static final String HMC_PATH = "/hmc/hybris";
    private static final String SAUCELABS_SAFARI_PLATFORM = "OS X 10.11";
    private static final String SAUCELABS_SAFARI_VERSION = "10.0";
    private static final String SAUCELABS_IE_PLATFORM = "Windows 7";
    private static final String SAUCELABS_IE_VERSION = "11.0";
    private static final String SAUCELABS_CHROME_PLATFORM = "Windows 7";
    private static final String SAUCELABS_CHROME_VERSION = "54.0";
    private static final String SAUCELABS_FIREFOX_PLATFORM = "Windows 10";
    private static final String SAUCELABS_FIREFOX_VERSION = "45.0";
    private static final String APPLITOOLS_API_KEY = System.getenv("APPLITOOLS_API_KEY");
    private static String STORED_BASE_URL;
    private static String HYBRIS_DETAILS_URL = "dt/omniweb/dev/hybris/";

    public static String getBaseUrl() {
        if (BASE_URL.toLowerCase().contains(HTTPS) || BASE_URL.toLowerCase().contains(HTTP)) {
            return BASE_URL;
        }
        return HTTP + BASE_URL;
    }

    public static void setBaseUrl(String url) {
        BASE_URL = url;
    }

    /**
     * Gets baseUrl with protocol prepended if valid one passed in, otherwise baseUrl returned
     *
     * @param protocol The protocol (http/https) to prepend to baseUrl
     * @return String
     */
    public static String getBaseUrl(String protocol) {
        if (protocol.equalsIgnoreCase(HTTP)) {
            return HTTP + BASE_URL;
        } else if (protocol.equalsIgnoreCase(HTTPS)) {
            return HTTPS + BASE_URL;
        } else {
            return BASE_URL;
        }
    }

    private static Boolean getIsMobile() {
        return Boolean.parseBoolean(IS_MOBILE);
    }

    public static String getSiteRegion() {
        return SITE_REGION;
    }

    public static String getSapDataSourceFlag() {
        return SAP_DATA_SOURCE_FLAG;
    }
    public static String getSapTestStabilityMetricFlag() {
        return SAP_TEST_STABILITY_METRIC_FLAG;
    }

    public static String getBrowser() {
        return BROWSER;
    }

    public static String getDataSet() {
        return DATA_SET;
    }

    public static int getScreenshotZoom() {
        return Integer.parseInt(SCREENSHOT_ZOOM);
    }

    public static boolean getAddBottomScreenshot() {
        return Boolean.parseBoolean(ADD_BOTTOM_SCREENSHOT);
    }

    public static String getIntegrationFlag() {
        return INTEGRATION_FLAG;
    }

    public static Boolean getIsRemoting() {
        return Boolean.parseBoolean(IS_REMOTING);
    }

    public static Boolean getIsSaucelabs() {
        return Boolean.parseBoolean(IS_SAUCELABS);
    }

    public static Boolean getIsApplitools() {
        return Boolean.parseBoolean(IS_APPLITOOLS);
    }

    public static String getWindowSize() {
        return WINDOW_SIZE;
    }

    public static String getSaucelabsBrowser() {
        return System.getenv("SELENIUM_BROWSER");
    }

    public static String getSaucelabsBrowserVersion() {
        return System.getenv("SELENIUM_VERSION");
    }

    public static String getSaucelabsPlatform() {
        return System.getenv("SELENIUM_PLATFORM");
    }

    public static String getSaucelabsDevice() {
        return System.getenv("SELENIUM_DEVICE");
    }

    public static String getSauceUsername() {
        return SAUCE_USERNAME;
    }

    public static String getSauceAccessKey() {
        return SAUCE_ACCESS_KEY;
    }

    public static String getDeviceName() {
        return DEVICE_NAME;
    }

    public static String getUdid() {
        return UDID;
    }

    public static String getAndroidBrowser() {
        return ANDROID_BROWSER;
    }

    public static String getEmulatedDevice() {
        return EMULATED_DEVICE;
    }

    public static String getHmcUserName() {
        return System.getenv("HMC_USER");
    }

    public static String getHmcUserPswd() {
        return System.getenv("HMC_PSWD");
    }

    public static String getSeleniumVersion() {
        return System.getenv("SELENIUM_POM_VERSION");
    }

    public static String getApplitoolsApiKey() {
        return APPLITOOLS_API_KEY;
    }

    public static String getOvcUserName() {
        return System.getenv("OVC_USER");
    }

    public static String getOvcUserPswd() {
        return System.getenv("OVC_PSWD");
    }

    public static String getOvcServer() {
        return System.getenv("OVC_SERVER");
    }

    public static String getMngrUser() {
        return System.getenv("MNGR_USER");
    }

    public static String getMngrPswd() {
        return System.getenv("MNGR_PSWD");
    }

    public static String getSapIdoc() {
        return System.getenv("SAP_IDOC");
    }

    public static String getSapUserName() {
        return System.getenv("SAP_USERNAME");
    }

    public static String getSapPassword() {
        return System.getenv("SAP_PASSWORD");
    }

    public static String getKCClientUserName() {
        return System.getenv("KC_USERNAME");
    }

    public static String getKCClientPassword() {
        return System.getenv("KC_PASSWORD");
    }

    public static String getWMUserName() {
        return System.getenv("WM_USERNAME");
    }

    public static String getWMPassword() {
        return System.getenv("WM_PASSWORD");
    }

    public static String getAddressLine1() {
        return System.getenv("ADDRESS_LINE");
    }

    public static String getCity() {
        return System.getenv("CITY");
    }

    public static String getState() {
        return System.getenv("STATE");
    }

    public static String getStateCode() {
        return STATE_CODE;
    }

    public static String getZip() {
        return System.getenv("ZIP");
    }

    public static String getExcelSheetName() {
        return System.getenv("EXCEL_SHEET_NAME");
    }

    public static String getScreenshotFolder() {
        return System.getenv("SCREENSHOT_FOLDER");
    }

    public static String getBackofficeUserName() {
        return System.getenv("BACKOFFICE_USER");
    }

    public static String getBackofficePassword() {
        return System.getenv("BACKOFFICE_PASSWORD");
    }

    public static String getMovementType() {
        return System.getenv("MOVEMENT_TYPE");
    }

    public static String getLastName() {
        return System.getenv("LAST_NAME");
    }

    public static String getTransactionType() {
        return System.getenv("TRANSACTION_TYPE");
    }

    public static String getLegacyLayawayAmount() {
        return System.getenv("LAYAWAY_AMOUNT");
    }

    public static String getDistributionCenter() {
        return System.getenv("DC");
    }

    public static String getEnvironmentFeeGlAccount() {
        try {
            return System.getenv("GL_ENVIRONMENT_FEE");
        } catch (NullPointerException n) {
            return System.getenv("GL_ENVIRONMENT_FEE");
        }
    }

    public static String getSalesTaxGlAccount() {
        try {
            return System.getenv("GL_SALES_TAX");
        } catch (NullPointerException n) {
            return System.getenv("GL_SALES_TAX");
        }
    }

    public static String getSiteNumber() {
        return System.getenv("SITE");
    }

    public static String getMyAccountUser() {
        return System.getenv("MY_ACCOUNT_USER");
    }


    public static String getAutoIntegrateUser() {
        return System.getenv("AI_USER");
    }

    public static String getAutoIntegratePassword() {
        return System.getenv("AI_PASSWORD");
    }

    public static String getNumberForTestRecords() {
        return System.getenv("NUMBER_OF_RECORDS");
    }

    public static String getHybrisWebOrderId() {
        return System.getenv("HYBRIS_WEB_ORDER");
    }

    public static String getTranStartTime() {
        return System.getenv("START_TIME");
    }

    public static String getTranEndTime() {
        return System.getenv("END_TIME");
    }

    public static String getTranDate() {
        return System.getenv("DATE");
    }

    public static String getPo() {
        return System.getenv("PO");
    }

    public static String getCustomerType() {
        return System.getenv("CUSTOMER_TYPE");
    }

    public static String getCustomerEmail() {
        return System.getenv("CUSTOMER_EMAIL");
    }

    public static String getInvoiceNumber() {
        return System.getenv("INVOICE");
    }


    /**
     * Checks if product is DTC
     *
     * @return boolean Whether product is DTC
     */
    public static boolean isDtc() {
        boolean isDtc = false;
        if (getSiteRegion() != null)
            isDtc = true;
        return isDtc;
    }

    /**
     * Checks if product is PDL
     *
     * @return boolean Whether product is PDL
     */
    public static boolean isPdl() {
        boolean isPdl = false;
        if (getBaseUrl().contains(ConstantsPdl.TIRE_FINDER))
            isPdl = true;
        return isPdl;
    }

    /**
     * Checks if product is OVC
     *
     * @return boolean Whether product is OVC
     */
    public static boolean isOvc() {
        return Boolean.parseBoolean(System.getProperty("isOvc", "false"));
    }

    /**
     * Checks if browser parameter is a mobile phone or tablet
     *
     * @return boolean Whether browser is a mobile phone or tablet
     */
    public static boolean isMobile() {
        boolean isMobile = false;
        if (isAndroidPhone() ||
                isAndroidTablet() ||
                isIphone() ||
                isIpad() ||
                getIsMobile())
            isMobile = true;
        return isMobile;
    }

    /**
     * Checks if current browser is Safari
     *
     * @return Boolean
     */
    public static boolean isSafari() {
        return getBrowser().equalsIgnoreCase(Constants.SAFARI_BROWSER);
    }

    /**
     * Checks if current browser is Chrome
     *
     * @return Boolean
     */
    public static boolean isChrome() {
        return getBrowser().equalsIgnoreCase(Constants.CHROME_BROWSER);
    }

    /**
     * Checks if current browser is Chrome with mobile emulation
     *
     * @return Boolean
     */
    public static boolean isChromeMobileEmulation() {
        return getBrowser().equalsIgnoreCase(Constants.CHROME_MOBILE_EMULATION_BROWSER);
    }

    /**
     * Checks if current browser is Fireofox
     *
     * @return Boolean
     */
    public static boolean isFirefox() {
        return getBrowser().equalsIgnoreCase(Constants.FIREFOX_BROWSER);
    }

    /**
     * Checks if current browser is Internet Explorer
     *
     * @return Boolean
     */
    public static boolean isIe() {
        return getBrowser().equalsIgnoreCase(Constants.IE_BROWSER);
    }

    /**
     * Checks if current browser is iphone
     *
     * @return Boolean
     */
    public static boolean isIphone() {
        return getBrowser().equalsIgnoreCase(Constants.IPHONE);
    }

    /**
     * Checks if current browser is ipad
     *
     * @return Boolean
     */
    public static boolean isIpad() {
        return getBrowser().equalsIgnoreCase(Constants.IPAD);
    }

    /**
     * Checks if current browser is an Android phone
     *
     * @return Boolean
     */
    public static boolean isAndroidPhone() {
        return getBrowser().equalsIgnoreCase(Constants.ANDROID_PHONE);
    }

    /**
     * Checks if current browser is an Android tablet
     *
     * @return Boolean
     */
    public static boolean isAndroidTablet() {
        return getBrowser().equalsIgnoreCase(Constants.ANDROID_TABLET);
    }


    /**
     * Gets the default store for current tests and passes it back as a string
     *
     * @return store String of store number
     */
    public static String getDefaultStore() {
        String store = null;
        if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                store = ConstantsDtc.DT_STG_DEFAULT_STORE;
            } else {
                store = ConstantsDtc.DT_QA_DEFAULT_STORE;
            }
        } else if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                store = ConstantsDtc.AT_STG_DEFAULT_STORE;
            } else {
                store = ConstantsDtc.AT_QA_DEFAULT_STORE;
            }
        }
        return store;
    }

    /**
     * Gets the default city for current store and passes it back as a string
     *
     * @return store String of city name
     */
    public static String getDefaultStoreCity() {
        String store = null;
        if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                store = ConstantsDtc.DT_STG_DEFAULT_STORE_CITY;
            } else {
                store = ConstantsDtc.DT_QA_DEFAULT_STORE_CITY;
            }
        } else if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                store = ConstantsDtc.AT_STG_DEFAULT_STORE_CITY;
            } else {
                store = ConstantsDtc.AT_QA_DEFAULT_STORE_CITY;
            }
        }
        return store;
    }

    /**
     * Gets the default store code url path for current tests and passes it back as a string
     *
     * @return store String of store code url path
     */
    public static String getDefaultStoreCodeURL() {
        String storeCode = null;
        if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                storeCode = ConstantsDtc.DT_STG_DEFAULT_STORE_CODE_PATH;
            } else {
                storeCode = ConstantsDtc.DT_QA_DEFAULT_STORE_CODE_PATH;
            }
        } else if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                storeCode = ConstantsDtc.AT_STG_DEFAULT_STORE_CODE_PATH;
            } else {
                storeCode = ConstantsDtc.AT_QA_DEFAULT_STORE_CODE_PATH;
            }
        }
        return storeCode;
    }

    /**
     * Gets the extended assortment default store code url path for current tests and passes it back as a string
     *
     * @return store String of store code url path
     */
    public static String getEADefaultStoreCodeURL() {
        String storeCode = null;
        if (getDataSet().equalsIgnoreCase(Constants.STG)) {
            storeCode = ConstantsDtc.DT_STG_DEFAULT_STORE_CODE_PATH;
        } else {
            storeCode = ConstantsDtc.DT_QA_EA_DEFAULT_STORE_CODE_PATH;
        }
        return storeCode;
    }

    /**
     * Gets the default store code url path for current tests and passes it back as a string
     *
     * @return store String of store code url path
     */
    public static String getIntegratedStoreCodeURL() {
        String storeCode = null;
        String region = Config.getSiteRegion();

        if (region.equalsIgnoreCase(ConstantsDtc.DT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                storeCode = ConstantsDtc.DT_STG_INTEGRATED_STORE_CODE_PATH;
            } else {
                storeCode = ConstantsDtc.DT_QA_INTEGRATED_STORE_CODE_PATH;
            }
        }
        return storeCode;
    }

    /**
     * Gets the full store address for current store and passes it back as a string
     *
     * @return address String of complete store address
     */
    public static String getDefaultStoreAddress() {
        String store = null;
        if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                store = ConstantsDtc.DT_STG_DEFAULT_STORE_ADDRESS;
            } else {
                store = ConstantsDtc.DT_QA_DEFAULT_STORE_ADDRESS;
            }
        } else if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                store = ConstantsDtc.AT_STG_DEFAULT_STORE_ADDRESS;
            } else {
                store = ConstantsDtc.AT_QA_DEFAULT_STORE_ADDRESS;
            }
        }
        return store;
    }

    /**
     * Gets the phone number for current store and passes it back as a string
     *
     * @return phone number
     */
    public static String getDefaultStorePhoneNumber() {
        String phoneNumber = null;
        if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                phoneNumber = ConstantsDtc.DT_STG_DEFAULT_STORE_PHONE_NUMBER;
            } else {
                phoneNumber = ConstantsDtc.DT_QA_DEFAULT_STORE_PHONE_NUMBER;
            }
        } else if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                phoneNumber = ConstantsDtc.AT_STG_DEFAULT_STORE_PHONE_NUMBER;
            } else {
                phoneNumber = ConstantsDtc.AT_QA_DEFAULT_STORE_PHONE_NUMBER;
            }
        } else if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            phoneNumber = ConstantsDtc.DTD_DEFAULT_PHONE_NUMBER;
        }
        return phoneNumber;
    }

    /**
     * Gets the store code for current store and passes it back as a string
     *
     * @return store code
     */
    public static String getDefaultStoreCode() {
        String storeCode = null;
        if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                storeCode = ConstantsDtc.DT_STG_DEFAULT_STORE_CODE;
            } else {
                storeCode = ConstantsDtc.DT_QA_DEFAULT_STORE_CODE;
            }
        } else if (getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                storeCode = ConstantsDtc.AT_STG_DEFAULT_STORE_CODE;
            } else {
                storeCode = ConstantsDtc.AT_QA_DEFAULT_STORE_CODE;
            }
        }
        return storeCode;
    }

    /**
     * Returns the saucelabs url
     *
     * @return url      The saucelabs url
     */
    public static String getSaucelabsUrl() {
        return "http://" + SAUCE_USERNAME + ":" + SAUCE_ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";
    }

    /**
     * Returns the default saucelabs platform when running locally
     *
     * @return platform     The default saucelabs platform based on browser
     */
    public static String getDefaultSaucelabsPlatform() {
        String platform = null;
        if (isSafari()) {
            platform = SAUCELABS_SAFARI_PLATFORM;
        } else if (isChrome()) {
            platform = SAUCELABS_CHROME_PLATFORM;
        } else if (isFirefox()) {
            platform = SAUCELABS_FIREFOX_PLATFORM;
        } else if (isIe()) {
            platform = SAUCELABS_IE_PLATFORM;
        }
        return platform;
    }

    /**
     * Returns the default saucelabs browser version when running locally
     *
     * @return version     The default saucelabs browser version based on browser
     */
    public static String getDefaultSaucelabsVersion() {
        String version = null;
        if (isSafari()) {
            version = SAUCELABS_SAFARI_VERSION;
        } else if (isChrome()) {
            version = SAUCELABS_CHROME_VERSION;
        } else if (isFirefox()) {
            version = SAUCELABS_FIREFOX_VERSION;
        } else if (isIe()) {
            version = SAUCELABS_IE_VERSION;
        }
        return version;
    }

    /**
     * Returns the Url for the Hybris Management Console. First checks if Steve notation is in base Url. Steve
     * environments  route dt, dtd, and at to same hybris site. Else HMC Url will be based on -DdataSet.
     *
     * @return Url for Hybris Management Console
     */
    public static String getHmcUrl() {
        if (getBaseUrl().contains("steve")) {
            return HTTP + "steve.discounttire.com" + HMC_PATH;
        } else {
            if (getDataSet().equalsIgnoreCase(Constants.STG)) {
                return HTTP + "cpstg.discounttire.com" + HMC_PATH;
            } else {
                return getBaseUrl() + HMC_PATH;
            }
        }
    }

    /**
     * Returns Hybris URL for integration testing based on -DdataSet.
     *
     * @return Url for Hybris
     */
    public static String getHybrisUrl() {
        //TODO: Currently assumes DT as site Region. Expand if needed.
        if (getDataSet().equalsIgnoreCase(Constants.STG)) {
            return HTTPS + "stg.discounttire.com";
        } else {
            return HTTPS + "dtqa1.epic.discounttire.com";
        }
    }

    /**
     * Returns WebMethod url for cross application Extended Assortment testing.
     *
     * @return URL for WebMethod
     */
    public static String getWMUrl() {
        if (Config.getDataSet().equals(Constants.QA) || Config.getDataSet().equalsIgnoreCase(Constants.RK2)) {
            return HTTPS + "esbmwslq01:4444";
        } else {
            return HTTPS + "esbmwsls01:4444";
        }
    }

    /**
     * Returns url for PatchTesting
     *
     * @return URL for KNOWLEDGE CENTER
     */
    public static String getPATCHUrl() {
        return HTTP + Constants.PATCH_URL;
    }

    /**
     * Returns nwbc url for QA/Stage/Dev environment
     *
     * @return URL for nwbc
     */
    public static String getNWBCUrl() {
        if (Config.getDataSet().equals(Constants.QA)) {
            return HTTP + Constants.NWBC_URL_RTK;
        } else if (Config.getDataSet().equals(Constants.RK2)) {
            return HTTPS + Constants.NWBC_URL_RK2;
        } else if (Config.getDataSet().equals(Constants.STG)) {
            return HTTP + Constants.NWBC_URL_RTQ;
        } else if (Config.getDataSet().equals(Constants.DEV)) {
            return HTTPS + Constants.NWBC_URL_RTD;
        } else if  (Config.getDataSet().equals(Constants.SAP_RT2)) {
            return HTTP + Constants.NWBC_URL_RT2;
        } else if (Config.getDataSet().equalsIgnoreCase(Constants.RQ2)) {
            return HTTP + Constants.NWBC_URL_RQ2;
        } else {
            Assert.fail("Please check -DdataSet parameter");
            return null;
        }
    }

    /**
     * Returns POSDM url for QA or Stage environment
     *
     * @return URL for posdm
     */
    public static String getPOSDMUrl(String dataSet) {
        //TODO: Need to revisit to add STG URL
        if (dataSet.equalsIgnoreCase(Constants.QA) || dataSet.equalsIgnoreCase(Constants.RK2)) {
            return HTTP + Constants.SAP_RTL;
        } else if (dataSet.equalsIgnoreCase(Constants.STG)) {
            return HTTP + Constants.SAP_RTC;
        } else if (dataSet.equalsIgnoreCase(Constants.DEV)) {
            return HTTP + Constants.SAP_RTA;
        } else if (Config.getDataSet().equalsIgnoreCase(Constants.PRD)) {
            return HTTP + Constants.SAP_RTB;
        } else {
            Assert.fail("Please check -DdataSet parameter");
            return null;
        }
    }

    /**
     * Returns POSDM url for QA or Stage environment
     *
     * @return URL for posdm
     */
    public static String getPOSDMUrl() {
        return getPOSDMUrl(Config.getDataSet());
    }

    /**
     * Returns POSDT url for QA or Stage environment
     *
     * @return URL for posdt
     */
    public static String getPOSDTUrl(String dataSet) {
        //TODO: Need to revisit to add STG URL
        if (dataSet.equalsIgnoreCase(Constants.QA) || dataSet.equalsIgnoreCase(Constants.RK2)) {
            return HTTP + Constants.SAP_CAQ;
        } else if (dataSet.equalsIgnoreCase(Constants.STG)) {
            return HTTP + Constants.SAP_CAS;
        } else if (dataSet.equalsIgnoreCase(Constants.DEV)) {
            return HTTP + Constants.SAP_CAD;
        } else {
            Assert.fail("Please check -DdataSet parameter");
            return null;
        }
    }

    /**
     * Returns POSDT url for QA or Stage environment
     *
     * @return URL for posdt
     */
    public static String getPOSDTUrl() {
        return getPOSDTUrl(Config.getDataSet());
    }

    /*
     * Returns SAP ECC url for QA or Stage environment
     *
     * @return URL for SAPECC
     */
    public static String getSapEccUrl(String dataSet) {
        if (dataSet.equalsIgnoreCase(Constants.QA)) {
            return HTTP + Constants.SAP_RTK_URL;
        } else if (dataSet.equalsIgnoreCase(Constants.DEV)) {
            return HTTP + Constants.SAP_RTD_URL;
        } else if (dataSet.equalsIgnoreCase(Constants.STG)) {
            return HTTP + Constants.SAP_RTQ_URL;
        } else if (dataSet.equalsIgnoreCase(Constants.RK2)) {
            return HTTP + Constants.SAP_RK2_URL;
        } else if (dataSet.equalsIgnoreCase(Constants.SAP_RT2)) {
            return HTTP + Constants.SAP_RT2_URL;
        } else if (dataSet.equalsIgnoreCase(Constants.SAP_RTD)) {
                return HTTP + Constants.SAP_RTD_URL;
        }  else if (dataSet.equalsIgnoreCase(Constants.DEV)) {
            return HTTP + Constants.SAP_RD2_URL;
        } else if (dataSet.equalsIgnoreCase(Constants.RQ2)) {
            return HTTP + Constants.SAP_RQ2_URL;
        } else {
            Assert.fail("Please check -DdataSet parameter");
            return null;
        }
    }

    /*
     * Returns SAP ECC url for QA or Stage environment
     *
     * @return URL for SAPECC
     */
    public static String getSapEccUrl() {
        return getSapEccUrl(Config.getDataSet());
    }

    /*
     * Returns SAP Web content Url
     *
     * @return URL for Web content
     */
    public static String getSapWebContentUrl() {
        if (Config.getDataSet().equalsIgnoreCase(Constants.QA)) {
            return HTTP + Constants.SAP_RTK_WEB_CONTENT_URL;
        } else if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            return HTTP + Constants.SAP_RTQ_WEB_CONTENT_URL;
        } else{
            return null;
        }
    }

    /*
     * Returns Back Office Url
     *
     * @return URL for Back office QA and Stage
     */
    public static String getBackofficeUrl() {
        if (Config.getDataSet().equals(Constants.QA)) {
            return HTTPS + Constants.BACKOFFICE_QA_URL;
        } else {
            return HTTPS + Constants.BACKOFFICE_STG_URL;
        }
    }

    /*
     * Returns Back Office Url
     *
     * @return URL for getTreadwellApplicationStatus QA and Stage
     */
    public static String getTreadwellApplicationStatus() {
        if (Config.getDataSet().equals(Constants.QA)) {
            return HTTPS + Constants.TREADWELL_APPLICATION_STATUS_QA_URL;
        } else {
            return HTTPS + Constants.TREADWELL_APPLICATION_STATUS_STG_URL;
        }
    }

    /**
     * Returns OVC URL for integration testing.
     *
     * @return Url for OVC
     */
    public static String getOvcUrl() {
        return "localhost:32040/index.html";
    }

    /**
     * Stores current baseUrl to STORED_BASE_URL
     */
    public static void storeCurrentBaseUrl() {
        STORED_BASE_URL = getBaseUrl(Constants.NONE);
    }

    /**
     * Gets stored baseUrl
     *
     * @return String Stored baseUrl
     */
    public static String getStoredBaseUrl() {
        return STORED_BASE_URL;
    }

    /**
     * Get the store type based on the current site region
     *
     * @return storeType 'America's Tire Store' or 'Discount Tire Store'
     */
    public static String getStoreType() {
        String storeType = null;
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            storeType = ConstantsDtc.AMERICAS_TIRE_STORE;
        } else {
            storeType = ConstantsDtc.DISCOUNT_TIRE_STORE;
        }
        return storeType;
    }

    /**
     * Get the image URL based on the current site region
     *
     * @return imageUrl
     */
    public static String getSolicitedReviewStoreImageUrl() {
        String baseUrl = Config.getBaseUrl();

        String imageUrl = null;
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            imageUrl = baseUrl + ConstantsDtc.IMAGE_FILEPATH_DT;
        } else {
            imageUrl = baseUrl + ConstantsDtc.IMAGE_FILEPATH_AT;
        }
        return imageUrl;
    }

    /**
     * Get the store name
     *
     * @return storeName
     */
    public static String getStoreName() {
        String store = Config.getDefaultStore();
        String storeType = Config.getStoreType();

        String storeName = storeType + " (" + store + ")";
        // TODO: Add conditions for staging when text labels are used there.
        if (Config.getDataSet().equals(Constants.QA)) {
            String storeTextLabel = ConstantsDtc.DT_QA_DEFAULT_STORE_TEXT_LABEL;

            if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
                storeTextLabel = ConstantsDtc.AT_QA_DEFAULT_STORE_TEXT_LABEL;
            }
            storeName = storeTextLabel + " (" + store + ")";
        }
        return storeName;
    }

    /**
     * Get Token service url
     *
     * @return string tokenUrl
     */
    public static String returnTokenUrl() {
        String tokenUrl;
        if (Config.getDataSet().equals(Constants.STG)) {
            tokenUrl = "https://cpstg.discounttire.com" + Constants.AUTHORIZATION_SERVER_LINK;
        } else
            tokenUrl = Config.getBaseUrl() + Constants.AUTHORIZATION_SERVER_LINK;
        return tokenUrl;
    }

    /**
     * Get Price service url
     *
     * @return string priceUrl
     */
    public static String returnPriceUrl() {
        String priceUrl = null;
        if (Config.getDataSet().equals(Constants.QA)) {
            //TODO - Once we have the endpoint, will update accordingly
            priceUrl = "https://dtdev1.epic.discounttire.com:9002/webservice/";
        } else if (Config.getDataSet().equals(Constants.DEV)) {
            priceUrl = "https://dtdev1.epic.discounttire.com:9002/webservice/";
        } else
            //TODO - Stage endpoint
            priceUrl = "https://dtdev1.epic.discounttire.com:9002/webservice/";
        return priceUrl;
    }

    /**
     * Get lookup service url
     *
     * @return urls string
     */
    public static String returnBuildNumberLookupURL() {
        String returnString = null;
        if (getBaseUrl().isEmpty())
            return null;
        if (getBaseUrl().endsWith("/"))
            returnString = getBaseUrl() + HYBRIS_DETAILS_URL;
        else
            returnString = getBaseUrl() + "/" + HYBRIS_DETAILS_URL;
        return returnString;
    }

    /*
     * Returns Fleet Url
     *
     * @return URL for Fleet
     */
    public static String getAutoFleetUrl() {
        if (Config.getDataSet().equals(Constants.QA)) {
            return Constants.FLEET_QA;
        } else {
            return Constants.FLEET_DEV;
        }
    }

    /*
     * Returns MicroStrategy Url
     *
     * @return URL for MicroStrategy
     */
    public static String getMicroStrategyUrl() {
        if (Config.getDataSet().equals(Constants.QA)) {
            return Constants.MICROSTRATEGY_QA;
        } else {
            return Constants.MICROSTRATEGY_STG;
        }
    }

    /*
     * Returns getGraphQLUrl Url
     *
     * @return URL for getGraphQLUrl
     */
    public static String getGraphQLUrl() {
        if (Config.getDataSet().equals(Constants.QA)) {
            return Constants.GRAPHQL_QA;
        } else if (Config.getDataSet().equals(Constants.STG)) {
            return Constants.GRAPHQL_STG;
        } else {
            return Constants.GRAPHQL_DEV;
        }
    }

    /**
     * Returns autoIntegrate URL
     *
     * @return URL for autoIntegrate
     */
    public static String getAutoIntegrateUrl() {
        if (Config.getDataSet().equals(Constants.QA)) {
            return Constants.AUTOINTEGRATE_URL_QA;
        } else {
            return Constants.AUTOINTEGRATE_URL_STG;
        }
    }

    /**
     * Returns VTV Car service URL
     *
     * @return URL
     */
    public static String returnVtvCarServiceUrl() {
        if (Config.getDataSet().equals(Constants.DEV)) {
            return "http://carhanld1.car.trtc.com:8000/VTV/api/searchVehicle.xsjs?VIN=";
        } else {
            Assert.fail("Please specify the value in -Ddataset or add implementation");
            return null;
        }
    }
}