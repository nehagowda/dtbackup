package common;

import dtc.data.Customer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aaronbriel on 9/17/16.
 */

public class Constants {

    public static final int ONE_THOUSAND = 1000;
    public static final int TWO_THOUSAND = 2000;
    public static final int THREE_THOUSAND = 3000;
    public static final int FOUR_THOUSAND = 4000;
    public static final int FIVE_THOUSAND = 5000;
    public static final int SIX_THOUSAND = 6000;
    public static final int SEVEN_THOUSAND = 7000;
    public static final int EIGHT_THOUSAND = 8000;
    public static final int FIFTEEN_THOUSAND = 15000;
    public static final int THIRTY_THOUSAND = 30000;
    public static final int SIXTY_FIVE_THOUSAND = 65000;
    public static final int NINETY_THOUSAND = 90000;
    public static final int THREE_HUNDRED = 300;
    public static final int FIVE_HUNDRED = 500;
    public static final int SIX_HUNDRED = 600;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FIVE = 5;
    public static final int TEN = 10;
    public static final int TWENTY = 20;
    public static final int THIRTY = 30;
    public static final int SIXTY = 60;
    public static final int ONE_HUNDRED_TWENTY = 120;
    public static final int ONE_HUNDRED_EIGHTY = 180;
    public static final int ONE_HUNDRED_FIFTY_FIVE = 155;
    public static final int NEGATIVE_ONE = -1;
    public static final int ONE_HUNDRED = 100;
    public static final int PEAK_DELAY_MINUTES = 45;
    public static final int ITEM_CLASS_COUNT = 2;
    public static final int ZERO = 0;
    public static final int RANDOM_GENERATOR_MIN = 1;
    public static final int RANDOM_GENERATOR_MAX = 900000;

    public static final String DEV = "dev";
    public static final String QA = "qa";
    public static final String STG = "stg";
    public static final String RK2 = "rk2";
    public static final String PRD = "PRD";

    public static final String FIREFOX_BROWSER = "FIREFOX";
    public static final String CHROME_BROWSER = "CHROME";
    public static final String CHROME_MOBILE_EMULATION_BROWSER = "CHROME_MOBILE_EMULATION";
    public static final String IE_BROWSER = "IE";
    public static final String SAFARI_BROWSER = "SAFARI";
    public static final String IPHONE = "IPHONE";
    public static final String TREADWELLONE = "Treadwell One";
    public static final String TREADWELLTWO = "Treadwell Two";
    public static final String IPAD = "IPAD";
    public static final String ANDROID_PHONE = "ANDROID_PHONE";
    public static final String ANDROID_TABLET = "ANDROID_TABLET";

    public static final String IE_REMOTE = "http://10.0.157.58:7777/wd/hub";
    public static final String CHROME_REMOTE = "http://10.0.157.58:7777/wd/hub";
    public static final String FIREFOX_REMOTE = "http://10.0.175.153:5555/wd/hub ";
    public static final String SAFARI_REMOTE = "http://10.0.177.38:4444/wd/hub";

    public static final String NONE = "none";
    public static final String DEFAULT_CONTENT = "Default Content";
    public static final String PARENT = "Parent";
    public static final String REFRESH = "Refresh";

    public static final String BACKGROUND_COLOR = "background-color";
    public static final String BORDER_TOP_COLOR = "border-top-color";
    public static final String BORDER_BOTTOM_COLOR = "border-bottom-color";
    public static final String COLOR = "color";
    public static final String GREEN = "Green";
    public static final String YELLOW = "Yellow";
    public static final String RED = "Red";
    public static final String GREY = "Grey";
    public static final String YELLOW_BANNER_RGB = "rgb(255, 161, 13)";
    public static final String YELLOW_BANNER_RGBA = "rgba(255, 161, 13, 1)";
    public static final String GREEN_BANNER_RGB = "rgb(82, 162, 64)";
    public static final String GREEN_BANNER_RGBA = "rgba(82, 162, 64, 1)";
    public static final String RED_COLOR_RGB = "rgb(237, 28, 36)";
    public static final String RED_COLOR_RGBA = "rgba(237, 28, 36, 1)";
    public static final String ALT_RED_COLOR_RGB = "rgb(205, 38, 37)";
    public static final String ALT_RED_COLOR_RGBA = "rgba(205, 38, 37, 1)";
    public static final String BORDER = "border";
    public static final String DTC_RED = "rgb(255, 0, 0)";
    //TODO: Leaving last color for RED_COLOR_SEARCH_BUTTON, as this new color was introduced
    //TODO (cont) and changed within a week "rgb(217, 46, 45)";
    public static final String RED_COLOR_SEARCH_BUTTON = "rgb(205, 38, 37)";
    public static final String BLUE_LINKTEXT_RGBA = "rgba(52, 131, 222, 1)";

    public static final String[] CREDIT_CARDS = {"Visa", "MasterCard Bopis", "American Express", "Discover"};

    public static final String INSTAGRAM = "instagram";
    public static final String CAREERS_PARTIAL_URL = "careers.discounttire.com";
    public static final String KC_URL = "http://portalprd.discounttire.com/portal/server.pt";
    public static final String STORE_LOCATOR_PARTIAL_URL = "store-locator";
    public static final String RETURN_POLICY_PARTIAL_URL = "customer-service/return-policy";

    public static final String ATTRIBUTE_ALT = "alt";
    public static final String ATTRIBUTE_SRC = "src";
    public static final String ATTRIBUTE_HREF = "href";
    public static final String ATTRIBUTE_PLACEHOLDER = "placeholder";

    public static final String CLASS_NAME = "class name";
    public static final String CSS = "css";
    public static final String ID = "id";
    public static final String INPUT = "input";
    public static final String NAME = "name";
    public static final String TAG_NAME = "tag name";
    public static final String LINK_TEXT = "link text";
    public static final String PARTIAL_LINK_TEXT = "partial link text";
    public static final String XPATH = "xpath";
    public static final String TITLE = "title";
    public static final String SUBTITLE = "subtitle";
    public static final String DATA_Q_TITLE = "data-q-title";
    public static final String NOT_YET_FULLY_RECEIVED = "not yet fully received";

    public static final String ENABLED = "enabled";
    public static final String DISABLED = "disabled";
    public static final String ACTIVE = "active";
    public static final String CHANGED = "changed";

    public static final String HTTP = "http://";
    public static final String HTTPS = "https://";
    public static final String LOCALHOST = "localhost";

    public static final String CLASSNAME = "className";
    public static final String CLASS = "class";
    public static final String VALUE = "value";
    public static final String INNERTEXT = "innerText";

    public static final String OPEN = "open";
    public static final String CITY = "City";
    public static final String STATE = "State";
    public static final String OTHER = "Other";
    public static final String NEXT = "Next";
    public static final String EMAIL = "Email";

    public static final String ORDER = "Order";

    public static final String CANADA = "Can";
    public static final String PRICE = "price";

    public static final String IMG_EXT_JPG = ".jpg";
    public static final String NO_IMG_EXT_PNG = "no_image.png";
    public static final String SPAN = "span";

    public static final String UNITED_STATES = "United States";
    public static final String FEE = "fee";
    public static final String SERVICE = "service";
    public static final String DAY_AM = "AM";
    public static final String DAY_PM = "PM";
    public static final String TIRE = "Tire";
    public static final String WHEEL = "Wheel";
    public static final String PACKAGE = "Package";
    public static final String ACCESSORIES = "Accessories";
    public static final String ORDERS_FILE_LOC = "\\\\10.0.157.13\\c$\\Users\\QATest\\";
    public static final String ACCESSORYTPMS = "AccessoryTPMS";
    public static final String LABOR = "Labor";
    public static final String ACCESSORYVALVESTEM = "AccessoryValveStem";
    public static final String MISCWHEEL = "MiscWheel";
    public static final String MISCTIRE = "MiscTire";

    public static final String USERS = "C:\\Users\\";
    public static final String USER_NAME = "user.name";
    public static final String USER_DIR = "user.dir";
    public static final String DOWNLOADS = "\\Downloads\\";
    public static final String PATH_TO_JSON_OBJECTS = "/src/test/resources/json_Objects/";
    public static final String DOWNLOAD_PATH = Constants.USERS + System.getProperty(Constants.USER_NAME) + Constants.DOWNLOADS;
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JSON = "application/json";
    public static final String PARTNER_NO = "Partner No.";
    public static final String VENDOR = "Vendor";
    public static final String ACCEPT = "Accept";
    public static final String UTF8 = "UTF8";
    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final String NO_LONGER = "No Longer";
    public static final String BACK = "BACK";
    public static final String REMOVE = "REMOVE";
    public static final String APPEND = "APPEND";
    public static final String BUTTON = "button";
    public static final String REGISTERED = "registered";
    public static final String JENKINS = "jenkins";

    public static final String THREE_HUNDRED_STRING = "300";

    public static final String DATE = "date";
    public static final String INVOICE = "invoice";
    public static final String CLOSE = "Close";
    public static final String EXECUTE = "Execute";

    public static final String MONDAY = "Monday";
    public static final String TUESDAY = "Tuesday";
    public static final String WEDNESDAY = "Wednesday";
    public static final String THURSDAY = "Thursday";
    public static final String FRIDAY = "Friday";
    public static final String SATURDAY = "Saturday";
    public static final String SUNDAY = "Sunday";

    public static final String MON = "Mon";
    public static final String TUE = "Tue";
    public static final String WED = "Wed";
    public static final String THU = "Thu";
    public static final String FRI = "Fri";
    public static final String SAT = "Sat";
    public static final String SUN = "Sun";

    public static final String JANUARY = "January";
    public static final String FEBRUARY = "February";
    public static final String MARCH = "March";
    public static final String APRIL = "April";
    public static final String MAY = "May";
    public static final String JUNE = "June";
    public static final String JULY = "July";
    public static final String AUGUST = "August";
    public static final String SEPTEMBER = "September";
    public static final String OCTOBER = "October";
    public static final String NOVEMBER = "November";
    public static final String DECEMBER = "December";

    public static final String JAN = "Jan";
    public static final String FEB = "Feb";
    public static final String MAR = "Mar";
    public static final String APR = "Apr";
    public static final String JUN = "Jun";
    public static final String JUL = "Jul";
    public static final String AUG = "Aug";
    public static final String SEP = "Sep";
    public static final String SEPT = "Sept";
    public static final String OCT = "Oct";
    public static final String NOV = "Nov";
    public static final String DEC = "Dec";

    public static final String ROLE = "role";
    public static final String MILES = "Miles";

    public static final String STATE_AZ = "az";
    public static final String STATE_GA = "ga";
    public static final String STATE_HI = "hi";
    public static final String STATE_CA = "ca";
    public static final String STATE_OH = "oh";
    public static final String STATE_TX = "tx";
    public static final String STATE_SC = "sc";
    public static final String STATE_IL = "il";
    public static final String STATE_FL = "fl";
    public static final String STATE_NV = "nv";
    public static final String STATE_LA = "la";
    public static final String STATE_NY = "ny";
    public static final String STATE_VA = "va";
    public static final String STATE_NC = "nc";
    public static final String STATE_MS = "ms";
    public static final String STATE_CT = "ct";
    public static final String STATE_AK = "ak";
    public static final String STATE_AE = "ae";
    public static final String STATE_AA = "aa";
    public static final String STATE_AR = "ar";
    public static final String STATE_CO = "co";
    public static final String STATE_DE = "de";
    public static final String STATE_ID = "id";
    public static final String STATE_IA = "ia";
    public static final String STATE_KS = "ks";
    public static final String STATE_ME = "me";
    public static final String STATE_MO = "mo";
    public static final String STATE_MT = "mt";
    public static final String STATE_NE = "ne";
    public static final String STATE_NH = "nh";
    public static final String STATE_NM = "nm";
    public static final String STATE_ND = "nd";
    public static final String STATE_OK = "ok";
    public static final String STATE_OR = "or";
    public static final String STATE_RI = "ri";
    public static final String STATE_SD = "sd";
    public static final String STATE_TN = "tn";
    public static final String STATE_UT = "ut";
    public static final String STATE_VT = "vt";
    public static final String STATE_WV = "wv";
    public static final String STATE_WY = "wy";
    public static final String STATE_WA = "wa";
    public static final String STATE_MI = "mi";
    public static final String STATE_AL = "al";
    public static final String STATE_IN = "in";
    public static final String STATE_KY = "ky";
    public static final String STATE_MD = "md";
    public static final String STATE_MA = "ma";
    public static final String STATE_MN = "mn";
    public static final String STATE_NJ = "nj";
    public static final String STATE_PA = "pa";
    public static final String STATE_WI = "wi";
    public static final String STATE_FPO = "fpo";

    public static final String TIME_ZONE_CST = "CST";
    public static final String TIME_ZONE_MST = "MST";

    public static final String GEO = "geo";

    public static final String SC_GEO_IP = "98.25.231.238";
    public static final String IL_GEO_IP = "184.154.83.119";
    public static final String CA_GEO_IP = "100.32.201.0";
    public static final String FL_GEO_IP = "100.13.0.0";
    public static final String NV_GEO_IP = "199.241.138.201";
    public static final String LA_GEO_IP = "72.211.66.53";
    public static final String NY_GEO_IP = "100.4.146.104";
    public static final String VA_GEO_IP = "208.253.114.165";
    public static final String NC_GEO_IP = "65.190.141.7";
    public static final String MS_GEO_IP = "130.18.139.171";
    public static final String CT_GEO_IP = "32.209.160.112";
    public static final String AK_GEO_IP = "66.230.105.38";
    public static final String AR_GEO_IP = "24.35.188.208";
    public static final String CO_GEO_IP = "63.151.67.7";
    public static final String DE_GEO_IP = "104.160.11.182";
    public static final String GA_GEO_IP = "104.104.245.100";
    public static final String HI_GEO_IP = "107.152.189.173";
    public static final String ID_GEO_IP = "70.56.146.217";
    public static final String IA_GEO_IP = "108.178.221.130";
    public static final String KS_GEO_IP = "173.197.41.70";
    public static final String ME_GEO_IP = "69.193.69.79";
    public static final String MO_GEO_IP = "208.110.83.202";
    public static final String MT_GEO_IP = "69.145.52.224 ";
    public static final String NE_GEO_IP = "181.214.156.122";
    public static final String NH_GEO_IP = "75.68.29.114";
    public static final String NM_GEO_IP = "199.115.134.208";
    public static final String ND_GEO_IP = "67.60.147.112";
    public static final String OK_GEO_IP = "129.15.57.1";
    public static final String OR_GEO_IP = "74.120.152.136";
    public static final String RI_GEO_IP = "131.109.19.9";
    public static final String SD_GEO_IP = "204.124.121.55";
    public static final String TN_GEO_IP = "38.29.144.26";
    public static final String UT_GEO_IP = "162.241.169.13";
    public static final String VT_GEO_IP = "199.21.140.134";
    public static final String WV_GEO_IP = "173.80.42.199";
    public static final String WY_GEO_IP = "23.251.52.29";
    public static final String TX_GEO_IP = "192.227.139.106";
    public static final String OH_GEO_IP = "10.18.14.0";
    public static final String WA_GEO_IP = "67.170.62.23";
    public static final String TEST = "test";
    public static final String MI_GEO_IP = "173.162.61.13";
    public static final String AL_GEO_IP = "71.8.36.114";
    public static final String IN_GEO_IP = "50.90.186.218";
    public static final String KY_GEO_IP = "170.185.249.179";
    public static final String MD_GEO_IP = "96.255.57.162";
    public static final String MA_GEO_IP = "96.81.109.17";
    public static final String MN_GEO_IP = "69.54.33.135";
    public static final String NJ_GEO_IP = "45.56.108.119";
    public static final String PA_GEO_IP = "147.31.182.137";
    public static final String WI_GEO_IP = "174.197.0.154";
    public static final String FPO_GEO_IP = "207.224.248.8";
    public static final String HOME = "Home";

    public static HashMap<String, String> geoCustomer = new HashMap<>();
    public static HashMap<String, String> geoIP = new HashMap<>();
    static {
        geoCustomer.put(STATE_SC, Customer.CustomerType.DEFAULT_CUSTOMER_SC.toString());
        geoCustomer.put(STATE_IL, Customer.CustomerType.DEFAULT_CUSTOMER_IL.toString());
        geoCustomer.put(STATE_CA, Customer.CustomerType.DEFAULT_CUSTOMER_CA.toString());
        geoCustomer.put(STATE_FL, Customer.CustomerType.DEFAULT_CUSTOMER_FL.toString());
        geoCustomer.put(STATE_NV, Customer.CustomerType.DEFAULT_CUSTOMER_NV.toString());
        geoCustomer.put(STATE_LA, Customer.CustomerType.DEFAULT_CUSTOMER_LA.toString());
        geoCustomer.put(STATE_NY, Customer.CustomerType.DEFAULT_CUSTOMER_NY.toString());
        geoCustomer.put(STATE_VA, Customer.CustomerType.DEFAULT_CUSTOMER_VA.toString());
        geoCustomer.put(STATE_NC, Customer.CustomerType.DEFAULT_CUSTOMER_NC.toString());
        geoCustomer.put(STATE_MS, Customer.CustomerType.DEFAULT_CUSTOMER_MS.toString());
        geoCustomer.put(STATE_CT, Customer.CustomerType.DEFAULT_CUSTOMER_CT.toString());
        geoCustomer.put(STATE_AK, Customer.CustomerType.DEFAULT_CUSTOMER_AK.toString());
        geoCustomer.put(STATE_AR, Customer.CustomerType.DEFAULT_CUSTOMER_AR.toString());
        geoCustomer.put(STATE_CO, Customer.CustomerType.DEFAULT_CUSTOMER_CO.toString());
        geoCustomer.put(STATE_DE, Customer.CustomerType.DEFAULT_CUSTOMER_DE.toString());
        geoCustomer.put(STATE_ID, Customer.CustomerType.DEFAULT_CUSTOMER_ID.toString());
        geoCustomer.put(STATE_IA, Customer.CustomerType.DEFAULT_CUSTOMER_IA.toString());
        geoCustomer.put(STATE_KS, Customer.CustomerType.DEFAULT_CUSTOMER_KS.toString());
        geoCustomer.put(STATE_ME, Customer.CustomerType.DEFAULT_CUSTOMER_ME.toString());
        geoCustomer.put(STATE_MO, Customer.CustomerType.DEFAULT_CUSTOMER_MO.toString());
        geoCustomer.put(STATE_MT, Customer.CustomerType.DEFAULT_CUSTOMER_MT.toString());
        geoCustomer.put(STATE_NE, Customer.CustomerType.DEFAULT_CUSTOMER_NE.toString());
        geoCustomer.put(STATE_NH, Customer.CustomerType.DEFAULT_CUSTOMER_NH.toString());
        geoCustomer.put(STATE_NM, Customer.CustomerType.DEFAULT_CUSTOMER_NM.toString());
        geoCustomer.put(STATE_ND, Customer.CustomerType.DEFAULT_CUSTOMER_ND.toString());
        geoCustomer.put(STATE_OK, Customer.CustomerType.DEFAULT_CUSTOMER_OK.toString());
        geoCustomer.put(STATE_OR, Customer.CustomerType.DEFAULT_CUSTOMER_OR.toString());
        geoCustomer.put(STATE_RI, Customer.CustomerType.DEFAULT_CUSTOMER_RI.toString());
        geoCustomer.put(STATE_TN, Customer.CustomerType.DEFAULT_CUSTOMER_TN.toString());
        geoCustomer.put(STATE_UT, Customer.CustomerType.DEFAULT_CUSTOMER_UT.toString());
        geoCustomer.put(STATE_VT, Customer.CustomerType.DEFAULT_CUSTOMER_VT.toString());
        geoCustomer.put(STATE_WV, Customer.CustomerType.DEFAULT_CUSTOMER_WV.toString());
        geoCustomer.put(STATE_WY, Customer.CustomerType.DEFAULT_CUSTOMER_WY.toString());
        geoCustomer.put(STATE_GA, Customer.CustomerType.DEFAULT_CUSTOMER_GA.toString());
        geoCustomer.put(STATE_HI, Customer.CustomerType.DEFAULT_CUSTOMER_HI.toString());
        geoCustomer.put(STATE_TX, Customer.CustomerType.DEFAULT_CUSTOMER_TX.toString());
        geoCustomer.put(STATE_OH, Customer.CustomerType.DEFAULT_CUSTOMER_OH.toString());
        geoCustomer.put(STATE_SD, Customer.CustomerType.DEFAULT_CUSTOMER_SD.toString());
        geoCustomer.put(STATE_WA, Customer.CustomerType.DEFAULT_CUSTOMER_WA.toString());
        geoCustomer.put(STATE_AL, Customer.CustomerType.DEFAULT_CUSTOMER_AL.toString());
        geoCustomer.put(STATE_IN, Customer.CustomerType.DEFAULT_CUSTOMER_IN.toString());
        geoCustomer.put(STATE_KY, Customer.CustomerType.DEFAULT_CUSTOMER_KY.toString());
        geoCustomer.put(STATE_MD, Customer.CustomerType.DEFAULT_CUSTOMER_MD.toString());
        geoCustomer.put(STATE_MA, Customer.CustomerType.DEFAULT_CUSTOMER_MA.toString());
        geoCustomer.put(STATE_MN, Customer.CustomerType.DEFAULT_CUSTOMER_MN.toString());
        geoCustomer.put(STATE_NJ, Customer.CustomerType.DEFAULT_CUSTOMER_NJ.toString());
        geoCustomer.put(STATE_PA, Customer.CustomerType.DEFAULT_CUSTOMER_PA.toString());
        geoCustomer.put(STATE_WI, Customer.CustomerType.DEFAULT_CUSTOMER_WI.toString());
        geoCustomer.put(STATE_MI, Customer.CustomerType.DEFAULT_CUSTOMER_MI.toString());
        geoCustomer.put(STATE_FPO, Customer.CustomerType.FPO_CUSTOMER.toString());


        geoIP.put(STATE_SC, SC_GEO_IP);
        geoIP.put(STATE_IL, IL_GEO_IP);
        geoIP.put(STATE_CA, CA_GEO_IP);
        geoIP.put(STATE_FL, FL_GEO_IP);
        geoIP.put(STATE_NV, NV_GEO_IP);
        geoIP.put(STATE_LA, LA_GEO_IP);
        geoIP.put(STATE_NY, NY_GEO_IP);
        geoIP.put(STATE_VA, VA_GEO_IP);
        geoIP.put(STATE_NC, NC_GEO_IP);
        geoIP.put(STATE_MS, MS_GEO_IP);
        geoIP.put(STATE_CT, CT_GEO_IP);
        geoIP.put(STATE_AK, AK_GEO_IP);
        geoIP.put(STATE_AR, AR_GEO_IP);
        geoIP.put(STATE_CO, CO_GEO_IP);
        geoIP.put(STATE_DE, DE_GEO_IP);
        geoIP.put(STATE_ID, ID_GEO_IP);
        geoIP.put(STATE_IA, IA_GEO_IP);
        geoIP.put(STATE_KS, KS_GEO_IP);
        geoIP.put(STATE_ME, ME_GEO_IP);
        geoIP.put(STATE_MO, MO_GEO_IP);
        geoIP.put(STATE_MT, MT_GEO_IP);
        geoIP.put(STATE_NE, NE_GEO_IP);
        geoIP.put(STATE_NH, NH_GEO_IP);
        geoIP.put(STATE_NM, NM_GEO_IP);
        geoIP.put(STATE_ND, ND_GEO_IP);
        geoIP.put(STATE_OK, OK_GEO_IP);
        geoIP.put(STATE_OR, OR_GEO_IP);
        geoIP.put(STATE_RI, RI_GEO_IP);
        geoIP.put(STATE_SD, SD_GEO_IP);
        geoIP.put(STATE_TN, TN_GEO_IP);
        geoIP.put(STATE_UT, UT_GEO_IP);
        geoIP.put(STATE_VT, VT_GEO_IP);
        geoIP.put(STATE_WV, WV_GEO_IP);
        geoIP.put(STATE_WY, WY_GEO_IP);
        geoIP.put(STATE_GA, GA_GEO_IP);
        geoIP.put(STATE_HI, HI_GEO_IP);
        geoIP.put(STATE_TX, TX_GEO_IP);
        geoIP.put(STATE_OH, OH_GEO_IP);
        geoIP.put(STATE_WA, WA_GEO_IP);
        geoIP.put(STATE_AL, AL_GEO_IP);
        geoIP.put(STATE_IN, IN_GEO_IP);
        geoIP.put(STATE_KY, KY_GEO_IP);
        geoIP.put(STATE_MD, MD_GEO_IP);
        geoIP.put(STATE_MA, MA_GEO_IP);
        geoIP.put(STATE_MN, MN_GEO_IP);
        geoIP.put(STATE_NJ, NJ_GEO_IP);
        geoIP.put(STATE_PA, PA_GEO_IP);
        geoIP.put(STATE_WI, WI_GEO_IP);
        geoIP.put(STATE_MI, MI_GEO_IP);
        geoIP.put(STATE_FPO, FPO_GEO_IP);

    }
    public static final String BUSIEST_DAY_OF_WEEK_MESSAGE = "Our busiest day of the week\nexpect a longer wait for service";
    public static final String HOURS_MINUTES_SECONDS = "HH:mm:ss";

    public static final String EIGHT_AM = "8 AM";
    public static final String ELEVEN_AM = "11 AM";
    public static final String ONE_PM = "1 PM";
    public static final String HYBRIS = "hybris";
    public static final String WEB_METHOD = "Web Method";
    public static final String NWBC = "NWBC";
    public static final String SAP = "SAP";
    public static final String SAP_ECC = "SAP ECC";
    public static final String SAP_WEB_CONTENT = "SAP web content";
    public static final String BACKOFFICE = "Backoffice";
    public static final String TREADWELL_APPLICATION_STATUS = "Treadwell Application Status";

    public static final String FONT_SIZE = "font-size";
    public static final String CHECKED = "checked";

    public static final String PATCH_TESTING = "Patch Testing";
    public static final String PATCH_URL = "portalprd.discounttire.com/portal/server.pt";
    public static final String CAR_HEADER_SERVICE_END_POINT = "/sap/bc/fmcall/Z_ORDER_HISTORY_HDR_GET";
    public static final String CAR_ITEM_SERVICE_END_POINT = "/sap/bc/fmcall/Z_ORDER_HISTORY_ITM_GET";
    public static final String NWBC_URL_RTK = "dctw08v733.trtc.com:8040/sap/bc/ui2/nwbc";
    public static final String NWBC_URL_RTQ = "dctw08v705.trtc.com:8500/sap/bc/ui2/nwbc";
    public static final String NWBC_URL_RT2 = "eccpocci1.trtc.com:8030/nwbc/?sap-client=100&sap-language=EN&sap-nwbc-node=root&sap-theme=sap_corbu";
    public static final String NWBC_URL_RTD = "dctw08v700.trtc.com:8001/sap/bc/ui2/nwbc";
    public static final String NWBC_URL_RK2 = "dctw08v745.trtc.com:8041/sap/bc/ui2/nwbc";
    public static final String NWBC_URL_RQ2 = "dctw08v741.trtc.com:8500/nwbc";
    public static final String SAP_RTL = "dctw08v732.trtc.com:8040/sap/bc/gui/sap/its/webgui";
    public static final String SAP_RTC = "dctw08v706.trtc.com:8010/sap/bc/gui/sap/its/webgui";
    public static final String SAP_RTA = "dctw08v701.trtc.com:8000/sap/bc/gui/sap/its/webgui?sap-client=100";
    public static final String SAP_RTB = "dctw09v716.trtc.com:8020/sap/bc/gui/sap/its/webgui";
    public static final String SAP_CAQ = "carnwcaq.trtc.com:8041/sap/bc/gui/sap/its/webgui";
    public static final String SAP_CAQ_SERVER = "carnwcaq.trtc.com:8041";
    public static final String SAP_CAS_SERVER = "carnwcls1.car.trtc.com:8011";
    public static final String SAP_CAS = "carnwcls1.car.trtc.com:8011/sap/bc/gui/sap/its/webgui";
    public static final String SAP_CAD = "carnwcld1.car.trtc.com:8001/sap/bc/gui/sap/its/webgui";
    public static final String SAP_RTK_URL = "dctw08v733.trtc.com:8040/sap/bc/gui/sap/its/webgui";
    public static final String SAP_RK2_URL = "dctw08v745.trtc.com:8040/sap/bc/gui/sap/its/webgui";
    public static final String SAP_RT2_URL = "eccpocci1.trtc.com:8030/sap/bc/gui/sap/its/webgui?sap-client=100";
    public static final String SAP_RQ2_URL = "dctw08v741.trtc.com:8500/sap/bc/gui/sap/its/webgui";
    public static final String SAP_RD2_URL = "dctw08v736.trtc.com:8000/sap/bc/gui/sap/its/webgui?sap-client=150";
    public static final String SAP_RTQ_URL = "dctw08v705.trtc.com:8500/sap/bc/gui/sap/its/webgui";
    public static final String SAP_RTD_URL = "dctw08v700.trtc.com:8000/sap/bc/gui/sap/its/webgui?sap-client=150";
    public static final String SAP_RTK_WEB_CONTENT_URL = "fithanlq01:8000/textSearch/WebContent/index1.html";
    public static final String SAP_RTQ_WEB_CONTENT_URL = "stg.fitment.trtc.com/textSearch/WebContent/index1.html";

    public static final String BACKOFFICE_QA_URL = "dtqa1.epic.discounttire.com/backoffice/login.zul";
    public static final String BACKOFFICE_STG_URL = "cpstg.discounttire.com/backoffice/login.zul";
    public static final String AUTOINTEGRATE_URL_QA = "https://demo.autointegrate.com";
    public static final String AUTOINTEGRATE_URL_STG = "https://uat.autointegrate.com/";
    public static final String WEB_STAGE_BASE_URL = "https://cpstg.discounttire.com";

    public static final String NWBC_RTK_SITE = "1002";
    public static final String NWBC_RTQ_SITE = "1022";
    public static final String AZ_DC = "1001";
    public static final String TX_DC = "1379";
    public static final String GA_DC = "1287";
    public static final String OH_DC = "1981";

    public static final String FLEET_QA = "http://qa.fleet.trtc.com";
    public static final String FLEET_DEV = "http://dev.fleet.trtc.com";

    public static final String MICROSTRATEGY_QA = "https://bi.qa.discounttire.com/MicrostrategyLDAP ";
    public static final String MICROSTRATEGY_STG = "https://bi.stg.discounttire.com/MicrostrategyLDAP ";

    public static final String GRAPHQL_QA = "http://qa.graphql.fleet.trtc.com";
    public static final String GRAPHQL_STG = "http://stg.graphql.fleet.trtc.com";
    public static final String GRAPHQL_DEV = "http://dev.graphql.trtc.com";


    public static final String FIRST = "First";
    public static final String PREVIOUS = "Previous";
    public static final String LAST = "Last";
    public static final String DISPLAYED = "displayed";
    public static final String RESULTS = "results";

    public static final String NOT_DISPLAYED = "not displayed";

    public static final String MB_WHEELS = "MB WHEELS";
    public static final String AMERICAN_OUTLAW = "AMERICAN OUTLAW";

    public static final String STORE = "store";
    public static final String STORE_DETAILS = "Store details";
    public static final String STORE_CODE = "Store Code";
    public static final String PRODUCT = "product";
    public static final String VALID = "valid";
    public static final String STORE_HOURS = "Store Hours";
    public static final String RANDOM = "random";
    public static final String REQUIRED = "Required";
    public static final String DEPT_OF_TRANS = "DISCOUNT0717";

    public static final String SELECT = "Select";
    public static final String DESELECT = "Deselect";
    public static final String SELECT_ALL = "Select All";
    public static final String ASCENDING = "Ascending";
    public static final String DESCENDING = "Descending";

    public static final String SPLIT_CREDIT_CARD_PAYMENT = "Split credit card payment";
    public static final String SOMEONE_ELSE_WILL_PICK_UP_MY_ORDER = "Someone else will pick up my order";
    public static final String PHONE = "Phone";
    public static final String PHONE_NUMBER = "Phone Number";
    public static final String CUSTOMER_PHONE_NUMBER = "Customer Phone Number";
    public static final String TEXT_UPDATES = "text updates";

    public static final String HALF = "half";
    public static final String NEW = "New";
    public static final String UPDATED = "Updated first";
    public static final String EA = "ea";
    public static final String TO = "To";

    public static final String EXTEND_ASSORT_ORDER_DATA_FILE_IN = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\Functions\\SAP\\Data Tables\\EA\\";
    public static final String EXTEND_ASSORT_EXCEL_FILE_EXTENSION = ".xls";
    public static final String SAP_PURCHASE_ORDER_EXCEL = "SAP_PURCHASE_ORDER_EXCEL";
    public static final String PURCHASE_ORDER_EXCEL = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\Functions\\SAP\\Data Tables\\EA\\ExtendedAssortment.xls";
    public static final String CONSENT_TO_DISCLOSURE = "Consent To Disclosure";
    public static final String I_CONSENT = "I Consent";
    public static final String DECLINE = "Decline";
    public static final String TERMS_OF_AGREEMENT = "Terms Of Agreement";
    public static final String ACCEPT_AMP_SUBMIT = "Accept & Submit";
    public static final String REVIEW_CONSENT_DISCLOSURE_AND_TERMS_OF_AGREEMENT = "Review the Consent Disclosure & Terms of Agreement";

    public static final String AE_VENDOR_STORE_ADDRESS = ", ATLANTA,";

    public static final String CREDIT_CARD_NUMBER = "Credit Card Number";
    public static final String SECURITY_CODE = "Security Code";
    public static final String EXP_DATE = "Exp Date";
    public static final String ZIP_CODE = "Zip Code";
    public static final String NAME_ON_CARD = "Name on Card";

    public static final String ADDRESS = "Address";
    public static final String ARTICLE = "Article";
    public static final String SITE = "Site";
    public static final String ORDERING = "ORDERING";
    public static final String CREATE_DTD_TRANSFER = "Create DTD Transfer";
    public static final String CREATE_STORE_TRANSFER = "Create Store Transfer";
    public static final String CONFIRM_DTD_TRANSFER = "Confirm DTD Transfer";
    public static final String CONFIRM_STO_TRANSFER = "Confirm Store Transfer";
    public static final String CONFIRM_GOODS_MOVEMENT_TRANSFER = "Document posted with the number";
    public static final String GENERAL_ACCESS = "General Access";
    public static final String NWBC_RECEIVING_TAB = "RECEIVING";
    public static final String GOODS_MOVEMENT = "Goods Movement";
    public static final String REPORTS = "Reports";
    public static final String CHANGE_EMPLOYEE_SITE = "Change Employee Site";
    public static final String NWBC_STO_PO_TRANSFER = "Store transfer";
    public static final String NWBC_DTD_PO_TRANSFER = "DTD transfer";
    public static final String NWBC_GOODS_ISSUE_PO_TRANSFER = "Goods Issue transfer";
    public static final String POST_GOODS_ISSUE = "Post Goods Issue";
    public static final String POST_GOODS_RECEIPT = "Post Goods Receipt";

    public static final String LOGON_STATUS_CHECK = "Logon Status Check";
    public static final String FAILED_LOGON_MESSAGE = "Number of failed password logon attempts";
    public static final String CONTINUE = "Continue";

    public static final String WEB_EXCEL_FILE_EXTENSION = ".xls";

    public static final String ORDER_TOTAL = "Order Total";

    public static final String SECONDARY = "secondary";
    public static final String AR_AUTOMATION = "AR Automation";
    public static final String EMPLOYEE_AUTOMATION = "Employee Automation";

    public static final String STANDARD = "Standard";
    public static final String EXTENDED_ASSORTMENT = "Extended Assortment";
    public static final String MIM = "MIM";
    public static final String SAP_ECC_STG = "RTQ";
    public static final String SAP_ECC_QA = "RTK";
    public static final String SAP_CAR_STG = "CAS";
    public static final String SAP_CAR_QA = "CAQ";
    public static final String NWBC_QA = "NWBC RTK";
    public static final String NWBC_RT2 = "NWBC RT2";
    public static final String NWBC_RTD = "NWBC RTD";
    public static final String NWBC_STG = "NWBC RTQ";
    public static final String SAP_BW_QA = "RTL";
    public static final String SAP_CAQ_QA = "CAQ";
    public static final String SAP_RT2 = "RT2";
    public static final String SAP_RTD = "RTD";
    public static final String SAP_ECC_RK2 = "RK2";
    public static final String RQ2 = "RQ2";
    public static final String SAP_POSDT = "SAP POSDT";
    public static final String SAP_POSDM = "SAP POSDM";
    public static final String STYLE = "style";
    public static final String HOMEPAGE = "homepage";
    public static final String RECENT = "recent";
    public static final String XLSX = "XLSX";
    public static final String XLS = "XLS";
    public static final String PNG_EXTENSION = ".png";
    public static final String QA_WEBORDER_EXCEL_COLUMN_LABEL = "QA_WebOrderID";
    public static final String STG_WEBORDER_EXCEL_COLUMN_LABEL = "STG_WebOrderID";
    public static final String TOTAL_AMOUNT_EXCEL_COLUMN_LABEL = "Total Amount";
    public static final String ASSET_ID_IN_RTK = "300001";
    public static final String ASSET_ID_IN_RTQ = "300792";
    public static final String DTD = "dtd";
    public static final String DT = "dt";
    public static final String AT = "at";
    public static final String SEE_PRICE_IN_CART = "price in cart";
    public static final String FLEET = "Fleet";
    public static final String AUTOINTEGRATE = "AutoIntegrate";
    public static final String MICROSTRATEGY = "MicroStrategy";
    public static final String GRAPHQL_ENDPOINT = "/graphql";
    public static final String CUSTOMER_ID = "customer_Id";
    public static final String WEBORDER_DTD_SHEET = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Bubble files\\Functions\\DTD POS\\nexus phase 2\\FT\\WebOrders.xls";
    public static final String DATA_STORAGE_FOLDER = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Bubble files\\Functions\\Selenium Automation Data\\";
    public static final String WEBORDERS_LEGACY_STOREPOS_EXCEL = "WEBORDERS_LEGACY_STOREPOS_EXCEL";
    public static final String WEBORDERS_SAP_RK2_DTD_STOREPOS_EXCEL = "WEBORDERS_SAP_RK2_DTD_STOREPOS_EXCEL";
    public static final String INVOICE_SAP = "INVOICE_SAP";
    public static final String INVOICE_DETAILS = "INVOICE_DETAILS";
    public static final String WEBORDERS_LEGACY_STOREPOS_EXCEL_FILE = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Bubble files\\Functions\\Store POS\\Data Tables\\SAP-" + Config.getDataSet() + "-BOPIS.xls";
    public static final String INVOICE_LEGACY_STOREPOS_EXCEL_FILE = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Bubble files\\Functions\\DTD POS\\nexus phase 2\\FT\\invoices.xlsx";
    public static final String WEBORDERS_SAP_RK2_DTD_STOREPOS_EXCEL_FILE = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Bubble files\\Functions\\Store POS\\Data Tables\\SAP-rk2-DTD.xls";
    public static final String INVOICE_DETAILS_EXCEL_FILE = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Bubble files\\Functions\\DTD POS\\DTDTax\\Tax.xls";
    public static final String PARENT_FOLDER_NAME_SCREENSHOT = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Bubble files\\Functions\\DTD POS\\nexus phase 2\\FT\\screenshots\\";
    public static final String SAP_DATA_SOURCE_LOCATION = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\";
    public static final String SAP_POSDM_PROD_LOCATION = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP POSDM PROD\\Sales TurnOver Types for 1601.xlsx";
    public static final String SAP_USERS_LOCATION = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP USERS\\sap_passwords.xlsx";
    public static final String SAP_PTS_DEBUG_LOCATION = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP PTS DEBUG\\";

    public static HashMap<String, String> fullPathFileNames = new HashMap<>();

    // Add more files here as needed
    static {
        fullPathFileNames.put(WEBORDERS_LEGACY_STOREPOS_EXCEL, WEBORDERS_LEGACY_STOREPOS_EXCEL_FILE);
        fullPathFileNames.put(INVOICE_SAP, INVOICE_LEGACY_STOREPOS_EXCEL_FILE);
        fullPathFileNames.put(INVOICE_DETAILS, INVOICE_DETAILS_EXCEL_FILE);
        fullPathFileNames.put(SAP_PURCHASE_ORDER_EXCEL, PURCHASE_ORDER_EXCEL);
        fullPathFileNames.put(WEBORDERS_SAP_RK2_DTD_STOREPOS_EXCEL, WEBORDERS_SAP_RK2_DTD_STOREPOS_EXCEL_FILE);
    }

    public static final String EXPAND = "expand";
    public static final String COLLAPSE = "collapse";
    public static final String MAXIMUM = "maximum";
    public static final String DISPLAYS = "displays";
    public static final String SUCCESS = "SUCCESS";
    public static final String INCREASED = "increased";
    public static final String SEARCH = "Search";
    public static final String TREADWELL_APPLICATION_STATUS_QA_URL ="https://qa.treadwell.trtc.com/application_status";
    public static final String TREADWELL_APPLICATION_STATUS_STG_URL ="https://stg.treadwell.trtc.com/application_status";
    public static final String LINE_SEPARATOR_SYSTEM_PROPERTY = "line.separator";
    public static final String NONSTAGGERED = "Non Staggered";
    public static final String STAGGERED = "Staggered";
    public static final String GEORGIA = "Georgia";
    public static final String APO_TEST = "APO-TEST";
    public static final String FPO_TEST = "FPO-TEST";
    public static final String CART = "cart";
    public static final String END = "end";
    public static final String SERVICES = "Services";
    public static final String CUSTOMER_SERVICES = "customer services";
    public static final String CUSTOMER_SERVICE = "CustomerService";
    public static final String ORDER_XML_SERVICE = "OrderXmlService";
    public static final String AUTHORIZATION_SERVICE = "AuthorizationService";
    public static final String AVS_SERVICE = "AVSService";
    public static final String MYACCOUNT_CUSTOMER_SERVICE = "MyAccountCustomerService";
    public static final String HAS = "has";
    public static final String MAXIMIZED = "maximized";
    public static final String WINDOWS = "Windows";
    public static final String HIGHLIGHT = "highlight";
    public static final String PLUS_SIGN = "+";
    public static final String STRING_DELIMITER = "-----";
    public static final String DELETE = "Delete";
    public static final String TEST_STATUS = "TEST STATUS";
    public static final String SPINNER_PRELOADER_DISPLAY_JS = "return document.getElementById('preloaderSpinner')." +
            "getAttribute('style').indexOf('none')>-1";
    public static final String SPINNER_NOT_DISPLAYED_JS = "return document.getElementById('spinner')===null";
    public static final String SPINNER_DISPLAYED_JS = "return document.getElementById('spinner')!==null";
    public static final String READ_ONLY = "readonly";

    public static final String GRANT_TYPE_KEY = "grant_type";
    public static final String CLIENT_CREDENTIALS = "client_credentials";
    public static final String SCOPE_KEY = "scope";
    public static final String BASIC = "basic";
    public static final String CLIENT_ID_KEY = "client_id";
    public static final String QUALITY_ANALYSIS = "quality_analysis";
    public static final String CLIENT_SECRET_KEY = "client_secret";
    public static final String SECRET = "secret";
    public static final String CONTENT_TYPE_VAL = "application/x-www-form-urlencoded";
    public static final String CHARSET = "UTF-8";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String ACCESS_TOKEN_VAL = "access_token_value";
    public static final String XML_PARAM_BODY = "List.item.price";
    public static final String UPDATED_PRICE_VAL = "updated_price_value";
    public static final String ORDER_TYPE = "order_type";
    public static final String CUSTOMER_TYPE = "customer_type";
    public static final String SHIPPING_METHOD = "shipping_method";
    public static final String PAST = "past";
    public static final String PRESENT = "present";
    public static final String FUTURE = "future";

    public static final String WEB_ORDER = "Web order";
    public static final String BEFORE = "before";
    public static final String AFTER = "after";
    public static final String DETAILS = "Details";
    public static final String VERSION_NUMBER = "versionNumber";
    public static final String BUILD = "Build";
    public static final String BY_ID = "by.id";

    public static final String SITE_ID_DT = "discounttire";
    public static final String SITE_ID_DTD = "discounttiredirect";
    public static final String QUANTITY_ORDER_SERVICE_KEY = "Quantity";
    public static final String CODE_ORDER_SERVICE_KEY = "Code";
    public static final String VALUE_ORDER_SERVICE_KEY = "value";
    public static final String SITE_ORDER_SERVICE_KEY = "site";
    public static final String STORE_NUMBER_ORDER_SERVICE_KEY = "storeNumber";
    public static final String TOTAL_PRICE_ORDER_SERVICE_KEY = "totalPriceWithTax";
    public static final String SHIPPING_AMOUNT_SERVICE_KEY = "shippingAmount";
    public static final String FET = "fet";

    public static final String JSON_PARAM_VALUE = "value";
    public static final String JSON_PARAM_UNCONSIGNED = "unconsignedEntries";
    public static final String JSON_PARAM_PRODUCT = "product";
    public static final String JSON_PARAM_TOTAL_PRICE = "totalPrice";
    public static final String JSON_PARAM_TOTAL_CODE = "code";
    public static final String JSON_PARAM_QUANTITY = "quantity";
    public static final String JSON_PARAM_TOTAL_PRICE_WITH_TAX = "totalPriceWithTax";
    public static final String JSON_PARAM_DELIVERY_COST = "deliveryCost";
    public static final String JSON_PARAM_DISCOUNTS = "totalDiscounts";
    public static final String JSON_PARAM_DELIVERY_POINT_OF_SERVICE = "deliveryPointOfService";
    public static final String JSON_PARAM_SITE = "site";
    public static final String ALTERNATE_CONTACT = "Alternate Contact";
    public static final String ALTERNATE = "Alternate";
    public static final String JSON_PARAM_NAME = "name";
    public static final String JSON_PARAM_ENTRIES = "entries";
    public static final String JSON_PARAM_PICKUP_ORDER_GROUPS = "pickupOrderGroups";

    public static final String COSTCO = "C000000011";
    public static final String WALMART = "C000000080";

    public static final String ET_ORDER_ITEM = "ET_ORDER_ITEM";
    public static final String RETAILLINEITEM = "RETAILLINEITEM";
    public static final String ITEMID = "ITEMID";
    public static final String RETAILQUANTITY = "RETAILQUANTITY";
    public static final String CUSTOMER_ID_CAR = "CUSTOMER_ID";
    public static final String TRANSSTATUS = "TRANSSTATUS";
    public static final String APPTID = "APPTID";
    public static final String APPOINTMENTDT = "APPOINTMENTDT";
    public static final String APTSTRTIME = "APTSTRTIME";
    public static final String TOTALAMT = "TOTALAMT";
    public static final String TRANSTYPE = "TRANSTYPE";
    public static final String APPTSTATUS = "APPTSTATUS";
    public static final String ORDER_CHANNEL = "ORDER_CHANNEL";
    public static final String ET_ORDER_HEADER = "ET_ORDER_HEADER";
    public static final String TRANSNUMBER = "TRANSNUMBER";
    public static final String FIELDVALUE = "FIELDVALUE";
    public static final String FIELDNAME = "FIELDNAME";
    public static final String HYBORDRNUM = "HYBORDRNUM";
    public static final String HEADERITEMID = "HEADERITEMID";
    public static final String HEADERITEM = "HEADERITEM";
    public static final String HEADERRETAILQUANTITY = "HEADERRETAILQUANTITY";
    public static final String HEADERTRANSSTATUS = "HEADERTRANSSTATUS";
    public static final String HEADERAPPTID = "HEADERAPPTID";
    public static final String HEADERAPPOINTMENTDT = "HEADERAPPOINTMENTDT";
    public static final String HEADERAPTSTRTIME = "HEADERAPTSTRTIME";
    public static final String HEADERTOTALAMT = "HEADERTOTALAMT";
    public static final String HEADERTRANSTYPE = "HEADERTRANSTYPE";
    public static final String HEADERAPPTSTATUS = "HEADERAPPTSTATUS";
    public static final String HEADERORDER_CHANNEL = "HEADERORDER_CHANNEL";
    public static final String CUSTOMER_TYPE_CAR = "CUSTOMER_TYPE";
    public static final String APPTDATE = "APPTDATE";
    public static final String VEHICLEID = "VEHICLEID";
    public static final String VEHTRIMID = "VEHTRIMID";
    public static final String ASEMBLYLTR = "ASEMBLYLTR";
    public static final String TYPE = "type";
    public static final String Billing = "Billing";
    public static final String SETTLE = "settle";
    public static final String FROM = "from";

    public static final String SINGLE = "single";
    public static final String REPAIR_NUMBER = "RepairNumber";
    public static final String VEHICLE = "Vehicle";
    public static final String LOGGER = "LOGGER";
    public static final String OAUTH_TOKEN = "oauth/token";
    public static final String CREATE = "Create";
    public static final String PRODUCT_ARTICLE_NUMBER_LABEL = "Item #";
    public static final String AUTHORIZATION_SERVER_LINK = "/authorizationserver/";
    public static final String ON = "on";
    public static Map<String, String> month = new HashMap<String, String>();

    static {
        month.put("Jan", "01");
        month.put("Feb", "02");
        month.put("Mar", "03");
        month.put("Apr", "04");
        month.put("May", "05");
        month.put("Jun", "06");
        month.put("Jul", "07");
        month.put("Aug", "08");
        month.put("Sep", "09");
        month.put("Oct", "10");
        month.put("Nov", "11");
        month.put("Dec", "12");
    }

    public static final String INSTALLATION_PRODUCT_CODE = "80219";
    public static final String ENVIRONMENTAL_FEE_PRODUCT_CODE = "80075";
    public static final String DISPOSAL_FEE_PRODUCT_CODE = "80224";
    public static final String CERTIFICATES_PRODUCT_CODE = "80017";
    public static final String HUB_FEE_PRODUCT_CODE = "81919";
    public static final String EDIT = "Edit";
    public static final String ITEMS = "Items";
}