package dtc.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 4/27/17.
 */
public class MyVehiclesPopupPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(MyVehiclesPopupPage.class.getName());

    public MyVehiclesPopupPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    private static final String VEHICLE_LIMIT_MESSAGE = "Vehicle limit reached (5)Remove vehicles before adding more";
    private static final String NO_RECENT_VEHICLES = "no recent vehicles";
    private static final String MY_VEHICLES_IMAGE = "my vehicles image";
    private static final String FITMENT_WHEELS_TAB = "fitment wheels tab";
    private static final String FITMENT_TIRES_TAB = "fitment tires tab";
    private static final String VEHICLE_ICON = "Vehicle icon";
    private static final String RECENTLY_SEARCHED_VEHICLES = "recently searched vehicles";
    private static final String CLEAR_RECENT_SEARCHES = "clear recent searches";
    private static final String SELECTED_VEHICLES = "selected vehicle";
    private static final String RECENT_SEARCHES = "recent searches";
    public static final String CONTACT_INFO_EDIT_LINK = "Contact Info Edit Link";
    private static final String SAVED_VEHICLES = "saved vehicles";

    @FindBy(className = "popover--my-vehicles")
    public static WebElement myVehiclePopup;

    @FindBy(className = "mobile-menu__vehicles")
    public static WebElement myVehiclePopupMobile;

    @FindBy(className = "my-vehicles__controls-message")
    public static WebElement vehicleControlMessage;

    @FindBy(css = ".dt-button.dt-button--secondary")
    public static WebElement currentVehicle;

    @FindBy(css = "[class*='my-vehicles__my-vehicles--modal___']")
    public static WebElement searchedVehicleList;

    @FindBy(css = "[class*='vehicle-detail__vehicle-detail--selected']")
    public static WebElement selectedVehicleSection;

    private static final By recentVehicleBy = By.className("my-vehicles__recent-vehicles-button");

    private static final By myVehiclesNoRecentSearchesBy =
            By.xpath("//span[contains(text(),'You have no recent vehicle searches')]");

    private static final By recentSearchesBy =
            By.cssSelector("[class*='my-vehicles__my-vehicles--modal___'] div:nth-child(2) div:nth-child(1) > h2");

    private static final By myVehicleListMessageBy = By.cssSelector("[class*='index__message___']>span");

    private static final By myVehiclesSelectedVehicleIconBy =
            By.cssSelector("div[class*='vehicle-detail__selected-message'] [class*='fa fa-car']");

    private static final By myVehiclesSelectedMessageBy =
            By.cssSelector("div[class*='vehicle-detail__selected-message']");

    private static final By myVehiclesModalClearRecentSearchesBy =
            By.cssSelector("span[class*='my-vehicles-actions__clear']");

    private static final By myVehiclesModalCancelYourTireAndWheelPackageBy =
            By.cssSelector("span[class*='cancel-package-modal__cancel-package-modal-fill']");

    private static final By treadwellVehiclePopupBy = By.cssSelector("[class*='recent-vehicles-add']");

    private static final By selectedTabBy = By.cssSelector("[class*='dt-tabs__tab--selected']");

    private static final By alternateContactHeaderBy = By.xpath("//p[contains(text(),'alternate')]");

    private static final By savedVehicleListBy = By.cssSelector("[class*='vehicles-list__vehicle-list--saved'] " +
            "[class*='vehicle-detail__content']");

    private static final By vehicleListBy = By.cssSelector("[class*='vehicle-detail__content']");

    private static final By vehicleDetailActionsBy = By.cssSelector("[class*='vehicle-detail__details'] " +
            "[class*='vehicle-detail__action'] [class*='dt-link']");

    /**
     * Clicks specified recent vehicle
     *
     * @param vehicle The vehicle to click
     */
    public void clickRecentVehicle(String vehicle) {
        LOGGER.info("clickRecentVehicle started");
        driver.waitForElementVisible(recentVehicleBy);
        driver.getElementWithText(recentVehicleBy, vehicle).click();
        LOGGER.info("clickRecentVehicle completed");
    }

    /**
     * Verifies the current vehicle is the selected vehicle
     *
     * @param vehicle The vehicle to check
     */
    public void assertCurrentVehicleIsSelected(String vehicle) {
        LOGGER.info("assertCurrentVehicleIsSelected started");
        assertRecentVehicleDisplay(vehicle, true);
        Assert.assertEquals("FAIL: The '" + vehicle + "' was not the selected vehicle",
                commonActions.returnMyVehiclesRowWithVehicle(vehicle).getText().toLowerCase(),
                commonActions.returnMyVehiclesRowWithVehicle(CommonActions.SELECTED_VEHICLE).getText().toLowerCase());
        LOGGER.info("assertCurrentVehicleIsSelected completed. '" + vehicle + "' was listed as selected vehicle.");
    }

    /**
     * Verifies the current vehicle
     */
    public void assertVehicleLimitMessage() {
        LOGGER.info("assertVehicleLimitMessage started");
        driver.waitForElementVisible(vehicleControlMessage);
        String vehicleMessage = vehicleControlMessage.getText().replaceAll("\n", "");

        Assert.assertTrue("FAIL: Vehicle limit message was not displayed, but instead contained \"" +
                vehicleMessage + "\".", vehicleMessage.contains(VEHICLE_LIMIT_MESSAGE));

        LOGGER.info("Confirmed that the vehicle limit message was displayed.");
        LOGGER.info("assertVehicleLimitMessage completed");
    }

    /**
     * Verify Add vehicle link displayed for my vehicle popup and treadwell modal popup
     *
     * @param option treadwell and default
     */
    public void assertAddVehicle(String option) {
        LOGGER.info("assertAddVehicle started");
        driver.waitForPageToLoad();
        WebElement addVehicle;
        if (option.equalsIgnoreCase(ConstantsDtc.TREADWELL)) {
            addVehicle = webDriver.findElement(treadwellVehiclePopupBy);
            Assert.assertTrue("FAIL: Add Vehicle not displayed in My Vehicle Popup", driver.isElementDisplayed(addVehicle));
            Assert.assertTrue("FAIL: Treadwell modal popup header was not displayed", driver.isElementDisplayed(CommonActions.dtModalHeaderBy));
        } else {
            addVehicle = driver.getElementWithText(CommonActions.dtButtonLgBy, CommonActions.ADD_VEHICLE_BUTTON_TEXT);
            driver.jsScrollToElement(addVehicle);
            Assert.assertTrue("FAIL: Add Vehicle not displayed in My Vehicle Popup", driver.isElementDisplayed(addVehicle));
        }
        LOGGER.info("assertAddVehicle completed");
    }

    /**
     * Click "Add Vehicle" in the "My Vehicles" popup
     */
    public void clickAddVehicle() {
        LOGGER.info("clickAddVehicle started");
        WebElement addVehicleElement;
        driver.waitForPageToLoad();
        if (Config.isMobile()) {
            driver.jsScrollToElementClick(myVehiclePopupMobile);
        }
        addVehicleElement = driver.getElementWithText(CommonActions.dtButtonLgBy,
                CommonActions.ADD_VEHICLE_BUTTON_TEXT, Constants.FIVE);
        Assert.assertNotNull("FAIL: Unable to find the 'ADD VEHICLE' button!", addVehicleElement);
        driver.webElementClick(addVehicleElement);
        commonActions.waitForSpinner();
        //TODO: Needed for IE popup window due to on screen movement that cannot be manually replicated
        if (Config.isIe())
            driver.waitForMilliseconds(Constants.THREE);
        LOGGER.info("clickAddVehicle completed");
    }

    /**
     * Clicks current vehicle on My Vehicle popup
     */
    public void clickCurrentVehicle() {
        LOGGER.info("clickCurrentVehicle started");
        driver.waitForPageToLoad();
        driver.webElementClick(currentVehicle);
        LOGGER.info("clickCurrentVehicle completed");
    }

    /**
     * Verifies the display of current vehicle in My Vehicles section
     */
    public void assertMyVehicles(String vehicle) {
        LOGGER.info("assertMyVehicles started");
        driver.waitForPageToLoad();
        WebElement myVehicles = driver.getElementWithText(CommonActions.myVehiclesHeaderBy, vehicle);
        Assert.assertTrue("FAIL: Vehicle not displayed in My Vehicle header", driver.isElementDisplayed(myVehicles));
        LOGGER.info("assertMyVehicles completed");
    }

    /**
     * Verify that specified vehicle listed in recent vehicle
     *
     * @param vehicle     The vehicle to verify in recent vehicles
     * @param isDisplayed displayed or not displayed
     */
    public void assertRecentVehicleDisplay(String vehicle, boolean isDisplayed) {
        LOGGER.info("assertRecentVehicleDisplay started for " + vehicle);
        driver.waitForPageToLoad();
        if (isDisplayed)
            Assert.assertTrue("FAIL: Vehicle not displayed in recent vehicles",
                    commonActions.returnMyVehiclesRowWithVehicle(vehicle) != null);
        else
            Assert.assertTrue("FAIL: Vehicle displayed in recent vehicles",
                    commonActions.returnMyVehiclesRowWithVehicle(vehicle) == null);
        LOGGER.info("assertRecentVehicleDisplay completed for " + vehicle);
    }

    /**
     * Verifies the Add Vehicle button is clickable and red
     */
    public void verifyAddVehicleButtonProperties() {
        LOGGER.info("verifyAddVechicleButtonProperties started ");
        String color;
        WebElement addVehicle;
        if (Config.isMobile()) {
            addVehicle = webDriver.findElement(CommonActions.dtButtonLgBy);
        } else {
            addVehicle = driver.getElementWithText(CommonActions.dtButtonLgBy, CommonActions.ADD_VEHICLE_BUTTON_TEXT);
        }
        driver.waitForElementClickable(addVehicle);
        color = addVehicle.getCssValue(Constants.BACKGROUND_COLOR);
        Assert.assertTrue("FAIL: The 'Add To Cart' button color was not Red (rgb: '"
                + Constants.RED_COLOR_RGB + "' OR rgba: '" + Constants.RED_COLOR_RGBA
                + "') but was actually " + color + "!", color.equals(Constants.RED_COLOR_RGB) ||
                color.equals(Constants.RED_COLOR_RGBA));
        LOGGER.info("verifyAddvehicleButtonProperties completed");

    }

    /**
     * Verifies Vehicle Details displayed In MyVehicles Modal for wheels & tires on regular vehicle
     *
     * @param vehicle   Non-staggered
     * @param type      Original Equipment/Non OE with Tire/Wheel
     * @param sizeBadge O.E/for Non O.E the difference of size
     * @param text      Vehicle description/Edit Vehicle link text inside the modal to verify
     */
    public void assertMyVehiclesModalVehicleDetails(String vehicle, String type, String sizeBadge, String text) {
        LOGGER.info("assertMyVehiclesModalVehicleDetails started on " + " for " + vehicle + " vehicle on " +
                "My Vehicles page verifying " + text + " text");
        String fitmentBanner;
        String fitmentBannerLabel;
        if (type.contains(Constants.TIRE)) {
            fitmentBanner = webDriver.findElement(CommonActions.fitmentBannerTireBy).getText();
            if (type.contains(ConstantsDtc.NON_OE)) {
                fitmentBannerLabel = ConstantsDtc.OPTIONAL_TIRE_SIZE;
            } else {
                fitmentBannerLabel = ConstantsDtc.FACTORY_TIRE_SIZE;
            }
        } else {
            fitmentBanner = webDriver.findElement(CommonActions.fitmentBannerWheelBy).getText();
            fitmentBannerLabel = Constants.WHEEL;
        }
        String fitmentBadge = webDriver.findElement(CommonActions.fitmentSizeBadgeBy).getText();
        Assert.assertTrue("FAIL: The vehicle details banner on " + " for " + type +
                " contains text: " + fitmentBanner + ", " + fitmentBadge + " does not match to expected text: "
                + text + ", " + fitmentBannerLabel + ", " + sizeBadge, fitmentBanner.contains(text)
                && fitmentBanner.toLowerCase().contains(fitmentBannerLabel.toLowerCase()) && fitmentBadge.contains(sizeBadge));
        LOGGER.info("assertMyVehiclesModalVehicleDetails completed on " + " for " + vehicle + " vehicle on " +
                "My Vehicles page verifying " + text + " text");
    }

    /**
     * Clicks on clear recent searches
     *
     * @param option true or false whether to clear the searches
     */
    public void clickClearRecentSearches(String option) {
        LOGGER.info("clickClearRecentSearches started for option: " + option);
        if (driver.isElementDisplayed(myVehiclesModalClearRecentSearchesBy, Constants.ZERO)) {
            driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
            WebElement clearRecentSearch = webDriver.findElement(myVehiclesModalClearRecentSearchesBy);
            driver.resetImplicitWaitToDefault();
            driver.jsScrollToElementClick(clearRecentSearch, false);
            driver.waitSeconds(Constants.THREE);
            if (option.equalsIgnoreCase(ConstantsDtc.YES_CLEAR_RECENT_SEARCHES) ||
                    option.equalsIgnoreCase(ConstantsDtc.NO_KEEP_THESE_SEARCHES)) {
                commonActions.clickButtonByText(option, Constants.TEN);
                return;
            }
        } else {
            LOGGER.info("My vehicles clear recent searches popup modal is not displayed");
            if (option.equalsIgnoreCase(ConstantsDtc.YES_CLEAR_RECENT_SEARCHES)) {
                LOGGER.info("clickClearRecentSearches completed for option: " + option);
                return;
            }
        }
        try {
            driver.clickElementWithLinkText(option, false);
        } catch (Exception e) {
            commonActions.clickButtonByText(option);
        }
        LOGGER.info("clickClearRecentSearches completed for option: " + option);
    }

    /**
     * This method will return the By element matching the input String.
     *
     * @param elementName - Name of the WebElement
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started for " + elementName);
        switch (elementName) {
            case NO_RECENT_VEHICLES:
                return webDriver.findElement(myVehiclesNoRecentSearchesBy);
            case MY_VEHICLES_IMAGE:
                return webDriver.findElement(CommonActions.vehicleDisplayImageBy);
            case FITMENT_WHEELS_TAB:
                return driver.getElementWithText(selectedTabBy, ConstantsDtc.WHEELS);
            case FITMENT_TIRES_TAB:
                return driver.getElementWithText(selectedTabBy, ConstantsDtc.TIRES);
            case VEHICLE_ICON:
                return webDriver.findElement(myVehiclesSelectedVehicleIconBy);
            case RECENTLY_SEARCHED_VEHICLES:
                return driver.getElementWithText(myVehicleListMessageBy,
                        ConstantsDtc.RECENTLY_SEARCHED_VEHICLES_MESSAGE);
            case CLEAR_RECENT_SEARCHES:
                return driver.getElementWithText(myVehiclesModalClearRecentSearchesBy,
                        ConstantsDtc.CLEAR_RECENT_SEARCHES);
            case ConstantsDtc.MY_VEHICLES:
                return driver.getElementWithText(CommonActions.dtModalTitleBy, ConstantsDtc.MY_VEHICLES);
            case SELECTED_VEHICLES:
                return webDriver.findElement(myVehiclesSelectedMessageBy);
            case RECENT_SEARCHES:
                return driver.getElementWithText(recentSearchesBy, ConstantsDtc.RECENT_SEARCHES);
            case ConstantsDtc.JOIN_SIGN_IN:
                if (Config.isMobile())
                    return webDriver.findElement(MyAccountPage.signInIconBy);
                else
                    return driver.getElementWithText(MyAccountPage.getSignInBy, ConstantsDtc.JOIN_SIGN_IN);
            case SAVED_VEHICLES:
                return driver.getElementWithText(recentSearchesBy, ConstantsDtc.SAVED_VEHICLES);
            case Constants.ALTERNATE_CONTACT:
                return webDriver.findElement(alternateContactHeaderBy);
            case CONTACT_INFO_EDIT_LINK:
                return webDriver.findElement(MyAccountPage.contactInfoBy).findElement(CommonActions.dtLinkBy);
            case ConstantsDtc.SEARCH_BAR:
                return webDriver.findElement(CommonActions.searchBarBy);
            case FitmentPage.BACK_TO_MY_VEHICLES_BUTTON_TEXT:
                return webDriver.findElement(FitmentPopupPage.backToMyVehiclesBy);
            default:
                Assert.fail("FAIL:Could not find the matched string passed from step");
                return null;
        }
    }

    /**
     * Verifies the webElement is displayed
     *
     * @param elementName Name of the specified element
     */
    public void verifyIsElementDisplayed(String elementName) {
        LOGGER.info("verifyIsElementDisplayed started for " + elementName);
        WebElement targetElement = returnElement(elementName);
        driver.waitForElementVisible(targetElement);
        Assert.assertTrue("FAIL: The Element " + targetElement + " is not displayed",
                driver.isElementDisplayed(targetElement));
        LOGGER.info("verifyIsElementDisplayed completed for " + elementName);
    }

    /**
     * Verify that specified vehicle listed in recent or saved display
     *
     * @param vehicle     The vehicle to verify in recent or saved display
     * @param isDisplayed displayed or not displayed
     */
    public void assertRecentOrSavedVehicleDisplay(String vehicle, boolean isDisplayed, String savedOrRecent) {
        LOGGER.info("assertRecentOrSavedVehicleDisplay started for " + vehicle + " in section " + savedOrRecent);
        driver.waitForPageToLoad();
        if (isDisplayed)
            Assert.assertTrue("FAIL: Vehicle not displayed in recent vehicles",
                    commonActions.returnMyVehiclesRowsListByChoice(vehicle, savedOrRecent) != null);
        else
            Assert.assertTrue("FAIL: Vehicle displayed in recent vehicles",
                    commonActions.returnMyVehiclesRowsListByChoice(vehicle, savedOrRecent) == null);
        LOGGER.info("assertRecentOrSavedVehicleDisplay completed for " + vehicle + " in section " + savedOrRecent);
    }

    /**
     * Verify the vehicle position in saved vehicles list
     *
     * @param year      year
     * @param make      make
     * @param model     model
     * @param trim      trim
     * @param isPresent is present or absent
     * @param position  The position of the vehicle
     */
    public void verifyVehiclePresentAtPosition(String year, String make, String model, String trim,
                                               String isPresent, String position) {
        LOGGER.info("verifyVehiclePresentAtPosition started");
        driver.waitForElementVisible(webDriver.findElement(savedVehicleListBy));
        int vehiclePoistion = Integer.parseInt(position);
        ArrayList<String> vehicleValuesList = new ArrayList<>(Arrays.asList(year, make, model, trim));
        List<WebElement> savedVehiclesList = webDriver.findElements(savedVehicleListBy);
        String savedVehicleText = savedVehiclesList.get(vehiclePoistion - 1).getText();

        if (isPresent.equalsIgnoreCase(Constants.PRESENT)) {
            for (int i = 0; i < vehicleValuesList.size() - 1; i++) {
                Assert.assertTrue("FAIL: The vehicle " + year + " " + make + " " + model + " " + trim + " does not exists",
                        savedVehicleText.toLowerCase().contains(vehicleValuesList.get(i).toLowerCase()));
            }
        } else {
            for (int i = 0; i < vehicleValuesList.size() - 1; i++) {
                Assert.assertFalse("FAIL: The vehicle " + year + " " + make + " " + model + " " + trim + " does not exists",
                        savedVehicleText.toLowerCase().contains(vehicleValuesList.get(i).toLowerCase()));
            }
        }
        LOGGER.info("verifyVehiclePresentAtPosition completed");
    }

    /**
     * Clicks on Shop tires or shop wheels for the specified vehicle
     *
     * @param option shop tires or shop wheels
     * @param year   The year value to select
     * @param make   The make value to select
     */
    public void selectShopTiresShopWheels(String option, String year, String make) {
        LOGGER.info("selectShopTiresShopWheels started");
        List<WebElement> vehiclesList = webDriver.findElements(vehicleListBy);
        for (WebElement vehicles : vehiclesList) {
            if (vehicles.getText().contains(year)) {
                if (vehicles.getText().toLowerCase().contains(make.toLowerCase())) {
                    WebElement vehicle = vehicles.findElement(vehicleDetailActionsBy);
                    WebElement vehicleOption = driver.getElementWithText(vehicle, option);
                    driver.jsScrollToElementClick(vehicleOption);
                    break;
                }
            }
        }
        LOGGER.info("selectShopTiresShopWheels completed");
    }
}