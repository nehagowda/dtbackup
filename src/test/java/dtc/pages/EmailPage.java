package dtc.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.steps.CommonActionsSteps;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.logging.Logger;

/**
 * @author Mukul Garg on 8/29/2017
 */
public class EmailPage {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(EmailPage.class.getName());
    private String currentEmailInbox;
    private Customer customer;
    private CommonActions commonActions;

    public EmailPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
        customer = new Customer();
        commonActions = new CommonActions(driver);
    }

    public static final String EMAIL_SUFFIX = "@discounttire.com";
    private static final String MAIN_EMAIL_FRAME = "s_MainFrame";
    private static final int MAX_TRIALS = 20;

    public static final By orderDetailsBy = By.cssSelector(".TimesNewRomanFIX.PaddingVerticalReset");

    public static final By detailsBy = By.className("TimesNewRomanFIX");

    public static final By emailSubjectsBy = By.cssSelector("._lvv_w");

    public static final By workLoadItemsBy = By.className("workload-item__text--fluent");

    @FindBy(className = "form-control")
    public static WebElement inboxField;

    @FindBy(css = "input[type='submit']")
    public static WebElement signInInboxBtn;

    @FindBy(id = "e-listview-container-mail-row-1")
    public static WebElement firstEmail;

    @FindBy(css = "[aria-label='Activate Search Textbox']")
    public static WebElement searchBox;

    @FindBy(css = "input[class*='textbox']")
    public static WebElement textBox;

    @FindBy(css = "._is_w.o365button")
    public static WebElement search;

    @FindBy(className = "inline-block")
    public static WebElement nextBtn;

    @FindBy(className = "flexcolumn")
    public static WebElement searchResultsArea;

    /**
     * Setting currentEmailInbox object value
     *
     * @param currentEmailInbox opened Inbox
     */
    public void setCurrentEmailInbox(String currentEmailInbox) {
        this.currentEmailInbox = currentEmailInbox;
    }

    /***
     * Goes to Microsoft outlook and opens the specified inbox. Note: emailInbox arg can either be the full email address
     * (dtc.test@discounttire.com) OR the portion before the @ symbol (dtc.test)
     * @param emailUsername email username
     * @param emailPassword email password
     */
    public void openInbox(String emailUsername, String emailPassword) {
        LOGGER.info("openInbox started for inbox: \"" + emailUsername + "\\" + emailPassword + "");
        driver.getUrl(CommonActions.EMAIL_URL);
        driver.waitForPageToLoad();
        driver.waitForElementClickable(inboxField);

        if (!emailUsername.contains(EMAIL_SUFFIX) && !emailUsername.contains(EMAIL_SUFFIX.toUpperCase())) {
            emailUsername = emailUsername + EMAIL_SUFFIX;
        }

        inboxField.sendKeys(emailUsername);
        nextBtn.click();
        driver.waitForMilliseconds();
        inboxField.sendKeys(emailPassword);
        signInInboxBtn.click();
        driver.waitForMilliseconds();
        signInInboxBtn.click();
        driver.waitForMilliseconds();
        driver.getElementWithText(workLoadItemsBy, ConstantsDtc.OUTLOOK).click();
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        setCurrentEmailInbox(emailUsername);
        LOGGER.info("openInbox completed for inbox: \"" + emailUsername + "\"");
    }

    /**
     * Gets the dynamically generated frame ID of the email body
     *
     * @return String frameId of the email body
     */
    private String getEmailBodyFrame() {
        LOGGER.info("getEmailBodyFrame started");
        String javascript = "frame = document.getElementsByClassName('s-form-bodyiframe s-basicpanel s-panel-border')[0]; " +
                "return frame.getAttribute('id');";
        String frameId;

        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        frameId = (String) jse.executeScript(javascript);

        LOGGER.info("getEmailBodyFrame completed, got frame ID \"" + frameId + "\"");
        return frameId;
    }

    /**
     * Verify the Store Details in Order|Appointment confirmation Email
     *
     * @param emailType Order|Appointment Confirmation Email
     */
    public void verifyStore(String emailType) {
        LOGGER.info("verifyStore started for " + emailType);
        Actions action = new Actions(webDriver);
        action.doubleClick(firstEmail).perform();

        driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        String emailBodyFrame = getEmailBodyFrame();
        webDriver.switchTo().frame(emailBodyFrame);

        String expectedOrderNumber = driver.scenarioData.getCurrentOrderNumber();
        String expectedStore = CommonActionsSteps.storeName.split("-")[0].trim();
        String displayedStore = webDriver.findElement(orderDetailsBy).getText();
        Assert.assertTrue("FAIL: The STORE DETAILS not correct, displayed: '" + displayedStore
                        + "' does not contain expected store: '" + expectedStore + "'.",
                displayedStore.toLowerCase().contains(expectedStore.toLowerCase()));
        LOGGER.info("verifyStore completed for " + emailType);
    }

    /**
     * Verify Customer Name in Order|Appointment confirmation Email
     *
     * @param customerType Customer type to grab dtc.data from
     * @param email        Order Confirmation Email or Appointment Confirmation Email
     */
    public void verifyCustomerName(String customerType, String email) {
        LOGGER.info("verifyCustomerName started for " + email);
        driver.waitForPageToLoad();
        String myInfo = webDriver.findElements(orderDetailsBy).get(1).getText();
        dtc.data.Customer orderEmailCustomer = customer.getCustomer(customerType);
        String expectedCustomer = orderEmailCustomer.firstName + " " + orderEmailCustomer.lastName;
        Assert.assertTrue("FAIL: Customer name in MY INFORMATION is not correct, displayed: '" + myInfo
                + "' does not contain expected name: '" + expectedCustomer + "'.", myInfo.contains(expectedCustomer));
        LOGGER.info("verifyCustomerName completed for " + email);
    }

    /**
     * Verify expected product code displayed in Order|Appointment confirmation Email
     *
     * @param itemCode product code
     * @param email    Order|Appointment Confirmation Email
     */
    public void verifyProductCode(String itemCode, String email) {
        LOGGER.info("verifyProductCode started for item " + itemCode + " in " + email);
        WebElement product;
        if (email.equalsIgnoreCase(ConstantsDtc.ORDER_CONFIRMATION)) {
            product = driver.getElementWithText(CommonActions.spanTagNameBy, itemCode);
        } else {
            product = driver.getElementWithText(detailsBy, itemCode);
        }
        Assert.assertTrue("FAIL: Item Code does not match, displayed: '" + product.getText()
                + "' does not contain expected item: '" + itemCode + "'.", product.getText().contains(itemCode));
        LOGGER.info("verifyProductCode completed for item " + itemCode + " in " + email);
    }

    /**
     * Search email in the outlook inbox contains the order number
     */
    public void assertOrderEmailInInbox() {
        LOGGER.info("assertOrderEmailInInbox started for inbox: '" + currentEmailInbox + "'");
        int currentTrial = 1;
        int waitInterval = Constants.FIVE;
        Actions action = new Actions(webDriver);
        String expectedOrderNumber = driver.scenarioData.getCurrentOrderNumber();
        driver.waitForMilliseconds();
        boolean success = false;
        do {
            try {
                searchBox.click();
                success = true;
            } catch (Exception e) {
                // In case incoming email balloon blocks the search box in mobile test.
                searchResultsArea.click();
                driver.waitSeconds(Constants.FIVE);
            }
        } while (!success);
        driver.waitForMilliseconds();
        success = false;
        do {
            try {
                searchBox.click();
                commonActions.clearAndPopulateEditField(textBox, expectedOrderNumber);
                textBox.sendKeys(Keys.ENTER);
                success = true;
            } catch (Exception e) {
                // In case incoming email balloon blocks the text box in mobile test.
                searchResultsArea.click();
                driver.waitSeconds(Constants.FIVE);
            }
        } while (!success);

        success = false;
        driver.waitForMilliseconds();
        do {
            if (driver.isElementDisplayed(emailSubjectsBy))
                success = true;
            else {
                if (Config.isChrome())
                    action.sendKeys(Keys.F9).perform();
                try {
                    search.click();
                    driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
                    LOGGER.info("Clicking Search button on email inbox: Iteration " + currentTrial);
                } catch (Exception e) {
                    // In case incoming email balloon blocks the search button in mobile test.
                    searchResultsArea.click();
                    currentTrial--;
                }
                currentTrial++;
                driver.waitSeconds(waitInterval);
            }
        } while (!success && currentTrial <= MAX_TRIALS);
        // Additional five seconds is the wait time in isElementDisplayed
        int totalSeconds = currentTrial * (waitInterval + Constants.FIVE);
        Assert.assertTrue("FAIL: The expected order number: '" + expectedOrderNumber +
                "' was NOT found in inbox after " + totalSeconds + " seconds", success);
        LOGGER.info("assertOrderEmailInInbox completed for inbox: '" + currentEmailInbox + "'. Order Number '" +
                expectedOrderNumber + "' found in " + totalSeconds + " seconds");
    }

    /**
     * Open Order|Appointment Confirmation Email
     *
     * @param emailType
     */
    public void openConfirmationEmail(String emailType) {
        LOGGER.info("openConfirmationEmail started for " + emailType);
        driver.waitForPageToLoad();
        WebElement confirmationEmail;
        if (emailType.equalsIgnoreCase(ConstantsDtc.ORDER_CONFIRMATION)) {
            confirmationEmail = driver.getElementWithText(emailSubjectsBy, ConstantsDtc.ORDER_CONFIRMATION);
        } else {
            confirmationEmail = driver.getElementWithText(emailSubjectsBy, ConstantsDtc.APPOINTMENT_CONFIRMATION);
        }
        Actions action = new Actions(webDriver);
        action.click(confirmationEmail).perform();
        driver.waitForPageToLoad();

        String emailBodyFrame = getEmailBodyFrame();
        webDriver.switchTo().frame(emailBodyFrame);
        LOGGER.info("openConfirmationEmail completed for " + emailType);
    }
}