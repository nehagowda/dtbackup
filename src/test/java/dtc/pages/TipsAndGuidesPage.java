package dtc.pages;

import common.Constants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class TipsAndGuidesPage {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(TipsAndGuidesPage.class.getName());

    public TipsAndGuidesPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
    }

    private static final By table = By.cssSelector(".contentpage > div > table");

    @FindBy(linkText = "Top of Page")
    public static WebElement topOfPage;

    /**
     * Helper method to access driver method, clicking a page link with text
     *
     * @param linkText Text of link to click
     */
    public void clickPageLink(String linkText) {
        LOGGER.info("clickPageLink " + linkText + " started");
        driver.clickElementWithLinkText(linkText);
        LOGGER.info("clickPageLink " + linkText + " complete");
    }

    /**
     * Clicks each link on Brands page and verifies it moves the screen
     */
    public void verifyBrandLinks() {
        LOGGER.info("verifyBrandLinks started");
        WebElement returnElement = webDriver.findElement(table);
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        Long before = getYOffset();
        Long top = before;
        StringBuilder errorLinks = new StringBuilder();

        List<WebElement> elements = returnElement.findElements(CommonActions.anchorTagBy);
        for (WebElement element : elements) {
            driver.moveToElementClick(element);
            Long after = getYOffset();
            if (after == top) {
                errorLinks.append(element.getText()).append(", ");
            }
            driver.moveToElementClick(topOfPage);
            top = getYOffset();
            Assert.assertTrue("FAIL: Brand links page did not scroll back to top.", top == before);
        }

        if (errorLinks.length() > 0) {
            errorLinks.setLength(errorLinks.length() - 2);
            Assert.fail("Fail: The following links did not navigate to a section when clicked on " + errorLinks);
        }
        LOGGER.info("verifyBrandLinks complete");
    }

    /**
     * Gets the Y coordinates of the visible screen
     *
     * @return Long type element
     */
    public Long getYOffset() {
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        Long after = (Long) executor.executeScript("return window.pageYOffset;");
        return after;
    }
}