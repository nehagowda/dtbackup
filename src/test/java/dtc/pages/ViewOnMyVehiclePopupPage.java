package dtc.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.logging.Logger;

public class ViewOnMyVehiclePopupPage {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(FoundItLowerPopupPage.class.getName());
    private MyAccountPage myAccountPage;

    public ViewOnMyVehiclePopupPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        myAccountPage = new MyAccountPage(driver);
        PageFactory.initElements(webDriver, this);
    }

    public static final By vehicleImageBy = By.className("vovImage");

    @FindBy(className = "vovStyles")
    public static WebElement vehicleTrim;

    @FindBy(className = "vovColors")
    public static WebElement vehicleColor;

    @FindBy(tagName = "h4")
    public static WebElement vehicleName;

    /**
     * Verifies the Image link text contains the VehicleName VehicleTrim VehicleColor WheelBrand and WheelName.
     */
    public void assertVehicleImageUrl(String vehicleName, String VehicleTrim, String VehicleColor, String wheelBrand, String wheelName) {
        LOGGER.info("assertVehicleImageUrl started ");
        WebElement element = null;

        driver.waitForPageToLoad();
        element = webDriver.findElement(ViewOnMyVehiclePopupPage.vehicleImageBy);
        String imageLinkText = element.getAttribute("src");

        Assert.assertTrue("FAIL: Image link text does not contains  " + vehicleName, imageLinkText.contains(vehicleName));
        Assert.assertTrue("FAIL: Image link text does not contains  " + VehicleTrim, imageLinkText.contains(VehicleTrim));
        Assert.assertTrue("FAIL: Image link text does not contains  " + VehicleColor, imageLinkText.contains(VehicleColor));
        Assert.assertTrue("FAIL: Image link text does not contains  " + wheelBrand, imageLinkText.contains(wheelBrand));
        Assert.assertTrue("FAIL: Image link text does not contains  " + wheelName, imageLinkText.contains(wheelName));

        LOGGER.info("assertVehicleImageUrl completed ");
    }

    /**
     * Verifies PDP page is present
     */
    public void verifyViewOnMyVehiclePopupPage() {
        LOGGER.info("verifyViewOnMyVehiclePopupPage started");
        driver.waitForPageToLoad();
        driver.waitForMilliseconds();
        Assert.assertTrue("FAIL: ViewOnMyVehicle PopupPage didn't display",
                driver.isElementDisplayed(vehicleImageBy));
        LOGGER.info("verifyViewOnMyVehiclePopupPage completed");
    }
}


