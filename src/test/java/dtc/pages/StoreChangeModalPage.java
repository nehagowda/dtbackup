package dtc.pages;

import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import java.util.List;
import java.util.logging.Logger;

public class StoreChangeModalPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private static final Logger LOGGER = Logger.getLogger(StoreChangeModalPage.class.getName());

    public StoreChangeModalPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    public static final String CONFIRM_STORE_CHANGE = "CONFIRM STORE CHANGE";
    public static final String CONFIRM_STORE_CHANGE_WARNING = "Updating your store to this location results in the following changes:";
    public static final String PRODUCT_NOT_AVAILABLE = "Product not available";

    public static final By confirmChangeStoreWarningBy = By.cssSelector("[class*='store-change-modal__warning-header']");

    @FindBy(css = "div[class*='store-change-modal__product-details']")
    public static List<WebElement> modalProductBox;

    @FindBy(css = "div[class*='store-change-modal__modal-title']")
    public static WebElement modalTitle;

    /***
     * Verify the store changed modal contains product and success message
     *
     * @param productName    product Name of the product to verify
     * @param availability  change related to availability (Increase |Decrease |Product not available|Available today)
     */
    public void verifyModalItemAndMessage(String productName, String availability) {
        LOGGER.info("verifyModalItemAndMessage started");
        boolean flag = false;
        for (WebElement productBox : modalProductBox) {
            String productBoxText = productBox.getText().toLowerCase();
            if (productBoxText.contains(productName.toLowerCase())) {
                Assert.assertTrue("FAIL: The Store Changed modal does not contains product - '" + productName +
                                "' reflecting availability with message - '" + availability + "'.",
                        productBoxText.contains(availability.toLowerCase()));
                flag = true;
                break;
            }
        }
        Assert.assertTrue("FAIL: The Store Changed modal does not contains product with name - : '" + productName, flag);
        LOGGER.info("verifyModalItemAndMessage completed");
    }

    /***
     * Verify the store changed modal contains product
     */
    public void verifyModalProductName() {
        LOGGER.info("verifyModalProductName started");
        int productCount = driver.scenarioData.productInfoList.size();
        for (int index = 0; index < productCount; index++) {
            boolean found = false;
            String productName = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT, index);
            for (WebElement productBox : modalProductBox) {
                String productBoxText = productBox.getText();
                if (CommonUtils.containsIgnoreCase(productBoxText, productName)) {
                    found = true;
                }
            }
            Assert.assertTrue("FAIL: The Store Changed modal does not contain '" + productName + "'", found);
        }
        LOGGER.info("verifyModalProductName completed");
    }

    /**
     * Remove unavailable products listed on the store changed modal from productInfoList hashmap
     */
    public void removeUnavailableProductsFromProductInfoList() {
        LOGGER.info("removeUnavailableProductsFromProductInfoList started");
        int productCount = driver.scenarioData.productInfoList.size();
        for (int index = 0; index < productCount; index++) {
            String productName = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT, index);
            for (WebElement productBox : modalProductBox) {
                String productBoxText = productBox.getText();
                if (CommonUtils.containsIgnoreCase(productBoxText, productName)) {
                    if (CommonUtils.containsIgnoreCase(productBoxText, PRODUCT_NOT_AVAILABLE)) {
                        String brand = commonActions.productInfoListGetValue(ConstantsDtc.BRAND, index);
                        commonActions.removeProductInfoListItem(brand, productName);
                        productCount--;
                    }
                }
            }
        }
        LOGGER.info("removeUnavailableProductsFromProductInfoList completed");
    }


    /***
     * Verify the store changed modal is displayed
     */
    public void verifyStoreChangedModalDisplayed() {
        LOGGER.info("verifyStoreChangedModalDisplayed started");
        driver.waitForElementVisible(modalTitle);
        Assert.assertTrue("FAIL: Store Changed modal is not displayed as expected",
                modalTitle.getText().contains(ConstantsDtc.STORE_CHANGED));
        LOGGER.info("verifyStoreChangedModalDisplayed completed");
    }

    /***
     * Verify the store changed modal contains store and success message
     *
     * @param text    Store text to verify on modal
     */
    public void verifyStoreChangeModalContains(String text) {
        LOGGER.info("verifyStoreChangeModalContains started");
        String modalText = CommonActions.changeStoreModal.getText();
        Assert.assertTrue("FAIL: The Store Changed modal does not contains expected text: '" + text +
                "', displayed text: '" + modalText + "'.", CommonUtils.containsIgnoreCase(modalText, text));
        LOGGER.info("verifyStoreChangeModalContains completed");
    }

    /***
     * Verify the "CONFIRM STORE CHANGE" modal is displayed
     */
    public void verifyConfirmStoreChangeModalWithWarningMessage() {
        LOGGER.info("verifyConfirmStoreChangeModalWithWarningMessage started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: " + CONFIRM_STORE_CHANGE + " modal is not displayed.",
                driver.isElementDisplayed(driver.getElementWithText
                        (CommonActions.dtModalHeaderBy, CONFIRM_STORE_CHANGE)));
        Assert.assertTrue("FAIL: " + CONFIRM_STORE_CHANGE + " does not contain warning message - "
                + CONFIRM_STORE_CHANGE_WARNING, driver.isElementDisplayed(driver.getElementWithText
                (confirmChangeStoreWarningBy, CONFIRM_STORE_CHANGE_WARNING)));
        LOGGER.info("verifyConfirmStoreChangeModalWithWarningMessage completed");
    }
}
