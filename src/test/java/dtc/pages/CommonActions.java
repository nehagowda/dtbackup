package dtc.pages;

import com.google.common.collect.Ordering;
import common.Config;
import common.Constants;
import crossapplication.data.CrossApplicationData;
import cucumber.api.Scenario;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import orderxmls.pages.OrderXmls;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.CommonUtils;
import utilities.Driver;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 10/5/16.
 */
public class CommonActions {
    private Driver driver;
    private WebDriver webDriver;
    private Actions actions;
    private Customer customer;
    private CrossApplicationData crossApplicationData;
    private final Logger LOGGER = Logger.getLogger(CommonActions.class.getName());

    public CommonActions(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        crossApplicationData = new CrossApplicationData();
        actions = new Actions(webDriver);
        customer = new Customer();
        PageFactory.initElements(webDriver, this);
    }

    public static HashMap<String, Integer> stringAndCount = new HashMap<>();
    public static final String FALSE = "false";
    public static final String ZERO_DOLLARS = "$0";
    public static final String ADD_VEHICLE_BUTTON_TEXT = "+ Add Vehicle";
    public static String defaultShippingOption = "Ground";
    public static final List<String> INSTALLATION_APPOINTMENT_MESSAGE =
            Arrays.asList(ConstantsDtc.INSTALLATION_APPOINTMENT_HEADER, ConstantsDtc.INSTALLATION_APPOINTMENT_LINE1,
                    ConstantsDtc.INSTALLATION_APPOINTMENT_LINE2);
    public static final int PRIMARY_FORM_INDEX = 0;
    public static final int SECONDARY_FORM_INDEX = 1;
    public static final String FIRST_NAME = "First Name";
    public static final String LAST_NAME = "Last Name";
    public static final String ADDRESS_LINE_1 = "Address Line 1";
    public static final String ADDRESS_LINE_2 = "Address Line 2";
    public static final String COUNTRY = "Country";
    public static final String ZIP = "Zip / Postal Code";
    public static final String EMAIL = "Email";
    public static final String EMAIL_ADDRESS = "Email Address";
    public static final String STATE_DROP_DOWN_WEB_STRING = "addressProvince_chosen";
    public static final String PHONE_TYPE_DROP_DOWN_WEB_STRING = "phoneTypeId_chosen";
    public static final String ADDRESS_VERIFICATION = "Address Verification";
    public static final String SHOW_FEE_DETAILS = "show fee details";
    public static final String MY_VEHICLES_MODAL = "My Vehicles Modal";
    public static final String NON_OE_FRONT_TIRE = "Non-OE FRONT Tire";
    public static final String OPTIONAL_FRONT_TIRE = "Optional front tire";
    public static final String NON_OE_REAR_TIRE = "Non-OE REAR Tire";
    public static final String OPTIONAL_REAR_TIRE = "Optional rear tire";
    public static final String SET = "set";
    public static final String OUTLOOK_URL = "https://outlook.office365.com/owa/?realm=discounttire.com";
    public static final String SELECTED_VEHICLE = "SELECTED VEHICLE";
    private static String stateTaxString = "0.00";
    private static String cityTaxString = "0.00";
    private static String otherTaxString = "0.00";
    public static final String EMAIL_URL = "https://login.microsoftonline.com/";
    public static final String STORYBOOK_URL = "http://storybook.epic.discounttire.com";
    public static final String CATALOG = "Catalog";
    public static final String CART = "Cart";
    public static final String OPTIONAL_TIRE_AND_WHEEL_SIZE = "optional tire and wheel size +";
    public static final String WHAT_ARE_YOU_SHOPPING_FOR = "What are you shopping for";
    public final String TEXT_XPATH = "//*[text()='";
    public static final String SAVED_VEHICLES = "Saved Vehicles";
    private static final String RECENT_SEARCHES = "Recent Searches";
    public static final String TRUE = "true";
    public static final String REVERT_TO_OE_SIZE = "Revert to oe size";
    public static String firstItemNumPlp;
    public static final String MIN = "min";
    public static final String MAX = "max";
    public static final String TABS = "Tabs";
    public static final String CONTINUE_WITH_THIS_VEHICLE = "Continue with this vehicle";
    public static int minValueApproximate = 0;
    public static int maxValueApproximate = 0;
    private static String appendInstanceString = "__";
    public static final String CHECKFIT_EDIT = "CheckFitEdit";
    public static boolean packageFlow = false;
    public static String generatedRandomEmail = "";
    private static final String PLP_PAGE = "PLP";
    private static final String COMPARE_PAGE = "compare";
    private static final String PDP_PAGE = "PDP";
    public static final String PRODUCT_NAME = "product name";
    public static final int MAX_CAROUSEL_LIST_SIZE = Constants.FIVE;

    private enum productData {
        BRAND,
        PRODUCT,
        PRICE,
        INVENTORY_MESSAGE,
        QUANTITY,
        IN_CART,
        ITEM;
    }

    // Initializing product quantity to blank so it can be populated during a test run only if needed
    public static String productQuantity = "";
    public final ArrayList<String> suggestedSellingArticles = new ArrayList<String>();

    boolean condition;

    private static final String welcomePopupClosed =
            "return document.getElementsByClassName('store-pop').length === 0;";

    private static final String chatNowPopupClosed =
            "return document.getElementsByClassName('LPMcontainer').length === 0;";

    private static final By plpFitmentVerificationBy = By.cssSelector("[class*='fitment-verification__fitment']");

    private static final By plpfitmentBannerBy = By.cssSelector("[class*='product-listing-page__fitment']");

    private static final By pdpfitmentBannerBy = By.cssSelector("[class*='fitment-verification__fitment__']");

    private static final By brandsFitmentBannerBy = By.cssSelector("[class*='fitment-verification__loading-container'] " +
            "[class*='fitment-verification__fits-no']");

    private static final By checkFitBannerBy = By.cssSelector("[class*='check-fit__check-fit']");

    private static final By checkFitBannerTitleBy = By.cssSelector("[class*='check-fit__title']");

    private static final By checkFitBannerIconBy = By.cssSelector("[class*='check-fit__check-fit'] [class*='icon-fitment']");

    public static final By breadcrumbLinkBy = By.cssSelector("[class='breadcrumb-items']");

    private static final By noThanksBtnBy = By.className("LPMcloseButton");

    public static final By nextBtnBy = By.cssSelector("[class*='pagination__pagination'] [class*='fa-angle-right']");

    private static final By previousBtnBy = By.cssSelector("[class*='pagination__pagination'] [class*='fa-angle-left']");

    public static final By btnDefaultBy = By.className("btn-default");

    public static final By dtButtonBy = By.className("dt-button");

    public static final By closeButtonXBy = By.className("fa-times");

    public static final By dropdownBy =
            By.className("react-selectize-search-field-and-selected-values");

    public static final By selectBy = By.className("react-selectize-control");

    public static final By dropdownOptionBy = By.cssSelector("[class*='simple-option']");

    public static final By formSubmitButtonBy = By.cssSelector("[class*='dt-form__submit']");

    public static final By closeButtonBy = By.className("icon-close");

    private static final By miniCartBy = By.className("fa-shopping-cart");

    public static final By firstBtnBy = By.cssSelector("[class*='pagination__pagination'] [class*='double-left']");

    private static final By lastBtnBy = By.cssSelector("[class*='pagination__pagination'] [class*='double-right']");

    public static final By paginationBy = By.cssSelector("[class*='pagination__pagination']");

    public static final By spanPaginationBy = By.cssSelector("span[class*='pagination-message']");

    public static final By addToPackageBy = By.cssSelector("[class*='add-to-package__button__']");

    private static final By selectedPageBy = By.cssSelector("[class*='pagination__selected']");

    public static final By overallRatingBy = By.xpath("//span[@class='overall-rating']");

    public static final By storeReviewScoreBy = By.cssSelector("[class *='review__container'] [class*='star-rating__rating-value']");

    public static final By storeReadReviewsBy = By.className("review-rating__read");

    public static final By headerBy = By.tagName("h1");

    public static final By headerHeadlineBy = By.cssSelector("[class*='headline-description__section-name']");

    public static final By ptagNameBy = By.tagName("p");

    public static final By spanTagNameBy = By.tagName("span");

    public static final By headerSecondBy = By.tagName("h2");

    public static final By headerFourthBy = By.tagName("h4");

    public static final By headerThirdBy = By.tagName("h3");

    public static final By headerFifthBy = By.tagName("h5");

    public static final By headerSixthBy = By.tagName("h6");

    public static final By buttonBy = By.tagName("button");

    public static final By optionTagBy = By.className("active-result");

    public static final By anchorTagBy = By.tagName("a");

    public static final By imgTagBy = By.tagName("img");
    //endregion

    public static final By dtModalCloseBy = By.cssSelector("button[class*='dt-modal__close']");

    public static final By dtLinkBy = By.className("dt-link");

    public static final By radioButtonBy = By.xpath("//span[contains(@class,'dt-radio__display')]");

    public static final By radioLabelBy = By.className("form-group__radio-label");

    public static final By collapsibleToggleBy = By.cssSelector("[class*='fitment-vehicle-results__optional-sizes-header']");

    public static final By formGroupBy = By.className("form-group");

    public static final By formGroupRadioBy = By.className("form-group--radio");

    public static final By inputBy = By.cssSelector("input");

    public static final By careCareOneReviewLabel = By.className("payment-details-form__cc1-review-label");

    public static final By dtModalHeaderBy = By.className("dt-modal__header");

    public static final By buttonLinkPrimaryBy = By.className("dt-button--primary");

    public static final By buttonLinkSecondaryBy = By.className("dt-button--secondary");

    public static final By buttonLinkDeclineBy = By.className("dt-button--ghost");

    public static final By dtModalTitleBy = By.className("dt-modal__title");

    public static final By dtModalContainerBy = By.cssSelector("[class*='dt-modal__container']");

    public static final By dtAddToCartModalContainerBy = By.cssSelector("[class*='add-to-cart__modal-container']");

    public static final By dtdAddToCartModalContainerBy = By.cssSelector("[class*='add-to-cart-old__modal___']");

    public static final By footerSectionLinkBy = By.cssSelector("[class*='footer__section-link']");

    public static final By inventoryMessageBy = By.cssSelector("[class*='product-locator__availability__']");

    public static final By inventoryMessageStaggeredBy = By.cssSelector("[class*='product-locator__bold']");

    public static final By cartInventoryMessageBy =
            By.cssSelector("[class*='product-details__cart-item__store-stock-message']");

    public static final By productLocatorAvailabilityBy =
            By.cssSelector("[class*='product-locator__availability']");

    public static final By plpProductAvailabilityListBy =
            By.cssSelector("[class*='product__text-block'] [class*='store-address__store-address']");

    public static final By plpProductAvailabilityListMobileBy =
            By.cssSelector("[class*='product__lower-block'] [class*='store-address__store-address']");

    public static final By productLowerBlockBy = By.cssSelector("[class*='product__lower-block']");

    public static final By inventoryMessagesBy = By.className("compare-main-section__inventory-messaging");

    public static final By stockInventoryMessageBy = By.cssSelector("[class*='selected-store__store-info'] [class*='store-quick-view__stock'] span");

    public static final By checkInventoryBy = By.cssSelector("[class*='product-locator__near-by-link']");

    public static final By pdpInfoContainerBy = By.cssSelector("[class*='product-detail-page__details']");

    public static final By tooltipBy = By.className("tooltip-toggle");

    public static final By addToCartProductAvailabilityBy = By.className("js-addToCartBtn");

    public static final By strongBy = By.cssSelector("strong");

    public static final By toolTipNeedItNowBy = By.cssSelector("div[class*='need-it-now__need-it-now___']");

    public static final By resultRowNeedItNowBy = By.cssSelector(".results-row .need-it-now");

    public static final By formGroupMessageErrorElementBy = By.className("form-group__message--error");

    public static final By installationAppointmentBy = By.cssSelector(".checkout-content__section--extended-assortment-message");

    public static final By productAvailabilityStoreStockBy = By.className("product-availability__store-stock");

    public static final By secondaryPaymentFormBy = By.className("payment-details-form__secondary-form-collapsible");

    public static final By addToCartByTextBy = By.xpath("//button[contains(text(),'Add to Cart')]");

    public static final By addToCartByClassBy = By.cssSelector("[class*='add-to-cart__button__']");

    public static final By reactSelectizePlaceholderBy = By.cssSelector("[class*='react-selectize-placeholder']");

    public static final By reactDropdownMenuBy = By.className("dropdown-menu");

    public static final By productQuantityBoxBy = By.cssSelector("[class*='product-input__input']");

    public static final By packageProductQuantityBoxBy = By.cssSelector("[class*='add-to-package__quantity__']");

    public static final By stockMessageCheckInventoryBy = By.className("cart-item__store-stock-message--check-inventory");

    public static final By brandNameBy = By.className("cart-item__product-brand");

    public static final By productNameBy = By.className("cart-item__product-name");

    public static final By modalProductInfoNameBy = By.cssSelector("[class*='modal-product-info__name']");

    public static final By itemAddedToCartBy = By.cssSelector("[class*='add-to-cart-old__name']");

    public static final By optionNameBy = By.className("cart-item__option-label");

    public static final By checkoutSummaryStoreAddressBy = By.className("checkout-summary-store__address");

    public static final By vehicleDisplayImageBy = By.cssSelector("[class*='fitment-banner__vehicle-display-image']");

    public static final By fitmentBannerTireBy = By.cssSelector("[class*='fitment-banner__tire']");

    public static final By fitmentBannerWheelBy = By.cssSelector("[class*='fitment-banner__wheel']");

    public static final By fitmentSizeBadgeBy = By.cssSelector("[class*='fitment-size-badge']");

    public static final By fitmentSizeLabelBy = By.cssSelector("[class*='fitment-banner__type-label']");

    public static final By fitmentVerificationIcon = By.cssSelector("[class*='fitment-verification__icon']");

    public static final By resultsBarSizeBy = By.cssSelector("[class*='results-bar_']");

    public static final By listItemBy = By.tagName("li");

    public static final By vehicleInfoSectionBy = By.cssSelector("[class*='vehicle-detail__vehicle-detail']");

    public static final By savedVehicleInfoSectionBy = By.xpath("//div[contains(@class,'saved')]//div[contains(@class,'vehicle-detail__vehicle-detail--modal___')]");

    public static final By cartItemsHeaderIconBy =
            By.cssSelector("[class*='-product__productType_CollapseToggle']");

    public static final By cartTireAndWheelPackageHeaderBy =
            By.cssSelector("[class*='collapsible-section__toggle  checkout-summary-cart__cart-items-header']");

    public static final By vehicleDescriptionBy = By.cssSelector("[class*='fitment-banner__vehicle-description']");

    public static final By vehicleCheckFitDescriptionBy = By.cssSelector("[class*='check-fit__check-fit'] [class*='fitment-banner__vehicle-description']");

    public static final By itemCodeBy = By.cssSelector("[class*='itemized-product__item-number']");

    public static final By editEnteredAddressBy = By.xpath("//button[contains(text(),'Edit Entered Address')]");

    public static final By useUspsCorrectedAddressBy = By.xpath("//button[contains(text(),'Use USPS Corrected Address')]");

    public static final By addressVerificationBy = By.xpath("//h2[contains(text(),'Address Verification')]");

    public static final By orderListItemBy = By.cssSelector("[class*='my-orders__order-list-item']");

    public static final By vehicleSelectedCheckFitBy = By.cssSelector("[class*='shop-vehicle-confirmation-modal__vehicle-container']");

    public static By label = By.tagName("label");

    public static final By checkboxInputBy = By.className("dt-checkbox__input");

    public static final By shopVehicleConfirmationModalButtonBy =
            By.cssSelector("[class*='shop-vehicle-confirmation-modal__button_']");

    public static final By sortByOptionBy = By.cssSelector("[class*='react-selectize-search-field-and-selected-values'] " +
            "[class*='value-wrapper'] [class*='simple-value']");

    public static final By compareProductColumnBy = By.className("auto-compare-main-section-columns");

    public static final By breadCrumbContainerBy = By.cssSelector("[class*='breadcrumb-container']");

    public static final By spinnerBy = By.cssSelector("[class*='loading-spinner__loading-spinner']");

    public static final By plpProductsBy = By.xpath("//*[contains(@class, 'product__text-block')]");

    public static final By viewDetailsLinkBy = By.cssSelector("[class*='suggested-carousel__view-details']");

    public static final By editIconBy = By.cssSelector("[class*='__edit-icon']");

    @FindBy(css = "[class*='fitment-banner__size-info']")
    public static WebElement fitmentBannerSizeInfo;

    @FindBy(css = "[class*='fitment-banner__size-info-rear']")
    public static WebElement fitmentBannerSizeInfoRear;

    @FindBy(css = "a[class='dt-link hide-sm']")
    public static WebElement editVehicleMobile;

    @FindBy(xpath = "//button[contains(@class,'add-to-cart__button')] | //button[contains(@class,'add-to-package__button')]")
    public static WebElement addToCartOrPackage;

    @FindBy(className = "header__nav-button-label")
    public static WebElement myAccountLoggedInLabel;

    @FindBy(css = "[class*='popover__list-item popover__list-item--heading']")
    public static WebElement getMyAccountLoggedInLabelMobile;

    @FindBy(css = "[class*='fitment-vehicle-results__product-heading']")
    public static WebElement whatAreYouShoppingForHeadingButton;

    @FindBy(className = "price-range-filter__apply")
    public static WebElement applyPriceRangeFilterButton;

    @FindBy(className = "page-title")
    public static WebElement pageTitle;

    @FindBy(css = "[class*='fitment-vehicle-results__toggle-size-label-optional']")
    public static WebElement fitmentVehicleResultsOptionalLabel;

    @FindBy(className = "fa-edit")
    public static WebElement editIcon;

    public static final By editVehicleBy = By.cssSelector("[class='dt-link display-sm']");

    public static final By quantityBy = By.className("product-availability__product-qty");

    public static final By dtMessageWarningBy = By.cssSelector(".dt-message--warning");

    public static final By fitmentBannerSizeInfoBy = By.cssSelector("[class*='fitment-banner__size-info']");

    public static final By freeShippingBy = By.cssSelector("div[class*='price__free-shipping']");

    public static final By strongTagBY = By.tagName("strong");

    @FindBy(css = "[class*='fitment-banner__']")
    public static WebElement fitmentBanner;

    public static final By productListFilter = By.className("product-list-filter__category-name");

    public static final By tireBrandBy = By.className("tire__brand");

    public static final By tireNameBy = By.className("tire__name");

    public static final By tirePriceBy = By.className("price-block__amount");

    public static final By reviewRatingBy = By.cssSelector("[class*=rating-overview__read-reviews]");

    public static final By starRatingBy = By.cssSelector("[class*=rating-overview__star-row]");

    public static By checkNearByStoresBy = By.cssSelector("a[class*='product-locator__near-by-link___']");

    public static By subTotalToolTipBy = By.cssSelector("button[class*='tooltip-toggle icon-info un']");

    public static By subTotalWithToolTipBy = By.cssSelector("//span[class*='subtotal__label']");

    public static final By bannerRecommendationBy = By.cssSelector("[class*='banner__recommendation']");

    public static final By bannerRecommendationMobileBy = By.cssSelector("[class*='compare-mobile-products'] [class*='banner__recommendation']");

    public static final By pdpDataViewDescriptionBy = By.className("pdl-data__view-description");

    public static final By itemSubtotalBy = By.cssSelector("[class='summary-product__subtotal-label']");

    public static final By feeslabelBy = By.cssSelector("[class='checkout-summary-cart__fees-header-label']");

    public static final By apptDetailsSelectDateActiveBy = By.className("appointment-details__select-date--active");

    public static final By availableApptTimesBy = By.className("appointment-details__select-time-row-time--available");

    public static final By apptDatesDayBy = By.className("appointment-details__select-date-day");

    public static final By lowerPriceLinkBy = By.cssSelector("a[class*='price__lower-price___']");

    public static final By myVehiclesHeaderBy = By.className("header__my-vehicles-label");

    public static final By dtButtonLgBy = By.cssSelector("[class*='dt-button-lg']");

    public static final By displayInlineSmBy = By.className("display-inline-sm");

    public static final By myVehiclesButtonBy = By.cssSelector("[class*='fitment-recent-vehicles__recent-vehicles-option']");

    public static final By successMessageBy = By.cssSelector("[class*='success-message']");

    public static final By promotionDiscountBy = By.cssSelector("[class*=promotions-overview__promotion-link]");

    public static final By colorSwatchContainerBy =
            By.cssSelector("[class*='swatches-selector__swatches-images-container']");

    public static final By colorSwatchBy = By.cssSelector("[class*='swatches-selector__swatches-img-container']");

    public static final By productPriceBy = By.cssSelector("div[class*='price__price-block']");

    public static final By simpleValueBy = By.className("simple-value");

    private static final By storeAddressListInComparePageBy = By.cssSelector("[class*='compare-desktop'] [class*='inventory-message auto-stock-mystore']");

    private static final By storeAddressListInComparePageMobileBy = By.cssSelector("[class*='compare-mobile'] [class*='inventory-message auto-stock-mystore']");

    private static final By storeAddressListInPDPBy = By.cssSelector("[class*='store-address__store-address']");

    public static final By searchBarBy = By.cssSelector("[class*='search__search']");

    public static final By checkBoxBy = By.className("dt-checkbox__display");

    public static final By resultsRowMobileBy = By.className("product-list__item");

    public static final By priceRangeMinimumPointBy = By.className("rc-slider-handle-1");

    public static final By priceRangeMaximumPointBy = By.className("rc-slider-handle-2");

    public static final By appointmentCheckCartBy = By.className("appointment-checkcart");

    public static final By expandCartItemsBy = By.cssSelector("[class*='checkout-summary-cart__cart-items-label']");

    public static final By addressLine1By = By.id("address-line-1");

    public static final By addressLine2By = By.id("address-line-2-optional");

    public static final By zipCodeBy = By.id("zip-code");

    public static final By cityBy = By.id("city");

    public static final By phoneNumberBy = By.id("phone-number");

    public static final By secondaryContactMobileBy = By.cssSelector("[class*='my-profile__second-address'] " +
            "[class*='my-profile__form-wrapper'] form");

    public static final By openCollapsibleSectionBy = By.className("collapsible-section__content--open");

    public static final By optionalTireAndWheelSizeLinkBy = By.cssSelector("[class*='fitment-banner__optional-sizes-link']");

    public static final By cartItemCodeBy =
            By.cssSelector("[class*='staggered-item__qty-price-container'] [class*='cart-item__product-code']");

    public static final By selectedValuesBy = By.cssSelector("[class*='selected-values']");

    public static final By listingTabContainerBy = By.cssSelector("[class*='listing-tabs__container']");

    public static final By staggeredItemNumberBy = By.cssSelector("[class*='itemized-product__item-number']");

    public static final By addToCartSecondaryBy = By.cssSelector("[class*='button-secondary']");

    public static final By cartItemQuantityBy = By.cssSelector("[class*='item__item-quantity']");

    public static final By cartItemProductBy = By.className("cart-item__product");

    public static final By addToCartModalBy = By.cssSelector("[class*='add-to-cart__overlay']");

    public static final By suggestedSellingTextContainerBy = By.cssSelector("[class*='suggested-carousel__text-container']");

    public static final By suggestedSellingInnerTitleBy = By.cssSelector("[class*='suggested-carousel__inner-tile']");

    public static final By suggestedSellingFeatureIconBy = By.cssSelector("[class*='product-features__product-features']");

    public static final By suggestedSellingCarouselBy = By.cssSelector("[class*='suggested-carousel__container']");

    public static final By suggestedSellingViewDetailsLinkBy = By.cssSelector("[class*='suggested-carousel__tile-link']");

    public static final By itemsAddedToCartProductNameBy = By.cssSelector("[class*='suggested-carousel__name']");

    @FindBy(css = "[class*='promotions-overview__promotion-link']")
    public static WebElement promotionDiscount;

    @FindBy(css = "[class*='canada-subtotal'] [class*='price']")
    public static WebElement taxSubtotal;

    @FindBy(xpath = "//input[contains(@class,'product-input__input')]")
    public static WebElement productQuantityBox;

    @FindBy(className = "btn-default")
    public static WebElement btnDefault;

    @FindBy(className = "global-message-content")
    public static WebElement globalMessage;

    @FindBy(xpath = "//button[contains(@class,'add-to-cart__button')]")
    public static WebElement addToCart;

    @FindBy(xpath = "//button[contains(@class,'add-to-package__button')]")
    public static WebElement addToPackage;

    @FindBy(xpath = "//a[contains(@class,'__button-secondary___')]")
    public static WebElement addToCartSecondary;

    @FindBy(xpath = "//div[contains(@class,'add-to-cart__title')]")
    public static WebElement cartTitle;

    @FindBy(xpath = "//div[contains(@class,'add-to-cart__subtitle')]")
    public static WebElement cartSubTitle;

    @FindBy(className = "icon-close")
    public static WebElement closeCart;

    @FindBy(id = "chooseStore")
    public static WebElement chooseStoreButton;

    @FindBy(className = "auto-header-my-store-address")
    public static WebElement cartMyStore;

    @FindBy(xpath = "//td[text()='There are no products to compare']")
    public static WebElement noProductsToCompareMessage;

    @FindBy(className = "tooltip-content__content")
    public static WebElement tooltipContent;

    @FindBy(css = "[class*='dt-form__content'] [class*=' form-group--focused'] > p")
    public static WebElement formGroupErrorMessageElement;

    @FindBy(className = "fitment-container")
    public static WebElement fitmentContainer;

    @FindBy(css = "[class*='dt-modal__container']")
    public static WebElement dtModalContainer;

    @FindBy(className = "pop-box")
    public static WebElement popUpDialog;

    @FindBy(className = "breadcrumb-container")
    public static WebElement breadCrumbContainer;

    @FindBy(className = "cart-summary__breakdown-price")
    public static WebElement cartSubtotal;

    @FindBy(className = "checkout-summary-cart__cart-items-label")
    public static WebElement expandCartItemsLabel;

    @FindBy(className = "checkout-summary-cart__order-total-price")
    public static WebElement orderTotal;

    @FindBy(className = "loading-spinner-placeholder")
    public static WebElement preloader;

    @FindBy(className = "fa-envelope")
    public static WebElement emailEnvelopeIcon;

    @FindBy(className = "checkout-form__optional-address-toggle")
    public static WebElement expandOptionalAddressLine;

    @FindBy(css = "[class*='dt-form__submit']")
    public static WebElement submitButton;

    @FindBy(id = "first-name")
    public static WebElement firstName;

    @FindBy(id = "last-name")
    public static WebElement lastName;

    @FindBy(id = "email-address")
    public static WebElement email;

    @FindBy(id = "phone-number")
    public static WebElement phone;

    @FindBy(id = "address-line-1")
    public static WebElement address1;

    @FindBy(css = "[id*='address-line-1']")
    public static WebElement billingAddress1;

    @FindBy(id = "address-line-2")
    public static WebElement address2;

    @FindBy(id = "address-line-2-optional")
    public static WebElement address2Optional;

    @FindBy(id = "zip-code")
    public static WebElement orderSummaryZipCode;

    @FindBy(name = "postcode")
    public static WebElement zip;

    @FindBy(css = "[id*='-zip-code']")
    public static WebElement billingZipCode;

    @FindBy(id = "city")
    public static WebElement city;

    @FindBy(css = "div[class*='tooltip-content__content']")
    public static WebElement toolTipContent;

    @FindBy(css = "[class*='banner__topRecommendation']")
    public static WebElement topRecommendation;

    @FindBy(css = "[class*='compare-mobile-products'] [class*='banner__topRecommendation']")
    public static WebElement topRecommendationMobile;

    @FindBy(css = "[class*='product-filter__close-icon']")
    public static WebElement closeIcon;

    @FindBy(id = "primary-driving-location")
    public static WebElement pdlZipcode;

    @FindBy(css = "[class*='store-change-modal__modal-body']")
    public static WebElement changeStoreModal;

    @FindBy(css = "div[class*='product-listing-page__container']")
    public static WebElement productListingPageContainer;

    @FindBy(tagName = "body")
    public static WebElement webBrowserBody;

    @FindBy(xpath = "//a[contains(@class, 'product-filter__button')][text()='Clear All']")
    public static WebElement clearAllFiltersLink;

    @FindBy(xpath = "//a[contains(@class, 'product-filter__button')][text()='Apply Filters']")
    public static WebElement applyFiltersButton;

    @FindBy(css = "a[class*='fitment-verification__link']")
    public static WebElement enterVehicleLink;

    @FindBy(id = "email")
    public static WebElement emailTextField;

    @FindBy(className = "js-nav-myvehicles-button")
    public static WebElement myVehiclesHeaderButton;

    @FindBy(css = "[class*='vehicle-detail__vehicle-detail--selected']")
    public static WebElement selectedVehicleSection;

    @FindBy(css = "button[class*='dt-button-lg']")
    public static WebElement dtButton;

    @FindBy(css = "button[class*=dt-button-lg--primary]")
    public static WebElement dtButtonLgPrimary;

    @FindBy(className = "react-selectize-toggle-button-container")
    public static WebElement sortByDropdown;

    @FindBy(xpath = "(//input[contains(@class,'auto-results-row-quantity')])[11]")
    public static WebElement itemQuantityCatalog;

    @FindBy(className = "payment-details-form__billing-container")
    public static WebElement paymentDetailBillingContainer;

    @FindBy(css = ".simple-value>span")
    public static WebElement simpleValue;

    @FindBy(id = "password")
    public static WebElement password;

    @FindBy(name = "billingAddress2")
    public static WebElement billingAddress2;

    @FindBy(css = "[class*='shop-all-button']")
    public static WebElement shopAllTiresButton;

    @FindBy(css = "[class*='order-summary-cart-fees__togglePriceDetailsIcon']")
    public static WebElement showRequiredFeesToggleIconStandard;

    @FindBy(css = "[class*='order-summary-cart-addons__togglePriceDetailsIcon']")
    public static WebElement showAddOnsToggleIconStandard;

    @FindBy(css = "[class*='order-summary-cart-fees-staggered__togglePriceDetailsIcon']")
    public static WebElement showRequiredFeesToggleIconStaggered;

    @FindBy(css = "[class*='order-summary-cart-addons-staggered__togglePriceDetailsIcon']")
    public static WebElement showAddOnsToggleIconStaggered;

    @FindBy(id = "city-state-or-zip-code")
    public static WebElement cityStateZipSearchField;

    @FindBy(css = ".auto-header-logo")
    public static WebElement dtLogo;

    @FindBy(className = "cart-item__final-summary-price")
    public static WebElement totalAmount;

    @FindBy(css = "[class*='__modal-container___']")
    public static WebElement addToCartModal;

    @FindBy(className = "myinfo-column")
    public static WebElement cartConfirmationMyInfoColumn;

    @FindBy(css = "[class*='suggested-carousel__suggested-carousel']")
    public static WebElement suggestedSellingCarousel;

    @FindBy(css = "[class*='suggested-carousel__carousel-title']")
    public static WebElement suggestedSellingCarouselTitle;

    @FindBy(css = "[class*='suggested-carousel__rectangle-right'")
    public static WebElement suggestedCarouselRightScrollRectangle;

    @FindBy(className = "appointment-details__select-time-message")
    public static WebElement apptSelectedMsg;

    /**
     * Clicks button containing certain text
     *
     * @param btnText        - Text of the button to be clicked
     * @param numberAttempts - Number of times to try to find the target button
     */
    public void clickButtonByText(String btnText, int numberAttempts) {
        LOGGER.info("clickButtonByText started with text = '" + btnText + "'");
        if (btnText.equalsIgnoreCase(ConstantsDtc.PLACE_ORDER) ||
                btnText.equalsIgnoreCase(ConstantsDtc.PAY_IN_STORE) ||
                btnText.equalsIgnoreCase(ConstantsDtc.PAY_ONLINE)) {
            addScenarioGenericData();
            if (btnText.equalsIgnoreCase(ConstantsDtc.PAY_IN_STORE))
                driver.scenarioData.genericData.put(Constants.ORDER_TYPE, ConstantsDtc.ROPIS);
            if (btnText.equalsIgnoreCase(ConstantsDtc.PAY_ONLINE))
                driver.scenarioData.genericData.put(Constants.ORDER_TYPE, ConstantsDtc.BOPIS);
        }
        if (btnText.equalsIgnoreCase(ConstantsDtc.FRONT) ||
                btnText.equalsIgnoreCase(ConstantsDtc.REAR) ||
                btnText.equalsIgnoreCase(ConstantsDtc.SETS) ||
                btnText.equalsIgnoreCase(ConstantsDtc.CONFIRM_APPOINTMENT))
            driver.waitForPageToLoad();
        WebElement button = getButtonWithText(btnText, numberAttempts);
        Assert.assertNotNull("FAIL: The " + btnText + " button was not displayed", button);
        driver.jsScrollToElementClick(button, false);
        driver.waitForPageToLoad(Constants.ONE_THOUSAND);
        if (btnText.equalsIgnoreCase(ConstantsDtc.SIGN_IN) ||
                btnText.equalsIgnoreCase(ConstantsDtc.CREATE_AN_ACCOUNT) ||
                btnText.equalsIgnoreCase(ConstantsDtc.CONTINUE_TO_CUSTOMER_DETAILS) ||
                btnText.equalsIgnoreCase(ConstantsDtc.CONTINUE_TO_PAYMENT) ||
                btnText.equalsIgnoreCase(ConstantsDtc.CONFIRM_APPOINTMENT)) {
            int counter = 0;
            int maxAttempts = Constants.ONE_HUNDRED;
            do {
                if (btnText.equalsIgnoreCase(ConstantsDtc.CONTINUE_TO_PAYMENT)) {
                    driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
                    List<WebElement> avsPopup = webDriver.findElements(addressVerificationBy);
                    driver.resetImplicitWaitToDefault();
                    if (avsPopup.size() > 0)
                        break;
                }
                counter++;
                if (counter%10 == 0) {
                    button = getButtonWithText(btnText, Constants.ONE);
                    driver.jsScrollToElementClick(button, false);
                }
                driver.waitForMilliseconds(Constants.ONE_HUNDRED);
            } while (getButtonWithText(btnText, Constants.ONE) != null && counter < maxAttempts);
        }
        if (btnText.equalsIgnoreCase(ConstantsDtc.CONTINUE_TO_PAYMENT) &&
                Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
            getDTDStoreCode();
            driver.scenarioData.genericData.putIfAbsent(Constants.SHIPPING_METHOD, CommonActions.defaultShippingOption);
        }
        LOGGER.info("clickButtonByText completed with text = '" + btnText + "'");
    }

    /**
     * Get the button that contains the specified text
     *
     * @param btnText        - The button text
     * @param numberAttempts - Number of attempts to find the button
     * @return
     */
    public WebElement getButtonWithText(String btnText, int numberAttempts) {
        LOGGER.info("getButtonWithText started with text = '" + btnText + "'");
        WebElement button = null;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        int counter = 0;
        List<By> buttonByElements = new ArrayList<>();
        buttonByElements.add(formSubmitButtonBy);
        buttonByElements.add(buttonBy);
        buttonByElements.add(dtButtonBy);
        buttonByElements.add(dtButtonLgBy);
        do {
            for (By buttonByElement : buttonByElements) {
                try {
                    button = driver.getElementWithText(buttonByElement, btnText);
                    if (button != null)
                        break;
                } catch (Exception e) {
                    // continue
                }
            }
            if (button == null)
                driver.waitForMilliseconds();
            counter++;
        } while (button == null && counter <= numberAttempts);
        driver.resetImplicitWaitToDefault();
        LOGGER.info("getButtonWithText completed with text = '" + btnText + "'");
        return button;
    }

    /**
     * Clicks button containing certain text
     *
     * @param text - Value of the button to be clicked
     */
    public void clickButtonByText(String text) {
        clickButtonByText(text, Constants.THIRTY);
    }

    /**
     * Selects option from bootstrap dropdown
     *
     * @param elementId The id of option list
     * @param value     The value to select
     */
    public void selectDropDownValue(String elementId, String value) {
        LOGGER.info("selectDropDownValue started");

        String js = "$('#" + elementId + "').trigger('mousedown');";
        JavascriptExecutor jse = (JavascriptExecutor) webDriver;

        try {
            driver.waitForElementClickable(webDriver.findElement(By.id(elementId)));
            WebElement webEle = webDriver.findElement(By.id(elementId));
            driver.jsScrollToElement(webEle);
            jse.executeScript(js);

            driver.waitForMilliseconds();

            //If no underlying "li" tags are found, trying finding "option" tags
            List<WebElement> options = webEle.findElements(By.tagName("li"));
            if (options.size() == 0) {
                options = webEle.findElements(By.tagName("option"));
            }

            for (WebElement option : options) {
                if (option.getText().toLowerCase().contains(value.toLowerCase())) {
                    driver.jsScrollToElement(option);
                    option.click();
                    break;
                }
            }
        } catch (Exception e) {
            Assert.fail("FAIL: Selecting value \"" + value + "\" from dropdown with ID \"" +
                    elementId + "\" FAILED with error: " + e);
        }
        LOGGER.info("selectDropDownValue completed");
    }

    /**
     * Asserts that taxes are listed on the page and contain/do not contain amount
     *
     * @param page     The page to verify
     * @param amount   The amount to check (Note that it expects $. For example $0.)
     * @param contains Check if element contains or does not contain amount
     */
    //TODO: change to assertEquals once we get unique auto classnames
    public void confirmTaxes(String page, String amount, boolean contains) {
        LOGGER.info("confirmTaxes started");
        driver.waitForPageToLoad();
        if (Config.isSafari())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        try {
            WebElement taxEl = getTaxElement(page);
            String taxRowString = taxEl.getText().trim();
            Assert.assertTrue("FAIL: \"" + ConstantsDtc.TAX + "\" was NOT displayed!",
                    taxRowString.contains(ConstantsDtc.TAX));
            LOGGER.info("Confirmed that " + ConstantsDtc.TAX + " was displayed.");
            Assert.assertTrue("FAIL: \"$\" was NOT displayed!", taxRowString.contains("$"));
            LOGGER.info("Confirmed that " + taxRowString + " contains $.");

            String itemsPrice = getItemsPriceElement(page).getText().trim();
            if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
                String salesTax = String.valueOf(Math.floor(getCalculatedSalesTaxForDTCRegion(itemsPrice) * 100) / 100);
                Assert.assertTrue("FAIL: The tax did NOT contain amount: \"" + salesTax + "\"!",
                        taxRowString.contains(salesTax));
            } else {
                if (contains) {
                    Assert.assertTrue("FAIL: The tax did NOT contain amount: \"" + amount + "\"!",
                            taxRowString.contains(amount));
                    LOGGER.info("Confirmed that tax contains amount " + amount + ".");
                } else {
                    Assert.assertTrue("FAIL: The tax DID contain amount: \"" + amount + "\"!",
                            !taxRowString.contains(amount));
                    LOGGER.info("Confirmed that tax did not contain amount " + amount + ".");
                }
            }
        } catch (Exception e) {
            Assert.fail("FAIL: Confirming taxes FAILED with error: " + e);
        }
        LOGGER.info("confirmTaxes completed");
    }

    /**
     * Gets the tax element from the page
     *
     * @param page The page to verify
     * @return WebElement that contains either Summary Subtotal or Summary Row
     */
    private WebElement getTaxElement(String page) {
        LOGGER.info("getTaxElement started");
        if (page.equalsIgnoreCase(Constants.ORDER)) {
            driver.waitForElementClickable(OrderPage.orderCartSummaryBy);
            LOGGER.info("getTaxElement completed");
            return driver.getElementWithText(OrderPage.orderCartSummaryBy, ConstantsDtc.TAX);
        } else {
            driver.waitForElementClickable(CheckoutPage.salesTaxRowBy);
            LOGGER.info("getTaxElement completed");
            return driver.getElementWithText(CheckoutPage.salesTaxRowBy, ConstantsDtc.TAX);
        }
    }

    /**
     * Gets the Cart Summary Items Price element from the page
     *
     * @param page The page to verify
     * @return WebElement that contains Cart Summary Items Price
     */
    private WebElement getItemsPriceElement(String page) {
        LOGGER.info("getTaxElement started");
        if (page.equalsIgnoreCase(Constants.ORDER)) {
            driver.waitForElementClickable(CommonActions.cartSubtotal);
            LOGGER.info("getItemsPriceElement completed");
            return CommonActions.cartSubtotal;
        } else {
            driver.waitForElementClickable(CheckoutPage.checkoutSubtotal);
            LOGGER.info("getItemsPriceElement completed");
            return CheckoutPage.checkoutSubtotal;
        }
    }

    /**
     * Gets the calculated sales tax based 3 tax factor
     *
     * @param price cart summary items price
     * @return Double that contains calculated sales price
     */
    public double getCalculatedSalesTaxForDTCRegion(String price) {
        LOGGER.info("getCalculatedSalesTaxForDTCRegion started");
        double stateTax = Double.parseDouble(stateTaxString);
        double cityTax = Double.parseDouble(cityTaxString);
        double otherTax = Double.parseDouble(otherTaxString);
        double calculatedSalesTax = 0.0;

        stateTax = twoDForm(stateTax * cleanMonetaryStringToDouble(price), 2);
        cityTax = twoDForm(cityTax * cleanMonetaryStringToDouble(price), 2);
        otherTax = twoDForm(otherTax * cleanMonetaryStringToDouble(price), 2);
        calculatedSalesTax = twoDForm((stateTax + cityTax + otherTax), 2);

        LOGGER.info("getCalculatedSalesTaxForDTCRegion completed");
        return calculatedSalesTax;
    }

    /**
     * Asserts that fees are listed on the page and contain/do not contain amount
     *
     * @param page     The page to pull the fee from
     * @param amount   The amount to check (Note that it expects $. For example $0.)
     * @param contains Check if element contains or does not contain amount
     */
    //TODO: change to assertEquals once we get unique auto classnames
    public void confirmFees(String page, String amount, boolean contains) {
        LOGGER.info("confirmFees started");
        try {
            WebElement feeEl = getFeeElement(page);
            String expectedText;
            String feeRowString = feeEl.getText().trim();

            if (page.equalsIgnoreCase(Constants.ORDER)) {
                expectedText = ConstantsDtc.ENVIRONMENTAL_FEE;
            } else {
                expectedText = ConstantsDtc.ABBREV_ENVIRONMENTAL_FEE;
            }

            Assert.assertTrue("FAIL: \"" + expectedText + "\" was NOT displayed!",
                    feeRowString.contains(expectedText));
            LOGGER.info("Confirmed that " + expectedText + " was displayed.");
            Assert.assertTrue("FAIL: Fee did NOT contain symbol: \"$\"!", feeRowString.contains("$"));
            LOGGER.info("Confirmed that " + feeRowString + " contains $.");
            if (contains) {
                Assert.assertTrue("FAIL: Actual fee: \"" + feeRowString
                                + "\" did NOT contain or match the expected: \"" + amount + "\"!",
                        feeRowString.contains(amount));
                LOGGER.info("Confirmed that fee contains amount " + amount + ".");
            } else {
                Assert.assertTrue("FAIL: Actual fee: \"" + feeRowString
                        + "\" DID contain or match the expected: \"" + amount + "\"!", !feeRowString.contains(amount));
                LOGGER.info("Confirmed that fee did not contain amount " + amount + ".");
            }
        } catch (Exception e) {
            Assert.fail("FAIL: Confirming fees FAILED with error: " + e);
        }
        LOGGER.info("confirmFees completed");
    }

    /**
     * Gets fee element from page
     *
     * @param page String used to determine which element to search page for
     */
    private WebElement getFeeElement(String page) {
        LOGGER.info("getFeeElement started");
        if (page.equalsIgnoreCase(Constants.ORDER)) {
            LOGGER.info("getFeeElement completed");
            return driver.getElementWithText(OrderPage.feeInfoBy, ConstantsDtc.ENVIRONMENTAL_FEE);
        } else {
            LOGGER.info("getFeeElement completed");
            return driver.getElementWithText(CheckoutPage.feeDetailsBy, ConstantsDtc.ABBREV_ENVIRONMENTAL_FEE);
        }
    }

    /**
     * Asserts that the current url matches base url with path or contains path
     *
     * @param path     The path of the url to verify
     * @param contains Boolean for URL vs partial URL
     */
    public void confirmCurrentUrl(String path, boolean contains) {
        LOGGER.info("confirmCurrentUrl started");
        driver.waitForMilliseconds();
        String currentUrl = webDriver.getCurrentUrl();
        if (contains) {
            Assert.assertTrue("FAIL: Current page URL (" + currentUrl + ") did not contain: " + path,
                    currentUrl.contains(path));
        } else {
            Assert.assertTrue("FAIL: Current page URL (" + currentUrl + ") did not match to: " + path,
                    currentUrl.equalsIgnoreCase(path));
        }
        LOGGER.info("confirmCurrentUrl completed");
    }

    /**
     * Waits for url to contain path
     *
     * @param path    The path of the url to wait for
     * @param seconds Integer of seconds for driver wait
     */
    public void waitForUrl(final String path, int seconds) {
        LOGGER.info("waitForUrl started");
        WebDriverWait wait = new WebDriverWait(webDriver, seconds);
        ExpectedCondition e = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return (d.getCurrentUrl().contains(path));
            }
        };

        try {
            wait.until(e);
        } catch (Exception exception) {
            Assert.fail("FAIL: Url after \"" + seconds + "\" seconds remained \"" + webDriver.getCurrentUrl() +
                    "\" and never ended up containing \"" + path + "\"!");
        }
        LOGGER.info("Confirmed that current URL now contains '" + path + "'.");
        LOGGER.info("waitForUrl completed");
    }

    /**
     * Asserts that the expected header is displayed on the page
     *
     * @param header The expected header text of the page
     */
    public void assertPageHeader(String header) {
        LOGGER.info("assertPageHeader started with '" + header + "' header");
        boolean valid = false;
        int counter = 0;
        int maxAttempts = Constants.TEN;
        do {
            valid = assertPageHeader(header, headerBy);
            if (!valid)
                valid = assertPageHeader(header, headerSecondBy);
            if (!valid)
                valid = assertPageHeader(header, headerThirdBy);
            if (!valid)
                valid = assertPageHeader(header, headerHeadlineBy);
            if (!valid)
                valid = assertPageHeader(header, headerFourthBy);
            if (!valid)
                driver.waitOneSecond();
            counter++;
        } while (!valid && counter < maxAttempts);
        Assert.assertTrue("FAIL: The expected page header '" + header + "' was not displayed", valid);
        LOGGER.info("assertPageHeader completed with '" + header + "' header");
    }

    /**
     * Asserts that the expected header is displayed on the page
     *
     * @param header       The header text
     * @param pageHeaderBy The By element representing the header
     * @return true or false whether the header element was displayed with the specified text
     */
    private boolean assertPageHeader(String header, By pageHeaderBy) {
        LOGGER.info("assertPageHeader started");
        boolean found = false;
        ArrayList<String> window = new ArrayList<String>(webDriver.getWindowHandles());
        if (window.size() > 1)
            webDriver.switchTo().window(window.get(window.size() - 1));
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> headers = webDriver.findElements(pageHeaderBy);
        if (headers.size() != 0) {
            for (WebElement pageHeader : headers) {
                if (pageHeader.getText().toLowerCase().contains(header.toLowerCase())) {
                    found = true;
                    break;
                }
            }
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("assertPageHeader completed");
        return found;
    }

    /**
     * Asserts that the expected text is displayed on the page
     *
     * @param by           The By element of the page
     * @param textToSearch The header (subtext) of the page
     */
    public void assertPageElementTextByElement(By by, String textToSearch) {
        LOGGER.info("assertPageElementTextByElement started");
        Set allHandles = webDriver.getWindowHandles();
        String mainHandle = webDriver.getWindowHandle();
        int count = allHandles.size();
        driver.waitForElementVisible(by);
        if (count == 1) {
            Assert.assertTrue("FAIL: \"" + textToSearch + "\" was NOT displayed on the page!",
                    driver.checkIfElementContainsText(by, textToSearch));
        } else {
            Iterator iter = allHandles.iterator();
            while (iter.hasNext()) {
                String popupHandle = iter.next().toString();
                if (!popupHandle.contains(mainHandle)) {
                    webDriver.switchTo().window(popupHandle);
                    Assert.assertTrue("FAIL: \"" + textToSearch + "\" was NOT displayed on the page!",
                            driver.checkIfElementContainsText(by, textToSearch));
                    webDriver.switchTo().window(mainHandle);
                }
            }
        }
        LOGGER.info("Confirmed that the element contains \"" + textToSearch + "\".");
        LOGGER.info("assertPageElementTextByElement completed");
    }

    /**
     * Asserts whether the current page title matches or not
     *
     * @param title The Title (title element text) of the page
     */
    public void assertPageTitle(String title) {
        LOGGER.info("assertPageTitle started");
        int time = Constants.THIRTY;
        String script = "return document.getElementsByTagName('title')[0].textContent.indexOf('" + title + "')>-1;";
        boolean foundTitle = driver.pollUntil(script, time);
        Assert.assertTrue("FAIL: Title \"" + title + "\" was NOT displayed in \"" + time + "\" seconds!",
                foundTitle);
        LOGGER.info("Confirmed that the page title is \"" + title + "\".");
        LOGGER.info("assertPageTitle completed");
    }

    /**
     * Click ADD TO CART button and take the specified action on the Add To Cart popup
     *
     * @param action     The action to take on the Add To Cart popup
     * @param buttonType : "Add To Cart"
     */
    public void addToCart(String buttonType, String action) {
        LOGGER.info(buttonType + " and " + action + " started");
        clickAddToCartOrPackageButton(buttonType);
        selectActionOnAddToCartModal(action);
        LOGGER.info(buttonType + " and " + action + " completed");
    }

    /**
     * This function clicks on Add To Cart or Add To Package
     *
     * @param buttonType : Type can be "Add To Cart" or "Add To Package"
     */
    public void clickAddToCartOrPackageButton(String buttonType) {
        LOGGER.info("clickAddToCartOrPackageButton started");
        driver.waitForPageToLoad();
        waitForSpinner();
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        WebElement addToCartElement = null;
        boolean found = true;

        if (driver.isElementDisplayed(ProductListPage.staggeredOptionTabBy, Constants.ONE)) {
            if (driver.isElementDisplayed(ProductListPage.errorMessageBy, Constants.ONE)) {
                selectStaggeredTab(ConstantsDtc.FRONT);
                if (driver.isElementDisplayed(ProductListPage.errorMessageBy, Constants.ONE)) {
                    selectStaggeredTab(ConstantsDtc.REAR);
                }
            }
            Assert.assertFalse("FAIL: There are no PLP products for the size entered",
                    driver.isElementDisplayed(ProductListPage.errorMessageBy, Constants.ONE));
        }

        if (driver.isElementDisplayed(productListingPageContainer, Constants.ONE)) {
            addToCartElement = getTargetAddToCartButton(buttonType, null, null, null, false);
        } else {
            try {
                if (driver.checkIfElementContainsText(headerSecondBy, ConstantsDtc.CHECK_AVAILABILITY))
                    addToCartElement = driver.getDisplayedElement(addToCartProductAvailabilityBy, Constants.ZERO);
                else {
                    if (webDriver.getCurrentUrl().contains(ConstantsDtc.COMPARE_PRODUCT_URL_SEGMENT)) {
                        int productCount = driver.scenarioData.productInfoList.size();
                        WebElement firstProductSection = webDriver.findElements(compareProductColumnBy).get(1);
                        String text = firstProductSection.getText();
                        for (int index = 0; index < productCount; index++) {
                            if (CommonUtils.containsIgnoreCase(text, productInfoListGetValue(ConstantsDtc.BRAND, index)) &&
                                    CommonUtils.containsIgnoreCase(text, productInfoListGetValue(ConstantsDtc.PRODUCT, index)) &&
                                    CommonUtils.containsIgnoreCase(text, productInfoListGetValue(Constants.PRICE, index))) {
                                productInfoListSetValue(productData.IN_CART, index, String.valueOf(true));
                                break;
                            }
                        }
                    } else if (webDriver.getCurrentUrl().contains(ConstantsDtc.WHEEL_CONFIGURATOR_URL_SEGMENT)) {
                        driver.jsScrollToElementClick(WheelConfiguratorPage.viewDetails);
                        extractAndSaveProductInfoOnPdpPage();
                        driver.browserNavigateBackAction();
                    } else {
                        extractAndSaveProductInfoOnPdpPage();
                    }
                    addToCartElement = webDriver.findElement(addToCartByTextBy);
                }
            } catch (Exception e) {
                found = false;
            }
        }

        Assert.assertTrue("FAIL: " + buttonType + " button was not displayed", found);
        driver.jsScrollToElementClick(addToCartElement);
        driver.resetImplicitWaitToDefault();
        LOGGER.info("clickAddToCartOrPackageButton completed");
    }

    /**
     * Get the product details from the PDP page and add it to the ProductInfoList hashmap
     */
    public void extractAndSaveProductInfoOnPdpPage() {
        LOGGER.info("extractAndSaveProductInfoOnPdpPage started");
        boolean staggered = false;
        String itemNumber = "";
        String frontRear = "";

        int productCount = 1;
        if (CommonUtils.containsIgnoreCase(webDriver.findElement(pdpInfoContainerBy).getText(), ConstantsDtc.REAR)) {
            staggered = true;
            productCount = 2;
        }

        for (int productIndex = Constants.ZERO; productIndex < productCount; productIndex++) {
            String quantity = "";
            String brand = webDriver.findElement(ProductDetailPage.productBrandBy).getText();
            String product = webDriver.findElement(ProductDetailPage.productNameBy).getText();
            String price = webDriver.findElements(CommonActions.productPriceBy).get(productIndex).getText();
            String inventoryMessage = webDriver.findElements(CommonActions.productLocatorAvailabilityBy).
                    get(productIndex).getText();
            if (packageFlow) {
                webDriver.findElements(CommonActions.packageProductQuantityBoxBy).get(productIndex)
                        .getText().equals("4");
            } else {
                quantity = webDriver.findElements(CommonActions.productQuantityBoxBy).get(productIndex).
                        getAttribute(Constants.VALUE);
            }
            if (driver.isElementDisplayed(itemCodeBy, Constants.ONE)) {
                itemNumber = webDriver.findElements(itemCodeBy).get(productIndex).getText().
                        replace(ConstantsDtc.ITEM_NUMBER_PREFIX, " ").trim();
            } else {
                itemNumber = webDriver.findElements(ProductDetailPage.pdpItemNumberBy).get(productIndex).getText().
                        replace(ConstantsDtc.ITEM_NUMBER_PREFIX, " ").trim();
            }

            if (staggered) {
                if (productIndex == 0) {
                    frontRear = ConstantsDtc.FRONT;
                } else {
                    frontRear = ConstantsDtc.REAR;
                }
            }
            addProductInfoListItem(brand, product, price, inventoryMessage, itemNumber, quantity, frontRear, true);
        }
        LOGGER.info("extractAndSaveProductInfoOnPdpPage completed");
    }

    /**
     * Selects either View Cart or Continue Shopping or close shopping cart popup
     *
     * @param action The action to take on the Add To Cart popup
     */
    public void selectActionOnAddToCartModal(String action) {
        LOGGER.info("selectActionOnAddToCartModal started");
        clickContinueWithThisVehicleButton();
        By addToCartModalContainerBy = dtAddToCartModalContainerBy;
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            addToCartModalContainerBy = dtdAddToCartModalContainerBy;
        }
        int attemptCount = Constants.ZERO;
        int maxAttempts = Constants.TWENTY;
        while (!driver.isElementDisplayed(addToCartModalContainerBy, Constants.ZERO) && attemptCount < maxAttempts) {
            driver.waitOneSecond();
            attemptCount++;
        }
        Assert.assertTrue("FAIL: The Add to Cart popup modal was not displayed after " + maxAttempts + " seconds",
                driver.isElementDisplayed(addToCartModalContainerBy, Constants.ZERO));
        if ((action.toLowerCase().contains(ConstantsDtc.CONTINUE_SHOPPING.toLowerCase()) &&
                !addToCartSecondary.getText().equalsIgnoreCase(ConstantsDtc.CONTINUE_SHOPPING)) ||
                CommonUtils.containsIgnoreCase(action, Constants.CLOSE)) {
            closeModalWindow();
            return;
        }
        driver.setImplicitWait(Constants.FIVE, TimeUnit.SECONDS);
        List<WebElement> webButtonLinks = webDriver.findElement(addToCartModalContainerBy).findElements(anchorTagBy);
        driver.resetImplicitWaitToDefault();
        boolean found = false;
        for (WebElement webButtonLink : webButtonLinks) {
            try {
                if (CommonUtils.containsIgnoreCase(webButtonLink.getText(), action)) {
                    driver.webElementClick(webButtonLink);
                    found = true;
                    break;
                }
            } catch (Exception e) {
                // continue
            }
        }
        Assert.assertTrue("FAIL: " + action + " button was not displayed on Add to Cart popup modal.", found);
        if (action.equalsIgnoreCase(ConstantsDtc.VIEW_SHOPPING_CART))
            setStaggeredNonStaggeredFitmentOnShoppingCart();

        driver.waitForMilliseconds();
        LOGGER.info("selectActionOnAddToCartModal completed");
    }

    /**
     * Return the Add To Cart that meets the specified criteria and is enabled
     *
     * @param option        The option in results to pick from (Ex. SETS, FRONT, REAR)
     * @param itemId        The itemid to identify which addToCart button to click
     * @param inStock       Criteria to select an item based on inStock availability set to ("true" or "false" or "none")
     * @param imageRequired true or false whether the product row must have a valid image to be selected
     * @param plpPageType   page type can be "Add To Cart" or "Add To Package"
     * @return WebElement for enabled Add To Cart button
     */
    public WebElement getTargetAddToCartButton(String plpPageType, String option, String itemId, String inStock, boolean imageRequired) {
        LOGGER.info("getTargetAddToCartButton started");
        WebElement returnElement = null;
        boolean found = false;
        clickContinueWithThisVehicleButton();
        selectProductListTab(option);
        navToDifferentPageOfResults(Constants.FIRST);

        packageFlow = plpPageType.equalsIgnoreCase(ConstantsDtc.ADD_TO_PACKAGE);
        if (option != null && !option.equalsIgnoreCase(Constants.NONE))
            selectProductListTab(option);

        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        do {
            driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
            List<WebElement> rows = webDriver.findElements(ProductListPage.plpResultsRowBy);
            int i = 0;
            for (WebElement row : rows) {
                boolean validRow = true;
                if (CommonUtils.containsIgnoreCase(row.getText(), ConstantsDtc.PRICE_IN_CART) ||
                        row.findElement(productPriceBy).getAttribute(Constants.CLASS).
                                contains(ConstantsDtc.PRICE_IS_MESSAGE_CLASS_SEGMENT)) {
                    validRow = false;
                }
                if (itemId != null && !row.getText().contains(itemId)) {
                    validRow = false;
                }
                if (validRow && inStock != null) {
                    try {
                        boolean zeroInStock = row.findElement(CommonActions.inventoryMessageBy).getText().
                                contains(ConstantsDtc.ORDER_NOW);
                        if (Boolean.parseBoolean(inStock) && zeroInStock || !Boolean.parseBoolean(inStock) && !zeroInStock) {
                            validRow = false;
                        }
                    } catch (Exception e) {
                        validRow = false;
                    }
                }
                if (validRow && !(CommonUtils.containsIgnoreCase(row.getText(), ConstantsDtc.ADD_TO_CART) ||
                        CommonUtils.containsIgnoreCase(row.getText(), ConstantsDtc.ADD_TO_PACKAGE))) {
                    if (CommonUtils.containsIgnoreCase(row.getText(), ConstantsDtc.VIEW_DETAILS)) {
                        WebElement viewDetailsBtn = row.findElement(ProductListPage.viewDetailsButtonBy);
                        if (viewDetailsBtn.isEnabled()) {
                            viewDetailsBtn.click();
                            selectAllFirstAvailableSizeOptionsOnPdpPage();
                            extractAndSaveProductInfoOnPdpPage();
                            if (plpPageType.equalsIgnoreCase(ConstantsDtc.ADD_TO_CART)) {
                                returnElement = webDriver.findElement(addToCartByTextBy);
                            } else if (plpPageType.equalsIgnoreCase(ConstantsDtc.ADD_TO_PACKAGE)) {
                                returnElement = webDriver.findElement(addToPackageBy);
                            }
                            found = true;
                            break;
                        } else {
                            //TODO: Will have to throw a conditional error here if itemID is specified once we get the
                            //TODO (cont'd): product API. This will allow us to add the specified item to cart from
                            //TODO (cont'd): PDP via View Details from PLP.
                        }
                    }
                    validRow = false;
                }

                if (!validRow) {
                    i++;
                    continue;
                }

                WebElement addToCartOrPackageButton = null;
                if (plpPageType.equalsIgnoreCase(ConstantsDtc.ADD_TO_CART)) {
                    addToCartOrPackageButton = row.findElement(CommonActions.addToCartByClassBy);
                } else if (plpPageType.equalsIgnoreCase(ConstantsDtc.ADD_TO_PACKAGE)) {
                    addToCartOrPackageButton = row.findElement(CommonActions.addToPackageBy);
                }
                boolean addToCartButtonEnabled = driver.isElementClickable(addToCartOrPackageButton, Constants.ONE);
                if (validRow && addToCartButtonEnabled) {
                    if (imageRequired) {
                        WebElement imageParent = driver.getParentElement(row.findElement(ProductListPage.productImageBy));
                        if (imageParent.getAttribute(Constants.CLASS).contains(ProductListPage.PRODUCT_LIST_IMAGE_MISSING))
                            validRow = false;
                    }
                    if (validRow) {
                        appendToOrRemoveFromProductInfoList(row, i, true, Constants.APPEND);
                        returnElement = addToCartOrPackageButton;
                        found = true;
                        break;
                    }
                }
                i++;
            }
        } while (!found && navToDifferentPageOfResults(Constants.NEXT));

        // TODO: Developers will expose the product API so we can apply this when itemId or inStock requirements are specified.
        if (!found && itemId == null && inStock == null) {
            navToDifferentPageOfResults(Constants.FIRST);
            clickFirstViewDetailsButton(false);
            selectAllFirstAvailableSizeOptionsOnPdpPage();
            extractAndSaveProductInfoOnPdpPage();
            if (plpPageType.equalsIgnoreCase(ConstantsDtc.ADD_TO_CART)) {
                returnElement = webDriver.findElement(addToCartByTextBy);
            } else if (plpPageType.equalsIgnoreCase(ConstantsDtc.ADD_TO_PACKAGE)) {
                returnElement = webDriver.findElement(addToPackageBy);
            }
            found = true;
        }

        String assertMessage = "FAIL: An enabled ADD TO CART button could not be found";
        if (itemId != null || inStock != null) {
            assertMessage = assertMessage + " for the specified criteria: ";
            if (itemId != null) {
                assertMessage = assertMessage + "ItemID = '" + itemId + "'. ";
            }
            if (inStock != null) {
                assertMessage = assertMessage + "In Stock = '" + inStock + "'. ";
            }
        }

        int counter = 0;
        String price = webDriver.findElement(CommonActions.productPriceBy).getText();
        while (price.contains("-") && counter < Constants.TEN) {
            driver.waitOneSecond();
            price = webDriver.findElement(CommonActions.productPriceBy).getText();
            counter++;
        }

        Assert.assertTrue(assertMessage, found);
        driver.resetImplicitWaitToDefault();
        LOGGER.info("getTargetAddToCartButton completed");
        return returnElement;
    }

    /**
     * Select all the first available size options for tire or wheel on the PDP page
     */
    public void selectAllFirstAvailableSizeOptionsOnPdpPage() {
        LOGGER.info("selectAllFirstAvailableSizeOptionsOnPdpPage started");
        String url = webDriver.getCurrentUrl();
        if (CommonUtils.containsIgnoreCase(url, ProductDetailPage.BUY_TIRES)) {
            chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.WIDTH, Constants.TIRE);
            chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.RATIO, Constants.TIRE);
            chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.DIAMETER, Constants.TIRE);
        } else if (CommonUtils.containsIgnoreCase(url, ProductDetailPage.BUY_WHEELS)) {
            chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.DIAMETER, Constants.WHEEL);
            chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.WIDTH, Constants.WHEEL);
            chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.BOLT_PATTERN, Constants.WHEEL);
            chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.OFFSET, Constants.WHEEL);
            chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.VENDOR_PRODUCT_NUMBER, Constants.WHEEL);
        } else {
            Assert.fail("The PDP page did not display");
        }
        chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.LOAD_RANGE, Constants.TIRE);
        chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.LOAD_INDEX_WITH_RATING_KEY, Constants.TIRE);
        chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.SIDEWALL_OPTION, Constants.TIRE);
        chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.OE_DESIGNATION, Constants.TIRE);
        chooseValueFromPdpDropdown(Constants.FIRST, ConstantsDtc.VENDOR_PRODUCT_NUMBER, Constants.TIRE);
        LOGGER.info("selectAllFirstAvailableSizeOptionsOnPdpPage completed");
    }

    /**
     * Select the dropdown value from Width, Ratio or Diameter dropdown PDP page
     *
     * @param dropDownValue Option which should be selected from dropdown
     * @param dropdown      dropdown name to be selected
     * @param category      like Tyre or wheel
     */
    public void chooseValueFromPdpDropdown(String dropDownValue, String dropdown, String category) {
        LOGGER.info("chooseValueFromPdpDropdown started for element: " + dropdown + " for " + category);
        waitForSpinner();
        String dropDownCategory = category.toLowerCase();
        String dropDownType = dropdown.substring(0, 1).toUpperCase() + dropdown.substring(1);
        By dropDownElementBy;
        if (dropdown.equalsIgnoreCase(ConstantsDtc.VENDOR_PRODUCT_NUMBER)) {
            dropDownElementBy = By.xpath("//input[@name = '" + ConstantsDtc.MANUFACTURE_AID.replace(" ", "") +
                    "']/following-sibling::div/div" + "[@class='react-selectize-toggle-button-container']");
        } else {
            dropDownElementBy = By.xpath("//input[@name = '" + dropDownCategory + dropDownType.replace(" ", "") +
                    "']/following-sibling::div/div" + "[@class='react-selectize-toggle-button-container']");
        }
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        try {
            if (driver.isElementVisible(webDriver.findElement(dropDownElementBy), Constants.ONE)) {
                selectPdpDropdownValue(dropDownElementBy, dropDownValue);
            } else {
                LOGGER.info("'" + dropdown + "' dropdown not displayed.");
            }
        } catch (Exception e) {
            // continue
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("chooseValueFromPdpDropdown completed for element: " + dropdown + " for " + category);
    }

    /**
     * Determine whether a size dimension has been set
     *
     * @param category     Product category: "Tire" or "Wheel"
     * @param dropdownType The size dropdown: "Width", "Ratio", "Diameter", "Bolt Pattern", or "Offset"
     * @return true or false whether the category has been set
     */
    public boolean sizeCategoryValueIsSet(String category, String dropdownType) {
        LOGGER.info("sizeCategoryValueIsSet started for '" + category + " " + dropdownType + "'");
        boolean sizeSpecificationIsSet = true;
        if (getSizeSpecificationValue(category, dropdownType).contains("-"))
            sizeSpecificationIsSet = false;
        LOGGER.info("sizeCategoryValueIsSet completed for '" + category + " " + dropdownType + "'");
        return sizeSpecificationIsSet;
    }

    /**
     * Get the Specifications row value for a size dimension based on whether the product is a tire or wheel
     *
     * @param category     Product category: "Tire" or "Wheel"
     * @param dropdownType The size dropdown: "Width", "Ratio", "Diameter", "Bolt Pattern", or "Offset"
     * @return The Specification row value
     */
    public String getSizeSpecificationValue(String category, String dropdownType) {
        LOGGER.info("getSizeSpecificationValue started for '" + category + " " + dropdownType + "'");
        String rowSizeLabel = getSpecificationsRowLabel(category, dropdownType);
        WebElement sizeSpecificationRow = driver.getElementWithText(ProductDetailPage.productDetailSpecificationsRowBy,
                rowSizeLabel);
        LOGGER.info("getSizeSpecificationValue completed for '" + category + " " + dropdownType + "'");
        return sizeSpecificationRow.findElement(ProductDetailPage.productDetailSpecificationsRowValueBy).getText();
    }

    /**
     * Get the Specifications row label for a size dimension based on whether the product is a tire or wheel
     *
     * @param category     Product category: "Tire" or "Wheel"
     * @param dropdownType The size dropdown: "Width", "Ratio", "Diameter", "Bolt Pattern", or "Offset"
     * @return The Specification row label
     */
    public String getSpecificationsRowLabel(String category, String dropdownType) {
        LOGGER.info("getSpecificationsRowLabel started for '" + category + " " + dropdownType + "'");
        String rowSizeLabel = "";
        switch (category) {
            case Constants.TIRE:
                switch (dropdownType) {
                    case ConstantsDtc.WIDTH:
                        rowSizeLabel = ConstantsDtc.SECTION_WIDTH;
                        break;
                    case ConstantsDtc.RATIO:
                        rowSizeLabel = ConstantsDtc.ASPECT_RATIO;
                        break;
                    case ConstantsDtc.DIAMETER:
                        rowSizeLabel = ConstantsDtc.RIM_DIAMETER;
                        break;
                    case ConstantsDtc.VENDOR_PRODUCT_NUMBER:
                        rowSizeLabel = ConstantsDtc.VPN;
                        break;
                    default:
                        Assert.fail("The '" + dropdownType + "' is not valid for '" + category + "'");
                        break;
                }
                break;
            case Constants.WHEEL:
                switch (dropdownType) {
                    case ConstantsDtc.DIAMETER:
                        rowSizeLabel = ConstantsDtc.RIM_DIAMETER;
                        break;
                    case ConstantsDtc.WIDTH:
                        rowSizeLabel = ConstantsDtc.RIM_WIDTH;
                        break;
                    case ConstantsDtc.BOLT_PATTERN:
                    case ConstantsDtc.OFFSET:
                        rowSizeLabel = dropdownType;
                        break;
                    case ConstantsDtc.VENDOR_PRODUCT_NUMBER:
                        rowSizeLabel = ConstantsDtc.VPN;
                        break;
                    default:
                        Assert.fail("The '" + dropdownType + "' is not valid for '" + category + "'");
                        break;
                }
                break;
            default:
                Assert.fail("The '" + category + "' category is not valid for assessing size specification on PDP");
                break;
        }
        LOGGER.info("getSpecificationsRowLabel completed for '" + category + " " + dropdownType + "'");
        return rowSizeLabel;
    }

    /**
     * Select the dropdown value from dropdown PDP page
     *
     * @param dropDownListBoxBy     Dropdown list box
     * @param dropDownValue         Value to be be selected from dropdown
     */
    public void selectPdpDropdownValue(By dropDownListBoxBy, String dropDownValue) {
        LOGGER.info("selectPdpDropdownValue Started for " + dropDownValue);
        webDriver.findElement(dropDownListBoxBy).click();
        boolean optionFound = false;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> dropOptions = webDriver.findElements(CommonActions.dropdownOptionBy);
        driver.resetImplicitWaitToDefault();
        if (CommonUtils.containsIgnoreCase(dropDownValue, Constants.FIRST)) {
            int listCount = dropOptions.size();
            int counter = 0;
            do {
                WebElement dropOption = dropOptions.get(counter);
                dropOptions = webDriver.findElements(CommonActions.dropdownOptionBy);
                String style = dropOption.getAttribute(ConstantsDtc.ATTR_STYLE);
                String optionValue = dropOption.getText();
                LOGGER.info("DropDown option: " + optionValue + ". Style: " + style);
                if (style.isEmpty()) {
                    driver.clickElementWithText(dropdownOptionBy, optionValue);
                    driver.waitOneSecond();
                    String price = webDriver.findElement(CommonActions.productPriceBy).getText();
                    if (!price.equals("$0.00") && !price.equalsIgnoreCase(ConstantsDtc.NO_PRICE))
                        break;
                    webDriver.findElement(dropDownListBoxBy).click();
                    dropOptions = webDriver.findElements(CommonActions.dropdownOptionBy);
                }
                counter++;
            } while (counter < listCount);
        } else {
            for (WebElement dropoption : dropOptions) {
                String optionValue = dropoption.getText();
                if (CommonUtils.containsIgnoreCase(optionValue, dropDownValue)) {
                    dropoption.findElement(By.xpath("//span[contains(., \"" + dropDownValue + "\")]")).click();
                    optionFound = true;
                    break;
                }
            }
            Assert.assertTrue("FAIL: The PDP dropdown did not have the specified value", optionFound);
        }
        driver.waitForPageToLoad();
        LOGGER.info("selectPdpDropdownValue completed for " + dropDownValue);
    }


    /**
     * Select the dropdown value from Width, Ratio or Diameter dropdown PDP page
     *
     * @param width    Option to be verified with actual Dropdown option
     * @param ratio    Option to be verified with actual Dropdown option
     * @param diameter Option to be verified with actual Dropdown option
     */
    public void verifyPdpTireSizeDropdownValues(String width, String ratio, String diameter) {
        LOGGER.info("verifyPdpTireSizeDropdownValues started for options: " + width + "  " + ratio +
                " and " + diameter);
        waitForSpinner();
        Map<String, String> mapDropDown = new HashMap<String, String>();
        mapDropDown.put(ConstantsDtc.WIDTH, width);
        mapDropDown.put(ConstantsDtc.RATIO, ratio);
        mapDropDown.put(ConstantsDtc.DIAMETER, diameter);
        String tireCategory = Constants.TIRE.toLowerCase();
        for (Map.Entry dropdown : mapDropDown.entrySet()) {
            By dropDownElementby = By.xpath("//input[@name = '" + tireCategory +
                    dropdown.getKey() + "']/following-sibling::div");
            WebElement dropDownElement = webDriver.findElement(dropDownElementby);
            Assert.assertTrue("FAIL: dropdown: " + dropdown.getKey() + " doesn't match the value selected ",
                    dropDownElement.getText().equals(dropdown.getValue()));
        }
        LOGGER.info("verifyPdpTireSizeDropdownValues completed for options: " + width + " for " + ratio +
                " and " + diameter);
    }

    /**
     * Verifies that all of the tire size dropdown filters are populated
     */
    public void verifyPdpTireSizeDropDownsArePopulated() {
        LOGGER.info("verifyPdpTireSizeDropDownsArePopulated started");
        ArrayList<String> dropDowns = new ArrayList<>();
        dropDowns.add(ConstantsDtc.WIDTH);
        dropDowns.add(ConstantsDtc.RATIO);
        dropDowns.add(ConstantsDtc.DIAMETER);
        dropDowns.add(ConstantsDtc.LOAD_INDEX_WITH_RATING_KEY);
        dropDowns.add(CommonUtils.removeSpaces(ConstantsDtc.LOAD_RANGE));
        dropDowns.add(CommonUtils.removeSpaces(ConstantsDtc.SIDEWALL_OPTION));
        String tireCategory = Constants.TIRE.toLowerCase();
        for (String dropdown : dropDowns) {
            By dropDownElementby = By.xpath("//input[@name = '" + tireCategory + dropdown +
                    "']/following-sibling::div");
            WebElement dropDownElement = webDriver.findElement(dropDownElementby);
            Assert.assertFalse("FAIL: Tire size dropdown: " + dropdown + " is not populated ",
                    dropDownElement.getText().equals(dropdown) || dropDownElement.getText().isEmpty());
            LOGGER.info("Tire " + dropdown + " dropdown = " + dropDownElement.getText());
        }
        LOGGER.info("verifyPdpTireSizeDropDownsArePopulated completed");
    }

    /**
     * Verify the values exist in the width and diameter dropdown boxes
     *
     * @param width    The width of the wheel
     * @param diameter The diameter of the wheel
     */
    public void verifyDropDownValuesForWheels(String width, String diameter) {
        LOGGER.info("verifyDropDownValuesForWheels started for options: " + width + " and " + diameter);
        driver.waitForPageToLoad(Constants.THREE_THOUSAND);
        Map<String, String> mapDropDown = new HashMap<String, String>();
        mapDropDown.put(ConstantsDtc.WIDTH, width);
        mapDropDown.put(ConstantsDtc.DIAMETER, diameter);
        String WheelCategory = Constants.WHEEL.toLowerCase();
        for (Map.Entry dropdown : mapDropDown.entrySet()) {
            By dropDownElementby = By.xpath("//input[@name = '" + WheelCategory + dropdown.getKey() + "']/following-sibling::div");
            WebElement dropDownElement = webDriver.findElement(dropDownElementby);
            Assert.assertTrue("FAIL: dropdown: " + dropdown.getKey() + " doesn't match the value selected ",
                    CommonUtils.containsIgnoreCase(dropDownElement.getText(), dropdown.getValue().toString()));
        }
        LOGGER.info("verifyDropDownValuesForWheels completed for options: " + width + " and " + diameter);
    }

    /**
     * Clicks the first available View Details button for a product containing a valid image
     *
     * @param imageRequired true or false whether a valid image on the product row is required for the VIEW DETAILS
     *                      to be selected
     */
    public void clickFirstViewDetailsButton(boolean imageRequired) {
        LOGGER.info("clickFirstViewDetailsButton started");
        driver.waitForPageToLoad(Constants.ZERO);
        waitForSpinner();
        boolean found = false;
        driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
        do {
            int i = 0;
            List<WebElement> rows = webDriver.findElement(ProductListPage.productListingBy).findElements(ProductListPage.plpResultsRowBy);
            while (rows.size() == 0) {
                driver.waitOneSecond();
                rows = webDriver.findElement(ProductListPage.productListingBy).findElements(ProductListPage.plpResultsRowBy);
            }
            for (WebElement row : rows) {
                if (!CommonUtils.containsIgnoreCase(row.getText(), ConstantsDtc.VIEW_DETAILS)) {
                    i++;
                    continue;
                }
                boolean validRow = true;
                if (imageRequired) {
                    WebElement imageParent = driver.getParentElement(row.findElement(ProductListPage.productImageBy));
                    if (imageParent.getAttribute(Constants.CLASS).contains(ProductListPage.PRODUCT_LIST_IMAGE_MISSING))
                        validRow = false;
                }
                if (validRow) {
                    try {
                        WebElement viewDetailsBtn = row.findElement(ProductListPage.viewDetailsButtonBy);
                        if (driver.isElementDisplayed(viewDetailsBtn, Constants.ONE) && viewDetailsBtn.isEnabled()) {
                            appendToOrRemoveFromProductInfoList(row, i, false, Constants.APPEND);
                            viewDetailsBtn.click();
                            found = true;
                            break;
                        }
                    } catch (Exception e) {
                        // continue to next iteration
                    }
                }
                i++;
            }
        } while (!found && navToDifferentPageOfResults(Constants.NEXT));
        driver.resetImplicitWaitToDefault();
        Assert.assertTrue("FAIL: No PLP result row was found for selecting VIEW DETAILS when image is required = " +
                imageRequired, found);
        LOGGER.info("clickFirstViewDetailsButton completed");
    }

    /**
     * Click the View Details button for a specified product name
     *
     * @param productName The product name to search for on PLP page
     */
    public void clickViewDetailsButtonForProductOnPlpPage(String productName) {
        LOGGER.info("clickViewDetailsButtonForProductOnPlpPage started for '" + productName + "'");
        driver.waitForPageToLoad();
        boolean found = false;
        driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
        do {
            int i = 0;
            List<WebElement> rows = webDriver.findElement(ProductListPage.productListingBy).
                    findElements(ProductListPage.plpResultsRowBy);
            for (WebElement row : rows) {
                if (!CommonUtils.containsIgnoreCase(row.getText(), productName)) {
                    i++;
                    continue;
                }
                found = true;
                try {
                    WebElement viewDetailsBtn = row.findElement(ProductListPage.viewDetailsButtonBy);
                    if (driver.isElementDisplayed(viewDetailsBtn, Constants.ONE) && viewDetailsBtn.isEnabled()) {
                        appendToOrRemoveFromProductInfoList(row, i, false, Constants.APPEND);
                        viewDetailsBtn.click();
                        break;
                    } else {
                        Assert.fail("FAIL: View Details button was not available or not enabled for product '" +
                                productName + "'");
                    }
                } catch (Exception e) {
                    // continue
                }

            }
        } while (!found && navToDifferentPageOfResults(Constants.NEXT));
        driver.resetImplicitWaitToDefault();
        Assert.assertTrue("FAIL: No PLP result row was found containing product '" + productName + "'", found);
        LOGGER.info("clickViewDetailsButtonForProductOnPlpPage completed for '" + productName + "'");
    }

    /***
     * Appends the product data from the specified PLP row to the productInfoList
     *
     * @param row       WebElement containing the values to retrieve and append to the productInfoList
     * @param index     The row number on the PLP page
     * @param addToCart true/false whether the item is to be added to cart
     */
    public void appendToOrRemoveFromProductInfoList(WebElement row, int index, boolean addToCart, String appendRemove) {
        LOGGER.info("appendToOrRemoveFromProductInfoList started for row '" + index + 1 + "'");
        //TODO: Will need to revisit this if there are stale element issues with Safari
        String brand = "";
        String product = "";
        String price = "";
        String quantity = "";
        String itemNumber = "";
        String inventoryMessage = "";
        boolean staggered = false;
        String rowText = row.getText();
        String frontRear = "";
        int productCount = Constants.ONE;
        if (CommonUtils.containsIgnoreCase(rowText, ConstantsDtc.REAR)) {
            staggered = true;
            productCount = Constants.TWO;
        }

        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);

        for (int productIndex = Constants.ZERO; productIndex < productCount; productIndex++) {
            brand = row.findElement(ProductListPage.brandNameBy).getText();
            String[] productText = row.findElement(ProductListPage.productNameBy).getText().split("\n");
            product = productText[Math.min(productText.length - Constants.ONE, Constants.ONE)];
            try {
                price = row.findElements(productPriceBy).get(productIndex).getText();
            } catch (Exception e) {
                //continue
            }
            try {
                if (inventoryMessage.isEmpty()) {
                    inventoryMessage = row.findElements(productLocatorAvailabilityBy).
                            get(productIndex).getText().replace(", ", " ");
                }
            } catch (Exception e) {
                //continue
            }
            try {
                if (packageFlow) {
                    quantity = row.findElements(packageProductQuantityBoxBy).get(productIndex).getText();
                } else {
                    quantity = row.findElements(productQuantityBoxBy).get(productIndex).getAttribute(Constants.VALUE);
                }
            } catch (Exception e) {
                //continue
            }

            try {
                if (driver.isElementDisplayed(itemCodeBy, Constants.ONE))
                    itemNumber = row.findElements(itemCodeBy).get(productIndex).getText().
                            replace(ConstantsDtc.ITEM_NUMBER_PREFIX, " ").trim();
                else {
                    itemNumber = row.findElements(ProductListPage.plpItemNumberBy).get(productIndex).getText().
                            replace(ConstantsDtc.ITEM_NUMBER_PREFIX, " ").split("\n")[0].trim();
                }
            } catch (Exception e) {
                LOGGER.info("Item number not saved. " +
                        "This may be a canonical listing on the PLP page with a price range and no item ID");
            }

            if (staggered) {
                if (productIndex == 0)
                    frontRear = ConstantsDtc.FRONT;
                else
                    frontRear = ConstantsDtc.REAR;
            }
            if (appendRemove.equalsIgnoreCase(Constants.REMOVE)) {
                removeProductInfoListItem(brand, product, itemNumber, frontRear);
            } else {
                addProductInfoListItem(brand, product, price, inventoryMessage, itemNumber, quantity, frontRear, addToCart);
            }
        }
        LOGGER.info("appendToOrRemoveFromProductInfoList completed for row '" + index + 1 + "'");
    }

    /**
     * Handles the selection of the desired product list tab when sets are available (SETS, FRONT, REAR)
     *
     * @param tab The tab in results to pick from (Ex. SETS, FRONT, REAR)
     */
    private void selectProductListTab(String tab) {
        LOGGER.info("selectProductListTab started for tab: " + tab);
        if (tab == null || tab.equalsIgnoreCase(Constants.NONE))
            return;
        try {
            By tabBy = ProductListPage.resultsOptionLinkBy;

            if (Config.isMobile()) {
                tabBy = ProductListPage.resultsOptionLinkMobileBy;
            }

            List<WebElement> optionLinks = webDriver.findElements(tabBy);
            for (WebElement optionLink : optionLinks) {
                if (optionLink.getText().toUpperCase().contains(tab.toUpperCase())) {
                    driver.jsScrollToElement(optionLink);
                    optionLink.click();
                    break;
                }
            }
        } catch (java.util.NoSuchElementException e) {
            Assert.fail("FAIL: The desired product list tab: \"" + tab + "\" was NOT found and nothing was " +
                    "added to the cart!");
        }
        LOGGER.info("selectProductListTab completed for tab: " + tab);
    }

    /**
     * Clicks "View shopping Cart" button
     */
    public void clickViewShoppingCart() {
        LOGGER.info("clickViewShoppingCart started");
        if (Config.isSafari() || Config.isFirefox()) {
            driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        }
        driver.waitForMilliseconds();
        String linkText = ConstantsDtc.VIEW_SHOPPING_CART;
        driver.clickElementWithLinkText(linkText);
        LOGGER.info("clickViewShoppingCart completed");
    }

    /**
     * Clicks "Continue Shopping" button
     */
    public void clickContinueShopping() {
        LOGGER.info("clickContinueShopping started");
        driver.waitForElementClickable(addToCartSecondary);
        driver.jsScrollToElement(addToCartSecondary);
        addToCartSecondary.click();
        driver.waitForPageToLoad();
        LOGGER.info("clickContinueShopping completed");
    }

    /**
     * Returns element among multiple that contains text substring, if substring not found
     * returns first
     *
     * @param by   The element to check
     * @param text The text to check
     */
    public void assertElementWithTextIsVisible(By by, String text) {
        LOGGER.info("assertTextPresentInElement started");
        if (driver.getElementWithText(by, text) == null) {
            Assert.fail("FAIL: Element \"" + by.toString() + "\" with text \"" + text + "\" NOT present!");
        } else {
            Assert.assertTrue("FAIL: Unable to find a displayed element with By = \"" + by
                            + "\" and text = \"" + text + "\"!",
                    driver.getElementWithText(by, text).isDisplayed());
        }
        LOGGER.info("assertTextPresentInElement ended - Line item \"" + text + "\" is present");
    }

    /**
     * Asserts whether an element of a particular By type is displayed
     *
     * @param byType  The by characteristic of the element (ie classname, css)
     * @param element The text of the element to search for
     */
    public void assertElementIsDisplayed(String byType, String element) {
        LOGGER.info("assertElementIsDisplayed started");
        boolean elementFound;
        try {
            By by = driver.getBy(byType, element);
            WebElement webElement = webDriver.findElement(by);
            elementFound = driver.isElementDisplayed(webElement, Constants.THIRTY);
        } catch (NoSuchElementException e) {
            elementFound = false;
        }
        Assert.assertTrue("FAIL: Element '" + element + " was either not found or not displayed.", elementFound);
        LOGGER.info("assertElementIsDisplayed ended - Element '" + element + " was displayed.");
    }


    /**
     * Returns the h1 header of the page
     *
     * @return String
     */
    public String getPageHeader() {
        return webDriver.findElement(headerBy).getText();
    }

    /**
     * Clicks next or previous in results pages. Since mweb and web share classnames for pagination,
     * getDisplayedElement must be leveraged in order to click the correct buttons
     *
     * @param direction The direction to paginate, "next", "previous", "first", "last"
     * @return boolean  Whether the button was found
     */
    public boolean navToDifferentPageOfResults(String direction) {
        LOGGER.info("navToDifferentPageOfResults started with direction: '" + direction + "'");
        WebElement button = null;
        boolean foundButton = false;
        boolean preselected = false;

        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        boolean multiplePages = webDriver.findElements(CommonActions.paginationBy).size() > 0;
        driver.resetImplicitWaitToDefault();

        if (multiplePages) {
            if (Config.isFirefox() || Config.isMobile())
                driver.waitForMilliseconds(Constants.TWO_THOUSAND);

            if (direction.equalsIgnoreCase(Constants.NEXT)) {
                button = driver.getDisplayedElement(nextBtnBy, Constants.ZERO);
            } else if (direction.equalsIgnoreCase(Constants.PREVIOUS)) {
                button = driver.getDisplayedElement(previousBtnBy, Constants.ZERO);
            } else if (direction.equalsIgnoreCase(Constants.FIRST)) {
                boolean myAccountSelected = false;
                try {
                    driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
                    myAccountSelected = driver.isElementDisplayed(
                            webDriver.findElement(MyAccountPage.myAccountSelectedTabBy), Constants.ONE);
                    driver.resetImplicitWaitToDefault();
                } catch (Exception e) {
                    // continue
                }
                if (myAccountSelected) {
                    if (webDriver.findElement(selectedPageBy).getText().equals(String.valueOf(Constants.ONE)))
                        preselected = true;
                } else if (webDriver.findElements(selectedPageBy).size() > 1) {
                    if (webDriver.findElements(selectedPageBy).get(1).getText().equals(String.valueOf(Constants.ONE)))
                        preselected = true;
                }
                if (!preselected)
                    button = driver.getDisplayedElement(firstBtnBy, Constants.ZERO);
            } else if (direction.equalsIgnoreCase(Constants.LAST)) {
                button = driver.getDisplayedElement(lastBtnBy, Constants.ZERO);
            } else {
                Assert.fail("FAIL: The results navigation direction of '" + direction + "' was NOT recognized!");
            }

            if (!preselected) {
                if (button != null) {
                    driver.jsScrollToElementClick(button);
                    driver.waitForPageToLoad();
                    waitForSpinner();
                    foundButton = true;
                }
            }
        }

        LOGGER.info("navToDifferentPageOfResults completed with direction: '" + direction + "'");
        return foundButton;
    }

    /**
     * Closes the Welcome Pop-Up by either clicking Continue or Search for another store link.
     *
     * @param action Search OR Close
     */
    public void closeWelcomePopUp(String action) {
        LOGGER.info("closeWelcomePopUp started with action: " + action);
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        try {
            if (CommonUtils.containsIgnoreCase(action, ConstantsDtc.SEARCH)) {
                StoreLocatorPage.welcomePopUpSearchLink.click();
            } else {
                if (driver.isElementDisplayed(CommonActions.chooseStoreButton, Constants.ONE)) {
                    CommonActions.chooseStoreButton.click();
                }
            }
        } catch (Exception e) {
            LOGGER.info("checkForWelcomePopup not shown on screen");
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("closeWelcomePopUp completed with action: " + action);
    }

    /**
     * Finds and clicks element with given link text
     *
     * @param linkText Text in the link to click
     */
    public void clickElementWithLinkText(String linkText) {
        LOGGER.info("clickElementWithLinkText started with link text '" + linkText + "'");
        driver.waitForPageToLoad(Constants.ONE_HUNDRED);
        if (linkText.equalsIgnoreCase(ConstantsDtc.CONTINUE_WITHOUT_VEHICLE) &&
                Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
            LOGGER.info("CheckFit is not active in DTD. " + ConstantsDtc.CONTINUE_WITHOUT_VEHICLE +
                    " link is not displayed");
            return;
        }
        if (linkText.equalsIgnoreCase(ConstantsDtc.EDIT_VEHICLE)) {
            selectEditVehicle();
            return;
        }
        if (linkText.equalsIgnoreCase(CommonActions.CONTINUE_WITH_THIS_VEHICLE)) {
            clickContinueWithThisVehicleButton();
            return;
        }
        if (linkText.equalsIgnoreCase(ConstantsDtc.CLEAR_CART)) {
            if (driver.isElementDisplayed(CartPage.miniCartClear, Constants.ZERO)) {
                return;
            }
        }
        if (linkText.equalsIgnoreCase(ConstantsDtc.VIEW_MY_ACCOUNT)) {
            if (!driver.isElementDisplayed(driver.getElementWithText(CommonActions.anchorTagBy, linkText))) {
                String firstName = customer.getCustomer(
                        Customer.CustomerType.MY_ACCOUNT_USER_A.toString()).firstName;
                driver.jsScrollToElementClick(
                        driver.getElementWithText(CommonActions.spanTagNameBy, firstName), false);
            }
        }
        driver.clickElementWithLinkText(linkText);
        if (linkText.equalsIgnoreCase(ConstantsDtc.CLEAR_ALL)) {
            driver.waitSeconds(Constants.TWO);
        }
        clickContinueWithThisVehicleButton();
        LOGGER.info("clickElementWithLinkText completed with link text '" + linkText + "'");
    }

    /**
     * Checks if text is displayed
     *
     * @param text The string text to check
     */
    public void assertTextPresentInPageSource(String text) {
        LOGGER.info("assertTextPresentInPageSource started with text \"" + text + "\"");
        driver.waitForMilliseconds();
        Assert.assertTrue("FAIL: Text: \"" + text + "\" - was NOT present!",
                driver.isTextPresentInPageSource(text));
        LOGGER.info("Text: \"" + text + "\" - was present");
        LOGGER.info("assertTextPresentInPageSource completed with text \"" + text + "\"");
    }

    /**
     * Checks for text inside the Breadcrumb container on the page
     *
     * @param linkName String representing the names/titles of the breadcrumb links being checked
     */
    public void verifyBreadcrumbLinks(String linkName) {
        LOGGER.info("verifyBreadcrumbLinks started for " + linkName);
        String links[] = linkName.split(",");
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        String crumbLinks = webDriver.findElement(breadCrumbContainerBy).getText();
        for (int i = 0; i < links.length; i++) {
            Assert.assertTrue("FAIL: Link '" + links[i] + "' was NOT found on page in the " +
                            "breadcrumb container: " + crumbLinks,
                    crumbLinks.toLowerCase().contains(links[i].trim().toLowerCase()));
        }
        LOGGER.info("verifyLinksInsideBreadcrumbContainer completed for " + linkName);
    }

    /**
     * Navigates to page with url + path
     *
     * @param path The path to append to base url
     */
    public void navigateToPage(String path) {
        LOGGER.info("navigateToPage started with path: \"" + path + "\"");

        if (Config.isFirefox())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        try {
            String url = Config.getBaseUrl() + path;
            driver.getUrl(url);
            driver.waitForPageToLoad();
        } catch (Exception e) {
            Assert.fail("FAIL: Navigating to page with url and path \"" + path + "\" FAILED with error: " + e);
        }
        LOGGER.info("navigateToPage completed with path: \"" + path + "\"");
    }

    /**
     * Verifies appropriate messages are present in top message banner
     *
     * @param page PLP or PDP page for banner selection
     * @param text Text inside the banner to verify
     */
    public void assertStringInTopBanner(String page, String text) {
        LOGGER.info("assertStringInTopBanner started for '" + page + "' with text '" + text + "'");
        driver.waitForPageToLoad();
        String resultMessage = "";
        By bannerIcon = null;
        if (page.equalsIgnoreCase(ConstantsDtc.PDP)) {
            resultMessage = webDriver.findElement(plpFitmentVerificationBy).getText();
            bannerIcon = fitmentVerificationIcon;
        } else if (page.equalsIgnoreCase(ConstantsDtc.PLP)) {
            resultMessage = webDriver.findElement(plpFitmentVerificationBy).getText();
            bannerIcon = fitmentVerificationIcon;
        } else if (page.equalsIgnoreCase(ConstantsDtc.CHECKFIT)) {
            resultMessage = webDriver.findElement(checkFitBannerTitleBy).getText();
            bannerIcon = checkFitBannerIconBy;
        }

        Assert.assertTrue("FAIL: The text '" + text + "' was not found in the " + page.toUpperCase()
                + " banner message!", resultMessage.toLowerCase().contains(text.toLowerCase()) &&
                driver.isElementDisplayed(bannerIcon));
        LOGGER.info("assertStringInTopBanner completed for '" + page + "' with text '" + text + "'");
    }

    /**
     * Verifies the message banner color
     *
     * @param page  The page to assert against (PLP,PDP or Brands)
     * @param color The color to assert (Green,Yellow, Red)
     */
    public void assertBannerColor(String page, String color) {
        LOGGER.info("assertBannerColor started");
        String expectedBorderTop = null;
        String expectedBorderBottom = null;
        WebElement fitmentBanner;
        driver.waitForPageToLoad();
        driver.setImplicitWait(Constants.TEN, TimeUnit.SECONDS);

        if (page.equalsIgnoreCase(ConstantsDtc.PDP)) {
            fitmentBanner = webDriver.findElement(pdpfitmentBannerBy);
        } else if (page.equalsIgnoreCase(ConstantsDtc.PLP)) {
            fitmentBanner = webDriver.findElement(plpfitmentBannerBy);
        } else if (page.equalsIgnoreCase(ConstantsDtc.CHECKFIT)) {
            fitmentBanner = webDriver.findElement(checkFitBannerBy);
        } else {
            fitmentBanner = webDriver.findElement(brandsFitmentBannerBy);
        }

        String borderTop = fitmentBanner.getCssValue(Constants.BORDER_TOP_COLOR);
        String borderBottom = fitmentBanner.getCssValue(Constants.BORDER_BOTTOM_COLOR);

        if (color.equalsIgnoreCase(Constants.GREEN)) {
            if (Config.isFirefox()) {
                expectedBorderTop = Constants.GREEN_BANNER_RGB;
                expectedBorderBottom = Constants.GREEN_BANNER_RGB;
            } else {
                expectedBorderTop = Constants.GREEN_BANNER_RGBA;
                expectedBorderBottom = Constants.GREEN_BANNER_RGBA;
            }
        } else if (color.equalsIgnoreCase(Constants.YELLOW)) {
            if (Config.isFirefox()) {
                expectedBorderTop = Constants.YELLOW_BANNER_RGB;
                expectedBorderBottom = Constants.YELLOW_BANNER_RGB;
            } else {
                expectedBorderTop = Constants.YELLOW_BANNER_RGBA;
                expectedBorderBottom = Constants.YELLOW_BANNER_RGBA;
            }
        } else if (color.equalsIgnoreCase(Constants.RED)) {
            if (Config.isFirefox()) {
                expectedBorderBottom = Constants.RED_COLOR_RGB;
                expectedBorderTop = Constants.RED_COLOR_RGB;
            } else {
                expectedBorderBottom = Constants.RED_COLOR_RGBA;
                expectedBorderTop = Constants.RED_COLOR_RGBA;
            }
        }

        Assert.assertTrue("FAIL: Expected '" + page + "' banner to have color: '" + expectedBorderTop
                + ", " + expectedBorderBottom + "' but the actual color was: '" + borderTop + ", " + borderBottom
                + "'!", borderTop.contains(expectedBorderTop) && borderBottom.contains(expectedBorderBottom));

        driver.resetImplicitWaitToDefault();
        LOGGER.info("assertBannerColor completed");
    }

    /**
     * Verifies provided text is contained inside the global results banner
     *
     * @param page The page to assert against (PLP or PDP)
     * @param text Text to verify appears
     */
    public void assertResultsMessageContains(String page, String text) {
        LOGGER.info("assertResultsMessageContains started on " + page);
        driver.waitForElementVisible(resultsBarSizeBy);
        Assert.assertNotNull("PLP result message banner display text did not contain expected text: " + text,
                driver.getElementWithText(resultsBarSizeBy, text));
        LOGGER.info("assertResultsMessageContains completed on " + page);
    }

    /***
     * Checks for and closes the 'Chat Now' popup modal on DTD site
     */
    public void checkForChatNowPopup() {
        LOGGER.info("checkForChatNowPopup started");
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            if (driver.isElementDisplayed(noThanksBtnBy, Constants.TWO)) {
                WebElement noThanksBtnEle = webDriver.findElement(noThanksBtnBy);
                driver.jsScrollToElement(noThanksBtnEle);
                noThanksBtnEle.click();
                driver.pollUntil(chatNowPopupClosed, Constants.FIVE);
                LOGGER.info("Chat Now Popup was closed");
            } else {
                LOGGER.info("Chat Now Popup not shown on screen");
            }
        }
        LOGGER.info("checkForChatNowPopup completed");
    }

    /**
     * Switch to Already Opened Next Browser Tab
     */
    public void switchToNextOpenedTab() {
        LOGGER.info("switchToNextOpenedTab started");
        String mainHandle = webDriver.getWindowHandle();
        //TODO: This seems to wait for 5 seconds regardless
        new WebDriverWait(webDriver, Constants.FIVE).until(ExpectedConditions.numberOfWindowsToBe(2));
        Set allHandles = webDriver.getWindowHandles();
        Iterator iter = allHandles.iterator();
        while (iter.hasNext()) {
            String popupHandle = iter.next().toString();
            if (!popupHandle.contains(mainHandle)) {
                webDriver.switchTo().window(popupHandle);
            }
        }
        LOGGER.info("switchToNextOpenedTab completed");
    }

    /**
     * Switch to Previous Opened Browser Tab
     */
    public void switchToPreviousOpenedTab() {
        LOGGER.info("switchToPreviousOpenedTab started");
        new WebDriverWait(webDriver, Constants.FIVE).until(ExpectedConditions.numberOfWindowsToBe(2));
        ArrayList<String> tab = new ArrayList<String>(webDriver.getWindowHandles());
        if (tab.size() > 1) {
            webDriver.switchTo().window(tab.get(1));
            webDriver.close();
            webDriver.switchTo().window(tab.get(0));
        } else {
            LOGGER.info("Previous Browser Tab Not Found");
        }
        LOGGER.info("switchToPreviousOpenedTab completed");
    }

    /**
     * Verifies element text is not empty
     *
     * @param element The element to check
     */
    public void assertElementTextPopulated(WebElement element) {
        LOGGER.info("assertElementTextPopulated started");

        driver.waitForElementVisible(element);

        Assert.assertTrue("FAIL: Element text was empty.",
                !element.getText().isEmpty());

        LOGGER.info("Element text was populated.");
        LOGGER.info("assertElementTextPopulated completed");
    }

    /**
     * Method takes in a string, removes all non-numeric characters and returns an Int
     *
     * @param phrase String containing numbers you'd like to return
     */
    public int removeNonNumericsFromString(String phrase) {
        LOGGER.info("removeNonNumericsFromString started");
        int returnInt = 0;
        try {
            phrase = phrase.replaceAll("\\D", "");
            returnInt = Integer.parseInt(phrase);
            LOGGER.info("Returning integer value of " + returnInt);
        } catch (Exception e) {
            Assert.fail("FAIL: Provided phrase did not contain a numeric value");
        }

        LOGGER.info("removeNonNumericsFromString completed");
        return returnInt;
    }

    /**
     * Closes all tabs or windows except the main handle
     */
    public void closeTabs() {
        LOGGER.info("closeTabs started");

        Set allHandles = webDriver.getWindowHandles();
        String mainHandle = webDriver.getWindowHandle();

        Iterator iter = allHandles.iterator();
        while (iter.hasNext()) {
            String popupHandle = iter.next().toString();
            if (!popupHandle.contains(mainHandle)) {
                webDriver.switchTo().window(popupHandle);
                webDriver.close();
                webDriver.switchTo().window(mainHandle);
            }
        }
        LOGGER.info("closeTabs completed");
    }

    /**
     * Cleans a string of any non-digit or non-decimal point chars and returns a double containing the newly parsed
     * value
     *
     * @param currencyString String containing a currency / monetary value needing to be cleaned & parsed
     * @return Monetary value in a Double type
     */
    public double cleanMonetaryStringToDouble(String currencyString) {
        return Double.parseDouble(currencyString.split(Constants.EA)[0].replaceAll("[^0-9.]+", ""));
    }

    /**
     * Cleans a string of any non-digit or non-decimal point chars and returns an int containing the newly parsed
     * value
     *
     * @param currencyString String containing a currency / monetary value needing to be cleaned & parsed
     * @return Monetary value in an int type
     */
    public int cleanMonetaryStringToInt(String currencyString) {
        return Integer.parseInt(currencyString.split("\\.\\d{2}")[0].replaceAll("[^0-9]", ""));
    }

    /**
     * Convert decimal to exactly two decimal place
     *
     * @return any calculated value in a Double type
     */
    public double twoDForm(double input, int digits) {
        double digitFactor = Math.pow(10.0D, digits);
        return Math.round(input * digitFactor) / digitFactor;
    }

    /**
     * Extracts the subtotal value from either the Cart or Checkout page
     *
     * @return Subtotal amount / value from the specified page, cleaned and typed as a Double
     */
    public double extractSubtotalAmount(String pageWithSubtotal) {
        LOGGER.info("extractSubtotalAmount started");
        WebElement subtotalEle;

        if (pageWithSubtotal.equalsIgnoreCase(Constants.CART)) {
            subtotalEle = cartSubtotal;
        } else {
            subtotalEle = CheckoutPage.checkoutSubtotal;
        }

        driver.waitForElementVisible(subtotalEle);
        LOGGER.info("extractSubtotalAmount completed");
        return cleanMonetaryStringToDouble(subtotalEle.getText());
    }

    /**
     * Converts a date in the format of mm/dd/yyyy to EEEE, MMM dd format (e.g. 12/04/2017 to Monday, Dec 04)
     *
     * @param date String in the format of mm/dd/yyyy to be converted
     * @return Date string in the format of EEEE, MMM dd (Monday, Dec 04)
     */
    public String convertDateToDayMonthDateFormat(String date) {
        LOGGER.info("convertDateToDayMonthDateFormat started with date:  " + date);
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
            Date t = fmt.parse(date);
            fmt.applyPattern("EEEE, MMM d");
            date = fmt.format(t);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LOGGER.info("convertDateToDayMonthDateFormat completed with date:  " + date);
        return date;
    }

    /**
     * Converts a date in the format of EEEE, MMM dd, yyyy to format yyyy-mm-dd(e.g. Wednesday, Dec 18, 2019 to 2019-12-18)
     *
     * @param date String in the format of EEEE, MMM dd, yyyy (Wednesday, Dec 18, 2019) to be converted
     * @return Date string in the format of yyyy-mm-dd (2019-12-18)
     */
    public String convertDateToYearMonthDateFormat(String date) {
        LOGGER.info("convertDateToYearMonthDateFormat started with date:  " + date);
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("EEEE, MMM d, yyyy", Locale.US);
            Date t = fmt.parse(date);
            SimpleDateFormat formatNeeded = new SimpleDateFormat("yyyy-MM-dd");
            date = formatNeeded.format(t).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LOGGER.info("convertDateToYearMonthDateFormat completed with date:  " + date);
        return date;
    }

    /**
     * Converts AM/PM time to 24 hours date time(e.g. 1.00 PM to 13.00)
     *
     * @param time String in the format of AM/PM time (1.00 PM) to be converted
     * @return time string in the format of 24 hours time (13.00)
     */
    public String convertToMilitaryTime(String time) {
        LOGGER.info("convertToMilitaryTime started with time:  " + time);
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("hh:mm a");
            Date t = fmt.parse(time);
            SimpleDateFormat formatNeeded = new SimpleDateFormat("HH:mm");
            time = formatNeeded.format(t).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LOGGER.info("convertToMilitaryTime completed with time:  " + time);
        return time;
    }

    /**
     * Extracts the sales tax from Cart page
     *
     * @return salesTax typed as a Double
     */
    public double extractTaxOnCart() {
        LOGGER.info("extractTaxOnCart started");
        WebElement tax = driver.getElementWithText(CartPage.cartSummaryBreakDownNameBy, ConstantsDtc.TAX);
        driver.jsScrollToElement(tax);
        driver.waitForMilliseconds();
        WebElement taxParent = driver.getParentElement(driver.getParentElement(tax));
        LOGGER.info("extractTaxOnCart completed");
        return cleanMonetaryStringToDouble(taxParent.findElement(CartPage.cartSummaryBreakDownPriceBy).getText());
    }

    /**
     * Selects the desired phone type from the phone type dropdown based on the flow path specified
     *
     * @param dropdownFlow The flow by which the dropdown is encountered (Checkout with shipping address,
     *                     Schedule Service, Adding Contact Information in My Account Profile tab ,etc)
     * @param phoneType    Phone type to select from the dropdown
     */
    public void selectPhoneTypeFromDropdown(String dropdownFlow, String phoneType) {
        LOGGER.info("selectPhoneTypeFromDropdown started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(dropdownBy, Constants.FIVE);

        int index = 0;
        if (dropdownFlow.equalsIgnoreCase(ConstantsDtc.CHECKOUT_WITH_SHIPPING)
                || dropdownFlow.equalsIgnoreCase(ConstantsDtc.CONTACT_INFORMATION)) {
            index = 1;
        }

        WebElement phoneDropdownEle = webDriver.findElements(dropdownBy).get(index);
        driver.jsScrollToElement(phoneDropdownEle);
        phoneDropdownEle.click();
        driver.getElementsWithText(dropdownOptionBy, phoneType).get(0).click();
        LOGGER.info("selectPhoneTypeFromDropdown completed");
    }

    /**
     * Verify whether the specified button is enabled or disabled
     *
     * @param buttonText        - the text on the button to be validated
     * @param enabledOrDisabled - expected state of 'enabled' or 'disabled'
     */
    public void assertButtonEnabledDisabled(String buttonText, String enabledOrDisabled) {
        LOGGER.info("assertButtonEnabledDisabled started");
        boolean found = false;
        List<WebElement> buttons = webDriver.findElements(buttonBy);

        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).getText().equalsIgnoreCase(buttonText)) {
                if (enabledOrDisabled.equalsIgnoreCase(Constants.ENABLED)) {
                    Assert.assertTrue("The '" + buttonText + "' button was disabled when it was expected" +
                            " to be enabled", buttons.get(i).isEnabled());
                } else {
                    Assert.assertTrue("The '" + buttonText + "' button was enabled when it was expected" +
                            " to be disabled", !buttons.get(i).isEnabled());
                }
                found = true;
                break;
            }
        }

        Assert.assertTrue(buttonText + " button not found", found);
        LOGGER.info("assertButtonEnabledDisabled completed");
    }

    /**
     * Verifies the expected attributes (strings) are contained within a specified section (By object)
     *
     * @param pageSection            Name of the page section
     * @param sectionHeaderBy        By object for the section of the page containing the attributes to be verified
     * @param expectedAttributesList List of attribute values expected to be contained with the specified page section
     */
    public void verifyPageSectionContainsAttributes(String pageSection, By sectionHeaderBy,
                                                    List<String> expectedAttributesList) {
        LOGGER.info("verifyPageSectionContainsAttributes started for section: '" + pageSection
                + "'\n\t\twith expected attributes: '" + expectedAttributesList + "'");
        String compareSectionContent = driver.getElementWithText(sectionHeaderBy, pageSection).getText();
        for (String attribute : expectedAttributesList) {
            Assert.assertTrue("FAIL - The page section '" + pageSection
                            + "' did NOT contain the expected attribute of: '" + attribute + "'!",
                    compareSectionContent.contains(attribute));
        }
        LOGGER.info("verifyPageSectionContainsAttributes completed successfully for section: '"
                + pageSection + "'");
    }

    /**
     * Verify Mini Cart not displayed on shipping details, shipping method, payment and order confirmation page
     *
     * @param page web page type
     */
    public void assertMiniCartNotDisplayed(String page) {
        LOGGER.info("assertMiniCartNotDisplayed started for " + page);
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: MiniCart is displayed", driver.isElementNotVisible(miniCartBy,
                Constants.TWO));
        LOGGER.info("assertMiniCartNotDisplayed completed for " + page);
    }

    /**
     * Closes a modal window
     */
    public void closeModalWindow() {
        LOGGER.info("closeModalWindow started");
        driver.waitSeconds(Constants.THREE);
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        WebElement closeButton = null;
        List<WebElement> modalContainers = webDriver.findElements(dtModalContainerBy);
        for (WebElement modalContainer : modalContainers) {
            closeButton = driver.getDisplayedElement(modalContainer, dtModalCloseBy, Constants.ZERO);
            if (closeButton != null)
                break;
        }
        if (closeButton == null) {
            try {
                driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
                closeButton = webDriver.findElement(CompareProductsPage.iconCloseBy);
            } catch (Exception e) {
                closeButton = null;
            }
        }
        if (closeButton != null) {
            driver.jsScrollToElementClick(closeButton, false);
        } else {
            LOGGER.info("No popup modal close button/icon was found. No action taken.");
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("closeModalWindow completed");
    }

    /**
     * Get the X coordinate value of an element
     *
     * @param element The element in reference
     * @return int     X coordinate value of an element
     */
    public int getXCoordinateValueOfAnElement(WebElement element) {
        LOGGER.info("getXCoordinateValueOfAnElement started");
        driver.waitForElementVisible(element);

        int xCoordinateValue = Integer.valueOf(element.getLocation().toString().split(",")[0].split("\\(")[1]);
        LOGGER.info("getXCoordinateValueOfAnElement completed");
        return xCoordinateValue;
    }

    /**
     * Evaluates a given String to determine if it is null or empty ("")
     *
     * @param str String of text to evaluate
     * @return True if string == empty OR False if string != empty
     */
    public static boolean isStringEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    /**
     * Verifies the checkbox with specified label has been selected
     *
     * @param element       WebElement for the checkbox
     * @param checkboxLabel String of the checkbox label
     */
    public void verifyCheckboxSelected(WebElement element, String checkboxLabel) {
        LOGGER.info("verifyCheckboxSelected started for checkbox w/ label: '" + checkboxLabel + "'");
        driver.waitForPageToLoad(Constants.THREE_THOUSAND);
        Assert.assertTrue("FAIL: checkbox with label: '" + checkboxLabel + "' was NOT selected!",
                element.isSelected());
        LOGGER.info("verifyCheckboxSelected completed - checkbox w/ label: '" + checkboxLabel + "' was selected!");
    }

    /**
     * Extracts the ratings and verifies they are in ascending/descending order
     *
     * @param sortOrder String (Ascending or Descending)
     * @param value     String (Lowest Rated,Overall Rating)
     */
    public void verifyRatingOrder(String value, String sortOrder) {
        LOGGER.info("verifyRatingOrder started");
        List<WebElement> reviews = null;
        if (value.equalsIgnoreCase(ConstantsDtc.OVERALL_RATING)) {
            Assert.assertFalse("Fail - The \"There are no products to compare\" message was displayed!",
                    driver.isElementDisplayed(noProductsToCompareMessage, Constants.ONE));
            driver.waitForElementVisible(overallRatingBy);
            reviews = webDriver.findElements(overallRatingBy);
        } else if (value.equalsIgnoreCase(ConstantsDtc.LOWEST_RATED)) {
            reviews = webDriver.findElements(storeReviewScoreBy);
        }
        ArrayList<Float> reviewList = new ArrayList<>();
        if (null != reviews) {
            for (WebElement review : reviews) {
                reviewList.add(Float.parseFloat(review.getText()));
            }
            if (sortOrder.equalsIgnoreCase(Constants.ASCENDING)) {
                if (!verifyAscendingOrder(reviewList)) {
                    Assert.fail("FAIL : Rating did NOT display in ascending order");
                }
            } else if (sortOrder.equalsIgnoreCase(Constants.DESCENDING)) {
                if (!verifyDescendingOrder(reviewList)) {
                    Assert.fail("FAIL : Rating did NOT display in descending order");
                }
            } else {
                Assert.fail("FAIL: Input value should either be Ascending or Descending but was :\""
                        + sortOrder + "\"!");
            }
        } else {
            Assert.fail("FAIL: There are no reviews ");
        }
        LOGGER.info("verifyRatingOrder completed");
    }

    /**
     * Checks the ascending order for the list of products on the compare tire reviews page excluding the first one
     * because the first one in the list is the Selected Tire
     *
     * @param value The array to check the ascending order of reviews
     * @return whether or not the list is sorted in ascending order
     */
    public Boolean verifyAscendingOrder(ArrayList<Float> value) {
        LOGGER.info("verifyAscendingOrder started");
        driver.waitForPageToLoad();
        for (int i = 1; i < value.size() - 1; i++) {
            if (value.get(i) > value.get(i + 1)) {
                return false;
            }
        }
        LOGGER.info("verifyAscendingOrder completed");
        return true;
    }

    /**
     * Checks the descending order for the list of products on the compare tire reviews page excluding the first one
     * because the first one in the list is the Selected Tire
     *
     * @param value The array to check the descending order of reviews
     * @return whether or not the list is sorted in descending order
     */
    public Boolean verifyDescendingOrder(ArrayList<Float> value) {
        LOGGER.info("verifyDescendingOrder started");
        driver.waitForPageToLoad();
        for (int i = 1; i < value.size() - 1; i++) {
            if (value.get(i) < value.get(i + 1)) {
                return false;
            }
        }
        LOGGER.info("verifyDescendingOrder completed");
        return true;
    }

    /**
     * Verify Inventory Message for item code
     *
     * @param message the expected Inventory Message
     * @param item    Product Code
     * @param page    web page
     */
    public void assertInventoryMessage(String message, String item, String page) {
        LOGGER.info("assertInventoryMessage started on " + page);
        driver.waitForPageToLoad();
        String inventoryMessageText;
        if (page.equalsIgnoreCase(ConstantsDtc.PDP)) {
            inventoryMessageText = webDriver.findElement(inventoryMessageBy).getText();
        } else if (page.equalsIgnoreCase(ConstantsDtc.PLP) && Config.isMobile()) {
            inventoryMessageText = getPLPResultTopElement(item).findElement(productLowerBlockBy).
                    findElement(inventoryMessageBy).getText();
        } else if (page.equalsIgnoreCase((ConstantsDtc.PLP))) {
            inventoryMessageText = getPLPResultTopElement(item).findElement(inventoryMessageBy).getText();
        } else if (page.equalsIgnoreCase(ConstantsDtc.CHECK_AVAILABILITY)) {
            inventoryMessageText = webDriver.findElement(stockInventoryMessageBy).getText();
        } else if (page.equalsIgnoreCase(ConstantsDtc.CHECK_INVENTORY)) {
            inventoryMessageText = webDriver.findElement(stockMessageCheckInventoryBy).getText();
        } else if (page.equalsIgnoreCase(ConstantsDtc.SHOPPING_CART) && Config.isMobile()) {
            inventoryMessageText = getProductSectionTopElement(item).findElements(CartPage.hideSmBy).get(1).getText();
        } else if (page.equalsIgnoreCase(ConstantsDtc.SHOPPING_CART) ||
                page.equalsIgnoreCase(ConstantsDtc.ORDER_CONFIRMATION)) {
            inventoryMessageText = getProductSectionTopElement(item).
                    findElement(displayInlineSmBy).getAttribute(Constants.INNERTEXT);
        } else if (page.equalsIgnoreCase(ConstantsDtc.COMPARE_PRODUCTS)) {
            inventoryMessageText = driver.getParentElement(driver.getParentElement(
                    driver.getParentElement(driver.getElementWithText(headerFifthBy, item)))).
                    findElement(CommonActions.inventoryMessagesBy).
                    getAttribute(Constants.INNERTEXT).replace("  ", " ");
        } else {
            inventoryMessageText = driver.getParentElement(driver.getParentElement(
                    driver.getParentElement(driver.getElementWithText(headerFifthBy, item))))
                    .findElement(inventoryMessageBy).getText();
        }

        inventoryMessageText = inventoryMessageText.trim();

        if (page.equalsIgnoreCase(ConstantsDtc.ORDER_CONFIRMATION) && message.contains(ConstantsDtc.ORDER_NOW)) {
            message = message.split(", ")[1];
        }

        boolean match = CommonUtils.containsIgnoreCase(inventoryMessageText, message);
        if (!match && CommonUtils.containsIgnoreCase(message, ConstantsDtc.ORDER_NOW_AVAILABLE_AS_SOON_AS_TOMORROW)) {
            message.replace(ConstantsDtc.ORDER_NOW_AVAILABLE_AS_SOON_AS_TOMORROW,
                    ConstantsDtc.ORDER_NOW_AVAILABLE_WITHIN_ONE_BUSINESS_DAY);
            match = CommonUtils.containsIgnoreCase(inventoryMessageText, message);
        }
        Assert.assertTrue("FAIL: The inventory message on" + page + " didn't match for product: " + item +
                ". Displayed: '" + inventoryMessageText + "'.  Expected:  '" + message + "'.", match);
        LOGGER.info("assertInventoryMessage completed on " + page);
    }

    /**
     * Verify Check nearby stores link display for item code
     *
     * @param item Product Code
     * @param page web page
     */
    public void assertCheckNearbyStoresLinkDisplay(String item, String page) {
        LOGGER.info("assertCheckNearbyStoresLinkDisplay started on " + page);
        driver.waitForPageToLoad();
        WebElement parentElement;
        if (page.equalsIgnoreCase(ConstantsDtc.PDP)) {
            parentElement = webDriver.findElement(pdpInfoContainerBy);
        } else if (page.equalsIgnoreCase(ConstantsDtc.PLP) && Config.isMobile()) {
            parentElement = getPLPResultTopElement(item).findElement(productLowerBlockBy);
        } else {
            parentElement = getPLPResultTopElement(item);
        }
        By inventoryBy = checkInventoryBy;
        Assert.assertTrue("FAIL: The Check nearby stores link on " + page
                        + " for product " + item + " is not displayed!",
                driver.isElementDisplayed(parentElement.findElement(inventoryBy)));
        Assert.assertTrue("FAIL: The Check nearby stores link text on " + page + " didn't match for product:"
                        + item + "! (displayed:-> '" + parentElement.findElement(inventoryBy).getText()
                        + "'  expected:->  " + ConstantsDtc.CHECK_NEARBY_STORES + "'!",
                parentElement.findElement(inventoryBy).getText().equalsIgnoreCase(ConstantsDtc.CHECK_NEARBY_STORES));
        LOGGER.info("assertCheckNearbyStoresLinkDisplay completed on " + page);
    }

    /**
     * Verify Inventory Message for staggered product set item
     *
     * @param message     the expected Inventory Message
     * @param productCode Product Code in the set
     * @param page        web page
     */
    public void assertInventoryMessageForSet(String message, String productCode, String page) {
        LOGGER.info("assertInventoryMessageForSet started on " + page);
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        WebElement inventoryMsgEle;
        List<WebElement> products;
        if (page.equalsIgnoreCase(ConstantsDtc.PDP)) {
            inventoryMsgEle = webDriver.findElement(CommonActions.pdpInfoContainerBy).
                    findElement(inventoryMessageStaggeredBy);
            products = webDriver.findElements(CommonActions.staggeredItemNumberBy);
        } else if (page.equalsIgnoreCase(ConstantsDtc.SHOPPING_CART) ||
                (page.equalsIgnoreCase(ConstantsDtc.ORDER_CONFIRMATION))) {
            inventoryMsgEle = webDriver.findElement(cartInventoryMessageBy);
            products = driver.getDisplayedElementsList(cartItemCodeBy);
        } else {
            WebElement topElement = getPLPResultTopElement(productCode);
            inventoryMsgEle = topElement.findElement(inventoryMessageBy);
            products = topElement.findElements(CommonActions.staggeredItemNumberBy);
        }
        Assert.assertNotEquals("FAIL: Product '" + productCode + "' was not found on the '" + page + "' page.",
                products.size(), Constants.ZERO);
        for (WebElement product : products) {
            if (product.getText().contains(productCode)) {
                Assert.assertTrue("FAIL: The inventory message on " + page + " didn't match for product: " + productCode
                                + ". Displayed:-> '" + inventoryMsgEle.getText() + "'. Expected:->  '" + message + "'",
                        CommonUtils.containsIgnoreCase(inventoryMsgEle.getText().trim(), message.trim()) ||
                                CommonUtils.containsIgnoreCase(message.trim(), inventoryMsgEle.getText().trim()));
                break;
            }
        }
        LOGGER.info("assertInventoryMessageForSet completed on " + page);
    }

    /**
     * Get Product/quantity parent on cart page
     *
     * @param itemCode The quantity value to validate
     * @return Webelement Grand Parent for item Quantity
     */
    public WebElement getProductSectionTopElement(String itemCode) {
        LOGGER.info("getProductSectionTopElement started");
        List<WebElement> productSections = webDriver.findElements(CartPage.cartItemDetailsParentBy);
        WebElement section = null;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        for (WebElement productSection : productSections) {
            if (driver.elementExists(productSection, CartPage.staggeredDetailsContainerBy) &&
                    driver.isElementDisplayed(productSection.findElement(CartPage.staggeredDetailsContainerBy))) {
                driver.scenarioData.setStaggeredProduct(true);
                List<WebElement> staggeredItemContainers = productSection.findElement(
                        CartPage.staggeredDetailsContainerBy).findElements(CartPage.staggeredItemContainerBy);
                for (WebElement staggeredItemContainer : staggeredItemContainers) {
                    if (staggeredItemContainer.getText().contains(itemCode)) {
                        section = staggeredItemContainer;
                        break;
                    }
                }
            } else if (productSection.getText().contains(itemCode)) {
                section = productSection;
                break;
            }
        }
        driver.resetImplicitWaitToDefault();
        Assert.assertTrue("FAIL: Could not find the product with Item Code " + itemCode +
                " listed on Cart page", section != null);
        LOGGER.info("getProductSectionTopElement completed");
        return section;
    }

    /**
     * Set boolean whether the fitment on the Cart page is staggered or non-staggered
     */
    public void setStaggeredNonStaggeredFitmentOnShoppingCart() {
        LOGGER.info("setStaggeredNonStaggeredFitmentOnShoppingCart started");
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> productSections = webDriver.findElements(CartPage.cartItemDetailsParentBy);
        WebElement section = null;
        boolean isStaggered = false;
        for (WebElement productSection : productSections) {
            if (driver.elementExists(productSection, CartPage.staggeredDetailsContainerBy) &&
                    driver.isElementDisplayed(productSection.findElement(CartPage.staggeredDetailsContainerBy))) {
                isStaggered = true;
                break;
            }
        }
        driver.scenarioData.setStaggeredProduct(isStaggered);
        driver.resetImplicitWaitToDefault();
        LOGGER.info("setStaggeredNonStaggeredFitmentOnShoppingCart completed");
    }

    /**
     * Get Product parent on PLP page
     *
     * @param itemCode The quantity value to validate
     * @return WebElement product parent
     */
    public WebElement getPLPResultTopElement(String itemCode) {
        return getPLPResultTopElements(itemCode).get(0);
    }

    /**
     * Assert Product parent on PLP page
     *
     * @param itemCode The quantity value to validate
     * @return boolean element exists
     */
    public boolean assertPLPResultTopElement(String itemCode) {
        LOGGER.info("assertPLPResultTopElement started");
        int count = getPLPResultTopElements(itemCode).size();
        boolean exists = false;
        if (count == 1) {
            exists = true;
        } else if (count > 1) {
            Assert.fail("assertPLPResultTopElement expected 0 or 1 elements, got " + count + "elements");
        }
        LOGGER.info("assertPLPResultTopElement completed");
        return exists;
    }

    /**
     * Gets all Product Parents for item code on PLP page as a list
     * so one can return get(0) as a WebElement, or size to assert exists
     * >
     * If itemCode is null, the list will contain all top elements on the CURRENT results page
     * Else, the code will loop through all results pages until it finds the specified item
     *
     * @param itemCode Nullable. The item to find parent element of
     * @return List<WebElement> list of matching elements
     */
    public List<WebElement> getPLPResultTopElements(String itemCode) {
        LOGGER.info("getPLPResultTopElements started");
        List<WebElement> topElements = new ArrayList<>();
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        while (topElements.size() == 0) {
            List<WebElement> rows = webDriver.findElements(ProductListPage.plpResultsRowBy);
            for (WebElement row : rows) {
                if (itemCode == null) {
                    topElements.add(row);
                } else {
                    if (row.getText().contains(itemCode)) {
                        topElements.add(row);
                        break;
                    }
                }
            }
            if (itemCode == null) {
                if (topElements.size() == 0 && navToDifferentPageOfResults(Constants.NEXT)) {
                    driver.waitForPageToLoad();
                    driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
                } else if (topElements.size() == 0) {
                    Assert.fail("getPLPResultTopElements unexpectedly found " + topElements.size() +
                            " elements, 0 or 1 are the only valid amounts");
                }
            }
        }
        LOGGER.info("getPLPResultTopElements completed");
        driver.resetImplicitWaitToDefault();
        return topElements;
    }

    /**
     * Verify tool tip display and text for item code
     *
     * @param type Tool Tip type
     * @param item Product Code
     * @param text Expected Tool Tip text
     */
    public void assertToolTip(String type, String item, String text) {
        LOGGER.info("assertToolTip started for " + type + " having item code " + item);
        driver.waitForPageToLoad();

        WebElement tooltipElement = driver.getDisplayedElement(getProductSectionTopElement(item), tooltipBy, Constants.ZERO);
        Assert.assertTrue("FAIL: The tooltip for " + type + " is not displayed for item '" + item + "'."
                , driver.isElementDisplayed(tooltipElement));

        driver.jsScrollToElementClick(tooltipElement);
        if (CommonActions.tooltipContent.getText().equals("")) {
            driver.jsScrollToElementClick(tooltipElement);
        }
        Assert.assertEquals("FAIL: The tooltip text for " + type + " did not match for item " + item
                        + ", displayed: '" + CommonActions.tooltipContent.getText() + "' ,expected: '" + text + "'.",
                CommonActions.tooltipContent.getText(), text);
        LOGGER.info("assertToolTip completed for " + type + " having item code " + item);
    }

    /**
     * Verify the error message that appears within a form
     *
     * @param errorMessage - The expected error message
     */

    /**
     * Return 'true' or 'false' whether an error message that appears within a form when invalid data is entered displayed
     *
     * @param errorMessage - The expected error message
     * @return 'true' or 'false' whether the error message was displayed
     */
    public boolean formGroupErrorMessageDisplayed(String errorMessage, String paymentForm) {
        LOGGER.info("formGroupErrorMessageDisplayed started for error message: '" + errorMessage + "'");
        boolean displayed = false;

        int formIndex = getPaymentFormIndex(paymentForm);

        if (!driver.isElementDisplayed(CommonActions.formGroupMessageErrorElementBy, Constants.ONE)) {
            return displayed;
        }

        List<WebElement> formErrorMessages = null;

        if (formIndex == SECONDARY_FORM_INDEX) {
            if (!driver.isElementDisplayed(secondaryPaymentFormBy))
                Assert.fail("The secondary payment form is not present to verify error message");
            else
                formErrorMessages = webDriver.findElement(secondaryPaymentFormBy).findElements(CommonActions.formGroupMessageErrorElementBy);
        } else {
            formErrorMessages = webDriver.findElements(CommonActions.formGroupMessageErrorElementBy);
        }

        for (WebElement formErrorMessage : formErrorMessages) {
            if (formErrorMessage.getText().equals(errorMessage)) {
                displayed = true;
                break;
            }
        }

        LOGGER.info("formGroupErrorMessageDisplayed completed for error message: '" + errorMessage + "'");
        return displayed;
    }

    /**
     * Verify installation appointment text message
     *
     * @param page web page
     */
    public void assertInstallationAppointmentMessage(String page) {
        LOGGER.info("assertInstallationAppointmentMessage started for " + page);
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: The Installation Appointment message not displayed  on " + page + "!",
                driver.isElementDisplayed(installationAppointmentBy));
        for (String message : INSTALLATION_APPOINTMENT_MESSAGE) {
            Assert.assertTrue("FAIL: The Installation Appointment displayed message: '"
                            + webDriver.findElement(installationAppointmentBy).getText()
                            + " did NOT contain expected message: \"" + message + "\"!" + " on " + page,
                    webDriver.findElement(installationAppointmentBy).getText().contains(message));
        }
        LOGGER.info("assertInstallationAppointmentMessage completed for " + page);
    }

    /**
     * Verify stock count display for item code on cart page
     *
     * @param itemCode Product Code
     */
    public void assertStockCountTextForItem(String page, String itemCode) {
        LOGGER.info("assertStockCountTextForItem started for item code " + itemCode + " on " + page);
        driver.waitForPageToLoad();
        int elementIndex = 0;
        if (Config.isMobile())
            elementIndex = 1;
        WebElement stockCount;
        if (page.equalsIgnoreCase(ConstantsDtc.SHOPPING_CART)) {
            stockCount = getProductSectionTopElement(itemCode).findElements(CartPage.cartStoreStockCountBy).get(elementIndex);
        } else if (page.equalsIgnoreCase(ConstantsDtc.CHECK_INVENTORY)) {
            stockCount = webDriver.findElement(CartPage.cartStoreStockCountBy);
        } else {
            stockCount = webDriver.findElement(productAvailabilityStoreStockBy);
        }
        Assert.assertTrue("FAIL: The stock count is not displayed for item " + itemCode + " on page " + page,
                driver.isElementDisplayed(stockCount));
        Assert.assertTrue("FAIL: The Cart inventory message stock count displayed: '" + stockCount.getText()
                + "' did NOT contain the" + " expected text '" + ConstantsDtc.IN_STOCK + " on page " + page
                + "'!", stockCount.getText().contains(ConstantsDtc.IN_STOCK));
        LOGGER.info("assertStockCountTextForItem completed for item code " + itemCode + " on " + page);
    }

    /**
     * Clicks the first Check Nearby Stores link
     *
     * @param page web page
     */
    public void clickCheckNearbyStores(String page) {
        LOGGER.info("clickCheckNearbyStores started for " + page);
        driver.waitForPageToLoad();
        if (driver.isElementDisplayed(CartPage.cartItemStoreInventoryCheckBy, Constants.ONE))
            driver.moveToElementClick(webDriver.findElement(CartPage.cartItemStoreInventoryCheckBy));
        else
            driver.moveToElementClick(webDriver.findElement(checkInventoryBy));
        LOGGER.info("clickCheckNearbyStores completed for " + page);
    }

    /**
     * Clicks the Check Nearby Stores link for a specific item
     *
     * @param itemCode Item Code for item to click button for
     * @param page     web page
     */
    public void clickCheckNearbyStores(String itemCode, String page) {
        LOGGER.info("clickCheckNearbyStores started for " + page);
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        int elementIndex = 0;
        if (page.equalsIgnoreCase(ConstantsDtc.PLP)) {
            if (driver.getDisplayedElementsList(checkInventoryBy).size() > 0) {
                if (Config.isIe() || Config.isFirefox() || Config.isMobile()) {
                    driver.waitForMilliseconds();
                }
                WebElement grandParent = getPLPResultTopElement(itemCode);
                driver.jsScrollToElement(grandParent);
                driver.getDisplayedElement(grandParent, checkInventoryBy, Constants.ZERO).click();
            } else {
                LOGGER.info("'Check nearby stores' link was not present for item # " + itemCode);
            }
        } else if (page.equalsIgnoreCase(ConstantsDtc.PDP)) {
            webDriver.findElement(checkInventoryBy).click();
        } else {
            if (Config.isMobile())
                elementIndex = 1;
            WebElement checkNearbyStore = webDriver.findElements(CartPage.cartItemStoreInventoryCheckBy).
                    get(elementIndex);
            driver.jsScrollToElementClick(checkNearbyStore);
        }
        LOGGER.info("clickCheckNearbyStores completed for " + page);
    }

    /**
     * Get the payment form index based on specified payment form - 'primary' or 'secondary'
     *
     * @param paymentForm - 'primary' or 'secondary'. Determines which of the two forms to get field info from.
     * @return formIndex - integer representing the index to reference the form.
     */
    public int getPaymentFormIndex(String paymentForm) {
        LOGGER.info("getPaymentFormIndex started for " + paymentForm + " form");
        int formIndex = PRIMARY_FORM_INDEX;
        if (!Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            if (paymentForm.equalsIgnoreCase(Constants.SECONDARY))
                formIndex = SECONDARY_FORM_INDEX;
        }
        LOGGER.info("getPaymentFormIndex completed for " + paymentForm + " form");
        return formIndex;
    }

    /**
     * Verify the specified file is valid
     *
     * @param fileAlias Text string representing a file name
     * @return file        File path and file name
     */
    public String getFullPathFileName(String fileAlias) {
        LOGGER.info("getFullPathFileName started for file text string '" + fileAlias + "'");
        String file = Constants.fullPathFileNames.get(fileAlias);

        if (file == null) {
            Assert.fail("FAIL: Invalid file, '" + fileAlias + "'");
        }

        LOGGER.info("getFullPathFileName completed for '" + file + "'");
        return file;
    }

    /**
     * Verifies linktext is displayed on homepage
     *
     * @param linktext - 'linktext' link to validate
     */
    public void verifyLinkIsDisplayedOnPage(String linktext) {
        LOGGER.info("verifyLinkIsDisplayedOnPage started for link " + linktext);
        if (Config.isFirefox())
            driver.waitForMilliseconds();
        WebElement link = webDriver.findElement(By.linkText(linktext));
        if (link == null)
            link = webDriver.findElement(By.partialLinkText(linktext));
        if (link == null) {
            if (!driver.isElementDisplayed(driver.getElementWithText(CommonActions.dtLinkBy, linktext))
                    && !driver.isElementDisplayed(driver.getElementWithText(CommonActions.anchorTagBy, linktext))) {
                Assert.fail("FAIL : Link " + linktext + " did NOT display on the page");
            }
        }
        LOGGER.info("verifyLinkIsDisplayedOnPage completed for link " + linktext);
    }

    /**
     * Goes to appointment URL to schedule an appointment for the order received at store
     * Goes to Email URL and open inbox with the saved user id and password
     *
     * @param type Appointment/Email Url
     */
    public void openUrl(String type) {
        LOGGER.info("openUrl started for " + type);
        if (type.equalsIgnoreCase(ConstantsDtc.EMAIL)) {
            driver.getUrl(EMAIL_URL);
        } else if (type.contains(ConstantsDtc.APPOINTMENT)) {
            //launch the appointment Url
            String expectedOrderNumber = driver.scenarioData.getCurrentOrderNumber();
            driver.getUrl(ConstantsDtc.APPOINTMENT_URL + expectedOrderNumber + ConstantsDtc.APPOINTMENT_URL_STORE);
        } else if (type.equalsIgnoreCase(ConstantsDtc.STORYBOOK)) {
            driver.getUrl(STORYBOOK_URL);
        } else if (type.toLowerCase().contains(ConstantsDtc.CANCEL)) {
            driver.getUrl(driver.scenarioData.genericData.get(OrderXmls.ORDER_CANCEL_URL));
        } else {
            driver.getUrl(webDriver.getCurrentUrl() + ConstantsDtc.GEO_IP_URL + type);
        }
        driver.waitForPageToLoad();
        LOGGER.info("openUrl completed for " + type);
    }

    /**
     * Get the current URL
     *
     * @return String of current page URL
     */
    public String getCurrentUrl() {
        String currentUrl = webDriver.getCurrentUrl();
        LOGGER.info("getCurrentUrl returning url = " + currentUrl);
        return currentUrl;
    }

    /**
     * Determine if the specified on order message is in the array of expected on order messages
     *
     * @param inventoryMessage - the specified on order message
     * @return - true or false, the message is present in the array.
     */
    public boolean onOrderMessageFound(String inventoryMessage) {
        LOGGER.info("onOrderMessageFound started for '" + inventoryMessage + "' inventory message");
        boolean messageFound = false;
        for (String message : ConstantsDtc.ON_ORDER_MESSAGES) {
            if (inventoryMessage.contains(message)) {
                messageFound = true;
                break;
            }
        }
        LOGGER.info("onOrderMessageFound completed for '" + inventoryMessage + "' inventory message. " +
                "Message found = " + String.valueOf(messageFound));
        return messageFound;
    }

    /**
     * Verifying the Order after SuccessFully Placed
     *
     * @param successText :Text to be verified on successful order
     */
    public void verifyOrder(String successText) {
        LOGGER.info("verifyOrder started ");
        Assert.assertTrue("FAIL:Unable to Place the order Successfully \"" + successText + "\"But it has \"" +
                        webDriver.findElement(headerBy).getText(),
                driver.getElementWithText(headerBy, successText, Constants.TEN) != null);
        LOGGER.info("verifyOrder completed ");
    }

    /**
     * Selects the desired state dropdown
     *
     * @param state        State to select from the dropdown
     * @param dropdownFlow The flow by which the dropdown is encountered (Primary and Secondary Contact Information
     *                     in My Account Profile tab)
     */
    public void selectStateFromDropdown(String dropdownFlow, String state) {
        LOGGER.info("selectStateFromDropdown started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(dropdownBy, Constants.FIVE);

        int index = 0;
        if (dropdownFlow.equalsIgnoreCase(ConstantsDtc.CONTACT_INFORMATION)) {
            index = 2;
        }

        WebElement stateDropdownEle = webDriver.findElements(dropdownBy).get(index);
        driver.jsScrollToElement(stateDropdownEle);
        stateDropdownEle.click();
        driver.getElementWithText(dropdownOptionBy, state).click();
        LOGGER.info("selectStateFromDropdown completed");
    }

    /**
     * Clicks the default submit button for the checkout page
     */
    public void clickDefaultSubmitButton() {
        LOGGER.info("clickDefaultSubmitButton started");
        driver.waitForPageToLoad();
        driver.jsScrollToElementClick(submitButton);
        LOGGER.info("clickDefaultSubmitButton completed");
    }

    /**
     * Verify the image is displayed for text on page
     *
     * @param text product/Vehicle Details Banner
     * @param page Check Avialability/PLP page
     */
    public void assertImageDisplayed(String text, String page) {
        LOGGER.info("assertImageDisplayed started for " + text + " on page " + page);
        By imageElement;
        if (text.equalsIgnoreCase(ConstantsDtc.PRODUCT)) {
            imageElement = CheckAvailabilityPopupPage.productImageBy;
            driver.waitForClassPresent(driver.getByValue(imageElement), Constants.THIRTY);
        } else {
            imageElement = vehicleDisplayImageBy;
        }
        Assert.assertTrue("FAIL: Image not displayed for " + text + " on page " + page,
                driver.getDisplayedElement(imageElement, Constants.ZERO) != null);
        LOGGER.info("assertImageDisplayed completed for \" + text + \" on page \" + page");
    }

    /**
     * Verifies Vehicle Details Banner displayed with vehicle description/Edit vehicle link
     *
     * @param page PLP fitment results box or PDP page for banner selection
     * @param text Vehicle description/Edit Vehicle link inside the banner to verify
     */
    public void assertStringInVehicleDetailsBanner(String page, List text) {
        LOGGER.info("assertStringInVehicleDetailsBanner started on " + page + " with text " + text);
        driver.waitForPageToLoad();
        waitForSpinner();
        WebElement vehicleBanner = null;
        List<String> vehicleBannerText = text;
        for (int i = 0; i < text.size() - 1; i++) {
            if (vehicleBannerText.get(i).equalsIgnoreCase(Constants.EDIT)) {
                if (Config.isMobile()) {
                    vehicleBanner = editVehicleMobile;
                } else {
                    vehicleBanner = driver.getElementWithText(editVehicleBy, vehicleBannerText.get(i));
                }
                Assert.assertTrue("FAIL: The " + text + " Link not displayed in Vehicle Details Banner!"
                        + " on page " + page, driver.isElementDisplayed(vehicleBanner));
            } else {
                if (page.equalsIgnoreCase(ConstantsDtc.CHECKFIT)) {
                    vehicleBanner = webDriver.findElement(vehicleCheckFitDescriptionBy);
                } else {
                    vehicleBanner = webDriver.findElement(vehicleDescriptionBy);
                }
                String actualText = vehicleBanner.getText().toLowerCase().replace("\n", " ");
                Assert.assertTrue("FAIL: The displayed text '" + actualText + "' does not contain the " +
                                "expected text '" + text + "' on page " + page,
                        actualText.contains(vehicleBannerText.get(i).toLowerCase()));
            }
        }
        LOGGER.info("assertStringInVehicleDetailsBanner completed for" + page + " with text " + text);
    }

    /**
     * Verifies Vehicle Details Banner displayed with fitment banner for Wheels & tires on regular vehicle
     *
     * @param page      PLP or PDP page for banner selection
     * @param type      Original Equipment/Non OE with Tire/Wheel
     * @param sizeBadge O.E/for Non O.E the difference of size
     * @param text      Vehicle description/Edit Vehicle link text inside the banner to verify
     */
    public void assertFitmentBannerInVehicleDetailsBanner(String page, String type, String sizeBadge, String text) {
        LOGGER.info("assertFitmentBannerInVehicleDetailsBanner started on " + page + " for " + type + " " + text);
        driver.waitOneSecond();
        String fitmentBanner;
        String fitmentBannerLabel;
        if (type.contains(Constants.TIRE)) {
            fitmentBanner = webDriver.findElement(fitmentBannerTireBy).getText();
            if (type.contains(ConstantsDtc.NON_OE)) {
                fitmentBannerLabel = ConstantsDtc.OPTIONAL_TIRE_SIZE;
            } else {
                fitmentBannerLabel = ConstantsDtc.FACTORY_TIRE_SIZE;
            }
        } else {
            fitmentBanner = webDriver.findElement(fitmentBannerWheelBy).getText();
            fitmentBannerLabel = ConstantsDtc.WHEELS_NAME;
        }
        String fitmentBadge = webDriver.findElement(fitmentSizeBadgeBy).getText();
        Assert.assertTrue("FAIL: The vehicle details banner on " + page + " for " + type +
                " contains text: " + fitmentBanner + ", " + fitmentBadge + " does not match to expected text: "
                + text + ", " + fitmentBannerLabel + ", " + sizeBadge, fitmentBanner.contains(text)
                && fitmentBanner.toLowerCase().contains(fitmentBannerLabel.toLowerCase()) && fitmentBadge.contains(sizeBadge));
        LOGGER.info("assertFitmentBannerInVehicleDetailsBanner completed on " + page + " for " + type + " " + text);
    }

    /**
     * Verifies Vehicle Details Banner displayed with fitment banner for Wheels & tires for Staggered Vehicle
     *
     * @param page      PLP, My Vehicle Modal or PDP page for banner selection
     * @param type      Original Equipment/Non OE with Tire/Wheel
     * @param sizeBadge O.E/for Non O.E the difference of size
     * @param text      Vehicle description/Edit Vehicle link text inside the banner to verify
     */
    public void assertFitmentBannerInStaggeredVehicleDetailsBanner(String page, String type, String sizeBadge, String text) {
        LOGGER.info("assertFitmentBannerInStaggeredVehicleDetailsBanner started on " + page + " for " + type + " " + text);
        driver.waitOneSecond();
        String fitmentBannerLabel;
        String sizeBadgeText;
        if (type.equalsIgnoreCase(NON_OE_FRONT_TIRE)) {
            fitmentBannerLabel = OPTIONAL_FRONT_TIRE;
        } else if (type.equalsIgnoreCase(NON_OE_REAR_TIRE)) {
            fitmentBannerLabel = OPTIONAL_REAR_TIRE;
        } else if (type.contains(ConstantsDtc.REAR) && !type.contains(ConstantsDtc.NON_OE)) {
            fitmentBannerLabel = ConstantsDtc.FACTORY_REAR_TIRE;
        } else {
            fitmentBannerLabel = ConstantsDtc.FACTORY_FRONT_TIRE;
        }

        List<WebElement> tireType = webDriver.findElements(fitmentBannerSizeInfoBy);
        for (int i = 0; i < tireType.size(); i++) {
            String tireTypeFound = tireType.get(i).getText();
            if (type.contains(SET) && type.contains(ConstantsDtc.NON_OE) && (page.equalsIgnoreCase(MY_VEHICLES_MODAL))) {
                condition = (tireTypeFound.contains(text)
                        && (tireTypeFound.toLowerCase().contains(ConstantsDtc.WHEELS.toLowerCase())));
            } else {
                condition = tireTypeFound.contains(text)
                        && ((tireTypeFound.contains(fitmentBannerLabel)
                        && tireTypeFound.toLowerCase().contains(ConstantsDtc.WHEELS.toLowerCase())));
            }
            if (condition)
                break;
            else
                LOGGER.info("FAIL: The vehicle details banner on " + page + " for " + type +
                        " contains text: " + type + " does not contain expected text: " + text
                        + ", " + fitmentBannerLabel);
        }

        if (!page.equalsIgnoreCase(ConstantsDtc.PDP)) {

            if (!sizeBadge.toLowerCase().contains(Constants.NO.toLowerCase())) {
                if (driver.isElementDisplayed(fitmentVehicleResultsOptionalLabel, Constants.ZERO)) {
                    sizeBadgeText = fitmentVehicleResultsOptionalLabel.getText();
                } else {
                    if ((type.contains(ConstantsDtc.REAR)) && (!type.contains(ConstantsDtc.NON_OE))) {
                        sizeBadgeText = fitmentBannerSizeInfoRear.findElement(fitmentSizeBadgeBy).getText();
                    } else {
                        sizeBadgeText = fitmentBannerSizeInfo.findElement(fitmentSizeBadgeBy).getText();
                    }
                }
                Assert.assertTrue("FAIL: The vehicle details banner on " + page + " for " + type +
                                " contains text: " + sizeBadgeText + " does not match to expected text: " + sizeBadge,
                        sizeBadgeText.contains(sizeBadge));
            }
        } else {
            LOGGER.info("Size badge is absent on " + page);
        }
        LOGGER.info("assertFitmentBannerInStaggeredVehicleDetailsBanner completed on " + page + " for " + type + " " + text);
    }

    /**
     * Verify the modal popup displayed with header having matching text
     *
     * @param text :Text to be verified on modal popup
     */
    public void assertModalPopupIsDisplayed(String text) {
        LOGGER.info("assertModalPopupIsDisplayed started for " + text);
        driver.waitOneSecond();
        WebElement modalHeader;
        if (Config.isSafari() || Config.isFirefox()) {
            driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        }
        if (text.equalsIgnoreCase(CommonActions.ADDRESS_VERIFICATION)) {
            driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
            List<WebElement> avsPopup = webDriver.findElements(addressVerificationBy);
            if (avsPopup.size() > 1)
                modalHeader = avsPopup.get(1);
            else
                modalHeader = webDriver.findElement(addressVerificationBy);
            driver.resetImplicitWaitToDefault();
        } else {
            modalHeader = driver.getElementWithText(dtModalTitleBy, text);
        }
        Assert.assertTrue("FAIL: " + text + " modal popup not displayed!",
                driver.isElementDisplayed(modalHeader));
        if (text.equalsIgnoreCase(ConstantsDtc.TAX_POLICY)) {
            String taxPolicyContent = driver.getParentElement(driver.getParentElement(modalHeader)).getText();
            Assert.assertTrue("FAIL: Displayed tax policy: " + taxPolicyContent + "does not contain the expected" +
                            " tax policy content: " + ConstantsDtc.TAX_POLICY_CONTENT,
                    taxPolicyContent.contains(ConstantsDtc.TAX_POLICY_CONTENT));
        }
        LOGGER.info("assertModalPopupIsDisplayed completed for " + text);
    }

    /**
     * Close the Tax Policy modal popup
     */
    public void closeTaxPolicyPopup() {
        LOGGER.info("closeTaxPolicyPopup started");
        WebElement closeTaxPolicy = driver.getParentElement(driver.getElementWithText(headerSecondBy,
                ConstantsDtc.TAX_POLICY)).findElement(dtModalCloseBy);
        closeTaxPolicy.click();
        LOGGER.info("closeTaxPolicyPopup completed");
    }

    /**
     * Verify page header text displayed for page type
     *
     * @param pageType web page
     */
    public void assertPageIsDisplayed(String pageType) {
        LOGGER.info("assertPageisDisplayed started");
        driver.waitForPageToLoad();
        WebElement page = driver.getElementWithText(headerBy, pageType);
        Assert.assertTrue("FAIL: " + pageType + " page is not displayed!",
                driver.isElementDisplayed(page));
        LOGGER.info("assertPageisDisplayed completed");
    }

    /**
     * Clicks the link with link Text
     *
     * @param linkText text appears for the link
     */
    public void clickLink(String linkText) {
        LOGGER.info("clickLink started for " + linkText);
        driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
        try {
            WebElement link = driver.getElementWithText(CommonActions.dtLinkBy, linkText, Constants.FIVE);
            if (link == null)
                driver.clickElementWithLinkText(linkText);
            else
                driver.jsScrollToElementClick(link);
        } catch (Exception e) {
            driver.clickElementWithLinkText(linkText);
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("clickLink completed for " + linkText);
    }

    /**
     * Verify quantity for the product on cart page/Check Availability popup modal
     *
     * @param item     product code
     * @param quantity Quantity to update
     * @param page     Cart Page/ Check Availability Popup
     */
    public void assertQuantityForItem(String item, String quantity, String page) {
        LOGGER.info("assertQuantityForItem started for " + item + " on page " + page);
        driver.waitForPageToLoad();
        List<WebElement> itemQuantities;
        if (page.equalsIgnoreCase(ConstantsDtc.CHECK_AVAILABILITY)) {
            itemQuantities = webDriver.findElements(quantityBy);
        } else {
            itemQuantities = getProductSectionTopElement(item).
                    findElements(CartPage.cartItemQuantityBy);
        }
        for (WebElement itemQuantity : itemQuantities) {
            String actualQuantity = itemQuantity.getAttribute(Constants.VALUE);
            Assert.assertTrue("FAIL: Item quantity didn't match (displayed:-> "
                    + actualQuantity + " expected:->  " + quantity + ")", actualQuantity.equals(quantity));
            LOGGER.info("assertQuantityForItem completed for " + item + " on page " + page);
        }
    }

    /**
     * Asserts that the current product quantity matches desired/default
     * quantity in Product List Page/Compare tire Review page
     *
     * @param value The value to verify
     * @param page  Cart Page/ Check Availability Popup
     */
    public void assertProductQuantity(String value, String page) {
        LOGGER.info("assertProductQuantity started on page " + page);
        List<WebElement> itemQuantity;
        if (page.equalsIgnoreCase(ConstantsDtc.COMPARE_TIRE_REVIEWS)) {
            itemQuantity = webDriver.findElements(CompareTireReviewsPage.comparisonQuantityBy);
        } else {
            driver.waitForElementVisible(CommonActions.productQuantityBox);
            itemQuantity = webDriver.findElements(CommonActions.productQuantityBoxBy);
        }
        for (WebElement quantity : itemQuantity) {
            Assert.assertTrue("FAIL : Staggered default quantity : " + value
                            + " didn't match the display quantity" + quantity.getAttribute(Constants.VALUE),
                    quantity.getAttribute(Constants.VALUE).equals(value));
        }
        LOGGER.info("assertProductQuantity completed on page " + page);
    }

    /**
     * Verify message when service response invalid
     *
     * @param serviceName name of the service
     * @param page        web page
     **/
    public void assertMessage(String serviceName, String page) {
        LOGGER.info("assertMessage started on page " + page + " for " + serviceName + "  down!");
        String expectedMessage = ConstantsDtc.TAX_OFFLINE_MESSAGE;
        String actualMessage = webDriver.findElement(dtMessageWarningBy).getText();
        if (page.equalsIgnoreCase(ConstantsDtc.CHECKOUT)) {
            driver.verifyTextDisplayed(AppointmentPage.activeSectionTitleElement,
                    CheckoutPage.SHIPPING_DETAILS);
        }
        Assert.assertTrue("FAIL: For " + serviceName + " invalid response, displayed message: "
                        + actualMessage + " does not match to expected message: " + expectedMessage,
                actualMessage.equals(expectedMessage));
        LOGGER.info("assertMessage completed on page " + page + " for " + serviceName + "  down!");
    }

    /**
     * Verify fee and Tax labels on web page for regular and staggered items
     *
     * @param text Fee/Tax Labels
     * @param page Web Page
     */
    public void assertLabelDisplayed(String text, String page) {
        LOGGER.info("assertLabelDisplayed started for " + text + " on page " + page);
        List<WebElement> elementLabels;
        if (text.toLowerCase().contains(Constants.FEE) && !page.equalsIgnoreCase(ConstantsDtc.CHECKOUT)) {
            elementLabels = driver.getElementsWithText(CartPage.childTip, text);
        } else if (text.toLowerCase().contains(Constants.FEE) && page.equalsIgnoreCase(ConstantsDtc.CHECKOUT)) {
            elementLabels = driver.getElementsWithText(CartPage.feeDetailsItemsRowLabelBy, text);
        } else if (!text.toLowerCase().contains(Constants.FEE) && !page.equalsIgnoreCase(ConstantsDtc.CHECKOUT)) {
            elementLabels = driver.getElementsWithText(CartPage.cartSummaryBreakDownNameBy, text);
        } else {
            elementLabels = driver.getElementsWithText(CheckoutPage.taxLabelOnCheckoutBy, text);
        }
        for (WebElement elementLabel : elementLabels) {
            Assert.assertTrue("FAIL: Expected label: " + text + " not displayed!, on " + page,
                    driver.isElementDisplayed(elementLabel));
        }
        LOGGER.info("assertLabelDisplayed completed for " + text + " on page " + page);
    }

    /***
     * Sets the Regional based different sales tax factors
     *
     * @param customer     Type of customer to pull data from and pass to payment fields
     *
     */
    public void setRegionalTaxesFactor(Customer customer) {
        LOGGER.info("setRegionalTaxesFactor started");
        if (Config.getDataSet().equalsIgnoreCase(Constants.QA)) {
            stateTaxString = customer.stateTaxQA;
            cityTaxString = customer.cityTaxQA;
            otherTaxString = customer.otherTaxQA;
        } else {
            stateTaxString = customer.stateTaxSTG;
            cityTaxString = customer.cityTaxSTG;
            otherTaxString = customer.otherTaxSTG;
        }
        LOGGER.info("setRegionalTaxesFactor completed");
    }

    /**
     * Switch to main browser window
     */
    public void switchToMainBrowserWindow() {
        LOGGER.info("switchToMainBrowserWindow started");
        String mainUrl = Config.getBaseUrl();
        String currentUrl = "";
        do {
            Set allHandles = webDriver.getWindowHandles();
            Iterator iter = allHandles.iterator();
            String browserHandle = iter.next().toString();
            webDriver.switchTo().window(browserHandle);
            try {
                currentUrl = webDriver.getCurrentUrl();
            } catch (Exception e) {
                currentUrl = "";
            }
        } while (!CommonUtils.containsIgnoreCase(currentUrl, mainUrl));
        LOGGER.info("switchToMainBrowserWindow completed");
    }

    /**
     * Fills out all address fields and Continue to next page
     *
     * @param customer        Type of customer dtc.data to use in Customers class
     * @param shippingAddress True / False on whether or not to use Customer's shipping address info
     * @param clickContinue   True / False whether to click continue after the form is populated
     */
    public void enterAddressForCustomer(Customer customer, boolean shippingAddress, boolean clickContinue) {
        LOGGER.info("enterAddressForCustomer started for '" + customer + "' with click continue = " + clickContinue);
        String submitButtonText = populateAddressFormFieldsForCustomer(customer, shippingAddress);
        if (Config.getSiteRegion().equals(Constants.DTD)) {
            setCustomerScenarioData();
        }
        if (clickContinue) {
            clickFormSubmitButtonByText(submitButtonText);
            if (driver.isElementDisplayed(driver.getElementWithText(CommonActions.formSubmitButtonBy,
                    ConstantsDtc.USE_USPS_CORRECTED_ADDRESS), Constants.THREE))
                clickFormSubmitButtonByText(ConstantsDtc.USE_USPS_CORRECTED_ADDRESS);
            driver.waitForPageToLoad();
        }
        LOGGER.info("enterAddressForCustomer completed for '" + customer + "' with click continue = " + clickContinue);
    }

    /**
     * Enter the address data for a specified customer into the address form
     *
     * @param customer        Type of customer dtc.data to use in Customers class
     * @param shippingAddress True / False on whether or not to use Customer's shipping address info
     * @return String value indicating the text of the submit button for the form
     */
    public String populateAddressFormFieldsForCustomer(Customer customer, boolean shippingAddress) {
        LOGGER.info("populateAddressFormFieldsForCustomer started for " + customer + " customer");
        driver.waitForPageToLoad();
        ArrayList<WebElement> addressFields = null;
        ArrayList<String> customerInfo = null;

        if (driver.isElementDisplayed(expandOptionalAddressLine, Constants.ZERO)) {
            driver.jsScrollToElement(expandOptionalAddressLine);
            expandOptionalAddressLine.click();
        }

        webDriver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
        boolean isDisplayed = webDriver.findElements(MyAccountPage.checkoutFormCreateAccountBy).size() > 0;
        String submitButtonText = submitButton.getText();
        driver.resetImplicitWaitToDefault();

        if (shippingAddress) {
            if ((submitButtonText.equalsIgnoreCase(ConstantsDtc.DONE_EDITING))
                    || (submitButtonText.equalsIgnoreCase(ConstantsDtc.CREATE_AN_ACCOUNT))) {
                addressFields = new ArrayList<>(Arrays.asList(firstName, lastName, password, phone, address1, address2, zip, city));
                customerInfo = new ArrayList<>(Arrays.asList(customer.firstName, customer.lastName, customer.emailPassword,
                        customer.phone.replaceAll("-", ""), customer.address1, customer.address2,
                        customer.zip, customer.city));
                selectStateFromDropdown(ConstantsDtc.CONTACT_INFORMATION, customer.state);
                submitButtonText = ConstantsDtc.ADD_CONTACT_INFO;
            } else if (submitButtonText.equalsIgnoreCase(ConstantsDtc.PLACE_ORDER) ||
                    submitButtonText.equalsIgnoreCase(ConstantsDtc.CONTINUE_TO_PAYMENT)) {
                if (isDisplayed) {
                    addressFields = new ArrayList<>(Arrays.asList(firstName, lastName, email, phone, address1,
                            orderSummaryZipCode, city));
                    customerInfo = new ArrayList<>(Arrays.asList(customer.firstName, customer.lastName, customer.email,
                            customer.phone.replaceAll("-", ""), customer.address1, customer.zip, customer.city));
                } else {
                    if (submitButtonText.equalsIgnoreCase(ConstantsDtc.PLACE_ORDER) ||
                            submitButtonText.equalsIgnoreCase(ConstantsDtc.CONTINUE_TO_PAYMENT)) {
                        addressFields = new ArrayList<>(Arrays.asList(firstName, lastName, email, phone, address1,
                                orderSummaryZipCode, city));
                        customerInfo = new ArrayList<>(Arrays.asList(customer.firstName, customer.lastName, customer.email,
                                customer.phone.replaceAll("-", ""), customer.address1, customer.zip, customer.city));
                    } else {
                        addressFields = new ArrayList<>(Arrays.asList(firstName, lastName, email, phone));
                        customerInfo = new ArrayList<>(Arrays.asList(customer.firstName, customer.lastName, customer.email,
                                customer.phone.replaceAll("-", "")));
                    }
                }
            } else {
                addressFields = new ArrayList<>(Arrays.asList(firstName, lastName, address1, address2, zip, email, phone));
                customerInfo = new ArrayList<>(Arrays.asList(customer.firstName, customer.lastName, customer.address1,
                        customer.address2, customer.zip, customer.email, customer.phone.replaceAll("-", "")));
                submitButtonText = ConstantsDtc.CONTINUE_TO_SHIPPING;
            }
        } else {
            driver.jsScrollToElement(paymentDetailBillingContainer);
            addressFields = new ArrayList<>(Arrays.asList(billingAddress1, billingAddress2, billingZipCode));
            customerInfo = new ArrayList<>(Arrays.asList(customer.address1, customer.address2, customer.zip));
            submitButtonText = ConstantsDtc.PLACE_ORDER;
        }

        // TODO:  sendKeys does not work for these fields with IE
        // 1) Talk to developers about what is different about these fields?  Can they be changed?
        // 2) Find a solution other than sendKeys.  Robot and JavascriptExecutor have been tried.
        for (int i = 0; i < addressFields.size(); i++) {
            if (!customerInfo.get(i).equalsIgnoreCase("")) {
                if (Config.isAndroidTablet() || Config.isAndroidPhone())
                    driver.waitForMilliseconds();
                if (Config.isSafari()) {
                    driver.jsScrollToElement(addressFields.get(i));
                    driver.waitForMilliseconds(Constants.TWO_THOUSAND);
                }
                driver.jsScrollToElementClick(addressFields.get(i), false);
                clearAndPopulateEditField(addressFields.get(i), customerInfo.get(i), Constants.ONE_THOUSAND);
                while (formGroupErrorMessageDisplayedNotDisplayed(CheckoutPage.ZIP_CODE_IS_INVALID, true)) {
                    clearAndPopulateEditField(addressFields.get(i), customerInfo.get(i));
                }
                if (customerInfo.get(i).equalsIgnoreCase(customer.zip)) {
                    int counter = 0;
                    while (driver.getElementWithText(dropdownBy, customer.state) == null && counter < Constants.TEN) {
                        driver.waitOneSecond();
                        counter++;
                    }
                }
            }
        }

        if (Config.isMobile()) {
            //TODO: On Mobile, the APO & FPO customers state is not accepted
            //TODO: (con't) the dropdown option appears and is selectable, but always throws an address issue when
            //TODO: (con't) attempting to go to delivery options. Workaround forces DT site to make a suggestion for
            //TODO: (con't) the APO / FPO customer's address, which is then accepted. CANNOT reproduce manually on
            //TODO: (con't) chrome emulator or android emulator. A Dragon.
            if (customer.lastName.equalsIgnoreCase(Constants.APO_TEST)
                    || customer.lastName.equalsIgnoreCase(Constants.FPO_TEST)) {
                customer.state = Constants.GEORGIA;
            }
        }

        if (!(submitButton.getText().equalsIgnoreCase(ConstantsDtc.CONTINUE_TO_PAYMENT))) {
            formListSelectItem(ConstantsDtc.COUNTRY, customer.country);

            if (shippingAddress) {
                formListSelectItem(ConstantsDtc.PHONE_TYPE, customer.phoneType);
            }
        }

        clickBrowserBody();
        driver.waitForMilliseconds();
        LOGGER.info("populateAddressFormFieldsForCustomer completed for " + customer + " customer");
        return submitButtonText;
    }

    /**
     * Select an item from a form drop-down list box
     *
     * @param fieldLabel The label on the field
     * @param selection  The item to be selected
     */
    public void formListSelectItem(String fieldLabel, String selection) {
        LOGGER.info("formListSelectItem started: Selecting " + selection + " from " + fieldLabel + " drop down list");
        WebElement dropDown = driver.getElementWithText(formGroupBy, fieldLabel).findElement(dropdownBy);
        while (!dropDown.getText().equalsIgnoreCase(selection)) {
            driver.jsScrollToElement(dropDown);
            dropDown.click();
            driver.clickElementWithText(CommonActions.dropdownOptionBy, selection);
            driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        }
        LOGGER.info("formListSelectItem completed: Selecting " + selection + " from " + fieldLabel + " drop down list");
    }

    /**
     * Edits specified address field
     *
     * @param field      The address field to edit
     * @param fieldValue The value to set the address field to
     */
    public void editAddressField(String field, String fieldValue) {
        LOGGER.info("editAddressField started");
        WebElement element = null;
        boolean editField = true;
        switch (field) {
            case FIRST_NAME:
                element = firstName;
                break;
            case LAST_NAME:
                element = lastName;
                break;
            case ADDRESS_LINE_1:
                element = address1;
                break;
            case ADDRESS_LINE_2:
                element = address2;
                break;
            case ZIP:
                element = zip;
                break;
            case EMAIL:
                element = email;
                break;
            case ConstantsDtc.STATE:
                selectDropDownValue(STATE_DROP_DOWN_WEB_STRING, fieldValue);
                editField = false;
                break;
            case COUNTRY:
                formListSelectItem(ConstantsDtc.COUNTRY, fieldValue);
                editField = false;
                break;
            case Constants.PHONE:
                selectDropDownValue(PHONE_TYPE_DROP_DOWN_WEB_STRING, fieldValue);
                editField = false;
                break;
            case ConstantsDtc.PRIMARY_DRIVING_LOCATION:
                element = pdlZipcode;
                break;
            case ConstantsDtc.MILES_DRIVEN_PER_YEAR:
                element = PdlxFitmentPopupPage.pdlInputMiles;
                break;
            default:
                LOGGER.info("FAIL: The text field was not updated with " + fieldValue);
        }

        if (editField) {
            clearAndPopulateEditField(element, fieldValue);
            clickBrowserBody();
        }

        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        LOGGER.info("editAddressField completed");
    }

    /**
     * Verify product Banner for treadwell TOP RECOMMENDATION and RECOMMENDED ranking
     *
     * @param text    TOP RECOMMENDATION/RECOMMENDED
     * @param ranking treadwell Recommended ranking
     * @param page    PLP/PDP
     */
    public void assertProductBanner(String text, String ranking, String page) {
        LOGGER.info("verifyProductBanner started for " + text + " on page " + page);
        List<WebElement> productBanners;
        WebElement topRecommendationRibbon = topRecommendation;
        WebElement recommendedRibbon = driver.getElementWithText(bannerRecommendationBy, ranking);
        if (page.equalsIgnoreCase(ConstantsDtc.COMPARE_PRODUCTS)) {
            productBanners = webDriver.findElement(CompareProductsPage.compareMainSectionBy).findElements(bannerRecommendationBy);
            if (Config.isMobile()) {
                productBanners = webDriver.findElement(CompareProductsPage.compareMobileProductsBy).findElements(bannerRecommendationBy);
                topRecommendationRibbon = topRecommendationMobile;
                recommendedRibbon = driver.getElementWithText(bannerRecommendationMobileBy, ranking);
            }
        } else {
            productBanners = webDriver.findElements(bannerRecommendationBy);
        }
        if (text.equalsIgnoreCase(ConstantsDtc.TOP_RECOMMENDATION)) {
            Assert.assertTrue("FAIL: Top recommendation ribbon was not displayed for first product in the list",
                    driver.isElementDisplayed(topRecommendationRibbon));
        } else {
            for (WebElement banner : productBanners) {
                if (banner.getText().equalsIgnoreCase(ranking)) {
                    Assert.assertTrue("FAIL: Product banner contains treadwell Recommended ranking: "
                                    + banner.getText() + " does not match to expected ranking: " + ranking,
                            driver.isElementDisplayed(recommendedRibbon));
                    break;
                }
            }
        }
        LOGGER.info("verifyProductBanner completed for " + text + " on page " + page);
    }

    /**
     * Select option on treadwell section on Compare Products/PDP Page
     *
     * @param option View Ride Rating Description popup/View Tire Life & Cost Rating Description/View Stopping
     *               Distance Rating Description
     * @param page   Compare Products/PDP
     */
    public void selectOptionOnTreadwellSection(String option, String page) {
        LOGGER.info("selectOptionOnTreadwellSection started for " + option + " on page " + page);
        WebElement viewDescription = driver.getElementWithText(pdpDataViewDescriptionBy, option);
        driver.jsScrollToElement(viewDescription);
        viewDescription.click();
        LOGGER.info("selectOptionOnTreadwellSection completed for " + option + " on page " + page);
    }

    /**
     * Verify the element with specified text doesn't exist
     *
     * @param by   By selector for element
     * @param text Text element should not contain
     */
    public void assertElementWithTextNotExists(By by, String text) {
        LOGGER.info("assertElementWithTextNotExists started with '" + text + "' text");
        Assert.assertFalse("FAIL:'" + text + "' is displayed. Expected not to be displayed here.",
                driver.checkIfElementContainsText(by, text));
        LOGGER.info("assertElementWithTextNotExists completed with '" + text + "' text");
    }

    /**
     * Verify the by element is not displayed
     *
     * @param by          By selector for element to check
     * @param elementText The element type to check
     */
    public void assertByElementNotDisplayed(By by, String elementText) {
        LOGGER.info("assertByElementNotDisplayed started with '" + elementText + "' element");
        Assert.assertFalse("FAIL: '" + elementText + "' is displayed. Expected not to be displayed here.",
                driver.isElementDisplayed(by));
        LOGGER.info("assertByElementNotDisplayed completed with '" + elementText + "' element");
    }

    /**
     * Verifies the element is displayed
     *
     * @param elementName Name of the element which is displayed
     * @param webElement  The Web element to check
     */
    public void assertWebElementDisplayed(WebElement webElement, String elementName) {
        LOGGER.info("assertWebElementDisplayed started with '" + elementName + "' element");
        Assert.assertTrue("FAIL: '" + elementName + "' is not displayed. Expected to be displayed here.",
                driver.isElementDisplayed(webElement));
        LOGGER.info("assertWebElementDisplayed completed with '" + elementName + "' element");
    }

    /**
     * Verify the web element is not displayed
     *
     * @param webElement  The web element to check
     * @param elementText The element type to check
     */
    public void assertWebElementNotDisplayed(WebElement webElement, String elementText) {
        LOGGER.info("assertWebElementNotDisplayed started with '" + elementText + "' element");
        Assert.assertFalse("FAIL: '" + elementText + "' is displayed. Expected not to be displayed here.",
                driver.isElementDisplayed(webElement));
        LOGGER.info("assertWebElementNotDisplayed completed with '" + elementText + "' element");
    }

    /**
     * This method will scroll to web element item subtotal in checkout page where all items are not visible unless we
     * scroll. This method will scroll to each item and will take a screenshot of each item added to cart.
     *
     * @param folderName - Name of the screenshot folder
     * @param imageName  - Name of the image
     * @throws Throwable
     */
    public void screenshotCoveringAllItemsInCheckOutPage(String folderName, String imageName) throws Throwable {
        LOGGER.info("screenshotCoveringAllItemsInCheckOutPage started: " + folderName + ", " + imageName);
        List<WebElement> elements = webDriver.findElements(itemSubtotalBy);
        List<WebElement> elements1 = webDriver.findElements(feeslabelBy);
        for (int i = 0; i < elements.size(); i++) {
            if (driver.isElementDisplayed(elements.get(i))) {
                driver.jsScrollToElement(elements.get(i));
                if (elements1.size() > i) {
                    if (elements1.get(i).getText().equalsIgnoreCase(SHOW_FEE_DETAILS)) {
                        elements1.get(i).click();
                    }
                }
                driver.waitForPageToLoad();
                actions.moveToElement(elements.get(i)).perform();
                crossApplicationData.deleteSpecificFileWhichContainsName(folderName, imageName + " - " + i);
                driver.takeScreenshotAndSaveToALocation(Constants.PARENT_FOLDER_NAME_SCREENSHOT, folderName, imageName + " - " + i, Constants.PNG_EXTENSION);
            }
        }
        LOGGER.info("screenshotCoveringAllItemsInCheckOutPage completed");
    }

    /**
     * Clear the contents of a specified edit field
     *
     * @param editField The edit field to clear
     */
    public void clearEditField(WebElement editField) {
        LOGGER.info("clearEditField started clearing the '" + editField + "' field.");
        if (isReadOnly(editField))
            return;
        if (Config.isMobile())
            driver.jsScrollToElement(editField);

        driver.waitForElementClickable(editField);
        editField.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        if (!editField.getAttribute(Constants.VALUE).isEmpty()) {
            Actions builder = new Actions(webDriver);
            editField.click();
            editField.sendKeys(Keys.END);
            do {
                builder.sendKeys(Keys.BACK_SPACE).release().perform();
            } while (!editField.getAttribute(Constants.VALUE).isEmpty());
        }
        driver.waitForMilliseconds(Constants.ONE_HUNDRED);
        if (!editField.getAttribute(Constants.VALUE).isEmpty()) {
            clearEditField(editField);
        }
        LOGGER.info("clearEditField completed clearing the '" + editField + "' field.");
    }

    /**
     * Clear and populate a specified edit field
     *
     * @param editField The edit field to clear and populate
     * @param value     The value to enter into the field
     */
    public void clearAndPopulateEditField(WebElement editField, String value) {
        clearAndPopulateEditField(editField, value, Constants.ZERO);
    }

    /**
     * Clear and populate a specified edit field
     *
     * @param editField                  The edit field to clear and populate
     * @param value                      The value to enter into the field
     * @param millisecondsWaitAfterClear Milliseconds to wait between clearing the field and entering the value
     */
    public void clearAndPopulateEditField(WebElement editField, String value, int millisecondsWaitAfterClear) {
        LOGGER.info("clearAndPopulateEditField started entering '" + value + "' into the '" + editField + "' field.");
        if (isReadOnly(editField))
            return;
        clearEditField(editField);
        driver.waitForMilliseconds(millisecondsWaitAfterClear);
        editField.sendKeys(value);
        LOGGER.info("clearAndPopulateEditField completed entering '" + value + "' into the '" + editField + "' field.");
    }

    /**
     * Clear and populate a specified edit field
     *
     * @param editField The edit field to clear and populate
     * @param value     The value to enter into the field
     */
    public void clearAndPopulateEditField(WebElement editField, int value) {
        clearAndPopulateEditField(editField, String.valueOf(value));
    }


    /**
     * Switch to window
     *
     * @param window Paypal/Outlook
     */
    public void switchToWindow(String window) {
        LOGGER.info("switchToWindow started for " + window);
        driver.waitForPageToLoad();
        String mainHandle = webDriver.getWindowHandle();
        int count = 1;
        int attempts = 0;
        Set allHandles = webDriver.getWindowHandles();

        do {
            driver.waitForMilliseconds();
            allHandles = webDriver.getWindowHandles();
            count = allHandles.size();
            attempts++;
        } while (count == 1 && attempts < 10);

        if (CommonUtils.containsIgnoreCase(window, ConstantsDtc.OUTLOOK) &&
                webDriver.getCurrentUrl().contains(OUTLOOK_URL)) {
            LOGGER.info("switchToWindow completed for " + window);
            return;
        }

        if (count == 1) {
            Assert.assertTrue("FAIL: " + window + " window not opened!", count > 1);
        } else {
            Iterator iter = allHandles.iterator();
            while (iter.hasNext()) {
                String popupHandle = iter.next().toString();
                if (!popupHandle.contains(mainHandle)) {
                    webDriver.switchTo().window(popupHandle);
                    driver.waitForMilliseconds();
                    if (CommonUtils.containsIgnoreCase(window, ConstantsDtc.OUTLOOK) &&
                            webDriver.getCurrentUrl().contains(OUTLOOK_URL)) {
                        break;
                    }
                }
            }
        }
        LOGGER.info("switchToWindow completed for " + window);
    }

    /**
     * Input text value in a specified field
     *
     * @param element              The field to enter text
     * @param text                 The text to enter
     * @param clickElement         true/false whether to click element
     * @param waitForClickableTime The amount of time to wait for an item to be clickable
     */
    public void enterTextIntoField(WebElement element, String text, boolean clickElement, int waitForClickableTime) {
        driver.enterTextIntoField(element, text, clickElement, waitForClickableTime);
    }

    /**
     * Input a text value in a specified field
     *
     * @param element The field to enter text
     * @param text    The text to enter
     */
    public void enterTextIntoField(WebElement element, String text) {
        driver.enterTextIntoField(element, text);
    }

    /**
     * Click the lower price link that may have either "Found it Lower?" or "Instant Price Match" text
     */
    public void clickLowerPriceLink(String linkText) {
        driver.getElementWithText(CommonActions.lowerPriceLinkBy, linkText).click();
    }

    /**
     * Click the Add Vehicle/Load More Reviews Button Link
     */
    public void clickButtonLink(String buttonName) {
        LOGGER.info("clickButtonLink started for " + buttonName);
        String buttonText;
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        if (buttonName.equalsIgnoreCase(ConstantsDtc.ADD_VEHICLE)) {
            buttonText = ADD_VEHICLE_BUTTON_TEXT;
        } else if (buttonName.equalsIgnoreCase(ConstantsDtc.LOAD_MORE_REVIEWS)) {
            buttonText = ConstantsDtc.LOAD_MORE_REVIEWS;
        } else {
            buttonText = buttonName;
        }
        WebElement button = driver.getElementWithText(dtButtonLgBy, buttonText);
        driver.jsScrollToElementClick(button);
        LOGGER.info("clickButtonLink completed for " + buttonName);
    }

    /**
     * Verifies the expected vehicle is displayed on specified page
     *
     * @param expectedVehicle Vehicle description to be verified
     * @param page            The page on which the vehicle is being verified
     */
    public void verifyVehicleDescription(String expectedVehicle, String page) {
        LOGGER.info("verifyVehicleDescription started for '" + expectedVehicle + "' on '" + page + "' page");
        driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
        String strvehicleDetails = webDriver.findElement(vehicleDescriptionBy).getText().toLowerCase().replace("\n", " ").
                replace(ConstantsDtc.EDIT_VEHICLE.toLowerCase(), " ").trim();
        driver.resetImplicitWaitToDefault();
        Assert.assertTrue("FAIL: Vehicle description is not correct",
                CommonUtils.containsIgnoreCase(strvehicleDetails.toLowerCase().replace(" ", ""),
                        expectedVehicle.toLowerCase().replace(" ", "")));
        LOGGER.info("verifyVehicleDescription completed for '" + expectedVehicle + "' on '" + page + "' page");
    }

    /**
     * Determine whether an element is readonly
     *
     * @param element - The element being evaluated
     * @return boolean - true or false whether the element is readonly
     */
    public boolean isReadOnly(WebElement element) {
        LOGGER.info("isReadOnly started for '" + element + "'");
        boolean returnVal = false;
        if (CommonUtils.containsIgnoreCase(element.getAttribute(Constants.CLASS), Constants.READ_ONLY))
            returnVal = true;
        LOGGER.info("isReadOnly completed for '" + element + "'");
        return returnVal;
    }

    /**
     * Verifies the UI on Wheel Configurator page & Store Details page.
     *
     * @param inputToVerify Vehicle Image/Close Icon/Share Icon
     * @param page          web page
     */
    public void assertElementOnpage(String inputToVerify, String page) {
        LOGGER.info("assertElementOnpage started for " + inputToVerify + " on page " + page);
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        WebElement element = null;
        if (inputToVerify.contains(ConstantsDtc.VEHICLE_IMAGE)) {
            element = webDriver.findElement(WheelConfiguratorPage.vehicleImageBy);
        } else if (inputToVerify.contains(ConstantsDtc.CLOSE_SYMBOL)) {
            element = WheelConfiguratorPage.wheelConfigClose;
        } else if (inputToVerify.contains(ConstantsDtc.SHARE_ICON)
                && page.equalsIgnoreCase(ConstantsDtc.STORE_DETAILS)) {
            element = driver.getElementWithText(StoreDetailsPage.shareStoreBy, ConstantsDtc.SHARE);
        } else if (inputToVerify.contains(ConstantsDtc.DIRECTIONS)
                && page.equalsIgnoreCase(ConstantsDtc.STORE_DETAILS)) {
            element = driver.getElementWithText(StoreDetailsPage.actionsBy, ConstantsDtc.DIRECTIONS);
        } else if (inputToVerify.contains(ConstantsDtc.SHARE_ICON) && page.contains(Constants.WHEEL)) {
            element = WheelConfiguratorPage.shareOption;
        } else if (inputToVerify.contains(ConstantsDtc.DIRECTIONS)
                && page.equalsIgnoreCase(ConstantsDtc.STORE_LOCATOR)) {
            element = webDriver.findElement(StoreLocatorPage.selectedStoreBy).
                    findElement(StoreDetailsPage.actionsBy);
        } else if (inputToVerify.contains(ConstantsDtc.SHARE_ICON)
                && page.equalsIgnoreCase(ConstantsDtc.STORE_LOCATOR)) {
            element = webDriver.findElement(StoreLocatorPage.selectedStoreBy).
                    findElement(StoreDetailsPage.shareStoreBy);
        } else if (inputToVerify.equalsIgnoreCase(ConstantsDtc.SCHEDULE_APPOINTMENT)
                && page.equalsIgnoreCase(ConstantsDtc.STORE_LOCATOR)) {
            element = webDriver.findElement(StoreLocatorPage.selectedStoreBy).
                    findElement(StoreLocatorPage.scheduleAppointmentBy);
        }
        Assert.assertTrue("FAIL: Expected element for " + inputToVerify + " not displayed on page " + page,
                driver.isElementDisplayed(element, 5));
        LOGGER.info("assertElementOnpage completed for " + inputToVerify + " on page " + page);
    }

    /**
     * Select option on Wheel Configurator/Store Details page
     *
     * @param option View Details|Share|X
     * @param page   web page
     */
    public void selectOption(String option, String page) {
        LOGGER.info("selectOption started for " + option + " on page " + page);
        if (option.equals(ConstantsDtc.VIEW_DETAILS)) {
            WheelConfiguratorPage.viewDetails.click();
        } else if (option.equals(ConstantsDtc.SHARE) && page.contains(Constants.WHEEL)) {
            WheelConfiguratorPage.shareOption.click();
        } else if (option.equals(ConstantsDtc.CLOSE_SYMBOL)) {
            WheelConfiguratorPage.wheelConfigClose.click();
        } else if (option.equals(ConstantsDtc.SHARE) && page.equalsIgnoreCase(ConstantsDtc.STORE_DETAILS)) {
            webDriver.findElement(StoreDetailsPage.shareStoreBy).click();
        } else if (option.equals(ConstantsDtc.DIRECTIONS) && page.equalsIgnoreCase(ConstantsDtc.STORE_DETAILS)) {
            webDriver.findElement(StoreDetailsPage.actionsBy).click();
        } else if (option.equals(ConstantsDtc.SHARE) && page.equalsIgnoreCase(ConstantsDtc.STORE_LOCATOR)) {
            webDriver.findElement(StoreLocatorPage.selectedStoreBy).findElement(StoreDetailsPage.shareStoreBy).click();
        } else if (option.equals(ConstantsDtc.DIRECTIONS) && page.equalsIgnoreCase(ConstantsDtc.STORE_LOCATOR)) {
            webDriver.findElement(StoreLocatorPage.selectedStoreBy).findElement(StoreDetailsPage.actionsBy).
                    click();
        }
        LOGGER.info("chooseWheelConfigOption completed for " + option + " on page " + page);
    }

    /**
     * Verify message present
     *
     * @param by          By element
     * @param messageText String to validate
     */
    public void assertMessagePresent(By by, String messageText) {
        LOGGER.info("assertMessagePresent started");
        String actualText = webDriver.findElement(by).getText();
        driver.waitForMilliseconds();
        Assert.assertTrue("FAIL: Expected Message :" + messageText + " is NOT present . Actual Message displayed "
                + actualText, actualText.equals(messageText));
        LOGGER.info("assertMessagePresent completed");
    }

    /**
     * Select operation on specified sharing option and sendkeys on the corresponding textbox
     *
     * @param option Email/Mobile/Send To Phone
     * @param value  Value to perform SendKeys
     * @param page   web page
     */
    public void sendTo(String option, String value, String page) {
        LOGGER.info("sendTo started for " + option + " on page " + page);
        By dtButton = null;
        if (CommonUtils.containsIgnoreCase(option, ConstantsDtc.EMAIL)) {
            dtButton = CommonActions.formSubmitButtonBy;
            CommonActions.emailEnvelopeIcon.click();
            email.sendKeys(value);
        } else if (CommonUtils.containsIgnoreCase(option, ConstantsDtc.MOBILE)) {
            dtButton = CommonActions.formSubmitButtonBy;
            WheelConfiguratorPage.mobileShare.click();
            phone.sendKeys(value);
        } else if (CommonUtils.containsIgnoreCase(option, Constants.PHONE)) {
            dtButton = StoreDetailsPage.sendButtonBy;
            driver.getElementWithText(collapsibleToggleBy, option).click();
            driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
            StoreDetailsPage.mobilePhoneNumber.sendKeys(value);
        }
        driver.jsScrollToElementClick(webDriver.findElement(dtButton));
        LOGGER.info("sendTo completed for " + option + " on page " + page);
    }

    /**
     * Switch to window
     *
     * @param text popup window second/third header text
     */
    public void switchToWindowWithHeader(String text) {
        LOGGER.info("switchToWindowWithHeader started for " + text);
        By headerElement;
        if (text.equalsIgnoreCase(ConstantsDtc.ACTION_NEEDED)) {
            driver.waitForPageToLoad();
            headerElement = headerThirdBy;
        } else {
            headerElement = headerSecondBy;
        }
        driver.waitForTextPresent(headerElement, text, Constants.FIVE);

        Set allHandles = webDriver.getWindowHandles();
        String mainHandle = webDriver.getWindowHandle();
        int count = allHandles.size();

        if (count == 1) {
            Assert.assertTrue("FAIL: \"" + text + "\" was NOT displayed on the page!",
                    driver.checkIfElementContainsText(headerElement, text));
        } else {
            Iterator iter = allHandles.iterator();
            while (iter.hasNext()) {
                String popupHandle = iter.next().toString();
                if (!popupHandle.contains(mainHandle)) {
                    webDriver.switchTo().window(popupHandle);
                    Assert.assertTrue("FAIL: \"" + text + "\" was NOT displayed on the page!",
                            driver.checkIfElementContainsText(headerElement, text));
                    webDriver.switchTo().window(mainHandle);
                }
            }
        }
        LOGGER.info("switchToWindowWithHeader completed for " + text);
    }

    /**
     * Return the WebElement representing the row containing a specified vehicle.
     *
     * @param vehicle The vehicle to search for. Either "SELECTED VEHICLE" or Make, model, style, etc.
     * @return WebElement   The row containing the specified vehicle
     */
    public WebElement returnMyVehiclesRowWithVehicle(String vehicle) {
        LOGGER.info("returnMyVehiclesRowWithVehicle started for '" + vehicle + "'");
        WebElement targetRow = null;
        if (vehicle.equalsIgnoreCase(SELECTED_VEHICLE)) {
            targetRow = selectedVehicleSection;
        } else {
            List<WebElement> vehicleRows = webDriver.findElements(vehicleInfoSectionBy);

            for (WebElement vehicleRow : vehicleRows) {
                String vehicleDetails = vehicleRow.findElement(CommonActions.vehicleDescriptionBy).getText().
                        toLowerCase().replace("\n", " ").replace(Constants.EDIT.toLowerCase(), " ").trim();

                if (CommonUtils.containsIgnoreCase(vehicleDetails.toLowerCase().replace(" ", ""),
                        vehicle.toLowerCase().replace(" ", "")))
                    targetRow = vehicleRow;
            }
        }
        LOGGER.info("returnMyVehiclesRowWithVehicle completed for '" + vehicle + "'");
        return targetRow;
    }

    /**
     * Verify a specified type of message displayed
     *
     * @param text - The type of message to display
     */
    public void verifyMessageDisplayed(String text) {
        LOGGER.info("verifyMessageDisplayed started for '" + text + "' message");
        By by = null;
        String messageText = null;

        if (text.equalsIgnoreCase(ConstantsDtc.COPIED)) {
            by = StoreDetailsPage.copyLinkMessageBy;
            messageText = ConstantsDtc.COPIED;
        } else if (text.contains(ConstantsDtc.EMAIL)) {
            by = MyAccountPage.checkoutFormAttentionMessageBy;
            messageText = ConstantsDtc.EMAIL_VERIFICATION_LINK_MESSAGE;
        } else if (text.equalsIgnoreCase(Constants.SUCCESS)) {
            by = CommonActions.successMessageBy;
            messageText = ConstantsDtc.TEXT_SUCCESSFULLY_SENT;
        } else {
            Assert.fail("'" + text + "' message is not accounted for in verifyMessageDisplayed");
        }
        assertMessagePresent(by, messageText);
        LOGGER.info("verifyMessageDisplayed completed for '" + text + "' message");
    }

    /**
     * Verify nearest stores displayed in ascending order based on miles from the user location
     *
     * @param page Store Details/Store Locator page
     */
    public void assertNearestStoresDisplayOrder(String page) {
        LOGGER.info("assertNearestStoresDisplayOrder started for " + page);
        List<WebElement> storeMiles;
        if (page.equalsIgnoreCase(ConstantsDtc.STORE_DETAILS)) {
            storeMiles = webDriver.findElements(StoreDetailsPage.nearestStoreMilesBy);
        } else {
            storeMiles = webDriver.findElements(StoreLocatorPage.nearestStoreDistanceBy);
        }
        ArrayList<Float> miles = new ArrayList<>();
        for (WebElement storeMile : storeMiles) {
            miles.add(Float.parseFloat(storeMile.getText().split("[^0-9.]")[0].trim()));
        }
        Assert.assertTrue("FAIL: Nearest stores not sorted on base of miles from user location!",
                verifyAscendingOrder(miles));
        LOGGER.info("assertNearestStoresDisplayOrder completed for " + page);
    }

    /**
     * Expand 'Expedite your Store Experience'
     */
    public void expandExpediteYourStoreExperience() {
        LOGGER.info("expandExpediteYourStoreExperience started");
        driver.clickElementWithText(collapsibleToggleBy, AppointmentPage.EXPEDITE_YOUR_STORE_EXPERIENCE);
        LOGGER.info("expandExpediteYourStoreExperience completed");
    }

    /**
     * Updating the quantity of cart item
     *
     * @param page:     PLP/PDP/Catalog page
     * @param quantity: Number of quantity to be updated
     */
    public void updateQuantity(String page, String quantity) {
        LOGGER.info("updateQuantity started");
        WebElement element = getQuantityWebElement(page);
        clearAndPopulateEditField(element, quantity);
        LOGGER.info("updateQuantity completed");
    }

    /**
     * Get the quantity displayed in the quantity box on a given page
     *
     * @param page The page containing the quantity box
     * @return The value displayed in the quantity box
     */
    public String getQuantity(String page) {
        LOGGER.info("getQuantity started");
        WebElement element = getQuantityWebElement(page);
        LOGGER.info("getQuantity completed");
        return element.getAttribute(Constants.VALUE);
    }

    /**
     * Get the WebElement for the quantity box depending on the page it is on
     *
     * @param page The page containing the quantity box
     * @return The WebElement representing the quantity box
     */
    public WebElement getQuantityWebElement(String page) {
        LOGGER.info("getQuantityWebElement started");
        WebElement element = null;
        if (page.contains(CATALOG)) {
            element = itemQuantityCatalog;
        } else if (page.contains(CART)) {
            element = webDriver.findElement(cartItemQuantityBy);
        } else {
            element = productQuantityBox;
        }
        Assert.assertTrue("There is no product quantity box available to update", driver.isElementDisplayed(element));
        LOGGER.info("getQuantityWebElement completed");
        return element;
    }

    /**
     * Either increase or decrease the quantity by a specified amount
     *
     * @param changeDirection The direction to change the amount: "increase" or "decrease"
     * @param page            The page where the quantity box is located
     * @param changeAmount    The amount to change by
     */
    public void changeQuantity(String changeDirection, String page, String changeAmount) {
        LOGGER.info("changeQuantity started: " + changeDirection + " the quantity on " + page + " page");
        String value = getQuantity(page);
        int currentValue = Integer.parseInt(value);
        int change = Integer.parseInt(changeAmount);
        String newValue = "";
        if (changeDirection.equalsIgnoreCase("increase")) {
            newValue = String.valueOf(currentValue + change);
        } else {
            newValue = String.valueOf(currentValue - change);
            if (Integer.parseInt(newValue) < 0)
                Assert.fail("Decreasing " + currentValue + " by " + change + " is " + newValue +
                        ". The new value cannot be less than zero");
        }
        updateQuantity(page, newValue);
        LOGGER.info("changeQuantity completed: " + changeDirection + " the quantity on " + page + " page");
    }

    /**
     * Sets the value for a specified value type for the first product data item in the ScenarioData productInfoList
     * hash map
     *
     * @param enumText brand, product, item id,  price, quantity, in cart, or inventory message
     * @param index    The index identifying the product for which a value will be set
     * @param value    The value to set
     */
    public void productInfoListSetValue(String enumText, int index, String value) {
        productData valueType = productData.valueOf(enumText);
        productInfoListSetValue(valueType, index, value);
    }

    /**
     * Sets the value for a specified value type in the ScenarioData productInfoList hash map product data item at
     * the specified index
     *
     * @param valueType Type of value requested: brand, product, item id,  price, quantity, in cart,
     *                  or inventory message
     * @param index     The index identifying the product for which a value will be set
     * @param value     The value to set
     */
    public void productInfoListSetValue(productData valueType, int index, String value) {
        LOGGER.info("productInfoListSetValue started setting '" + valueType + "' to '" + value + "' at list index " + index);
        driver.scenarioData.productInfoList.get(driver.scenarioData.productInfoList.keySet().toArray()[index])
                [valueType.ordinal()] = value;
        LOGGER.info("productInfoListSetValue completed setting '" + valueType + "' to '" + value + "' at list index " + index);
    }

    /**
     * Returns a product value for the specified value type of the first product data item in
     * ScenarioData productInfoList hash map.
     *
     * @param valueType Type of value requested: brand, product, item id, price, quantity, in cart,
     *                  or inventory message
     * @return The product value
     */
    public String productInfoListGetValue(String valueType) {
        return productInfoListGetValue(valueType, Constants.ZERO);
    }

    /**
     * Returns a product value for brand, product, item id,  price, quantity, in cart, or inventory message for the
     * product data item at the specified array index in ScenarioData productInfoList hash map.
     *
     * @param valueType Type of value requested: brand, product, item id,  price, quantity, in cart,
     *                  or inventory message
     * @param index     The array index of the productInfoList array containing the list of products
     * @return The product value
     */
    public String productInfoListGetValue(String valueType, int index) {
        LOGGER.info("productInfoListGetValue started for '" + valueType + "' at list index " + index);
        switch (valueType) {
            case ConstantsDtc.BRAND:
                return driver.scenarioData.productInfoList.get(driver.scenarioData.productInfoList.keySet().toArray()[index])
                        [productData.BRAND.ordinal()];
            case ConstantsDtc.PRODUCT:
                return driver.scenarioData.productInfoList.get(driver.scenarioData.productInfoList.keySet().toArray()[index])
                        [productData.PRODUCT.ordinal()];
            case Constants.PRICE:
                return driver.scenarioData.productInfoList.get(driver.scenarioData.productInfoList.keySet().toArray()[index])
                        [productData.PRICE.ordinal()];
            case ConstantsDtc.INVENTORY_MESSAGE:
                return driver.scenarioData.productInfoList.get(driver.scenarioData.productInfoList.keySet().toArray()[index])
                        [productData.INVENTORY_MESSAGE.ordinal()];
            case ConstantsDtc.QUANTITY:
                return driver.scenarioData.productInfoList.get(driver.scenarioData.productInfoList.keySet().toArray()[index])
                        [productData.QUANTITY.ordinal()];
            case ConstantsDtc.ITEM:
                return driver.scenarioData.productInfoList.get(driver.scenarioData.productInfoList.keySet().toArray()[index])
                        [productData.ITEM.ordinal()];
            case ConstantsDtc.IN_CART:
                return driver.scenarioData.productInfoList.get(driver.scenarioData.productInfoList.keySet().toArray()[index])
                        [productData.IN_CART.ordinal()];
            case ConstantsDtc.TOTAL_PRICE:
                double totalPrice = Double.parseDouble(
                        productInfoListGetValue(Constants.PRICE, index).replace("$", "")) *
                        Integer.parseInt(productInfoListGetValue(
                                ConstantsDtc.QUANTITY, index));
                return String.valueOf(totalPrice);
            default:
                Assert.fail("Invalid value type, '" + valueType + "', for retrieving productInfo value");
                return null;
        }
    }

    /**
     * Add an item to the productInfoList hashmap containing values for brand, product, price, inventory message,
     * item number, and quantity.
     *
     * @param brand            The value to set for brand and used as part of the hashmap key
     * @param product          The value to set for product and used as part of the hashmap key
     * @param price            The value to set for price
     * @param inventoryMessage The value to set for inventory message
     * @param itemNumber       The value to set for item number and used as part of the hashmap key if it is not empty
     * @param quantity         The value to set for quantity
     * @param frontRear        Used as part of the hashmap key if it is not empty
     * @param inCart           true / false whether the product has been added to cart
     */
    public void addProductInfoListItem(String brand, String product, String price, String inventoryMessage,
                                       String itemNumber, String quantity, String frontRear, boolean inCart) {
        LOGGER.info("addProductInfoListItem started for '" + brand + " " + product + " " + price + "'");
        int itemInstance = 1;
        String[] productInfo = new String[productData.values().length];
        productInfo[productData.BRAND.ordinal()] = brand;
        productInfo[productData.PRODUCT.ordinal()] = product;
        productInfo[productData.PRICE.ordinal()] = price;
        productInfo[productData.INVENTORY_MESSAGE.ordinal()] = inventoryMessage.
                replace(ConstantsDtc.CHECK_NEARBY_STORES.toLowerCase(), "").
                replace(ConstantsDtc.CHANGE_STORE.toLowerCase(), "").trim();
        productInfo[productData.QUANTITY.ordinal()] = quantity;
        productInfo[productData.ITEM.ordinal()] = itemNumber;
        String key = brand + Constants.STRING_DELIMITER + product;
        if (!itemNumber.isEmpty())
            key += Constants.STRING_DELIMITER + itemNumber;
        if (!frontRear.isEmpty())
            key += Constants.STRING_DELIMITER + frontRear;
        productInfo[productData.IN_CART.ordinal()] = String.valueOf(inCart);
        key += appendInstanceString + String.valueOf(itemInstance);
        while (driver.scenarioData.productInfoList.keySet().contains(key)) {
            key = key.split(appendInstanceString)[0];
            itemInstance++;
            key += appendInstanceString + String.valueOf(itemInstance);
        }
        driver.scenarioData.productInfoList.put(key, productInfo);
        LOGGER.info("addProductInfoListItem completed for '" + brand + " " + product + " " + price + "'");
    }

    /**
     * Remove item from productInfoList based on item number
     *
     * @param itemNumber The item number of the product
     */
    public boolean removeProductInfoListItem(String itemNumber) {
        LOGGER.info("removeProductInfoListItem started for item ID '" + itemNumber + "'");
        boolean removed = false;
        Object[] keySet = driver.scenarioData.productInfoList.keySet().toArray();
        int index = 0;
        for (Object key : keySet) {
            String itemId = productInfoListGetValue(ConstantsDtc.ITEM, index);
            if (itemId.equals(itemNumber)) {
                String brand = productInfoListGetValue(ConstantsDtc.BRAND, index);
                String product = productInfoListGetValue(ConstantsDtc.PRODUCT, index);
                String frontRear = "";
                if (CommonUtils.containsIgnoreCase(key.toString(), ConstantsDtc.FRONT))
                    frontRear = ConstantsDtc.FRONT;
                if (CommonUtils.containsIgnoreCase(key.toString(), ConstantsDtc.REAR))
                    frontRear = ConstantsDtc.REAR;
                removeProductInfoListItem(brand, product, itemId, frontRear);
                removed = true;
                LOGGER.info("An instance of " + brand + " " + product + " (Item: " + itemId +
                        "') was removed from the productInfoList");
                break;
            }
            index++;
        }
        LOGGER.info("removeProductInfoListItem completed for '" + itemNumber + "'");
        return removed;
    }

    /**
     * Remove a product info list item that contains the specified brand, product, item number (may be empty),
     * and "FRONT" or "REAR" for a staggered product (may be empty).
     *
     * @param brand      The brand of the product
     * @param product    The product name
     * @param itemNumber The item number. May be empty.
     * @param frontRear  "FRONT" or "REAR". May be empty.
     */
    public void removeProductInfoListItem(String brand, String product, String itemNumber, String frontRear) {
        LOGGER.info("removeProductInfoListItem started for '" + brand + " " + product +
                " " + itemNumber + " " + frontRear + "'");
        String key = brand + Constants.STRING_DELIMITER + product;
        if (!itemNumber.isEmpty())
            key += Constants.STRING_DELIMITER + itemNumber;
        if (!frontRear.isEmpty())
            key += Constants.STRING_DELIMITER + frontRear;

        int itemInstance = 1;
        int highestItemInstance = 0;

        for (int i = 0; i < driver.scenarioData.productInfoList.keySet().size(); i++) {
            if (driver.scenarioData.productInfoList.keySet().toArray()[i].toString().contains(key)) {
                itemInstance = Integer.parseInt(driver.scenarioData.productInfoList.keySet().
                        toArray()[i].toString().split(appendInstanceString)[1]);
                if (highestItemInstance < itemInstance)
                    highestItemInstance = itemInstance;
            }
        }

        key += appendInstanceString + String.valueOf(highestItemInstance);
        driver.scenarioData.productInfoList.remove(key);
        LOGGER.info("removeProductInfoListItem completed for '" + brand + " " + product +
                " " + itemNumber + " " + frontRear + "'");
    }

    /**
     * Remove first product info list item where the key starts with the specified brand and product
     *
     * @param brand   The brand of the product
     * @param product The product name
     */
    public boolean removeProductInfoListItem(String brand, String product) {
        LOGGER.info("removeProductInfoListItem started for '" + brand + " " + product + "'");
        boolean removed = false;
        Object[] keySet = driver.scenarioData.productInfoList.keySet().toArray();
        int index = 0;
        for (Object key : keySet) {
            if (key.toString().startsWith(brand + Constants.STRING_DELIMITER + product)) {
                String itemId = productInfoListGetValue(ConstantsDtc.ITEM, index);
                String frontRear = "";
                if (CommonUtils.containsIgnoreCase(key.toString(), ConstantsDtc.FRONT))
                    frontRear = ConstantsDtc.FRONT;
                if (CommonUtils.containsIgnoreCase(key.toString(), ConstantsDtc.REAR))
                    frontRear = ConstantsDtc.REAR;
                removeProductInfoListItem(brand, product, itemId, frontRear);
                removed = true;
                LOGGER.info("An instance of " + brand + " " + product + " (Item: " + itemId +
                        "') was removed from the productInfoList");
                break;
            }
            index++;
        }
        LOGGER.info("removeProductInfoListItem completed for '" + brand + " " + product + "'");
        return removed;
    }

    /**
     * Remove all product info list items where the key starts with the specified brand and product
     *
     * @param brand   The brand of the product
     * @param product The product name
     */
    public boolean removeAllSpecifiedProductInfoListItems(String brand, String product) {
        LOGGER.info("removeAllSpecifiedProductInfoListItems started for '" + brand + " " + product + "'");
        boolean removed = false;
        do {
            removed = removeProductInfoListItem(brand, product);
        } while (removed);
        LOGGER.info("removeAllSpecifiedProductInfoListItems completed for '" + brand + " " + product + "'");
        return removed;
    }

    /**
     * Wait for spinner on page to appear and then disappear
     */
    public void waitForSpinner() {
        waitForSpinner(Constants.ONE, Constants.SIXTY);
    }

    /**
     * Wait for spinner on page to appear and then disappear
     *
     * @param waitTimeSpinnerAppear    The time in seconds to wait for the spinner to appear
     * @param waitTimeSpinnerDisappear The time in seconds to wait for the spinner to not be displayed
     */
    public void waitForSpinner(int waitTimeSpinnerAppear, int waitTimeSpinnerDisappear) {
        LOGGER.info("waitForSpinner started");
        driver.pollUntil(Constants.SPINNER_DISPLAYED_JS, waitTimeSpinnerAppear);
        driver.pollUntil(Constants.SPINNER_NOT_DISPLAYED_JS, waitTimeSpinnerDisappear);
        LOGGER.info("waitForSpinner completed");
    }

    /**
     * Select the link in Breadcrumb
     *
     * @param linkName String representing the names/titles of the breadcrumb links being checked
     */
    public void selectBreadcrumbLink(String linkName) {
        LOGGER.info("selectBreadcrumbLink started");
        WebElement breadcrumb = driver.getElementWithText(breadcrumbLinkBy, linkName);
        driver.jsScrollToElementClick(breadcrumb);
        LOGGER.info("selectBreadcrumbLink completed");
    }

    /**
     * Select collapsible toggle button
     *
     * @param toggleElementBy The toggle button element to expand or collapse
     * @param option          expand/collapse
     * @param buttonText      text representing the button
     */
    public void selectCollapsibleToggleButton(By toggleElementBy, String option, String buttonText) {
        LOGGER.info("selectCollapsibleToggleButton started to " + option + " '" + buttonText + "' toggle button");
        driver.waitForPageToLoad();
        boolean found = false;
        int attempts = Constants.ZERO;
        int maxAttempts = Constants.TEN;
        do {
            WebElement toggleButton = driver.getElementWithText(toggleElementBy, buttonText);
            if (toggleButton == null) {
                driver.waitOneSecond();
                attempts++;
            } else {
                boolean expanded = driver.isElementDisplayed(CommonActions.openCollapsibleSectionBy, Constants.ZERO);
                if (option.equalsIgnoreCase(Constants.COLLAPSE) && !expanded ||
                        option.equalsIgnoreCase(Constants.EXPAND) && expanded) {
                    found = true;
                    break;
                }
                driver.jsScrollToElementClick(toggleButton, false);
                driver.waitSeconds(Constants.THREE);
                found = true;
            }
        } while (!found && attempts < maxAttempts);
        Assert.assertTrue("FAIL: '" + buttonText + "' toggle button was not found.", found);
        LOGGER.info("selectCollapsibleToggleButton completed " + option + " '" + buttonText + "' toggle button");
    }

    /**
     * Select collapsible toggle button
     *
     * @param option     expand/collapse
     * @param buttonText text representing the button
     */
    public void selectCollapsibleToggleButton(String option, String buttonText) {
        LOGGER.info("selectCollapsibleToggleButton started to " + option + " '" + buttonText + "' toggle button");
        if (buttonText.equalsIgnoreCase(Constants.SOMEONE_ELSE_WILL_PICK_UP_MY_ORDER))
            selectCollapsibleToggleButton(CheckoutPage.someoneElsePickUpElementBy, option, buttonText);
        else if (buttonText.equalsIgnoreCase(ConstantsDtc.OPTIONAL_TIRE_AND_WHEEL_SIZE))
            selectCollapsibleToggleButton(CommonActions.optionalTireAndWheelSizeLinkBy, option, buttonText);
        else
            selectCollapsibleToggleButton(CommonActions.collapsibleToggleBy, option, buttonText);
        LOGGER.info("selectCollapsibleToggleButton completed to " + option + " '" + buttonText + "' toggle button");
    }

    /**
     * Verifies the message is visible and matches the expected value
     *
     * @param message message text
     */
    public void verifyGlobalMessage(String message) {
        LOGGER.info("verifyGlobalMessage started for " + message);
        driver.waitForElementVisible(CommonActions.globalMessage);
        String expectedMessage = null;
        if (message.equalsIgnoreCase(ConstantsDtc.SPECIAL_ORDER)) {
            expectedMessage = AppointmentPage.SPECIAL_ORDER_MESSAGE;
        } else if (CheckoutPage.UNABLE_TO_PROCESS_CARD.toLowerCase().contains(message.toLowerCase())) {
            expectedMessage = CheckoutPage.UNABLE_TO_PROCESS_CARD;
        } else if (AppointmentPage.CALL_US_MESSAGE.toLowerCase().contains(message.toLowerCase())) {
            expectedMessage = AppointmentPage.CALL_US_MESSAGE;
        }
        Assert.assertTrue("FAIL: Global message \"" + CommonActions.globalMessage.getText() +
                        "\" does NOT contain " + "\"" + expectedMessage + "\"!",
                CommonActions.globalMessage.getText().contains(expectedMessage));
        driver.waitOneSecond();
        LOGGER.info("verifyGlobalMessage completed for " + message);
    }

    /**
     * Return the WebElement representing the row containing a specified vehicle in Saved or Recent searches sections
     *
     * @param vehicle       The vehicle to search for. Either "SELECTED VEHICLE" or Make, model, style, etc.
     * @param savedOrRecent : Choice of section where we want to verify the vehicle data Saved or Recent
     * @return WebElement   The row containing the specified vehicle
     */
    public WebElement returnMyVehiclesRowsListByChoice(String vehicle, String savedOrRecent) {
        LOGGER.info("returnMyVehiclesRowsListByChoice in " + savedOrRecent + " started for '" + vehicle + "'");
        WebElement targetRow = null;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        if (vehicle.equalsIgnoreCase(SELECTED_VEHICLE)) {
            targetRow = selectedVehicleSection;
        } else {
            List<WebElement> vehicleRows = null;

            if (savedOrRecent.equalsIgnoreCase(SAVED_VEHICLES)) {
                vehicleRows = webDriver.findElements(savedVehicleInfoSectionBy);
            } else if (savedOrRecent.equalsIgnoreCase(RECENT_SEARCHES)) {
                vehicleRows = webDriver.findElements(myVehiclesButtonBy);
            }

            for (WebElement vehicleRow : vehicleRows) {
                String vehicleDetails = vehicleRow.findElement(CommonActions.vehicleDescriptionBy).getText().
                        toLowerCase().replace("\n", " ").replace(ConstantsDtc.EDIT_VEHICLE.toLowerCase(), " ").trim();

                if (CommonUtils.containsIgnoreCase(vehicleDetails.toLowerCase().replace(" ", ""),
                        vehicle.toLowerCase().replace(" ", "")))
                    targetRow = vehicleRow;
            }
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("returnMyVehiclesRowsListByChoice in " + savedOrRecent + " completed for '" + vehicle + "'");
        return targetRow;
    }

    /**
     * Extracts total result count from the product list page, then verifies that each page
     * displays 10 results per page within that total, also checking that last page contains the remainder count
     *
     * @param webPage web page
     */
    public void validatePagination(String webPage) {
        LOGGER.info("validatePagination started for " + webPage);
        int total = getTotalCount(webPage);
        int totalPages = total / 10;
        int remainder = total % 10;
        int displayCountExpected = 10;
        int displayCount;
        By elementBy;

        if (webPage.equalsIgnoreCase(ConstantsDtc.ORDERS))
            elementBy = CommonActions.orderListItemBy;
        else if (webPage.equalsIgnoreCase(ConstantsDtc.PLP))
            elementBy = ProductListPage.plpResultsRowBy;
        else
            elementBy = StoreLocatorPage.storeRowBy;

        //NOTE: This will work for web or mweb, as all elements exist even if not displayed
        String paginationActiveScript =
                "return document.getElementsByClassName('pagination__link active')[0].textContent == ";

        if ((totalPages == 0) || (totalPages == 1)) {
            LOGGER.info("Either there were no results or only one page, skipping pagination validation.");
        } else {
            //if there is an uneven number, you will end up with an extra page
            if (remainder != 0)
                totalPages++;

            for (int page = 1; page <= totalPages; page++) {
                //waits for next page to load (for slower browsers / mobile)
                driver.pollUntil(paginationActiveScript + Integer.toString(page), Constants.THIRTY);
                try {
                    driver.pollUntil(Constants.SPINNER_DISPLAYED_JS, Constants.ONE);
                    driver.waitForElementVisible(elementBy);
                } catch (java.util.NoSuchElementException e) {
                    driver.waitForMilliseconds();
                    driver.waitForElementVisible(elementBy);
                }

                //last page should include the remainder
                if ((page == totalPages) && (remainder != 0)) {
                    displayCountExpected = remainder;
                }

                displayCount = webDriver.findElements(elementBy).size();

                if (displayCount == displayCountExpected) {
                    LOGGER.info("Confirmed that display count for page " + page + " was " + displayCountExpected);
                } else {
                    Assert.fail("FAIL: Display count expected was \"" + displayCountExpected
                            + "\" but actual count was \"" + displayCount + "\"!");
                }

                //nextPage button is not present on the last page
                if (page < totalPages) {
                    navToDifferentPageOfResults(Constants.NEXT);
                    driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
                }
            }
        }
        LOGGER.info("validatePagination completed for " + webPage);
    }

    /**
     * Gets the total count of displayed result rows on the specified page
     *
     * @param page web page
     * @return total : integer count of pages of results displayed
     */
    private int getTotalCount(String page) {
        LOGGER.info("getTotalCount started for " + page);
        String totalCountString = null;
        if (page.equalsIgnoreCase(ConstantsDtc.PLP)) {
            totalCountString = driver.getDisplayedElement(ProductListPage.resultsBarCountBy).getText();
        } else if (page.equalsIgnoreCase(ConstantsDtc.STORE_LOCATOR)) {
            totalCountString = webDriver.findElement(CommonActions.spanPaginationBy).getText().
                    split("of ")[1].split(" ")[0].trim();
        } else {
            totalCountString = String.valueOf(driver.getDisplayedElementsList(CommonActions.orderListItemBy).size());
        }
        LOGGER.info("getTotalCount completed and returning count of '" + totalCountString + "'");
        return Integer.parseInt(totalCountString);
    }

    /**
     * Sets the "Price Range" slider filter to a smaller range than currently displayed
     */
    public void reducePriceRangeSliderFilter() {
        LOGGER.info("reducePriceRangeSliderFilter started");
        setPriceRangeSliderFilterToRange("", "");
        LOGGER.info("reducePriceRangeSliderFilter completed");
    }

    /**
     * Sets the "Price Range" slider filter to the specified range
     *
     * @param priceMin String for the minimum point value
     * @param priceMax String for the maximum point value
     */
    public void setPriceRangeSliderFilterToRange(String priceMin, String priceMax) {
        LOGGER.info("setPriceRangeSliderFilterToRange started with min: \"" + priceMin + "\" and max: \""
                + priceMax + "\"");
        //TODO CCL - retest in the future with an updated version of Safari driver
        if (!Config.isSafari()) {
            driver.waitForPageToLoad(Constants.FIVE_HUNDRED);

            if (Config.isMobile())
                ProductListPage.openFilterSectionBtn.click();

            driver.waitForElementVisible(ProductListPage.searchFilterSectionBy);

            int currentMin = Integer.parseInt(getCurrentPriceRangePoint(MIN));
            int currentMax = Integer.parseInt(getCurrentPriceRangePoint(MAX));
            int slideDistance = Math.min((currentMax - currentMin) / 5, Constants.TEN);
            if (priceMin.isEmpty())
                priceMin = String.valueOf(currentMin + slideDistance);
            if (priceMax.isEmpty())
                priceMax = String.valueOf(currentMax - slideDistance);

            minValueApproximate = Integer.parseInt(priceMin);
            maxValueApproximate = Integer.parseInt(priceMax);
            setPriceRangeFilterValue(MIN, priceMin);
            if (Integer.parseInt(getCurrentPriceRangePoint(MIN)) < Integer.parseInt(priceMax))
                setPriceRangeFilterValue(MAX, priceMax);

            if (Config.isMobile()) {
                if (driver.isElementDisplayed(applyFiltersButton))
                    applyFiltersButton.click();
            } else {
                if (driver.isElementDisplayed(applyPriceRangeFilterButton))
                    applyPriceRangeFilterButton.click();
            }
            driver.waitForMilliseconds();

        } else {
            LOGGER.info("Skipping setPriceRangeSliderFilterToRange() for Safari browser");
        }
        LOGGER.info("setPriceRangeSliderFilterToRange completed with min: \"" + priceMin + "\" and max: \""
                + priceMax + "\"");
    }

    /**
     * Sets the specified range point (Min or Max) for the "Price Range" slider filter to the specified value
     *
     * @param rangePoint         String for the range point (Min or Max)
     * @param desiredFilterValue String for the filter value
     */
    private void setPriceRangeFilterValue(String rangePoint, String desiredFilterValue) {
        LOGGER.info("setPriceRangeFilterValue started setting " + rangePoint + " to " + desiredFilterValue);
        By sliderPointBy = null;
        int expectedFilterValue = Integer.parseInt(desiredFilterValue);
        int currentValue = Integer.parseInt(getCurrentPriceRangePoint(rangePoint));
        Actions moveSlider = new Actions(webDriver);
        boolean found = false;
        int offset = 9;

        if (expectedFilterValue != currentValue) {
            switch (rangePoint.toLowerCase()) {
                case MIN:
                    sliderPointBy = priceRangeMinimumPointBy;
                    if (currentValue > expectedFilterValue) {
                        Assert.fail("FAIL: The starting minimum price point on the slider, " + currentValue +
                                ",  is greater than the desired minimum price: " + desiredFilterValue);
                    }
                    break;
                case MAX:
                    sliderPointBy = priceRangeMaximumPointBy;
                    if (currentValue < expectedFilterValue) {
                        Assert.fail("FAIL: The starting maximum price point on the slider, " + currentValue +
                                ",  is less than the desired maximum price: " + desiredFilterValue);
                    }
                    break;
                default:
                    Assert.fail("FAIL: Did NOT recognize \"Price Range\" point: \"" + rangePoint + "\"!");
            }

            WebElement sliderPoint = driver.getDisplayedElement(sliderPointBy, Constants.ZERO);
            driver.jsScrollToElementClick(sliderPoint, false);
            while (!found) {
                driver.jsScrollToElementClick(sliderPoint, false);
                Action action = moveSlider.dragAndDropBy(sliderPoint,
                        setXoffsetValue(rangePoint, offset, expectedFilterValue), Constants.ZERO).build();
                action.perform();
                currentValue = Integer.parseInt(getCurrentPriceRangePoint(rangePoint));

                if (expectedFilterValue == currentValue) {
                    found = true;
                } else {
                    if ((rangePoint.equalsIgnoreCase(MIN) && currentValue > expectedFilterValue) ||
                            (rangePoint.equalsIgnoreCase(MAX) && expectedFilterValue > currentValue)) {
                        if (checkForValueInApproximateRange(rangePoint, desiredFilterValue, Constants.TWENTY))
                            found = true;
                        else
                            Assert.fail("FAIL: Failed to set " + rangePoint + " value to " + expectedFilterValue);
                    }
                }

                if (!found && Config.isFirefox()) {
                    driver.jsScrollToElementClick(sliderPoint, false);
                }
            }
        }
        LOGGER.info("setPriceRangeFilterValue completed. " + rangePoint + " = " + currentValue);
    }

    /**
     * Checks to see if the specified slider range point is within a specified threshold in cases where the drag & drop
     * action cannot exactly select the desired value, and the current value has gone beyond the expected value
     *
     * @param rangePoint         Min or Max ("Price Range" slider), left or right, low or high, etc.
     * @param desiredFilterValue Desired value for the range point
     * @param rangeThreshold     Acceptable threshold for the range point value to differ from the desired value
     * @return True if a value was found within the specified threshold, else False
     */
    private boolean checkForValueInApproximateRange(String rangePoint, String desiredFilterValue, int rangeThreshold) {
        LOGGER.info("checkForValueInApproximateRange started for range point: \"" + rangePoint
                + "\", desired value of: \"" + desiredFilterValue + "\" and price threshold of: \""
                + rangeThreshold + "\"");
        int currentFilterValue = Integer.parseInt(getCurrentPriceRangePoint(rangePoint));
        int expectedFilterValue = Integer.parseInt(desiredFilterValue);

        if (Math.abs(currentFilterValue - expectedFilterValue) <= rangeThreshold) {
            minValueApproximate = currentFilterValue;
            LOGGER.info("Could not select exact amount of \"" + desiredFilterValue
                    + "\" for " + rangePoint + " price range point, instead selected approximate amount of \""
                    + String.valueOf(minValueApproximate) + "\"");
            return true;
        }
        return false;
    }

    /**
     * Gets the current value for the specified range point (Min or Max) of the "Price Range" slider filter. Method
     * cleans the string such that just the numeric portion is returned
     *
     * @param rangePoint String for the range point (Min or Max)
     * @return String containing current value of the specified range point
     */
    public String getCurrentPriceRangePoint(String rangePoint) {
        LOGGER.info("getCurrentPriceRangePoint started for range point '" + rangePoint + "'");
        List<WebElement> rows = null;
        driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
        if (!CommonUtils.containsIgnoreCase(webDriver.getCurrentUrl(), ConstantsDtc.WHEEL_CONFIGURATOR_URL_SEGMENT)) {
            try {
                rows = webDriver.findElements(ProductListPage.plpResultsRowBy);
            } catch (Exception e) {
                LOGGER.info("There are no products listed in PLP to compare price to price range filter");
                return null;
            }
            if (rows.size() == 1) {
                List<WebElement> prices = rows.get(0).findElements(CommonActions.productPriceBy);
                if (prices.size() == 1) {
                    if (!prices.get(0).getText().contains("-")) {
                        LOGGER.info("There is only one price on PLP so price slider will not be displayed");
                        return null;
                    }
                }
            }
            if (rows.size() == 0) {
                LOGGER.info("There are no products displayed on PLP. No price slider will be displayed and " +
                        "price range cannot be verified.");
                return null;
            }
        }

        driver.resetImplicitWaitToDefault();
        String currentPriceRange = driver.getDisplayedElement(ProductListPage.currentPriceRangeBy,
                Constants.ZERO).getText();
        switch (rangePoint.toLowerCase()) {
            case MIN:
                LOGGER.info("getCurrentPriceRangePoint returning current minimum 'Price Range' point");
                currentPriceRange = StringUtils.substringBetween(currentPriceRange, "$", " ");
                break;
            case MAX:
                LOGGER.info("getCurrentPriceRangePoint returning current maximum 'Price Range' point");
                currentPriceRange = StringUtils.substringAfterLast(currentPriceRange, "$");
                break;
            default:
                Assert.fail("FAIL: cannot get current value of \"Price Range\" point: \"" + rangePoint + "\"!");
        }
        return currentPriceRange;
    }

    /**
     * Sets and adjusts the x axis offset value dependent upon the difference between the current slider point's value
     * and the expected / desired value. Since currently the value of 1 unit of offset appears to be equal to 3 dollars,
     * the default value and ranges are based on multiples of 3.
     *
     * @param rangePoint          String for the range point (Min or Max)
     * @param xOffsetValue        Int for the current xOffsetValue
     * @param expectedFilterValue Int for the desired / expected filter value
     * @return Int defaulting to the initially passed in value; 2 if between the range of 90 and 18; 1 if below 18
     */
    private int setXoffsetValue(String rangePoint, int xOffsetValue, int expectedFilterValue) {
        LOGGER.info("setXoffsetValue started with range point: \"" + rangePoint
                + "\", default offset value for x: \"" + xOffsetValue + "\" and expected filter value: \""
                + expectedFilterValue + "\"");
        int filterValueDifference;
        int currentFilterValue = Integer.parseInt(getCurrentPriceRangePoint(rangePoint));

        if (rangePoint.equalsIgnoreCase(MIN)) {
            filterValueDifference = expectedFilterValue - currentFilterValue;
        } else {
            filterValueDifference = currentFilterValue - expectedFilterValue;
        }

        if (filterValueDifference <= 18 && filterValueDifference >= 1) {
            xOffsetValue = 5;
            LOGGER.info("Using xOffsetValue of: \"" + xOffsetValue + "\"");
        }

        if (filterValueDifference < 1) {
            xOffsetValue = 1;
            LOGGER.info("Using xOffsetValue of: \"" + xOffsetValue + "\"");
        }

        if (rangePoint.equalsIgnoreCase(MAX)) {
            xOffsetValue = -xOffsetValue;
        }

        LOGGER.info("setXoffsetValue completed with range point: \"" + rangePoint
                + "\", default offset value for x: \"" + xOffsetValue + "\" and expected filter value: \""
                + expectedFilterValue + "\"");
        return xOffsetValue;
    }

    /***
     * Checks for the "No Results found for ..." message on the Product List Page.
     */
    public void checkForNoResultsMessage() {
        LOGGER.info("checkForNoResultsMessage started");
        Assert.assertFalse("Fail - The \"No product results found...\" message was displayed!",
                driver.isElementDisplayed(ProductListPage.noResultsInfoMessage, Constants.ONE));
        LOGGER.info
                ("checkForNoResultsMessage completed - \"No product results found\" message was NOT displayed");
    }

    /**
     * Set whether or not to expedite your experience during customer data entry
     *
     * @param expedite - true / false whether to expedite your experience
     */
    public void setScenarioDataExpediteYourExperience(boolean expedite) {
        driver.scenarioData.setExpediteYourExperience(expedite);
    }

    /**
     * Clicks the default form submit button
     */
    public void clickDefaultFormSubmitButton() {
        LOGGER.info("clickDefaultFormSubmitButton started");
        String submitButtonText = webDriver.findElement(formSubmitButtonBy).getText();
        clickFormSubmitButtonByText(submitButtonText);
        LOGGER.info("clickDefaultFormSubmitButton completed");
    }

    /**
     * Clicks the form submit button with specified text
     *
     * @param buttonText - The text on the form submit button
     */
    public void clickFormSubmitButtonByText(String buttonText) {
        LOGGER.info("clickFormSubmitButtonByText started");
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        int counter = 0;
        int maxAttempts = Constants.ONE_HUNDRED;
        boolean enabled = false;
        Assert.assertTrue("FAIL: The '" + buttonText + "' button was not displayed",
                driver.checkIfElementContainsText(formSubmitButtonBy, buttonText));
        do {
            if (driver.getElementWithText(formSubmitButtonBy, buttonText).isEnabled())
                enabled = true;
            else {
                clickBrowserBody();
                driver.waitForMilliseconds(Constants.ONE_HUNDRED);
            }
            counter++;
        } while (!enabled && counter < maxAttempts);
        Assert.assertTrue("FAIL: The '" + buttonText + "' button was not enabled", enabled);
        driver.resetImplicitWaitToDefault();
        driver.jsScrollToElementClick(driver.getElementWithText(formSubmitButtonBy, buttonText));
        LOGGER.info("clickFormSubmitButtonByText completed");
    }

    /**
     * Click the browser body without failing due to possible exception. This can be used to assist in enabling submit
     * buttons, having form field values be fully entered and recognized, or for error messages to become visible, etc.
     */
    public void clickBrowserBody() {
        try {
            driver.jsClick(webBrowserBody);
        } catch (Exception e) {
            // continue
        }
    }

    /**
     * Method verifies all the stores address present in PLP, PDP and compare page
     *
     * @param pageType : page type = PLP, PDP or compare
     */
    public void verifyStoreAddressesOnPage(String pageType) {
        LOGGER.info("verifyStoreAddressesOnPage started for page = " + pageType);
        driver.waitForPageToLoad();
        List<WebElement> elements = null;
        if (pageType.equalsIgnoreCase(COMPARE_PAGE)) {
            if (Config.isMobile()) {
                elements = webDriver.findElements(storeAddressListInComparePageMobileBy);
            } else {
                elements = webDriver.findElements(storeAddressListInComparePageBy);
            }
        } else if (pageType.equalsIgnoreCase(PLP_PAGE)) {
            if (Config.isMobile()) {
                elements = webDriver.findElements(plpProductAvailabilityListMobileBy);
            } else {
                elements = webDriver.findElements(plpProductAvailabilityListBy);
            }
        } else if (pageType.equalsIgnoreCase(PDP_PAGE)) {
            elements = webDriver.findElements(storeAddressListInPDPBy);
        }
        Assert.assertTrue("Fail: There are no store addresses listed on " + pageType + "page", !elements.isEmpty());
        for (WebElement element : elements) {
            Assert.assertTrue("FAIL: Stores addresses are not equal in " + pageType + " page", element.getText().
                    contains(driver.scenarioData.genericData.get(Constants.STORE_DETAILS)));
        }
        LOGGER.info("verifyStoreAddressesOnPage completed for page = " + pageType);
    }

    /**
     * Clicks the site logo at the top of page
     */
    public void clickSiteLogo() {
        LOGGER.info("clickStoreLogo started");
        if (Config.isMobile()) {
            driver.waitForElementClickable(MobileHeaderPage.dtLogoMobile);
            MobileHeaderPage.dtLogoMobile.click();
        } else {
            try {
                driver.jsScrollToElementClick(dtLogo);
            } catch (TimeoutException e) {
                LOGGER.info("Page load timed out, attempting refresh...");
                webDriver.navigate().refresh();
                driver.waitForPageToLoad();
                driver.jsScrollToElementClick(dtLogo);
            }
        }
        LOGGER.info("clickStoreLogo completed");
    }

    /**
     * Checks for the global message containing "Our payment system is currently down".
     *
     * @return true / false whether the message exists
     */
    public boolean paymentSystemDownMessageExists(Scenario scenario) throws IOException {
        LOGGER.info("paymentSystemDownMessageExists started");
        boolean messageExists = false;
        if (driver.isElementDisplayed(CheckoutPage.checkoutErrorBy, Constants.ZERO)) {
            String messageText = webDriver.findElement(CheckoutPage.checkoutErrorBy).getText();
            if (CommonUtils.containsIgnoreCase(messageText, ConstantsDtc.OUR_PAYMENT_SYSTEM_IS_DOWN)) {
                messageExists = true;
                LOGGER.info("Error Message displayed: '" + messageText + "'");
                driver.embedScreenshot(scenario);
            }
        }
        LOGGER.info("paymentSystemDownMessageExists completed");
        return messageExists;
    }

    /**
     * Sort date ("MM-dd-yyyy") in ascending order
     *
     * @param list  list of dates in MM-dd-yyyy format
     * @param order Ascending and Descending
     */
    public void dateBubbleSort(ArrayList<String> list, String order) {
        LOGGER.info("dateBubbleSort started");
        Collections.sort(list, new Comparator<String>() {
            DateFormat f = new SimpleDateFormat("MM-dd-yyyy");

            @Override
            public int compare(String o1, String o2) {
                try {
                    return f.parse(o1).compareTo(f.parse(o2));
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });
        if (order.equalsIgnoreCase(Constants.DESCENDING)) {
            Collections.sort(list, Ordering.natural().reverse());
        }
        LOGGER.info("dateBubbleSort completed");
    }

    /**
     * Update scenario data with order type and order total for various orders
     */
    public void addScenarioGenericData() {
        LOGGER.info("addScenarioGenericData started");
        if (driver.isElementDisplayed(driver.getElementWithText(CommonActions.headerBy,
                ConstantsDtc.SERVICE_APPOINTMENT))) {
            driver.scenarioData.genericData.put(Constants.ORDER_TOTAL, "0.0");
            driver.scenarioData.genericData.put(Constants.ORDER_TYPE, ConstantsDtc.ORDER_TYPE_SERVICE_APPOINTMENT);
        } else {
            if (HomePage.siteLogo.getAttribute(Constants.ATTRIBUTE_SRC).contains(HomePage.DTD_LOGO)) {
                driver.scenarioData.genericData.put(Constants.ORDER_TYPE, ConstantsDtc.MAIL_ORDER);
            } else if (driver.isElementDisplayed(driver.getElementWithText(formSubmitButtonBy,
                    ConstantsDtc.PLACE_ORDER))) {
                String total = String.valueOf(cleanMonetaryStringToDouble(orderTotal.getText()));
                driver.scenarioData.genericData.put(Constants.ORDER_TOTAL, total);
            }
            addOrderTotalScenarioGenericData();
        }
        LOGGER.info("addScenarioGenericData completed");
    }

    /**
     * Update scenario data for order total
     */
    public void addOrderTotalScenarioGenericData() {
        LOGGER.info("addOrderTotalScenarioGenericData started");
        String total;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        try {
            total = String.valueOf(cleanMonetaryStringToDouble(totalAmount.getText()));
        } catch (Exception e) {
            total = String.valueOf(cleanMonetaryStringToDouble(orderTotal.getText()));
        }
        driver.scenarioData.genericData.put(Constants.ORDER_TOTAL, total);
        driver.resetImplicitWaitToDefault();
        LOGGER.info("addOrderTotalScenarioGenericData completed");
    }

    /**
     * Update scenario data for Customer type
     */
    public void addCustomerTypeScenarioGenericData() {
        LOGGER.info("addCustomerTypeScenarioGenericData started");
        if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
            LOGGER.info("MyAccount is turned off in DTD and the myAccountLoggedInLabel is not displayed. " +
                    "Setting generic data for Customer Type to GUEST, the default value in Order web service. " +
                    "Please remove this condition if MyAccount is turned on in DTD");
            driver.scenarioData.genericData.put(Constants.CUSTOMER_TYPE, ConstantsDtc.GUEST_CUSTOMER);
            return;
        }

        if (!driver.isElementDisplayed(CommonActions.myAccountLoggedInLabel)) {
            driver.scenarioData.genericData.put(Constants.CUSTOMER_TYPE, ConstantsDtc.GUEST_CUSTOMER);
        } else {
            String currentCustomerType = driver.scenarioData.genericData.get(Constants.CUSTOMER_TYPE);
            String labelText = CommonActions.myAccountLoggedInLabel.getText();
            if (!labelText.contains(ConstantsDtc.JOIN_SIGN_IN)) {
                driver.scenarioData.genericData.put(Constants.CUSTOMER_TYPE, ConstantsDtc.REGISTERED_CUSTOMER);
            }
            else {
                if (currentCustomerType == null && labelText.contains(ConstantsDtc.JOIN_SIGN_IN)) {
                    driver.scenarioData.genericData.put(Constants.CUSTOMER_TYPE, ConstantsDtc.GUEST_CUSTOMER);
                } else {
                    if (labelText.contains(ConstantsDtc.JOIN_SIGN_IN) &&
                            !currentCustomerType.equalsIgnoreCase(ConstantsDtc.GUEST_CUSTOMER))
                        driver.scenarioData.genericData.put(Constants.CUSTOMER_TYPE, ConstantsDtc.REGISTERED_CUSTOMER);
                }
            }
        }
        LOGGER.info("addCustomerTypeScenarioGenericData completed with Customer Type: '" +
                driver.scenarioData.genericData.get(Constants.CUSTOMER_TYPE) + "'");
    }

    /**
     * Get DTD store code and update scenario generic store code
     */
    public void getDTDStoreCode() {
        LOGGER.info("getDTDStoreCode started");
        String stateCode = CheckoutPage.checkoutSummaryCustomerDetails.getText().split(", ")[1].split(" ")[0].trim();
        String dtdStoreCode = Constants.AZ_DC;
        if (stateCode.equalsIgnoreCase(Constants.STATE_AE) || stateCode.equalsIgnoreCase(Constants.STATE_OH) ||
                stateCode.equalsIgnoreCase(Constants.STATE_AA)) {
            dtdStoreCode = Constants.GA_DC;
        } else if (stateCode.equalsIgnoreCase(Constants.STATE_FL) || stateCode.equalsIgnoreCase(Constants.STATE_GA)) {
            dtdStoreCode = Constants.OH_DC;
        }
        driver.scenarioData.genericData.put(Constants.STORE_CODE, dtdStoreCode);
        LOGGER.info("getDTDStoreCode completed");
    }

    /**
     * Click the 'CONTINUE WITH THIS VEHICLE' button if it is present.
     */
    public void clickContinueWithThisVehicleButton() {
        LOGGER.info("clickContinueWithThisVehicle started");
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        try {
            WebElement continueWithVehicleButton = driver.getElementWithText(
                    CommonActions.shopVehicleConfirmationModalButtonBy, CommonActions.CONTINUE_WITH_THIS_VEHICLE);
            driver.waitForElementClickable(continueWithVehicleButton, Constants.TWO);
            driver.webElementClick(continueWithVehicleButton);
            driver.waitOneSecond();
        } catch (Exception e) {
            // continue
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("clickContinueWithThisVehicle completed");
    }

    /**
     * Get the price for an item on the cart page
     *
     * @param itemCode The item code
     * @return The item price (single item price multiplied by quantity)
     */
    public String getProductInfoListValueForItemCode(String itemCode, String targetData) {
        LOGGER.info("getProductInfoListValueForItemCode started");
        String value = "";
        for (int index = 0; index < driver.scenarioData.productInfoList.size(); index++) {
            if (productInfoListGetValue(ConstantsDtc.ITEM, index).equals(itemCode)) {
                value = productInfoListGetValue(targetData, index);
            }
        }
        LOGGER.info("getProductInfoListValueForItemCode completed");
        return value;
    }

    /**
     * Get the price for an item
     *
     * @param itemCode The item code
     * @return The single item price
     */
    public double getItemPrice(String itemCode) {
        LOGGER.info("getItemPrice started");
        double value = cleanMonetaryStringToDouble(getProductInfoListValueForItemCode(itemCode, Constants.PRICE));
        LOGGER.info("getItemPrice completed");
        return value;
    }

    /**
     * Get the quantity of an item
     *
     * @param itemCode The item code
     * @return The item quantity
     */
    public int getItemQuantity(String itemCode) {
        LOGGER.info("getItemPrice started");
        int value = Integer.parseInt(getProductInfoListValueForItemCode(itemCode, ConstantsDtc.QUANTITY));
        LOGGER.info("getItemPrice completed");
        return value;
    }

    /**
     * Get the price of an item
     *
     * @param itemCode The item code
     * @return The item price (single item price multiplied by quantity)
     */
    public double getTotalItemPrice(String itemCode) {
        LOGGER.info("getItemPrice started");
        double value = getItemPrice(itemCode) * getItemQuantity(itemCode);
        LOGGER.info("getItemPrice completed");
        return value;
    }

    /**
     * Verify the selected vehicle on CheckFit
     *
     * @param vehicle String composed of "year make model"
     */
    public void verifySelectedVehicleOnCheckfit(String vehicle) {
        LOGGER.info("verifySelectedVehicleOnCheckfit started for '" + vehicle + "'");

        WebElement targetRow = null;

        List<WebElement> vehicleRows = webDriver.findElements(vehicleSelectedCheckFitBy);

        for (WebElement vehicleRow : vehicleRows) {
            String vehicleDetails = vehicleRow.findElement(CommonActions.vehicleDescriptionBy).getText().
                    toLowerCase().replace("\n", " ").replace(ConstantsDtc.EDIT_VEHICLE.toLowerCase(), " ").trim();

            if (CommonUtils.containsIgnoreCase(vehicleDetails.toLowerCase().replace(" ", ""), vehicle.toLowerCase().
                    replace(" ", ""))) targetRow = vehicleRow;
        }
        Assert.assertTrue("FAIL: Vehicle not displayed in recent vehicles", targetRow != null);

        LOGGER.info("verifySelectedVehicleOnCheckfit completed for '" + vehicle + "'");
    }

    /**
     * Return (true or false) whether a form group error message is displayed or not displayed
     *
     * @param message         The error message being evaluated
     * @param expectDisplayed true or false whether to expect that the message is displayed
     * @return boolean          true or false whether the message was displayed or not displayed based on expectation
     */
    public boolean formGroupErrorMessageDisplayedNotDisplayed(String message, boolean expectDisplayed) {
        LOGGER.info("formGroupErrorMessageDisplayedNotDisplayed started");
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        boolean returnStatus = false;
        boolean elementDisplayed = driver.isElementDisplayed(driver.getElementWithText(
                CommonActions.formGroupMessageErrorElementBy, message), Constants.ZERO);
        driver.resetImplicitWaitToDefault();
        if ((elementDisplayed && expectDisplayed) || (!elementDisplayed && !expectDisplayed))
            returnStatus = true;
        LOGGER.info("formGroupErrorMessageDisplayedNotDisplayed completed");
        return returnStatus;
    }

    /**
     * Switch to Originated Tab or Page
     */
    public void switchToOriginatedTabOrPage() {
        LOGGER.info("switchToOriginatedTabOrPage started");
        ArrayList<String> openedTabs = new ArrayList<String>(webDriver.getWindowHandles());
        if (openedTabs.size() > 1) {
            switchToPreviousOpenedTab();
        } else {
            driver.browserNavigateBackAction();
        }
        LOGGER.info("switchToOriginatedTabOrPage completed");
    }

    /**
     * Waits for the element with text, when the text attribute of the element is passed.
     *
     * @param text text attribute of the element
     */
    public void verifyButtonWithTextIsDisplayed(String text) {
        LOGGER.info("waitForElementWithText started");
        driver.getElementWithText(CommonActions.buttonBy, text);
        Assert.assertTrue("FAIL: The button with text '" + text + "' is not displayed.",
                driver.isElementDisplayed(driver.getElementWithText(CommonActions.buttonBy, text)));
        LOGGER.info("waitForElementWithText completed");
    }

    /**
     * Click staggered option tab
     *
     * @param tabValue The value of the tab
     */
    public void selectStaggeredTab(String tabValue) {
        LOGGER.info("selectStaggeredTab started");
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        WebElement optionElement = driver.getElementWithText(ProductListPage.staggeredOptionTabBy, tabValue);
        driver.webElementClick(optionElement);
        LOGGER.info("selectStaggeredTab completed");
    }

    /**
     * Verifies the specified element is not displayed
     *
     * @param elementName element name
     */
    public void assertElementNotDisplayed(String elementName) {
        LOGGER.info("assertElementNotDisplayed started");
        WebElement element = null;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        try {
            switch (elementName) {
                case FitmentPage.SUB_HEADER:
                    element = webDriver.findElement(FitmentPopupPage.StaggeredVehicleSubHeaderBy);
                    break;
                case ConstantsDtc.TREADWELL:
                    element = webDriver.findElement(FitmentPopupPage.staggeredTreadwellFitmentBoxBy);
                    break;
                case CommonActions.TABS:
                    element = webDriver.findElement(CommonActions.listingTabContainerBy);
                    break;
                default:
                    Assert.fail("FAIL: Input is invalid for " + elementName + ".Valid inputs are: "
                            + FitmentPage.SUB_HEADER + ", " + ConstantsDtc.TREADWELL + ", " + CommonActions.TABS + ".");
            }
        } catch (Exception e) {
            // continue
        }
        Assert.assertFalse("FAIL: The element " + elementName + " is displayed.",
                driver.isElementDisplayed(element, Constants.ZERO));
        driver.resetImplicitWaitToDefault();
        LOGGER.info("assertElementNotDisplayed completed");
    }

    /**
     * Closes browser with specified url
     *
     * @param urlText Partial text of URL in browser to be closed
     */
    public void closeBrowserWithUrl(String urlText) {
        LOGGER.info("closeBrowserWithUrl started for " + urlText);
        String mainUrl = webDriver.getCurrentUrl();
        Set allHandles = webDriver.getWindowHandles();
        Iterator iter = allHandles.iterator();
        while (iter.hasNext()) {
            webDriver.switchTo().window(iter.next().toString());
            if (CommonUtils.containsIgnoreCase(webDriver.getCurrentUrl(), urlText)) {
                webDriver.close();
                webDriver.switchTo().window(allHandles.iterator().next().toString());
                break;
            }
        }
        LOGGER.info("closeBrowserWithUrl completed for " + urlText);
    }

    /**
     * Verifies the presence of element with the specified text on Cart popup page.
     *
     * @param text can be any one of View Cart | Continue Shopping | Item added to cart
     *             | You can choose product add-ons and schedule installation during check out
     */
    public void assertCartPopUpOptions(String text) {
        LOGGER.info("assertCartPopUpOption started");
        driver.setImplicitWait(Constants.FIVE, TimeUnit.SECONDS);
        int attempts = 0;
        while (!driver.isElementDisplayed(addToCartModal) && attempts < Constants.TEN) {
            driver.waitOneSecond();
            attempts++;
        }
        if (CommonUtils.containsIgnoreCase(text, ConstantsDtc.VIEW_SHOPPING_CART)) {
            Assert.assertTrue("FAIL: Unable to find '" + text + "'", driver.isElementDisplayed(
                    addToCartModal.findElement(CommonActions.addToCartByClassBy)));
        } else if (CommonUtils.containsIgnoreCase(text, ConstantsDtc.CONTINUE_SHOPPING)) {
            Assert.assertTrue("FAIL: Unable to find '" + text + "'", driver.isElementDisplayed(
                    addToCartModal.findElement(CommonActions.addToCartSecondaryBy)));
        } else if (CommonUtils.containsIgnoreCase(text, ConstantsDtc.ITEM_ADDED_TO_CART)) {
            Assert.assertTrue("FAIL: Unable to find '" + text + "'",
                    driver.isElementDisplayed(CommonActions.cartTitle));
        } else if (CommonUtils.containsIgnoreCase(text, ConstantsDtc.CHOOSE_PRODUCT_ADD_ONS_MESSAGE)) {
            Assert.assertTrue("FAIL: Unable to find '" + text + "'", driver.isElementDisplayed(cartSubTitle));
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("assertCartPopUpOption completed");
    }

    /**
     * Write checkout customer populated data to scenario data
     */
    public void setCustomerScenarioData() {
        LOGGER.info("setCustomerScenarioData started");
        driver.waitForPageToLoad();
        List<WebElement> formValues = webDriver.findElements(CommonActions.formGroupBy);
        String fieldLabel = null;
        String actualValue = "";
        if (driver.isElementDisplayed(CheckoutPage.bopisBy)) {
            OrderXmls.customerDataList = OrderXmls.bopisCustomerDataList;
        }
        for (WebElement formValue : formValues) {
            String valueText = formValue.getText();
            String label = formValue.findElement(CommonActions.label).getText();
            if (label.contains("*")) {
                fieldLabel = label.split("\\*")[1].trim();
            } else {
                fieldLabel = label;
            }
            if (valueText.contains("\n")) {
                valueText = valueText.split("\n")[0].trim();
            }
            if (fieldLabel != null) {
                if (valueText.contains(ConstantsDtc.STATE) || valueText.contains(ConstantsDtc.COUNTRY)) {
                    actualValue = formValue.findElement(CommonActions.dropdownBy).getText();
                    if (actualValue.toLowerCase().contains(ConstantsDtc.ARMED_FORCE.toLowerCase())) {
                        actualValue = ConstantsDtc.STATE_AP;
                    } else if (actualValue.toLowerCase().contains(ConstantsDtc.STATE_NEVADA)) {
                        actualValue = Constants.STATE_NV;
                    }
                } else if (!driver.scenarioData.genericData.keySet().contains(fieldLabel)) {
                    actualValue = formValue.findElement(CommonActions.inputBy).getAttribute(Constants.VALUE);
                } else if (Config.getSiteRegion().equals(Constants.DTD)) {
                    actualValue = formValue.findElement(CommonActions.inputBy).getAttribute(Constants.VALUE);
                }
                driver.scenarioData.genericData.put(fieldLabel, actualValue);
            }
        }
        LOGGER.info("setCustomerScenarioData completed");
    }

    /**
     * Store string count and string name from an array list
     *
     * @param arrayName name of the Array list
     */
    public void getStringCountInArray(ArrayList<String> arrayName) {
        LOGGER.info("getStringCountInArray started");
        for (String name : arrayName) {
            Integer count = stringAndCount.get(name);
            if (count == null) {
                stringAndCount.put(name, 1);
            } else {
                stringAndCount.put(name, ++count);
            }
        }
        LOGGER.info("getStringCountInArray completed");
    }
    /**
     * Verifies Suggested Selling Carousel is displayed with no more than 5 products
     * Verifies Suggested Selling Title is displayed
     * Verifies no duplicate products in the carousel
     *
     * @param page      Homepage, PDP, Add to cart
     * @param displayed displayed or not displayed
     * @param title     carousel status
     */
    public void assertCarouselTitleProductListSizeAndNoDuplicates(String page, String displayed, String title) {
        LOGGER.info("assertCarouselTitleProductListSizeAndNoDuplicates started");
        List<WebElement> suggestedSellingArticlesList = null;
        if (page.equalsIgnoreCase(ConstantsDtc.HOMEPAGE)) {
            clickSiteLogo();
        }
        boolean carouselDisplayed = driver.isElementDisplayed(suggestedSellingCarousel, Constants.TWO);
        if (displayed.equalsIgnoreCase(ConstantsDtc.DISPLAYED)) {
            driver.waitForPageToLoad();
            Assert.assertTrue("FAIL: The suggested selling carousel is not displayed on " + page + " page.",
                    carouselDisplayed);
            driver.jsScrollToElement(suggestedSellingCarousel);
            if (page.toLowerCase().contains(Constants.CART)) {
                if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
                    suggestedSellingArticlesList = webDriver.findElement(dtModalContainerBy).
                            findElements(suggestedSellingTextContainerBy);
                } else {
                    suggestedSellingArticlesList = webDriver.findElement(addToCartModalBy).
                            findElements(suggestedSellingTextContainerBy);
                }
            } else {
                suggestedSellingArticlesList = webDriver.findElements(suggestedSellingTextContainerBy);
            }
            String carouselTitle = suggestedSellingCarouselTitle.getText();
            Assert.assertTrue("FAIL: There are more than " + MAX_CAROUSEL_LIST_SIZE + " suggestions that are " +
                    "displayed on the carousel", suggestedSellingArticlesList.size() <= MAX_CAROUSEL_LIST_SIZE);
            Assert.assertTrue("FAIL: The suggested selling title is not displayed",
                    driver.isElementDisplayed(suggestedSellingCarouselTitle));
            if (CommonUtils.containsIgnoreCase(title, ConstantsDtc.TRENDING_NEAR)) {
                String storeAddress = driver.scenarioData.genericData.get(Constants.STORE_DETAILS);
                String cityState = storeAddress.split("\n")[1];
                String city = cityState.split(",")[0];
                String state = cityState.split(",")[1].trim().substring(0, 2);
                title = ConstantsDtc.TRENDING_NEAR + " " + city;
                Assert.assertTrue("FAIL: The suggested selling carousel title '" + carouselTitle +
                        "' did not start with '" + title + "' and did not contain state: " + state + ".",
                        carouselTitle.toLowerCase().startsWith(title.toLowerCase()) &&
                                carouselTitle.toLowerCase().endsWith(state.toLowerCase()));
            } else {
                Assert.assertTrue("FAIL: The suggested selling carousel title '" + carouselTitle +
                        "' did not match with " + "expected title '" + title + "'.",
                        CommonUtils.containsIgnoreCase(carouselTitle, title));
            }
            suggestedSellingArticles.clear();
            for (WebElement suggestedSellingArticle : suggestedSellingArticlesList) {
                driver.jsScrollToElement(suggestedSellingArticle);
                try {
                    suggestedSellingArticles.add(suggestedSellingArticle.getText());
                } catch (Exception e) {
                    suggestedSellingArticles.add(suggestedSellingArticle.getText());
                }
            }
            List<String> duplicateValues = driver.getArrayListDuplicates(suggestedSellingArticles);
            Assert.assertTrue("FAIL: " + duplicateValues + " displayed twice on the carousel",
                    duplicateValues.isEmpty());
        } else {
            Assert.assertFalse("FAIL: The suggested selling carousel is displayed on " + page + " page.",
                    carouselDisplayed);
        }
        LOGGER.info("assertCarouselTitleProductListSizeAndNoDuplicates completed");
    }

    /**
     * Verify the product name and the position of the product on the suggested selling carousel
     *
     * @param productName     Name of the product
     * @param productType     Tires or wheels
     * @param productPosition Position of the product on the suggested selling carousel
     */
    public void assertProductPosition(String productName, String productType, int productPosition) {
        LOGGER.info("assertProductPosition started");
        List<WebElement> products = webDriver.findElements(suggestedSellingInnerTitleBy);
        ArrayList<String> carouselArticles = new ArrayList<String>();
        String productAtPosition = "";
        for (WebElement suggestedSellingProduct : products) {
            carouselArticles.add(suggestedSellingProduct.getText());
        }
        productAtPosition = carouselArticles.get(productPosition - 1);
        Assert.assertTrue("FAIL: The expected product " + productName + " did not match the actual product"
                + productAtPosition, productAtPosition.toLowerCase().contains(productName.toLowerCase()));
        Assert.assertTrue("FAIL: The expected product type " + productType + " did not match the actual product"
                + productAtPosition, productAtPosition.toLowerCase().contains(productType.toLowerCase()));
        LOGGER.info("assertProductPosition completed");
    }

    /**
     * Verify Feature Icon is displayed for the Tire Product listed in the Suggested Selling Carousel
     */
    public void assertToolTipIcon() {
        LOGGER.info("assertToolTipIcon started");
        List<WebElement> products = webDriver.findElements(suggestedSellingInnerTitleBy);
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getText().toLowerCase().contains(ConstantsDtc.TIRES.toLowerCase())) {
                Assert.assertTrue("FAIL: The expected icon did not display ",
                        driver.isElementDisplayed(products.get(i).findElement(suggestedSellingFeatureIconBy)));
                LOGGER.info("The tooltip icon displayed for the TIRE PRODUCT at position " + i);
            }
        }
        LOGGER.info("assertToolTipIcon completed");
    }

    /**
     * Verifies if the selected product is not displayed on the suggested selling carousel
     *
     * @param page PDP or Add to cart
     */
    public void assertSuggestedSellingProductsNotDisplayed(String page) {
        LOGGER.info("assertSuggestedSellingProductsNotDisplayed started for " + page + " page.");
        WebElement productByName = null;
        String productName;
        waitForSpinner();
        if (!driver.isElementDisplayed(suggestedSellingCarousel, Constants.ONE))
            return;
        if (page.equalsIgnoreCase(ConstantsDtc.PDP)) {
            productByName = webDriver.findElement(ProductDetailPage.productNameBy);
        } else if (page.equalsIgnoreCase(ConstantsDtc.ADD_TO_CART)) {
            if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
                productByName = webDriver.findElement(itemAddedToCartBy);
            } else {
                productByName = webDriver.findElement(modalProductInfoNameBy);
            }
        } else if (page.equalsIgnoreCase(ConstantsDtc.WHEEL_CONFIGURATOR)) {
            productByName = webDriver.findElement(WheelConfiguratorPage.wheelNameBy);
        }
        productName = productByName.getText();
        boolean found = suggestedSellingArticles.contains(productName);
        Assert.assertFalse("FAIL: The selected article is found in the suggested selling carousel", found);
        LOGGER.info("assertSuggestedSellingProductsNotDisplayed completed for " + page + " page.");
    }

    /**
     * Verifies a product is not displayed in the homepage or cart page carousel after adding it to the cart
     *
     * @param page homepage or cart
     */
    public void assertProductInCartNotDisplayedInCarousel(String page) {
        LOGGER.info("assertProductInCartNotDisplayedInCarousel started for " + page + " page.");
        List<WebElement> products = null;
        String productName;
        if (!driver.isElementDisplayed(suggestedSellingCarousel, Constants.ONE))
            return;
        driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
        driver.jsScrollToElement(suggestedSellingCarousel);
        if (page.equalsIgnoreCase(Constants.HOMEPAGE)|| page.equalsIgnoreCase(ConstantsDtc.PDP)) {
            products = webDriver.findElements(suggestedSellingInnerTitleBy);
            productName = CartPage.hashMap.get(PRODUCT_NAME);
        } else {
            if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
                products = dtModalContainer.findElements(itemsAddedToCartProductNameBy);
            } else {
                products = addToCartModal.findElements(suggestedSellingInnerTitleBy);
            }
            productName = CartPage.hashMap.get(ConstantsDtc.PRODUCT_NAME_CART);
        }
        for (WebElement suggestedSellingProduct : products) {
            Assert.assertFalse("FAIL: The Product added to the cart from the carousel " + productName + " displayed," +
                    "when it should not have displayed", suggestedSellingProduct.getText().toLowerCase().
                    contains(productName.toLowerCase()));
        }
        LOGGER.info(productName + "did not display in the carousel as expected");
        LOGGER.info("assertProductInCartNotDisplayedInCarousel completed for " + page + " page.");
    }

    /**
     * Clicks a product image on a specified page at a specified location
     *
     * @param position          - The position of the product on the page
     * @param page              - The page on which the product will be selected
     * @param requiredAvailable - true/false whether the product must be available
     */
    public void selectProductImageByPosition(String position, String page, boolean requiredAvailable) {
        LOGGER.info("selectProductImageByPosition started  at position " + position + " for " + page);
        driver.waitForPageToLoad();
        WebElement productImage;
        if (Config.isSafari() || Config.isIphone())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        List<WebElement> imageList;
        if (page.equalsIgnoreCase(ConstantsDtc.PLP)) {
            imageList = webDriver.findElements(ProductListPage.productImageBy);
        } else {
            // This covers compare products page
            if (page.equalsIgnoreCase(ConstantsDtc.COMPARE_SETS)) {
                imageList = webDriver.findElements(ProductListPage.compareSetProductImageBy);
                if (Config.isMobile())
                    imageList = webDriver.findElements(ProductListPage.compareSetProductMobileImageBy);
            } else {
                if (page.equalsIgnoreCase(ConstantsDtc.COMPARE_PRODUCTS)) {
                    imageList = webDriver.findElements(ProductListPage.compareProductImageBy);
                    if (Config.isMobile())
                        imageList = webDriver.findElements(ProductListPage.compareProductMobileImageBy);
                } else {
                    imageList = webDriver.findElements(suggestedSellingInnerTitleBy);
                }
            }
        }
        if (position.equalsIgnoreCase(Constants.FIRST)) {
            int imageIndex = 0;
            boolean done;
            productImage = driver.getDisplayedElement(imageList, imageIndex);
            if (page.equalsIgnoreCase(ConstantsDtc.PLP) && requiredAvailable) {
                do {
                    done = true;
                    productImage = imageList.get(imageIndex);
                    Assert.assertTrue("FAIL: There is no product image at position " + imageIndex + 1,
                            driver.isElementDisplayed(productImage, Constants.ZERO));
                    driver.jsScrollToElement(productImage);
                    String productText = driver.getParentElement(driver.getParentElement(
                            driver.getParentElement(productImage))).getText();
                    if (CommonUtils.containsIgnoreCase(productText, ProductListPage.NOT_AVAILABLE)) {
                        done = false;
                        imageIndex++;
                    } else {
                        List<WebElement> priceElements = driver.getParentElement(driver.getParentElement(productImage)).
                                findElements(CommonActions.productPriceBy);
                        for (WebElement priceElement : priceElements) {
                            if (priceElement.getText().equalsIgnoreCase(ConstantsDtc.NO_PRICE)) {
                                done = false;
                                imageIndex++;
                                continue;
                            }
                        }
                    }
                } while (!done);
            }
        } else {
            int positionNumber = Integer.parseInt(position.split("\\D")[0].trim());
            productImage = imageList.get(positionNumber - 1);
            if (page.equalsIgnoreCase(ConstantsDtc.PLP)) {
                String productText = driver.getParentElement(driver.getParentElement(
                        driver.getParentElement(productImage))).getText();
                Assert.assertFalse("FAIL: The product at the " + position + " position is not available",
                        CommonUtils.containsIgnoreCase(productText, ProductListPage.NOT_AVAILABLE));
            }
        }
        driver.jsScrollToElementClick(productImage);
        LOGGER.info("selectProductImageByPosition completed  at position " + position + " for " + page);
    }

    /**
     * Selects image with the passed in 'alt' attribute
     *
     * @param altText Text contained in the 'alt' attribute
     */
    public void selectImageWithAltText(String altText) {
        LOGGER.info("selectImageWithAltText started");
        WebElement image = driver.getElementWithAttribute(imgTagBy, Constants.ATTRIBUTE_ALT, altText);
        image.click();
        LOGGER.info("selectImageWithAltText completed");
    }


    /**
     * Verify if all the products displayed in the carousel are of specified size
     *
     * @param size Tire size or wheel size
     */
    public void assertCarouselProductsSize(String size) {
        LOGGER.info("assertCarouselProductsSize started for " + size + " size.");
        List<WebElement> viewDetailsSuggestedProducts = webDriver.findElements(suggestedSellingViewDetailsLinkBy);
        for (int i = 0; i < viewDetailsSuggestedProducts.size(); i++) {
            viewDetailsSuggestedProducts = webDriver.findElements(suggestedSellingViewDetailsLinkBy);
            driver.jsScrollToElement(viewDetailsSuggestedProducts.get(i));
            viewDetailsSuggestedProducts.get(i).click();
            boolean sizeVerification = fitmentBannerSizeInfo.getText().contains(size);
            Assert.assertTrue("FAIL: The Size of the product " + fitmentBannerSizeInfo.getText() + "" +
                    "does not match the expected size " + size, sizeVerification);
            driver.browserNavigateBackAction();
        }
        LOGGER.info("assertCarouselProductsSize completed for " + size + " size.");
    }

    /**
     * Returns whether a specified popup modal is displayed
     *
     * @param headerName    The popup modal header text
     * @return              true/false whether the popup modal is displayed
     */
    public boolean popupModalDisplayed(String headerName) {
        LOGGER.info("popupModalDisplayed started for '" + headerName + "' popup modal");
        boolean isDisplayed = false;
        if (driver.isElementDisplayed(dtModalContainer)) {
            if (CommonUtils.containsIgnoreCase(dtModalContainer.findElement(dtModalHeaderBy).getText(), headerName)) {
                isDisplayed = true;
            }
        }
        LOGGER.info("popupModalDisplayed completed for '" + headerName + "' popup modal");
        return isDisplayed;
    }

    /**
     * Get the cart item count from the page header
     *
     * @return cart item count
     */
    public String getCartItemCount() {
        LOGGER.info("getCartItemCount started");
        String cartItemCount = "";
        List<WebElement> cartItemElements = webDriver.findElements(HomePage.cartItemCountBy);
        for (WebElement cartItemElement : cartItemElements) {
            String elementText = cartItemElement.getText();
            if (!elementText.isEmpty()) {
                cartItemCount = CommonUtils.removeSpaces(elementText).replace("(", "").replace(")", "").
                        replace(Constants.ITEMS.toLowerCase(), "");
                break;
            }
        }
        Assert.assertFalse("FAIL: Unable to obtain the Cart item count from the page header", cartItemCount.isEmpty());
        LOGGER.info("getCartItemCount completed");
        return cartItemCount;
    }

    /**
     * Navigate to the previous page
     */
    public void navigateToPreviousPage() {
        String initialUrl = getCurrentUrl();
        driver.browserNavigateBackAction();
        if (CommonUtils.containsIgnoreCase(initialUrl, ConstantsDtc.COMPARE_PRODUCT_URL_SEGMENT) &&
                CommonUtils.containsIgnoreCase(getCurrentUrl(), ConstantsDtc.COMPARE_PRODUCT_URL_SEGMENT)) {
            driver.browserNavigateBackAction();
        }
    }

    /**
     * Select Edit Icon within fitment banner to edit vehicle
     */
    public void selectEditVehicle() {
        LOGGER.info("selectEditVehicle started");
        driver.jsScrollToElementClick(fitmentBanner.findElement(CommonActions.editIconBy), false);
        LOGGER.info("selectEditVehicle completed");
    }
}