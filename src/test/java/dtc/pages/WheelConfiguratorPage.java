package dtc.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class WheelConfiguratorPage {
    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private FitmentPage fitmentPage;

    public WheelConfiguratorPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        fitmentPage = new FitmentPage(driver);
        PageFactory.initElements(webDriver, this);
    }

    private final Logger LOGGER = Logger.getLogger(WheelConfiguratorPage.class.getName());

    private static final By reactSelectizeInputBy = By.className("resizable-input");

    private static final By wheelBannerBy = By.className("simple-banner__image");

    public static final By vehicleImageBy = By.cssSelector("[class*='wheel-configurator__left-column'][style*='image']");

    private static final By wheelImageBy = By.xpath("//*[contains(@class, 'wheel-list__image')]");

    private static final By wheelBrandBy = By.className("wheel-list__brand");

    private static final By wheelPriceBy = By.className("wheel-list__price");

    private static final By wheelPriceBlockBy = By.cssSelector("[class*='price__price-block']");

    public static final By wheelNameBy = By.className("wheel-list__name");

    private static final By wheelListItemBy = By.className("wheel-list__list-item");

    private static final By lastWheelListItem = By.className("wheel-list__list-item__message");

    public static final By facetDropDown = By.className("new-select__hot-spot");

    public static final By wheelConfiguratorResultTextBy = By.className("filter-block__results");

    public static final By viewDetailsButtonBy = By.className("[class*='product__view-details-wheel-link']");


    @FindBy(className = "list-item__message--internal")
    WebElement listEndingMessage;

    @FindBy(className = "wheel-list")
    WebElement wheelList;

    @FindBy(className = "fitment-block__subtitle")
    public static WebElement wheelconfigSubTitle;

    @FindBy(className = "fitment-block__title")
    public static WebElement wheelconfigPageTitle;

    @FindBy(className = "wheel-configurator__close-button")
    public static WebElement wheelConfigClose;

    @FindBy(className = "fa-share-alt")
    public static WebElement shareOption;

    @FindBy(className = "filter-block__results")
    public static WebElement totalwheelCountOnHeader;

    @FindBy(className = "vehicle-heading__title")
    public static WebElement vehicleName;

    @FindBy(className = "wheel-list__image")
    public static WebElement wheelImg;

    @FindBy(className = "filter-block__button")
    public static WebElement wheelFilter;

    @FindBy(className = "wheel-list__details-link")
    public static WebElement viewDetails;

    @FindBy(xpath = "//a[contains(@class,'fitment-verification__vehicle')]")
    public static WebElement vehicleDetails;

    @FindBy(className = "cart-item__column-header--my-store")
    public static WebElement instoreCartMsg;

    @FindBy(className = "cart-item__column-header")
    public static WebElement toShipCartMsg;

    @FindBy(className = "form-group__text")
    public static WebElement email;

    @FindBy(xpath = "//input[@name='phone']")
    public static WebElement phone;

    @FindBy(className = "SocialMediaShareButton--facebook")
    public static WebElement facebookShare;

    @FindBy(className = "fa-google-plus")
    public static WebElement googlePlus;

    @FindBy(className = "fa-twitter")
    public static WebElement twitterShare;

    @FindBy(className = "fa-pinterest-p")
    public static WebElement pinterestShare;

    @FindBy(className = "comment-dots")
    public static WebElement mobileShare;

    @FindBy(xpath = "(//input[contains(@class,'auto-results-row-quantity')])[2]")
    public static WebElement itemQuantity;

    @FindBy(className = "auto-results-row-quantity")
    public static WebElement quantityPdp;

    @FindBy(className = "view-on-vehicle-box__unavailable")
    public static WebElement unavailableScreen;

    @FindBy(className = "wheel-configurator-filters")
    public static WebElement wheelConfiguratorFilters;

    @FindBy(className = "vehicle-heading")
    public static WebElement vehicleHeading;

    @FindBy(className = "new-select__wrapper--size")
    public static WebElement wheelSizeDropDown;

    @FindBy(className = "wheel-configurator__close-button--share")
    public static WebElement shareSectionCloseButton;

    public static final String IN_STORE = "In Store";
    public static final String TO_SHIP = "To Ship";
    private static final String ALL_RESULTS_LOADED_MESSAGE = "All results loaded";

    /**
     * Verifies the presence of Wheel Banner on page.
     */
    public void assertWheelBanner() {
        LOGGER.info("assertWheelBanner started");
        Assert.assertTrue("FAIL: Wheel Banner is not displayed on Wheel Configurator page.",
                driver.isElementDisplayed(wheelBannerBy));
        LOGGER.info("assertWheelBanner completed");
    }

    /**
     * Performs click on the "BROWSE WHEELS" button.
     */
    public void selectBrowseWheels() {
        LOGGER.info("selectBrowseWheels started");
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            driver.waitForElementClickable(HomePage.browseWheelsStg);
            HomePage.browseWheelsStg.click();
        } else {
            driver.waitForElementClickable(HomePage.browseWheels);
            HomePage.browseWheels.click();
        }
        LOGGER.info("selectBrowseWheels completed");
    }

    /**
     * Verifies the title and subtitle on the Wheel Configurator page.
     *
     * @param title      title type to verify (title | subtitle)
     * @param inputTitle string value of title to verify
     */
    public void assertWheelConfigPageTitles(String title, String inputTitle) {
        LOGGER.info("assertWheelConfigPageTitles started");
        String actualWheelPageLabel = null;
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        switch (title) {
            case Constants.TITLE:
                actualWheelPageLabel = wheelconfigPageTitle.getText();
                break;
            case Constants.SUBTITLE:
                actualWheelPageLabel = wheelconfigSubTitle.getText();
                break;
        }
        Assert.assertEquals("FAIL: Incorrect " + title + " on wheel configurator page",
                inputTitle, actualWheelPageLabel);
        LOGGER.info("assertWheelConfigPageTitles completed");
    }

    /**
     * Verifies the presence of Wheel Results on UI.
     */
    public void assertWheelResults() {
        LOGGER.info("assertWheelResults started");
        driver.waitForPageToLoad(Constants.SIX_THOUSAND);
        String wheelCount = totalwheelCountOnHeader.getText();
        int intWheelCount = commonActions.cleanMonetaryStringToInt(wheelCount);
        Assert.assertTrue("FAIL: Unable to display the Wheel Results.", intWheelCount > 1);
        LOGGER.info("assertWheelResults completed");
    }

    /**
     * Verifies the vehicle displayed on Wheel Page by Name.
     *
     * @param expectedVehicle vehicle parameter required to verify for e.g: make, model.
     */
    public void assertVehicleByModel(String expectedVehicle) {
        LOGGER.info("assertVehicleByModel started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(vehicleName);
        String actualVehicleModel = vehicleName.getText();
        Assert.assertTrue("FAIL: Expected Vehicle on the Wheel Configurator popup should be: "
                + expectedVehicle + ". Actual heading displayed on the Wheel Configurator popup is "
                + actualVehicleModel, actualVehicleModel.contains(expectedVehicle));
        LOGGER.info("assertVehicleByModel completed");
    }

    /**
     * Verifies the vehicle properties -> Name, Brand, Image, Price getting displayed on UI.
     */
    public void assertWheelProperties() {
        LOGGER.info("assertWheelProperties started");
        driver.setImplicitWait(Constants.TWO, TimeUnit.SECONDS);
        List<WebElement> wheelListItems = null;
        driver.resetImplicitWaitToDefault();
        List<String> properties = new ArrayList<>();
        properties.add(ConstantsDtc.IMAGE);
        properties.add(ConstantsDtc.BRAND);
        properties.add(Constants.NAME);
        properties.add(Constants.PRICE);
        int row = 1;
        int i = 0;
        do {
            wheelListItems = wheelList.findElements(wheelListItemBy);
            if (i + 1 != wheelListItems.size())
                driver.jsScrollToElement(wheelListItems.get(i + 1));
            else
                driver.jsScrollToElement(wheelListItems.get(i));
            driver.waitForMilliseconds(Constants.THREE_HUNDRED);
            if (wheelListItems.get(i).getText().equalsIgnoreCase(ALL_RESULTS_LOADED_MESSAGE))
                break;
            for (String property : properties) {
                boolean done = false;
                int attempts = 0;
                do {
                    WebElement element = null;
                    driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
                    try {
                        switch (property) {
                            case ConstantsDtc.IMAGE:
                                element = wheelListItems.get(i).findElement(wheelImageBy);
                                break;
                            case ConstantsDtc.BRAND:
                                element = wheelListItems.get(i).findElement(wheelBrandBy);
                                break;
                            case Constants.NAME:
                                element = wheelListItems.get(i).findElement(wheelNameBy);
                                break;
                            case Constants.PRICE:
                                try {
                                    element = wheelListItems.get(i).findElement(wheelPriceBy);
                                } catch (Exception e) {
                                    element = wheelListItems.get(i).findElement(wheelPriceBlockBy);
                                }
                                break;
                        }
                    } catch (Exception e) {
                        //continue
                    }
                    driver.resetImplicitWaitToDefault();
                    if (driver.isElementDisplayed(element, Constants.ZERO))
                        done = true;
                    else {
                        attempts++;
                        driver.waitOneSecond();
                    }
                } while (!done && attempts < 5);
                Assert.assertTrue("FAIL: Wheel " + property + " was not displayed on row " + row + " of the wheel " +
                        "list on Wheel Configurator page.", done);
            }
            row++;
            i++;
        } while (i < wheelListItems.size());
        LOGGER.info("assertWheelProperties completed");
    }

    /**
     * Verifies the vehicle count displayed  on header with the wheels displayed on the page .
     */
    public void assertWheelCount() {
        LOGGER.info("assertWheelCount started");
        int displayedCount = commonActions.cleanMonetaryStringToInt(totalwheelCountOnHeader.getText()
                .replaceAll("[^0-9]", ""));
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        int counter = 0;
        int listCount = 0;
        do {
            listCount = webDriver.findElements(wheelNameBy).size();
            if (listCount == displayedCount)
                break;
            webDriver.findElements(wheelNameBy).get(listCount - 1).click();
            commonActions.waitForSpinner();
            js.executeScript("arguments[0].scrollIntoView();", webDriver.findElement(lastWheelListItem));
            driver.waitSeconds(Constants.TWO);
            counter++;
        } while (listCount != displayedCount && counter < Constants.FIVE);
        Assert.assertEquals("FAIL: Wheel count displayed on the header is not correct.", listCount, displayedCount);
        LOGGER.info("assertWheelCount completed");
    }

    /**
     * Clicks edit icon on Wheel Configurator page to change vehicle .
     */
    public void clickEditVehicleIcon() {
        LOGGER.info("clickEditVehicleIcon started");
        driver.jsScrollToElementClick(CommonActions.editIcon);
        LOGGER.info("clickEditVehicleIcon completed");
    }

    /**
     * Verifies that Wheel has a default size and color .
     *
     * @param option Size or Color to verify
     */
    public void assertDefaultWheelSizeColor(String option) {
        LOGGER.info("assertDefaultWheelSizeColor Started");
        List<WebElement> listBox = webDriver.findElements(By.tagName(Constants.SELECT.toLowerCase()));
        if (option.contains(ConstantsDtc.WHEEL_COLOR)) {
            LOGGER.info("Default Selected Wheel Color is " + listBox.get(0).getText());
            Assert.assertTrue("FAIL: No Color displayed as selected for vehicle ",
                    listBox.get(0).getText() != null);
        } else if (option.contains(ConstantsDtc.WHEEL_SIZE)) {
            try {
                String text = vehicleHeading.getText();
                String[] textArray = text.split("\n");
                String targetText = "";
                for (String textSegment : textArray) {
                    if (textSegment.contains(ConstantsDtc.WHEELS_NAME)) {
                        targetText = textSegment.substring(Constants.ZERO, Constants.TWO);
                        break;
                    }
                }
                int size = Integer.parseInt(targetText);
                LOGGER.info("Default Selected Wheel Size is " + size);
            } catch (Exception e) {
                Assert.fail("FAIL: No wheel size displayed on Wheel Configurator header");
            }
        }
        LOGGER.info("assertDefaultWheelSizeColor Completed");
    }

    /**
     * Selects the dropdown option to open on page.
     *
     * @param option Size or Color to select.
     * @param value  value to be set on the drop down
     */
    public void chooseWheelSize(String option, String value) throws Exception {
        LOGGER.info("chooseWheelSize started");
        WebElement parentElement;
        driver.waitForPageToLoad();
        parentElement = webDriver.findElement
                (By.cssSelector("[class*='react-selectize default root-node simple-select fitment-block__select--" + option.toLowerCase() + "']"));
        try {
            fitmentPage.selectDropDownValue(parentElement, value);
        } catch (Exception e) {
            parentElement = webDriver.findElement
                    (By.cssSelector("[class*='react-selectize default root-node simple-select fitment-block__select--" + option.toLowerCase() + "']"));
            fitmentPage.selectDropDownValue(parentElement, value);
        }
        LOGGER.info("chooseWheelSize completed");
    }

    /**
     * Performs click operation on Filter button.
     */
    public void chooseFilter() {
        LOGGER.info("chooseFilter started");
        driver.jsScrollToElementClick(wheelFilter);
        driver.waitOneSecond();
        LOGGER.info("chooseFilter completed");
    }

    /**
     * Verifies the presence of all the wheel filters on the configurator page.
     */
    public void assertWheelFilteringOptions() {
        LOGGER.info("assertWheelFilteringOptions started");
        List<String> expectedOptions = ConstantsDtc.wheelFilterCategoriesList;
        if (driver.scenarioData.isStaggeredProduct())
            expectedOptions.remove(ConstantsDtc.DIAMETER);
        List<WebElement> displayedOptionElements = webDriver.findElements(CommonActions.productListFilter);
        ArrayList<String> displayedOptions = new ArrayList<String>(displayedOptionElements.size());
        for (WebElement displayedOptionElement : displayedOptionElements) {
            String option = displayedOptionElement.getText();
            displayedOptions.add(option);
        }
        Assert.assertTrue("FAIL: Unable to find the Facet Filters  " + expectedOptions +
                        " in displayed section: " + displayedOptions,
                displayedOptions.containsAll(expectedOptions));
        LOGGER.info("assertWheelFilteringOptions completed");
    }

    /**
     * Choosing a Given Brand
     *
     * @param brand brand name to choose from  list
     */
    public void selectBrand(String brand) {
        LOGGER.info("selectBrand  started ");
        webDriver.findElement(By.xpath("(//*[text()='Brands'])[1]/following-sibling::ul//label[contains(text(),'"
                + brand + "')]")).click();
        LOGGER.info("selectBrand  completed ");
    }

    /**
     * Selects a wheel from the wheel list displayed.
     */
    public void selectWheelOnPage() {
        LOGGER.info("selectWheelOnPage started");
        selectWheelOnWheelConfiguratorPage("");
        LOGGER.info("selectWheelOnPage completed");
    }

    /**
     * selects a wheel from the wheel list displayed with specified button.
     *
     * @param buttonName
     */
    public void selectWheelOnPage(String buttonName) {
        LOGGER.info("selectWheelOnPage with '" + buttonName + "' button started");
        selectWheelOnWheelConfiguratorPage(buttonName);
        LOGGER.info("selectWheelOnPage with '" + buttonName + "' button completed");
    }

    /**
     * Selects a wheel from the wheel list displayed with specified button.
     *
     * @param buttonName
     */
    private void selectWheelOnWheelConfiguratorPage(String buttonName) {
        LOGGER.info("selectWheelOnWheelConfiguratorPage with '" + buttonName + "' button started");
        driver.waitForElementVisible(wheelList);
        boolean found = false;
        driver.waitOneSecond();
        boolean buttonFound = false;
        List<WebElement> wheels = wheelList.findElements(CommonActions.listItemBy);
        // Below code makes sure to click a wheel, whose combination is available with the selected vehicle.
        for (WebElement wheel : wheels) {
            if (wheel.getText().isEmpty()) {
                continue;
            }
            driver.jsScrollToElementClick(wheel);
            driver.waitOneSecond();
            driver.setImplicitWait(Constants.ONE, TimeUnit.SECONDS);
            if (buttonName.equalsIgnoreCase(ConstantsDtc.VIEW_DETAILS)) {
                try {
                    WebElement buttonElement = wheel.findElement(viewDetailsButtonBy);
                    if (buttonElement == null) {
                        continue;
                    }
                    buttonFound = true;
                } catch (Exception e) {
                    continue;
                }
            }
            if (buttonName.equalsIgnoreCase(ConstantsDtc.ADD_TO_CART)) {
                try {
                    WebElement buttonElement = wheel.findElement(CommonActions.addToCartByClassBy);
                    if (buttonElement == null) {
                        continue;
                    }
                    buttonFound = true;
                } catch (Exception e) {
                    continue;
                }
            }
            driver.resetImplicitWaitToDefault();
            if (buttonName.isEmpty() || buttonFound) {
                if (!driver.isElementDisplayed(unavailableScreen, Constants.THREE)) {
                    found = true;
                    break;
                }
            }
        }
        Assert.assertTrue("FAIL: Unable to find valid wheel to select on Wheel Configurator page", found);
        LOGGER.info("selectWheelOnWheelConfiguratorPage with '" + buttonName + "' button completed");
    }

    /**
     * Verifies the page contains the specified wheel details
     *
     * @param option Option can be Clear All | Apply Filters
     */
    public void chooseFilterOptions(String option) {
        LOGGER.info("chooseFilterOptions started");
        if (option.contains(ConstantsDtc.CLEAR_ALL)) {
            CommonActions.clearAllFiltersLink.click();
        } else if (option.contains(ConstantsDtc.APPLY_FILTERS)) {
            CommonActions.applyFiltersButton.click();
        }
        LOGGER.info("chooseFilterOptions completed");
    }

    /**
     * Verifies the page contains the specified wheel details
     *
     * @param option Option can be View Details | Share | X
     */
    public void chooseWheelConfigOption(String option) {
        LOGGER.info("chooseWheelConfigOption started");
        driver.waitForPageToLoad();
        if (option.equalsIgnoreCase(ConstantsDtc.VIEW_DETAILS)) {
            viewDetails.click();
        } else if (option.equalsIgnoreCase(ConstantsDtc.SHARE)) {
            shareOption.click();
        } else if (option.equalsIgnoreCase(Constants.CLOSE)) {
            wheelConfigClose.click();
        }
        LOGGER.info("chooseWheelConfigOption completed");
    }

    /**
     * Verifies the previously selected wheel is in focus on page
     */
    public void assertPreviouslySelectedWheel() {
        LOGGER.info("assertPreviouslySelectedWheel started");
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        Assert.assertTrue("FAIL: Unable to focus on the last selected wheel", viewDetails.isDisplayed());
        LOGGER.info("assertPreviouslySelectedWheel completed");
    }

    /**
     * Verifies the previously added filters on page
     *
     * @param filterValue Can be any text from the available list of filters.
     */
    public void assertPreviousAppliedFilters(String filterValue) {
        LOGGER.info("assertPreviousAppliedFilters started");
        List<WebElement> appliedFiltersElement = webDriver.findElements(CommonActions.strongTagBY);
        ArrayList<String> appliedFilters = new ArrayList<String>();
        for (WebElement element : appliedFiltersElement) {
            appliedFilters.add(element.getText());
        }
        Assert.assertTrue("FAIL: Unable to find the Facet Filters  " + filterValue +
                        " in displayed section: " + appliedFilters,
                appliedFilters.contains(filterValue));
        LOGGER.info("assertPreviousAppliedFilters completed");
    }

    /**
     * Verify the Shopping Page  label for In Store/To Ship
     *
     * @param itemType In Store/To Ship item
     * @param label    In Store/To Ship item's label
     */
    public void assertShoppingCartlabel(String itemType, String label) {
        LOGGER.info("assertShoppingCartlabel started ");
        driver.waitForPageToLoad();
        if (itemType.contains(IN_STORE)) {
            Assert.assertTrue("FAIL:Instore Item Order Message Should be Displayed as " + label +
                    " Displayed text is " + instoreCartMsg.getText(), instoreCartMsg.getText().equalsIgnoreCase(label));
            LOGGER.info("checkTextForInstoreItem  Completed");

        } else if (itemType.contains(TO_SHIP)) {
            Assert.assertTrue("FAIL:To Ship Item Order Message Should be Displayed as " + label +
                    " Displayed text is " + toShipCartMsg.getText(), toShipCartMsg.getText().equalsIgnoreCase(label));
        }
        LOGGER.info("assertShoppingCartlabel completed ");
    }

    /**
     * Verifies the presence of different sharing options available on page.
     *
     * @param option can be any option from Facebook | Google Plus | Pinterest | Twitter | Email | Mobile
     */
    public void assertSocialSites(String option) {
        LOGGER.info("assertSocialSites started ");
        if (option.contains(ConstantsDtc.EMAIL)) {
            Assert.assertTrue("FAIL:Unable to find the " + option, CommonActions.emailEnvelopeIcon.isDisplayed());
        } else if (option.toLowerCase().contains(ConstantsDtc.FACEBOOK)) {
            Assert.assertTrue("FAIL:Unable to find the " + option, facebookShare.isDisplayed());
        } else if (option.contains(ConstantsDtc.GOOGLE_PLUS)) {
            Assert.assertTrue("FAIL:Unable to find the " + option, googlePlus.isDisplayed());
        } else if (option.toLowerCase().contains(ConstantsDtc.PINTEREST)) {
            Assert.assertTrue("FAIL:Unable to find the " + option, pinterestShare.isDisplayed());
        } else if (option.toLowerCase().contains(ConstantsDtc.TWITTER)) {
            Assert.assertTrue("FAIL:Unable to find the " + option, twitterShare.isDisplayed());
        } else if (option.contains(ConstantsDtc.MOBILE)) {
            Assert.assertTrue("FAIL:Unable to find the " + option, mobileShare.isDisplayed());
        } else if (option.contains(ConstantsDtc.PINTEREST)) {
            Assert.assertTrue("FAIL:Unable to find the " + option, pinterestShare.isDisplayed());
        }
        LOGGER.info("assertSocialSites completed ");
    }

    /**
     * Checking the default cart quantity
     *
     * @param quantity value to check
     */
    public void assertQuantity(String quantity) {
        LOGGER.info("assertQuantity started");
        String defaultQuantity = webDriver.findElement(CommonActions.cartItemQuantityBy).getAttribute(Constants.VALUE);
        Assert.assertTrue("FAIL:Expected Quantity in the Cart is " + quantity +
                ". Actual Quantity in the Cart is " + defaultQuantity, defaultQuantity.equals(quantity));
        LOGGER.info("assertQuantity  completed");
    }

    /**
     * Performs click operation on specified sharing option
     *
     * @param option: can be any option from Facebook | Google Plus | Pinterest | Twitter
     */
    public void chooseSocialSites(String option) {
        LOGGER.info("chooseSocialSites started");
        if (option.toLowerCase().contains(ConstantsDtc.FACEBOOK)) {
            facebookShare.click();
        } else if (option.contains(ConstantsDtc.GOOGLE_PLUS)) {
            googlePlus.click();
        } else if (option.toLowerCase().contains(ConstantsDtc.PINTEREST)) {
            pinterestShare.click();
        } else if (option.toLowerCase().contains(ConstantsDtc.TWITTER)) {
            twitterShare.click();
        }
        LOGGER.info("chooseSocialSites completed");
    }

    /**
     * Verify the Wheel Configurator filters are displayed or not displayed
     *
     * @param displayStatus indicates whether to expect the filters to be displayed or not displayed
     */
    public void verifyWheelConfiguratorFiltersDisplayedNotDisplayed(String displayStatus) {
        LOGGER.info("verifyWheelConfiguratorFiltersDisplayedNotDisplayed started. Filter expected to be " + displayStatus);
        if (displayStatus.equals(Constants.DISPLAYED)) {
            Assert.assertTrue("FAIL: The Wheel Configurator Filters are not displayed",
                    driver.isElementDisplayed(wheelConfiguratorFilters));
        } else {
            Assert.assertFalse("FAIL: The Wheel Configurator Filters are displayed",
                    driver.isElementDisplayed(wheelConfiguratorFilters));
        }
        LOGGER.info("verifyWheelConfiguratorFiltersDisplayedNotDisplayed completed. Filter expected to be " + displayStatus);
    }

    /**
     * selects a wheel from the wheel list displayed. by name and brand
     *
     * @param wheelBrand name of Wheel brand to be selected
     * @param wheelName  name of Wheel to be selected
     */
    public void selectWheelOnPage(String wheelBrand, String wheelName) {
        LOGGER.info("selectWheelOnPage started");
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        while (driver.isElementDisplayed(CommonActions.preloader, Constants.ONE))
            driver.waitForMilliseconds();
        driver.waitForElementVisible(wheelList);
        List<WebElement> wheels = wheelList.findElements(CommonActions.listItemBy);
        for (WebElement wheel : wheels) {
            WebElement brand = wheel.findElement(wheelBrandBy);
            WebElement name = wheel.findElement(wheelNameBy);
            if (brand.getText().equalsIgnoreCase(wheelBrand) && name.getText().equalsIgnoreCase(wheelName)) {
                driver.jsScrollToElementClick(wheel);
                while (driver.isElementDisplayed(CommonActions.preloader))
                    driver.waitForMilliseconds();
                if (!driver.isElementDisplayed(unavailableScreen)) {
                    break;
                }
            }
        }
        LOGGER.info("selectWheelOnPage completed");
    }

    /**
     * Verify the vehicle image of Wheel Configurator link contains the search text
     *
     * @param text string that will be search on link image
     */
    public void verifyImageText(String text) {
        LOGGER.info("verifyImageText started");
        WebElement image = webDriver.findElement(vehicleImageBy);
        driver.waitForElementClickable(image);
        Assert.assertTrue("FAIL: text not found!", image.getAttribute(Constants.ATTRIBUTE_SRC).toLowerCase().contains(text.toLowerCase()));
        LOGGER.info("verifyImageText completed");
    }

    /**
     * Select a wheel size in the wheel configurator
     *
     * @param size value to select
     */
    public void selectWheelSize(String size) {
        LOGGER.info("selectWheelSize started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(wheelSizeDropDown, Constants.TEN);
        if (!wheelSizeDropDown.getAttribute(Constants.CLASS).contains(Constants.OPEN)) {
            if (Config.isSafari() || Config.isIphone() || Config.isIpad() || Config.isIe()) {
                wheelSizeDropDown.findElement(reactSelectizeInputBy).click();
            } else if (Config.isFirefox()) {
                wheelSizeDropDown.findElement(CommonActions.reactSelectizePlaceholderBy).click();
            } else {
                wheelSizeDropDown.click();
            }
        }
        List<WebElement> options = wheelSizeDropDown.findElements(CommonActions.listItemBy);
        for (WebElement option : options) {
            if (option.getAttribute("innerHTML").equals(size)) {
                option.click();
                break;
            }
        }
        LOGGER.info("selectWheelSize completed");
    }

    /**
     * Enter email address on Wheel Configurator page
     *
     * @param emailAddress  email address to enter
     */
    public void enterEmailAddress(String emailAddress) {
        LOGGER.info("enterEmailAddress started for '" + emailAddress + "' on Wheel Configurator page");
        if (!driver.isElementDisplayed(email, Constants.ZERO)) {
            closeShareSection();
            driver.webElementClick(CommonActions.emailEnvelopeIcon);
            driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        }
        email.sendKeys(emailAddress);
        LOGGER.info("enterEmailAddress completed for '" + emailAddress + "' on Wheel Configurator page");
    }

    /**
     * Enter phone number on Wheel Configurator page
     *
     * @param phoneNumber  phone number to enter
     */
    public void enterPhoneNumber(String phoneNumber) {
        LOGGER.info("enterPhoneNumber started for '" + phoneNumber + "' on Wheel Configurator page");
        if (!driver.isElementDisplayed(phone, Constants.ZERO)) {
            closeShareSection();
            driver.webElementClick(mobileShare);
            driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        }
        phone.sendKeys(phoneNumber);
        LOGGER.info("enterPhoneNumber completed for '" + phoneNumber + "' on Wheel Configurator page");
    }

    /**
     * Close the Wheel Configurator close section
     */
    private void closeShareSection() {
        if (driver.isElementDisplayed(shareSectionCloseButton, Constants.ZERO)) {
            driver.webElementClick(shareSectionCloseButton);
            driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        }
    }
}
