package dtc.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by Channing Luden on 10/6/2016.
 */
public class StoreLocatorPage {

    private HomePage homePage;
    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;

    private final Logger LOGGER = Logger.getLogger(StoreLocatorPage.class.getName());

    public StoreLocatorPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        homePage = new HomePage(driver);
        PageFactory.initElements(webDriver, this);
    }

    private static final String CURRENT_STORE_LABEL = "current store";
    private static final String CURRENT_STORE_BLUE = "rgba(1, 177, 227, 1)";

    @FindBy(xpath = "//button[text()='Search']")
    public static WebElement storeLocatorSearchButton;

    @FindBy(id = "opposite")
    public static WebElement welcomePopUpSearchLink;

    @FindBy(className = "activate-message")
    public static WebElement currentStoreActive;

    @FindBy(className = "button[aria-label='Make This My Store']")
    public static WebElement makeThisMyStoreButton;

    @FindBy(css = "[class*='shop-dtd__content']")
    public static WebElement shopDtdContent;

    @FindBy(css = "[class*='increase-radius-message']")
    public static WebElement increaseRadisMessage;

    @FindBy(css = "[class*='store-change-modal__warning-header-wrapper']")
    public static WebElement storeChangeModalWarning;

    @FindBy(css = "[class*='dt-button-lg dt-button-lg--ghost']")
    public static WebElement makeThisMyStore;

    private static final By storeNameInStoreLocatorResults = By.className("link-quaternary");

    private static final By currentSelectedStoreInStoreLocatorResults =
            By.cssSelector("[class=\"activate-message store-locator__current\"]");

    private static final By directionsLinks = By.linkText(ConstantsDtc.DIRECTIONS);

    private static final By sendToPhoneResultLinks = By.className("sendstoretophone");

    private static final By scheduleApptResultButtons = By.xpath("//button[normalize-space()='Schedule appointment']");

    public static final By storeQuickViewContainerBy = By.cssSelector("store-quick-view__container");

    public static final By myStoreBadgeBy = By.cssSelector("[class*='my-store-badge__badge']");

    private static final By makeThisMyStoreBy = By.cssSelector("[aria-label='Make This My Store']");

    private static final By storeInfoBox = By.className("quick-view__store-info");

    public static final By storeBy = By.cssSelector("[href*='/store/']");

    public static final By welcomePopUpWindow = By.className("store-pop");

    public static final By hoursOpTextBy = By.cssSelector("[class*='store-quick-view__store-hours']");

    public static final By storeRowBy = By.cssSelector("[class*='store-quick-view-list__list-item']");

    public static final By changeStoreModalBy = By.cssSelector("[class*='store-change-modal__modal-body']");

    public static final By storeLocatorPageBy = By.cssSelector("[class*='store-locator-page']");

    public static final By storeLocationListItemBy = By.cssSelector("[class*='locations-list__location-list-item']");

    public static final By selectedStoreBy = By.className("selected-store");

    public static final By scheduleAppointmentBy = By.cssSelector("button[aria-label='Schedule Appointment']");

    public static final By nearestStoreDistanceBy = By.cssSelector("[class*='store-locator-page__nearby-stores'] [class*='store-quick-view__distance']");

    public boolean currentStoreSelected = false;


    /**
     * Searches for a store within a specified range, using city and state or zip code.
     *
     * @param searchRange    range or distance with which to limit the store locator search. Current valid selections
     *                       are 10, 25, 50, and 75 miles.
     * @param cityStateOrZip city and state or zip code for store locator search.
     */
    public void searchForStore(String searchRange, String cityStateOrZip) {
        LOGGER.info("searchForStore started for " + cityStateOrZip + " with a search range " + searchRange);
        if (Config.getSiteRegion().contains(ConstantsDtc.AT)) {
            commonActions.closeWelcomePopUp(Constants.CONTINUE);
        }
        if (!CommonActions.simpleValue.getText().contains(searchRange)) {
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
            CommonActions.simpleValue.click();
            driver.clickElementWithText(CommonActions.dropdownOptionBy, searchRange);
        }
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        commonActions.clearAndPopulateEditField(CommonActions.cityStateZipSearchField, cityStateOrZip);

        storeLocatorSearchButton.click();
            driver.waitForPageToLoad();
        if (Config.getSiteRegion().contains(ConstantsDtc.AT)) {
            commonActions.closeWelcomePopUp(Constants.CONTINUE);
        }
        LOGGER.info("searchForStore completed for " + cityStateOrZip + " with a search range " + searchRange);
    }

    /**
     * Asserts store location search results on first page all contain the expected site name.
     *
     * @param siteToValidate name of the site, used in validating the store location names match as expected
     */
    public void confirmStoreFoundForSite(String siteToValidate) {
        LOGGER.info("confirmStoreFoundForSite started");
        List<WebElement> searchResultStoreNames = webDriver.findElements(storeNameInStoreLocatorResults);
        for (WebElement searchResult : searchResultStoreNames) {
            Assert.assertTrue("FAIL: Store location search results \"" + searchResult.getText()
                            + "\" did not contain \"" + siteToValidate + "\"!",
                    searchResult.getText().contains(siteToValidate));
            LOGGER.info("Confirmed search results store name matched \"" + siteToValidate + "\".");
        }
        LOGGER.info("confirmStoreFoundForSite completed");
    }

    /**
     * Clicks Make This My Store button
     *
     * @param closeStoreChangedModal true/false whether the 'Store Changed' modal should be closed
     */
    public void clickMakeThisMyStoreButtonFromStoreDetails(boolean closeStoreChangedModal) {
        LOGGER.info("clickMakeThisMyStoreButtonFromStoreDetails started");
        try {
            driver.jsScrollToElementClick(makeThisMyStore, false);
        } catch (Exception e) {
            homePage.goToHome();
        }

        //Below code is to handle the default 'Store Changed' modal after click action on 'Make This My Store' button.
        if (closeStoreChangedModal && driver.isElementDisplayed(CommonActions.changeStoreModal, Constants.ONE)) {
            commonActions.closeModalWindow();
        }
        LOGGER.info("clickMakeThisMyStoreButtonFromStoreDetails completed");
    }

    /**
     * Clicks Make This My Store button in row where store info box contains storeName
     *
     * @param storeName The store name of the row to click Make This My Store
     */
    public void clickMakeThisMyStoreButtonFromStoreFinder(String storeName) {
        LOGGER.info("clickMakeThisMyStoreButtonFromStoreFinder started for store: '" + storeName + "'");

        // return if the target store is already selected
        String myStore = webDriver.findElement(selectedStoreBy).getText();
        boolean found = false;
        if (CommonUtils.containsIgnoreCase(myStore, storeName)) {
            found = true;
        }
        if (found) {
            return;
        }

        do {
            driver.waitForMilliseconds();
            //clicking indexed Make This My Store button if storeName is contained in storeInfoBox
            List<WebElement> storeBoxes = webDriver.findElements(storeInfoBox);
            List<WebElement> makeThisMyStoreButtons =
                    driver.getElementsWithText(CommonActions.dtButtonLgBy, ConstantsDtc.MAKE_THIS_MY_STORE);

            int i = 0;

            //TODO: retest when new safaridriver is stable
            //safaridriver fails in the loop if size is 1, so this conditional was added.
            if (makeThisMyStoreButtons.size() == 1) {
                driver.jsScrollToElementClick(makeThisMyStoreButtons.get(0));
            } else {
                for (WebElement storeBox : storeBoxes) {
                    if (storeBox.getText().contains(storeName)) {
                        if (Config.isMobile()) {
                            driver.jsScrollToElementClick(makeThisMyStoreButtons.get(i));
                        } else {
                            //Handing intermittent 'cannot determine loading status' failures
                            try {
                                makeThisMyStoreButtons.get(i).click();
                            } catch (Exception e) {
                                makeThisMyStoreButtons = webDriver.findElements(makeThisMyStoreBy);
                                makeThisMyStoreButtons.get(i).click();
                            }
                        }
                        found = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
        } while (!found && commonActions.navToDifferentPageOfResults(Constants.NEXT));
        driver.waitForPageToLoad();
        LOGGER.info("clickMakeThisMyStoreButtonFromStoreFinder completed for store: '" + storeName + "'");
    }

    /**
     * Uses site region and environment to determine default store city and asserts against store displayed
     *
     * @param siteRegion Store region currently AT or DT
     * @param dataSet    Test environment, currently QA or STG
     */
    public void verifyDefaultStoreCity(String siteRegion, String dataSet) {
        LOGGER.info("verifyDefaultStoreCity started with siteRegion: " + siteRegion + " and dataSet: " + dataSet);
        String city = "";

        if (siteRegion.equalsIgnoreCase(ConstantsDtc.AT) && dataSet.equalsIgnoreCase(Constants.QA)) {
            city = ConstantsDtc.AT_QA_DEFAULT_STORE_CITY;
        } else if (siteRegion.equalsIgnoreCase(ConstantsDtc.AT) && dataSet.equalsIgnoreCase(Constants.STG)) {
            city = ConstantsDtc.AT_STG_DEFAULT_STORE_CITY;
        } else if (siteRegion.equalsIgnoreCase(ConstantsDtc.DT) && dataSet.equalsIgnoreCase(Constants.QA)) {
            city = ConstantsDtc.DT_QA_DEFAULT_STORE_CITY;
        } else if (siteRegion.equalsIgnoreCase(ConstantsDtc.DT) && dataSet.equalsIgnoreCase(Constants.STG)) {
            city = ConstantsDtc.DT_STG_DEFAULT_STORE_CITY;
        }
        Assert.assertTrue("FAIL: The homepage \"My Store\" address: " + HomePage.myStoreAddress.getText()
                        + " did NOT contain the expected city of: \"" + city + "\"!",
                HomePage.myStoreAddress.getText().toLowerCase().contains(city));
        LOGGER.info("verifyDefaultStoreCity completed successfully with siteRegion: " + siteRegion
                + " and dataSet: " + dataSet);
    }

    //TODO: This method is NOT working

    /**
     * Confirm "current store" text marked / present on store-locator page.
     */
    public void confirmCurrentStoreTextPresent() {
        if (driver.waitForTextPresent(currentSelectedStoreInStoreLocatorResults,
                CURRENT_STORE_LABEL, Constants.THIRTY))
            LOGGER.info("Confirmed 'Current Store' label is present.");
        else
            LOGGER.info("Confirmed 'Current Store' label is not present.");
    }

    /**
     * Navigate to default stored details page through URL.
     */
    public void navigateToDefaultStoreThroughUrl() {
        LOGGER.info("navigateToDefaultStoreThroughUrl Started");
        driver.getUrl(Config.getBaseUrl().concat(Config.getDefaultStoreCodeURL()));
        LOGGER.info("navigateToDefaultStoreThroughUrl Completed");
    }

    /**
     * Navigate to the store used for system integration testing
     */
    public void navigateToIntegratedStoreThroughUrl() {
        LOGGER.info("navigateToIntegratedStoreThroughUrl Started");
        driver.getUrl(Config.getBaseUrl().concat(Config.getIntegratedStoreCodeURL()));
        LOGGER.info("navigateToIntegratedStoreThroughUrl Completed");
    }

    /**
     * Navigate to store details page through specified URL path.
     *
     * @param path Path to store
     */
    public void navigateToMyStoreThroughUrlPath(String path) {
        LOGGER.info("navigateToMyStoreThroughUrlPath Started");
        String currentUrl = webDriver.getCurrentUrl();
        if (currentUrl.contains(path)){
            return;
        }
        do {
            driver.getUrl(Config.getBaseUrl().concat(path));
            driver.waitForPageToLoad();
        } while (!CommonUtils.containsIgnoreCase(webDriver.getCurrentUrl(), path));
        String storeCode = webDriver.getCurrentUrl().split("/s/")[1].trim().substring(0, 4);
        driver.scenarioData.genericData.put(Constants.STORE_CODE, storeCode);
        driver.scenarioData.genericData.put(Constants.STORE_DETAILS,
                StoreDetailsPage.storeAddress.getText().trim());
        LOGGER.info("navigateToMyStoreThroughUrlPath Completed");
    }

    /**
     * Navigate to default stored details page through URL.
     */
    public void navigateToScottsdaleStoreThroughUrl() {
        LOGGER.info("navigateToScottsdaleStoreThroughUrl Started");
        driver.getUrl(Config.getBaseUrl().concat(ConstantsDtc.SCOTTSDALE_STORE_CODE_PATH));
        LOGGER.info("navigateToScottsdaleStoreThroughUrl Completed");
    }

    /**
     * Executes an action for a store location result, specified by its number in the result list. If there is no match
     * on action, the default is to Schedule appointment.
     *
     * @param action            The action to take (Schedule appointment, Make this my store, Send to phone)
     * @param storeResultNumber number representing position of the store location result item in the result list
     */
    public void selectActionForStoreLocationResultItem(String action, String storeResultNumber) {
        //TODO - Refactor this method as the index needed for many of the actions differ
        LOGGER.info("selectionActionForResultItem started action: " + action + " for store #" + storeResultNumber);
        List<WebElement> elements = null;
        driver.waitForMilliseconds();
        if (action.equalsIgnoreCase(ConstantsDtc.APPOINTMENT)) {
            elements = driver.getElementsWithText(StoreDetailsPage.actionsBy, ConstantsDtc.APPOINTMENT);
        } else if (action.equalsIgnoreCase(ConstantsDtc.MAKE_THIS_MY_STORE)) {
            elements = driver.getElementsWithText(commonActions.dtButtonLgBy, ConstantsDtc.MAKE_THIS_MY_STORE);
        } else if (action.equalsIgnoreCase(ConstantsDtc.READ_REVIEWS)) {
            elements = driver.getElementsWithText(commonActions.dtLinkBy, ConstantsDtc.READ_REVIEWS);
        } else if (action.equalsIgnoreCase(ConstantsDtc.DIRECTIONS)) {
            elements = driver.getElementsWithText(StoreDetailsPage.actionsBy, ConstantsDtc.DIRECTIONS);
        } else if (action.equalsIgnoreCase(ConstantsDtc.SHARE)) {
            elements = webDriver.findElements(StoreDetailsPage.shareStoreBy);
        }

        if (!(elements.size() == 0)) {
            if (action.equalsIgnoreCase(ConstantsDtc.MAKE_THIS_MY_STORE)) {
                if (driver.isElementDisplayed(currentStoreActive)) {
                    try {
                        driver.jsScrollToElement(elements.get(Integer.parseInt(storeResultNumber)));
                        elements.get(Integer.parseInt(storeResultNumber) - 1).click();
                    } catch (Exception e) {
                        driver.jsScrollToElement(elements.get((0)));
                        elements.get(0).click();
                    }
                }
            } else {
                driver.jsScrollToElementClick(elements.get(Integer.parseInt(storeResultNumber) - 1));
            }
        } else {
            Assert.fail("FAIL: No store location results were found!");
        }
        if (driver.isElementDisplayed(storeChangeModalWarning)) {
            commonActions.clickButtonByText(ConstantsDtc.USE_THIS_STORE);
        }
        LOGGER.info("selectActionForStoreLocationResultItem completed action: " + action + " for store #" + storeResultNumber);
    }

    /**
     * Verifies the Current Store text color is blue
     */
    public void assertCurrentStoreTextColor() {
        LOGGER.info("assertCurrentStoreTextColor started");
        String color = currentStoreActive.getCssValue(Constants.COLOR);
        Assert.assertTrue("FAIL: Expected text property to be CURRENT STORE! Actual: " +
                currentStoreActive.getText(), currentStoreActive.getText().equalsIgnoreCase(CURRENT_STORE_LABEL));
        Assert.assertTrue("FAIL: Expected Current Store text color: " + CURRENT_STORE_BLUE +
                        " but the actual color was: " + color + "!",
                color.equalsIgnoreCase(CURRENT_STORE_BLUE));
        LOGGER.info("assertCurrentStoreTextColor completed");
    }

    /**
     * Takes input of a store name and adds region data to a string
     *
     * @return boolean Whether My Store is already selected
     */
    public boolean isMyStoreSelected() {
        LOGGER.info("isMyStoreSelected started");
        boolean myStoreSelected = false;
        if (driver.isElementDisplayed(makeThisMyStoreBy, Constants.ONE))
            myStoreSelected = true;
        LOGGER.info("isMyStoreSelected completed");
        return myStoreSelected;
    }

    /**
     * Assert Schedule Appointment Option is not available on on store locator page DTD site
     */
    public void assertScheduleAppointmentOptionNotAvailableOnDTDStoreLocatorPage() {
        LOGGER.info("assertScheduleAppointmentOptionNotAvailableOnDTDStoreLocatorPage started");
        driver.waitForElementVisible(storeLocatorSearchButton);
        if (driver.isTextPresentInPageSource(ConstantsDtc.SCHEDULE_APPOINTMENT)
                || driver.isElementDisplayed(scheduleApptResultButtons)) {
            Assert.fail("Schedule Appointment option is available to user on store locator page DTD site ");
        } else {
            LOGGER.info("Confirmed: Schedule Appointment option was not displayed on store locator page");
        }
        LOGGER.info("assertScheduleAppointmentOptionNotAvailableOnDTDStoreLocatorPage completed");
    }

    /***
     * Verifies the listed "HOURS OF OPERATION" for each store location result item matches the expected Discount Tire
     * standard
     */
    public void verifyHoursOfOperationForStoreSearchResults() {
        LOGGER.info("verifyHoursOfOperationForStoreSearchResults started");
        driver.waitForMilliseconds();
        driver.waitForElementVisible(StoreDetailsPage.actionsBy);
        List<WebElement> resultsHoursOpList = webDriver.findElements(hoursOpTextBy);
        for (WebElement result : resultsHoursOpList) {
            for (String hoursOpCombination : ConstantsDtc.STORE_DETAILS_STORE_HOURS) {
                Assert.assertTrue("FAIL: The current result item's day and hours combo: \""
                                + result.getText() + "\" did NOT contain \"" + hoursOpCombination + "\"!",
                        result.getText().contains(hoursOpCombination));
            }
        }
        LOGGER.info("verifyHoursOfOperationForStoreSearchResults completed");
    }

    /***
     * Clicks the store link in store search results that contains passed text
     * @param storeText The text of the store link (e.g. AZF 01 or CAN 19) to click in order to load the store details
     */
    public void selectStoreForStoreDetails(String storeText) {
        LOGGER.info("selectStoreForStoreDetails started");
        driver.waitForMilliseconds();
        driver.waitForElementVisible(StoreDetailsPage.actionsBy);
        driver.jsScrollToElementClick(driver.getElementWithText(storeInfoBox, storeText).findElement(storeBy));
        LOGGER.info("selectStoreForStoreDetails completed");
    }

    /**
     * Verifies the Store Locator City/State List is displayed
     *
     * @param text City/State
     */
    public void assertStoreLocatorCityStateListIsDisplayed(String text) {
        LOGGER.info("assertStoreLocatorCityStateListIsDisplayed started  for " + text);
        Assert.assertTrue("FAIL: DiscountTire stores located By " + text + " is not displayed! ",
                driver.getElementWithText(storeLocatorPageBy,
                        ConstantsDtc.STORES_LOCATED_BY).getText().contains(text)
                        && driver.isElementDisplayed(storeLocationListItemBy));
        LOGGER.info("assertStoreLocatorCityStateListIsDisplayed completed  for " + text);
    }

    /***
     * Verifies SHOP DISCOUNT TIRE DIRECT displays
     *
     * @param searchedResult No search result/valid searched results
     */
    public void assertShopDtdMessageDisplayed(String searchedResult) {
        LOGGER.info("assertShopDtdMessageDisplayed started");
        driver.waitForMilliseconds();
        String dtdMessage;
        String searchResultMessage;
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            dtdMessage = ConstantsDtc.DTD_SHOP_DTD_MESSAGE;
            Assert.assertTrue("FAIL: 'SHOP TIRES' and 'SHOP WHEELS' not displayed! ",
                    driver.isElementDisplayed(driver.getElementWithText(CommonActions.dtButtonLgBy,
                            ConstantsDtc.SHOP_TIRES)) && driver.isElementDisplayed(driver.getElementWithText(
                            CommonActions.dtButtonLgBy, ConstantsDtc.SHOP_WHEELS)));
        } else {
            dtdMessage = ConstantsDtc.DT_AT_SHOP_DTD_MESSAGE;
            Assert.assertTrue("FAIL: 'SHOP DISCOUNT TIRE DIRECT' not displayed! ",
                    driver.isElementDisplayed(driver.getElementWithText(CommonActions.dtButtonLgBy,
                            ConstantsDtc.SHOP_DISCOUNT_TIRE_DIRECT)));
        }
        WebElement messageElement = shopDtdContent;
        if (searchedResult.toLowerCase().contains(Constants.NO.toLowerCase())) {
            searchResultMessage = ConstantsDtc.NO_SEARCH_RESULT_MESSAGE;
        } else if (searchedResult.toLowerCase().contains(Constants.EXPAND)) {
            messageElement = increaseRadisMessage;
            searchResultMessage = ConstantsDtc.EXPAND_SEARCH_RESULT_MESSAGE;
        } else {
            searchResultMessage = ConstantsDtc.VALID_SEARCH_RESULT_MESSAGE;
        }
        Assert.assertTrue("FAIL: Expected shop DTD message not displayed! ",
                shopDtdContent.getText().contains(dtdMessage) &&
                        messageElement.getText().contains(searchResultMessage));
        LOGGER.info("assertShopDtdMessageDisplayed completed");
    }

    /**
     * Verifies Discount Tire stores (maximum of 3 stores) displayed for 100+ Miles Display Logic
     */
    public void assertStoreForHundredPlusMilesDisplayLogic() {
        LOGGER.info("assertStoreForHundredPlusMilesDisplayLogic started");
        driver.waitForMilliseconds(Constants.ONE_THOUSAND);
        Assert.assertTrue("FAIL: Discount Tire Stores for 100 + Miles Display Logic not displayed!",
                webDriver.findElements(StoreLocatorPage.storeRowBy).size() > 0 &&
                        webDriver.findElements(StoreLocatorPage.storeRowBy).size() <= 3);
        LOGGER.info("assertStoreForHundredPlusMilesDisplayLogic completed");
    }

    /***
     * Select SHOP DISCOUNT TIRE DIRECT
     */
    public void selectShopDtd() {
        LOGGER.info("selectShopDtd started");
        driver.waitForMilliseconds();
        driver.jsScrollToElementClick(
                driver.getElementWithText(CommonActions.dtButtonLgBy, ConstantsDtc.SHOP_DISCOUNT_TIRE_DIRECT));
        LOGGER.info("selectShopDtd completed");
    }
}