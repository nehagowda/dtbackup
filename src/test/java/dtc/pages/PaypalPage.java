package dtc.pages;

import common.Config;
import common.Constants;
import dtc.data.Customer;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.logging.Logger;

/**
 * Created by aaronbriel on 9/27/16.
 */
public class PaypalPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(PaypalPage.class.getName());

    public PaypalPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
        CommonActions commonActions = new CommonActions(driver);
    }

    @FindBy(css = "[class*='SingleShippingAddress_addresses']")
    private static WebElement addressDisplay;

    @FindBy(tagName = "iframe")
    public static WebElement frame;

    @FindBy(id = "btnNext")
    public static WebElement nextButton;

    @FindBy(id = "btnLogin")
    public static WebElement logInButton;

    @FindBy(id = "password")
    public static WebElement passwordTextField;

    @FindBy(id = "payment-submit-btn")
    public static WebElement continueButton;

    @FindBy(id = "continue_abovefold")
    public static WebElement continueButtonUpper;

    @FindBy(css = "[id*='zoid-paypal-checkout']")
    public static WebElement paypalSanboxIframe;

    @FindBy(className = "nemo_welcomeMessageHeader")
    public static WebElement welcomeMessageHeader;

    private static final By address = By.className("full-address");

    /**
     * Logs into paypal account using given credentials
     *
     * @param username Username for login
     * @param password Password for login
     */
    public void login(String username, String password) {
        LOGGER.info("login started with user '" + username + "', password  '" + password + "'");
        driver.waitForMilliseconds();

            //TODO: retest when new safaridriver is stable
        if (Config.isSafari() || Config.isIphone() || Config.isIpad() || Config.isFirefox() || Config.isIe())
            driver.waitForMilliseconds(Constants.FIFTEEN_THOUSAND);

        driver.pollUntil(Constants.SPINNER_PRELOADER_DISPLAY_JS, Constants.THIRTY);
        if (driver.isElementDisplayed(welcomeMessageHeader, Constants.TWO)) {
            return;
        }
        driver.waitForElementClickable(CommonActions.emailTextField);
        CommonActions.emailTextField.click();
        CommonActions.emailTextField.clear();
        CommonActions.emailTextField.sendKeys(username);
        if (!driver.isElementDisplayed(passwordTextField, Constants.ZERO)) {
            nextButton.click();
            driver.pollUntil(Constants.SPINNER_PRELOADER_DISPLAY_JS, Constants.THIRTY);
        }
        driver.waitForElementClickable(passwordTextField);
        passwordTextField.click();
        passwordTextField.clear();
        passwordTextField.sendKeys(password);

        try {
            logInButton.click();
        } catch (Exception e) {
            driver.jsScrollToElementClick(logInButton);
        }

        //TODO: Retest parentFrame call when new safaridriver is stable
        //webDriver.switchTo().parentFrame();
        webDriver.switchTo().defaultContent();
        Assert.assertTrue("Login failed for user: '" + username + "', password: '" + password + "'",
                driver.isElementDisplayed(addressDisplay, Constants.SIXTY));
        LOGGER.info("login completed with user '" + username + "', password  '" + password + "'");
        if (username.contains(Constants.STATE_OH)) {
            driver.scenarioData.genericData.put(Constants.STORE_CODE, Constants.GA_DC);
        }
    }

    /**
     * Clicks the continue button
     */
    public void clickContinue() {
        LOGGER.info("clickContinue started");
        driver.jsScrollToElementClick(continueButton);
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        LOGGER.info("clickContinue completed");
    }
}