package dtc.pages;

/**
 * Created by aarora on 10/10/16.
 */

import common.Config;
import common.Constants;
import cucumber.api.DataTable;
import dtc.data.ConstantsDtc;
import dtc.data.Product;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import utilities.Driver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class ProductDetailPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(ProductDetailPage.class.getName());
    private Product product;

    public ProductDetailPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
        product = new Product();
    }

    public static final String SIZE = "SIZE";
    private static final String TREAD_AND_TRACTION = "TREAD & TRACTION";
    private static final String SAFETY_AND_PERFORMANCE = "SAFETY & PERFORMANCE";
    private static final String STYLE_AND_CONSTRUCTION = "STYLE & CONSTRUCTION";
    private static final String SIDEWALL = "Sidewall";
    private static final String COLOR_OR_FINISH = "Color / Finish";
    private static final String WHEEL_SIZE = "Wheel Size";
    private static final String FIRST = "First";
    private static final String SECOND = "Second";
    private static final String THIRD = "Third";
    private static final String FOURTH = "Fourth";
    private static final String FIFTH = "Fifth";
    private static final String LEARN_MORE_ABOUT_LOAD_RANGE = "Learn more about Load Range";
    private static final String LEARN_MORE_ABOUT_LOAD_INDEX = "Learn more about Load Index";
    private static final String LEARN_MORE_ABOUT_SPEED_RATING = "Learn more about Speed Rating";
    private static final String WITH_RANGE = "With range";
    private static final String WITH_OUT_RANGE = "Without range";
    private static final String TIRE_SIDEWALL_IMAGE = "help-links__help-image";
    public static final String BUY_TIRES = "buy-tires";
    public static final String BUY_WHEELS = "buy-wheels";
    private static final String REVIEW_SUMMARY = "review summary";

    private static List<String> tireSizeSectionAttributes = new ArrayList<>(Arrays.asList("Wheel Diameter",
            "Aspect Ratio", "Section Width (Cross Section)", "Overall Diameter", "Rim Width Range", "Rim Width",
            "Weight"));

    private static List<String> tireTreadSectionAttributes = new ArrayList<>(Arrays.asList("Tread Depth",
            "Tread Grade", "Traction Grade", "Tread Width"));

    private static List<String> tireSafetyPerformanceSectionAttributes = new ArrayList<>(Arrays.asList("Load Index",
            "Max PSI", "Speed Rating (up to X mph)", "Temperature Grade", "Vendor Product Number / MPN", "Warranty"));

    private static List<String> wheelSizeSectionAttributes = new ArrayList<>(Arrays.asList("Wheel Size", "Rim Diameter",
            "Wheel Width", "Bolt Pattern", "Offset", "Hub Bore Size", "Weight"));

    private static List<String> wheelStyleConstructionSectionAttributes = new ArrayList<>(Arrays.asList("Color",
            "Finish", "Accent", "Construction Material Description", "Wheel Pieces Quantity", "Number of Bolts"));

    private static List<String> wheelSafetyPerformanceSectionAttributes = new ArrayList<>(Arrays.asList(
            "Tire Load Capacity", "Load Rating", "Vendor Product Number / MPN"));

    private static final By productSpecsBy = By.className("pdp-specs__infoname");

    private static final By productDetailSectionHeaderBy = By.className("pdp-specs__list");

    public static final By productDetailSpecificationsRowBy = By.cssSelector(".pdp-specs__list > li");

    private static final By warningMessageBy = By.cssSelector("[class*='product-details__restricted-shipping']");

    public static final By pdpItemNumberBy = By.cssSelector("[class*='product-details__code']");

    public static final By productAvailabilityBy = By.cssSelector("[class*='product-locator__product-locator']");

    public static final By productBrandBy = By.cssSelector("[class*='product-details__brand']");

    public static final By productNameBy = By.cssSelector("[class*='product-details__name']");

    public static final By productLogoBy = By.cssSelector("[class*='brand-logo__logo']");

    public static final By pdpProductReviewBlocksBy = By.cssSelector("[class='detail__reviews__review']");

    public static final By pdpDropDownBy = By.cssSelector("[class*='react-selectize-search-field-and-selected-values']");

    public static final By productAttributesBy = By.cssSelector("form[class*='canonical-pdp-form__canonical-form']");

    public static final By pdpSelectBy = By.cssSelector("[class*='canonical-pdp-form__tire-size-row']");

    public static final By colorTextBy = By.cssSelector("[class*='canonical-pdp-form__filter-criteria-value']");

    public static final By productImageBy = By.cssSelector("div[class*='product-image__image___']>img");

    public static final By productMileWarrantyBy = By.cssSelector("div[class*='product-details__warranty']");

    public static final By toolTipIconContainerBy = By.cssSelector("div[class*='product-features__product-features']");

    public static final By toolTipIconPdpBy = By.cssSelector("div[class*='attribute-icon__tooltip']");

    public static final By productSizeBy = By.cssSelector("[class*='product-details__size']");

    public static final By helpMeChooseLinkHelpTextBy = By.cssSelector("[class*='help-links__help-text']");

    public static final By productDetailSpecificationsRowValueBy = By.className("pdp-specs__size--front");

    public static final By moreDetailsBy = By.cssSelector("[class='detail__reviews__review__toggle']");

    public static final By financingLearnMoreBy = By.cssSelector("[class*='financing-box__rightColumn']");

    public static final By itemSizeOnPdpBy = By.cssSelector("[class*='itemized-product__size']");

    @FindBy(css = "[class*='product-details__size']")
    public static WebElement productSize;

    @FindBy(xpath = "//a[text()='Mail In Rebate']")
    private static WebElement mailInRebate;

    @FindBy(css = "[class*='product-details__name']")
    public static WebElement productNameInfo;

    @FindBy(css = "[class*='product-locator'] [class*='stock']")
    public static WebElement inStockInfo;

    @FindBy(css = "[class*='price__price-block']")
    public static WebElement productPrice;

    @FindBy(className = "auto-pdp-info-map-amount")
    public static WebElement mapProductPrice;

    @FindBy(css = "div[class*='detail__reviews']")
    public static WebElement customerReviewContainer;

    @FindBy(linkText = "what is Load Range?")
    public static WebElement loadRangeLink;

    @FindBy(linkText = "what is Load Index / Speed Rating?")
    public static WebElement loadIndexLink;

    @FindBy(linkText = "what is O.E. Designation?")
    public static WebElement oeDesignationLink;

    @FindBy(css = "div[class*='product-detail-page__container']")
    public static WebElement productDetailPageContainer;

    @FindBy(xpath = "//strong[contains(.,'OE DESIGNATION')]")
    public static WebElement oeDesignationPopupText;

    @FindBy(css = "[class*='help-links__help-link']")
    private static WebElement helpMeChooseBy;

    @FindBy(css = "[class*='review-summary__container']")
    private static WebElement reviewSummary;

    @FindBy(xpath = "//input[@name = 'tireOEDesignation']/following-sibling::div/div[@class='react-selectize-search-field-and-selected-values']")
    private static WebElement oeDesignation;

    @FindBy(css = "[class*='back-to-results']")
    public static WebElement backToResults;

    /**
     * Asserts the product name on screen is the same as the product name passed in
     *
     * @param productName Product name that should appear on page
     */
    public void assertProductNameOnProductDetailPage(String productName) {
        LOGGER.info("assertProductNameOnProductDetailPage started with '" + productName + "'");
        driver.verifyTextDisplayedCaseInsensitive(productNameInfo, productName);
        LOGGER.info("assertProductNameOnProductDetailPage completed. '" + productName + "' " +
                "was listed in the Product Detail Page.");
    }

    /**
     * Verifies the itemID parameter equals the page item id
     *
     * @param itemId Validate actually item Id against this
     */
    public void assertItemIdOnProductDetailPage(String itemId) {
        LOGGER.info("assertItemIdOnProductDetailPage started");
        List<WebElement> products = webDriver.findElements(pdpItemNumberBy);

        for (WebElement product : products) {
            if (product.getText().contains(itemId)) {
                Assert.assertTrue("FAIL: The expected itemID: '" + itemId + "' does NOT contain in actual" +
                        " displayed itemID: '" + product.getText() + "'!", product.getText().contains(itemId));
                LOGGER.info("Confirmed that \"" + itemId + "\" was listed in the Product Detail Page.");
                break;
            }
        }
        LOGGER.info("assertItemIdOnProductDetailPage completed");
    }

    /**
     * Logs message based on if product is in stock or not
     *
     * @return Boolean
     */
    private boolean isItemInStockOnProductDetailPage() {
        LOGGER.info("isItemInStockOnProductDetailPage started");
        boolean isInStock = inStockInfo.getText().equals(ConstantsDtc.IN_STOCK);
        if (isInStock)
            LOGGER.info("Confirmed that selected Item was listed in stock on the Product Detail Page." + isInStock);
        else
            LOGGER.info("Confirmed that selected Item is currently not in stock on the Product Detail Page.");
        LOGGER.info("isItemInStockOnProductDetailPage completed");
        return isInStock;
    }

    /**
     * Asserts that the current product quantity matches desired/default
     * quantity in Product Page
     *
     * @param value The value to verify
     */
    public void assertDefaultQuantityValueOnPdp(String value) {
        LOGGER.info("assertDefaultQuantityValueOnPdp started for value: " + value);
        driver.waitForElementVisible(CommonActions.productQuantityBox);
        String actualValue = CommonActions.productQuantityBox.getAttribute(Constants.VALUE);
        Assert.assertTrue("FAIL: The expected default quantity value: " + value +
                " doesn't match the displayed quantity: " + actualValue + "!", value.equalsIgnoreCase(actualValue));
        LOGGER.info("Confirmed that default product quantity'" + value + "' matches with rendered quantity ==> "
                + actualValue);
        LOGGER.info("assertDefaultQuantityValueOnPdp completed for value: " + value);
    }

    /**
     * Extracts the Product Specification
     *
     * @param specLabel - Specification label's value to extract
     * @return String
     */
    public String getPrdSpecificationValueText(String specLabel) {
        LOGGER.info("getPrdSpecificationValueText started");
        boolean prodSpecFound = false;

        List<WebElement> specs = webDriver.findElements(productSpecsBy);
        List<WebElement> specsValues = webDriver.findElements(productDetailSpecificationsRowValueBy);
        int i = 0;
        for (WebElement spec : specs) {
            if (spec.getText().contains(specLabel)) {
                prodSpecFound = true;
                break;
            } else {
                i++;
            }
        }
        if (!prodSpecFound) {
            Assert.fail("FAIL: Product Specification \"" + specLabel + "\" NOT found on PDP!");
        }
        LOGGER.info("getPrdSpecificationValueText completed");
        return specsValues.get(i).getText();
    }

    /**
     * Assert provided "Product's Specific Specification Value" matches with the actual
     * value on product detail page
     *
     * @param specLabel The string product specification label
     * @param specValue The specValue to verify
     */
    public void assertProductSpecValue(String specLabel, String specValue) {
        LOGGER.info("assertProductSpecValue started");
        String getSpecValue = getPrdSpecificationValueText(specLabel);
        Assert.assertEquals(
                "FAIL: Spec didn't match: (" + specLabel + " :-> " + getSpecValue + " but expected:->  "
                        + specValue + ")!", specValue, getSpecValue);
        LOGGER.info("Spec matched: (" + specLabel + " :-> " + getSpecValue + " &  expected:->  " + specValue
                + ")");
        LOGGER.info("assertProductSpecValue completed");
    }

    /**
     * Checks if "Certificate for Repair Refund Replacement" text is displayed
     *
     * @param text The string text to check
     */
    public void assertProductSpecLabelVisible(String text) {
        LOGGER.info("assertProductSpecLabelVisible started");
        commonActions.assertElementWithTextIsVisible(productSpecsBy, text);
        LOGGER.info("assertProductSpecLabelVisible completed");
    }

    /**
     * Asserts "selected product's inventory stock details"
     */
    public void assertProductInventoryMessage() {
        LOGGER.info("assertProductInventoryMessage started");
        boolean inStockStatus = this.isItemInStockOnProductDetailPage();
        if (inStockStatus) {
            Assert.assertTrue("In Stock related inventory message not present",
                    inStockInfo.getText().contains(ConstantsDtc.IN_STOCK));
        } else {
            Assert.assertTrue(
                    "On Order inventory stock message (1) - " + ConstantsDtc.ZERO_STOCK + " was not present",
                    inStockInfo.getText().contains(ConstantsDtc.ZERO_STOCK));

            String inventoryMessage = webDriver.findElement(CommonActions.inventoryMessageBy).getText();
            Assert.assertTrue("On Order inventory stock message was not present",
                    commonActions.onOrderMessageFound(inventoryMessage));
        }
        LOGGER.info("assertProductInventoryMessage completed");
    }

    /**
     * Verifies the produce price for the first product listed
     *
     * @param price The price to verify
     */
    public void assertProductPrice(String price) {
        LOGGER.info("assertProductContainsPrice started");
        driver.waitForElementVisible(productPrice);
        Assert.assertTrue("FAIL: The expected price: \"" + price
                        + "\" does NOT match the actual displayed price: \"" + productPrice.getText() + "\"!",
                productPrice.getText().contains(price));
        LOGGER.info("Confirmed that \"" + price + "\" was listed for the product.");
        LOGGER.info("assertProductContainsPrice completed");
    }

    /**
     * Verifies the expected attributes for a given section of the product details page are displayed
     *
     * @param productType          The type of product e.g. Tire or Wheel
     * @param productDetailSection The section of the product details page containing the attributes to be validated
     */
    public void verifyExpectedTypeAttributesForProductDetailSection(String productType, String productDetailSection) {
        LOGGER.info("verifyExpectedTypeAttributesForProductDetailSection started for product type: '"
                + productType + "' product detail page section: '" + productDetailSection + "'");

        driver.waitForElementVisible(CommonActions.productQuantityBox);
        List<String> expectedAttributesList = null;

        switch (productDetailSection) {
            case SIZE:
                if (productType.equalsIgnoreCase(Constants.TIRE)) {
                    expectedAttributesList = tireSizeSectionAttributes;
                } else {
                    expectedAttributesList = wheelSizeSectionAttributes;
                }
                break;
            case TREAD_AND_TRACTION:
                expectedAttributesList = tireTreadSectionAttributes;
                break;
            case SAFETY_AND_PERFORMANCE:
                if (productType.equalsIgnoreCase(Constants.TIRE)) {
                    expectedAttributesList = tireSafetyPerformanceSectionAttributes;
                } else {
                    expectedAttributesList = wheelSafetyPerformanceSectionAttributes;
                }
                break;
            case STYLE_AND_CONSTRUCTION:
                expectedAttributesList = wheelStyleConstructionSectionAttributes;
                break;
            default:
                Assert.fail("FAIL - Did not recognize the product detail section '" + productDetailSection +
                        "'! Please verify section arg matches the displayed case and/or wording");
        }
        commonActions.verifyPageSectionContainsAttributes(productDetailSection, productDetailSectionHeaderBy,
                expectedAttributesList);
        LOGGER.info("verifyExpectedTypeAttributesForProductDetailSection completed for product type: '"
                + productType + "' product detail page section: '" + productDetailSection + "'");
    }

    /**
     * Verifies the '...cannot ship...' warning message text and locations on PDP. IF displayStatus == Display,
     * verifies the warning container is displayed along with the messaging. ELSE verifies the warning container is
     * not displayed for the current product on the PDP
     *
     * @param displayStatus Determines if the Shipping Warning message message should or should not be displayed for
     *                      the current product on the PDP
     */
    public void verifyCannotShipMessageForProductDetailPage(String displayStatus) {
        LOGGER.info("verifyCannotShipMessageForProductDetailPage started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(CommonActions.productQuantityBox);
        String locationValidationMessaging = ConstantsDtc.CANNOT_SHIP_LOCATIONS.replace(" ", "");

        if (displayStatus.equalsIgnoreCase(Constants.DISPLAYED)) {
            if (driver.isElementDisplayed(warningMessageBy)) {
                String warningMessageText = webDriver.findElement(warningMessageBy).getText();
                Assert.assertTrue("FAIL: PDP warning message text did not contain expected warning"
                                + " message! \n\t\tExpected string: '" + ConstantsDtc.CANNOT_SHIP_ITEMS_WARNING
                                + "' \n\t\tActual string: '" + warningMessageText + "'",
                        warningMessageText.contains(ConstantsDtc.CANNOT_SHIP_ITEMS_WARNING));

                if (Config.isSafari()) {
                    locationValidationMessaging = ConstantsDtc.CANNOT_SHIP_LOCATIONS_SAFARI;
                    warningMessageText = warningMessageText.replaceAll("[\\r\\n\\t]+", "");
                }

                warningMessageText = warningMessageText.replace(" ", "");

                Assert.assertTrue("FAIL: PDP warning message text did not contain expected locations!"
                        + " \n\t\tExpected string: '" + locationValidationMessaging + "' \n\t\tActual string: '"
                        + warningMessageText + "'", warningMessageText.contains(locationValidationMessaging));
            } else {
                Assert.fail("FAIL: Current product details page is NOT displaying the "
                        + "'...cannot ship __ items...' message!");
            }
        } else {
            Assert.assertTrue("FAIL: The '...cannot ship...' warning message was displayed when it was "
                    + "expected NOT to be!", !driver.isElementDisplayed(warningMessageBy, Constants.TWO));
        }
        LOGGER.info("verifyCannotShipMessageForProductDetailPage completed");
    }

    /**
     * Verifies PDP page is present
     *
     * @param pdpType Type of PDP page to be verified. Can have values(standard | canonical)
     */
    public void verifyProductDetailsPageTypeDisplayed(String pdpType) {
        LOGGER.info("verifyProductDetailsPageTypeDisplayed for type: " + pdpType + " is started");
        driver.waitForPageToLoad();
        if (productDetailPageContainer.isDisplayed()) {
            if (pdpType.contains(ConstantsDtc.CANONICAL)) {
                Assert.assertTrue("FAIL: PDP page displayed is not canonical PDP",
                        webDriver.findElement(productAttributesBy).isDisplayed() ||
                                !webDriver.findElement(CommonActions.addToCartByTextBy).isEnabled());
            } else {
                Assert.assertTrue("FAIL: PDP page displayed is not standard PDP",
                        webDriver.findElement(CommonActions.addToCartByTextBy).isEnabled()
                                && driver.isElementDisplayed(pdpItemNumberBy));
            }
        } else {
            Assert.fail("FAIL: None of the PDP pages are displayed.");
        }
        LOGGER.info("verifyProductDetailsPageTypeDisplayed for type: " + pdpType + " is completed");
    }

    /**
     * Verify the stock count display on PDP page
     */
    public void verifyStockCountTextPdp() {
        LOGGER.info("verifyStockCountTextPdp started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: The stock count is not displayed on PDP page!",
                driver.isElementDisplayed(inStockInfo));
        String myStoreStockMessage = inStockInfo.getText();
        Assert.assertTrue("FAIL: The PDP inventory message stock count displayed: '" + myStoreStockMessage
                        + "' did NOT contain the" + " expected text '" + ConstantsDtc.IN_STOCK + "'!",
                myStoreStockMessage.contains(ConstantsDtc.IN_STOCK));
        LOGGER.info("verifyStockCountTextPdp completed");
    }

    /**
     * Verify Need it Now? message display for item on PDP
     *
     * @param item Product Code
     */
    public void assertNeedItNowDisplayPdp(String item) {
        LOGGER.info("assertNeedItNowDisplayPdp started for item " + item);
        driver.waitForPageToLoad();
        List<String> inventoryMessages = new ArrayList<>();
        List<WebElement> messages = webDriver.findElements(CommonActions.inventoryMessageBy);
        for (WebElement message : messages) {
            inventoryMessages.add(message.getText());
        }
        if (Arrays.asList(inventoryMessages).toString().contains(ConstantsDtc.ORDER_NOW)) {
            Assert.assertTrue("FAIL: Need it Now? Call store not displayed for item " + item,
                    driver.isElementDisplayed(CommonActions.toolTipNeedItNowBy));
            Assert.assertTrue("FAIL: Need it Now? Call store displayed for item " + item
                            + " with text: '" + webDriver.findElement(CommonActions.toolTipNeedItNowBy).getText().replace("\n", "")
                            + "', does not contain expected text:'" + ConstantsDtc.NEED_IT_NOW,
                    webDriver.findElement(CommonActions.toolTipNeedItNowBy).getText().replace("\n", "").contains(ConstantsDtc.NEED_IT_NOW));
        } else {
            Assert.assertTrue("FAIL: Need it Now? Call store displayed for item " + item,
                    !driver.isElementDisplayed(CommonActions.toolTipNeedItNowBy));
        }
        LOGGER.info("assertNeedItNowDisplayPdp completed for item " + item);
    }

    /**
     * Verify the free shipping label on PDP page
     */
    public void verifyFreeShippinglabelPdp() {
        LOGGER.info("verifyFreeShippinglabelPdp started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: Free Shipping label is not displayed on PDP page!",
                driver.isElementDisplayed(CommonActions.freeShippingBy));
        LOGGER.info("verifyFreeShippinglabelPdp completed");
    }

    /**
     * Verify input text is present on Customer review on PDP page
     *
     * @param text The text that needs to be verified
     */
    public void assertReviewPortionText(String text) {
        LOGGER.info("assertReviewPortionText started for: " + text);
        driver.waitForPageToLoad();
        driver.verifyTextDisplayedCaseInsensitive(customerReviewContainer, text);
        LOGGER.info("assertReviewPortionText completed for: " + text);
    }

    /**
     * Verifies product Brand and Product Name on the PDP with data from productInfoList hashmap
     *
     */
    public void assertProductBrandAndNameOnPdpPage() {
        LOGGER.info("assertProductBrandAndNameOnPdpPage started");
        driver.waitForPageToLoad();
        commonActions.waitForSpinner();
        String expectedBrand = commonActions.productInfoListGetValue(ConstantsDtc.BRAND);
        String expectedProduct = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT);

        List<WebElement> displayedBrand = webDriver.findElement(CommonActions.pdpInfoContainerBy).
                findElements(productBrandBy);
        List<WebElement> displayedProduct = webDriver.findElement(CommonActions.pdpInfoContainerBy).
                findElements(productNameBy);
        Assert.assertEquals("FAIL: Product brand label  did not display or displayed multiple times on PDP page",
                displayedBrand.size(), Constants.ONE);
        Assert.assertEquals("FAIL: Product name label did not display or displayed multiple times on PDP page",
                displayedProduct.size(), Constants.ONE);
        String displayedBrandName = displayedBrand.get(0).getText();
        String displayedProductName = displayedProduct.get(0).getText();
        Assert.assertTrue("FAIL: Displayed product brand is not correct on PDP page. Expected: '" + expectedBrand +
                        "'. Displayed: '" + displayedBrandName + "'",
                displayedBrandName.equalsIgnoreCase(expectedBrand));
        Assert.assertTrue("FAIL: Displayed product name is not correct on PDP page. Expected: '" + expectedProduct +
                        "'. Displayed: '" + displayedProductName + "'",
                displayedProductName.equalsIgnoreCase(expectedProduct));
        LOGGER.info("assertProductBrandAndNameOnPdpPage completed for '" + expectedBrand + " " + expectedProduct + "'");
    }

    /**
     * This method will return the Web element matching the input String.
     *
     * @param elementName - Name of the element
     * @return - WebElement
     */
    public WebElement returnWebElement(String elementName) {
        LOGGER.info("returnWebElement started for element: " + elementName);
        switch (elementName) {
            case ConstantsDtc.ENTER_VEHICLE:
                return CommonActions.enterVehicleLink;
            case ConstantsDtc.CHECK_NEARBY_STORES:
                return webDriver.findElement(CommonActions.checkNearByStoresBy);
            case ConstantsDtc.WHAT_IS_LOAD_RANGE:
                return loadRangeLink;
            case ConstantsDtc.MAIL_IN_REBATE:
                return CommonActions.promotionDiscount;
            case ConstantsDtc.TOOL_TIP_CONTENT:
                return CommonActions.toolTipContent;
            case ConstantsDtc.SUB_TOTAL_TOOL_TIP:
                return webDriver.findElement(CommonActions.subTotalToolTipBy);
            case ConstantsDtc.SUBTOTAL:
                return webDriver.findElement(CommonActions.subTotalWithToolTipBy);
            case ConstantsDtc.ADD_TO_CART:
                return CommonActions.addToCart;
            case ConstantsDtc.LOAD_INDEX_SPEED_RATING:
                return loadIndexLink;
            case ConstantsDtc.WHAT_IS_OE_DESIGNATION:
                return oeDesignationLink;
            case ConstantsDtc.TIRE_SIZE:
            case ConstantsDtc.WHEEL_SIZE:
                return webDriver.findElement(pdpSelectBy);
            case REVIEW_SUMMARY:
                return reviewSummary;
            case ConstantsDtc.OE_DESIGNATION:
                return oeDesignation;
            case SIZE:
                return webDriver.findElement(itemSizeOnPdpBy);
            default:
                Assert.fail("FAIL: Could not find By objects that matched string passed from step");
                return null;
        }
    }

    /**
     * This method will return the By element matching the input String.
     *
     * @param elementName - Name of the element
     * @return - By
     */
    private By returnByElement(String elementName) {
        LOGGER.info("returnByElement started for element: " + elementName);
        switch (elementName) {
            case ProductListPage.PROMOTIONS:
            case ConstantsDtc.MAIL_IN_REBATE:
                return CommonActions.promotionDiscountBy;
            case ConstantsDtc.DEALS_AND_REBATES:
                return CommonActions.headerBy;
            case ConstantsDtc.SUBTOTAL:
                return CommonActions.subTotalWithToolTipBy;
            case ConstantsDtc.TIRE_SIZE:
            case ConstantsDtc.WHEEL_SIZE:
                return pdpSelectBy;
            default:
                Assert.fail("FAIL: Could not find By objects that matched string passed from step");
                return null;
        }
    }

    /**
     * Click method for ProductDetailPage.java class
     *
     * @param elementName - send in elementName and gets the Web Element from returnWebElement method.
     */
    public void clickWebElementOnPdpPage(String elementName) {
        LOGGER.info("clickWebElementOnPdpPage started for element: " + elementName);
        WebElement element = returnWebElement(elementName);
        driver.jsScrollToElementClick(element);
        LOGGER.info("clickWebElementOnPdpPage completed for element: " + elementName);
    }

    /**
     * This method verifies that list of elements specified are displayed
     *
     * @param elementName send in elementName and gets the By from returnByElement method.
     */
    public void verifyIfEachElementInTheListIsDisplayed(String elementName) {
        LOGGER.info("verifyIfEachElementInTheListIsDisplayed started for element: " + elementName);
        driver.waitForPageToLoad();
        By by = returnByElement(elementName);
        List<WebElement> elements = webDriver.findElements(by);
        for (WebElement element : elements) {
            Assert.assertTrue("FAIL: Element: " + elementName + " is not displayed",
                    driver.isElementDisplayed(element));
        }
        LOGGER.info("verifyIfEachElementInTheListIsDisplayed completed for element: " + elementName);
    }

    /**
     * Verify Price and product details of standard PDP page
     */
    public void assertSinglePriceOnPdp() {
        LOGGER.info("assertSinglePriceOnPdp started");
        driver.waitForPageToLoad();
        String price = productPrice.getText();
        int counter = 0;
        while (price.contains("-") && counter < Constants.TEN) {
            driver.waitOneSecond();
            commonActions.clickBrowserBody();
            price = productPrice.getText();
            counter++;
        }
        Assert.assertTrue("FAIL: Standard PDP product '"
                + productNameInfo + "' contains Price-Range: " + price, !price.contains("-"));
        LOGGER.info("assertSinglePriceOnPdp completed");
    }

    /**
     * Verify that the product availability section is displayed on the PDP page
     */
    public void assertProductAvailibilitySectionOnPdp() {
        Assert.assertTrue("FAIL: Product availability details not displayed for: " + productNameInfo,
                driver.isElementDisplayed(productAvailabilityBy));
    }

    /**
     * Verify price details displayed on PDP products
     */
    public void assertPdpPriceRange() {
        LOGGER.info("assertPdpPriceRange started");
        commonActions.waitForSpinner();
        driver.waitForElementVisible(productPrice);
        String productName = productNameInfo.getText();
        if (driver.isElementDisplayed(productPrice)) {
            String displayedPrice = productPrice.getText();
            String priceRange = displayedPrice.replaceAll("\\$", "").replaceAll("\\s+", "");
            if (priceRange.contains("-")) {
                String[] parts = priceRange.split("-", 0);
                String price1 = parts[0], price2 = parts[1];
                double minimumRange = Double.parseDouble(price1);
                double maximumRange = Double.parseDouble(price2);
                Assert.assertTrue("FAIL: Minimum price range is not less than Maximum price range '"
                        + productName + "' on PDP page", minimumRange < maximumRange);
            } else {
                LOGGER.info("Price details for the product '" + productName + "' are: " + displayedPrice);
            }
        } else {
            Assert.fail("FAIL: Price details not available for the product: " + productName);
        }
        LOGGER.info("assertPdpPriceRange completed");
    }

    /**
     * Selects drop down's index on PDP page
     *
     * @param elementName - the dropdown element to be selected
     * @param category    - category to which the dropdown belongs(TIRES/WHEELS)
     */
    private int returnDropDownIndex(String elementName, String category) {
        LOGGER.info("returnDropDownIndex started for category: " + category);
        int i = 0;
        if (category.equalsIgnoreCase(ConstantsDtc.TIRES)) {
            try {
                switch (elementName) {
                    case FIRST:
                    case ConstantsDtc.WIDTH:
                        return i = 0;
                    case SECOND:
                    case ConstantsDtc.RATIO:
                        return i = 1;
                    case THIRD:
                    case ConstantsDtc.DIAMETER:
                        return i = 2;
                    case FOURTH:
                    case ConstantsDtc.OE_DESIGNATION:
                        return i = 3;
                    default:
                        Assert.fail("FAIL: Could not find the option that match the element name: " + elementName +
                                " under " + category + " category");
                }
            } catch (Exception e) {
                LOGGER.info("Exception caught: " + e);
            }
        } else if (category.equalsIgnoreCase(ConstantsDtc.WHEELS)) {
            try {
                switch (elementName) {
                    case FIRST:
                    case ConstantsDtc.DIAMETER:
                        return i = 0;
                    case SECOND:
                    case ConstantsDtc.WIDTH:
                        return i = 1;
                    case THIRD:
                    case ConstantsDtc.BOLT_PATTERN:
                        return i = 2;
                    case FOURTH:
                    case ConstantsDtc.OFFSET:
                        return i = 3;
                    case FIFTH:
                    case ConstantsDtc.OE_DESIGNATION:
                        return i = 4;
                    default:
                        Assert.fail("FAIL: Could not find the option that match the element name: " + elementName +
                                " under " + category + " category");
                }
            } catch (Exception e) {
                LOGGER.info("Exception caught: " + e);
            }
        }
        LOGGER.info("returnDropDownIndex completed for category: " + category);
        return i;
    }

    /**
     * Verifies different attributes present on PDP page
     *
     * @param option - Option (Tires/Wheels) to be verified
     */
    public void checkAttributeOptions(String option) {
        LOGGER.info("checkAttributeOptions started for: " + option);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productAttributesBy);
        if (driver.isElementDisplayed(productAttributesBy)) {
            String formText = webDriver.findElement(productAttributesBy).getText();
            if (option.equals(ConstantsDtc.TIRES)) {
                Assert.assertTrue("Canonical Tire product: " + productNameBy + " does not contain any of the " +
                        "expected data: " + ConstantsDtc.SPEED_RATING + ", " + ConstantsDtc.LOAD_RANGE + ", "
                        + SIDEWALL + " or " + ConstantsDtc.OE_DESIGNATION, formText.contains(ConstantsDtc.SPEED_RATING) ||
                        formText.contains(ConstantsDtc.LOAD_RANGE) || formText.contains(SIDEWALL) ||
                        formText.contains(ConstantsDtc.OE_DESIGNATION));
            } else if (option.equals(ConstantsDtc.WHEELS)) {
                Assert.assertTrue("Canonical Wheel product: " + productNameBy + " does not contain any of the " +
                        "expected data: " + COLOR_OR_FINISH + ", " + WHEEL_SIZE + ", " + ConstantsDtc.BOLT_PATTERN + " or "
                        + ConstantsDtc.OFFSET, formText.contains(COLOR_OR_FINISH) || formText.contains(WHEEL_SIZE) ||
                        formText.contains(ConstantsDtc.BOLT_PATTERN) || formText.contains(ConstantsDtc.OFFSET));
            }
        }
        LOGGER.info("checkAttributeOptions completed for: " + option);
    }

    /**
     * Verify Load Range and Load Index popup descriptions are displayed
     *
     * @param popUpName - Name of the popup to be verified
     */
    public void assertPopUpText(String popUpName) {
        LOGGER.info("assertPopUpText started for popup: " + popUpName);
        driver.waitForElementVisible(CommonActions.dtModalContainerBy, Constants.ZERO);
        driver.setImplicitWait(Constants.FIVE, TimeUnit.SECONDS);
        if (popUpName.equals(ConstantsDtc.LOAD_RANGE)) {
            WebElement loadRangeLinkInPopup = webDriver.findElement(By.linkText(LEARN_MORE_ABOUT_LOAD_RANGE));
            Assert.assertTrue("FAIL: Load Range related info not found in Popup",
                    driver.isElementDisplayed(loadRangeLinkInPopup));
        }
        if (popUpName.equals(ConstantsDtc.LOAD_INDEX)) {
            WebElement loadIndexLinkInPopup = webDriver.findElement(By.linkText(LEARN_MORE_ABOUT_LOAD_INDEX));
            WebElement speedRatingLinkInPopup = webDriver.findElement(By.linkText(LEARN_MORE_ABOUT_SPEED_RATING));
            Assert.assertTrue("FAIL: Load Index/Speed rating related info not found in Popup",
                    driver.isElementDisplayed(loadIndexLinkInPopup)
                            && driver.isElementDisplayed(speedRatingLinkInPopup));
        }
        if (popUpName.equals(ConstantsDtc.OE_DESIGNATION)) {
            Assert.assertTrue("FAIL: OE Designation info not found in Popup",
                    driver.isElementDisplayed(oeDesignationPopupText));
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("assertPopUpText completed for popup: " + popUpName);
    }

    /**
     * Verify Add To Cart is enabled or disabled in pdp
     *
     * @param checkOption The functionality to be verified for the element
     */
    public void assertPdpAddToCartEnabledDisabledDisplayed(String checkOption) {
        LOGGER.info("assertPdpAddToCartEnabledDisabledDisplayed started to check if element is: " + checkOption);
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        if (checkOption.equalsIgnoreCase(Constants.ENABLED)) {
            Assert.assertTrue("FAIL: Add to cart button is Disabled for the product: " + productNameBy,
                    CommonActions.addToCart.isEnabled());
        } else if (checkOption.equalsIgnoreCase(Constants.DISABLED)) {
            Assert.assertTrue("FAIL: Add to cart button is Enabled for the product: " + productNameBy,
                    !CommonActions.addToCart.isEnabled());
        } else if (checkOption.equalsIgnoreCase(Constants.DISPLAYED)) {
            Assert.assertTrue("FAIL: Add to cart button is not displayed for the product: " + productNameBy,
                    driver.isElementDisplayed(driver.getElementWithText(CommonActions.buttonBy, ConstantsDtc.ADD_TO_CART)));
        }
        LOGGER.info("assertPdpAddToCartEnabledDisabledDisplayed completed to check if element is: " + checkOption);
    }

    /**
     * Verify the subtotal on PDP page using retail price and qty on PDP page
     */
    public void assertPdpSubtotalCalculation() {
        LOGGER.info("assertPdpSubtotalCalculation started");
        driver.waitForPageToLoad();
        String price = webDriver.findElement(CommonActions.productPriceBy).getText();
        String productText = webDriver.findElement(CommonActions.pdpInfoContainerBy).getText();
        if (productText.contains(ConstantsDtc.SUBTOTAL)) {
            price = price.replaceAll("\\$", "").replaceAll("\\s+", "");
            double retailPrice = Double.parseDouble(price);
            String quantity = CommonActions.productQuantityBox.getAttribute(Constants.VALUE);
            int qty = Integer.parseInt(quantity);
            double expectedPrice = retailPrice * qty;
            String totalPrice = webDriver.findElement(ProductListPage.subTotalPrice).getText();
            totalPrice = totalPrice.replaceAll("\\$", "").replaceAll("\\s+", "").replaceAll(",", "");
            double actualPrice = Double.parseDouble(totalPrice);
            Assert.assertTrue("FAIL: Subtotal of product: " + productNameBy + " is incorrect, Actual price: "
                    + totalPrice + "Expected price: " + expectedPrice, expectedPrice == actualPrice);
        } else {
            Assert.fail("FAIL: SubTotal does not exist for the Product: " + productNameBy);
        }
        LOGGER.info("assertPdpSubtotalCalculation completed");
    }

    /**
     * Close the Load Range/Load Index popup
     *
     * @param popUpName Name of the popup to be closed
     */
    public void closeLoadRangeOrLoadIndexPopup(String popUpName) {
        LOGGER.info("closeLoadRangeOrLoadIndexPopup started for: " + popUpName);
        String popUpText;
        switch (popUpName) {
            case ConstantsDtc.LOAD_RANGE:
                popUpText = ConstantsDtc.WHAT_IS_LOAD_RANGE;
                break;
            case ConstantsDtc.LOAD_INDEX:
                popUpText = ConstantsDtc.LOAD_INDEX_SPEED_RATING;
                break;
            case ConstantsDtc.OE_DESIGNATION:
                popUpText = ConstantsDtc.WHAT_IS_OE_DESIGNATION;
                break;
            default:
                Assert.fail("FAIL: Could not find the option that match the element name: " + popUpName);
                popUpText = null;
        }

        WebElement element = driver.getParentElement(driver.getElementWithText(CommonActions.headerSecondBy, popUpText))
                .findElement(CommonActions.dtModalCloseBy);
        element.click();

        LOGGER.info("closeLoadRangeOrLoadIndexPopup completed for: " + popUpName);
    }

    /**
     * Verify Review and Ratings on PDP page
     */
    public void assertPdpReviewRatingDetails() {
        LOGGER.info("assertPdpReviewRatingDetails started");
        driver.waitForPageToLoad();
        String productText = webDriver.findElement(CommonActions.pdpInfoContainerBy).getText();
        String productName = webDriver.findElement(productNameBy).getText();
        if (driver.isElementDisplayed(CommonActions.reviewRatingBy)) {
            driver.waitForElementVisible(customerReviewContainer);
            if (driver.isElementDisplayed(CommonActions.starRatingBy)) {
                LOGGER.info("Star rating available for this product: " + productName);
            }
            Assert.assertTrue("FAIL: Product: " + productName + " contains Review link but customer " +
                    "review details are not displayed", driver.isElementDisplayed(customerReviewContainer));
        }
        if (driver.isElementDisplayed(CommonActions.reviewRatingBy) && !productText.contains(ConstantsDtc.READ_REVIEWS)) {
            Assert.assertTrue("FAIL: Product: " + productName + " doesn't contain 'No reviews yet' message",
                    productText.contains(ProductListPage.NO_REVIEWS_YET));
        }
        LOGGER.info("assertPdpReviewRatingDetails completed");
    }

    /**
     * Verify input text is not present on PDP page
     *
     * @param text The text that needs to be verified
     */
    public void assertTextNotPresentOnPdp(String text) {
        LOGGER.info("assertTextNotPresentOnPdp started for text: " + text);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productDetailPageContainer);
        String pageText = productDetailPageContainer.getText();
        Assert.assertTrue("FAIL: PDP Page contains the text: " + text,
                !pageText.toLowerCase().contains(text.toLowerCase()));
        LOGGER.info("assertTextNotPresentOnPdp completed for text: " + text);
    }

    /**
     * verifies the text Attributes of the Web Elements
     *
     * @param elementName - send in elementName and gets the Web Element from returnWebElement method.
     * @param validation  - String to validate against
     */
    public void verifyTextOfElementPdp(String elementName, String validation) {
        LOGGER.info("verifyTextOfElementPdp started for element: " + elementName + " and validation text: "
                + validation);
        driver.verifyTextDisplayedCaseInsensitive(returnWebElement(elementName), validation);
        LOGGER.info("verifyTextOfElementPdp completed for element: " + elementName + " and validation text: "
                + validation);
    }

    /**
     * Verify selected color swatch with wheel image displayed on Canonical PDP page
     */
    public void verifyColorSwatchWithWheelImage() {
        LOGGER.info("verifyColorSwatchWithWheelImage started");
        List<WebElement> colorSwatchList = webDriver.findElement(CommonActions.colorSwatchContainerBy).
                findElements(CommonActions.colorSwatchBy);
        for (WebElement colorSwatch : colorSwatchList) {
            driver.moveToElementClick(colorSwatch);
            commonActions.waitForSpinner();
            String text1 = null;
            String text2 = null;
            String selectedColorText = webDriver.findElement(colorTextBy).getText().toLowerCase();
            String imageAttribute = webDriver.findElement(productImageBy).getAttribute(Constants.ATTRIBUTE_SRC);
            if (selectedColorText.contains(" ")) {
                String[] parts = selectedColorText.split(" ", 0);
                text1 = parts[0];
                text2 = parts[1];
            } else {
                text1 = selectedColorText;
            }
            if (imageAttribute.contains(Constants.NO_IMG_EXT_PNG)) {
                Assert.fail("FAIL: Selected color: " + selectedColorText + " doesn't contain product image");
            } else if (text1.equals(selectedColorText)) {
                Assert.assertTrue("FAIL: Selected color: " + selectedColorText + " doesn't match with " +
                        "displayed product image.", imageAttribute.contains(text1));
            } else {
                Assert.assertTrue("FAIL: Selected color: " + selectedColorText + " doesn't match with " +
                        "displayed product image.", imageAttribute.contains(text1) &&
                        imageAttribute.contains(text2));
            }
        }
        LOGGER.info("verifyColorSwatchWithWheelImage completed");
    }

    /**
     * Verify input text is present on breadcrumbs Canonical PDP page
     *
     * @param text The text that needs to be verified
     */
    public void assertBreadCrumbsText(String text) {
        LOGGER.info("assertBreadCrumbsText started verifying text: " + text);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(CommonActions.breadCrumbContainerBy);
        String breadCrumbText = webDriver.findElement(CommonActions.breadCrumbContainerBy).getText().toLowerCase();
        Assert.assertTrue("FAIL: Bread crumb section doesn't contain the expected text: " + text,
                breadCrumbText.contains(text.toLowerCase()));
        LOGGER.info("assertBreadCrumbsText completed verifying text: " + text);
    }

    /**
     * Verify Mileage warranty for the product on PDP
     *
     * @param withoutOrWithRange The type to check with
     */
    public void verifyMileWarrantyRange(String withoutOrWithRange) {
        LOGGER.info("verifyMileWarrantyRange started for: " + withoutOrWithRange);
        driver.waitForPageToLoad();
        String productName = webDriver.findElement(productNameBy).getText();
        if (driver.isElementDisplayed(productMileWarrantyBy)) {
            String mileText = webDriver.findElement(productMileWarrantyBy).getText();
            mileText = mileText.replaceAll(ConstantsDtc.MILE_WARRANTY, "")
                    .replaceAll("\\s+", "").replaceAll(",", "");
            if (withoutOrWithRange.equalsIgnoreCase(WITH_RANGE)) {
                Assert.assertTrue("FAIL: Product: " + productName + " does not contain range in mile warranty: "
                        + mileText, mileText.contains("-"));
                String[] parts = mileText.split("-", 0);
                String mile1 = parts[0], mile2 = parts[1];
                int minimumRange = Integer.parseInt(mile1);
                int maximumRange = Integer.parseInt(mile2);
                Assert.assertTrue("FAIL: Mile warranty range is not in 'min - max' " +
                        "format for this product: " + productName, (minimumRange < maximumRange));
            } else if (withoutOrWithRange.equalsIgnoreCase(WITH_OUT_RANGE)) {
                Assert.assertTrue("FAIL: Product: " + productName + " contains range in its mile warranty: "
                        + mileText, !mileText.contains("-"));
            }
        } else {
            Assert.fail("Mile warranty is not present for the product: " + productName);
        }
        LOGGER.info("verifyMileWarrantyRange completed for: " + withoutOrWithRange);
    }

    /**
     * Verify Iconography and Tooltip text of pdp product
     */
    public void verifyTooltipTextOnPdp() {
        LOGGER.info("verifyTooltipTextOnPdp started");
        driver.waitForPageToLoad();
        String productName = webDriver.findElement(productNameBy).getText();
        if (driver.isElementDisplayed(toolTipIconContainerBy)) {
            List<WebElement> tooltipIcons = webDriver.findElement(toolTipIconContainerBy).findElements(toolTipIconPdpBy);
            for (int i = 0; i < tooltipIcons.size(); i++) {
                WebElement toolTip = tooltipIcons.get(i);
                new Actions(webDriver).moveToElement(toolTip).perform();
                String toolTipText = toolTip.getText();
                Assert.assertTrue("FAIL: Product: " + productName +
                        " doesn't contain tooltip text for the icon", !toolTipText.equals(null));
            }
        } else {
            Assert.fail("Tool tip is not present for the product: " + productName);
        }
        LOGGER.info("verifyTooltipTextOnPdp completed");
    }

    /**
     * Verify the Help me Link color and Help text which gets displayed on PDP
     */
    public void verifyLinkTextColorAndHelpText(String linktext) {
        LOGGER.info("verifyLinkTextColorAndHelpText Started");
        String helpMeChooseLinkColor = helpMeChooseBy.getCssValue(Constants.COLOR);
        Assert.assertTrue("FAIL: The expected color values " + Constants.BLUE_LINKTEXT_RGBA + " doesn't match with "
                + helpMeChooseLinkColor, helpMeChooseLinkColor.equals(Constants.BLUE_LINKTEXT_RGBA));
        driver.waitForElementClickable(helpMeChooseBy);
        helpMeChooseBy.click();
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        WebElement helpText = webDriver.findElement(helpMeChooseLinkHelpTextBy);
        WebElement tireSideWallImage = driver.getElementWithClassContainingText(TIRE_SIDEWALL_IMAGE);
        Assert.assertTrue("FAIL: Tire sidewall image 'alt text' attribute did NOT match expected!" +
                        " \n\tActual: '" + tireSideWallImage.getAttribute(Constants.ATTRIBUTE_ALT) + "' \n\tExpected: '"
                        + FitmentPage.TIRE_SIDEWALL_IMAGE_ALT_TEXT + "'",
                tireSideWallImage.getAttribute(Constants.ATTRIBUTE_ALT).equalsIgnoreCase(FitmentPage.TIRE_SIDEWALL_IMAGE_ALT_TEXT));
        Assert.assertTrue("FAIL: The messaging did not match expected! \n\tActual: '"
                        + helpText.getText() + "' \n\tExpected: '" + FitmentPage.TIRE_SIDEWALL_HELP_MESSAGE + "'",
                helpText.getText().equalsIgnoreCase(FitmentPage.TIRE_SIDEWALL_HELP_MESSAGE));
        LOGGER.info("verifyLinkTextColorAndHelpText completed");
    }

    /**
     * Get the product name of item on Product Detail Page
     *
     * @return product name
     */
    public String getPdpProductName() {
        LOGGER.info("getPdpProductName started");
        String productName = productNameInfo.getText();
        LOGGER.info("getPdpProductName completed returning '" + productName + "'");
        return productName;
    }

    /**
     * Verify text present in all review blocks
     *
     * @param table: All the rows from the feature file which should be validated.
     */
    public void verifyTextPresentInReviewBlocks(DataTable table) {
        LOGGER.info("verifyTextPresentInReviewBlocks started");
        int column = 0;
        List<List<String>> rows = table.raw();
        for (int row = 0; row < rows.size(); row++) {
            String text = rows.get(row).get(column);
            List<WebElement> reviewBlocks = webDriver.findElements(pdpProductReviewBlocksBy);
            for (WebElement reviewBlock : reviewBlocks) {
                if (Config.isMobile()) {
                    driver.jsScrollToElementClick(reviewBlock.findElement(moreDetailsBy));
                }
                driver.verifyTextDisplayedCaseInsensitive(reviewBlock, text);
                if (Config.isMobile()) {
                    driver.jsScrollToElementClick(reviewBlock.findElement(moreDetailsBy));
                }
            }
        }
        LOGGER.info("verifyTextPresentInReviewBlocks completed");
    }

    /**
     * Verify the financing learn more link is displayed
     *
     * @param option: status to validate for element.
     */
    public void verifyFinancingLearnMorePresent(String option) {
        LOGGER.info("verifyFinancingLearnMorePresent started");
        if (option.equals(Constants.DISPLAYED)) {
            driver.waitForElementClickable(financingLearnMoreBy);
        } else {
            driver.waitForElementNotVisible(financingLearnMoreBy);
        }
        LOGGER.info("verifyFinancingLearnMorePresent completed");
    }
}