package dtc.pages;

import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Logger;

public class TireAndWheelPackagesPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(TireAndWheelPackagesPage.class.getName());
    public static HashMap<String, String[]> tireWheelPackageDetails = new HashMap<>();;
    private List<WebElement> elementsList;

    public TireAndWheelPackagesPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(css = "[class*='tire-wheel-package__selection-container']:nth-of-type(1)")
    private static WebElement chooseTires;

    @FindBy(css = "[class*='tire-wheel-package__selection-container']:nth-of-type(2)")
    private static WebElement chooseWheels;

    @FindBy(css = "[class*=filter-facet-display__facet-container]")
    public static WebElement filterCategory;

    @FindBy(xpath = "(//*[contains(@class,'package-navigation__icons')])[1]")
    public static WebElement choseTiresIcon;

    @FindBy(xpath = "(//*[contains(@class,'package-navigation__icons')])[2]")
    public static WebElement choseWheelsIcon;

    @FindBy(xpath = "(//*[contains(@class,'package-navigation__icons')])[3]")
    public static WebElement packageCompleteIcon;

    @FindBy(xpath = "(//*[contains(@class,'tire-wheel-package__treadwell-selection')])[1]")
    public static WebElement treadWellTireGuide;

    @FindBy(xpath = "(//*[contains(@class,'tire-wheel-package__treadwell-selection')])[2]")
    public static WebElement onPromotion;

    @FindBy(xpath = "(//*[contains(@class,'tire-wheel-package__treadwell-selection')])[3]")
    public static WebElement bestSeller;

    @FindBy(xpath = "(//*[contains(@class,'tire-wheel-package__treadwell-selection')])[4]")
    public static WebElement highestRated;

    @FindBy(css = "[class*='tire-wheel-package__tire-category-button']")
    public static WebElement shopByCategoryButton;

    @FindBy(xpath = "//img[contains(@alt,'Tire Wheel Package')]")
    public static WebElement wheelTireImage;
    
    public static final By packageDetailsBy = By.cssSelector("[class*='package-details__product-common-wrap']");

    public static final By tiresSelectionBy = By.cssSelector("[class*='choose-tire__tire-container']");

    public static final By wheelSelectionBy = By.cssSelector("[class*='choose-wheel__tire-container']");

    public static final By divBy = By.xpath("div");

    public static final By packageEntrySelectionBy = By.cssSelector("[class*='package-entry__selection-container']");

    private static final String CHOOSE_TIRES = "CHOOSE TIRES";
    private static final String CHOOSE_WHEELS = "CHOOSE WHEELS";
    private static final String SHOP_ALL_TIRES = "SHOP_ALL_TIRES";
    private static final String PACKAGE_COMPLETE = "PACKAGE COMPLETE";
    private static final String COMPLETE = "complete";
    private static final String AVAILABLE = "Available";
    private static final String TREADWELL_TIRE_GUIDE = "Treadwell tire guide";
    private static final String ON_PROMOTION = "On promotion";
    private static final String HIGHEST_RATED = "Highest rated";
    private static final String SHOP_BY_CATEGORY = "Shop by category";
    private static final String ORDER_NOW = "Order now";
    private static final String IN_STOCK = "In";
    private static final String WHEEL_TIRE_IMAGE = "Wheel Tire Image";

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case CHOOSE_TIRES:
                return chooseTires;
            case CHOOSE_WHEELS:
            case SHOP_ALL_TIRES:
                return chooseWheels;
            case TREADWELL_TIRE_GUIDE:
                return treadWellTireGuide;
            case ON_PROMOTION:
                return onPromotion;
            case ConstantsDtc.BEST_SELLER:
                return bestSeller;
            case HIGHEST_RATED:
                return highestRated;
            case SHOP_BY_CATEGORY:
                return shopByCategoryButton;
            case WHEEL_TIRE_IMAGE:
                return wheelTireImage;
            default:
                Assert.fail("FAIL: Could not find element that matched string passed from step");
                return null;
        }
    }

    /**
     * Click elements in Tire Wheel Package page.
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     */
    public void clickInTireWheelPackagePage(String elementName) {
        LOGGER.info("clickInTireWheelPackagePage started");
        returnElement(elementName).click();
        LOGGER.info("clickInTireWheelPackagePage completed");
    }

    /**
     * Clicks an element with By selector and text
     *
     * @param text Text of element to click on
     */
    public void selectedOrNotSelectedElementWithText(String text, String action) {
        for (WebElement element : elementsList) {
            if (element.getText().toLowerCase().contains(text.toLowerCase())) {
                Assert.assertTrue("Fail: Not selected",
                        element.findElement(divBy).getAttribute(Constants.CLASS).contains(action));
            }
        }
    }


    /**
     * Clicks an element with By selector and text
     *
     * @param by   By selector of element with text
     * @param text Text of element to click on
     */
    public void clickElementWithTextOnWheelTirePackage(By by, String text) {
        LOGGER.info("clickElementWithText started with text '" + text + "'");
        boolean found = false;
        try {
            elementsList = webDriver.findElements(by);
            for (WebElement element : elementsList) {

                if (element.getText().toLowerCase().contains(text.toLowerCase())) {
                    found = true;
                    try {
                        element.findElement(divBy).click();
                    } catch (Exception e) {
                        driver.jsClick(element);
                    }
                    break;
                }
            }
        } catch (NoSuchElementException e) {
            Assert.fail("FAIL: Element \"" + by.toString() + "\" NOT found! (Full Stack Trace: " + e.toString() + ")");
        }

        if (!found)
            Assert.fail("FAIL: Element \"" + by.toString() + "\" with text \"" + text + "\" NOT found!");
        LOGGER.info("clickElementWithText completed with text '" + text + "'");
    }

    /**
     * Verifies the icon if it completed.
     *
     * @param wheelTireNavigationIcon type of icon to verify if it's completed
     */
    public void wheelTireNavigationIcon(String wheelTireNavigationIcon) {
        if (wheelTireNavigationIcon.equalsIgnoreCase(CHOOSE_TIRES)) {
            Assert.assertTrue("FAIL: Not completed",
                    choseTiresIcon.findElement(divBy).getAttribute(Constants.CLASS).contains(COMPLETE));
        } else if (wheelTireNavigationIcon.equalsIgnoreCase(CHOOSE_WHEELS)) {
            Assert.assertTrue("FAIL: Not completed",
                    choseWheelsIcon.findElement(divBy).getAttribute(Constants.CLASS).contains(COMPLETE));
        } else if (wheelTireNavigationIcon.equalsIgnoreCase(PACKAGE_COMPLETE)) {
            Assert.assertTrue("FAIL: Not completed",
                    packageCompleteIcon.findElement(divBy).getAttribute(Constants.CLASS).contains(COMPLETE));
        }
    }

    /**
     * Saves the product info data into Hashmap with user defined key name
     *
     * @param keyName : value to save the details with in Hash Map
     */
    public void saveProductDetailsWithKey(String keyName) {
        LOGGER.info("saveProductDetailsWithKey started for key name" + keyName);
        String lastKey = null;
        Iterator it = driver.scenarioData.productInfoList.keySet().iterator();
        while (it.hasNext()) {
            lastKey = it.next().toString();
        }
        tireWheelPackageDetails.put(keyName, formatData(driver.scenarioData.productInfoList.get(lastKey)));

        LOGGER.info("saveProductDetailsWithKey completed for key name" + keyName);
    }

    /**
     * Method removed unwanted string values from the string collection.
     *
     * @param data : Type String array
     */
    public String[] formatData(String[] data) {
        String values[] = data;
        List<String> list = new ArrayList<>();
        Collections.addAll(list, values);
        list.removeIf(value -> value.isEmpty() | value.equalsIgnoreCase(CommonActions.TRUE)
                | value.startsWith(AVAILABLE) | value.startsWith("$") | value.startsWith(ORDER_NOW)
                | value.contains(IN_STOCK) | value.equalsIgnoreCase(ConstantsDtc.ON_ORDER));
        return list.toArray(new String[0]);
    }

    /**
     * Method verifies data in Tire and Wheel packages pop up
     *
     * @param keyName : value to get the data from Hash Map
     */
    public void verifyDataIntoWheelTirePopUp(String keyName) {
        LOGGER.info("verifyDataIntoWheelTirePopUp started for key name " + keyName);
        String values[] = tireWheelPackageDetails.get(keyName);
        for (String value : values) {
            if (!CommonUtils.containsIgnoreCase(value, ConstantsDtc.IN_STOCK)) {
                Assert.assertNotNull("FAIL: '" + value + "' was not displayed on Tire and Wheel packages popup",
                        driver.getElementWithText(packageDetailsBy, value));
            }
        }
        LOGGER.info("verifyDataIntoWheelTirePopUp completed for key name " + keyName);
    }

    /**
     * Select specified option from Select Your Tire & Wheel Package modal
     *
     * @param option The option to select
     */
    public void selectOptionFromSelectYourTireWheelPackageModal(String option) {
        LOGGER.info("selectOptionFromSelectYourTireWheelPackageModal started for option '" + option + "'");
        WebElement element = driver.getElementWithText(packageEntrySelectionBy, option);
        driver.jsScrollToElementClick(element, false);
        LOGGER.info("selectOptionFromSelectYourTireWheelPackageModal completed for option '" + option + "'");
    }
}
