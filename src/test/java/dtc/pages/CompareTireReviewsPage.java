package dtc.pages;

import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by collinreed on 7/14/17.
 */

public class CompareTireReviewsPage {

    private final Driver driver;
    private final WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(CompareTireReviewsPage.class.getName());
    private final CommonActions commonActions;

    public CompareTireReviewsPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    public static final By compareTireReviewsHeaderBy = By.className("review-comparison__title");

    public static final By resultsRowBy = By.className("comparison-row");

    private static final By addToCartBy = By.cssSelector(".btn-small.price-block__add-to-cart");

    private static final String sortOptionXpathBy = "//span[text()='%s']";

    private static final By comparisonRowSelectedBy = By.cssSelector(".comparison-row--selected td");

    public static final By comparisonQuantityBy = By.className("comparison-quantity");

    @FindBy(className = "sorted-desc")
    public static WebElement sortedDesc;

    /**
     * Verify current page is Compare Tires page
     */
    public void assertCompareTireReviewsPageHeader() {
        LOGGER.info("assertCompareTireReviewsPageHeader started");
        driver.waitForPageToLoad();
        Assert.assertEquals("FAIL: 'Compare Tire Reviews' page is NOT displayed! Header does not match expected!'",
                webDriver.findElement(compareTireReviewsHeaderBy).getText(), ConstantsDtc.COMPARE_TIRE_REVIEWS);
        LOGGER.info("assertCompareTireReviewsPageHeader completed");
    }

    /**
     * Click Add to Cart button for saved product on Compare Tire Reviews page
     */
    public void clickAddToCartForSavedProductOnCompareTireReviews() {
        LOGGER.info("clickAddToCartForSavedProductOnCompareTireReviews started");
        driver.waitForPageToLoad();
        List<WebElement> rows = webDriver.findElements(resultsRowBy);
        String expectedBrand = commonActions.productInfoListGetValue(ConstantsDtc.BRAND);
        String expectedProduct = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT);
        String expectedPrice = commonActions.productInfoListGetValue(Constants.PRICE);

        boolean found = false;
        for (WebElement row : rows) {
            if (row.getText().contains(ConstantsDtc.SELECTED_TIRE)) {
                String displayBrand = row.findElement(CommonActions.tireBrandBy).getText();
                String displayProduct = row.findElement(CommonActions.tireNameBy).getText();
                String displayPrice = row.findElement(CommonActions.tirePriceBy).getText().replace("ea.", "").trim();

                if (displayBrand.equalsIgnoreCase(expectedBrand) && displayProduct.equalsIgnoreCase(expectedProduct)
                        && displayPrice.equals(expectedPrice)) {
                    driver.moveToElementClick(row.findElement(addToCartBy));
                    found = true;
                    break;
                }
            }
        }

        Assert.assertTrue("FAIL: The selected product '" + expectedBrand + " " + expectedProduct + " "
                + expectedPrice + "' was not listed on the Compare Tire Reviews page!", found);
        LOGGER.info("clicked on Add To Cart button for '" + expectedBrand + " " + expectedProduct + " "
                + expectedPrice + "'");
        LOGGER.info("clickAddToCartForSavedProductOnCompareTireReviews completed");
    }

    /**
     * Clicks Add To Cart for first item listed on Compare Tire Reviews page
     */
    public void clickAddToCartForFirstItemOnCompareTireReviews() {
        //TODO: When Compare Tire Reviews is rewritten in React, consider using common AddToCart function
        LOGGER.info("clickAddToCartForFirstItemOnCompareTireReviews started");
        driver.waitForElementClickable(addToCartBy);
        webDriver.findElement(addToCartBy).click();
        LOGGER.info("clickAddToCartForFirstItemOnCompareTireReviews completed");
    }

    /**
     * Extracts Overall Rating
     *
     * @return double The rating of the Selected Tire
     */
    public double extractOverallRatingForSelectedTire() {
        LOGGER.info("extractOverallRatingForSelectedTire started");
        WebElement row = webDriver.findElement(comparisonRowSelectedBy);
        driver.waitForElementVisible(row.findElement(CommonActions.overallRatingBy));
        double itemOverallRating = Double.parseDouble(row.findElement(CommonActions.overallRatingBy).getText());
        LOGGER.info("extractOverallRatingForSelectedTire completed");
        return itemOverallRating;
    }

    /**
     * Verify overall rating is average of Ride Comfort, Cornering / Steering, Ride Noise, Tread life
     * Dry Traction, Wet Traction
     * Winter Traction and Buy Tire Again is not part of the calculation
     */
    public void assertOverallRating() {
        LOGGER.info("assertOverallRating started");
        double reviewListTotalValue = 0.0;
        int reviewCount = 0;

        List<WebElement> elements = webDriver.findElements(comparisonRowSelectedBy);
        for (int i = 3; i < elements.size() - 2; i++) {
            if (elements.get(i).getText() != ConstantsDtc.NOT_APPLICABLE) {
                reviewListTotalValue = reviewListTotalValue + Double.valueOf(elements.get(i).getText());
                reviewCount++;
            }
        }
        Assert.assertTrue("FAIL: Overall Rating " + extractOverallRatingForSelectedTire() + " did NOT match " +
                        commonActions.twoDForm((reviewListTotalValue / reviewCount), 1),
                extractOverallRatingForSelectedTire() == commonActions.twoDForm((reviewListTotalValue / reviewCount), 1));

        LOGGER.info("assertOverallRating completed");
    }

    /**
     * Verifies the sort option displays on the page
     *
     * @param sortOption sort option on the page
     */
    public void assertSortOptionIsDisplayed(String sortOption) {
        LOGGER.info("assertSortOptionIsDisplayed started");
        driver.waitForPageToLoad();
        String sortOptionDisplay;
        if (sortOption.contains("Cornering /")) {
            sortOptionDisplay = sortOption.split("/ ")[1];
        } else if (sortOption.contains("Buy Tire")) {
            sortOptionDisplay = sortOption.substring(9);
        } else {
            sortOptionDisplay = sortOption.split(" ")[0].trim();
        }

        WebElement sortOptionDisplayEle = webDriver.findElement(By.xpath(String.format(sortOptionXpathBy, sortOptionDisplay)));
        Assert.assertTrue("FAIL:  Unable to locate the sort option!",
                driver.isElementDisplayed(sortOptionDisplayEle));
        LOGGER.info("assertSortOptionIsDisplayed completed");
    }

    /**
     * Clicks Arrow on the Sort Option
     */
    public void clickArrowOnTheSortOption() {
        LOGGER.info("clickArrowOnTheSortOption started");
        driver.waitForElementClickable(sortedDesc);
        sortedDesc.click();
        driver.waitForPageToLoad();
        LOGGER.info("clickArrowOnTheSortOption completed");
    }
}