package dtc.pages;

import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;
import java.util.logging.Logger;

public class FoundItLowerPopupPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(FoundItLowerPopupPage.class.getName());
    private MyAccountPage myAccountPage;

    public FoundItLowerPopupPage(Driver driver) {
        commonActions = new CommonActions(driver);
        this.driver = driver;
        webDriver = driver.getDriver();
        myAccountPage = new MyAccountPage(driver);
        PageFactory.initElements(webDriver, this);
    }

    public static final String PRICE_TEXTBOX = "Lower Price(Per Tire)";
    public static final String PRICE_LINKBOX = "Link to Lower Price";
    public static final String DAYTIME_PHONE = "Daytime Phone";
    public static final String POSTAL_CODE = "Postal Code";
    public static final String FOUND_IT_LOWER_FORM_TEXT = "Found it lower form text";
    public static final String FOUND_IT_LOWER_HEADER = "Found it lower header";
    public static final String FOUND_IT_LOWER_FOOTER = "Found it lower footer";
    public static final String FOUND_IT_LOWER_BRAND = "Found it lower brand";
    public static final String FOUND_IT_LOWER_PRODUCT_NAME = "Found it lower product name";
    public static final String FOUND_IT_LOWER_PRODUCT_PRICE = "Found it lower product price";
    public static final String FOUND_IT_LOWER_MODAL_SUCCESS = "Found it lower modal success";

    public static final String FORM_TEXT = "Submit your Price Match request below and we will get back to you" +
            " within 1 business day.";

    public static final String FOOTER_TEXT = "Most competitors don't show the total price until the shopping cart. " +
            "For an accurate comparison, be sure to add the shipping charge to the base price. We will beat " +
            "competitors' delivered non-sale regular pricing (sale, clearance & closeout pricing not applicable). " +
            "The retailer must be an authorized reseller of the product and selling it through their own, " +
            "non-auction channel. Applies to In-Stock product only. We reserve the right to decline any price match" +
            " request under our discretion.";

    @FindBy(css = "[class*='found-it-lower__modal']")
    public static WebElement foundItLowerModal;

    @FindBy (css = "[class*='found-it-lower__success-wp']")
    public static WebElement foundItLowerModalSuccess;

    @FindBy(css = "[class*='found-it-lower__header']")
    public static WebElement foundItLowerHeader;

    @FindBy(css = "[class*='found-it-lower__brand']")
    public static WebElement foundItLowerBrand;

    @FindBy(css = "[class*='found-it-lower__name']")
    public static WebElement foundItLowerProductName;

    @FindBy(css = "[class*='found-it-lower__price']")
    public static WebElement foundItLowerProductPrice;

    @FindBy(css = "[class*='found-it-lower__footer']")
    public static WebElement foundItLowerFooter;

    @FindBy(css = "input[id='daytime-phone']")
    public static WebElement daytimePhone;

    @FindBy(css = "input[id='postal-code']")
    public static WebElement postalCode;

    @FindBy(css = "input[id='lower-price-per-tire']")
    public static WebElement lowerPriceField;

    @FindBy(css = "input[id='link-to-lower-price']")
    public static WebElement lowerPriceLink;

    @FindBy(css = "[class*='found-it-lower__title-text']")
    public static WebElement foundItLowerFormText;

    /**
     * Verify (Found it Lower | Instant Price Match) modal is displayed
     */
    public void assertModalIsDisplayed(String modalName) {
        LOGGER.info("assertModalIsDisplayed started for: " + modalName);
        driver.waitForPageToLoad();
        if (!driver.isElementDisplayed(foundItLowerModal)) {
            if(modalName.equalsIgnoreCase(ConstantsDtc.FOUND_IT_LOWER)){
                Assert.fail("FAIL : Found it Lower modal is not displayed");
            }else if (modalName.equalsIgnoreCase(ConstantsDtc.INSTANT_PRICE_MATCH)){
                Assert.fail("FAIL : Instant Price Match modal is not displayed");
            }
        }
        LOGGER.info("assertModalIsDisplayed completed for: " + modalName);
    }

    /**
     * Verify the specified web element exists and contains the specified text
     *
     * @param elementName The name of the web element
     * @param text The text to verify the web element contains
     */
    public void verifyWebElementContainsText(String elementName, String text) {
        LOGGER.info("verifyWebElementContainsText started for '" + elementName + "' element with text = '" + text + "'");
        WebElement element = returnWebElement(elementName);
        driver.waitForElementVisible(element, Constants.TEN);
        driver.getElementWithText(element, text, false);
        LOGGER.info("verifyWebElementContainsText started for '" + elementName + "' element with text = '" + text + "'");
    }

    /**
     * Return the web element corresponding to the input String
     *
     * @param elementName   Name of the web element
     * @return              Web Element
     */
    public WebElement returnWebElement(String elementName) {
        LOGGER.info("returnWebElement started for element name: '" + elementName + "'");
        switch (elementName) {
            case FOUND_IT_LOWER_HEADER:
                return foundItLowerHeader;
            case PRICE_TEXTBOX:
                return lowerPriceField;
            case FOUND_IT_LOWER_FORM_TEXT:
                return foundItLowerFormText;
            case FOUND_IT_LOWER_FOOTER:
                return foundItLowerFooter;
            case FOUND_IT_LOWER_BRAND:
                return foundItLowerBrand;
            case FOUND_IT_LOWER_PRODUCT_NAME:
                return foundItLowerProductName;
            case FOUND_IT_LOWER_PRODUCT_PRICE:
                return foundItLowerProductPrice;
            case FOUND_IT_LOWER_MODAL_SUCCESS:
                return foundItLowerModalSuccess;
            case PRICE_LINKBOX:
                return lowerPriceLink;
            case CommonActions.FIRST_NAME:
                return CommonActions.firstName;
            case CommonActions.LAST_NAME:
                return CommonActions.lastName;
            case DAYTIME_PHONE:
                return daytimePhone;
            case POSTAL_CODE:
                return postalCode;
            case CommonActions.EMAIL:
                return CommonActions.emailTextField;
            case CommonActions.ADDRESS_LINE_2:
                return CommonActions.billingAddress2;
            case ConstantsDtc.SEARCH_BAR:
                return webDriver.findElement(CommonActions.searchBarBy);
            case ConstantsDtc.PASSWORD:
                return CommonActions.password;
            case ConstantsDtc.ZIP_CODE:
                return webDriver.findElement(CheckoutPage.billingZipCodeBy);
            case CommonActions.ADDRESS_LINE_1:
                return webDriver.findElement(CheckoutPage.addressLine1By);
            case ConstantsDtc.EMAIL_ADDRESS_LABEL:
                return CommonActions.email;
            case ConstantsDtc.CREATE_ACCOUNT_ADDRESS_LINE_1:
                return CommonActions.address1;
            case ConstantsDtc.CREATE_ACCOUNT_ADDRESS_LINE_2:
                return CommonActions.address2;
            case ConstantsDtc.CREATE_ACCOUNT_ZIPCODE:
                return CommonActions.orderSummaryZipCode;
            case Constants.PHONE:
                return CommonActions.phone;
            default:
                Assert.fail("FAIL: Could not find web element for '" + elementName + "'");
                return null;
        }
    }

    /**
     * Clears the specified field and enters the value into the field
     *
     * @param value value to entered into the field
     * @param field field in which the value has to be entered into
     */
    public void enterIntoField(String value, String field) {
        LOGGER.info("enterIntoField started");
        WebElement fieldElement = returnWebElement(field);
        commonActions.clearEditField(fieldElement);
        fieldElement.sendKeys(value);
        LOGGER.info("enterIntoField completed");
    }
}
