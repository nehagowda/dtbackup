package dtc.pages;

/**
 * Created by aaronbriel on 9/16/16.
 */

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;
import java.util.List;

public class HomePage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private MobileHeaderPage mobileHeaderPage;
    private MyVehiclesPopupPage myVehiclesPopupPage;
    private final Logger LOGGER = Logger.getLogger(HomePage.class.getName());

    public HomePage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        mobileHeaderPage = new MobileHeaderPage(driver);
        myVehiclesPopupPage = new MyVehiclesPopupPage(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(className = "js-open-search")
    public static WebElement searchIcon;

    @FindBy(className = "header__my-store-label")
    public static WebElement myStore;

    @FindBy(className = "auto-popover-change-store")
    public static WebElement changeStoreLink;

    @FindBy(className = "auto-popover-store-details")
    public static WebElement storeDetailsLink;

    @FindBy(className = "auto-popover-schedule-appointment")
    public static WebElement scheduleAppointmentPopOverLink;

    @FindBy(id = "js-site-search-input")
    public static WebElement searchBoxInput;

    @FindBy(css = "[class*='fitment-recent-vehicles__remove-vehicle']")
    public static WebElement removeCurrentVehicleButton;

    @FindBy(className = "my-vehicles__remove-button")
    public static WebElement removeRecentVehicleButton;

    @FindBy(className = "js-checkout-w-appointment")
    public static WebElement checkoutWithAppointmentButton;

    @FindBy(className = "auto-popover-view-cart-button")
    public static WebElement viewCart;

    @FindBy(linkText = "CONTINUE SHOPPING")
    public static WebElement continueShoppingLink;

    @FindBy(className = "header__my-store-address")
    public static WebElement myStoreAddress;

    @FindBy(className = "auto-header-installers")
    public static WebElement installersMenu;

    @FindBy(className = "fa-shopping-cart")
    public static WebElement miniCartIconMobile;

    @FindBy(linkText = "Tires")
    public static WebElement tiresRadioLink;

    @FindBy(linkText = "Wheels")
    public static WebElement wheelsRadioLink;

    @FindBy(className = "mini-cart__item-quantity")
    public static WebElement miniCartItemQty;

    @FindBy(className = "my-store__hours")
    public static WebElement myStoreHours;

    @FindBy(className = "homepage")
    public static WebElement homepage;

    @FindBy(css = ".auto-header-logo > img")
    public static WebElement siteLogo;

    @FindBy(css = "[title='BROWSE WHEELS']")
    public static WebElement wheelConfiguratorImage;

    @FindBy(className = "fitment-component__imageBlock")
    public static WebElement browseWheels;

    @FindBy(css = ".wheel-viewer img")
    public static WebElement browseWheelsStg;

    @FindBy(css = "span.my-store__phone.js-store-phone > span")
    private static WebElement myStorePhoneNumber;

    @FindBy(className = "js-cart-count")
    public static WebElement miniCartCount;

    @FindBy(css = "[class*='fitment-banner__wheel'] span:nth-child(1)")
    public static WebElement wheelSize;

    public static By storePopUpBy = By.className("store-pop");

    private static By miniCartIconBy = By.className("fa-shopping-cart");

    public static By cartItemCountBy = By.className("header__cart-item-count");

    public static By siteLogoBy = By.cssSelector(".auto-header-logo > img");

    public static By profileIconMobileBy = By.cssSelector("[class*='header__drop-down-icon--mobile']");

    public static final String DT_LOGO = "DT_logo.svg";

    public static final String AT_LOGO = "AT_logo.svg";

    public static final String DTD_LOGO = "DTD_logo.png";

    /**
     * Clicks "Choose a store" button when prompted
     */
    public void handleChooseStorePopup() {
        LOGGER.info("handleChooseStorePopup started");
        if (driver.isElementDisplayed(storePopUpBy, Constants.FIVE))
            driver.jsScrollToElementClick(CommonActions.chooseStoreButton);
        driver.waitForElementNotVisible(storePopUpBy);
        LOGGER.info("handleChooseStorePopup completed");
    }

    /**
     * Opens the My Vehicles popup
     */
    public void openMyVehiclesPopup() {
        LOGGER.info("openFitmentPopup started");
        driver.waitForPageToLoad();
        if (!driver.isElementDisplayed(CommonActions.dtModalContainerBy, Constants.ONE)) {
            if (Config.isMobile()) {
                mobileHeaderPage.openMobileMenu();
            } else {
                do {
                    driver.jsScrollToElementClick(CommonActions.myVehiclesHeaderButton);
                    driver.waitSeconds(Constants.ONE);
                } while (!driver.isElementDisplayed(CommonActions.dtModalContainer));
            }
        }
        LOGGER.info("openFitmentPopup completed");
    }

    /**
     * Opens the fitment popup page via 'My Vehicles' and 'Add Vehicle' elements
     */
    public void openFitmentPopup() {
        LOGGER.info("openFitmentPopup started");
        if (driver.isElementDisplayed(CommonActions.dtModalContainerBy, Constants.ONE))
            return;
        if (Config.isMobile()) {
            mobileHeaderPage.openMobileMenu();
            mobileHeaderPage.clickMenuLink(ConstantsDtc.ADD_VEHICLE.toUpperCase());
        } else {
            CommonActions.myVehiclesHeaderButton.click();
            commonActions.clickButtonLink(ConstantsDtc.ADD_VEHICLE);
            driver.waitForMilliseconds(Constants.ONE_HUNDRED);
        }
        LOGGER.info("openFitmentPopup completed");
    }

    /**
     * Clicks the Store Details link in the My Store menu
     */
    public void openStoreDetails() {
        LOGGER.info("openStoreDetails started");
        if (!driver.isElementVisible(storeDetailsLink, Constants.TWO)) {
            clickGlobalMyStoreHeader();
        }
        driver.waitForElementClickable(storeDetailsLink);
        driver.jsScrollToElement(storeDetailsLink);
        storeDetailsLink.click();
        LOGGER.info("openStoreDetails completed");
    }

    /**
     * Clicks the Change Store link in the My Store menu
     */
    public void openChangeStore() {
        LOGGER.info("openChangeStore started");
        driver.waitForMilliseconds();
        clickGlobalMyStoreHeader();
        driver.waitForElementClickable(changeStoreLink);
        driver.jsScrollToElement(changeStoreLink);

        //Deal with intermittent loading status failures
        try {
            changeStoreLink.click();
        } catch (Exception e) {
            driver.waitForElementClickable(changeStoreLink);
            changeStoreLink.click();
        }
        driver.waitForPageToLoad(Constants.ZERO);
        LOGGER.info("openChangeStore completed");
    }

    /**
     * Clicks the Schedule Appointment link in the My Store menu
     */
    public void openScheduleAppointment() {
        LOGGER.info("openScheduleAppointment started");
        clickGlobalMyStoreHeader();
        driver.waitForElementClickable(scheduleAppointmentPopOverLink);
        driver.jsScrollToElement(scheduleAppointmentPopOverLink);
        scheduleAppointmentPopOverLink.click();
        LOGGER.info("openScheduleAppointment completed");
    }

    /**
     * Clicks 'My Store' global header
     */
    public void clickGlobalMyStoreHeader() {
        LOGGER.info("clickGlobalMyStoreHeader started");
        driver.jsScrollToElementClick(myStore);
        LOGGER.info("clickGlobalMyStoreHeader completed");
    }

    /**
     * Navigates the page to the main home page and checks for Welcome popup
     * Dynamically generated based on which site and region is being tested
     */
    public void goToHome() {
        goToHome(true);
    }

    /**
     * Navigates the page to the main home page with the option to check for Welcome popup
     * Dynamically generated based on which site and region is being tested
     *
     * @param checkForPopup true/false whether to check for Welcome popup
     */
    public void goToHome(boolean checkForPopup) {
        LOGGER.info("goToHome started");
        driver.waitForPageToLoad(Constants.ZERO);
        String currentUrl = webDriver.getCurrentUrl();
        String baseUrl = Config.getBaseUrl();
        if (!currentUrl.equals(baseUrl)) {
            driver.getUrl(baseUrl);
            driver.waitForPageToLoad(Constants.ZERO);
        }
        if (checkForPopup)
            commonActions.closeWelcomePopUp(Constants.CONTINUE);
        LOGGER.info("goToHome completed");
    }

    /**
     * Searches for a product via the search box at top right of home page
     *
     * @param item Specific item or product to search for as well as View All Results link
     */
    public void searchItem(String item) {
        LOGGER.info("searchItem started");

        if (Config.isIe())
            driver.waitForMilliseconds(Constants.THIRTY_THOUSAND);

        driver.waitForPageToLoad();

        //The search icon doesn't exist in IE
        if (!Config.isIe()) {
            driver.waitForElementVisible(searchIcon);
            driver.jsScrollToElement(searchIcon);
            searchIcon.click();
        }

        //TODO CCL - fails here for IE depending on the resolution of the VM / machine running the test; If too small
        //TODO CCL (cont) the textbox for search cannot be seen or acted on by the user
        driver.waitForElementClickable(searchBoxInput);
        searchBoxInput.sendKeys(item);

        //Needed for Firefox and IE to keep the focus on the search field and in turn keep the suggested items visible
        if (Config.isFirefox() || Config.isIe()) {
            searchBoxInput.click();
        }

        //TODO: retest when new safaridriver is stable
        if (Config.isSafari() || Config.isIe())
            driver.waitForMilliseconds();

        LOGGER.info("searchItem completed");
    }

    /**
     * Searches for a product via the search box at top right of home page and hit enter
     *
     * @param item Specific item or product to search for as well as View All Results link
     */
    public void searchItemHitEnter(String item) {
        LOGGER.info("searchItemHitEnter started");
        searchItem(item);
        driver.waitForPageToLoad();
        searchBoxInput.sendKeys(Keys.ENTER);
        LOGGER.info("searchItemHitEnter completed");
    }

    /**
     * Clicks the Mini-cart icon and then View Cart
     */
    public void openMiniCartPopupAndViewCart() {
        LOGGER.info("openMiniCartPopupAndViewCart started");
        clickMiniCartIcon();
        driver.jsScrollToElementClick(viewCart);
        LOGGER.info("openMiniCartPopupAndViewCart completed");
    }

    /**
     * Clicks the Mini-cart icon and then Clear Cart
     */
    public void openMiniCartPopupAndClearCart() {
        LOGGER.info("openMiniCartPopupAndViewCart started");
        clickMiniCartIcon();
        if (driver.getElementWithText(CommonActions.displayInlineSmBy, ConstantsDtc.CLEAR_CART) != null) {
            driver.clickElementWithText(CommonActions.displayInlineSmBy, ConstantsDtc.CLEAR_CART);
            commonActions.clickButtonByText(ConstantsDtc.YES_CLEAR_CART);
            driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
            Assert.assertEquals("FAIL: Cart was not cleared", 0, Integer.parseInt(commonActions.getCartItemCount()));
        }
        LOGGER.info("openMiniCartPopupAndViewCart completed");
    }

    /**
     * Clicks the Mini-cart icon
     */
    public void clickMiniCartIcon() {
        LOGGER.info("clickMiniCartIcon started");
        if (Config.isMobile()) {
            driver.waitForElementVisible(miniCartIconMobile);
            miniCartIconMobile.click();
        } else {
            driver.waitForClassPresent(driver.getByValue(miniCartIconBy), Constants.THIRTY);
            WebElement miniCartIconEl = driver.getDisplayedElement(miniCartIconBy, Constants.ZERO);
            miniCartIconEl.click();
        }
        LOGGER.info("clickMiniCartIcon completed");
    }

    /**
     * Clicks the Mini-cart icon and then Continue Shopping
     */
    public void openMiniCartPopupAndContinueShopping() {
        LOGGER.info("openMiniCartPopupAndContinueShopping started");
        clickMiniCartIcon();
        driver.waitForElementClickable(continueShoppingLink);
        continueShoppingLink.click();
        LOGGER.info("openMiniCartPopupAndContinueShopping completed");
    }

    /**
     * Removes a specified vehicle.
     *
     * @param vehicleDescription The vehicle to be removed. Either "SELECTED VEHICLE" or Make, model, style, etc.
     */
    public void removeSpecifiedVehicle(String vehicleDescription) {
        LOGGER.info("removeSpecifiedVehicle started for '" + vehicleDescription + "''");
        driver.waitForPageToLoad();
        if (Config.isMobile()) {
            mobileHeaderPage.openMobileMenu();
        } else {
            openMyVehiclesPopup();
        }
        WebElement vehicleRow = commonActions.returnMyVehiclesRowWithVehicle(vehicleDescription);
        driver.clickElementWithText(vehicleRow, CommonActions.displayInlineSmBy, Constants.DELETE);
        driver.clickElementWithText(vehicleRow, CommonActions.dtButtonLgBy, ConstantsDtc.YES_DELETE_THIS_VEHICLE);
        LOGGER.info("removeSpecifiedVehicle completed for '" + vehicleDescription + "''");
    }

    /**
     * Verifies the details listed under the My Store header
     *
     * @param store Store address to verify
     */
    public void verifyMyStoreDetails(String store) {
        LOGGER.info("verifyMyStore started");
        driver.waitForElementClickable(myStore);
        Assert.assertTrue("FAIL: \"My Store\" address: " + myStoreAddress.getText()
                + " did NOT contain expected: \"" + store + "\"!", myStoreAddress.getText().trim().toLowerCase().contains(store.toLowerCase()));
        LOGGER.info("verifyMyStore completed");
    }

    /**
     * Verifies My Vehicle in the header
     *
     * @param vehicle Vehicle to verify
     */
    public void verifyMyVehicle(String vehicle) {
        LOGGER.info("verifyMyVehicle started");
        Assert.assertTrue("FAIL: My Vehicle in the header did not equal \"" + vehicle + "\".",
                driver.waitForTextPresent(CommonActions.myVehiclesHeaderBy, vehicle, Constants.THIRTY));
        LOGGER.info("verifyMyVehicle completed");
    }

    /**
     * Verifies item total in the header cart
     *
     * @param total Item total to verify in the header cart
     */
    public void verifyCartTotal(String total) {
        LOGGER.info("verifyCartTotal started");
        WebElement miniCart;
        if (Config.isMobile()) {
            miniCart = miniCartCount;
        } else {
            miniCart = CartPage.miniCartPrice;
        }
        driver.waitForMilliseconds(Constants.THREE_THOUSAND);
        Assert.assertTrue("FAIL: The actual cart total: \"" + miniCart.getText()
                        + "\" did not contain the expected total: \"" + total + "\"!",
                miniCart.getText().contains(total));
        LOGGER.info("verifyCartTotal completed");
    }

    /**
     * Verifies item count in the header cart
     *
     * @param count Item count to verify in the header cart
     */
    public void verifyCartItemCount(String count) {
        LOGGER.info("verifyCartItemCount started");
        Assert.assertTrue("FAIL: Cart item count in header not equal to \"" + count + "\".",
                commonActions.getCartItemCount().equals(count));
        LOGGER.info("verifyCartItemCount completed");
    }

    /**
     * Assert displayed item Qty matches with provided one on mini cart modal
     */
    public void assertItemQtyMiniCartModal(String qty) {
        LOGGER.info("assertItemQtyMiniCartModal started");

        driver.waitForClassPresent(driver.getByValue(miniCartIconBy), Constants.THIRTY);
        WebElement cartIcon = driver.getDisplayedElement(miniCartIconBy, Constants.ZERO);
        cartIcon.click();

        driver.waitForElementClickable(viewCart);
        Assert.assertTrue("FAIL: Mini Cart item count did not equal \"" + qty + "\".",
                miniCartItemQty.getText().contains(qty));

        //To close the mini-cart modal
        cartIcon.click();

        LOGGER.info("assertItemQtyMiniCartModal completed");
    }

    /***
     * Verifies the "STORE HOURS" information listed in the "My Store" popup
     */
    public void verifyMyStoreHours() {
        LOGGER.info("verifyMyStoreHours started");
        driver.waitForElementClickable(storeDetailsLink);
        for (String daysHours : ConstantsDtc.MY_STORE_POPUP_STORE_HOURS) {
            Assert.assertTrue("FAIL: \"My Store\" hours: " + myStoreHours.getText()
                            + " did NOT contain expected day and hour combination of: \"" + daysHours + "\"!",
                    myStoreHours.getText().contains(daysHours));
        }
        LOGGER.info("verifyMyStoreHours completed");
    }

    /**
     * Verifies user is on the homepage
     */
    public void verifyHomepage() {
        LOGGER.info("verifyHomepage started");

        if (Config.isFirefox())
            driver.waitForMilliseconds();

        driver.waitForPageToLoad();
        driver.waitForElementVisible(homepage);
        LOGGER.info("verifyHomepage completed");
    }

    /**
     * Returns site logo filename based on site region
     *
     * @return String The logo filename
     */
    public String getSiteLogo() {
        LOGGER.info("getSiteLogo started");
        String siteLogo = null;

        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            siteLogo = DT_LOGO;
        } else if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            siteLogo = AT_LOGO;
        } else if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            siteLogo = DTD_LOGO;
        }
        LOGGER.info("getSiteLogo completed");

        return siteLogo;
    }

    /**
     * Verifies site logo
     */
    public void verifySiteLogo() {
        LOGGER.info("verifySiteLogo started");
        driver.waitForElementAttribute(siteLogo, Constants.ATTRIBUTE_SRC, getSiteLogo());
        LOGGER.info("verifySiteLogo completed");
    }

    /**
     * Verifies site logo
     *
     * @param site Discount Tire/America's tire
     */
    public void verifySiteLogo(String site) {
        LOGGER.info("verifySiteLogo started");
        String expectedSiteLogo;
        if (site.equalsIgnoreCase(ConstantsDtc.DISCOUNT_TIRE_SITE)) {
            expectedSiteLogo = DT_LOGO;
        } else {
            expectedSiteLogo = AT_LOGO;
        }
        driver.waitForElementAttribute(siteLogo, Constants.ATTRIBUTE_SRC, expectedSiteLogo);
        LOGGER.info("verifySiteLogo completed");
    }

    /**
     * Verifies join/sign-in option displayed on homepage
     *
     * @param page Homepage/PLP/PDP/Checkout
     */
    public void verifyJoinSignInIsDisplayed(String page) {
        LOGGER.info("verifyJoinSignInIsDisplayed started on " + page);
        if (Config.isFirefox())
            driver.waitForMilliseconds();

        driver.waitForPageToLoad();
        if (Config.isMobile()) {
            Assert.assertTrue("FAIL : Join / Sign In link did NOT display on " + page,
                    driver.isElementVisible(webDriver.findElement(HomePage.profileIconMobileBy), Constants.ONE));
        } else if (!driver.isElementDisplayed(driver.getElementWithText(CommonActions.dtLinkBy, ConstantsDtc.JOIN_SIGN_IN))) {
            Assert.fail("FAIL : Join / Sign In link did NOT display on " + page);
        }
        LOGGER.info("verifyJoinSignInIsDisplayed completed on " + page);
    }

    /**
     * Verify the link with link text is displayed on respective page/global header
     *
     * @param linkText text appears for the link
     * @param pageType web page
     */
    public void verifyLinkIsDisplayed(String linkText, String pageType) {
        LOGGER.info("verifyLinkIsDisplayed started for " + linkText + " on " + pageType + " page");
        if (Config.isFirefox())
            driver.waitForMilliseconds();

        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        if (!driver.isElementDisplayed(driver.getElementWithText(CommonActions.dtLinkBy, linkText))) {
            Assert.fail("FAIL : " + linkText + " link did NOT displayed on " + pageType + " page");
        }
        LOGGER.info("verifyLinkIsDisplayed completed for " + linkText + " on " + pageType + " page");
    }

    /**
     * Click 'Checkout with Appointment' button
     */
    public void clickCheckoutWithAppointmentButton() {
        LOGGER.info("selectCheckoutWithAppointment started");
        commonActions.clickButtonByText(ConstantsDtc.CHECKOUT_WITH_APPOINTMENT);
        LOGGER.info("selectCheckoutWithAppointment completed");
    }

    /**
     * Click the header "Join/Sign In" link for My Account
     */
    public void clickJoinSignInLink() {
        LOGGER.info("clickJoinSignInLink started");
        WebElement profileIconElement;
        if (Config.isMobile())
            profileIconElement = driver.getDisplayedElement(profileIconMobileBy, Constants.ZERO);
        else
            profileIconElement = driver.getDisplayedElement(MyAccountPage.getSignInBy, Constants.ONE);

        Assert.assertNotNull("FAIL: The 'Join/Sign In' link is not present. Ensure that MyAccount is turned on.",
                profileIconElement);
        profileIconElement.click();
        LOGGER.info("clickJoinSignInLink completed");
    }

    /**
     * Verifies the "My Store" popup contains the "CHANGE STORE", "STORE DETAILS", and "SCHEDULE APPOINTMENT" controls
     */
    public void verifyMyStoreContainsControls() {
        LOGGER.info("verifyMyStoreContainsControls started");
        driver.waitForElementClickable(storeDetailsLink);

        ArrayList<WebElement> expectedControlsList = new ArrayList<>(Arrays.asList(changeStoreLink,
                scheduleAppointmentPopOverLink, storeDetailsLink));

        for (WebElement expectedControl : expectedControlsList) {
            Assert.assertTrue("FAIL: The expected control: '" + expectedControl.getText() + "' was NOT displayed!",
                    driver.isElementDisplayed(expectedControl));
        }
        LOGGER.info("verifyMyStoreContainsControls completed");
    }

    /**
     * Verify Wheel Configurator text is displayed on home page
     */
    public void assertWheelConfiguratorTextIsDisplayed(String text) {
        LOGGER.info("assertWheelConfiguratorTextIsDisplayed started");
        driver.waitForPageToLoad();
        WebElement wheelConfiguratorText = webDriver.findElement(CommonActions.strongBy);
        driver.jsScrollToElement(wheelConfiguratorText);
        Assert.assertTrue("FAIL: Wheel Configurator text: " + wheelConfiguratorText.getText()
                        + " not matching to expected text: " + text,
                wheelConfiguratorText.getText().equalsIgnoreCase(text));
        LOGGER.info("assertWheelConfiguratorTextIsDisplayed completed");
    }

    /**
     * Verify Wheel Configurator image is displayed on home page
     */
    public void assertWheelConfiguratorImageIsDisplayed() {
        LOGGER.info("assertWheelConfiguratorImageIsDisplayed started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: Wheel Configurator image not displayed!",
                driver.isElementDisplayed(wheelConfiguratorImage));
        LOGGER.info("assertWheelConfiguratorImageIsDisplayed completed");
    }

    /**
     * Verify BROWSE WHEELS is displayed on home page
     */
    public void assertBrowseWheelsIsDisplayed() {
        LOGGER.info("assertBrowseWheelsIsDisplayed started");
        driver.waitForPageToLoad();
        String browseWheelsBorderColor = browseWheels.getCssValue(Constants.BORDER);
        String browseWheelsColor = browseWheels.getCssValue(Constants.COLOR);
        Assert.assertTrue("FAIL: BROWSE WHEELS displayed on home page with border:"
                        + browseWheelsBorderColor + ", expected red border contains:" + Constants.DTC_RED
                        + ". Displayed font:" + browseWheelsColor + ", expected red font contains:" + Constants.RED_COLOR_RGBA,
                driver.isElementDisplayed(browseWheels) &&
                        browseWheelsBorderColor.contains(Constants.DTC_RED) &&
                        browseWheelsColor.contains(Constants.RED_COLOR_RGBA));
        LOGGER.info("assertBrowseWheelsIsDisplayed completed");
    }

    /**
     * Verifies "My Store" phone number matches the phone number of the store selected on the "Check Inventory" pop up
     */
    public void verifyMyStoreMatchesCheckInventoryPopupSelectionByPhoneNumber() {
        LOGGER.info("verifyMyStoreMatchesCheckInventoryPopupSelectionByPhoneNumber started");
        clickGlobalMyStoreHeader();
        Assert.assertEquals("FAIL: The current 'My Store' does NOT have the expected phone number!",
                CheckAvailabilityPopupPage.makeMyStorePhoneNumber.trim(), myStorePhoneNumber.getText().trim());
        LOGGER.info("verifyMyStoreMatchesCheckInventoryPopupSelectionByPhoneNumber completed");
    }

    /**
     * Navigates to Geo IP located store home page
     */
    public void goToGeoIpStoreHomePage() {
        LOGGER.info("goToGeoIpStoreHomePage started");
        driver.waitForPageToLoad();
        String state = Config.getStateCode().toLowerCase();
        String geoip = Constants.geoIP.get(state);
        driver.getUrl(Config.getBaseUrl() + ConstantsDtc.GEO_IP_URL + geoip);
        driver.waitForPageToLoad();
        commonActions.closeWelcomePopUp(Constants.CONTINUE);
        LOGGER.info("goToGeoIpStoreHomePage completed");
    }

    /**
     * Navigates to store home page for a specified state
     *
     * @param state The state of the homepage to go to
     */
    public void goToStateIpStoreHomePage(String state) {
        LOGGER.info("goToStateIpStoreHomePage started");
        driver.waitForPageToLoad();
        String geoip = Constants.geoIP.get(state.toLowerCase());
        driver.getUrl(Config.getBaseUrl() + ConstantsDtc.GEO_IP_URL + geoip);
        driver.waitForPageToLoad();
        commonActions.closeWelcomePopUp(Constants.CONTINUE);
        LOGGER.info("goToStateIpStoreHomePage completed");
    }

    /**
     * This method will validate the Tire/Wheel Size on the PDP Page
     *
     * @param elementName  Name of element TIRE/WHEEL
     */
    public void assertTireOrWheelSizeOnPDP(String elementName) {
        LOGGER.info("assertTireOrWheelSizeOnPDP started");
        String actualValue;
        String actualWheelValue;
        String actualTireValue;
        if (elementName.equalsIgnoreCase(Constants.TIRE)) {
            actualTireValue = ProductDetailPage.productSize.getText();
            String[] arrayList = actualTireValue.trim().replaceAll(" +", " ").split(" ");
            actualValue = arrayList[0] + arrayList[1] + "-" + arrayList[2];
        } else {
            actualWheelValue = ProductDetailPage.productSize.getText();
            actualValue = actualWheelValue.split(" ")[0];
        }

        String expectedValue;
        String expectedValueTemp;
        if (elementName.equalsIgnoreCase(Constants.TIRE)) {
            expectedValue = driver.scenarioData.genericData.get(ConstantsDtc.TIRE_SIZE);
        } else {
            expectedValueTemp = driver.scenarioData.genericData.get(ConstantsDtc.WHEEL_SIZE);
            expectedValue = expectedValueTemp.split(" ")[0];
        }

        Assert.assertTrue("FAIL: The actual size of " + elementName + " = " + actualValue +
                        "on the PDP page does not contain the expected value = " + expectedValue,
                actualValue.equalsIgnoreCase(expectedValue));
        LOGGER.info("assertTireOrWheelSizeOnPDP completed");
    }

    /**
     * This method will extract the Size of the product at the specified location on the Homepage
     *
     * @param tireOrWheel Tires or Wheels
     */
    public void extractTireOrWheelSizeOnHomepage(String tireOrWheel) {
        LOGGER.info("extractTireOrWheelSizeOnHomepage started");
        String tireText;
        String wheelText;
        StringBuilder wheelDesiredText = new StringBuilder();
        if (tireOrWheel.equalsIgnoreCase(Constants.TIRE)) {
            tireText = ProductListPage.factoryFrontTire.getText();
            driver.scenarioData.genericData.put(ConstantsDtc.TIRE_SIZE, tireText);
        }
        if (tireOrWheel.equalsIgnoreCase(Constants.WHEEL)) {
            wheelText = HomePage.wheelSize.getText();
            for (int i = 0; i < wheelText.length(); i++) {
                int x;
                try {
                    x = Integer.parseInt(wheelText.substring(i, i + 1));
                    wheelDesiredText.append(x);
                } catch (Exception e) {
                    //continue
                }
            }
            driver.scenarioData.genericData.put(ConstantsDtc.WHEEL_SIZE, wheelDesiredText.toString());
        }
        LOGGER.info("extractTireOrWheelSizeOnHomepage completed");
    }
}