package dtc.pages;

/**
 * Created by aarora on 01/15/18.
 */

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class PdlxFitmentPopupPage {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(PdlxFitmentPopupPage.class.getName());
    private CommonActions commonActions;

    public PdlxFitmentPopupPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
        commonActions = new CommonActions(driver);
    }

    @FindBy(className = "fitment-box__close")
    public static WebElement closeButton;

    @FindBy(className = "fitment-vehicle-display__change")
    public static WebElement changeVehicle;

    @FindBy(className = "fitment-pdl-entry__oe-size")
    public static WebElement oeSizePdlFitment;

    @FindBy(className = "fitment-pdl-entry__title")
    public static WebElement pdlDrivingDetailsTitle;

    @FindBy(id = "primary-driving-location")
    public static WebElement pdlZipcode;

    @FindBy(id = "pdl-miles-driven")
    public static WebElement pdlInputMiles;

    @FindBy(className = "fitment-pdl-entry__submit")
    public static WebElement viewRecommendationTiresButton;

    @FindBy(css = "[class*='fa fa-pencil-square-o']")
    public static WebElement editOptionIcon;

    @FindBy(css = "div[class*='homepage']")
    public static WebElement treadwellHomePage;

    @FindBy(css = "[class*='banner__topRecommendation']")
    public static WebElement topRecommendation;

    @FindBy(css = "[src*='treadwell_logo.svg']")
    public static WebElement treadwellLogoImg;

    @FindBy(css = "[class*='treadwell-logo']")
    public static WebElement treadwellLogo;

    private static final By drivingPriorityOptionsOrderBy = By.className("fitment-pdl-entry__priority-option-order");

    private static final By errorMessageBy = By.cssSelector("[class*='form-group__message form-group__message--error']");

    private static final By myStoreZipCodeBy = By.cssSelector("[class*='my-store__zip']");


    private static final By closeButtonBy = By.className("fitment-box__close");

    public static final By drivingPriorityOptionsBy = By.className("fitment-pdl-entry__priority-option");

    public static final By drivingPriorityOptionsNameBy = By.className("fitment-pdl-entry__priority-option-name");

    private static final By getStartedBy = By.cssSelector("[href='/#/fitment/treadwell']");

    private static final By treadwellZipcodeBy = By.id("primary-driving-location");

    private static final By milesDrivenPerYearBy = By.className("fitment-pdl-entry__miles-driven-error");

    private static final By myStoreZipCodeMobileBy = By.cssSelector("[class*='address-postalcode']");

    public static final By titleBy = By.cssSelector("[class*='__title']");

    public static final By subtitleBy = By.cssSelector("[class*='__subtitle']");

    public static final By sloganTitleBy = By.cssSelector("[class*='slogan-title']");

    public static final By sloganDesceBy = By.cssSelector("[class*='slogan-desc']");

    public static final String STORE_FINDER = "Store Finder";
    private static final int rightPositionXOffsetValue = 188;
    private static final int leftPositionXOffsetValue = -182;


    /**
     * Verifies the pdl fitment oe size label matches with provided value
     *
     * @param value Text value of the oe size fitment
     */
    public void assertSelectedPdlFitmentTireSizeValue(String value) {
        LOGGER.info("assertSelectedPdlFitmentTireSizeValue started");
        driver.waitForElementVisible(oeSizePdlFitment);
        Assert.assertTrue("FAIL: The actual fitment pdl oe size value: \"" + oeSizePdlFitment.getText()
                        + "\" did not match with expected: \"" + value + "\"!",
                oeSizePdlFitment.getText().contains(value));
        LOGGER.info("Confirmed that \"" + value + "\" was listed as oe size for selected pdl fitment.");
        LOGGER.info("assertSelectedPdlFitmentTireSizeValue completed");
    }

    /**
     * Verify treadwell logo, title and subtitle displayed on PLP/PDP/Compare Products/Fitment Popup/Treadwell Fitment popup page
     *
     * @param page web page
     */
    public void assertTreadwellLogoTitleAndSubtitle(String page) {
        LOGGER.info("assertTreadwellLogoTitleAndSubtitle started for " + page);
        WebElement treadwellImage;
        WebElement treadwellTitle;
        WebElement treadwellSubtitle;
        if (page.contains(ConstantsDtc.PDP) || page.contains(ConstantsDtc.TREADWELL)) {
            treadwellTitle = driver.getElementWithText(titleBy, ConstantsDtc.TREADWELL_TITLE);
            treadwellSubtitle = driver.getElementWithText(subtitleBy, ConstantsDtc.TREADWELL_SUBTITLE);
            treadwellImage = treadwellLogo;

        } else {
            // This covers for fitment modal, PLP page and Compare products page
            treadwellTitle = driver.getElementWithText(sloganTitleBy, ConstantsDtc.TREADWELL_TITLE);
            treadwellSubtitle = driver.getElementWithText(sloganDesceBy, ConstantsDtc.TREADWELL_SUBTITLE);
            treadwellImage = treadwellLogoImg;
        }
        driver.jsScrollToElement(treadwellImage);
        Assert.assertTrue("FAIL: The treadwell title not displayed on page " + page,
                driver.isElementDisplayed(treadwellTitle, Constants.ZERO));
        Assert.assertTrue("FAIL: The treadwell Subtitle not displayed on page " + page,
                driver.isElementDisplayed(treadwellSubtitle, Constants.ZERO));
        Assert.assertTrue("FAIL: The treadwell Logo not displayed on page " + page,
                driver.isElementDisplayed(treadwellImage, Constants.ZERO));
        LOGGER.info("assertTreadwellLogoTitleAndSubtitle completed for " + page);
    }

    /**
     * Clicks & set miles to input miles driven per year
     *
     * @param value Text value of miles to set
     */
    public void setMiles(String value) {
        LOGGER.info("setMiles started");
        driver.waitForElementVisible(pdlInputMiles);
        Assert.assertTrue("FAIL: Miles driver per year text box is not present", driver.isElementDisplayed(pdlInputMiles));
        pdlInputMiles.click();
        pdlInputMiles.clear();
        if (!pdlInputMiles.getText().equals(" ")) {
            pdlInputMiles.clear();
        }
        pdlInputMiles.sendKeys(value);
        LOGGER.info("setMiles completed");
    }

    /**
     * Verifies the miles driven per year value matches with provided value
     *
     * @param value Text value of the miles
     */
    public void assertMilesDrivenPerYearValue(String value) {
        LOGGER.info("assertMilesDrivenPerYearValue started");
        driver.waitForElementVisible(pdlInputMiles);
        String milesValue = pdlInputMiles.getAttribute(Constants.VALUE);
        if (milesValue.equalsIgnoreCase(null)) {
            Assert.fail("FAIL: The actual miles driven per year value was: \"" + null
                    + "\" did not match with expected: \"" + value + "\"!");
        } else {
            Assert.assertTrue("FAIL: The actual miles driven per year value: \"" + milesValue
                    + "\" did not match with expected: \"" + value + "\"!", milesValue.contains(value));
            LOGGER.info("Confirmed that \"" + value + "\" was listed as miles value.");
            LOGGER.info("assertMilesDrivenPerYearValue completed");
        }
    }

    /**
     * Finds an driving priority tab element on the PDL Fitment popup screen and selects it
     *
     * @param priority Driving Priority to be selected
     */
    public void selectDrivingPriority(String priority) {
        LOGGER.info("selectDrivingPriority started");

        //TODO: retest when new safaridriver is stable
        if (Config.isSafari() || Config.isMobile())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        WebElement drivingPriorityTab = driver.getElementWithText(CommonActions.buttonBy, priority);
        try {
            driver.jsScrollToElement(drivingPriorityTab);
            drivingPriorityTab.click();
        } catch (NoSuchElementException e) {
            Assert.fail("FAIL: Driving Priority label \"" + priority + "\" NOT found. (Full Stack Trace: " + e.toString() + ")");
        } catch (WebDriverException we) {
            driver.waitForMilliseconds();
            drivingPriorityTab.click();
        }
        driver.waitForPageToLoad();

        LOGGER.info("selectDrivingPriority completed");
    }

    /**
     * Closes the Fitment popup
     **/
    public void closeFitmentPopUp() {
        LOGGER.info("closeFitmentPopUp started");
        driver.waitForElementVisible(closeButton);
        closeButton.click();
        driver.waitForElementNotVisible(closeButtonBy);
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        LOGGER.info("closeFitmentPopUp completed");
    }


    /**
     * Clicks View Recommended Tires Button
     */
    public void selectViewRecommendedTiresButton() {
        LOGGER.info("selectViewRecommendedTiresButton started");
        setDrivingPrioritiesScenarioDataFromTreadwellModal();
        driver.waitForElementClickable(viewRecommendationTiresButton);
        driver.jsScrollToElement(viewRecommendationTiresButton);
        viewRecommendationTiresButton.click();
        driver.waitForMilliseconds();
        LOGGER.info("selectViewRecommendedTiresButton completed");
    }

    /**
     * checks weather the zipcode error message is displayed for invalid zipcode and absent for vaild zipcode
     *
     * @param option1 valid or invalid zipcode
     * @param option2 primary driving location or miles driven per year
     */
    public void invalidZipcodeErrorMessage(String option1, String option2) {
        LOGGER.info("invalidZipcodeErrorMessage started");
        if (option2.equalsIgnoreCase(ConstantsDtc.PRIMARY_DRIVING_LOCATION)) {
            String errorMessage = webDriver.findElement(errorMessageBy).getText();
            if (option1.equalsIgnoreCase(ConstantsDtc.VALID)) {
                Assert.assertTrue("FAIL: Error message did not contain" + ConstantsDtc.PRIMARYDRIVINGLOCATIONERRORMESSAGE,
                        errorMessage.toLowerCase().equals(ConstantsDtc.PRIMARYDRIVINGLOCATIONERRORMESSAGE));
            } else if (option1.equalsIgnoreCase(ConstantsDtc.INVALID)) {
                Assert.assertTrue("FAIL: Error message was not displayed", driver.isElementDisplayed(errorMessageBy));
            }
        } else if (option2.equalsIgnoreCase(ConstantsDtc.MILES_DRIVEN_PER_YEAR)) {
            String errorMessage = webDriver.findElement(milesDrivenPerYearBy).getText();
            if (option1.equalsIgnoreCase(ConstantsDtc.VALID)) {
                Assert.assertTrue("FAIL: Error message did not contain" + ConstantsDtc.MILESDRIVENPERYEARERRORMESSAGE,
                        errorMessage.toLowerCase().equals(ConstantsDtc.MILESDRIVENPERYEARERRORMESSAGE));
            } else if (option1.equalsIgnoreCase(ConstantsDtc.INVALID)) {
                Assert.assertTrue("FAIL: Error message was not displayed", driver.isElementDisplayed(milesDrivenPerYearBy));
            }
        }
        LOGGER.info("invalidZipcodeErrorMessage completed");
    }

    /**
     * verifies weather the edit option is displayed on the treadwell model
     */
    public void assertEditOptionDisplayed() {
        LOGGER.info("assertEditOptionDisplayed started");
        Assert.assertTrue("FAIL: Edit icon is not displayed ", driver.isElementDisplayed(editOptionIcon));
        LOGGER.info("assertEditOptionDisplayed completed");
    }

    /**
     * moves the priority option to the specifed position on the treadwell model
     *
     * @param optionName specifies the name of the priority which has to be moved
     * @param position   specifies the position to which the priority option to be moved to
     */

    public void moveToElement(String optionName, String position) {
        LOGGER.info("moveToElement started");
        int adjustmentOffSet = 0;
        WebElement destination = webDriver.findElement(By.xpath("//span[@class='fitment-pdl-entry__priority-option-order']" +
                "[text()='" + position + "']/parent::div"));
        WebElement option = webDriver.findElement(By.xpath("//span[text()='" + optionName + "']/parent::div"));
        WebElement optionOrder = webDriver.findElement(By.xpath("//span[text()='" + optionName + "']/preceding::span[1]"));
        driver.jsScrollToElementClick(driver.getElementWithText(drivingPriorityOptionsOrderBy, position));
        if (!option.getText().contains(position)) {
            if (Config.isMobile()) {
                if (Integer.valueOf(optionOrder.getText()) < Integer.valueOf(position)) {
                    adjustmentOffSet = -50;
                } else {
                    adjustmentOffSet = 50;
                }
            } else {
                if ((Integer.valueOf(optionOrder.getText()) < Integer.valueOf(position))) {
                    adjustmentOffSet = -60;
                } else {
                    adjustmentOffSet = 60;
                }
            }
            Actions actions = new Actions(webDriver);
            if (Config.isMobile()) {
                actions.clickAndHold(option).moveByOffset(0, adjustmentOffSet).perform();
            } else {
                actions.clickAndHold(option).moveByOffset(adjustmentOffSet, 0).perform();
            }
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
            actions.release(destination).build().perform();
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
            LOGGER.info("moveToElement completed");
        }
    }

    /**
     * verifies if the My store zipcode on homepage matches the zipcode on the treadwell model or
     * checks the zipcode on the treadwell model with the zipcode on the treadwell details section on plp
     *
     * @param zip  zipcode from my store or the treadwell model
     * @param page treadwell modal/plp
     */

    public void assertPrimaryDrivingLocation(String zip, String page) {
        LOGGER.info("assertPrimaryDrivingLocation started");
        driver.waitForMilliseconds();
        String displayedZipcode;
        if (page.contains(ConstantsDtc.TREADWELL)) {
            displayedZipcode = webDriver.findElement(treadwellZipcodeBy).getAttribute(Constants.VALUE);
        } else {
            displayedZipcode = webDriver.findElement(ProductListPage.treadwellDetailsSectionBy).getText();
        }
        Assert.assertTrue("FAIL: The zipcode displayed for primary driving location: " + displayedZipcode
                + " did not match to expected zipcode: " + zip, displayedZipcode.contains(zip));
        LOGGER.info("assertPrimaryDrivingLocation completed");
    }

    /**
     * clicks on the get started button of the treadwell model on the homepage
     */
    public void clickOnGetStarted() {
        LOGGER.info("clickOnGetStarted started");
        driver.waitForElementVisible(treadwellHomePage);
        webDriver.findElement(getStartedBy).click();
        LOGGER.info("clickOnGetStarted completed");
    }

    /**
     * Drag & Drops the Driving Priority to specified position
     *
     * @param optionName Driving Priority option name
     * @param order      Position where option will be set to
     */
    public void moveDrivingPriorityOptionTo(String optionName, String order) {
        LOGGER.info("moveDrivingPriorityOptionTo started");

        int xOffset = 0;
        int position = Integer.valueOf(order);
        driver.waitForElementVisible(drivingPriorityOptionsBy);
        Actions slide = new Actions(webDriver);
        List<WebElement> options = webDriver.findElements(drivingPriorityOptionsBy);
        for (WebElement option : options) {
            WebElement optionElement = option.findElement(drivingPriorityOptionsNameBy);
            WebElement currentOrder = option.findElement(drivingPriorityOptionsOrderBy);
            if (optionElement.getText().contains(optionName)) {
                driver.jsScrollToElement(optionElement);
                int elementCurrentPosition = Integer.valueOf(currentOrder.getText());
                if (elementCurrentPosition == position) {
                    break;
                } else {
                    if (elementCurrentPosition < position) {
                        xOffset = rightPositionXOffsetValue;
                        position = position - elementCurrentPosition;
                    } else {
                        xOffset = leftPositionXOffsetValue;
                        position = 4 - position;
                    }
                    for (int i = 1; i <= position; i++) {
                        try {
                            driver.waitForMilliseconds(Constants.TWO);
                            Action action = slide.dragAndDropBy(option, xOffset, 0).build();
                            action.perform();
                            driver.waitForMilliseconds();
                        } catch (Exception e) {
                            driver.waitForMilliseconds(Constants.TWO);
                            Action action = slide.dragAndDropBy(option, xOffset, 0).build();
                            action.perform();
                        }
                    }
                }
                break;
            }
        }
        LOGGER.info("moveDrivingPriorityOptionTo completed");
    }

    /**
     * Clicks the edit icon on the Treadwell modal , PLP and brand page
     *
     * @param page The page that the Edit Icon will be clicked on
     */
    public void clickEditIcon(String page) {
        LOGGER.info("clickEditIcon started for " + page);
        driver.waitForElementClickable(editOptionIcon);
        editOptionIcon.click();
        LOGGER.info("clickEditIcon completed for " + page);
    }

    /**
     * Set the scenario data array for driving priorities shown on the treadwell modal
     */
    public void setDrivingPrioritiesScenarioDataFromTreadwellModal() {
        LOGGER.info("setDrivingPrioritiesScenarioDataFromTreadwellModal started");
        List<String> drivingPriorityList = new ArrayList<>();
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> drivingPriorities = webDriver.findElements(drivingPriorityOptionsBy);
        driver.resetImplicitWaitToDefault();
        for (WebElement drivingPriorityEle : drivingPriorities) {
            drivingPriorityList.add(drivingPriorityEle.getText().split("\n")[1]);
        }
        driver.scenarioData.setDrivingPriorityList(drivingPriorityList);
        LOGGER.info("setDrivingPrioritiesScenarioDataFromTreadwellModal completed");
    }
}