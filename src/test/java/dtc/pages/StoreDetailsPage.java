package dtc.pages;

import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by ericseverson on 10/10/16.
 */
public class StoreDetailsPage {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(StoreDetailsPage.class.getName());

    public StoreDetailsPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(name = "isAppointment")
    public static WebElement scheduleAppointmentButton;

    @FindBy(css = "[class*='store-quick-view__store-name']")
    public static WebElement storeTitle;

    @FindBy(css = "[class*='store-quick-view__address']")
    public static WebElement storeAddress;

    @FindBy(css = ".store-hours")
    public static WebElement storeHours;

    @FindBy(className = "store-details-title")
    public static WebElement storeDetailTitle;

    @FindBy(css = "[class*='dt-carousel__next']")
    public static WebElement storeCarouselButton;

    @FindBy(css = "[class*='dt-carousel__indicator__']")
    public static WebElement storeCarouselIndicator;

    @FindBy(css = "[class*='copy-link-input']")
    public static WebElement copyLinkInput;

    @FindBy(css = "[class*='copy-link-icon'] path")
    public static WebElement copyLinkIcon;

    @FindBy(id = "mobile-phone-number")
    public static WebElement mobilePhoneNumber;

    @FindBy(css = "[class*='store-quick-view__store-hours']")
    public static WebElement storeQuickViewStoreHours;

    public static final By tabDefaultBy = By.cssSelector("[class*='tab__default']");
    public static final By shareStoreBy = By.cssSelector("[href*='share/store']");
    public static final By copyLinkMessageBy = By.cssSelector("[class*='copy-link-message']");
    public static final By sendButtonBy = By.className("send-button");
    public static final By actionsBy = By.cssSelector("button[class*='alt-actions__alt-button']");
    public static final By storeQuickViewToggleBy = By.cssSelector("[class*='store-quick-view-list__toggle']");
    public static final By nearestStoreQuickViewListBy =
            By.cssSelector("[class*='store-quick-view-list__list-item-collapsible']");
    public static final By nearestStoreMilesBy =
            By.cssSelector("[class*='nearest-stores__stores'] [class*='store-quick-view__distance']");
    public static final By storeQuickViewExpandedBy =
            By.cssSelector("[class*='store-quick-view-list__toggle-expanded']");

    /**
     * Verifies the current store details in the header
     *
     * @param title   Store title
     * @param address Store address
     */
    public void verifyMyStoreDetails(String title, String address) {
        LOGGER.info("verifyMyStoreDetails started");
        driver.waitForElementClickable(CommonActions.dtButtonLgPrimary);
        Assert.assertTrue("FAIL: Store in header: \"" + storeTitle.getText() + "\" did NOT contain title \""
                        + title + "\"!",
                storeTitle.getText().trim().contains(title));
        Assert.assertTrue("FAIL: The store's address: \"" + storeAddress.getText()
                        + "\" did NOT contain expected address: \"" + address + "\"!",
                storeAddress.getText().trim().contains(address));
        LOGGER.info("verifyMyStoreDetails completed");
    }

    /**
     * Clicks the "Schedule appointment" button on the Store Details page
     *
     * @param page Store Details/Store Locator page
     */
    public void clickScheduleAppointment(String page) {
        LOGGER.info("clickScheduleAppointment started for " + page);
        driver.waitForPageToLoad();
        WebElement scheduleAppt = driver.
                getElementWithText(CommonActions.dtButtonLgPrimary, ConstantsDtc.SCHEDULE_APPOINTMENT);
        driver.waitForElementClickable(scheduleAppt);
        driver.jsScrollToElement(scheduleAppt);
        scheduleAppt.click();
        LOGGER.info("clickScheduleAppointment completed for " + page);
    }

    /**
     * Clicks the Store Image carousel/Store Services on the Store Details page
     *
     * @param text store image carousel indicator/button, Store Services
     */
    public void selectOnStoreDetails(String text) {
        LOGGER.info("selectOnStoreDetails started for " + text);
        WebElement element;
        if (text.equalsIgnoreCase(ConstantsDtc.STORE_SERVICES)) {
            element = driver.getElementWithText(tabDefaultBy, text);
        } else if (text.contains(ConstantsDtc.CAROUSEL_BUTTON)) {
            element = storeCarouselButton;
        } else {
            //Covered for store image carousel indicator
            element = storeCarouselIndicator;
        }
        driver.waitForElementClickable(element);
        element.click();
        LOGGER.info("selectOnStoreDetails completed for " + text);
    }

    /**
     * Select link/copy link icon on share Store Details popup page
     *
     * @param option link/copy link icon
     * @param text store/installer
     */
    public void selectOnShareStoreDetailsPopup(String option, String text) {
        LOGGER.info("selectOnShareStoreDetailsPopup started for " + option + " for " + text);
        WebElement copyLink;
        if (option.contains(ConstantsDtc.ICON)) {
            copyLink = copyLinkIcon;
        } else {
            copyLink = copyLinkInput;
        }
        copyLink.click();
        driver.waitForMilliseconds();
        LOGGER.info("selectOnShareStoreDetailsPopup completed for " + option + " for " + text);
    }

    /***
     * Verifies the displayed text with text copied to my clipboard
     *
     * @param textType  Store Details Link
     */
    public void assertTextWithClipboardText(String textType) {
        LOGGER.info("assertTextWithClipboardText started for " + textType);
        String data = null;
        try {
            data = (String) Toolkit.getDefaultToolkit()
                    .getSystemClipboard().getData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException ex) {
            LOGGER.info("Text not copied to clipboard, threw an error: " + ex);
        } catch (IOException ex) {
            LOGGER.info("Expected " + textType + " link not copied to clipboard" + ex);
        }
        Assert.assertEquals("FAIL: The copied text to clipboard didn't match!",
                copyLinkInput.getAttribute(Constants.VALUE), data);
        LOGGER.info("assertTextWithClipboardText completed for " + textType);
    }

    /***
     * Verifies MORE STORES NEAR THIS STORE displays with 3 nearest store result
     */
    public void assertNearestStoresDisplayed() {
        LOGGER.info("assertNearestStoresDisplayed started");
        driver.waitForElementVisible(
                driver.getElementWithText(CommonActions.headerThirdBy, ConstantsDtc.MORE_STORES_NEAR_THIS_STORE));
        List<WebElement> nearestStoresMiles = webDriver.findElements(nearestStoreMilesBy);
        Assert.assertTrue("FAIL: Three nearest stores not displayed! ",
                webDriver.findElements(nearestStoreQuickViewListBy).size() == 3);
        for (WebElement mile : nearestStoresMiles) {
            Assert.assertTrue("FAIL: Three nearest store miles not displayed! ",
                    mile.getText().contains(ConstantsDtc.MILES_ABBREV));
        }
        LOGGER.info("assertNearestStoresDisplayed completed");
    }

    /**
     * Select nearest store quick view toggle button on Store Details page
     *
     * @param select expand/collapse
     * @param number number 1/number 2/number 3 nearest store
     */
    public void selectNearestStoreQuickViewToggleButton(String select, Integer number) {
        LOGGER.info("selectNearestStoreQuickViewToggleButton started for " + select
                + " option for store number " + number);
        List<WebElement> nearestStores = webDriver.findElements(nearestStoreQuickViewListBy);
        driver.jsScrollToElementClick(nearestStores.get(number - 1).findElement(storeQuickViewToggleBy));
        LOGGER.info("selectNearestStoreQuickViewToggleButton completed for " + select
                + " option for store number " + number);
    }

    /***
     * Verifies nearest store result is expanded/collapsed
     *
     * @param storeNumber nearest store number
     * @param option expanded/collapsed
     */
    public void assertNearestStoreResultExpansion(Integer storeNumber, String option) {
        LOGGER.info("assertNearestStoreResultExpansion started for " + option + " for store number " + storeNumber);
        WebElement storeExpansionView;
        if (option.toLowerCase().contains(Constants.EXPAND)) {
            storeExpansionView = webDriver.findElements(nearestStoreQuickViewListBy).get(storeNumber - 1).
                    findElement(storeQuickViewExpandedBy);
        } else {
            storeExpansionView = webDriver.findElements(nearestStoreQuickViewListBy).get(storeNumber - 1).
                    findElement(storeQuickViewToggleBy);
        }
        Assert.assertTrue("FAIL: The nearest store " + storeNumber + " is not " + option,
                driver.isElementDisplayed(storeExpansionView));
        LOGGER.info("assertNearestStoreResultExpansion completed for " + option + " for store number " + storeNumber);
    }
}