package dtc.pages;

import com.google.common.collect.Ordering;
import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import utilities.CommonUtils;
import utilities.Driver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static dtc.pages.TireAndWheelPackagesPage.tireWheelPackageDetails;

/**
 * Created by aaronbriel on 9/22/16.
 */
public class ProductListPage {

    private Driver driver;
    private WebDriver webDriver;
    private final CommonActions commonActions;
    private final CommonUtils commonUtils;
    private static final Logger LOGGER = Logger.getLogger(ProductListPage.class.getName());
    public static String itemID;

    public ProductListPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        commonUtils = new CommonUtils();
        PageFactory.initElements(webDriver, this);
    }

    private boolean showFilterBtnClicked = false;

    private int totalSelectedItems = 0;
    private static final Integer ZERO = 0;
    private static final Integer ONE_HUNDRED_THOUSAND = 100000;
    private static final String PRICE_LOW_TO_HIGH = "Price (Low to High)";
    private static final String PRICE_HIGH_TO_LOW = "Price (High to Low)";
    private static final String OUR_RECOMMENDATION = "Our Recommendation";
    private static final String ORIGINAL_EQUIPMENT = "Original Equipment";
    private static final String HIGHEST_RATED = "Highest Rated";
    private static final String NAME_ASC = "Name (Ascending)";
    private static final String NAME_DESC = "Name (Descending)";

    private static final String TOTAL_RESULTS = "Total Results";
    private static final String E = "e";
    public static final String ASCENDING = "Ascending";

    private static final String QUANTITY_ERROR = "Please enter a quantity between 1 and 99";
    public static final String NO_REVIEWS_YET = "No reviews yet";
    private static final String MULTIPLE = "multiple";
    private static final String THOUSAND_MILES_SUFFIX = ",000";
    private static String filterFontSize = " ";

    public static final String[] PDL_EVERYDAY_PRIORITY_ORDER = {
            "Stopping Distance", "Life of Tire", "Handling", "Comfort & Noise"};

    public static final String[] PDL_PERFORMANCE_PRIORITY_ORDER = {
            "Handling", "Stopping Distance", "Comfort & Noise", "Life of Tire"};
    private static final String[] STAGGERED_PLP_TAB = {"SETS", "FRONT", "REAR"};
    private static HashMap<String, String> productPriceOnPlp = new HashMap<>();
    private static final String UNSURE_ITEM_WILL_FIT_MSG = "We're not sure if these items will fit.";
    private static final String TELL_US_ABOUT_VEHICLE_MSG = "Tell us about your vehicle to get the correct items.";
    private ArrayList<String> sortOptionsList = new ArrayList<>(Arrays.asList(ConstantsDtc.BEST_SELLER,
            PRICE_LOW_TO_HIGH, PRICE_HIGH_TO_LOW, HIGHEST_RATED));

    public static final String SETS = "SETS";
    private static final String compare = "Compare";
    private static final String iconOpenClass = "--open";
    private static final String PRICE_RANGE = "Price Range";
    private static final String SEARCH_BRAND_MESSAGING = "Search Brand...";
    private static final String COMPARE_ELEMENT = "Compare";
    private static final String SUB_TOTAl_WITH_TOOL_TIP = "Sub Total with Tool Tip";
    private static final String NEED_IT_NOW_WITH_TOOL_TIP = "Need It Now with Tool Tip";
    public static final String TOOL_TIP = "Tool Tip";
    public static final String CHECK_NEARBY_STORES_BANNER = "Check nearby Stores Banner";
    public static final String PROMOTIONS = "Promotions";
    private static final String BEST_BETTER_GOOD_BANNER = "Best, Better, Good";
    public static final String TOOL_TIP_TIRE_MESSAGE = "We can usually get these tires even sooner via transfer from a " +
            "different store or nearby warehouse.";
    public static final String TOOL_TIP_WHEEL_MESSAGE = "We can usually get these wheels even sooner via transfer from a " +
            "different store or nearby warehouse.";
    public static final String SHOPPING_CART_PAGE = "Shopping Cart page";
    public static final String DRIVING_DETAILS = "driving details";
    public static final String FREE_SHIPPING = "Free Shipping";
    private static final String DOLLAR = "DOLLAR";
    public static final String PRODUCT_LIST_IMAGE_MISSING = "product-list-image__missing";
    private static final String NO_RESULTS_DISPLAY_MESSAGE_VEHICLE_INSESSION = "Your search did not match any products that fit your vehicle, but we may still be able to help. Please give us a call at ";
    private static final String NO_RESULTS_DISPLAY_MESSAGE_NO_VEHICLE_INSESSION = "Your search did not match any products, but we may still be able to help. Please give us a call at ";
    private static final String NO_RESULTS_DISPLAY_MESSAGE_2 = " and we can review some more options.";
    private static final String NO_VEHICLE_INSESSION = "no vehicle in session";
    private static final String VEHICLE_INSESSION = "vehicle in session";
    private static final String NO_RESULTS = "no results";
    public  static final String NOT_AVAILABLE = "NOT AVAILABLE";
    private static final String BUTTON_UNAVAILABLE = "button-unavailable";

    public static final By resultsOptionLinkBy = By.xpath("//a[contains(@class,'listing-tabs__tab')]");
    public static final String MILE_WARRANTY = "Mile Warranty";

    public static final String VEHICLE_ON_SESSION = "Vehicle on session";

    public static final String VEHICLE_NOT_ON_SESSION = "Vehicle Not on session";

    private static String PRODUCT_DETAILS_TEXT = "View product details for exact price & inventory";

    private static String ENTER_VEHICLE_FOR_EXACT_PRICE = "Enter vehicle for exact price & inventory";

    private static String VIEW_PRODUCT_DETAILS_FOR_EXACT_PRICE = "View product details for exact price & inventory";

    public static final By plpResultsRowBy = By.cssSelector("[class*='product-listing-page__list-item']");

    private static final By plpResultsRowMobileBy = By.className("product-list__info");

    private static final By compareCheckboxBy = By.className("dt-checkbox");

    private static final By compareButtonActiveBy = By.xpath("//a[contains(@class,'compare-selector__comparable')]");

    private static final By compareButtonTagBy = By.xpath("//a[contains(@class, 'compare-selector__button')]");

    private static final By compareSelectorValueBy =
            By.xpath("//div[contains(@class,'compare-selector__compare-selector')]");

    public static final By brandNameBy = By.cssSelector("small[class*='product__brand']");

    public static final By productNameBy = By.cssSelector("h4[class*='product__product-name']");

    private static final By searchRefinementFilterBy = By.cssSelector("[class*='filter-facet__button']");

    public static final By dropDownOptionBy = By.cssSelector("[class*='dropdown-menu']");

    private static final By specialOrderAvailabilityMobileBy = By.className("product-list__item-availability");

    public static final By searchFilterSectionBy = By.className("product-list-filter__filter-category");

    private static final By openFilterSectionIconBy = By.className("product-list-filter__toggle");

    private static final By filterOptionBy = By.cssSelector("label[class*='filter-option__label']");

    public static final By addToCartMobileBy = By.className("js-add-to-cart-button");

    public static final By resultsOptionLinkMobileBy = By.className("dt-tabs__label");

    private static final By resultsMessageBy = By.className("product-list__total-results");

    public static final By productImageBy = By.cssSelector("a[class*='product-list-image__image-link']");

    public static final By compareProductImageBy = By.className("compare-main-section__image");

    public static final By compareSetProductImageBy = By.cssSelector("[class*='compare-main-section__columns'] [class*='compare-main-section__image']");

    public static final By compareProductMobileImageBy = By.cssSelector("[class*='mobile'] .auto-compare-main-section-image");

    public static final By compareSetProductMobileImageBy = By.cssSelector("[class*='mobile'] [class*='compare-main-section__columns'] [class*='compare-main-section__image']");

    private static final By addToCartErrorMessageBy = By.xpath("//div[contains(@class,'add-to-cart__error')] | //div[contains(@class,'itemized-product__error')]");

    private static final By promotionDiscountMobileBy = By.className("product-promo__message");

    private static final By compareTireReviewsBy = By.cssSelector("[class*='rating-overview__compare-reviews']");

    private static final By resultsRowRatingBy = By.cssSelector("[class*='rating-overview__rating-overview']");

    public static final By productListingBy = By.cssSelector("[class*='product-listing-page__product-list']");

    private static final By milesPerYearBy = By.className("product-list__pdl-details-value--green");

    private static final By milesPerYearPDPBy = By.cssSelector("[id*='miles-driven-input']");

    private static final By ourRecommendationBannerBy = By.cssSelector("div[class*='desktop'] h5[class*='our-recommendation']");

    private static final By pdlDrivingPriorityOptionNameBy = By.cssSelector("[class*='product-listing-page__inner'] ol[class*='product-list__pdl-priorities']  [class*='display-inline-block-lg']");

    private static final By pdlEditDrivingDetailsBy = By.cssSelector("[class*='product-listing-page__inner'] .product-list__pdl-edit");

    private static final By tireSizeBy = By.cssSelector("[class*='product__size']");

    public static final By staggeredOptionTabBy = By.cssSelector("a[class*='listing-tabs__tab']");

    private static final By staggeredSizeBy = By.cssSelector("[class*='listing-tabs__value']");

    private static final By productListOEBy = By.cssSelector("[class*='banner__gray']");

    private static final By filterOptionLabelBy = By.cssSelector("[class*='filter-option__label']");

    private static final By compareButtonBy = By.cssSelector("[class*='compare-selector__comparable']");

    public static final By currentPriceRangeBy = By.className("price-range-filter__price-range");

    private static final By top3TilesBy = By.cssSelector("[class*='product-listing-page__top-section'] [class*='top-three__inner-tile']");

    private static final By top3TilesrecommendationBy = By.cssSelector("[class*='product-listing-page__top-section'] [class*='top-three__title']");

    private static final By top3TilesMobileBy = By.cssSelector("[class*='top-three-mobile'] [class*='top-three__title']");

    public static final By top3TreadwellRecommendationTop = By.cssSelector("[class*='top-three__our-treadwell-recommendation-top']");

    private static final By top3TilesBrandBy = By.cssSelector("[class*='product-listing-page__top-section'] [class*='top-three__tile-link']");

    public static final By top3titleLinkBy = By.cssSelector("[class*='top-three__tile-link']");

    public static final By top3ComponentBy = By.cssSelector("[class*='__top-section'] [class*='top-three__container']");

    public static final By top3ComponentMobileBy = By.cssSelector("[class*='top-three__container']" +
            "[class*='top-three-mobile']");

    public static final By resultsBarCountBy = By.xpath("//span[contains(@class,'results-bar__count')]");

    private static final By plpCurrentResultsTabBy = By.cssSelector("[class*='listing-tabs__selected']");

    public static final By plpItemNumberBy = By.cssSelector("[class*='product__subtext-line']");

    private static final By checkInventoryBy = By.cssSelector("[class*='product-locator__near-by-link']");

    public static final By toolTipNeedItNowBy = By.cssSelector("div[class*='need-it-now__need-it-now___']");

    public static final By toolTipBy = By.cssSelector("button[class*='tooltip-toggle icon-info need-it-now__tooltip-toggle___']");

    public static final By promotionsBy = By.cssSelector("a[class*='promotions-overview__promotion-link___']");

    public static final By foundItLowerBy = By.cssSelector("a[class*='price__lower-price___']");

    public static final By shoppingCartBy = By.cssSelector("h1[class*='page-title']");

    public static final By mileWarranty = By.xpath("//span[contains(text(),'mile warranty')]");

    public static final By bestBetterGoodBy = By.cssSelector("div[class*='product-list-image__image-block___']");

    public static final By itemNumber = By.xpath("//span[contains(text(),'Item#')]");

    private static final By availableTodayBy = By.xpath("//span[contains(text(),'Available today')]");

    public static final By toolTipRowBy = By.cssSelector("ul[class*='product__icon-list']");

    public static final By toolTipIconBy = By.cssSelector("li[class*='product__icon-list-item']");

    public static final By subTotalPrice = By.cssSelector("[class*='subtotal__price']");

    public static final By drivingDetailsContainer = By.cssSelector("div[class*='product-driving-details__container']");

    public static final By productRowBlockBy = By.cssSelector("div[class*='itemized-product__row']");

    public static final By topRecommendationBy = By.cssSelector("div[class*='desktop'] div[class*='our-treadwell-recommendation'] ");

    public static final By treadwellDetailsSectionBy = By.cssSelector("[class*='product-listing-page__inner'] [class*='location'] [class*='details-value']");

    public static final By checkBoxInputMobileBy = By.cssSelector("[class*='mobile'] [class*='dt-checkbox__input']");

    public static final By errorMessageBy = By.cssSelector("[class*='fitment-error__inner'] div");

    public static final By treadwellTireLifeAndCostBy = By.cssSelector("div[class='pdl-data__tl']");

    public static final By drivingDetailsContainerBy = By.cssSelector("[class*='product-driving-details__container']");

    public static final By staggerdInventoryMessageBy = By.cssSelector("span[class*='product-locator__bold']");

    public static final By priceAndInventoryCanonicalBy = By.cssSelector("[class*='product__enter-vehicle___']");

    public static final By viewDetailsButtonBy = By.linkText("VIEW DETAILS");

    public static final By productBlockBy = By.cssSelector("[class*='product__product-block']");

    @FindBy(className = "js-add-to-cart-button")
    public static WebElement addToCartMobile;

    @FindBy(className = "no-results__description")
    public static WebElement noResultsInfoMessage;

    @FindBy(css = "a[class*='results-bar__filter-button']")
    public static WebElement openFilterSectionBtn;

    @FindBy(className = "product-list__details")
    public static WebElement productListDetails;

    @FindBy(className = "results__showfilter")
    public static WebElement showFilterBtn;

    @FindBy(xpath = "//div[@class='my-vehicles__selected-vehicle-description']/span")
    public static WebElement vehicleSelectedDescription;

    @FindBy(xpath = "(//label[contains(text(),'Bolt Pattern')])[1]")
    public static WebElement boltPatternFacet;

    @FindBy(xpath = "//div[contains(@class,'results-bar__count-line')]")
    public static WebElement plpResultsHeader;

    @FindBy(className = "product-list-filter__option--brand-filter")
    public static WebElement brandFilterSearchBar;

    @FindBy(css = "div[class*='desktop'] h5[class*='highest-rated']")
    public static WebElement pdlHighestRatedContainer;

    @FindBy(css = "div[class*='desktop'] h5[class*='best-seller']")
    public static WebElement pdlBestSellerContainer;

    @FindBy(css = "div[class*='desktop'] h5[class*='original-equipment']")
    public static WebElement pdlOriginalequipmemtContainer;

    @FindBy(css = "[class*='product-listing-page__container'] div:nth-child(3) h5[class*='product-list__pdl-heading']")
    private static WebElement drivingDetailsBlockHeader;

    @FindBy(className = "fitment-pdl-entry__cancel")
    private static WebElement fitmentPdlEntryCancelButton;

    @FindBy(linkText = "VIEW DETAILS")
    private static WebElement viewDetailsButton;

    @FindBy(xpath = "//span[contains(@class,'subtotal__label')]//button")
    public static WebElement subTotalTooltip;

    @FindBy(css = "[class*='cart-item__details']")
    private static WebElement cartDetails;

    @FindBy(css = "[class*='package-details__selected-package']")
    private static WebElement packageDetails;

    @FindBy(css = "[class*='product-listing-page__inner'] [class*='product-listing-page__driving-details']")
    public static WebElement treadwellDetailsBox;

    @FindBy(css = "[class*='product-listing-page__inner'] .product-list__pdl-heading")
    public static WebElement treadwellLogoDetailsBox;

    @FindBy(css = "[class*='filter-option__option'] [class*='treadwellLogo']")
    public static WebElement treadwellLogo;

    @FindBy(css = "[class*='mobile'] [class*='treadwellLogo']")
    public static WebElement treadwellLogoMobile;

    @FindBy(linkText = "Enter vehicle")
    public static WebElement enterVehicleCanonicalProductLink;

    @FindBy(css = "[class*='results-bar__results-bar']")
    public static WebElement resultsBar;

    @FindBy(css = "[class*='results-bar__staggered-size-line']")
    public static WebElement staggeredSizeLine;

    @FindBy(css = "[class*='results-bar__size-label']")
    public static WebElement frontTireSizeLine;

    @FindBy(css = "[class*='-rear-size-tires'] [class*='results-bar__size-label']")
    public static WebElement rearTireSizeLine;

    @FindBy(css = "[class*='fitment-banner__tire-front'] span:nth-child(1)")
    public static WebElement factoryFrontTire;

    @FindBy(css = "[class*='fitment-banner__size-info-rear'] [class*='fitment-banner__size']")
    public static WebElement factoryRearTire;

    /**
     * Returns the total number of selected items
     *
     * @return int      the total number of selected items
     */
    public int getTotalSelectedItems() {
        return totalSelectedItems;
    }

    /**
     * Sets the total number of selected items
     *
     * @param totItems the total number of selected items
     */
    public void setTotalSelectedItems(int totItems) {
        totalSelectedItems = totItems;
    }

    /**
     * Selects options from fitment search results to add to cart
     *
     * @param option        The option in results to pick from (Ex. SETS, FRONT, REAR)
     * @param itemId        The itemid to identify which addToCart button to click
     * @param inStock       Criteria to select an item based on inStock availability set to ("true" or "false" or "none")
     * @param action        The action to take in the item added popup
     * @param imageRequired true or false whether the product row must have a valid image to be selected
     */
    public void addToCart(String option, String itemId, String inStock, String action, boolean imageRequired) {
        LOGGER.info("addToCart started with '" + option + "' option, '" + itemId + "' item ID, and '" + action
                + "' action");
        commonActions.waitForSpinner();
        driver.jsScrollToElementClick(commonActions.getTargetAddToCartButton(ConstantsDtc.ADD_TO_CART, option, itemId,
                inStock, imageRequired));
        if (!action.toLowerCase().contains(Constants.NONE.toLowerCase()))
            commonActions.selectActionOnAddToCartModal(action);
        if (driver.isElementDisplayed(ProductDetailPage.backToResults)) {
            driver.webElementClick(ProductDetailPage.backToResults);
            driver.waitOneSecond();
        }
        LOGGER.info("addToCart completed with '" + option + "' option, '" + itemId + "' item ID, and '" + action
                + "' action");
    }

    /**
     * Clicks the Check Availability button for the first item that has the link and
     * fails if no items are found
     */
    public void clickCheckInventoryForFirstAvailableItem() {
        LOGGER.info("clickCheckInventoryForFirstAvailableItem started");
        driver.waitForPageToLoad();
        WebElement checkInventory = webDriver.findElements(checkInventoryBy).get(0);
        driver.jsScrollToElementClick(checkInventory);
        LOGGER.info("clickCheckInventoryForFirstAvailableItem completed");
    }

    /**
     * Verifies the value is contained within a result row on search results
     *
     * @param results String to verify
     */
    public void assertSearchResults(String results) {
        LOGGER.info("assertSearchResults started");
        if (Config.isMobile()) {
            Assert.assertTrue("FAIL: The current page of search results did NOT contain a single instance " +
                    "of: \"" + results + "\"!", driver.waitForTextPresent(CommonActions.resultsRowMobileBy, results,
                    Constants.THIRTY));
        } else {
            Assert.assertTrue("FAIL: The current page of search results did NOT contain a single instance " +
                    "of: \"" + results + "\"!", driver.waitForTextPresent(plpResultsRowBy, results,
                    Constants.THIRTY));
        }
        LOGGER.info("Confirmed that '" + results + "' was listed in the fitment search results.");
        LOGGER.info("assertSearchResults completed");
    }

    /**
     * Cycles through results for unchecked items
     * Checks the checkboxes of each item as well as adds their values to a list
     *
     * @param quantity Number of checkboxes to be selected on page
     */
    public void clickCompareCheckboxes(int quantity) {
        LOGGER.info("clickCompareCheckboxes started");
        int i;
        boolean itemsFound = false;
        List<WebElement> rows = new ArrayList<>();

        driver.waitForPageToLoad();

        driver.waitForElementVisible(CommonActions.addToCartOrPackage);

        // check this condition just in case there are already enough selected items when entering the function
        if (totalSelectedItems >= quantity) {
            itemsFound = true;
        }

        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);

        // Traverse the pages (clicking next) if necessary to find the specified quantity of valid items for comparison.
        while (!itemsFound) {
            //TODO: Retest once latest safaridriver or geckodriver is updated & stabilized
            if (Config.isSafari() || Config.isFirefox() || Config.isIe()) {
                driver.waitForPageToLoad();
            }

            rows.clear();
            rows = webDriver.findElements(plpResultsRowBy);
            totalSelectedItems = 0;
            for (WebElement row : rows) {
                if (row.findElement(CommonActions.checkBoxBy).isSelected()) {
                    totalSelectedItems++;
                }
            }

            i = 0;
            for (WebElement row : rows) {
                driver.setImplicitWait(Constants.TWO, TimeUnit.SECONDS);
                List<WebElement> compareButtonValueList = row.findElements(compareSelectorValueBy);
                driver.resetImplicitWaitToDefault();
                if (compareButtonValueList.size() > 0) {
                    if (row.getText().contains(compare)) {
                        if (!row.findElement(CommonActions.checkboxInputBy).isSelected()) {
                            int counter = 0;
                            selectDeselectCompareProductsCheckbox(row, Constants.SELECT);
                            commonActions.appendToOrRemoveFromProductInfoList(row, i, false, Constants.APPEND);
                            totalSelectedItems++;
                            verifyCompareButtonUpdated(row, i);
                        }
                    }

                    i++;
                    if (quantity == totalSelectedItems) {
                        LOGGER.info(quantity + " products selected for comparison");
                        itemsFound = true;
                        String productDetails = row.getText().split("#")[1].trim();
                        itemID = productDetails.split("\\r?\\n")[0].trim();
                        LOGGER.info("Storing last compare ItemID is " + itemID);
                        break;
                    }
                } else {
                    i++;
                }
            }

	        /*
               Click Next Page if less than specified quantity of valid items have been found.
	           navToDifferentPageOfResults returns boolean for Next button exists.  False indicates final page.
	        */
            if (!itemsFound && commonActions.navToDifferentPageOfResults(Constants.NEXT)) {
                LOGGER.info("Navigating to next page.");
            } else if (!itemsFound) {
                Assert.fail("FAIL: There were not enough non-special-order items to do a comparison");
                break;
            }

        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("clickCompareCheckboxes completed");
    }

    /**
     * Cycles through results for unchecked items - Mobile
     * Checks the checkboxes of each item as well as adds their values to a list
     *
     * @param quantity Number of checkboxes to be selected on page
     */
    public void clickCompareCheckboxesMobile(int quantity) {
        LOGGER.info("clickCompareCheckboxesMobile started");

        //TODO: retest when new safaridriver or geckodriver is updated and stabilized
        if (Config.isSafari() || Config.isFirefox() || Config.isIe())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        driver.waitForPageToLoad();
        commonActions.checkForNoResultsMessage();
        driver.waitForElementVisible(addToCartMobile);

        int currentPageSelectedItemCount = 0;

        List<WebElement> rows = webDriver.findElements(CommonActions.resultsRowMobileBy);

        for (WebElement row : rows) {
            if (row.findElement(CommonActions.checkBoxBy).isSelected()) {
                currentPageSelectedItemCount++;
            }
        }

        if (currentPageSelectedItemCount != 0) {
            totalSelectedItems = currentPageSelectedItemCount;
        }

        int i = 0;
        for (WebElement row : rows) {
            if (!row.findElement(specialOrderAvailabilityMobileBy).getText().contains(ConstantsDtc.SPECIAL_ORDER)) {

                if (!row.findElement(CommonActions.checkBoxBy).isSelected()) {
                    try {
                        WebElement checkBox = row.findElement(CommonActions.checkBoxBy);
                        driver.webElementClick(checkBox);
                    } catch (StaleElementReferenceException e) {
                        LOGGER.info(Config.getBrowser() + " threw an error: " + e);
                        webDriver.findElements(CommonActions.checkBoxBy).get(i).click();
                    }
                    commonActions.appendToOrRemoveFromProductInfoList(row, i, false, Constants.APPEND);
                    totalSelectedItems++;
                }
                i++;

                if (i == quantity && totalSelectedItems > 1) {
                    break;
                }

                if (quantity == totalSelectedItems) {
                    break;
                }
            }
        }
        LOGGER.info("clickCompareCheckboxes completed");
    }

    /**
     * Clicks the Compare button on the last checked product
     */
    public void clickCompareProductsButton() {
        LOGGER.info("clickCompareProductsButton started");
        driver.waitForMilliseconds();
        driver.waitForElementVisible(webDriver.findElement(plpResultsRowBy));
        List<WebElement> rows = webDriver.findElements(plpResultsRowBy);
        boolean buttonClicked = false;

        for (WebElement row : rows) {
            //TODO CCL - needed in cases where we have wheels and tires on PLP; because wheels cannot be compared
            //TODO CCL (cont) they do not have a compare button which breaks our previous assumptions for PLP
            driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
            List<WebElement> compareButtonTagList = row.findElements(compareButtonActiveBy);
            driver.resetImplicitWaitToDefault();
            if (compareButtonTagList.size() > 0) {
                if (row.findElement(compareButtonActiveBy).getText().equalsIgnoreCase(compare)) {
                    driver.jsScrollToElementClick(row.findElement(compareButtonActiveBy));
                    buttonClicked = true;
                    break;
                }
            }
        }
        Assert.assertTrue("FAIL: Unable to click compare button", buttonClicked);
        LOGGER.info("clickCompareProductsButton completed");
    }

    /**
     * Sets the sort dropdown box displays passed in value
     *
     * @param value Value to set in the drop down list box value
     */
    public void setSortByValue(String value) {
        LOGGER.info("setSortByValue started");
        int counter = 0;
        while (driver.getElementWithText(CommonActions.reactSelectizePlaceholderBy, value) == null && counter < Constants.FIVE) {
            WebElement dropDown = null;
            if (value.toLowerCase().contains(Constants.DATE.toLowerCase())) {
                dropDown = webDriver.findElement(CommonActions.sortByOptionBy);
            } else {
                List<WebElement> dropDownElements = webDriver.findElements(CommonActions.reactSelectizePlaceholderBy);
                for (WebElement dropDownElement : dropDownElements) {
                    if (driver.isElementDisplayed(dropDownElement, Constants.ZERO)) {
                        dropDown = dropDownElement;
                        break;
                    }
                }
            }
            Assert.assertNotNull("FAIL: Sort drop down list box was not displayed on PLP page", dropDown);
            driver.jsScrollToElementClick(dropDown);
            WebElement dropDownOptionList = webDriver.findElement(dropDownOptionBy);
            do {
                driver.waitForMilliseconds(Constants.ONE_HUNDRED);
            } while (!driver.isElementDisplayed(dropDownOptionList, Constants.ZERO));

            //If no underlying "class" tags are found, try finding "option" tags
            List<WebElement> options = dropDownOptionList.findElements(By.xpath(".//div//div"));
            if (options.size() == 0) {
                options = dropDown.findElements(CommonActions.optionTagBy);
            }

            for (WebElement option : options) {
                if (option.getText().toLowerCase().contains(value.toLowerCase())) {
                    driver.webElementClick(option);
                    break;
                }
            }
            driver.waitForPageToLoad();
            counter++;
        }
        LOGGER.info("setSortByValue completed");
    }

    /**
     * Verifies the sort dropdown box displays passed in value
     *
     * @param expectedValue Value to verify in sortBy
     */
    public void verifySortByValue(String expectedValue) {
        LOGGER.info("verifySortByValue started for '" + expectedValue + "'");
        driver.waitForElementVisible(CommonActions.reactSelectizePlaceholderBy);
        WebElement dropDown = driver.getElementWithText(CommonActions.reactSelectizePlaceholderBy, expectedValue);
        Assert.assertNotNull("FAIL: SortBy filter is displayed", dropDown);
        String actualValue = dropDown.getText().trim();
        Assert.assertTrue("FAIL: Product page sort by dropdown was NOT set to expected value: '" + expectedValue +
                "'. Actual value: '" + actualValue + "'", actualValue.equalsIgnoreCase(expectedValue));
        LOGGER.info("verifySortByValue completed. Confirmed sort drop down was set to '" + expectedValue + "'");
    }

    /**
     * Verifies how many filters are checked on the product results page
     *
     * @param number Expected number of filters to be checked
     */
    public void verifyNumberOfSearchRefinementFilters(String number) {
        LOGGER.info("verifyNumberOfSearchRefinementFilters started");
        int numberOfFilters = webDriver.findElements(searchRefinementFilterBy).size();
        Assert.assertEquals("FAIL: The number of search refinement filters was NOT \"" + number + "\"!",
                numberOfFilters, Integer.parseInt(number));
        LOGGER.info("Confirmed that the number of search refinement filters was set to \"" + number + "\".");
        LOGGER.info("verifyNumberOfSearchRefinementFilters completed");
    }

    /***
     * Verifies the Refinements section contains specified value(s)
     *
     * @param multiple "single" or "multiple" values to verify
     * @param values String representing the value(s) to be verified in the refinement section. Multiple values
     *                need to be separated by a comma
     */
    public void verifySearchRefinementFilterValues(String multiple, String values) {
        LOGGER.info("verifySearchRefinementFilterValues started & is looking for value(s):\n\t\t" + values);
        List<String> valuesToVerifyList = new LinkedList<>(Arrays.asList(values.split(",")));
        int valuesToFind = valuesToVerifyList.size();
        int foundValuesCount = 0;
        boolean allValuesFound = false;
        List<String> valuesFound = new ArrayList<>();
        List<WebElement> searchRefinementFilterElements;

        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);

        if (Config.isMobile() && !driver.isElementDisplayed(CommonActions.clearAllFiltersLink)) {
            driver.jsScrollToElementClick(openFilterSectionBtn);
            driver.waitForElementClickable(CommonActions.clearAllFiltersLink);
        }

        if (multiple.equalsIgnoreCase(MULTIPLE))
            values = values.replaceAll(",\\s+", ",");

        if (Config.isFirefox())
            driver.waitForMilliseconds();

        driver.waitForElementVisible(searchFilterSectionBy);
        searchRefinementFilterElements = driver.getDisplayedElementsList(searchRefinementFilterBy);

        for (String value : valuesToVerifyList) {
            for (WebElement searchRefinementFilterElement : searchRefinementFilterElements) {
                String searchText = searchRefinementFilterElement.getText();
                if (searchText.toLowerCase().contains(value.toLowerCase())) {
                    foundValuesCount++;
                    valuesFound.add(value);
                    break;
                } else {
                    if (CommonUtils.containsIgnoreCase(searchText, PRICE_RANGE)) {
                        searchText = searchText.replace(PRICE_RANGE, "").trim();
                        int minSearchValue = commonActions.cleanMonetaryStringToInt(searchText.split(" - ")[0]);
                        int maxSearchValue = commonActions.cleanMonetaryStringToInt(searchText.split(" - ")[1]);
                        int minValue = commonActions.cleanMonetaryStringToInt(value.split(" - ")[0]);
                        int maxValue = commonActions.cleanMonetaryStringToInt(value.split(" - ")[1]);

                        if (Math.abs(minValue - minSearchValue) <= 2 && Math.abs(maxValue - maxSearchValue) <= 2) {
                            foundValuesCount++;
                            valuesFound.add(value);
                            break;
                        }
                    }
                }

                if (searchRefinementFilterElement.getText().toLowerCase().contains(PRICE_RANGE.toLowerCase())) {
                    String approxPriceRange = "$" + CommonActions.minValueApproximate + " - $" +
                            CommonActions.maxValueApproximate;
                    if (searchRefinementFilterElement.getText().toLowerCase().contains(approxPriceRange)) {
                        foundValuesCount++;
                        valuesFound.add(approxPriceRange);
                        LOGGER.info("An approximate price range (instead of the exact: \"" + value + "\") was"
                                + " used to validate the \"Price Range\" refinement filter was set!");
                        break;
                    }
                }

                //TODO CCL - remove when "Price Range" filter works as intended for Safari
                if (Config.isSafari() && value.contains("$")) {
                    foundValuesCount++;
                    valuesFound.add("Skipped \"Price Range\" filter validation for Safari!");
                    break;
                }
            }
        }

        if (foundValuesCount == valuesToFind) {
            allValuesFound = true;
        } else {
            valuesToVerifyList.removeAll(valuesFound);
        }

        Assert.assertTrue("FAIL: The following value(s) was/were NOT located as search refinement "
                + "filter(s): \"" + valuesToVerifyList + "\"!", allValuesFound);

        if (Config.isMobile()) {
            driver.jsScrollToElement(CommonActions.applyFiltersButton);
            CommonActions.applyFiltersButton.click();
        }
        LOGGER.info("Confirmed the expected value(s): \n\t\t" + values
                + "\n\t was/were displayed as search refinement filter(s).");
    }

    /**
     * Removes the first selected item from Brand, Product and Price lists
     * Used when removing the first item from a compare products results page
     */
    public void removeFirstSelectedItem() {
        LOGGER.info("removeFirstSelectedItem started");
        totalSelectedItems--;
        LOGGER.info("removeFirstSelectedItem completed");
    }

    /**
     * Extracts all prices on page and verifies they are in ascending/descending order
     *
     * @param order String (Ascending or Descending)
     */
    public void verifyPricesInOrder(String order) {
        LOGGER.info("verifyPricesInOrder started with prices expected to be in '" + order + "' order");
        double low = ZERO;
        double high = ONE_HUNDRED_THOUSAND;

        driver.waitForMilliseconds();
        if (Config.isFirefox())
            driver.waitForPageToLoad();

        for (int i = 0; i < 5; i++) {
            if (!driver.isElementDisplayed(CommonActions.productPriceBy)) {
                if (commonActions.navToDifferentPageOfResults(Constants.NEXT))
                    LOGGER.info("Navigating to next page of results as the current page is not displaying prices "
                            + "for products");
            } else {
                break;
            }

            if (i == 4)
                Assert.fail("FAIL: No prices were displayed for products on the first 5 pages of results!");
        }

        commonActions.checkForNoResultsMessage();
        List<WebElement> prices = webDriver.findElements(CommonActions.productPriceBy);
        for (WebElement price : prices) {
            String cost = price.getText().substring(1);
            String[] output = cost.split("-");
            if (order.equals(ASCENDING) && output.length == 2) {
                cost = output[0];
            } else if (order.equals(ConstantsDtc.DESCENDING) && output.length == 2) {
                cost = output[1];
            }
            cost = cost.split(E)[0];
            cost = cost.replace("$", "");
            double value = Double.valueOf(cost);
            if (order.equalsIgnoreCase(ASCENDING)) {
                if (value < low)
                    Assert.fail("FAIL: Price list NOT sorted in ascending order at '" + price + "'!");
                low = value;
            } else if (order.equalsIgnoreCase(ConstantsDtc.DESCENDING)) {
                if (value > high)
                    Assert.fail("FAIL: Price list NOT sorted in descending order at '" + price + "'!");
                high = value;
            } else {
                Assert.fail("FAIL: Order parameter should either be Ascending or Descending but was: '"
                        + order + "'!");
            }
        }
        LOGGER.info("verifyPricesInOrder successful");
    }

    /***
     * Expands or Collapses the filter section specified. If action is set to open, method will check the section to see
     * if it is already "open" or "expanded" and skip the action if this is the case.
     *
     * @param action Expand or collapse the filter section
     * @param filterSection String representing the name of the filter section to expand
     */
    public boolean expandCollapseFilterSection(String action, String filterSection) {
        LOGGER.info("expandCollapseFilterSection started. " + action + " the '" + filterSection + "' filter section");
        if (Config.isFirefox())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        if (driver.isElementNotVisible(CommonActions.productListFilter, Constants.ONE))
            return false;
        WebElement filterSectionExpandIcon = getFilterSectionExpandCollapseIcon(filterSection);
        Boolean filterExpanded = filterSectionExpandIcon.getAttribute(Constants.CLASSNAME).contains(iconOpenClass);
        if (!filterExpanded && action.equalsIgnoreCase(Constants.EXPAND)
                || filterExpanded && action.equalsIgnoreCase(Constants.COLLAPSE))
            driver.moveToElementClick(filterSectionExpandIcon);
        driver.waitOneSecond();
        LOGGER.info("expandCollapseFilterSection completed. " + action + " the '" + filterSection + "' filter section");
        return true;
    }

    /***
     * Selects refinement filter options from a specified filter section.
     * NOTE: If the section contains a "Show More" link, it will be clicked before every filter option is selected
     * to avoid issue if an option to be selected is being hidden.
     *
     * @param multiple "single" or "multiple" values to verify
     * @param filterSection String representing the name of the filter section from which to select options
     * @param options String representing the option(s) to be selected from the filter section. Multiple values
     *                need to be separated by a comma
     */
    public void selectFromFilterSection(String multiple, String filterSection, String options) {
        LOGGER.info("selectFromFilterSection started for section: \n\t\t" + filterSection
                + "\n With option(s): \n\t\t" + options);
        commonActions.checkForNoResultsMessage();

        if (Config.isMobile())
            openFilterSectionBtn.click();

        List<String> optionsToSelectList = Collections.singletonList(options);

        if (multiple.equalsIgnoreCase(MULTIPLE))
            optionsToSelectList = Arrays.asList(options.split(","));

        if (Config.isSafari()) {
            if (!showFilterBtnClicked) {
                if (driver.isElementDisplayed(showFilterBtn)) {
                    showFilterBtn.click();
                    showFilterBtnClicked = true;
                }
            }
        }

        if (!expandCollapseFilterSection(Constants.EXPAND, filterSection)) {
            LOGGER.info("There are no filters to expand. This is probably a staggered fitment.");
            return;
        }
        selectShowMoreOrLessForFilterSection(ConstantsDtc.SHOW_MORE, filterSection);

        for (String optionToSelect : optionsToSelectList) {
            WebElement filterOption = getFilterOptionElement(optionToSelect);

            if (Config.isSafari())
                filterOption = filterOption.findElement(CommonActions.checkBoxBy);

            driver.jsScrollToElementClick(filterOption, false);
            driver.waitForElementVisible(searchFilterSectionBy);
        }

        if (Config.isMobile()) {
            CommonActions.applyFiltersButton.click();
            driver.waitForMilliseconds();
        }
        driver.waitForPageToLoad();

        LOGGER.info("selectFromFilterSection completed for section: \n\t\t" + filterSection
                + "\n With option(s): \n\t\t" + options);
    }

    /**
     * Returns the displayed filter option with matching text
     *
     * @param optionText Text of the option to be selected
     * @return WebElement of the displayed filter option with matching text
     */
    private WebElement getFilterOptionElement(String optionText) {
        LOGGER.info("getFilterOptionElement started for filter option: '" + optionText + "'");
        List<WebElement> possibleFilterOptionEleList = driver.getElementsWithText(filterOptionLabelBy,
                optionText.trim());
        LOGGER.info("getFilterOptionElement completed for filter option: '" + optionText + "'");
        WebElement returnElement = driver.getDisplayedElement(possibleFilterOptionEleList, Constants.ZERO);
        Assert.assertNotNull("FAIL: The '" + optionText + "' option was not present on the facet filters",
                returnElement);
        return returnElement;
    }

    /**
     * Returns the displayed element for a filter section specified by its text content
     *
     * @param filterSection String of the filter section to retrieve
     * @return WebElement of the displayed filter section
     */
    private WebElement getFilterSectionElement(String filterSection) {
        LOGGER.info("getFilterSectionElement started for filter section: '" + filterSection + "'");
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> possibleFilterSectionEleList = driver.getElementsWithText(searchFilterSectionBy,
                filterSection);
        LOGGER.info("getFilterSectionElement completed for filter section: '" + filterSection + "'");
        WebElement firstDisplayedFilterSection = driver.getDisplayedElement(possibleFilterSectionEleList, Constants.ZERO);
        driver.resetImplicitWaitToDefault();
        return firstDisplayedFilterSection;
    }

    /**
     * Selects / clicks the "Show More" or "Show Less" link for a filter section if the link is present
     *
     * @param showLinkType  "Show More" or "Show Less" link
     * @param filterSection Text of the filter section to be checked for a "Show More" or "Show Less" link
     */
    public void selectShowMoreOrLessForFilterSection(String showLinkType, String filterSection) {
        LOGGER.info("selectShowMoreOrLessForFilterSection started");
        WebElement showLink = getShowMoreOrLessLinkForFilterSection(showLinkType, filterSection);

        if (showLink != null) {
            driver.jsScrollToElement(showLink);
            showLink.click();
            driver.waitForMilliseconds();
        } else {
            LOGGER.info("'" + showLinkType + "' link was NOT found for '" + filterSection + "' section! Skipping "
                    + "selection");
        }
        LOGGER.info("selectShowMoreOrLessForFilterSection completed");
    }

    /**
     * Returns the "Show More" or "Show Less" link (if it exists) for a specified filter section on the PLP page
     *
     * @param showLinkType  "Show More" or "Show Less" link
     * @param filterSection Text of the filter section to be checked for a "Show More" or "Show Less" link
     * @return "Show More" or "Show Less" link for a filter section if it exists, else method returns null
     */
    private WebElement getShowMoreOrLessLinkForFilterSection(String showLinkType, String filterSection) {
        LOGGER.info("getShowMoreOrLessLinkForFilterSection started with '" + showLinkType + "' link for '"
                + filterSection + "' section");
        WebElement filterSectionElement = getFilterSectionElement(filterSection);
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> sectionLinkList = filterSectionElement.findElements(CommonActions.dtLinkBy);
        driver.resetImplicitWaitToDefault();

        if (sectionLinkList.size() > 0) {
            for (WebElement sectionLink : sectionLinkList) {
                if (sectionLink.getText().equalsIgnoreCase(showLinkType)) {
                    LOGGER.info("getShowMoreOrLessLinkForFilterSection completed - Found '" + showLinkType
                            + "' link for '" + filterSection + "' section!");
                    return sectionLink;
                }
            }
        }

        LOGGER.info("getShowMoreOrLessLinkForFilterSection completed - Could NOT find '" + showLinkType
                + "' link for '" + filterSection + "' section! Returning value == null!");
        return null;
    }

    /***
     * Clicks the "CLEAR ALL" link - removes all currently active search refinement filters
     */
    public void clearAllSearchFilters() {
        LOGGER.info("clearAllSearchFilters started");
        if (Config.isMobile()) {
            openFilterSectionBtn.click();
            CommonActions.clearAllFiltersLink.click();
        } else {
            if (!driver.isElementDisplayed(searchRefinementFilterBy, Constants.ONE)) {
                webDriver.navigate().refresh();
                return;
            }
            int appliedFiltersStartCount;
            List<WebElement> currentAppliedFiltersList = driver.getDisplayedElementsList(searchRefinementFilterBy);
            appliedFiltersStartCount = currentAppliedFiltersList.size();

            for (int i = 0; i < appliedFiltersStartCount; i++) {
                currentAppliedFiltersList.clear();
                currentAppliedFiltersList = driver.getDisplayedElementsList(searchRefinementFilterBy);
                currentAppliedFiltersList.get(0).click();
                driver.waitForMilliseconds();
                driver.waitForElementVisible(searchFilterSectionBy);
            }
            driver.resetImplicitWaitToDefault();
        }
        LOGGER.info("clearAllSearchFilters completed");
    }

    /***
     * Verifies no search refinement filters are currently active/displayed on the product results
     */
    public void verifyNoSearchRefinementsAreApplied() {
        LOGGER.info("verifyNoSearchRefinementsAreApplied started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: A search refinement filter was still being applied to the product results!",
                !driver.isElementDisplayed(searchRefinementFilterBy, Constants.TWO));
        LOGGER.info("verifyNoSearchRefinementsAreApplied completed - " +
                "No refinement filters were active on the product results");
    }

    /***
     * Verifies the tire sizes appearing on the Product List Page match the expected value(s). Work with either one or
     * two tire sizes. For two tire sizes, method will validate that the correct tire sizes appear for each of the
     * potential tabs: SET, FRONT, REAR.
     * @param tireSizes String representing the tire size(s) to validate. Can handle up to 2 tire sizes, specified by a
     *                  comma between the sizes
     */
    public void verifyProductsMatchTireSize(String tireSizes) {
        LOGGER.info("verifyProductsMatchTireSize started with tire size(s): " + tireSizes);
        List<String> tireSizesList = new ArrayList<String>();

        Matcher m = Pattern.compile("[0-9]{3}\\s[\\\\/][0-9]{2}\\sR[0-9]{2}").matcher(tireSizes);

        while (m.find()) {
            tireSizesList.add(m.group());
        }

        commonActions.checkForNoResultsMessage();

        if (tireSizesList.size() == 2) {
            LOGGER.info("Verifying tire sizes match the values on the SET, FRONT, and REAR tabs");

            //Validate SETS tab
            List<WebElement> setTabResultRowsList = webDriver.findElements(plpResultsRowBy);
            for (WebElement setTabResultRow : setTabResultRowsList) {
                Assert.assertTrue("A row on the SET tab does not contain a tire size matching either "
                                + tireSizesList.get(0) + " or " + tireSizesList.get(1),
                        setTabResultRow.getText().toLowerCase().contains(
                                tireSizesList.get(0).toLowerCase())
                                || setTabResultRow.getText().toLowerCase().contains(
                                tireSizesList.get(1).toLowerCase()));
            }

            //Validate FRONT tab
            driver.clickElementByPartialText(ConstantsDtc.FRONT);
            commonActions.checkForNoResultsMessage();
            verifyFirstPageOfResultsContainExpected(tireSizesList.get(0));
            LOGGER.info("Front tire size: " + tireSizesList.get(0)
                    + " matched the values displayed on the FRONT tab");

            //Validate REAR tab
            driver.clickElementByPartialText(ConstantsDtc.REAR);
            commonActions.checkForNoResultsMessage();
            verifyFirstPageOfResultsContainExpected(tireSizesList.get(1));
            LOGGER.info("Rear tire size: " + tireSizesList.get(1)
                    + " matched the values displayed on the REAR tab");

        } else if (tireSizesList.size() == 1) {
            verifyFirstPageOfResultsContainExpected(tireSizesList.get(0));
            LOGGER.info("Tire size: " + tireSizesList.get(0)
                    + " matched the values on the Product List Page search results");
        } else {
            Assert.fail("Fail - either NO tire sizes OR more than 2 tire sizes were passed into this method!");
        }
        LOGGER.info("verifyProductsMatchTireSize completed successfully with tire size(s): " + tireSizes);
    }

    /**
     * Verifies each result row on the current search expectedText page contains the expected value
     *
     * @param expectedText String to verify
     */
    private void verifyFirstPageOfResultsContainExpected(String expectedText) {
        LOGGER.info("verifyFirstPageOfResultsContainExpected started");
        commonActions.checkForNoResultsMessage();

        List<WebElement> searchResultRowsList = commonActions.getPLPResultTopElements(null);
        List<Integer> rowsWithoutDataList = new ArrayList<>();

        for (int i = 0; i < searchResultRowsList.size(); i++) {
            String searchResultRowText = searchResultRowsList.get(i).getText();

            if (Config.isSafari())
                searchResultRowText = searchResultRowText.trim().replaceAll(" +", " ");

            if (!searchResultRowText.toLowerCase().contains(expectedText.toLowerCase()))
                rowsWithoutDataList.add(i + 1);
        }

        Assert.assertTrue("Search result row #'s" + rowsWithoutDataList
                + " did not contain: \"" + expectedText + "\"", rowsWithoutDataList.size() == 0);
        LOGGER.info("verifyFirstPageOfResultsContainExpected completed - all result rows on the current page"
                + " contained " + expectedText);
    }

    /**
     * Click on the first product found that has an Add to Cart or View Details button to go to the PDP page
     */
    public void clickFirstAvailableProductOnPlp() {
        LOGGER.info("clickFirstAvailableProductOnPlp started");
        if (driver.isElementDisplayed(ProductDetailPage.backToResults, Constants.TWO)) {
            driver.webElementClick(ProductDetailPage.backToResults);
            driver.waitOneSecond();
        }
        WebElement plpRow = findFirstPlpRowWithAddToCart();
        WebElement productName = plpRow.findElement(productNameBy);
        boolean viewDetails = CommonUtils.containsIgnoreCase(plpRow.getText(), ConstantsDtc.VIEW_DETAILS);
        driver.jsScrollToElementClick(productName);
        driver.waitForPageToLoad();
        commonActions.waitForSpinner();
        if (viewDetails)
            commonActions.selectAllFirstAvailableSizeOptionsOnPdpPage();
        LOGGER.info("clickFirstAvailableProductOnPlp completed");
    }


    /**
     * Click on the first product found that has an Add to Cart or View Details button and has a
     * "View on my vehicle" link and go to go to the PDP page
     */
    public void clickFirstAvailableProductOnPlpWithViewOnMyVehicleLink() {
        LOGGER.info("clickFirstAvailableProductOnPlpWithViewOnMyVehicleLink started");
        List<WebElement> plpRows = webDriver.findElements(plpResultsRowBy);
        WebElement productName = null;
        boolean viewDetails = false;
        boolean addToCart = false;
        boolean itemFound = false;
        while (!itemFound) {
            for (WebElement plpRow : plpRows) {
                String rowText = plpRow.getText();
                viewDetails = CommonUtils.containsIgnoreCase(rowText, ConstantsDtc.VIEW_DETAILS);
                addToCart = CommonUtils.containsIgnoreCase(rowText, ConstantsDtc.ADD_TO_CART);
                if (viewDetails || addToCart) {
                    if (CommonUtils.containsIgnoreCase(rowText, ConstantsDtc.VIEW_ON_MY_VEHICLE)) {
                        productName = plpRow.findElement(productNameBy);
                        itemFound = true;
                        break;
                    }
                }
            }
            if (!itemFound && !commonActions.navToDifferentPageOfResults(Constants.NEXT)) {
                Assert.fail("FAIL: Unable to find PLP row with 'View on my vehicle' link");
            }
        }

        driver.jsScrollToElementClick(productName);
        driver.waitForPageToLoad();
        commonActions.waitForSpinner();
        if (viewDetails)
            commonActions.selectAllFirstAvailableSizeOptionsOnPdpPage();
        LOGGER.info("clickFirstAvailableProductOnPlpWithViewOnMyVehicleLink completed");
    }

    /**
     * Find the first row with an Add To Cart or View Details button
     */
    public WebElement findFirstPlpRowWithAddToCart() {
        LOGGER.info("findFirstAvailableProductOnPlp started");
        WebElement returnRow = null;
        driver.waitForPageToLoad();
        List<WebElement> rows = webDriver.findElements(plpResultsRowBy);
        while (returnRow == null) {
            for (WebElement row : rows) {
                try {
                    WebElement button = row.findElement(CommonActions.addToCartByClassBy);
                    String buttonText = button.getText();
                    if (buttonText.equalsIgnoreCase(ConstantsDtc.ADD_TO_CART) ||
                            buttonText.equalsIgnoreCase(ConstantsDtc.VIEW_DETAILS)) {
                        returnRow = row;
                        break;
                    }
                } catch (Exception e) {
                    try {
                        WebElement button = row.findElement(viewDetailsButtonBy);
                        returnRow = row;
                        break;
                    } catch (Exception ex) {
                        continue;
                    }
                }
            }
            if (returnRow == null) {
                if (!commonActions.navToDifferentPageOfResults(Constants.NEXT)) {
                    Assert.fail("FAIL: Could find an Add to Cart or View Details button in the Fitment Results");
                    break;
                }
            }
        }
        LOGGER.info("findFirstAvailableProductOnPlp completed");
        return returnRow;
    }

    /**
     * Verifies the Quick Filter Checkbox is not selected by default
     *
     * @param checkboxLabel String to verify
     */
    public void assertCheckboxDeselectedOrSelected(String checkboxLabel, String option) {
        LOGGER.info("assertCheckboxDeselectedOrSelected started");
        driver.waitForPageToLoad();
        commonActions.checkForNoResultsMessage();
        List<WebElement> checkboxes;

        if (Config.isMobile() && driver.isElementDisplayed(openFilterSectionBtn)
                && !driver.isElementDisplayed(CommonActions.closeIcon)) {
            openFilterSectionBtn.click();
            checkboxes = webDriver.findElements(checkBoxInputMobileBy);
        } else {
            checkboxes = webDriver.findElements(CommonActions.checkboxInputBy);
        }
        driver.waitForElementVisible(searchFilterSectionBy);
        boolean isFilterChecked;
        boolean found = false;

        if (option.equalsIgnoreCase(ConstantsDtc.SELECTED)) {
            for (WebElement checkbox : checkboxes) {
                isFilterChecked = checkbox.isSelected();
                if (isFilterChecked) {
                    if (checkbox.getAttribute(Constants.VALUE).equals(ConstantsDtc.quickFilters.get(checkboxLabel))) {
                        found = true;
                        break;
                    }
                }
            }
        } else {
            for (WebElement checkbox : checkboxes) {
                isFilterChecked = checkbox.isSelected();
                if (!isFilterChecked) {
                    if (checkbox.getAttribute(Constants.VALUE).equals(ConstantsDtc.quickFilters.get(checkboxLabel)) ||
                            (checkbox.getText().equalsIgnoreCase(ConstantsDtc.quickFilters.get(checkboxLabel)))) {
                        found = true;
                        break;
                    }
                }
            }
        }
        Assert.assertTrue("FAIL: " + checkboxLabel + "option not " + option, found);
        if (Config.isMobile() && driver.isElementDisplayed(CommonActions.closeIcon)) {
            CommonActions.closeIcon.click();
        }
        LOGGER.info("assertCheckboxDeselectedOrSelected completed");
    }

    /**
     * Clicks the specified filter checkbox to select or deselect it. IF method is called to deselect a filter
     * it checks to make sure the checkbox has already been selected
     *
     * @param checkboxLabel String of checkbox label
     * @param deselect      True to deselect a filter, False to select a filter
     */
    public void clickPlpCheckboxToSelectDeselectFilter(String checkboxLabel, boolean deselect) {
        LOGGER.info("clickPlpCheckboxToSelectDeselectFilter started");
        driver.waitForPageToLoad();

        if (Config.isMobile())
            openFilterSectionBtn.click();

        WebElement filterCheckbox = getPlpFilterCheckbox(checkboxLabel);
        driver.jsScrollToElement(filterCheckbox);

        if (deselect)
            verifyPlpCheckboxFilterSelected(checkboxLabel);

        filterCheckbox.click();

        if (Config.isMobile()) {
            driver.jsScrollToElement(CommonActions.applyFiltersButton);
            CommonActions.applyFiltersButton.click();

            if (driver.isElementDisplayed(CommonActions.dtModalCloseBy))
                commonActions.closeModalWindow();
        }
        LOGGER.info("clickPlpCheckboxToSelectDeselectFilter completed");
    }

    /**
     * Returns the displayed PLP filter checkbox if it contains the specified string of text
     *
     * @param checkboxText Text the filter checkbox label should contain
     * @return The displayed PLP filter checkbox element
     */
    private WebElement getPlpFilterCheckbox(String checkboxText) {
        LOGGER.info("getPlpFilterCheckbox started for checkbox '" + checkboxText + "'");
        WebElement filterCheckbox = driver.getElementWithText(filterOptionBy, checkboxText);
        Assert.assertNotNull("FAIL: Could NOT find the '" + checkboxText + "' checkbox!", filterCheckbox);
        LOGGER.info("getPlpFilterCheckbox completed for checkbox '" + checkboxText + "'");
        return filterCheckbox;
    }

    /**
     * Verifies the checkbox with specified label has been selected
     *
     * @param checkboxLabel String of the checkbox label
     */
    public void verifyPlpCheckboxFilterSelected(String checkboxLabel) {
        LOGGER.info("verifyPlpCheckboxFilterSelected started");
        driver.waitForPageToLoad();
        WebElement filterCheckbox = getPlpFilterCheckbox(checkboxLabel).findElement(CommonActions.checkboxInputBy);
        commonActions.verifyCheckboxSelected(filterCheckbox, checkboxLabel);
        LOGGER.info("verifyPlpCheckboxFilterSelected completed");
    }

    /**
     * Asserts total results message for mobile validation, as normal "Results for Wheels/Tires"
     * message does not display in search results for mobile
     */
    public void assertResultsMessage() {
        LOGGER.info("assertResultsMessage started");
        driver.waitForPageToLoad();
        int time = Constants.THIRTY;
        boolean foundHeader = driver.waitForTextPresent(resultsMessageBy, TOTAL_RESULTS, time);
        Assert.assertTrue("FAIL: Results message \"" + TOTAL_RESULTS + "\" was NOT displayed in \"" + time
                + "\" seconds!", foundHeader);
        LOGGER.info("Confirmed that the results message was displayed.");
        LOGGER.info("assertResultsMessage completed");
    }

    /**
     * Verifies the "Compare" button updates appropriately when the containing row / item is selected for comparison.
     *
     * @param row   - WebElement; row that should have been selected for comparison and contains the "Compare" button
     * @param index - Int; used in Safari to by pass Stale Element Exception if thrown.
     */
    private void verifyCompareButtonUpdated(WebElement row, int index) {
        LOGGER.info("verifyCompareButtonUpdated started");
        boolean checkBoxSelected = false;
        int counter = 0;
        do {
            checkBoxSelected = row.findElement(CommonActions.checkboxInputBy).isSelected();
            if (!checkBoxSelected)
                driver.waitOneSecond();
            counter++;
        } while (counter < Constants.TEN && !checkBoxSelected);
        Assert.assertTrue("FAIL: Attempting to verify an update to the compare button of a row that has " +
                "NOT been selected", checkBoxSelected);
        String compareMessageText;
        if (totalSelectedItems == 1) {
            try {
                compareMessageText = row.findElement(compareSelectorValueBy).getText();
                Assert.assertTrue("FAIL: 'Compare' messaging for this row (and the only selected item)"
                        + " should read: '" + ConstantsDtc.COMPARE_ITEM_MESSAGE + "' but was actually: '"
                        + compareMessageText + "'!", compareMessageText.contains(ConstantsDtc.COMPARE_ITEM_MESSAGE));
            } catch (StaleElementReferenceException stale) {
                LOGGER.info(Config.getBrowser() + " threw an error: " + stale);
                compareMessageText = webDriver.findElements(compareSelectorValueBy).get(index).getText().trim();
                Assert.assertTrue("FAIL: 'Compare' messaging for this row (and the only selected item)"
                        + " should read: '" + ConstantsDtc.COMPARE_ITEM_MESSAGE + "' but was actually: '"
                        + compareMessageText + "'!", compareMessageText.contains(ConstantsDtc.COMPARE_ITEM_MESSAGE));
            }
        } else if (totalSelectedItems <= 3) {
            try {
                compareMessageText = row.findElement(compareButtonActiveBy).getText().trim();
                Assert.assertTrue("FAIL: 'Compare' button for this row (one of multiple selected items)"
                                + "should read: '" + compare + "' but was actually: '" + compareMessageText + "'",
                        compareMessageText.equalsIgnoreCase(compare));
            } catch (StaleElementReferenceException stale) {
                LOGGER.info(Config.getBrowser() + " threw an error: " + stale);
                compareMessageText = row.findElement(compareButtonActiveBy).getText().trim();
                Assert.assertTrue("FAIL: 'Compare' button for this row (one of multiple selected items)"
                                + "should read: '" + compare + "' but was actually: '" + compareMessageText + "'",
                        compareMessageText.equalsIgnoreCase(compare));
            }
        } else {
            Assert.fail("FAIL: Between 1 and 3 items at most can be compared. The total selected item count of '"
                    + totalSelectedItems + "' is outside of those bounds!");
        }
        LOGGER.info("verifyCompareButtonUpdated completed successfully");
    }

    /**
     * Verifies that the expected options are contained in the Sort By dropdown
     */
    private void verifySortByOptions() {
        LOGGER.info("verifySortByOptions started");
        WebElement dropdown = driver.getDisplayedElement(CommonActions.reactSelectizePlaceholderBy);

        dropdown.click();
        dropdown = driver.getDisplayedElement(CommonActions.reactDropdownMenuBy);
        for (String option : sortOptionsList) {
            Assert.assertTrue("FAIL: Excepted option: \""
                    + option + "\" was not in the \"Sort By\" dropdown", dropdown.getText().contains(option));
        }
        LOGGER.info("verifySortByOptions completed");
    }

    /***
     * Verifies the specified section of the PLP UI is displayed to the user
     * @param sectionToVerify Section of the UI on PLP to verify e.g. basic controls, banner without a vehicle,
     *                       filter / sorting, OR pagination
     */
    public void verifyPlpUiSection(String sectionToVerify) {
        LOGGER.info("verifyPlpUiSection started for section: '" + sectionToVerify + "'");
        String BASIC_CONTROLS = "basic controls";
        String BANNER_WITHOUT_VEHICLE = "banner without vehicle";
        String SORTING_OPTIONS = "sorting options";
        String PAGINATION = "pagination";

        driver.waitForPageToLoad();
        commonActions.checkForNoResultsMessage();

        if (sectionToVerify.equalsIgnoreCase(BASIC_CONTROLS)) {
            List<By> controlsByList = new ArrayList<>();

            controlsByList.add(CommonActions.addToCartByClassBy);
            controlsByList.add(CommonActions.checkBoxBy);

            for (By control : controlsByList) {
                List<WebElement> controlsDisplayList;
                controlsDisplayList = webDriver.findElements(control);
                Assert.assertTrue("FAIL: At least one of the basic PLP controls "
                                + "(Add to Cart or Compare checkbox) was NOT displayed!",
                        controlsDisplayList.size() > 0);
            }
        } else if (sectionToVerify.equalsIgnoreCase(BANNER_WITHOUT_VEHICLE)) {
            commonActions.assertBannerColor(ConstantsDtc.PLP, Constants.YELLOW);
            commonActions.assertResultsMessageContains(ConstantsDtc.PLP, UNSURE_ITEM_WILL_FIT_MSG);
            commonActions.assertResultsMessageContains(ConstantsDtc.PLP, TELL_US_ABOUT_VEHICLE_MSG);
        } else if (sectionToVerify.equalsIgnoreCase(SORTING_OPTIONS)) {
            verifySortByOptions();
        } else if (sectionToVerify.equalsIgnoreCase(PAGINATION)) {
            commonActions.validatePagination(ConstantsDtc.PLP);
        } else {
            Assert.fail("FAIL: unrecognized PLP aspect to verify - '" + sectionToVerify
                    + "'! Please check for typos, or update verification method");
        }
    }

    /**
     * Asserts the text passed in appears in the product list details
     *
     * @param text Text to verify in product list details
     */
    public void assertTextInProductListDetails(String text) {
        LOGGER.info("assertTextInProductListDetails started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListDetails);
        Assert.assertTrue("FAIL: The expected text: \"" + text
                        + "\" does NOT match the actual text: \"" + productListDetails.getText() + "\"!",
                productListDetails.getText().contains(text));
        LOGGER.info("Confirmed that \"" + text + "\" was listed in the Product List Page.");
        LOGGER.info("assertTextInProductListDetails completed");
    }

    /***
     * Verifies the current product list items are sorted properly by name
     *
     * @param order - String representing the sort order expected (ASCENDING or DESCENDING)
     */
    public void verifyNamesInOrder(String order) {
        LOGGER.info("verifyNamesInOrder started");
        List<WebElement> nameEleList;
        List<String> nameStringList = new ArrayList<>();
        driver.waitForPageToLoad();
        commonActions.checkForNoResultsMessage();

        driver.waitForElementVisible(productNameBy);
        nameEleList = webDriver.findElements(productNameBy);

        for (WebElement nameEle : nameEleList) {
            nameStringList.add(nameEle.getText().split("\n")[1]);
        }

        if (order.equalsIgnoreCase(ASCENDING)) {
            Assert.assertTrue("FAIL: The product list is NOT sorted by NAME in ASCENDING order. "
                            + "Results in order: \"" + nameStringList + "\"",
                    Ordering.from(String.CASE_INSENSITIVE_ORDER).isOrdered(nameStringList));
        } else {
            List<String> reversedNameList = nameStringList;
            Collections.sort(reversedNameList, Ordering.natural().reverse());
            Assert.assertTrue("FAIL: The product list is NOT sorted by NAME in DESCENDING order. "
                    + "Results in order: \"" + nameStringList + "\"", nameStringList.equals(reversedNameList));
        }
        LOGGER.info("verifyNamesInOrder completed");
    }

    /***
     * Updates the row quantity box with provided integer
     *
     * @param quantity Number that
     */
    public void updateFirstRowItemQuantity(int quantity) {
        LOGGER.info("updateRowItemQuantity started");
        try {
            WebElement quantityField = CommonActions.productQuantityBox;
            if (quantity > Constants.NEGATIVE_ONE && quantity < Constants.ONE_HUNDRED) {
                driver.jsScrollToElement(quantityField);
                quantityField.click();
                commonActions.clearAndPopulateEditField(quantityField, quantity);
            }
        } catch (Exception e) {
            Assert.fail("FAIL: Entered quantity could not be converted to an integer!");
        }
        LOGGER.info("updateRowItemQuantity completed successfully");
    }

    /***
     * Verifies the (first row for PLP) 'ADD TO CART' button is clickable and red
     */
    public void verifyAddToCartButtonProperties(String page) {
        LOGGER.info("verifyAddToCartButtonProperties started for '" + page + "'");
        String color;
        driver.waitForElementClickable(CommonActions.addToCart);
        color = CommonActions.addToCart.getCssValue(Constants.BACKGROUND_COLOR);
        Assert.assertTrue("FAIL: The '" + page + "' 'Add To Cart' button color was not Red (rgb: '"
                + Constants.RED_COLOR_RGB + "' OR rgba: '" + Constants.RED_COLOR_RGBA
                + "') but was actually " + color + "!", color.equals(Constants.RED_COLOR_RGB) ||
                color.equals(Constants.RED_COLOR_RGBA));
        LOGGER.info("verifyAddToCartButtonProperties completed successfully for '" + page + "'");
    }

    /***
     *  Method to verify the error message on the quantity box on the PLP page
     *
     */
    public void verifyProductQuantityErrorMessage() {
        LOGGER.info("verifyProductQuantityErrorMessage started");
        driver.waitForElementVisible(addToCartErrorMessageBy);
        WebElement errorMessage = driver.getDisplayedElement(addToCartErrorMessageBy);
        Assert.assertNotNull("FAIL: The error message '" + QUANTITY_ERROR + "' was NOT displayed!", errorMessage);
        Assert.assertTrue("FAIL: Error message text was not " + QUANTITY_ERROR + "! Actual: " +
                errorMessage.getText(), errorMessage.getText().contains(QUANTITY_ERROR));
        LOGGER.info("verifyProductQuantityErrorMessage completed successfully");
    }


    /**
     * Verifies the currently selected / default value of the "Sort By" dropdown matches the specified value
     *
     * @param expectedValue - String containing the expected / default value of the PLP "Sort By" dropdown
     */
    public void verifySortByCurrentValue(String expectedValue) {
        LOGGER.info("verifySortByCurrentValue started with value: '" + expectedValue + "'");
        WebElement sortDropdown = driver.getDisplayedElement(CommonActions.reactSelectizePlaceholderBy, Constants.ZERO);

        Assert.assertTrue("FAIL: Default value of PLP 'Sort By' dropdown was: '" + sortDropdown.getText()
                + "' but expected: '" + expectedValue + "'!", driver.checkIfElementContainsText(sortDropdown,
                expectedValue));
        LOGGER.info("verifySortByCurrentValue completed with value: '" + expectedValue + "'");
    }

    /**
     * Extracts the Fixed dollar discount of a certain item
     *
     * @param itemCode Code number of item to extract discount
     */
    public double extractFixedDiscountFromItem(String itemCode) {
        LOGGER.info("extractFixedDiscountFromItem started");
        driver.waitForElementVisible(CommonActions.addToCart);
        double amount = 0;
        List<WebElement> results = webDriver.findElements(plpResultsRowBy);
        for (WebElement result : results) {

            if (result.getText().contains(itemCode)) {
                WebElement discount = result.findElement(CommonActions.promotionDiscountBy);
                if (discount.getText().contains(ConstantsDtc.FIXED_DISCOUNT)) {
                    String sub = discount.getText().substring(1);
                    String[] split = sub.split(" ");
                    amount = Integer.parseInt(split[0]);
                    break;
                } else {
                    Assert.fail("FAIL: Item number " + itemCode + " did not have a fixed promotion discount!");
                }
            }
        }
        LOGGER.info("Returning integer value of " + amount);
        LOGGER.info("extractFixedDiscountFromItem completed");
        return amount;
    }

    /**
     * Extracts the Percentage instant savings discount of a certain item
     *
     * @param itemCode Code number of item to extract discount
     */
    public int extractFixedDiscountPercentageFromItem(String itemCode) {
        LOGGER.info("extractFixedDiscountPercentageFromItem started");
        driver.waitForElementVisible(CommonActions.addToCart);
        Boolean itemFound = false;
        int percentage = 0;
        commonActions.navToDifferentPageOfResults(Constants.FIRST);

        // Condition check - While item not found on current page, navigate to next page
        // till item is not found & next pagination link is displayed
        while (!itemFound) {
            driver.waitForPageToLoad();
            driver.waitForElementVisible(plpResultsRowBy);

            List<WebElement> rows = webDriver.findElements(plpResultsRowBy);
            if (Config.isIe() || Config.isSafari())
                driver.waitForMilliseconds();
            for (WebElement row : rows) {
                if (row.getText().contains(itemCode)) {
                    WebElement discount = row.findElement(CommonActions.promotionDiscountBy);
                    if (discount.getText().contains(ConstantsDtc.INSTANT)
                            || discount.getText().contains(ConstantsDtc.PERCENT_DISCOUNT)
                            || discount.getText().contains(ConstantsDtc.PERCENT_PERCENTAGE_DISCOUNT)) {
                        String percentageOffer = (discount.getText().split(" ")[0].trim());
                        int percentageLength = percentageOffer.length();
                        percentage = Integer.parseInt(percentageOffer.substring(1, percentageLength));
                        itemFound = true;
                        break;
                    }
                }
            }
            //navToDifferentPageOfResults now returns a boolean, indicating whether the next/prev button was found
            if (!itemFound && commonActions.navToDifferentPageOfResults(Constants.NEXT)) {
                LOGGER.info("Navigating to next page.");

                if (Config.isFirefox())
                    driver.waitForMilliseconds(Constants.TWO_THOUSAND);

            } else if (!itemFound) {
                Assert.fail("FAIL: Item - \"" + itemCode + "\" NOT found! Unable to add to cart.");
                break;
            }
        }
        LOGGER.info("Returning integer value of " + percentage);
        Assert.assertTrue("FAIL: Item number " + itemCode + " did not have a fixed discount!", itemFound);
        LOGGER.info("extractFixedDiscountPercentageFromItem completed");
        return percentage;
    }

    /**
     * Clicks on the first available 'Compare tire reviews' link on the PLP page
     */
    public void clickCompareTireReviewsLink() {
        LOGGER.info("clickCompareTireReviewsLink started");

        boolean itemFound = false;
        List<WebElement> rows = new ArrayList<WebElement>();

        driver.waitForPageToLoad();

        // Navigate to next page until link is found
        while (!itemFound) {
            //TODO: Retest after stable IE runs
            if (Config.isIe())
                driver.waitForMilliseconds(Constants.TWO);

            rows = webDriver.findElements(plpResultsRowBy);
            int i = 0;

            // Click on first available 'Compare tire reviews' link on the current page
            for (WebElement row : rows) {
                if (!row.findElement(resultsRowRatingBy).getText().contains(NO_REVIEWS_YET)) {
                    WebElement element = row.findElement(compareTireReviewsBy);
                    commonActions.appendToOrRemoveFromProductInfoList(row, i, false, Constants.APPEND);
                    driver.webElementClick(element);
                    itemFound = true;
                    break;
                }
                i++;
            }

            //navToDifferentPageOfResults now returns a boolean, indicating whether the next/prev button was found
            if (!itemFound && commonActions.navToDifferentPageOfResults(Constants.NEXT)) {
                LOGGER.info("Navigating to next page.");
            } else if (!itemFound) {
                Assert.fail("FAIL: There are no available 'Compare tire reviews' links.");
                break;
            }
        }

        LOGGER.info("clickCompareTireReviewsLink completed");
    }

    /**
     * Verifies passed in text does not appear in the Tire set tab titles
     *
     * @param text Text to verify does not appear in tabs
     */
    public void assertTextNotInTireTabTitles(String text) {
        LOGGER.info("assertTextNotInTireTabTitles started");
        driver.waitForPageToLoad();
        List<WebElement> results = webDriver.findElements(resultsOptionLinkBy);

        for (WebElement result : results) {
            Assert.assertTrue("FAIL: Tire set tabs do not contain the word " + text,
                    !result.getText().contains(text));
        }
        LOGGER.info("assertTextNotInTireTabTitles completed");
    }

    /**
     * Asserts item passed in is present on the product list page and Add to Cart is displayed
     *
     * @param item selected item/product to verify appears on page
     */
    public void assertItemOnProductListPage(String item) {
        LOGGER.info("assertItemOnProductListPage started");
        boolean itemFound = false;

        //TODO: retest when new safaridriver is stable
        if (Config.isSafari() || Config.isMobile() || Config.isFirefox())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);

        while (!itemFound) {
            //TODO: Retest once latest safaridriver or geckodriver is updated & stabilized
            if (!Config.isSafari()) {
                if (Config.isFirefox() || Config.isIe()) {
                    driver.waitForMilliseconds(Constants.ONE);
                }
                driver.waitForPageToLoad();
            } else {
                driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
            }

            driver.waitForElementVisible(plpResultsRowBy);

            // selecting add to cart item if item is contained in text of row
            List<WebElement> rows = webDriver.findElements(plpResultsRowBy);
            List<WebElement> addToCartButtons;
            addToCartButtons = webDriver.findElements(CommonActions.addToCartByClassBy);

            int i = 0;
            for (WebElement row : rows) {

                if (Config.isIe() || Config.isSafari())
                    driver.waitForMilliseconds();

                if (row.getText().contains(item)) {
                    driver.jsScrollToElement(addToCartButtons.get(i));
                    itemFound = true;
                    break;
                } else {
                    i++;
                }
            }

            //navToDifferentPageOfResults now returns a boolean, indicating whether the next/prev button was found
            if (!itemFound && commonActions.navToDifferentPageOfResults(Constants.NEXT)) {
                LOGGER.info("Navigate to next page.");

                if (Config.isFirefox())
                    driver.waitForMilliseconds(Constants.TWO_THOUSAND);

            } else if (!itemFound) {
                Assert.fail("FAIL: Item - \"" + item + "\" NOT found! Unable to add to cart.");
                break;
            }
        }
        LOGGER.info("Confirmed \"" + item + "\" was visible on the product list page.");
        LOGGER.info("assertItemOnProductListPage completed");
    }


    /**
     * Verifies the value is contained within a driving details result section on search results
     *
     * @param milesPerYear String to verify
     * @param page         PLP or PDP
     */
    public void assertMilesPerYearValueOnPdlDrivingDetails(String milesPerYear, String page) {
        LOGGER.info("assertMilesPerYearValueOnPdlDrivingDetails started");
        WebElement MilesEle;
        String milesPerYearText;
        if (page.equalsIgnoreCase(ConstantsDtc.PLP)) {
            milesPerYear += THOUSAND_MILES_SUFFIX;
            MilesEle = driver.getElementWithText(milesPerYearBy, milesPerYear);
            milesPerYearText = MilesEle.getText();

        } else {
            MilesEle = webDriver.findElement(milesPerYearPDPBy);
            milesPerYearText = MilesEle.getAttribute(Constants.VALUE);
        }

        Assert.assertTrue("FAIL: Pdl Driving Details results section did NOT contain a miles per year value "
                + "of: \"" + milesPerYear + "\"!", milesPerYearText.contains(milesPerYear));
        LOGGER.info("Confirmed that '" + milesPerYear + "' as miles per year was listed on Pdl Driving Details "
                + "results section.");
        LOGGER.info("assertMilesPerYearValueOnPdlDrivingDetails completed");
    }

    /**
     * Verify the order sequence of PDL Driving priorities
     *
     * @param drivingPrioritiesOrder Driven Priorities
     * @param page                   The page to be verified (PLP, PDLX )
     */
    public void assertDrivingPrioritiesOrder(String[] drivingPrioritiesOrder, String page) {
        LOGGER.info("assertDrivingPrioritiesOrder started");
        int i = 0;
        List<WebElement> options;
        if (ConstantsDtc.PLP.equalsIgnoreCase(page)) {
            options = webDriver.findElements(pdlDrivingPriorityOptionNameBy);
        } else {
            options = webDriver.findElements(PdlxFitmentPopupPage.drivingPriorityOptionsNameBy);
        }
        for (WebElement option : options) {
            // TODO: Success rate of this method is intermittent, needs a proper fix for IE
            if (!(drivingPrioritiesOrder[i].contains(option.getText().split(" ")[0].trim()))) {
                Assert.fail("FAIL: PDL driving priority sequence didn't match, At position " + (i + 1)
                        + " Expected " + drivingPrioritiesOrder[i] + " but Actual : " + option.getText());
            }
            LOGGER.info("At position " + (i + 1) + " Driving Priority " + drivingPrioritiesOrder[i] + " matched");
            i++;
        }
        LOGGER.info("assertDrivingPrioritiesOrder completed");
    }

    /**
     * Select Edit PDL Driving Details
     */
    public void selectEditPdlDrivingDetails() {
        LOGGER.info("selectEditPdlDrivingDetails started");
        WebElement pdlEdit;
        if (Config.isMobile()) {
            pdlEdit = webDriver.findElement(pdlEditDrivingDetailsBy);
        } else {
            pdlEdit = driver.getElementWithText(pdlEditDrivingDetailsBy, Constants.EDIT);
        }
        driver.waitForElementClickable(pdlEdit);
        driver.jsScrollToElementClick(pdlEdit);
        LOGGER.info("selectEditPdlDrivingDetails completed");
    }

    /**
     * Extract product item#(s) and price(s) from the first result row and save them to a hashmap as key-value pairs.
     *
     * @return product details hashmap
     */
    public HashMap<String, String> getProductPriceOnPlp() {
        LOGGER.info("getProductPriceOnPlp started");
        driver.waitForPageToLoad();

        WebElement resultRow = webDriver.findElements(plpResultsRowBy).get(0);

        List<WebElement> productCodes = null;
        if (driver.isElementDisplayed(CommonActions.itemCodeBy))
            productCodes = resultRow.findElements(CommonActions.itemCodeBy);
        else {
            productCodes = resultRow.findElements(plpItemNumberBy);
        }

        List<WebElement> productPrices = resultRow.findElements(CommonActions.productPriceBy);
        int index = 0;
        for (WebElement productCode : productCodes) {
            driver.jsScrollToElement(productCode);
            productPriceOnPlp.put(
                    productCode.getText().split(" ")[1].replace("#", "").trim().split("\n")[0],
                    productPrices.get(index).getText()
            );
            index++;
        }
        LOGGER.info("getProductPriceOnPlp completed");
        return productPriceOnPlp;
    }

    /**
     * Handles the selection of the desired product list tab when sets are available (SETS, FRONT, REAR)
     *
     * @param tab The tab in results to pick from (Ex. SETS, FRONT, REAR)
     */
    private void selectProductListTab(String tab) {
        LOGGER.info("selectProductListTab started for tab: " + tab);
        try {
            By tabBy = resultsOptionLinkBy;

            if (Config.isMobile()) {
                tabBy = resultsOptionLinkMobileBy;
            }

            List<WebElement> optionLinks = webDriver.findElements(tabBy);
            for (WebElement optionLink : optionLinks) {
                if (optionLink.getText().toUpperCase().contains(tab.toUpperCase())) {
                    driver.jsScrollToElement(optionLink);
                    optionLink.click();
                    break;
                }
            }
        } catch (NoSuchElementException e) {
            Assert.fail("FAIL: The desired product list tab: \"" + tab + "\" was NOT found and nothing was " +
                    "added to the cart!");
        }
        LOGGER.info("selectProductListTab completed for tab: " + tab);
    }

    /**
     * Verify Product List page display tabs for staggered vehicle
     */
    public void verifyPLPTabDisplayForStaggered() {
        LOGGER.info("verifyPLPTabDisplayForStaggered started");
        driver.waitForPageToLoad();
        List<WebElement> plpTabs = webDriver.findElements(resultsOptionLinkBy);
        if (STAGGERED_PLP_TAB.length != plpTabs.size()) {
            Assert.fail("FAIL: All three plp display tabs not showing for staggered vehicle");
        }
        for (int i = 0; i < STAGGERED_PLP_TAB.length; i++) {
            String tabValue = plpTabs.get(i).getText().split("\\s+")[0].trim();
            if (Arrays.asList(STAGGERED_PLP_TAB).contains(tabValue)) {
                LOGGER.info("PASS: Staggered vehicle plp display tab found : " + tabValue);
            } else {
                Assert.fail("FAIL: For Staggered vehicle plp display tab : (" + tabValue + ") expected : "
                        + STAGGERED_PLP_TAB[i]);
            }
        }
        LOGGER.info("verifyPLPTabDisplayForStaggered completed");
    }

    /**
     * Verifies the display of a specified list of filter sections on the product list page
     *
     * @param filterSections - String of comma seperated values containing the name(s) of the refinement filter
     *                       section(s) as should appear on the PLP
     */
    public void verifyFilterSectionsDisplay(String filterSections) {
        LOGGER.info("verifyFilterSectionsDisplay started w/ filter section(s): '" + filterSections + "'");
        driver.waitForPageToLoad();

        if (Config.isSafari()) {
            if (driver.isElementDisplayed(showFilterBtn)) {
                showFilterBtn.click();
            }
        }

        driver.waitForElementVisible(searchFilterSectionBy);
        List<String> sectionsToVerifyList = Arrays.asList(filterSections.split(","));
        List<WebElement> displayedFilterSectionsList = webDriver.findElements(searchFilterSectionBy);

        for (String sectionToVerify : sectionsToVerifyList) {
            boolean filterSectionFound = false;
            for (WebElement displayedSection : displayedFilterSectionsList) {
                if (displayedSection.getText().toLowerCase().contains(sectionToVerify.trim().toLowerCase())) {
                    filterSectionFound = true;
                    break;
                }
            }
            Assert.assertTrue("FAIL: Unable to find '" + sectionToVerify.trim() + "' filter section!",
                    filterSectionFound);
        }
        LOGGER.info("verifyFilterSectionsDisplay completed w/ filter section(s): '" + filterSections + "'");
    }

    /**
     * Verifies that a staggered option tab with the specified value set is visible
     *
     * @param tabValue The value of the tab
     */
    public void verifyStaggeredOptionTabIsDisplayed(String tabValue) {
        LOGGER.info("verifyStaggeredOptionTabIsDisplayed started");
        driver.waitForPageToLoad();

        WebElement optionElement = driver.getElementWithText(staggeredOptionTabBy, tabValue);
        if (!driver.isElementDisplayed(optionElement)) {
            Assert.fail("FAIL: Staggered option tab : " + tabValue + "didn't display on PLP result page");
        }
        LOGGER.info("verifyStaggeredOptionTabIsDisplayed completed");
    }

    /**
     * Extracts the Staggered size from fitment tab
     *
     * @param fitmentType The value of the staggered tab
     * @return String       Diameter of the selected fitment type
     */
    public String getStaggeredFitmentSizeFromTab(String fitmentType) {
        LOGGER.info("getStaggeredFitmentSizeFromTab started");
        driver.waitForPageToLoad();

        WebElement optionElement = driver.getElementWithText(staggeredOptionTabBy, fitmentType);
        LOGGER.info("getStaggeredFitmentSizeFromTab completed");
        return optionElement.findElement(staggeredSizeBy).getText().split("\"")[0];
    }

    /**
     * Verifies Front Or Rear wheel diameter matches with the size of each product on the results page
     *
     * @param fitmentType The value of the staggered tab - Front Or Rear
     */
    public void verifyListedProductsSizeMatchesWithSelectedStaggeredDiameter(String fitmentType) {
        LOGGER.info("verifyListedProductsSizeMatchesWithSelectedStaggeredDiameter started");
        driver.waitForPageToLoad();
        String diameter;
        List<WebElement> displaySizes = new ArrayList<WebElement>();

        boolean sizeMatched = true;
        if (plpResultsHeader.getText().toLowerCase().contains(Constants.TIRE.toLowerCase())) {
            diameter = getStaggeredFitmentSizeFromTab(fitmentType).replace("-", " R").
                    replace("/", " /").trim();
        } else {
            diameter = CommonActions.fitmentBannerSizeInfo.getText().split("\n", 5)[3].substring(0, 2);
        }
        List<WebElement> displayedStaggeredSizes = webDriver.findElements(tireSizeBy);
        for (WebElement displayStaggeredSize : displayedStaggeredSizes) {
            if (!displayStaggeredSize.getText().isEmpty()) {
                displaySizes.add(displayStaggeredSize);
            }
        }
        List<WebElement> items = webDriver.findElements(plpItemNumberBy);
        int row = 0;

        for (WebElement displayedStaggeredSize : displaySizes) {
            if (!displayedStaggeredSize.getText().split("X")[0].trim().toLowerCase().contains(diameter.toLowerCase())) {
                sizeMatched = false;
                row = displayedStaggeredSizes.indexOf(displayedStaggeredSize);
                break;
            }
        }
        Assert.assertTrue("FAIL: Staggered size " + displayedStaggeredSizes.get(row).getText() + " for '"
                + items.get(row).getText() + "' did NOT match with selected " + fitmentType + " diameter: '"
                + diameter + "'!", sizeMatched);

        LOGGER.info("verifyListedProductsSizeMatchesWithSelectedStaggeredDiameter completed");
    }

    /**
     * Verify that Customer Rating is displayed and either a link for 'Read reviews'
     * or a message stating 'There are no reviews yet.' is displayed for each product.
     */
    public void verifyCustomerRatingAndReviewsDisplayedForListedProducts() {
        LOGGER.info("verifyCustomerRatingAndReviewsDisplayedForListedProducts started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);

        List<WebElement> productRows = driver.getDisplayedElementsList(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productRowText = productRow.getText();
            if (productRowText.contains(ConstantsDtc.VIEW_DETAILS)) {
                Assert.assertTrue("FAIL: Rating star is not present on the page ",
                        driver.isElementDisplayed(productRow.findElement(CommonActions.starRatingBy)));
                Assert.assertTrue("FAIL: The read review option is available for the canonical PLP product",
                        !driver.isElementDisplayed(productRow.findElement(CommonActions.reviewRatingBy)));
            } else if (driver.isElementDisplayed(productRow.findElement(CommonActions.reviewRatingBy))
                    && !productRowText.contains(ConstantsDtc.READ_REVIEWS)) {
                Assert.assertTrue("Product doesn't contain the message 'No reviews yet'",
                        productRowText.contains(NO_REVIEWS_YET));
            } else if (!productRowText.contains(NO_REVIEWS_YET) &&
                    !driver.isElementDisplayed(productRow.findElement(CommonActions.reviewRatingBy))) {
                Assert.fail("FAIL:  Neither customer reviews Nor the 'No reviews yet.' message is displayed for product '"
                        + productRow.findElement(productNameBy).getText() + "'");
            }
        }
        LOGGER.info("verifyCustomerRatingAndReviewsDisplayedForListedProducts completed");
    }

    /**
     * Verify the 'Compare tire reviews' link appears for products that have reviews
     */
    public void verifyCompareTireReviewsLinksDisplayed() {
        LOGGER.info("verifyCompareTireReviewsLinksDisplayed started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);

        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        if (driver.getElementsWithText(CommonActions.reviewRatingBy, ConstantsDtc.READ_REVIEWS).size() > 1) {
            for (WebElement productRow : productRows) {
                boolean found = true;
                if (!productRow.findElement(resultsRowRatingBy).getText().contains(NO_REVIEWS_YET)) {
                    found = driver.isElementDisplayed(productRow.findElement(compareTireReviewsBy));
                }
                if (!found) {
                    Assert.fail("FAIL:  Compare tire reviews not displayed for product '"
                            + productRow.findElement(productNameBy).getText() + "'!");
                }
            }
        } else {
            Assert.fail("FAIL: Not having enough reviews to display Compare tire reviews for products!");
        }
        LOGGER.info("verifyCompareTireReviewsLinksDisplayed completed");
    }

    /**
     * @param filterLabel Verify that selected filter is selected or deselected for specific brands
     * @param option      selected or deselected
     * @param type        Tire or Wheel
     */
    public void verifyFilterIsDeselectedForSpecificBrands(String filterLabel, String option, String type) {
        LOGGER.info("verifyFilterIsDeselectedForSpecificBrands started");
        driver.waitForPageToLoad();
        String[] brandURLs;

        if (type.equalsIgnoreCase(Constants.TIRE)) {
            brandURLs = ConstantsDtc.TIRE_BRANDS_URLS;
        } else {
            brandURLs = ConstantsDtc.WHEEL_BRANDS_URLS;
        }
        for (String brandURL : brandURLs) {
            commonActions.navigateToPage(brandURL);
            commonActions.waitForUrl(brandURL, Constants.THIRTY);
            assertCheckboxDeselectedOrSelected(filterLabel, option);
            LOGGER.info("Confirmed: " + filterLabel + " filter was deselected for " + type + " Brand with URL "
                    + brandURL);
        }
        LOGGER.info("verifyFilterIsDeselectedForSpecificBrands completed");
    }

    /**
     * Verifies the specified filter option's font size remains the same before and after selection
     *
     * @param checkBoxLabel String of text the filter contains
     */
    public void assertFilterFontSizeBeforeAndAfterFilterSelection(String checkBoxLabel) {
        LOGGER.info("assertFilterFontSizeBeforeAndAfterFilterSelection started");
        String newFontSize = getPlpFilterCheckbox(checkBoxLabel).getCssValue(Constants.FONT_SIZE);
        Assert.assertTrue("FAIL: The '" + checkBoxLabel + "' filter's font size value before and after selection did"
                + " NOT match!", filterFontSize.equalsIgnoreCase(newFontSize));
        LOGGER.info("Confirmed " + checkBoxLabel + " filter font size value matched:" + newFontSize + " : "
                + filterFontSize);
        LOGGER.info("assertFilterFontSizeBeforeAndAfterFilterSelection completed");
    }

    /**
     * Extracts the font size value from the specified filter option
     *
     * @param checkBoxLabel Filter label
     */
    public void getFilterFontSize(String checkBoxLabel) {
        LOGGER.info("getFilterFontSize started for the '" + checkBoxLabel + "' filter");
        filterFontSize = getPlpFilterCheckbox(checkBoxLabel).getCssValue(Constants.FONT_SIZE);
        LOGGER.info("getFilterFontSize completed for filter '" + checkBoxLabel + "'. Current font size is '"
                + filterFontSize + "'");
    }

    /**
     * Verifies the Bolt Pattern Facet on the PLP
     *
     * @param isPresent True, if the Bolt Pattern displays and there is no Fitment
     *                  False, if the Bolt Pattern does not display and there is Fitment
     */
    public void verifyBoltPatternFacetIsDisplayed(boolean isPresent) {
        LOGGER.info("verifyBoltPatternFacetIsDisplayed started");
        if (isPresent && (!driver.isAttributePresent(vehicleSelectedDescription, ConstantsDtc.NO_VEHICLE_SELECTED))) {
            Assert.assertTrue("FAIL: Bolt Pattern facet did not display on PLP Page ",
                    driver.isElementDisplayed(boltPatternFacet));
            LOGGER.info("As expected, the Bolt Pattern facet displayed on the PLP without Fitment");
        } else {
            Assert.assertFalse("FAIL: Bolt Pattern facet displayed on PLP",
                    driver.isElementDisplayed(boltPatternFacet));
            LOGGER.info("As expected , the Bolt Pattern facet should not display on PLP page with Fitment");
        }
        LOGGER.info("verifyBoltPatternFacetIsDisplayed completed");
    }

    /**
     * Selects the specified items in a comma seperated list for comparison from the product list page results. Method
     * paginates through the entire result list looking for the requested item number, and if found, restarts from
     * the first page of the results list to begin looking for the subsequent item numbers. If the method iterates
     * through the entire list and does not find a specified item number, an error is thrown.
     *
     * @param itemNumbers Comma separated list of product / item numbers to select for comparison
     */
    public void selectItemNumbersForComparison(String itemNumbers) {
        LOGGER.info("selectItemNumbersForComparison started with number(s):'" + itemNumbers + "'");
        driver.waitForPageToLoad();
        commonActions.checkForNoResultsMessage();
        driver.waitForElementVisible(CommonActions.addToCart);

        List<WebElement> resultRowsList = new ArrayList<>();
        List<String> itemNumbersToSelectList = Arrays.asList(itemNumbers.split(","));

        for (String itemNumber : itemNumbersToSelectList) {
            boolean itemFound = false;

            if (commonActions.navToDifferentPageOfResults(Constants.FIRST))
                LOGGER.info("Navigating back to first page of results");

            while (!itemFound) {
                //TODO: Retest once latest safaridriver or geckodriver is updated & stabilized
                if (Config.isSafari() || Config.isFirefox() || Config.isIe()) {
                    driver.waitForPageToLoad();
                }

                resultRowsList.clear();
                resultRowsList = webDriver.findElements(plpResultsRowBy);

                for (WebElement resultRow : resultRowsList) {
                    if (CommonUtils.containsIgnoreCase(resultRow.getText(), ConstantsDtc.ITEM_NUMBER_PREFIX + itemNumber.trim())) {
                        itemFound = true;
                        driver.jsScrollToElement(resultRow.findElement(CommonActions.checkBoxBy));
                        resultRow.findElement(CommonActions.checkBoxBy).click();
                        break;
                    }
                }

                if (!itemFound && commonActions.navToDifferentPageOfResults(Constants.NEXT)) {
                    LOGGER.info("Navigating to next page.");

                    if (Config.isFirefox())
                        driver.waitForMilliseconds(Constants.TWO_THOUSAND);

                } else if (!itemFound) {
                    Assert.fail("FAIL: Item - \"" + itemNumber.trim() + "\" NOT found! "
                            + "Unable to select for comparison");
                    break;
                }
            }
        }
        LOGGER.info("selectItemNumbersForComparison completed with number(s):'" + itemNumbers + "'");
    }

    /**
     * Verify the only expected brand products are displayed on PLP
     *
     * @param brand expected brand products
     */
    public void assertProductBrandOnPLP(String brand) {
        LOGGER.info("assertProductBrandOnPLP started for " + brand);
        List<WebElement> productsBrand = webDriver.findElements(brandNameBy);
        for (WebElement productBrand : productsBrand) {
            Assert.assertTrue("FAIL: Product brand : " + productBrand.getText()
                    + " displayed different from expected brand : " + brand, productBrand.getText()
                    .equalsIgnoreCase(brand));
        }
        LOGGER.info("assertProductBrandOnPLP completed for " + brand);
    }

    /**
     * Verify Original Equipment Tire is displayed on PLP page
     *
     * @param page PLP or PDP
     */
    public void assertOETireDisplayedOnPage(String page) {
        LOGGER.info("assertOETireDisplayedOnPage started on " + page + " page");
        driver.waitForPageToLoad();
        WebElement oeProduct = webDriver.findElement(productListOEBy);
        Assert.assertEquals("FAIL: 'Original Equipment tire' was NOT displayed with expected text bar: '"
                + ConstantsDtc.OE_TIRE + "'!", ConstantsDtc.OE_TIRE, oeProduct.getText());
        LOGGER.info("assertOETireDisplayedOnPage completed on " + page + " page");
    }

    /**
     * Verifies the order of the filter categories matches the expectation, dependent on the product type
     * (Wheels or Tires)
     *
     * @param productType Product type (Wheels Or Tires) which determines the categories used in validation
     */
    public void verifyOrderOfFilterCategoriesForProductType(String productType) {
        LOGGER.info("verifyOrderOfFilterCategoriesForProductType started with productType: '" + productType + "'");
        if (Config.isMobile())
            driver.jsScrollToElementClick(openFilterSectionBtn);

        List<String> filterCategoriesToVerifyList = new ArrayList<>();
        List<String> displayedFilterCategoriesList = new ArrayList<>();

        if (productType.equalsIgnoreCase(ConstantsDtc.TIRES)) {
            filterCategoriesToVerifyList = ConstantsDtc.tireFilterCategoriesList;
        } else if (productType.equalsIgnoreCase(ConstantsDtc.WHEELS)) {
            filterCategoriesToVerifyList = ConstantsDtc.wheelFilterCategoriesList;
        } else {
            Assert.fail("FAIL: The product type of : '" + productType + "' is NOT currently a valid selection!");
        }

        if (Config.isFirefox())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        if (Config.isSafari()) {
            if (driver.isElementDisplayed(showFilterBtn)) {
                showFilterBtn.click();
            }
        }
        driver.waitForElementVisible(CommonActions.productListFilter);

        List<WebElement> searchFilterSectionsList = driver.getDisplayedElementsList(CommonActions.productListFilter);
        for (WebElement searchFilterSection : searchFilterSectionsList) {
            displayedFilterCategoriesList.add(searchFilterSection.getText().trim());
        }

        Assert.assertEquals("FAIL: The displayed filter categories:\n\t" + displayedFilterCategoriesList
                + "\n\n\t did NOT match the expected order of categories:\n\t" + filterCategoriesToVerifyList
                + "!", filterCategoriesToVerifyList, displayedFilterCategoriesList);
        if (Config.isMobile() && driver.isElementDisplayed(CommonActions.closeIcon)) {
            CommonActions.closeIcon.click();
        }
        LOGGER.info("verifyOrderOfFilterCategoriesForProductType completed with productType: '" + productType + "'");
    }

    /**
     * Verifies the quantity of products selected
     *
     * @param quantity Number of checkboxes to be verified
     */
    public void assertProductQuantitySelected(Integer quantity) {
        LOGGER.info("assertProductQuantitySelected started");
        List<WebElement> checkboxes = webDriver.findElements(compareButtonBy);
        Assert.assertTrue("FAIL: The Expected " + quantity + " did not match the actual quantity " + checkboxes.size(),
                checkboxes.size() == quantity);
        LOGGER.info("assertProductQuantitySelected completed");
    }

    /**
     * Verifies the text on the compare button
     *
     * @param expectedValue The text to be verified
     */
    public void assertCompareButtonText(String expectedValue) {
        LOGGER.info("assertCompareButtonText started");
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> checkboxes = webDriver.findElements(compareButtonBy);
        for (WebElement checkbox : checkboxes) {
            String actualText = checkbox.getText();
            Assert.assertTrue("FAIL: The Compare Button text '" + actualText + "' did not match expected text '"
                    + expectedValue + "'.", actualText.equalsIgnoreCase(expectedValue));
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("assertCompareButtonText completed");
    }

    /**
     * Return the index for the row containing the first compare button that can be selected or
     * deselected based on specification
     *
     * @param selectDeselect "Select" or "Deselect"
     * @return row number
     */
    public int findEligibleCompareCheckBox(String selectDeselect) {
        LOGGER.info("findEligibleCompareCheckBox to '" + selectDeselect + "' started");
        List<WebElement> compareButtons = webDriver.findElements(compareButtonTagBy);
        int rowIndex = 0;
        for (WebElement compareButton : compareButtons) {
            if (CommonUtils.containsIgnoreCase(compareButton.getAttribute(Constants.CLASS),
                    ConstantsDtc.COMPARE_SELECTOR_COMPARABLE)) {
                if (!selectDeselect.equalsIgnoreCase(Constants.SELECT))
                    break;
            } else {
                if (selectDeselect.equalsIgnoreCase(Constants.SELECT))
                    break;
            }
            rowIndex++;
        }
        LOGGER.info("findEligibleCompareCheckBox to '" + selectDeselect + "' completed");
        return rowIndex;
    }

    /**
     * Selects a Product Checkbox based on the position
     *
     * @param position The specific value of rendered checkbox's position
     */
    public void selectCompareCheckboxAtPosition(int position) throws Exception {
        LOGGER.info("selectCompareCheckboxAtPosition started");
        driver.waitForPageToLoad();
        List<WebElement> inputs;

        WebElement row = webDriver.findElements(plpResultsRowBy).get(position - 1);
        WebElement compareButton = row.findElements(compareButtonTagBy).get(position - 1);

        if (!CommonUtils.containsIgnoreCase(compareButton.getAttribute(Constants.CLASS),
                ConstantsDtc.COMPARE_SELECTOR_COMPARABLE)) {
            selectDeselectCompareProductsCheckbox(row, Constants.SELECT);
            if (Config.isMobile()) {
                row = webDriver.findElements(plpResultsRowMobileBy).get(position - 1);
            } else {
                row = webDriver.findElements(plpResultsRowBy).get(position - 1);
            }
            commonActions.appendToOrRemoveFromProductInfoList(row, position - 1, false, Constants.APPEND);
        } else {
            LOGGER.info("Product list checkbox # " + position + " was already selected");
        }
        LOGGER.info("selectCompareCheckboxAtPosition completed");
    }

    /**
     * De-selects the Compare Checkbox at a given position
     *
     * @param position The specific value of rendered checkbox's position
     */
    public void deselectCompareCheckboxAtPosition(int position) {
        LOGGER.info("deselectCompareCheckboxAtPosition started");
        driver.waitForPageToLoad();

        WebElement row = webDriver.findElements(plpResultsRowBy).get(position - 1);
        WebElement compareButton = row.findElements(compareButtonTagBy).get(position - 1);

        if (CommonUtils.containsIgnoreCase(compareButton.getAttribute(Constants.CLASS),
                ConstantsDtc.COMPARE_SELECTOR_COMPARABLE)) {
            selectDeselectCompareProductsCheckbox(row, Constants.DESELECT);
        }
        commonActions.appendToOrRemoveFromProductInfoList(row, position - 1, false, Constants.REMOVE);
        LOGGER.info("deselectCompareCheckboxAtPosition completed");
    }

    /**
     * Select or deselect the compare products checkbox on the specified PLP row
     * @param row               Product row on PLP page
     * @param selectDeselect    "Select" or "Deselect"
     */
    private void selectDeselectCompareProductsCheckbox(WebElement row, String selectDeselect) {
        LOGGER.info("selectDeselectCompareProductsCheckbox started for '" + selectDeselect + "' checkbox on PLP page");
        WebElement checkBox = row.findElement(CommonActions.checkBoxBy);
        WebElement checkBoxInput = row.findElement(CommonActions.checkboxInputBy);
        int counter1 = 0;
        boolean success = false;
        Assert.assertTrue("FAIL: Must specify either 'Select' or 'Deselect' for Compare Products checkbox",
                selectDeselect.equalsIgnoreCase(Constants.SELECT) ||
                        selectDeselect.equalsIgnoreCase(Constants.DESELECT));
        do {
            driver.jsScrollToElement(checkBox);
            driver.waitForMilliseconds(Constants.ONE_HUNDRED);
            driver.webElementClick(checkBoxInput);
            int counter2 = 0;
            do {
                if (selectDeselect.equalsIgnoreCase(Constants.SELECT)) {
                    success = checkBoxInput.isSelected();
                } else {
                    success = !checkBoxInput.isSelected();
                }
                if (!success) {
                    driver.waitOneSecond();
                }
                counter2++;
            } while (!success && counter2 < Constants.THREE);
            counter1++;
        } while (!success && counter1 < Constants.TEN);
        Assert.assertTrue("FAIL: Unable to '" + selectDeselect + "' compare products checkbox on PLP page", success);
        LOGGER.info("selectDeselectCompareProductsCheckbox completed for '" + selectDeselect + "' checkbox on PLP page");
    }

    /**
     * Verifies that the specified text is present in the PLP results header message e.g. "28 results for wheels"
     *
     * @param textToValidate Text string that is expected to appear in the PLP results header
     */
    public void verifyPlpHeaderMessageContainsText(String textToValidate) {
        LOGGER.info("verifyPlpHeaderMessageContainsText started with text: '" + textToValidate + "'");
        driver.waitForPageToLoad();
        commonActions.waitForSpinner();
        commonActions.checkForNoResultsMessage();

        String actualHeader;

        if (!Config.isMobile()) {
            if (driver.isElementDisplayed(plpCurrentResultsTabBy, Constants.ZERO)
                    && driver.getDisplayedElement(plpCurrentResultsTabBy).getText().contains(SETS)) {
                LOGGER.info("Skipping PLP header text validation as the \"SETS\" tab does not display header text!");
            } else {
                driver.waitForElementVisible(plpResultsHeader);
                actualHeader = plpResultsHeader.getText();
                Assert.assertTrue("FAIL: The PLP header text '" + actualHeader.trim() + "' did NOT contain the"
                        + " expected text '" + textToValidate + "'!", actualHeader.trim().toLowerCase().contains(
                        textToValidate.trim().toLowerCase()));
                LOGGER.info("verifyPlpHeaderMessageContainsText completed with text: '" + textToValidate + "'");
            }
        } else {
            LOGGER.info("Skipping PLP header text validation as the mobile site does not display header text based on search terms!");
        }
    }

    /**
     * Verifies the selected category appears in the URL path
     *
     * @param category selected category to appear on the page/URL path
     * @param brand    whether or not you're verifying a brand catalog
     */
    public void verifyCatalogPage(String category, boolean brand) {
        LOGGER.info("verifyCatalogPage started");
        String brandCategoryUrlPath;
        verifyPlpDisplayedWithResults();
        if (webDriver.getCurrentUrl().contains(Constants.WHEEL.toLowerCase())) {
            if (category.equalsIgnoreCase(Constants.MB_WHEELS) || (category.equalsIgnoreCase(Constants.AMERICAN_OUTLAW)))
                brandCategoryUrlPath = getCategoryWheelUrlPath(category.replace(" ", "-").toLowerCase(), brand);
            else
                brandCategoryUrlPath = getCategoryWheelUrlPath(category.split(" ")[0].toLowerCase(), brand);
        } else
            brandCategoryUrlPath = getCategoryTireUrlPath(category.split(" ")[0].toLowerCase(), brand);
        commonActions.waitForUrl(brandCategoryUrlPath, Constants.TEN);

        LOGGER.info("verifyCatalogPage completed");
    }

    /**
     * Returns partial URL path for tire catalog containing category
     *
     * @param category selected category to appear in URL
     * @param brand    whether or not you're verifying a brand catalog
     */
    private String getCategoryTireUrlPath(String category, boolean brand) {
        LOGGER.info("getCategoryTireUrlPath started with category = " + category + ", and brand = " + brand);
        if (brand) {
            String path = "/tires/brands/" + category + "-catalog";
            LOGGER.info("getCategoryTireUrlPath returning brand path = " + path);
            return path;
        } else {
            String path = "/tires/" + category + "-catalog";
            LOGGER.info("getCategoryTireUrlPath returning path = " + path);
            return path;
        }
    }

    /**
     * Returns partial URL path for wheel catalog containing category
     *
     * @param category selected category to appear in URL
     * @param brand    whether or not you're verifying a brand catalog
     */
    private String getCategoryWheelUrlPath(String category, boolean brand) {
        LOGGER.info("getCategoryWheelUrlPath started with category = " + category + ", and brand = " + brand);
        if (brand) {
            String path = "/wheels/brands/" + category + "-catalog";
            LOGGER.info("getCategoryWheelUrlPath returning brand path = " + path);
            return path;
        } else {
            String path = "/wheels/" + category + "-catalog";
            LOGGER.info("getCategoryWheelUrlPath returning path = " + path);
            return path;
        }
    }

    /**
     * Verify the PLP page is displayed and products are listed
     */
    public void verifyPlpDisplayedWithResults() {
        LOGGER.info("verifyPlpDisplayedWithResults started");
        if (!Config.isMobile()) {
            driver.waitForElementVisible(plpResultsHeader);
            Assert.assertTrue("FAIL: The PLP page is not displayed",
                    CommonUtils.containsIgnoreCase(plpResultsHeader.getText(), Constants.RESULTS));
        }
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        boolean resultsDisplayed = driver.isElementDisplayed(plpResultsRowBy, Constants.ZERO);
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        boolean isStaggered = driver.isElementDisplayed(staggeredOptionTabBy, Constants.ZERO);
        driver.scenarioData.setStaggeredProduct(isStaggered);
        if (!resultsDisplayed && isStaggered) {
            driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
            String tabText = webDriver.findElement(staggeredOptionTabBy).getText();
            if (CommonUtils.containsIgnoreCase(tabText, ConstantsDtc.SETS)) {
                commonActions.selectStaggeredTab(ConstantsDtc.FRONT);
                if (driver.isElementDisplayed(errorMessageBy, Constants.ONE)) {
                    commonActions.selectStaggeredTab(ConstantsDtc.REAR);
                }
            }
        }
        if (driver.isElementDisplayed(errorMessageBy, Constants.ONE)) {
            Assert.fail("FAIL: Products not listed on PLP. Error message displayed: " +
                    webDriver.findElement(errorMessageBy).getText());
        }
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        Assert.assertTrue("FAIL: There are no products listed on the PLP page",
                driver.isElementDisplayed(webDriver.findElement(plpResultsRowBy), Constants.ZERO));
        LOGGER.info("verifyPlpDisplayedWithResults completed");
    }

    /**
     * Assert provided for validating the Top 3 Tiles section is displayed
     */
    public void assertTop3TilesComponentIsDisplayed() {
        LOGGER.info("assertTop3TilesComponentIsDisplayed started");
        By top3Section;
        driver.waitForPageToLoad();
        if (Config.isMobile()) {
            top3Section = top3ComponentMobileBy;
        } else {
            top3Section = top3ComponentBy;
        }
        Assert.assertTrue("FAIL: Top 3 Tiles component not found on the product results page!",
                driver.isElementDisplayed(top3Section));
        LOGGER.info("assertTop3TilesComponentIsDisplayed completed");
    }

    /**
     * Assert provided for validating the values in the Top 3 Tiles section
     *
     * @param value1 The first expected value in the Top 3 Tiles section
     * @param value2 The second expected value in the Top 3 Tiles section
     * @param value3 The third expected value in the Top 3 Tiles section
     */
    public void assertTop3TileValues(String value1, String value2, String value3) {
        LOGGER.info("assertTop3TileValues started");
        driver.waitForPageToLoad();
        List<WebElement> top3Tiles;
        List<String> expectedValues = Arrays.asList(value1, value2, value3);

        if (Config.isMobile()) {
            top3Tiles = webDriver.findElements(top3TilesMobileBy);
        } else
            top3Tiles = webDriver.findElements(top3TilesrecommendationBy);
        //verify if all three top tiles options found
        if (top3Tiles.size() != expectedValues.size()) {
            Assert.fail("FAIL: All three top tiles options not displayed");
        }
        int i = 0;
        for (WebElement top3Tile : top3Tiles) {
            Assert.assertTrue("FAIL: The Actual Tile value does not contain the Expected Tile value : "
                            + expectedValues.get(i) + " ! ",
                    (top3Tile.getAttribute(Constants.CLASS).contains(expectedValues.get(i).split(" ")[0]
                            .toLowerCase()) || top3Tile.getText().contains(expectedValues.get(i).split(" ")[0]
                            .toLowerCase())) && top3Tile.getAttribute(Constants.CLASS).contains(expectedValues.get(i).
                            split(" ")[1].toLowerCase()));
            i++;
        }
        LOGGER.info("assertTop3TileValues started");
    }

    /**
     * Extracts a Product on the position from Homepage or PLP
     *
     * @param position The specific value of rendered product position
     * @param page   Homepage or PLP
     */
    public String extractProductBrandOnHomepageOrPLP(int position, String page) throws Exception {
        LOGGER.info("extractProductBrandOnHomepageOrPLP started");
        driver.waitForPageToLoad();
        position = position - 1;
        String brandName;
        if(page.equalsIgnoreCase(ConstantsDtc.PLP)) {
            brandName = webDriver.findElements(productNameBy).get(position).getText();
        }
        else{
            brandName = webDriver.findElements(CommonActions.suggestedSellingTextContainerBy).get(position).getText();
        }
        LOGGER.info("extractProductBrandOnHomepageOrPLP completed");
        return brandName;
    }

    /**
     * Assert provided for item in Top 3 Tiles with brand in the result list
     *
     * @param expectedBrandName The brand to be verified
     * @param position          The position of the expected brand
     */
    public void assertProductInTop3Tiles(String expectedBrandName, int position) {
        LOGGER.info("assertProductsOnTop3Tiles started");
        driver.waitForPageToLoad();
        List<WebElement> top3TilesBrands = webDriver.findElements(top3TilesBrandBy);
        Assert.assertTrue("FAIL: The displayed product brand does not contain expected brand:->  "
                + expectedBrandName + " !", top3TilesBrands.get(position - 1).getAttribute(Constants.ATTRIBUTE_HREF)
                .toLowerCase().contains(expectedBrandName.toLowerCase()));
        LOGGER.info("assertProductsOnTop3Tiles completed");
    }

    /**
     * Assert provided to check for a filter not part of the facet
     *
     * @param checkboxLabel The filter to be validated
     */
    public void assertFilterNotDisplayed(String checkboxLabel) {
        LOGGER.info("assertFilterNotDisplayed started");
        driver.waitForPageToLoad();
        if (Config.isMobile())
            driver.jsScrollToElementClick(openFilterSectionBtn);

        List<WebElement> filterOptions = webDriver.findElements(filterOptionBy);
        for (WebElement filterOption : filterOptions) {
            Assert.assertTrue("FAIL: The '" + checkboxLabel + "' filter was displayed when it was expected to" +
                    " NOT be displayed!", (!(filterOption.getText().equalsIgnoreCase(checkboxLabel))));
        }
        if (Config.isMobile() && driver.isElementDisplayed(CommonActions.closeIcon)) {
            CommonActions.closeIcon.click();
        }
        LOGGER.info("assertFilterNotDisplayed started");
    }

    /**
     * Click Read Reviews link on PLP
     */
    public void selectReadReviewsLink() {
        LOGGER.info("selectReadReviewsLink started");
        driver.waitForPageToLoad();
        webDriver.findElement(CommonActions.reviewRatingBy).click();
        LOGGER.info("selectReadReviewsLink completed");
    }

    /**
     * Click on product
     *
     * @param itemCode product code
     */
    public void clickOnProduct(String itemCode) {
        LOGGER.info("clickOnProduct started");
        driver.waitForPageToLoad();
        WebElement productSection = commonActions.getPLPResultTopElements(itemCode).get(0);
        driver.jsScrollToElementClick(productSection.findElement(productNameBy));
        driver.waitForPageToLoad();
        LOGGER.info("clickOnProduct completed");
    }

    /**
     * Click compare for item code on the product list page
     *
     * @param item product code
     */
    public void selectProductToCompare(String item) {
        LOGGER.info("selectProductToCompare started for " + item);
        assertItemOnProductListPage(item);

        WebElement row = commonActions.getPLPResultTopElement(item);
        row.findElement(CommonActions.checkBoxBy).click();
        LOGGER.info("selectProductToCompare completed for " + item);
    }

    /**
     * Verify Need it Now? message display for item on PLP
     *
     * @param productCode Product Code
     */
    public void assertNeedItNowDisplayPlp(String productCode) {
        LOGGER.info("assertNeedItNowDisplayPlp started");
        driver.waitForPageToLoad();
        List<String> inventoryMessages = new ArrayList<>();
        WebElement productSection = commonActions.getPLPResultTopElement(productCode);
        List<WebElement> messages = productSection.findElements(CommonActions.inventoryMessageBy);

        for (WebElement message : messages) {
            inventoryMessages.add(message.getText());
        }

        if (Arrays.asList(inventoryMessages).toString().contains(ConstantsDtc.ORDER_NOW)) {
            Assert.assertTrue("FAIL: 'Need it Now? Call store' was NOT displayed for item '" + productCode + "'!",
                    driver.isElementDisplayed(productSection.findElement(CommonActions.toolTipNeedItNowBy)));
            String tooltipText =
                    productSection.findElement(CommonActions.toolTipNeedItNowBy).getText().replace("\n", "");
            Assert.assertTrue("FAIL: 'Need it Now? Call store' displayed for item '" + productCode + "' with text: '"
                            + tooltipText + "', does NOT contain expected text: '" + ConstantsDtc.NEED_IT_NOW,
                    CommonUtils.containsIgnoreCase(tooltipText, ConstantsDtc.NEED_IT_NOW));
        } else {
            List<WebElement> needItNowElements = webDriver.findElements(CommonActions.resultRowNeedItNowBy);
            for (WebElement element : needItNowElements) {
                if (driver.getParentElement(driver.getParentElement(element)).findElement(plpItemNumberBy)
                        .getText().contains(productCode))
                    Assert.fail("FAIL: Need it Now? Call store displayed for item: '" + productCode + "'!");
            }
        }
        LOGGER.info("assertNeedItNowDisplayPlp completed");
    }

    /**
     * Assert link is displayed for item on the product list page
     *
     * @param productCode     product code
     * @param linktext linktext to validate
     */
    public void assertLinkIsDisplayedForItem(String productCode, String linktext) {
        LOGGER.info("assertLinkIsDisplayedForItem started for " + productCode + " & Link " + linktext);
        assertItemOnProductListPage(productCode);

        WebElement row = commonActions.getPLPResultTopElement(productCode);
        Assert.assertTrue("FAIL : Link " + linktext + " did NOT display for the itemCode" + productCode,
                driver.isElementDisplayed(row.findElement(By.linkText(linktext))));

        LOGGER.info("assertLinkIsDisplayedForItem completed for " + productCode + " & Link " + linktext);
    }

    /**
     * Verifies that the currently displayed products on PLP are "In Stock" i.e. have an enabled 'Add to Cart' button
     *
     * @param assertInStock boolean, expected result
     */
    public void assertAllProductsInStock(boolean assertInStock) {
        LOGGER.info("assertAllProductsInStock started");
        assertAllProductsState(ConstantsDtc.IN_STOCK, assertInStock);
        LOGGER.info("assertAllProductsInStock completed");
    }

    /**
     * Verifies that the currently displayed products on PLP are "On Promotion" i.e. have promotional messaging
     * displayed as part of their product row
     *
     * @param assertOnPromotion boolean, expected result
     */
    public void assertAllProductsOnPromotion(boolean assertOnPromotion) {
        LOGGER.info("assertAllProductsOnPromotion started");
        assertAllProductsState(ConstantsDtc.TIRES_ON_PROMOTION, assertOnPromotion);
        LOGGER.info("assertAllProductsOnPromotion completed");
    }

    /**
     * Verify filtered state of products on all pages of PLP
     */
    private void assertAllProductsState(String state, Boolean stateTrueOrFalse) {
        boolean nextButtonFound = true;
        while (nextButtonFound) {
            driver.waitForPageToLoad();
            List<WebElement> results = commonActions.getPLPResultTopElements(null);
            for (WebElement result : results) {
                //On Promotion
                if (state.equals(ConstantsDtc.TIRES_ON_PROMOTION)) {
                    By promotionElement;
                    if (Config.isMobile()) {
                        promotionElement = promotionDiscountMobileBy;
                    } else {
                        promotionElement = CommonActions.promotionDiscountBy;
                    }
                    Assert.assertEquals("FAIL : Promotion text not found for some PLP results!",
                            stateTrueOrFalse,
                            !result.findElements(promotionElement).isEmpty());
                    //In Stock
                } else if (state.equals(ConstantsDtc.IN_STOCK)) {
                    By addToCartElement;
                    if (Config.isMobile()) {
                        addToCartElement = addToCartMobileBy;
                    } else {
                        addToCartElement = CommonActions.addToCartByClassBy;
                    }
                    Assert.assertEquals("FAIL : Expected PLP results In Stock status to be " + stateTrueOrFalse,
                            stateTrueOrFalse, driver.getDisplayedElement(result, addToCartElement, Constants.ZERO).isEnabled());
                }
                //TODO: Expand further? Maybe state = "Full-Sized Product Image" for 15491?
                //In which case stateTrueOrFalse is always false for mobile, and always true for web, I think.
            }
            if (driver.getDisplayedElementsList(CommonActions.nextBtnBy).size() == 2) {
                driver.getDisplayedElement(CommonActions.nextBtnBy, Constants.ZERO).click();
            } else if (driver.getDisplayedElementsList(CommonActions.nextBtnBy).size() == 3) {
                driver.getDisplayedElementsList(CommonActions.nextBtnBy).get(1).click();
            } else if (driver.getDisplayedElementsList(CommonActions.nextBtnBy).size() == 4) {
                driver.getDisplayedElementsList(CommonActions.nextBtnBy).get(2).click();
            } else {
                nextButtonFound = false;
            }

            if (commonActions.getCurrentUrl().contains(ConstantsDtc.PAGE_FIVE)) {
                break;
            }
        }
    }

    /**
     * Verifies value selected in the Filter Facet on the PLP  matches with results on the PLP page
     *
     * @param value Selected from filter section
     */

    public void verifyListedProductsMatchesWithSelectedValue(String value) {
        LOGGER.info("verifyListedProductsMatchesWithSelectedValue started");
        driver.waitForPageToLoad();

        boolean sizeMatched = true;
        List<WebElement> displayedProductSizes = webDriver.findElements(tireSizeBy);
        List<WebElement> items = webDriver.findElements(plpItemNumberBy);
        int row = 0;

        for (WebElement displayedProductSize : displayedProductSizes) {
            if (!displayedProductSize.getText().split(" ")[0].trim().equalsIgnoreCase(value)) {
                sizeMatched = false;
                row = displayedProductSizes.indexOf(displayedProductSize);
                break;
            }
        }

        Assert.assertTrue("FAIL: Product size " + displayedProductSizes.get(row).getText() + " for '"
                + items.get(row).getText() + "' did NOT match with selected filter value : "
                + value, sizeMatched);

        LOGGER.info("verifyListedProductsMatchesWithSelectedValue completed");
    }

    /**
     * Extracts the PLP results item count. Grabs the header portion of the PLP item container, and uses regex to
     * strip away any non-numeric values, giving us the total count of items returned
     *
     * @return Total number of items found and viewable on the PLP
     */
    public int extractResultsCountFromPlp() {
        LOGGER.info("extractResultsCountFromPlp started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(plpResultsHeader);
        String resultListCountText = plpResultsHeader.getText();
        int resultsCount;

        resultListCountText = resultListCountText.replaceAll("\\D+", "");

        if (resultListCountText.isEmpty() || StringUtils.isBlank(resultListCountText)) {
            Assert.fail("FAIL: PLP Results header did NOT contain the number of search results found!");
        }

        resultsCount = Integer.parseInt(resultListCountText);
        LOGGER.info("extractResultsCountFromPlp completed with result item count of: '" + resultsCount + "'");
        return resultsCount;
    }

    /**
     * Verifies that the current PLP result item count matches the specified item count
     *
     * @param expectedCount The expected PLP result item count
     */
    public void verifyPlpResultCountMatchesExpectation(int expectedCount) {
        LOGGER.info("verifyPlpResultCountMatchesExpectation started with previous PLP results count of '"
                + expectedCount + "'");
        int currentPlpResultsCount = extractResultsCountFromPlp();
        Assert.assertEquals("FAIL: The PLP results count did NOT match!", expectedCount, currentPlpResultsCount);
        LOGGER.info("verifyPlpResultCountMatchesExpectation completed with previous PLP results count of '"
                + expectedCount + "'");
    }

    /**
     * Verifies the placeholder or search string text in the "Brands" filter section search bar matches expectation
     *
     * @param textType       Placeholder text (text displayed before a value is entered) or search string text
     * @param textToValidate Expected placeholder textToValidate of the "Brands" search bar
     */
    public void verifyBrandsFilterSearchBarContainsText(String textType, String textToValidate) {
        LOGGER.info("verifyBrandsFilterSearchBarContainsText started for '" + textType + "': '"
                + textToValidate + "'");
        commonActions.checkForNoResultsMessage();
        driver.waitForElementVisible(brandFilterSearchBar);
        String currentSearchBarText;

        if (textType.toLowerCase().contains(Constants.ATTRIBUTE_PLACEHOLDER)) {
            currentSearchBarText = brandFilterSearchBar.getAttribute(Constants.ATTRIBUTE_PLACEHOLDER);
            textToValidate = SEARCH_BRAND_MESSAGING;
        } else {
            currentSearchBarText = brandFilterSearchBar.getAttribute(Constants.VALUE);
        }

        Assert.assertTrue("FAIL: The 'Brands' search bar did not have the expected '" + textType + "': '"
                        + textToValidate + "'! Actual: '" + currentSearchBarText + "'",
                currentSearchBarText.equalsIgnoreCase(textToValidate));
        LOGGER.info("verifyBrandsFilterSearchBarContainsText completed for '" + textType + "': '"
                + textToValidate + "'");
    }

    /**
     * Verifies "Show More" or "Show Less" link is either displayed or not displayed for a specified filter section on
     * the PLP
     *
     * @param showLinkType  "Show More" or "Show Less" link
     * @param displayCheck  Display expectation for the "Show More" or "Show Less" link
     * @param filterSection Text of the filter section to be checked for a "Show More" or "Show Less" link
     */
    public void verifyShowMoreOrLessDisplayForFilterSection(String showLinkType, String displayCheck,
                                                            String filterSection) {
        LOGGER.info("verifyShowMoreOrLessDisplayForFilterSection started with '" + showLinkType
                + "' checking if it is '" + displayCheck + "' for filter section '" + filterSection + "'");
        driver.waitForPageToLoad();
        commonActions.checkForNoResultsMessage();
        driver.waitForElementVisible(searchFilterSectionBy);

        Boolean linkDisplayed = driver.isElementDisplayed(getShowMoreOrLessLinkForFilterSection(showLinkType,
                filterSection), Constants.TWO);

        if (displayCheck.equalsIgnoreCase(Constants.NOT_DISPLAYED)) {
            Assert.assertFalse("FAIL: The '" + showLinkType + "' link was displayed for the '" + filterSection
                    + "' section!", linkDisplayed);
        } else {
            Assert.assertTrue("FAIL: The '" + showLinkType + "' link was NOT displayed for the '" + filterSection
                    + "' section!", linkDisplayed);
        }
        LOGGER.info("verifyShowMoreOrLessDisplayForFilterSection completed with '" + showLinkType
                + "' checking if it is '" + displayCheck + "' for filter section '" + filterSection + "'");
    }

    /**
     * Enters a text string into the
     *
     * @param searchText Text to be entered into the "Brands" search bar
     */
    public void enterTextIntoBrandsSearchBar(String searchText) {
        LOGGER.info("enterTextIntoBrandsSearchBar started with search text: '" + searchText + "'");
        driver.jsScrollToElementClick(brandFilterSearchBar, false);
        commonActions.clearAndPopulateEditField(brandFilterSearchBar, searchText);
        LOGGER.info("enterTextIntoBrandsSearchBar completed with search text: '" + searchText + "'");
    }

    /**
     * Verifies that the current string of chars entered into the "Brands" search bar is equal to or less than a
     * specified limit (25 chars as of 8/10/2018)
     *
     * @param charLimit The max number or char limit for the "Brands" search bar on the PLP
     */
    public void verifyUpToCharLimitEnteredInBrandsSearchBar(int charLimit) {
        LOGGER.info("verifyUpToCharLimitEnteredInBrandsSearchBar started with a char limit of '" + charLimit + "'");
        driver.waitForElementVisible(brandFilterSearchBar);

        int currentCharCount = brandFilterSearchBar.getAttribute(Constants.VALUE).length();

        Assert.assertTrue("FAIL: The current char count of '" + currentCharCount
                        + "' is greater than the expected limit of '" + charLimit + "' for the 'Brands' search bar!",
                currentCharCount <= charLimit);

        LOGGER.info("verifyUpToCharLimitEnteredInBrandsSearchBar completed with a char limit of '" + charLimit
                + "'");
    }

    /**
     * Clears the search bar under the "Brands" filter on the PLP
     */
    public void clearBrandsSearchBar() {
        LOGGER.info("clearBrandsSearchBar started");
        driver.waitForElementVisible(brandFilterSearchBar);
        commonActions.clearEditField(brandFilterSearchBar);
        LOGGER.info("clearBrandsSearchBar completed");
    }

    /**
     * Verifies the current options displayed in the "Brands" filter section contain the specified search text
     *
     * @param searchText Search term that should be contained within the displayed "Brands" filter options
     */
    public void verifyOptionsContainingTextDisplayForBrandsFilterSection(String searchText) {
        LOGGER.info("verifyOptionsContainingTextDisplayForBrandsFilterSection started with search text: '"
                + searchText + "'");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(brandFilterSearchBar);

        List<WebElement> brandsFilterOptionList =
                driver.getDisplayedElementsList(getFilterSectionElement(ConstantsDtc.BRANDS)
                        .findElements(filterOptionLabelBy));

        for (WebElement option : brandsFilterOptionList) {
            Assert.assertTrue("FAIL: The displayed option '" + option.getText()
                            + "' does NOT contain the search string/text: '" + searchText + "'!",
                    option.getText().toLowerCase().contains(searchText.toLowerCase()));
        }
        LOGGER.info("verifyOptionsContainingTextDisplayForBrandsFilterSection completed with search text: '"
                + searchText + "'");
    }

    /**
     * Verifies the specified filter section is currently expanded on the PLP
     *
     * @param filterSection Name of the filter section that should be expanded
     */
    public void verifyFilterSectionIsExpanded(String filterSection) {
        LOGGER.info("verifyFilterSectionIsExpanded started for '" + filterSection + "' filter section");
        driver.waitForElementVisible(searchFilterSectionBy, Constants.ZERO);
        WebElement filterSectionExpandIcon = getFilterSectionExpandCollapseIcon(filterSection);
        Assert.assertTrue("FAIL: ", filterSectionExpandIcon.getAttribute(Constants.CLASSNAME).contains(iconOpenClass));
        LOGGER.info("verifyFilterSectionIsExpanded completed for '" + filterSection + "' filter section");
    }

    /**
     * Returns the expand/collapse icon for a specified filter section on the PLP
     *
     * @param filterSection Name of the filter section from which to retrieve the expand/collapse icon
     * @return WebElement of the expand/collapse icon for the specified filter section on the PLP
     */
    private WebElement getFilterSectionExpandCollapseIcon(String filterSection) {
        LOGGER.info("getFilterSectionExpandCollapseIcon started for '" + filterSection + "' filter section");
        WebElement expandIconElement = null;
        try {
            expandIconElement = getFilterSectionElement(filterSection).findElement(
                    openFilterSectionIconBy);
        } catch (Exception e) {
            Assert.fail("FAIL: An exception occurred when attempting to get the expand icon for the '" + filterSection
                    + "' filter section! Exception: \n\t" + e);
        }
        LOGGER.info("getFilterSectionExpandCollapseIcon completed for '" + filterSection + "' filter section");
        return expandIconElement;
    }

    /**
     * Verifies the "Price Range" filter slider control has two handles (min and max) for selection
     */
    public void verifyPriceRangeSliderFilterHasTwoHandles() {
        LOGGER.info("verifyPriceRangeSliderFilterHasTwoHandles started");
        driver.waitForElementVisible(searchFilterSectionBy);
        Assert.assertTrue("FAIL: The 'Price Range' slider filter does NOT appear to have both handles!",
                driver.isElementDisplayed(CommonActions.priceRangeMinimumPointBy, Constants.ZERO)
                        && driver.isElementDisplayed(CommonActions.priceRangeMaximumPointBy, Constants.ZERO));
        LOGGER.info("verifyPriceRangeSliderFilterHasTwoHandles completed");
    }

    /**
     * Extracts the price of the first available (displayed) product on the PLP as an int
     *
     * @return String containing value of the first product on the PLP with a displayed price.
     */
    public int extractFirstAvailableProductPriceOnPlpInt() {
        LOGGER.info("extractFirstAvailableProductPriceOnPlpInt started");
        int extractedPrice = 0;
        WebElement sortDropdown = driver.getDisplayedElement(CommonActions.reactSelectizePlaceholderBy, Constants.ZERO);
        String dropDownSelection = sortDropdown.getText();
        List<WebElement> rows = webDriver.findElements(plpResultsRowBy);
        boolean found = false;
        List<WebElement> prices = null;
        while (!found) {
            for (WebElement row : rows) {
                try {
                    prices = row.findElements(CommonActions.productPriceBy);
                    if (!CommonUtils.containsIgnoreCase(row.getText(), ConstantsDtc.PRICE_IN_CART)) {
                        found = true;
                        break;
                    }
                } catch (Exception e) {
                    //continue
                }
            }
        }
        Assert.assertTrue("Could not find any prices on the PLP page", found);
        for (WebElement price : prices) {
            String priceText = price.getText();
            String[] multiPrice = priceText.split("-");
            if (priceText.equalsIgnoreCase(ConstantsDtc.NO_PRICE)) {
                LOGGER.info("No Price is listed in the top row in PLP");
                return 0;
            }
            for (String singlePrice : multiPrice) {
                int roundedPrice = (int)(Math.round(Double.parseDouble(singlePrice.replace("$", ""))));
                if (extractedPrice == 0) {
                    extractedPrice = roundedPrice;
                } else if (dropDownSelection.equalsIgnoreCase(PRICE_LOW_TO_HIGH)) {
                    extractedPrice = Math.min(extractedPrice, roundedPrice);
                } else if (dropDownSelection.equalsIgnoreCase(PRICE_HIGH_TO_LOW)){
                    extractedPrice = Math.max(extractedPrice, roundedPrice);
                } else {
                    LOGGER.info("The PLP rows are not sorted by roundedPrice. Sort is set to '" + dropDownSelection + "'");
                }
            }
        }
        LOGGER.info("extractFirstAvailableProductPriceOnPlpInt completed. Extracted price: '" + extractedPrice + "'");
        return extractedPrice;
    }

    /**
     * Verifies that for the specified "Price Range" price point (minimum|maximum)
     *
     * @param pricePoint Which price point "Minimum" or "Maximum", the price value will be compared against
     */
    public void verifyFirstListedPriceMatchesMinimumOrMaximum(String pricePoint) {
        LOGGER.info("verifyProductPriceDisplaysAsPricePointInPriceRangeFilter started with '" + pricePoint + "'");
        int productPriceToVerify = extractFirstAvailableProductPriceOnPlpInt();
        if (productPriceToVerify == 0) {
            LOGGER.info("verifyProductPriceDisplaysAsPricePointInPriceRangeFilter completed. " +
                    "'No Price' was listed on the first PLP row. Add prices in backoffice to get a valid test");
            return;
        } else {
            driver.waitForElementVisible(searchFilterSectionBy);
            String priceRangePoint = CommonActions.MIN;
            if (pricePoint.equalsIgnoreCase(Constants.MAXIMUM))
                priceRangePoint = CommonActions.MAX;
            int currentPricePointValue = Integer.parseInt(commonActions.getCurrentPriceRangePoint(priceRangePoint));
            if (pricePoint.equalsIgnoreCase(Constants.MAXIMUM)) {
                Assert.assertTrue("FAIL: The product price '" + productPriceToVerify +
                        "' was not less than the maximum '" + pricePoint + "' price point value of '" +
                        currentPricePointValue + "'", productPriceToVerify - currentPricePointValue <= 0);
            } else {
                Assert.assertTrue("FAIL: The product price '" + productPriceToVerify +
                        "' was not greater than the minimum '" + pricePoint + "' price point value of '" +
                        currentPricePointValue + "'", productPriceToVerify - currentPricePointValue >= 0);
            }
        }
        LOGGER.info("verifyProductPriceDisplaysAsPricePointInPriceRangeFilter completed with '" + pricePoint
                + "' price point verified to be '" + productPriceToVerify + "'");
    }

    /**
     * This method will return the By element matching the input String.
     *
     * @param elementName - Name of the By element
     * @return - By
     */
    private By returnByElement(String elementName) {
        LOGGER.info("returnByElement started");
        switch (elementName) {
            case COMPARE_ELEMENT:
                return compareButtonBy;
            case SUB_TOTAl_WITH_TOOL_TIP:
                return CommonActions.subTotalWithToolTipBy;
            case NEED_IT_NOW_WITH_TOOL_TIP:
                return toolTipNeedItNowBy;
            case TOOL_TIP:
                return toolTipBy;
            case CHECK_NEARBY_STORES_BANNER:
                return CommonActions.checkNearByStoresBy;
            case ConstantsDtc.FOUND_IT_LOWER:
                return foundItLowerBy;
            case ConstantsDtc.INSTANT_PRICE_MATCH:
                return CommonActions.lowerPriceLinkBy;
            case BEST_BETTER_GOOD_BANNER:
                return bestBetterGoodBy;
            case PROMOTIONS:
                return promotionsBy;
            case ConstantsDtc.AVAILABLE_NOW:
                return availableTodayBy;
            case SHOPPING_CART_PAGE:
                return shoppingCartBy;
            case FREE_SHIPPING:
                return CommonActions.freeShippingBy;
            case MILE_WARRANTY:
                return mileWarranty;
            default:
                Assert.fail("FAIL: Could not find By objects that matched string passed from step");
        }
        return null;
    }

    /**
     * This method will return the Web element matching the input String.
     *
     * @param elementName - Name of the element
     * @return - WebElement
     */
    private WebElement returnWebElement(String elementName) {
        LOGGER.info("returnWebElement started for element name: " + elementName);
        switch (elementName) {
            case TOOL_TIP:
                return webDriver.findElements(toolTipBy).get(1);
            case ConstantsDtc.SUB_TOTAL_TOOL_TIP:
                return webDriver.findElements(CommonActions.subTotalToolTipBy).get(6);
            case ConstantsDtc.TOOL_TIP_CONTENT:
                return CommonActions.toolTipContent;
            case PROMOTIONS:
                return webDriver.findElements(promotionsBy).get(0);
            case ConstantsDtc.VIEW_DETAILS:
                return viewDetailsButton;
            case ConstantsDtc.ADD_TO_CART:
                return CommonActions.addToCart;
            case ConstantsDtc.ENTER_VEHICLE:
                return CommonActions.enterVehicleLink;
            case Constants.PACKAGE:
                return packageDetails;
            default:
                Assert.fail("FAIL: Could not find By objects that matched string passed from step");
                return null;
        }
    }


    /**
     * This method will verify the text attribute of list of elements
     *
     * @param elementName send in elementName and gets the By from returnByElement method.
     * @param validation  String to validate against
     */
    public void verifyTextAttributeOfListOfElements(String elementName, String validation) {
        LOGGER.info("verifyTextAttributeOfListOfElements started");
        By by = returnByElement(elementName);
        List<WebElement> elements = webDriver.findElements(by);
        for (int i = 0; i <= elements.size() - 1; i++) {
            driver.waitForElementVisible(elements.get(i));
            String text = elements.get(i).getText().toLowerCase();
            Assert.assertTrue("FAIL: The expected text of - " + validation + " was Not Found, " +
                    "This Text was found - " + text, text.contains(validation.toLowerCase()));
        }
        LOGGER.info("verifyTextAttributeOfListOfElements completed");
    }

    /**
     * This method verifies that list of elements specified are displayed
     *
     * @param elementName send in elementName and gets the By from returByElement method.
     */
    public void verifyIfEachElementInTheListIsDisplayed(String elementName) {
        LOGGER.info("verifyIfEachElementInTheListIsDisplayed started");
        By by = returnByElement(elementName);
        List<WebElement> elements = webDriver.findElements(by);
        for (int i = 0; i <= elements.size() - 1; i++) {
            Assert.assertTrue("FAIL: Element: " + elementName + " is not displayed here",
                    driver.isElementDisplayed(elements.get(i)));
        }
        LOGGER.info("verifyIfEachElementInTheListIsDisplayed completed");
    }

    /**
     * This method verifies the text attribute of list of alternate elements
     *
     * @param elementName send in elementName and gets the By element from returnByElement method.
     * @param validation  String to validate against
     */
    public void verifyTextAttributeOfListOfElementsAlternateElement(String elementName, String validation) {
        LOGGER.info("verifyTextAttributeOfListOfElementsAlternateElement started");
        By by = returnByElement(elementName);
        List<WebElement> elements = webDriver.findElements(by);
        for (int i = 0; i <= elements.size() - 1; i = i + 2) {
            driver.waitForElementVisible(elements.get(i));
            String text = elements.get(i).getText().toLowerCase();
            Assert.assertTrue("FAIL: The expected text of - " + validation + " was Not Found, " +
                    "This Text was found - " + text, text.contains(validation.toLowerCase()));
        }
        LOGGER.info("verifyTextAttributeOfListOfElementsAlternateElement completed");
    }

    /**
     * Click method for ProductListPage.java class
     *
     * @param elementName - send in elementName and gets the Web Element from returnWebElement method.
     */
    public void clickInPlp(String elementName) {
        LOGGER.info("clickInPlp started");
        driver.waitForElementClickable(returnWebElement(elementName));
        WebElement element = returnWebElement(elementName);
        driver.jsScrollToElementClick(element);
        LOGGER.info("clickInPlp completed");
    }

    /**
     * verifies the text Attributes of the Web Elements
     *
     * @param elementName - send in elementName and gets the Web Element from returnWebElement method.
     * @param validation  - String to validate against
     */
    public void verifyTextOfElementPlp(String elementName, String validation) {
        LOGGER.info("verifyTextOfElementPlp started");
        driver.waitForElementVisible(returnWebElement(elementName));
        WebElement element = returnWebElement(elementName);
        String text = element.getText().toLowerCase();
        Assert.assertTrue("FAIL: The expected text of - " + validation + " was Not Found, " +
                "This Text was found - " + text, text.contains(validation.toLowerCase()));
        LOGGER.info("verifyTextOfElementPlp completed");
    }

    public void addElementToCart(String status) {
        LOGGER.info("addElementToCart started");
        boolean itemFound = setElementAndClickAddToCart(status);
        if (!itemFound) {
            commonActions.navToDifferentPageOfResults(Constants.NEXT);
            LOGGER.info("Navigate to next page.");
            itemFound = setElementAndClickAddToCart(status);
        }
        if (!itemFound) {
            Assert.fail("Item with status of " + status + " is Not available in page 1 and page 2");
        }
        LOGGER.info("addElementToCart completed");
    }

    /**
     * This method will set the product according to the status and clicks on add to cart button associated with item
     * and status
     *
     * @param status - Availability status
     */
    private boolean setElementAndClickAddToCart(String status) {
        LOGGER.info("setElementAndClickAddToCart started");
        List<WebElement> elements = webDriver.findElements(CommonActions.inventoryMessageBy);
        boolean itemFound = false;
        for (int i = 0; i <= elements.size() - 1; i++) {
            String elementStatus = elements.get(i).getText();
            if (elementStatus.toLowerCase().contains(status.toLowerCase())) {
                driver.jsScrollToElementClick(webDriver.findElements(CommonActions.addToCartByClassBy).get(i));
                itemFound = true;
                break;
            }
        }
        LOGGER.info("setElementAndClickAddToCart completed");
        return itemFound;
    }

    /**
     * verifies the presence of driving details block on page
     *
     * @param visibility visibility type to be verified - (displays|not displays)
     */
    public void assertDrivingDetailsBlock(String visibility) {
        LOGGER.info("assertDrivingDetailsBlock started");
        if (visibility.equalsIgnoreCase(Constants.DISPLAYS)) {
            Assert.assertTrue("FAIL: The expected Driving Details block is not displayed with the text " +
                            DRIVING_DETAILS + " on the PLP",
                    drivingDetailsBlockHeader.getAttribute(Constants.INNERTEXT).toLowerCase().contains(DRIVING_DETAILS));
        } else {
            Assert.assertFalse("FAIL: The unexpected Driving Details block is displayed with the text " +
                            DRIVING_DETAILS + " on the PLP",
                    driver.isElementDisplayed(drivingDetailsBlockHeader));
        }
        LOGGER.info("assertDrivingDetailsBlock completed");
    }

    /**
     * Select cancel button on PDL entry Fitment Panel
     */
    public void selectCancelPdlFitmentPanel() {
        LOGGER.info("selectCancelPdlFitmentPanel started");
        driver.waitForElementClickable(fitmentPdlEntryCancelButton);
        fitmentPdlEntryCancelButton.click();
        LOGGER.info("selectCancelPdlFitmentPanel completed");
    }

    /**
     * Verify the type of product displayed on Top 3 PDL Tiles
     *
     * @param type String value of Top 3 PDL Container
     */
    public void assertPDLTile(String type) {
        LOGGER.info("assertPDLTile started");
        if (type.equalsIgnoreCase(ConstantsDtc.BEST_SELLER)) {
            Assert.assertTrue("FAIL: Top3 PDL Tile - " + type + " is not displayed on the Top 3 Tiles", driver.isElementDisplayed(pdlBestSellerContainer));
        } else if (type.equalsIgnoreCase(OUR_RECOMMENDATION)) {
            Assert.assertTrue("FAIL: Top3 PDL Tile - " + type + " is not displayed on the Top 3 Tiles", driver.isElementDisplayed(ourRecommendationBannerBy));
        } else if (type.equalsIgnoreCase(ORIGINAL_EQUIPMENT)) {
            Assert.assertTrue("FAIL: Top3 PDL Tile - " + type + " is not displayed on the Top 3 Tiles", driver.isElementDisplayed(pdlOriginalequipmemtContainer));
        } else if (type.equalsIgnoreCase(HIGHEST_RATED)) {
            Assert.assertTrue("FAIL: Top3 PDL Tile - " + type + " is not displayed on the Top 3 Tiles", driver.isElementDisplayed(pdlHighestRatedContainer));
        }
        LOGGER.info("assertPDLTile completed");
    }

    /**
     * Click item in Top 3 Tiles with the given position
     *
     * @param position The position of the expected brand
     */
    public void selectProductInTop3Tiles(int position) {
        LOGGER.info("selectProductInTop3Tiles started for product at position " + position);
        List<WebElement> top3Products;
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        if (Config.isMobile()) {
            top3Products = webDriver.findElements(top3titleLinkBy);
        } else {
            top3Products = webDriver.findElements(top3TilesBrandBy);
        }
        driver.jsScrollToElementClick(top3Products.get(position - 1));
        LOGGER.info("selectProductInTop3Tiles completed for product at position " + position);
    }

    /**
     * Checks if no longer available product's brand replacement button's redirection is correct
     *
     * @param sizeCriteria no longer available product size
     * @param targetBrand  replacement brand
     */
    public void assertRedirectToTargetSearchResultsPage(String sizeCriteria, String targetBrand) {
        LOGGER.info("assertRedirectToTargetSearchResultsPage started");
        commonActions.assertResultsMessageContains(ConstantsDtc.PLP, sizeCriteria);
        if (Config.isMobile())
            openFilterSectionBtn.click();
        if (targetBrand.equalsIgnoreCase(Constants.NONE)) {
            verifyNoSearchRefinementsAreApplied();
        } else {
            verifyPlpCheckboxFilterSelected(targetBrand);
            verifySearchRefinementFilterValues("single", targetBrand);
        }
        LOGGER.info("assertRedirectToTargetSearchResultsPage completed");
    }

    /**
     * checks weather the treadwell details section is present if the treadwell service is available and is absent if the
     * treadwell service is not available
     *
     * @param option if details is present or absent
     */

    public void assertTreadwellDetailsBox(String option) {
        LOGGER.info("assertTreadwellDetailsBox started");
        driver.waitForMilliseconds();
        if (option.equalsIgnoreCase(ConstantsDtc.PRESENT)) {
            Assert.assertTrue("FAIL: Treadwell details box was not displayed",
                    driver.isElementDisplayed(treadwellDetailsBox));
            Assert.assertTrue("FAIL: Treadwell logo was not displayed",
                    driver.isElementDisplayed(treadwellLogoDetailsBox));
        } else {
            Assert.assertFalse("FAIL: Treadwell details box was displayed",
                    driver.isElementDisplayed(treadwellDetailsBox));
        }
        LOGGER.info("assertTreadwellDetailsBox completed");
    }

    /**
     * checks weather the treadwell filter is present in the first position in quick filter
     *
     * @param option present or absent
     */
    public void assertTreadwellLogoPosition(String option) {
        LOGGER.info("assertTreadwellLogoPosition started");
        if (Config.isMobile()) {
            openFilterSectionBtn.click();
            if (option.equalsIgnoreCase(ConstantsDtc.PRESENT)) {
                Assert.assertTrue("FAIL:treadwell logo is not present at the first position in quick filters",
                        driver.isElementDisplayed(treadwellLogoMobile));
            } else {
                Assert.assertFalse("FAIL:treadwell logo is present at the first position in quick filters",
                        driver.isElementDisplayed(treadwellLogoMobile));
            }
            CommonActions.closeIcon.click();
        } else {
            if (option.equalsIgnoreCase(ConstantsDtc.PRESENT)) {
                Assert.assertTrue("FAIL:treadwell logo is not present at the first position in quick filters",
                        driver.isElementDisplayed(treadwellLogo));
            } else {
                Assert.assertFalse("FAIL:treadwell logo is present at the first position in quick filters",
                        driver.isElementDisplayed(treadwellLogo));
            }
        }
        LOGGER.info("assertTreadwellLogoPosition completed");
    }

    /**
     * checks weather the error message is present if the treadwell service is not available and is not present if the
     * treadwell service is available and checks the message that is displayed
     *
     * @param message error message text
     */
    public void assertTreadwellErrorMessage(String message) {
        LOGGER.info("assertTreadwellErrorMessage started");
        Boolean errorMessageMatch = false;
        List<WebElement> errorMessageBox = webDriver.findElements(errorMessageBy);
        for (int i = 0; i < errorMessageBox.size(); i++) {
            String errorMessage = errorMessageBox.get(i).getText();
            if (message.equalsIgnoreCase(errorMessage)) {
                errorMessageMatch = true;
            }
        }
        Assert.assertTrue("FAIL: Treadwell error message does not match with the displayed error message",
                errorMessageMatch);
        Assert.assertTrue("FAIL: Treadwell error message is not displayed", errorMessageMatch);
        LOGGER.info("assertTreadwellErrorMessage completed");
    }

    /**
     * Verify treadwell top recommended product displayed in top 3 tiles at 2nd position or not displayed when service is available
     *
     * @param option displayed or not displayed
     */
    public void assertTreadwellProductInTop3Tiles(String option) {
        LOGGER.info("assertTreadwellProductInTop3Tiles started");
        List<WebElement> top3TileProducts;

        if (option.equalsIgnoreCase(ConstantsDtc.NOT_DISPLAYED)) {
            Assert.assertFalse("FAIL: Pdl search results did contain item with :" + ConstantsDtc.TOP_RECOMMENDATION +
                    " banner!", driver.isElementDisplayed(topRecommendationBy));
            LOGGER.info("Confirmed that '" + ConstantsDtc.TOP_RECOMMENDATION + "' banner not was listed on Pdl Driving "
                    + "Details search results.");
        } else {
            String treadwellTopRecommendedProduct = webDriver.findElement(productNameBy).getText().split("\n")[1];
            if (Config.isMobile()) {
                top3TileProducts = webDriver.findElements(top3TilesMobileBy);
            } else {
                top3TileProducts = webDriver.findElements(top3TilesBy);
            }
            String top3TreadwellProduct = top3TileProducts.get(1).getText();
            Assert.assertTrue("FAIL: The 2nd product in top3 tiles is not a treadwell product",
                    driver.isElementDisplayed(top3TileProducts.get(1).findElement(top3TreadwellRecommendationTop))
                            && top3TreadwellProduct.contains(treadwellTopRecommendedProduct)
                            && driver.isElementDisplayed(CommonActions.topRecommendation));
        }
        LOGGER.info("assertTreadwellProductInTop3Tiles completed");
    }

    /**
     * verifies whether the items on PLP are in ascending order of ranking
     */
    public void assertTreadwellItemsRanking() {
        LOGGER.info("assertTreadwellItemsRanking started");
        List<WebElement> treadwellProducts = webDriver.findElements(CommonActions.bannerRecommendationBy);

        for (int i = 0; i <= treadwellProducts.size() - 1; i++) {
            String ranking;
            if (i == 0) {
                ranking = treadwellProducts.get(i).getAttribute(Constants.CLASS);
                Assert.assertTrue("FAIL: The top recommendation was not present in the first position of the list",
                        ranking.contains("topRecommendation"));
            } else {
                ranking = treadwellProducts.get(i).getText();
                int j = i + 1;
                String treadwellRanking = String.valueOf(j);
                Assert.assertTrue("FAIL: The items are not in the asending order", ranking.contains(treadwellRanking));
            }
        }
        LOGGER.info("assertTreadwellItemsRanking completed");
    }

    /**
     * verifies whether the product details container is present for all the products on the PLP
     */
    public void assertProductDetailsContainer() {
        LOGGER.info("assertProductDetailsContainer started");
        List<WebElement> productDetailsContainer = webDriver.findElements(drivingDetailsContainerBy);
        for (int i = 0; i <= productDetailsContainer.size() - 1; i++) {
            Assert.assertTrue("FAIL: The product details container is not displayed for product" + i, driver.isElementDisplayed(productDetailsContainer.get(i)));
        }
        LOGGER.info("assertProductDetailsContainer completed");
    }

    /**
     * Extracts the tire life and cost for the specified product on the PLP
     *
     * @param position product position on PLP
     * @return Tire life and cost
     */
    public String extractTireLifeAndCost(int position) {
        LOGGER.info("extractTireLifeAndCost started");
        String tireLifeAndCost = null;
        List<WebElement> productDetailsContainer = webDriver.findElements(drivingDetailsContainerBy);
        for (int i = 0; i <= productDetailsContainer.size() - 1; i++) {
            if (i == (position - 1)) {
                tireLifeAndCost = productDetailsContainer.get(i).getText().replace("\n", "");
                break;
            }
        }
        LOGGER.info("extractTireLifeAndCost completed");
        return tireLifeAndCost;
    }

    /**
     * Verifies the values of the tire life and cost from PLP with the values on PDP
     *
     * @param tireLifeAndCost tire life and cost values extracted from the PLP
     */
    public void assertTireLifeAndCost(String tireLifeAndCost) {
        LOGGER.info("assertTireLifeAndCost started");
        driver.jsScrollToElement(webDriver.findElement(treadwellTireLifeAndCostBy));
        driver.waitForPageToLoad();
        String tireLifeAndCostPDP = webDriver.findElement(treadwellTireLifeAndCostBy).getText().replace("\n", "");
        String tireLifeAndCostSubString = commonUtils.getSubstring(tireLifeAndCostPDP, ConstantsDtc.WARRANTY.toUpperCase(), DOLLAR);
        Assert.assertTrue("FAIL: The value of Tire Life and Cost didn't match", tireLifeAndCost.toLowerCase()
                .contains(tireLifeAndCostSubString.toLowerCase()));
        LOGGER.info("assertTireLifeAndCost completed");
    }

    /**
     * Verify the Canonical products are displayed on the PLP page
     *
     * @param expectCanonicalProducts true or false whether to expect Canonical products to be displayed
     */
    public void assertCanonicalProductsOnPlpPage(boolean expectCanonicalProducts) {
        LOGGER.info("assertCanonicalProductsOnPlpPage started");
        driver.waitForPageToLoad();
        if (expectCanonicalProducts)
            Assert.assertTrue("FAIL: Canonical products were not displayed on PLP page ",
                    driver.isElementDisplayed(viewDetailsButton));
        else
            Assert.assertTrue("FAIL: Canonical products were not displayed on PLP page ",
                    !driver.isElementDisplayed(viewDetailsButton));
        LOGGER.info("assertCanonicalProductsOnPlpPage completed");
    }

    /**
     * Verify displayed PLP by Brand
     *
     * @param brand The brand name that needs to be verified
     */
    public void assertPlpResultsByBrand(String brand) {
        LOGGER.info("assertPlpResultsByBrand started for: " + brand);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            if (driver.isElementDisplayed(brandNameBy)) {
                String brandName = productRow.findElement(brandNameBy).getText();
                Assert.assertTrue("FAIL: Canonical plp products are not grouped by brand: "
                        + brand, brandName.equalsIgnoreCase(brand));
            }
        }
        LOGGER.info("assertPlpResultsByBrand completed for: " + brand);
    }

    /**
     * Verify price details displayed on PLP products
     */
    public void assertPlpProductPrice() {
        LOGGER.info("assertPlpProductPrice started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            String productText = productRow.findElement(CommonActions.productLowerBlockBy).getText();
            if (driver.isElementDisplayed(CommonActions.productPriceBy)) {
                if (!productText.contains(Constants.SEE_PRICE_IN_CART)) {
                    String displayedPrice = productRow.findElement(CommonActions.productPriceBy).getText();
                    String priceRange = displayedPrice.replaceAll("\\$", "").replaceAll("\\s+", "");
                    if (driver.isElementDisplayed(viewDetailsButton)) {
                        if (priceRange.contains("-")) {
                            String[] parts = priceRange.split("-", 0);
                            String price1 = parts[0], price2 = parts[1];
                            double minimumRange = Double.parseDouble(price1);
                            double maximumRange = Double.parseDouble(price2);
                            Assert.assertTrue("FAIL: Minimum range is not less than the Maximum range. " +
                                    "product: " + productName, minimumRange < maximumRange);
                        }
                    } else {
                        Assert.assertTrue("FAIL: Non-canonical product '"
                                + productName + "' contains Price-Range " + displayedPrice, !displayedPrice.contains("-"));
                    }
                }
            } else {
                Assert.fail("FAIL: Price details not available for the product: " + productName);
            }
        }
        LOGGER.info("assertPlpProductPrice completed");
    }

    /**
     * Verify non-Canonical products details contain Item#
     */
    public void verifyItemNumber() {
        LOGGER.info("verifyItemNumber started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            if (driver.isElementDisplayed(CommonActions.addToCart)) {
                Assert.assertTrue("FAIL: Non-Canonical product '"
                                + productRow.findElement(productNameBy).getText() + "' does not contain Item#",
                        driver.isElementDisplayed(productRow.findElement(itemNumber)));
            }
        }
        LOGGER.info("verifyItemNumber completed");
    }

    /**
     * Verify Iconography and Tooltip text of plp products
     */
    public void verifyTooltipText() {
        LOGGER.info("verifyTooltipText started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            if (driver.isElementDisplayed(toolTipIconBy)) {
                List<WebElement> tooltipIcons = productRow.findElement(toolTipRowBy).findElements(toolTipIconBy);
                for (WebElement toolTipIcon : tooltipIcons) {
                    driver.jsScrollToElement(toolTipIcon);
                    new Actions(webDriver).moveToElement(toolTipIcon).perform();
                    String toolTipText = toolTipIcon.getText();
                    Assert.assertTrue("FAIL: Product " + productName +
                            " doesn't contain tooltip text for the icon", !toolTipText.equals(null));
                }
            }
        }
        LOGGER.info("verifyTooltipText completed");
    }

    /**
     * Verify the product(Tire/wheel) thumbnail image is displayed on PLP.
     */
    public void verifyProductThumbnailImageDisplayedForListedProducts() {
        LOGGER.info("verifyProductThumbnailImageDisplayedForListedProducts started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        List<WebElement> productRows = driver.getDisplayedElementsList(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            if (!driver.isElementDisplayed(productRow.findElement(productImageBy))) {
                Assert.fail("FAIL: The product image is not available for the product '"
                        + productRow.findElement(productNameBy).getText() + "'");
            }
        }
        LOGGER.info("verifyProductThumbnailImageDisplayedForListedProducts completed");
    }

    /**
     * Verify the subtotal with tooltip exist for non- canonical products on PLP page
     */
    public void verifySubtotalExistForStandardProduct() {
        LOGGER.info("verifySubtotalExistForStandardProduct started");
        driver.waitForPageToLoad();
        List<WebElement> productRows = driver.getDisplayedElementsList(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            String productText = productRow.findElement(CommonActions.productLowerBlockBy).getText().toLowerCase();
            if (productText.contains(ConstantsDtc.ADD_TO_CART.toLowerCase())) {
                Assert.assertTrue("FAIL: Subtotal with tool tip of standard product: " + productName +
                        " doesn't exist on PLP page", subTotalTooltip.isDisplayed());
            } else {
                LOGGER.info("Its a Canonical Product: " + productName);
            }
        }
        LOGGER.info("verifySubtotalExistForStandardProduct completed");
    }

    /**
     * Verify the subtotal on PLP page using retail price and qty for on PLP page
     */
    public void assertSubtotalCalculation() {
        LOGGER.info("assertSubtotalCalculation started");
        driver.waitForPageToLoad();
        List<WebElement> productRows = driver.getDisplayedElementsList(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            String productText = productRow.findElement(CommonActions.productLowerBlockBy).getText();
            if (productRow.findElement(CommonActions.productPriceBy).isDisplayed()) {
                if (!productText.contains(Constants.SEE_PRICE_IN_CART)) {
                    String price = productRow.findElement(CommonActions.productPriceBy).getText();
                    if (productText.contains(ConstantsDtc.SUBTOTAL)) {
                        price = price.replaceAll("\\$", "").replaceAll("\\s+", "");
                        double retailPrice = Double.parseDouble(price);
                        String quantity = CommonActions.productQuantityBox.getAttribute(Constants.VALUE);
                        int qty = Integer.parseInt(quantity);
                        double expectedPrice = retailPrice * qty;
                        String totalPrice = productRow.findElement(subTotalPrice).getText();
                        totalPrice = totalPrice.replaceAll("\\$", "").replaceAll("\\s+", "")
                                .replaceAll(",", "");
                        double actualPrice = Double.parseDouble(totalPrice);
                        Assert.assertTrue("FAIL: Subtotal of product: " + productName +
                                " is incorrect, Actual price: " + totalPrice + "Expected price: "
                                + expectedPrice, expectedPrice == actualPrice);
                    } else {
                        LOGGER.info("Its a Canonical Product: " + productName);
                    }
                }
            } else {
                Assert.fail("FAIL: Product: " + productName + " doesn't contain retail price.");
            }
        }
        LOGGER.info("assertSubtotalCalculation completed");
    }

    /**
     * Verify the subtotal on PLP page using retail price and qty on PLP page for staggered products SETS tab
     */
    public void assertSubtotalCalculationOnSetsTab() {
        LOGGER.info("assertSubtotalCalculationOnSetsTab started");
        driver.waitForPageToLoad();
        List<WebElement> productRows = driver.getDisplayedElementsList(plpResultsRowBy);
        double expectedPrice = 0;
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            String productText = productRow.findElement(CommonActions.productLowerBlockBy).getText();
            double subTotal = 0;
            if (!productText.contains(Constants.SEE_PRICE_IN_CART)) {
                List<WebElement> itemizedRows = productRow.findElements(productRowBlockBy);
                for (WebElement itemizedRow : itemizedRows) {
                    if (itemizedRow.findElement(CommonActions.productPriceBy).isDisplayed()) {
                        String price = itemizedRow.findElement(CommonActions.productPriceBy).getText();
                        if (productText.contains(ConstantsDtc.SUBTOTAL)) {
                            price = price.replaceAll("\\$", "").replaceAll("\\s+", "");
                            double retailPrice;
                            retailPrice = Double.parseDouble(price);
                            String quantity = itemizedRow.findElement(CommonActions.productQuantityBoxBy).
                                    getAttribute(Constants.VALUE);
                            int qty = Integer.parseInt(quantity);
                            subTotal = retailPrice * qty;
                            expectedPrice = expectedPrice + subTotal;
                        } else {
                            LOGGER.info("Its a Canonical Product: " + productName);
                        }
                    } else {
                        Assert.fail("FAIL: Product: " + productName + " doesn't contain retail price.");
                    }
                }
                String totalPrice = productRow.findElement(subTotalPrice).getText();
                totalPrice = totalPrice.replaceAll("\\$", "").replaceAll("\\s+", "")
                        .replaceAll(",", "");
                double actualPrice = Double.parseDouble(totalPrice);
                Assert.assertTrue("FAIL: Subtotal of product: " + productName + " is incorrect, Actual price: "
                        + totalPrice + "Expected price: " + expectedPrice, expectedPrice == actualPrice);
                expectedPrice = 0;
            }
        }
        LOGGER.info("assertSubtotalCalculationOnSetsTab completed");
    }

    /**
     * Verify the Quantity of the Front and Rear products of plp
     */
    public void assertFrontRearQty() {
        LOGGER.info("assertFrontRearQty started");
        driver.waitForPageToLoad();
        List<WebElement> productRows = driver.getDisplayedElementsList(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productText = productRow.findElement(CommonActions.productLowerBlockBy).getText();
            if (productText.contains(ConstantsDtc.SUBTOTAL)) {
                String quantity = CommonActions.productQuantityBox.getAttribute(Constants.VALUE);
                int qty = Integer.parseInt(quantity);
                Assert.assertTrue("FAIL: Quantity of product: "
                        + productRow.findElement(productNameBy).getText() + " is incorrect", qty == 2);
            } else {
                LOGGER.info("Its a Canonical Product: " + productRow.findElement(productNameBy).getText());
            }
        }
        LOGGER.info("assertFrontRearQty completed");
    }

    /**
     * Verify Add To Cart is displayed in Standard plp
     */
    public void assertAddToCartDisplay() {
        LOGGER.info("assertAddToCartDisplay started");
        driver.waitForPageToLoad();
        List<WebElement> productRows = driver.getDisplayedElementsList(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productText = productRow.findElement(CommonActions.productLowerBlockBy).getText();
            if (productText.contains(ConstantsDtc.SUBTOTAL)) {
                Assert.assertTrue("FAIL: Add to cart button is not displayed for the product '"
                                + productRow.findElement(productNameBy).getText() + "'",
                        driver.isElementDisplayed(CommonActions.addToCart));
            } else {
                LOGGER.info("Its a Canonical Product: " + productRow.findElement(productNameBy).getText());
            }
        }
        LOGGER.info("assertAddToCartDisplay completed");
    }

    /**
     * Verify input text is Not present on PLP page
     *
     * @param text The text that needs to be verified
     */
    public void assertTextNotPresentOnPlp(String text) {
        LOGGER.info("assertTextNotPresentOnPlp started for text: " + text);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(CommonActions.productListingPageContainer);
        String pageText = CommonActions.productListingPageContainer.getText();
        Assert.assertTrue("PLP Page contains the text: " + text,
                !pageText.toLowerCase().contains(text.toLowerCase()));
        LOGGER.info("assertTextNotPresentOnPlp completed for text: " + text);
    }

    /**
     * Verify mile Warranty range is in minimum-maximum format for Canonical PLP products
     */
    public void verifyMileWarrantyRange() {
        LOGGER.info("verifyMileWarrantyRange started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            if (productRow.findElement(mileWarranty).isDisplayed()) {
                String mileText = productRow.findElement(mileWarranty).getText();
                mileText = mileText.replaceAll(ConstantsDtc.MILE_WARRANTY, "")
                        .replaceAll("\\s+", "").replaceAll(",", "");
                if (viewDetailsButton.isDisplayed() && mileText.contains("-")) {
                    String[] parts = mileText.split("-", 0);
                    String mile1 = parts[0], mile2 = parts[1];
                    int minimumRange = Integer.parseInt(mile1);
                    int maximumRange = Integer.parseInt(mile2);
                    Assert.assertTrue("FAIL: Mile warranty range is not in 'min - max' " +
                            "format for this product: " + productName, (minimumRange < maximumRange));
                } else {
                    Assert.assertTrue("FAIL: Non-canonical product '"
                            + productName + "' contains Mile-Range: " + mileText, !mileText.contains("-"));
                }
            }
        }
        LOGGER.info("verifyMileWarrantyRange completed");
    }

    /**
     * Verify all PLP product's mile warranty is within expected filter range
     *
     * @param min_Range Expected minimum mile range
     * @param max_Range Expected maximum mile range
     */
    public void verifyMileWarrantyWithinExpectedRange(int min_Range, int max_Range) {
        LOGGER.info("verifyMileWarrantyWithinExpectedRange started for: " + min_Range + " - " + max_Range);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            if (productRow.findElement(mileWarranty).isDisplayed()) {
                String mileText = productRow.findElement(mileWarranty).getText();
                mileText = mileText.replaceAll(ConstantsDtc.MILE_WARRANTY, "")
                        .replaceAll("\\s+", "").replaceAll(",", "");
                if (mileText.contains("-")) {
                    String[] parts = mileText.split("-", 0);
                    String mile1 = parts[0], mile2 = parts[1];
                    int minimumRange = Integer.parseInt(mile1);
                    int maximumRange = Integer.parseInt(mile2);
                    Assert.assertTrue("FAIL: Mile warranty is not within expected range: " + min_Range +
                                    "-" + +max_Range + " for this product: " + productName,
                            (minimumRange >= min_Range && maximumRange <= max_Range));
                } else {
                    int mileWar = Integer.parseInt(mileText);
                    Assert.assertTrue("FAIL: Mile warranty is not within expected range: " + min_Range +
                                    "-" + +max_Range + " for this product: " + productName,
                            mileWar >= min_Range && mileWar <= max_Range);
                }
            }
        }
        LOGGER.info("verifyMileWarrantyWithinExpectedRange completedfor: " + min_Range + " - " + max_Range);
    }

    /**
     * Verify Item(s) added to cart
     */
    public void verifySelectedItemCodesOnCart() {
        LOGGER.info("verifySelectedItemCodesOnCart started");
        List<String> displayedItemCodes = new ArrayList<>();
        int cartTextSplitByItem = cartDetails.getText().split(Constants.PRODUCT_ARTICLE_NUMBER_LABEL).length - 1;
        for (int i = 1; i <= cartTextSplitByItem; i += 2) {
            String itemCode = cartDetails.getText().split(Constants.PRODUCT_ARTICLE_NUMBER_LABEL)[i].
                    substring(Constants.ZERO, Constants.FIVE);
            displayedItemCodes.add(itemCode);
        }
        int productListSize = driver.scenarioData.productInfoList.size();
        for (int index = 0; index < productListSize; index++) {
            if (Boolean.valueOf(commonActions.productInfoListGetValue(ConstantsDtc.IN_CART, index))) {
                boolean itemFound = false;
                String expectedItemCode = commonActions.productInfoListGetValue(ConstantsDtc.ITEM, index);
                for (String displayedItemCode : displayedItemCodes) {
                    if (expectedItemCode.equalsIgnoreCase(displayedItemCode)) {
                        itemFound = true;
                        LOGGER.info("Item code " + displayedItemCode + " was displayed on the cart page");
                    }
                }
                Assert.assertTrue("FAIL: Selected item " + expectedItemCode +
                        " was not displayed in shopping cart", itemFound);
            }
        }
        LOGGER.info("verifySelectedItemCodesOnCart completed");
    }

    /**
     * Selects the first available Standard product on PLP page
     *
     * @param buttonType : Add To Cart or Add To Package
     */
    public void selectFirstStandardProductOnPLP(String buttonType) {
        LOGGER.info("selectFirstStandardProductOnPLP started for " + buttonType);
        WebElement cartOrPackageElement = null;
        if (buttonType.equalsIgnoreCase(ConstantsDtc.ADD_TO_CART)) {
            cartOrPackageElement = CommonActions.addToCart;
        } else if (buttonType.equalsIgnoreCase(ConstantsDtc.ADD_TO_PACKAGE)) {
            cartOrPackageElement = CommonActions.addToPackage;
        }
        driver.waitForElementVisible(cartOrPackageElement);
        boolean found = false;
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String rowText = productRow.getText().toLowerCase();
            if (productRow.findElement(itemNumber).isDisplayed()
                    && rowText.toLowerCase().contains(buttonType.toLowerCase())) {
                WebElement productImage = productRow.findElement(productImageBy);
                driver.waitForElementVisible(productImage);
                driver.jsScrollToElementClick(productImage);
                found = true;
                break;
            }
        }
        Assert.assertTrue("There is no Standard product found on PLP page", found);
        LOGGER.info("selectFirstStandardProductOnPLP completed for " + buttonType);
    }

    /**
     * Verify driving details on PLP page when Treadwell is applied
     */
    public void assertDrivingDetailsOnPlp() {
        LOGGER.info("assertDrivingDetailsOnPLP started for element: " + productNameBy);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            Assert.assertTrue("FAIL: Product driving details not present for: " + productName,
                    productRow.findElement(drivingDetailsContainer).isDisplayed());
            String containerText = productRow.findElement(drivingDetailsContainer).getText();
            Assert.assertTrue("FAIL: Product driving details container doesn't contain any data for: "
                    + productName, !containerText.equals(null));
        }
        LOGGER.info("assertDrivingDetailsOnPLP completed for element: " + productNameBy);
    }

    /**
     * select product by name
     *
     * @param product name that should get selected from PLP
     */
    public void selectProductByName(String product) {
        LOGGER.info("selectProductByName started  for " + product);
        boolean itemFound = false;
        if (Config.isSafari() || Config.isIphone())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        while (!itemFound) {
            List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
            for (WebElement productRow : productRows) {
                String productNameRow = productRow.findElement(productNameBy).getText();
                String[] productNameInRow = productNameRow.split("\\n");
                if (product.equalsIgnoreCase(productNameInRow[1])) {
                    WebElement productName = productRow.findElement(productNameBy);
                    driver.jsScrollToElement(productName);
                    productName.click();
                    itemFound = true;
                    break;
                }
            }
            //navToDifferentPageOfResults now returns a boolean, indicating whether the next/prev button was found
            if (!itemFound && commonActions.navToDifferentPageOfResults(Constants.NEXT)) {
                LOGGER.info("Navigating to next page.");
                driver.waitForMilliseconds();
            } else if (!itemFound) {
                Assert.fail("FAIL: Product '" + product + "' NOT found");
                break;
            }
        }

        Assert.assertTrue("FAIL: The Product name did not find match on PLP PAGE", itemFound);
        LOGGER.info("selectProductByName completed for " + product);
    }

    /**
     * Verify display drilled down as per vehicle selection
     *
     * @param brand The brand name that needs to be verified
     * @param size  The size that needs to be verified
     */
    public void assertPlpDetailsWhenVehicleOnSession(String brand, String size) {
        LOGGER.info("assertPlpDetailsWhenVehicleOnSession started for: " + brand + " and " + size);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        assertPlpResultsByBrand(brand);
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productRowText = productRow.getText();
            if (productRowText.contains(ConstantsDtc.VIEW_DETAILS)) {
                WebElement element = productRow.findElement(CommonActions.productLowerBlockBy);
                driver.verifyTextDisplayedCaseInsensitive(element, PRODUCT_DETAILS_TEXT);
            } else if (driver.isElementDisplayed(CommonActions.addToCart) && driver.isElementDisplayed(tireSizeBy)) {
                String tireSize = productRow.findElement(tireSizeBy).getText().replaceAll("\\$", "");
                Assert.assertTrue("FAIL: Canonical plp products are not drilled down on PLP as per the actual" +
                                " wheel size: " + size + " of the vehicle on session. Displayed size is: " + tireSize,
                        tireSize.contains(size));
            }
        }
        LOGGER.info("assertPlpDetailsWhenVehicleOnSession completed for: " + brand + " and " + size);
    }

    /**
     * Verify exact price link when vehicle on session
     *
     * @param vehicleSession vehicle on or not_on session
     */
    public void assertCanonicalForPriceLinkAndInventoryText(String vehicleSession) {
        LOGGER.info("assertCanonicalForPriceLinkAndInventoryText started for: " + vehicleSession);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(productListingBy);
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            String productRowText = productRow.getText();
            if (CommonUtils.containsIgnoreCase(productRowText, ConstantsDtc.VIEW_DETAILS)) {
                String priceAndInventoryText = productRow.findElement(priceAndInventoryCanonicalBy).getText();
                if (vehicleSession.equalsIgnoreCase(VEHICLE_ON_SESSION)) {
                    Assert.assertTrue("FAIL: Canonical product: " + productName + " does not contain the " +
                                    "expected text: " + VIEW_PRODUCT_DETAILS_FOR_EXACT_PRICE,
                            priceAndInventoryText.equalsIgnoreCase(VIEW_PRODUCT_DETAILS_FOR_EXACT_PRICE));
                } else if (vehicleSession.equalsIgnoreCase(VEHICLE_NOT_ON_SESSION)) {
                    Assert.assertTrue("FAIL: Canonical product: " + productName + " does not contain the " +
                                    "expected text: " + ENTER_VEHICLE_FOR_EXACT_PRICE,
                            priceAndInventoryText.equalsIgnoreCase(ENTER_VEHICLE_FOR_EXACT_PRICE));
                    Assert.assertTrue("FAIL: Canonical product: " + productName + "does not contain " +
                            "'Enter vehicle' link: ", enterVehicleCanonicalProductLink.isDisplayed());
                }
            }
        }
        LOGGER.info("assertCanonicalForPriceLinkAndInventoryText completed for: " + vehicleSession);
    }

    /**
     * Verify the details like Item Number, Size, Price and Inventory message
     */
    public void verifyItemNumberSizePrice() {
        LOGGER.info("verifyItemNumberSizePrice started");
        driver.waitForPageToLoad();
        By inventoryMessage;
        driver.waitForElementVisible(productListingBy);
        List<WebElement> plpTabs = webDriver.findElements(resultsOptionLinkBy);
        if (STAGGERED_PLP_TAB.length == plpTabs.size()) {
            inventoryMessage = staggerdInventoryMessageBy;
        } else {
            inventoryMessage = CommonActions.inventoryMessageBy;
        }
        List<WebElement> productRows = webDriver.findElement(productListingBy).findElements(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            String productName = productRow.findElement(productNameBy).getText();
            Assert.assertTrue("FAIL: Non-Canonical product '" + productName + "' does not contain Item#",
                    driver.isElementDisplayed(productRow.findElement(itemNumber)));
            Assert.assertTrue("FAIL: Non-Canonical product '" + productName + "' does not contain Price",
                    driver.isElementDisplayed(productRow.findElement(CommonActions.productPriceBy)));
            Assert.assertTrue("FAIL: Non-Canonical product '" + productName + "' does not contain size",
                    driver.isElementDisplayed(productRow.findElement(tireSizeBy)));
            Assert.assertTrue("FAIL: PLP product '" + productName + "' does not contain Inventory Message",
                    driver.isElementDisplayed(productRow.findElement(inventoryMessage)));
        }
        LOGGER.info("verifyItemNumberSizePrice completed");
    }

    /**
     * Verify if the size of products listed on the PLP matches the specified size
     *
     * @param size Tire or wheel size
     */
    public void verifyPlpResultsSize(String size) {
        LOGGER.info("verifyPlpResultsSize started");
        List<WebElement> addToCartButtonText = webDriver.findElements(productBlockBy);

        for (int j = 0; j < addToCartButtonText.size(); j++) {
            if (addToCartButtonText.get(j).getText().toLowerCase().contains(ConstantsDtc.ADD_TO_CART.toLowerCase())) {
                String sizeOfProductListed = addToCartButtonText.get(j).getText();
                Assert.assertTrue("FAIL: The product " + sizeOfProductListed.split("\n")[1] + " " +
                        "does not have the specified size " + size + "", sizeOfProductListed.toLowerCase().contains(size.toLowerCase()));
            }
        }
        LOGGER.info("verifyPlpResultsSize completed");
    }

    /**
     * Verifies the message display in 'no results page'
     *
     * @param vehicleState : Value can be "vehicle in session" or "no vehicle in session" or "no results"
     */
    public void verifyNoResultsSearchMessage(String vehicleState) {
        LOGGER.info("verifyNoResultsSearchMessage started for " + vehicleState);
        driver.waitForPageToLoad(Constants.ONE_HUNDRED);
        if (vehicleState.equalsIgnoreCase(NO_VEHICLE_INSESSION)) {
            Assert.assertTrue("FAIL: Message is not equal", noResultsInfoMessage.getText().
                    equalsIgnoreCase(NO_RESULTS_DISPLAY_MESSAGE_NO_VEHICLE_INSESSION + Config.getDefaultStorePhoneNumber()
                            + NO_RESULTS_DISPLAY_MESSAGE_2));
        } else if (vehicleState.equalsIgnoreCase(VEHICLE_INSESSION)) {
            Assert.assertTrue("FAIL: Message is not equal", noResultsInfoMessage.getText().
                    equalsIgnoreCase(NO_RESULTS_DISPLAY_MESSAGE_VEHICLE_INSESSION + Config.getDefaultStorePhoneNumber()
                            + NO_RESULTS_DISPLAY_MESSAGE_2));
        } else if (vehicleState.equalsIgnoreCase(NO_RESULTS)) {
            Assert.assertFalse("Fail - The No product results found...message was displayed!",
                    driver.isElementDisplayed(noResultsInfoMessage, Constants.TWO));
        }
        LOGGER.info("verifyNoResultsSearchMessage completed for " + vehicleState);
    }

    /**
     * Verify Add To Cart is disables in PLP page for specified item code
     *
     * @param itemCode: product who's add to cart should be verified.
     */
    public void assertAddToCartDisabledInPlp(String itemCode) {
        LOGGER.info("assertAddToCartDisabledInPlp started for " + itemCode);
        driver.waitForPageToLoad();
        List<WebElement> productRows = driver.getDisplayedElementsList(plpResultsRowBy);
        for (WebElement productRow : productRows) {
            if (productRow.getText().contains(itemCode)) {
                Assert.assertTrue("FAIL: Add to cart button didn't contain 'Not available'",
                        productRow.findElement(CommonActions.addToCartByClassBy).getText().contains(NOT_AVAILABLE));
                Assert.assertTrue("FAIL: Add to cart button is enabled ",
                        productRow.findElement(CommonActions.addToCartByClassBy).getAttribute(Constants.CLASS).contains(BUTTON_UNAVAILABLE));
                break;
            }
        }
        LOGGER.info("assertAddToCartDisabledInPlp completed for " + itemCode);
    }


    /**
     * Method verifies package details in PLP page
     *
     * @param keyName: Returns all the package details with key name.
     */
    public void assertPackageDetailsInPlpPage(String keyName) {
        LOGGER.info("assertPackageDetailsInPlpPage started for " + keyName);
        String values[] = tireWheelPackageDetails.get(keyName);
        for (String value : values) {
            driver.verifyTextDisplayed(returnWebElement(Constants.PACKAGE), value);
        }
        LOGGER.info("assertPackageDetailsInPlpPage completed for " + keyName);
    }

    /**
     * verifies the size on the staggered size line matches the factory size displayed on the banner
     */
    public void assertStaggeredSizeLine() {
        LOGGER.info("assertStaggeredSizeLine started");
        Assert.assertTrue("FAIL: The staggered size line is not displayed", driver.isElementDisplayed(staggeredSizeLine));
        Assert.assertTrue("FAIL: The front tire size line does not match",
                frontTireSizeLine.getText().replace("R", "-R").equalsIgnoreCase(factoryFrontTire.getText()));
        Assert.assertTrue("FAIL: The front tire size line does not match",
                rearTireSizeLine.getText().replace("R", "-R").equalsIgnoreCase(factoryRearTire.getText()));
        LOGGER.info("assertStaggeredSizeLine completed");
    }

    /**
     * Select the image of the specified item code on PLP
     *
     * @param itemCode Item code
     */
    public void selectProductImageOnPLP(String itemCode) {
        LOGGER.info("selectProductImageOnPLP started");
        List<WebElement> products = webDriver.findElements(CommonActions.plpProductsBy);
        boolean found = false;
        for (WebElement plpProduct : products) {
            if (plpProduct.getText().toLowerCase().contains(itemCode)) {
                WebElement image = plpProduct.findElement(By.xpath("preceding-sibling::div"));
                driver.jsScrollToElementClick(image);
                found = true;
                break;
            }
        }
        if (!found) {
            Assert.fail("FAIL: The product with item code: " + itemCode + " is not found.");
        }
        LOGGER.info("selectProductImageOnPLP completed");
    }

    /**
     * Verify the product in specified position on the add to cart popup modal carousel matches the product specified
     *
     * @param productName name of the product to which the product on the carousel at specified position
     * @param position    position of the product on the carousel
     */
    public void assertProductPositionOnSuggestedSellingCarousel(String productName, int position) {
        LOGGER.info("assertProductPositionOnSuggestedSellingCarousel started");
        List<WebElement> products = CommonActions.addToCartModal.findElements(CommonActions.suggestedSellingTextContainerBy);
        driver.jsScrollToElement(products.get(position - 1));
        String productAtPositionName = products.get(position - 1).getText();
        Assert.assertTrue("FAIL: The product at position: " + position + ": " + productAtPositionName.split("\n", 3)[1] +
                " did not match the expected product: " + productName.split("\n", 3)[1], productName.split("\n", 3)[1].
                equalsIgnoreCase(productAtPositionName.split("\n", 3)[1]));
        LOGGER.info("assertProductPositionOnSuggestedSellingCarousel completed");
    }

    /**
     * Selects the specified button on the specified product in position.
     *
     * @param position   position of the product with button type
     * @param buttonType add to cart or view details
     */
    public void selectProductWithButton(String position, String buttonType) {
        LOGGER.info("selectProductWithButton started");
        List<WebElement> addToCartProduct;
        if (buttonType.equalsIgnoreCase(ConstantsDtc.ADD_TO_CART)) {
            addToCartProduct = driver.getElementsWithText(plpResultsRowBy, ConstantsDtc.ADD_TO_CART);
        } else {
            addToCartProduct = driver.getElementsWithText(plpResultsRowBy, ConstantsDtc.VIEW_DETAILS);
        }
        int positionNumber = Integer.parseInt(position.split("\\D")[0].trim());
        addToCartProduct.get(positionNumber - 1).findElement(productImageBy).click();
        LOGGER.info("selectProductWithButton completed");
    }
}