package dtc.pages;

import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.steps.CommonActionsSteps;
import common.CommonExcel;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 9/27/16.
 */
public class OrderPage {

    private Driver driver;
    private WebDriver webDriver;
    private final CommonActions commonActions;
    private final CartPage cartPage;
    private CommonExcel commonExcel;
    private final Logger LOGGER = Logger.getLogger(OrderPage.class.getName());
    private Customer customer;
    public static String confirmationOrderNumber;
    Scenario scenario;

    public OrderPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        customer = new Customer();
        cartPage = new CartPage(driver);
        commonExcel = new CommonExcel();
        PageFactory.initElements(webDriver, this);
    }

    public static final String ORDER_CONFIRMATION_MESSAGE = "Thank you for your order";
    public static final String ORDER_CANCELLATION_REQUEST = "Cancellation Request";
    public static final String CANCEL_ORDER = "Cancel Order";
    public static final String ORDER_CANCELLED = "Order Cancelled";
    public static final String READY_TO_INSTALL = "Ready to Install";
    public static final String CANCELLATION_COMPLETE = "Cancellation Complete";
    private static String ORDER_CONFIRMATION_MESSAGE_SENT_TO = "An order confirmation has been sent to ";
    private static final String SURVEY_FEEDBACK_LINK_TEXT = "Tell us about your web experience";

    //TODO: Update to use auto className when available
    @FindBy(css = "span.cart-confirmation__order-info")
    public static WebElement orderNumber;

    @FindBy(className = "order-summary__price")
    public static WebElement orderTotal;

    @FindBy(className = "cart-wrapper")
    public static WebElement confirmationWrapper;

    @FindBy(className = "cart-confirmation__details")
    public static WebElement customerConfirmationContainer;

    @FindBy(className = "cart-confirmation__order-message")
    public static WebElement confirmationMessageContainer;

    @FindBy(className = "cart-confirmation__action")
    public static WebElement confirmationSurveyContainer;

    @FindBy(className = "cart-confirmation__appointment-date")
    public static WebElement orderPageDateTime;

    @FindBy(className = "cart-item__fee-details-price")
    public static WebElement envFeeSubtotalPrice;

    @FindBy(css = ".cart-item__fee-details")
    public static WebElement cartItemFeeDetails;

    @FindBy(xpath = "//div[@class='cart-summary__canada-tax-container']//div[contains(.,'Subtotal')]//div[contains(@class,'price')]")
    public static WebElement canadaTax;

    @FindBy(css = ".order-confirmation__my-info-payment-column:nth-child(3) div:nth-child(2)")
    public static WebElement sellersTransaction;

    public static final By orderCartSummaryBy = By.className("cart-summary__breakdown");
    public static final By feeInfoBy = By.className("cart-item__fee-details");
    public static final By cartItemDetailsBy = By.className("cart-wrapper");
    public static final By cartItemColumnDetailsBy = By.className("cart-item__column");
    public static final By cartItemStaggeredRearBy = By.cssSelector(".cart-item__staggered-product--rear .cart-item__row");
    public static final By orderConfirmationMessage = By.className("cart-confirmation__order-message");

    /**
     * Writes order number, store number, and timestamp to txt file for post-run integration testing
     */
    public void storeOrderNumber() {
        LOGGER.info("storeOrderNumber started");
        String cleanedOrderNumber = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date date = new Date();
            cleanedOrderNumber = orderNumber.getText().replaceAll("[^\\d]", "");
            String orderInfo = "Order Number: " + cleanedOrderNumber + ", Store: " +
                    Config.getDefaultStore() + ", Date/Time: " + dateFormat.format(date);
            LOGGER.info(orderInfo);
            driver.scenarioData.setCurrentOrderNumber(cleanedOrderNumber);
            CommonUtils.appendFile(Constants.ORDERS_FILE_LOC.concat(ConstantsDtc.ORDERS_FILE), orderInfo + "\n");
        } catch (Exception e) {
            String msg = "There was an error when attempting to store the order number: " + e.toString();
            LOGGER.info(msg);
            CommonUtils.appendFile(Constants.ORDERS_FILE_LOC.concat(ConstantsDtc.ORDERS_FILE), msg + "\n");
        }
        LOGGER.info("storeOrderNumber completed for " + cleanedOrderNumber);
    }

    /**
     * Stores the total amount from order confirmation page into hash map in scenario data with key - "Total Amount".
     */
    public void storeTotalAmount() {
        LOGGER.info("storeTotalAmount started");
        driver.scenarioData.setData(Constants.TOTAL_AMOUNT_EXCEL_COLUMN_LABEL, CommonActions.totalAmount.getText().replace("$", "").replace(",", ""));
        LOGGER.info("storeTotalAmount completed");
    }

    /**
     * Stores order number in scenario data for integration testing
     */
    public void storeOrderNumberInScenarioData() {
        LOGGER.info("storeOrderNumberInScenarioData started");
        String cleanedOrderNumber = orderNumber.getText().replaceAll("[^\\d]", "");
        driver.scenarioData.setCurrentOrderNumber(cleanedOrderNumber);
        LOGGER.info("Successfully stored order number: " + cleanedOrderNumber);
        LOGGER.info("storeOrderNumberInScenarioData completed");
    }

    /**
     * Asserts order confirmation/Order Cancellation message appears on page
     *
     * @param pagetitle web page title
     */
    public void assertOrderMessage(String pagetitle) {
        LOGGER.info("assertOrderMessage started for " + pagetitle);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(CommonActions.pageTitle, Constants.SIXTY);
        String pageTitleText = pagetitle;
        if (pagetitle.equalsIgnoreCase(ConstantsDtc.ORDER_CONFIRMATION)) {
            pageTitleText = ORDER_CONFIRMATION_MESSAGE;
        }
        Assert.assertTrue("FAIL: The order confirmation page was NOT displayed!",
                driver.isElementDisplayed(driver.getElementWithText(
                        CommonActions.pageTitle, pageTitleText)));
        if (pagetitle.contains(ORDER_CANCELLATION_REQUEST)) {
            Assert.assertTrue("FAIL: The order cancellation link not taking to right order!",
                    orderNumber.getText().contains(driver.scenarioData.getCurrentOrderNumber()));
        }

        // Wait to allow Javascript to fully load. Requested by Engineering team for reporting.
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        LOGGER.info("assertOrderMessage completed for " + pagetitle);
    }

    /**
     * Asserts product name and item code appear on order list
     *
     * @param product product to validate appears
     * @param item    item to validate appears
     */
    public void assertItemOnOrderConfirmationPage(String product, String item) {
        LOGGER.info("assertItemOnOrderConfirmationPage started");
        LOGGER.info("cartItemDetails contains" + webDriver.findElement(cartItemDetailsBy).getText());
        Assert.assertTrue("FAIL: Product: " + product + " was not found on the final order confirmation page",
                driver.isElementDisplayed(driver.getElementWithText(cartItemDetailsBy, product)));
        Assert.assertTrue("FAIL: Item code: " + item + " was not found on the final order confirmation page",
                driver.isElementDisplayed(driver.getElementWithText(cartItemDetailsBy, item)));
        LOGGER.info("assertItemOnOrderConfirmationPage completed. Product '" + product + "' with item code '" + item +
                "' was listed on the order confirmation page.");
    }

    /**
     * Asserts tha Total on screen matches the price from before
     *
     * @param total Price that should appear in the Total section
     */
    public void assertOrderTotal(String total) {
        LOGGER.info("assertOrderTotal started");
        driver.waitForElementVisible(orderTotal);
        Assert.assertTrue("FAIL: Order Summary Total price did not equal " + orderTotal.getText(),
                total.equalsIgnoreCase(orderTotal.getText()));
        LOGGER.info("assertOrderTotal completed");
    }

    /**
     * Asserts that specified text is visible on the order confirmation page
     *
     * @param text Text to verify on order confirmation page
     */
    public void assertTextOnOrderConfirmationPage(String text) {
        LOGGER.info("assertTextOnOrderConfirmationPage started");
        driver.waitForElementVisible(confirmationWrapper);
        Assert.assertTrue("FAIL: Confirmation page did not contain \"" + text + "\"",
                confirmationWrapper.getText().contains(text));
        LOGGER.info("assertTextOnOrderConfirmationPage completed");
    }

    /**
     * Selects / expands the "show fee details" section for an item on the Order confirmation page
     */
    public void expandFeeDetailsForItem() {
        LOGGER.info("expandFeeDetailsForItem started");
        driver.waitForElementVisible(feeInfoBy);
        webDriver.findElement(feeInfoBy).click();
        LOGGER.info("expandFeeDetailsForItem completed");
    }

    /**
     * Verifies the order total amounts on confirmation page matches with Shopping cart order total
     */
    public void assertOrderConfirmationAndCartOrderTotal() {
        LOGGER.info("assertConfirmationAndCartOrderTotal started");
        driver.waitForElementVisible(CommonActions.totalAmount);
        double confirmationOrderTotal = commonActions.cleanMonetaryStringToDouble(CommonActions.totalAmount.getText());
        Assert.assertTrue("FAIL: Expected order total on Order Confirmation page: ("
                        + CommonActionsSteps.orderTotal + ") did not match to the actual order total: ("
                        + confirmationOrderTotal + ")",
                CommonActionsSteps.orderTotal == confirmationOrderTotal);
        LOGGER.info("assertConfirmationAndCartOrderTotal completed");
    }

    /**
     * Extracts the sales tax from Order Confirmation page
     *
     * @param customerType Customer Type
     * @return salesTax typed as a double
     */
    public double extractSalesTaxOnOrderConfirmation(String customerType) {
        LOGGER.info("extractSalesTaxOnOrderConfirmation started for " + customerType + " customer");
        Double taxOnOrderConfirmation;
        if (CommonUtils.containsIgnoreCase(customerType, Constants.CANADA)) {
            driver.waitForElementVisible(canadaTax);
            taxOnOrderConfirmation = commonActions.cleanMonetaryStringToDouble(canadaTax.getText());
        } else {
            driver.waitForElementVisible(CartPage.cartSummaryBreakDownNameBy);
            WebElement tax = driver.getElementWithText(CartPage.cartSummaryBreakDownNameBy, ConstantsDtc.TAXES);
            WebElement taxParent = driver.getParentElement(driver.getParentElement(tax));
            taxOnOrderConfirmation = commonActions.cleanMonetaryStringToDouble(
                    taxParent.findElement(CartPage.cartSummaryBreakDownPriceBy).getText());
        }
        LOGGER.info("extractSalesTaxOnOrderConfirmation completed for " + customerType + " customer");
        return taxOnOrderConfirmation;
    }

    /**
     * Verifies the sales tax amount on Order confirmation page matches with estimated tax on Shopping cart
     *
     * @param customer Customer Type
     */
    public void assertOrderConfirmationAndCartSalesTax(String customer) {
        LOGGER.info("assertConfirmationAndCartSalesTax started");
        driver.waitForElementVisible(CartPage.cartSummaryBreakDownNameBy);
        double orderConfirmationSalesTax = extractSalesTaxOnOrderConfirmation(customer);
        Assert.assertTrue("FAIL: Expected sales tax on Order confirmation page: ("
                        + CommonActionsSteps.salesTax + ") did not match to the actual sales tax: ("
                        + orderConfirmationSalesTax + ")",
                CommonActionsSteps.salesTax == orderConfirmationSalesTax);
        LOGGER.info("assertConfirmationAndCartSalesTax completed");
    }

    /**
     * Validates current date and time listed on the OrderConfirmation page
     *
     * @param date Appointment Date to verify.  Example format:  Wednesday, January 24, 2018
     * @param time Appointment Time to verify.  Example format:  11:00 AM
     */
    public void verifyDateAndTime(String date, String time) {
        LOGGER.info("Order Page verifyDateAndTime started");
        driver.waitForElementVisible(orderPageDateTime);
        String apptDT = orderPageDateTime.getText();
// Commenting this out for now. When VoV and Drop-off are added back into the build. This is the code we will need.
//        String apptDate = apptDT.split("\n")[0].trim();
//        String apptTime = apptDT.split("\n")[1].replace(ConstantsDtc.TIME_LABEL, "").trim();
        String apptDate = apptDT.split("-")[0].trim();
        String apptTime = apptDT.split("-")[1].trim();
        date = CommonUtils.replaceLongMonthWithShortMonth(date);
        apptDate = CommonUtils.replaceLongMonthWithShortMonth(apptDate);

        Assert.assertTrue("FAIL: Did NOT see expected date \"" + date + "\" in appointment info: \""
                + apptDate + "\"!", date.contains(apptDate));
        LOGGER.info("Confirmed that expected date \"" + date + "\" was listed in appointment info");

        Assert.assertTrue("FAIL: Did NOT see expected time \"" + time + "\" in appointment info: \""
                + apptTime + "\"!", apptTime.equals(time));
        LOGGER.info("Confirmed that expected time (" + time + ") was listed in appointment info");
        LOGGER.info("Order Page verifyDateAndTime completed");
    }

    /**
     * Verifies the order total amounts on confirmation page matches with checkout order total
     */
    public void assertOrderConfirmationAndCheckoutOrderTotal() {
        LOGGER.info("assertOrderConfirmationAndCheckoutOrderTotal started");
        driver.waitForElementVisible(CommonActions.totalAmount);
        double confirmationOrderTotal = commonActions.cleanMonetaryStringToDouble(CommonActions.totalAmount.getText());
        Assert.assertTrue("FAIL: Expected order total on Order Confirmation page: ("
                        + CommonActionsSteps.orderTotalCheckout + ") did not match to the actual order total: ("
                        + confirmationOrderTotal + ")",
                CommonActionsSteps.orderTotalCheckout == confirmationOrderTotal);
        LOGGER.info("assertOrderConfirmationAndCheckoutOrderTotal completed");
    }

    /**
     * Verifies the sales tax amount on Order confirmation page matches with estimated tax on checkout page
     *
     * @param customer Customer Type
     */
    public void assertOrderConfirmationAndCheckoutSalesTax(String customer) {
        LOGGER.info("assertOrderConfirmationAndCheckoutSalesTax started");
        driver.waitForElementVisible(CartPage.cartSummaryBreakDownNameBy);
        double orderConfirmationSalesTax = extractSalesTaxOnOrderConfirmation(customer);
        Assert.assertTrue("FAIL: Expected sales tax on Order confirmation page: ("
                        + CommonActionsSteps.salesTaxCheckout + ") did not match to the actual sales tax: ("
                        + orderConfirmationSalesTax + ")",
                CommonActionsSteps.salesTaxCheckout == orderConfirmationSalesTax);
        LOGGER.info("assertOrderConfirmationAndCheckoutSalesTax completed");
    }

    /**
     * Asserts that specified customer text is visible on the order confirmation page
     *
     * @param text Text to verify on order confirmation page
     */
    public void assertCustomerDetailsOnOrderConfirmationPage(String text) {
        LOGGER.info("assertCustomerDetailsOnOrderConfirmationPage started");
        driver.waitForElementVisible(customerConfirmationContainer);
        Assert.assertTrue("FAIL: Confirmation page did not contain \"" + text + "\"",
                CommonUtils.containsIgnoreCase(customerConfirmationContainer.getText(), text));
        LOGGER.info("assertCustomerDetailsOnOrderConfirmationPage completed");
    }

    /**
     * Asserts that specified text is visible on the order confirmation page
     *
     * @param element WebElement to refer
     * @param text    Text to verify on order confirmation page
     */
    public void assertTextOnOrderConfirmationPage(WebElement element, String text) {
        LOGGER.info("assertTextOnOrderConfirmationPage started");
        driver.waitForElementVisible(element);
        Assert.assertTrue("FAIL: Confirmation page did not contain \"" + text + "\"",
                CommonUtils.containsIgnoreCase(element.getText(), text));
        LOGGER.info("assertTextOnOrderConfirmationPage completed");
    }

    /**
     * Verify Customer details on order confirmation page
     *
     * @param customer Type of customer dtc.data to use in Customers class
     * @param page     : order confirmation or order history detail page
     */
    public void verifyCustomerDetailsOnOrderConfirmation(Customer customer, String page) {
        LOGGER.info("verifyCustomerDetailsOnOrderConfirmation started for" + page);
        verifyCustomerEmailMessageOnOrderConfirmation(customer);
        assertCustomerDetailsOnOrderConfirmationPage(customer.firstName);
        assertCustomerDetailsOnOrderConfirmationPage(customer.lastName);
        assertCustomerDetailsOnOrderConfirmationPage(customer.email);
        String phoneNumber = customer.phone;
        String formattedPhoneNumber = phoneNumber.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
        assertCustomerDetailsOnOrderConfirmationPage(formattedPhoneNumber);
        LOGGER.info("verifyCustomerDetailsOnOrderConfirmation completed for" + page);
    }

    /**
     * Verify Customer email confirmation message on order confirmation page
     *
     * @param customer Type of customer dtc.data to use in Customers class
     */
    public void verifyCustomerEmailMessageOnOrderConfirmation(Customer customer) {
        LOGGER.info("verifyCustomerEmailMessageOnOrderConfirmation started");
        assertTextOnOrderConfirmationPage(confirmationMessageContainer,
                ORDER_CONFIRMATION_MESSAGE_SENT_TO + customer.email);
        LOGGER.info("verifyCustomerEmailMessageOnOrderConfirmation completed");
    }

    /**
     * Clicks Survey feedback link via linkText
     */
    public void clickSurveyLink() {
        LOGGER.info("clickSurveyLink started");
        driver.clickElementWithLinkText(SURVEY_FEEDBACK_LINK_TEXT);
        LOGGER.info("clickSurveyLink completed");
    }

    /**
     * Verify environment fee label & subtotal price on order confirmation page
     */
    public void verifyEnvironmentFeeDetailsOnOrderConfirmation() {
        LOGGER.info("verifyEnvironmentFeeDetailsOnOrderConfirmation started");
        assertTextOnOrderConfirmationPage(cartItemFeeDetails, ConstantsDtc.ENVIRONMENTAL_FEE);
        assertTextOnOrderConfirmationPage(envFeeSubtotalPrice, CartPage.environmentFee);
        LOGGER.info("verifyEnvironmentFeeDetailsOnOrderConfirmation completed");
    }

    /**
     * Save the Web Order Number and Extended Assortment Flag to excel file
     *
     * @param file file name
     * @param flag Extended Assortment flag
     */
    public void storeDtcOrderNumberEAFlagToExcel(String file, String flag) {
        LOGGER.info("storeDtcOrderNumberEAFlagToExcel started");
        String orderNumber = OrderPage.orderNumber.getText().replaceAll("[^\\d]", "");
        try {
            String location = ConstantsDtc.WEB_ORDER_DATA_FILE_IN + file + Constants.WEB_EXCEL_FILE_EXTENSION;
            commonExcel.saveDtcOrderNumberFlagToExcel(location, orderNumber, flag);
        } catch (Exception e) {
            Assert.fail("FAIL: Exception occurred in writing '" + orderNumber +
                    "' to the excel file: " + file + " with EA flag " + flag);
        }
        LOGGER.info("storeDtcOrderNumberEAFlagToExcel completed for Order # " + orderNumber);
    }

    /**
     * Verify the expected TPMS option is displayed on the order confirmation page
     *
     * @param optionText - The TPMS option
     */
    public void verifySelectedTPMSOptionDisplayed(String optionText) {
        LOGGER.info("verifySelectedTPMSOptionDisplayed started");
        String displayedTPMSOption = "";

        if (!driver.isElementDisplayed(CommonActions.optionNameBy)) {
            Assert.fail("The TPMS option was not displayed");
        } else {
            displayedTPMSOption = webDriver.findElement(CommonActions.optionNameBy).getText();
        }

        Assert.assertTrue("FAIL: The TPMS option was not displayed on the order confirmation page. Expected '" +
                optionText + "'. Actual '" + displayedTPMSOption + "'", optionText.equals(displayedTPMSOption));

        LOGGER.info("verifySelectedTPMSOptionDisplayed completed");
    }

    /**
     * This method will return the string value from the order confirmation page for the matched elementName
     *
     * @param elementName - Name of element on order confirmation page
     * @return - String value from the order confirmation page for the matched element
     */
    public String returnValueFromOrderConfirmation(String elementName) {
        LOGGER.info("returnValueFromOrderConfirmation started");
        String returnValue = null;
        switch (elementName) {
            case ConstantsDtc.TAXES:
                returnValue = webDriver.findElements(cartPage.cartSummaryBreakDownPriceBy).get(1).getText();
                break;
            case ConstantsDtc.TOTAL:
                returnValue = CommonActions.totalAmount.getText().split("\\$")[1].trim();
                break;
            case ConstantsDtc.ENVIRONMENTAL_FEE:
                returnValue = cartPage.getFeeTotal(ConstantsDtc.ENVIRONMENTAL_FEE);
                break;
            case ConstantsDtc.ENVIRONMENTAL_FEE + " " + ConstantsDtc.REAR:
                returnValue = cartPage.getSpecialPricingOnCartPage(cartItemStaggeredRearBy,
                        cartPage.feeDetailsItemsRowLabelBy, cartPage.feeDetailsItemsRowPriceBy,
                        ConstantsDtc.ENVIRONMENTAL_FEE);
                break;
            default:
                Assert.fail("FAIL: Could not find matched string name on order confirmation Page!");

        }
        LOGGER.info("returnValueFromOrderConfirmation completed");
        return returnValue;
    }

    /**
     * This method saves order numbers from Order Confirmation page into list
     */
    public void saveOrderNumbersFromOrderConfirmationPageInList() {
        LOGGER.info("saveOrderNumbersFromOrderConfirmationPageInList started");
        driver.waitForElementVisible(orderNumber);
        confirmationOrderNumber = orderNumber.getText().replaceAll("[^\\d]", "");
        LOGGER.info("saveOrderNumbersFromOrderConfirmationPageInList completed");
    }

    /**
     * This method verifies order number from Order confirmation page with Order History Details page
     */
    public void assertOrderNumberFromOrderConfirmationWithOrderDetailsPage() {
        LOGGER.info("assertOrderNumberFromOrderConfirmationWithOrderDetailsPage started");
        Assert.assertEquals("FAIL: Order numbers are not equal", confirmationOrderNumber,
                orderNumber.getText().replaceAll("[^\\d]", ""));
        LOGGER.info("assertOrderNumberFromOrderConfirmationWithOrderDetailsPage completed");
    }

    /**
     * Verify the specified email address is displayed on the order confirmation page
     *
     * @param emailAddress Expected email address
     */
    public void verifyEmailAddressOnOrderConfirmationPage(String emailAddress) {
        LOGGER.info("verifyEmailAddressOnOrderConfirmationPage started for email address: '" + emailAddress + "'");
        WebElement emailAddressElement = CommonActions.cartConfirmationMyInfoColumn.
                findElement(AppointmentConfirmationPage.appointmentDetailsMyEmailInfoWebBy);
        Assert.assertTrue("FAIL: The email address is not displayed on the order confirmation page",
                driver.isElementDisplayed(emailAddressElement));
        String actualEmailAddress = emailAddressElement.getText();
        Assert.assertTrue("FAIL: The expected email address is not displayed on the order confirmation page. " +
                        "Expected: '" + emailAddress + "'. Actual: '" + actualEmailAddress + "'.",
                emailAddress.equalsIgnoreCase(actualEmailAddress));
        LOGGER.info("verifyEmailAddressOnOrderConfirmationPage completed for email address: '" + emailAddress + "'");
    }

}