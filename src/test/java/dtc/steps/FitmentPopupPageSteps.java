package dtc.steps;

import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.pages.CommonActions;
import dtc.data.ConstantsDtc;
import dtc.data.Vehicle;
import dtc.pages.FitmentPopupPage;
import dtc.pages.SearchAutocompleteDropDownPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/24/16.
 */
public class FitmentPopupPageSteps {

    private FitmentPopupPage fitmentPopupPage;
    private CommonActions commonActions;
    private Driver driver;
    private SearchAutocompleteDropDownPage searchAutocompleteDropDownPage;
    private Scenario scenario;
    private Vehicle vehicle;

    public FitmentPopupPageSteps(Driver driver) {
        this.driver = driver;
        fitmentPopupPage = new FitmentPopupPage(driver);
        commonActions = new CommonActions(driver);
        searchAutocompleteDropDownPage = new SearchAutocompleteDropDownPage(driver);
        vehicle = new Vehicle();
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @Then("^I should see the fitment panel with vehicle \"(.*?)\"$")
    public void i_should_see_the_fitment_panel_with_vehicle(String vehicleText) throws Throwable {
        if (vehicleText.startsWith(Constants.VEHICLE.toUpperCase())) {
            Vehicle veh = vehicle.getVehicle(vehicleText);
            vehicleText = veh.year + " " + veh.make;
            scenario.write(veh.toString());
        }
        fitmentPopupPage.assertVehicleInPanel(vehicleText);
    }

    @When("^I select enter driving details for recommended tires$")
    public void i_select_pdlx_driving_details_recommended_tires() throws Throwable {
        fitmentPopupPage.selectPdlxDrivingDetailsRecommendedTiresLink();
    }

    @When("^I select the fitment vehicle \"(.*?)\"$")
    public void i_select_a_fitment_vehicle(String vehicle) throws Throwable {
        fitmentPopupPage.selectFitmentVehicle(vehicle);
    }

    @Then("^I am brought to the fitment page with the \"(.*?)\" vehicle message$")
    public void i_am_brought_to_the_details_page_with_message(String optionType) throws Throwable {
        fitmentPopupPage.assertVehicleDetailsMessage(optionType);
    }

    @And("^I verify all the \"([^\"]*)\" fitment option links by clicking on them$")
    public void i_verify_all_the_fitment_option_links_by_clicking_on_them(String optionType) throws Throwable {
        fitmentPopupPage.verifyAllFitmentLinksWork(optionType);
    }

    @Then("^I verify the selected fitment box option has a value of \"([^\"]*)\"$")
    public void i_verify_selected_fitment_box_option_has_a_value_of(String value) throws Throwable {
        fitmentPopupPage.assertFitmentBoxIsSelected(value);
    }

    @Then("^I verify the fitment box option has a value of \"([^\"]*)\"$")
    public void i_verify_fitment_box_option_has_a_value_of(String value) throws Throwable {
        fitmentPopupPage.assertFitmentBoxValue(value);
    }

    @And("^I select the \"([^\"]*)\" fitment box option$")
    public void i_select_the_fitment_box_option(String value) throws Throwable {
        fitmentPopupPage.selectFitmentBoxOption(value);
    }

    @And("^I select 'Change' on the vehicle fitment panel page")
    public void i_select_change_vehicle_on_vehicle_fitment_panel_page() throws Throwable {
        fitmentPopupPage.clickChangeVehicle();
    }

    @And("^I select the \"([^\"]*)\" (menu|staggered menu) option$")
    public void i_select_the_menu_option(String value, String menuOptionType) throws Throwable {
        fitmentPopupPage.clickMenuOption(value, menuOptionType);
    }

    @And("^I verify no duplicate Optional tire sizes are displayed$")
    public void i_verify_no_duplicate_option_tire_sizes_are_displayed() throws Throwable {
        fitmentPopupPage.assertNoDuplicateOptionalTireSizes();
    }

    @Then("^Treadwell fitment box in \"(homepage|popup page)\" should be \"(displayed|not displayed)\"$")
    public void i_should_see_treadwell_fitment_box(String page, String option) throws Throwable {
        fitmentPopupPage.assertTreadwellFitmentBox(page, option);
    }

    @And("^I select find tires using Treadwell$")
    public void i_select_find_your_tires_using_treadwell() throws Throwable {
        fitmentPopupPage.clickTreadwellButton();
    }

    @Then("^I verify optional tire and wheel size is displayed$")
    public void i_verify_optional_tire_and_wheel_size_is_displayed() throws Throwable {
        commonActions.assertWebElementDisplayed(fitmentPopupPage.returnWebElement(CommonActions.OPTIONAL_TIRE_AND_WHEEL_SIZE)
                , CommonActions.OPTIONAL_TIRE_AND_WHEEL_SIZE);
    }

    @Then("^I verify what are you shopping for heading is displayed$")
    public void i_verify_what_are_you_shopping_for_heading_is_displayed() throws Throwable {
        commonActions.assertWebElementDisplayed(fitmentPopupPage.returnWebElement(CommonActions.WHAT_ARE_YOU_SHOPPING_FOR),
                CommonActions.WHAT_ARE_YOU_SHOPPING_FOR);
    }

    @Then("^I verify no duplicate optional fitment sizes are displayed$")
    public void i_verify_no_duplicate_fitment_size_are_displayed() throws Throwable {
        fitmentPopupPage.assertFitmentOptionalSizes();
    }

    @Then("^I verify \"(O.E.)\" badging is displayed on \"(.*?)\"$")
    public void i_verify_badging_is_displayed_on_size(String badge, String size) throws Throwable {
        fitmentPopupPage.assertBadgeDisplayedForSize(badge, size);
    }

    @Then("^I verify that the optional tire and wheel size is \"(expanded|collapsed)\"$")
    public void i_verify_that_the_optional_tire_and_wheel_size_expanded_or_collapsed(String state) throws Throwable {
        fitmentPopupPage.assertOptionalSizeHeader(state);
    }

    @When("^I select revert to O.E. size$")
    public void i_select_revert_to_oe_size() throws Throwable {
        driver.clickElementWithText(FitmentPopupPage.revertBackToOEBy, FitmentPopupPage.REVERT_BACK_TO_OE);
    }

    @Then("^I verify fitment popup page displays$")
    public void i_verify_fitment_popup_page_display() throws Throwable {
        searchAutocompleteDropDownPage.verifySearchResultPageElementExists(ConstantsDtc.FITMENT_POPUP);
    }

    @Then("^I verify revert to O.E size is displayed$")
    public void i_verify_revert_to_oe_size_is_displayed() throws Throwable {
        commonActions.assertWebElementDisplayed(fitmentPopupPage.returnWebElement(CommonActions.REVERT_TO_OE_SIZE)
                , CommonActions.REVERT_TO_OE_SIZE);
    }

    @Then("^I verify that the optional size message for \"(.*?)\" is displayed$")
    public void i_verify_that_the_optional_size_message_size_is_displayed(String size) throws Throwable {
        fitmentPopupPage.assertOptionalSizeMessage(size);
    }
}