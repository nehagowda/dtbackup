package dtc.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.pages.BrandsPage;
import dtc.pages.CommonActions;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/26/16.
 */
public class BrandsPageSteps {

    private BrandsPage brandsPage;
    private CommonActions commonActions;

    public BrandsPageSteps(Driver driver) {
        brandsPage = new BrandsPage(driver);
        commonActions = new CommonActions(driver);
    }

    @Then("^I see the \"(.*?)\" that I selected$")
    public void i_see_the_that_I_selected(String brand) throws Throwable {
        brandsPage.verifyBrandSelected(brand);
    }

    @When("^I select \"(.*?)\" to shop$")
    public void i_select_to_shop(String subCategory) throws Throwable {
        if (subCategory.equalsIgnoreCase(ConstantsDtc.TIRES)) {
            brandsPage.clickShopBrandCategoryTires(subCategory);
        } else {
            commonActions.clickElementWithLinkText(subCategory);
        }
    }
    
    @Then("^I can see selected \"(.*?)\" under refinements section on PLP page$")
    public void i_can_see_selected_refinement_under_refinements_section_on_PLP_page(String refinement) throws Throwable {
        brandsPage.verifyBrandRefinementSelected(refinement);
    }

    @And("^I select the \"([^\"]*)\" brand image$")
    public void i_select_the_brand_image(String brand) throws Throwable {
        brandsPage.selectBrandImage(brand);
    }

    @When("^I select fitment by brand for \"(.*?)\"$")
    public void i_select_fitment_by_brand_for(String category) throws Throwable {
        brandsPage.selectBrand(category);
    }

    @When("^I select view brands that don't fit your vehicle dropdown$")
    public void I_select_view_brands_that_dont_fit_your_vehicle_dropdown() throws Throwable {
        brandsPage.selectViewBrandsThatDontFit();
    }

    @Then("^I verify \"([^\"]*)\" Brand Category page is displayed$")
    public void i_verify_brand_category_page_displayed(String brand) throws Throwable {
        brandsPage.assertBrandCategoryPage(brand);
    }
}