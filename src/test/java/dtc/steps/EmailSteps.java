package dtc.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.pages.EmailPage;
import utilities.Driver;

/**
 * Created by Mukul Garg on 8/30/2017.
 */
public class EmailSteps {
    private EmailPage emailPage;

    public EmailSteps(Driver driver) {
        emailPage = new EmailPage(driver);
    }

    @When("^I open Outlook inbox username: \"(.*?)\" and password: \"(.*?)\"$")
    public void i_open_outlook_inbox(String emailInbox, String password) throws Throwable {
        emailPage.openInbox(emailInbox, password);
    }

   @Then("^I verify \"(.*?)\" is displayed in \"(Order Confirmation|Appointment Confirmation)\" email")
    public void i_verify_order_details_in_order_confirmation_email(String text, String email) throws Throwable {
        if (text.equalsIgnoreCase(ConstantsDtc.STORE_DETAILS)) {
            emailPage.verifyStore(email);
        } else {
            emailPage.verifyProductCode(text, email);
        }
    }

    @Then("^I verify customer name for \"(.*?)\" in \"(Order Confirmation|Appointment Confirmation)\" email$")
    public void i_verify_customer_name_in_order_confirmation_email(String customerType, String email) throws Throwable {
        emailPage.verifyCustomerName(customerType, email);
    }

    @When("^I search inbox with the order number and open \"(Order Confirmation|Appointment Confirmation)\" email")
    public void i_search_inbox_with_orderid_and_open_confirmation_email(String emailType) throws Throwable {
        emailPage.assertOrderEmailInInbox();
        emailPage.openConfirmationEmail(emailType);
    }
}