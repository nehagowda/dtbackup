package dtc.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.pages.CommonActions;
import dtc.pages.ProductBrandsPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/27/16.
 */
public class ProductBrandsPageSteps {

    private ProductBrandsPage productBrandsPage;
    private CommonActions commonActions;

    public ProductBrandsPageSteps(Driver driver) {
        commonActions = new CommonActions(driver);
        productBrandsPage = new ProductBrandsPage(driver);
    }

    @When("^I select \"([^\"]*)\" from the Product Brands page$")
    public void i_select_from_product_brands_page(String option) throws Throwable {
        productBrandsPage.selectBrandOption(option);
    }

    @And("^I select \"([^\"]*)\" from the Product Brand page$")
    public void i_select_option_from_the_product_brand_page(String option) throws Throwable {
        productBrandsPage.selectTireWheelBrandOption(option);
    }

    @And("^I select shop all from the Product Brand page$")
    public void i_select_shop_all_from_the_product_brand_page() throws Throwable {
        ProductBrandsPage.shopAllProductsButtonLink.click();
    }

    @Then("^I verify all listed brands have corresponding images$")
    public void i_verify_all_listed_brands_have_images() throws Throwable {
        productBrandsPage.verifyAllListBrandsHaveImages();
    }

    @Then("^I verify all the models in brand page are expanded$")
    public void i_verify_all_the_models_in_brand_page_are_expanded() throws Throwable {
        productBrandsPage.verifyAllModelsExpanded();
    }

}