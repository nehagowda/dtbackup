package dtc.steps;

import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import dtc.data.ConstantsDtc;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.Product;
import dtc.data.Vehicle;
import dtc.pages.CommonActions;
import dtc.pages.HomePage;
import dtc.pages.HeaderPage;
import dtc.pages.StoreDetailsPage;
import dtc.pages.MobileHeaderPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/24/16.
 */
public class HomePageSteps {

    private HomePage homePage;
    private StoreLocatorPageSteps storeLocatorPageSteps;
    private HeaderPage headerNavigationPage;
    private MobileHeaderPage mobileHeaderPage;
    private Driver driver;
    private WebDriver webDriver;
    private Product product;
    private Scenario scenario;
    private StoreDetailsPage storeDetailsPage;
    private CommonActions commonActions;
    private Vehicle vehicle;

    public HomePageSteps(Driver driver) {
        homePage = new HomePage(driver);
        headerNavigationPage = new HeaderPage(driver);
        storeLocatorPageSteps = new StoreLocatorPageSteps(driver);
        headerNavigationPage = new HeaderPage(driver);
        mobileHeaderPage = new MobileHeaderPage(driver);
        storeDetailsPage = new StoreDetailsPage(driver);
        commonActions = new CommonActions(driver);
        this.driver = driver;
        webDriver = driver.getDriver();
        product = new Product();
        vehicle = new Vehicle();
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    //TODO: This should be refactored to be a commonActions method in order to leverage driver methods and it's
    //TODO (cont) become inappropriate for a step definition method
    @Given("^I change to the default store$")
    public void i_change_to_the_default_store() throws Throwable {
        String region = Config.getSiteRegion();
        String storeCode;
        if (!region.equals(ConstantsDtc.DTD)) {
            storeLocatorPageSteps.i_navigate_and_change_to_the_default_store_through_url();
            storeCode = Config.getDefaultStoreCode();
            driver.scenarioData.genericData.put(Constants.STORE_DETAILS,
                    StoreDetailsPage.storeAddress.getText().trim());
            scenario.write(Config.getDefaultStore() + " (" + Config.getDefaultStoreCode() + ")\n" +
                    Config.getDefaultStoreAddress());
            if (Config.isMobile()) {
                mobileHeaderPage.clickMobileSiteLogo();
            } else {
                commonActions.clickSiteLogo();
            }
        } else {
            homePage.goToHome();
            storeCode = Constants.AZ_DC;
        }
        driver.scenarioData.genericData.put(Constants.STORE_CODE, storeCode);
    }

    @When("^I change to the integrated test store$")
    public void i_change_to_the_integrated_test_store() throws Throwable {
        String region = Config.getSiteRegion();

        if (region.equals(ConstantsDtc.DT)) {
            storeLocatorPageSteps.i_navigate_and_change_to_the_integrated_store_through_url();

            if (Config.isMobile()) {
                mobileHeaderPage.clickMobileSiteLogo();
            } else {
                commonActions.clickSiteLogo();
            }
        } else {
            // This conditional failure can be changed if we start using other regions
            Assert.fail("FAIL: Integrated tests are done in the DT region, '" + region + "' region is invalid.");
        }
    }

    @Given("^I go to the homepage$")
    public void i_go_to_the_homepage() throws Throwable {
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            i_change_to_the_default_store();
        } else {
            homePage.goToHome();
        }
    }

    @Given("^I go to the Geoip store homepage$")
    public void i_go_to_the_geoip_store_homepage() throws Throwable {
        homePage.goToGeoIpStoreHomePage();
    }

    @Given("^I go to the \"([^\"]*)\" state homepage$")
    public void i_go_to_the_state_homepage(String state) throws Throwable {
        homePage.goToStateIpStoreHomePage(state);
    }

    @When("^I open the fitment popup$")
    public void i_open_the_fitment_popup() throws Throwable {
        homePage.openFitmentPopup();
    }

    @When("^I open the My Vehicles popup$")
    public void i_open_the_my_vehicles_popup() throws Throwable {
        homePage.openMyVehiclesPopup();
        if (Config.isMobile())
            mobileHeaderPage.clickMenuLink(ConstantsDtc.MY_VEHICLES);
    }

    @When("^I remove my \"([^\"]*)\" vehicle$")
    public void i_remove_vehicle(String vehicleSection) throws Throwable {
        homePage.removeSpecifiedVehicle(vehicleSection);
    }

    @When("^I remove my \"([^\"]*)\" vehicle type$")
    public void i_remove_vehicle_type(String vehicleSection) throws Throwable {
        Vehicle veh = vehicle.getVehicle(vehicleSection);
        scenario.write(veh.toString());
        homePage.removeSpecifiedVehicle(veh.year + veh.make + veh.model);
    }


    @And("^I verify the default \"([^\"]*)\" in the popup$")
    public void i_verify_the_default_in_the_popup(String storeAddress) throws Throwable {
        homePage.verifyMyStoreDetails(storeAddress);
        homePage.clickGlobalMyStoreHeader();
    }

    @Then("^I click on Store details button in My Store popup$")
    public void i_click_on_store_details_button_in_my_store_popup_page() throws Throwable {
        homePage.openStoreDetails();
    }

    @When("^I click on \"My Store\" title$")
    public void i_click_on_my_store_title() throws Throwable {
        homePage.clickGlobalMyStoreHeader();
    }

    @When("^I select mini cart and \"(Continue Shopping|View Cart|Clear Cart)\"$")
    public void i_select_mini_cart_icon_and(String action) throws Throwable {
        switch (action) {
            case ConstantsDtc.CONTINUE_SHOPPING:
                homePage.openMiniCartPopupAndContinueShopping();
                break;
            case ConstantsDtc.VIEW_CART:
                homePage.openMiniCartPopupAndViewCart();
                break;
            case ConstantsDtc.CLEAR_CART:
                homePage.openMiniCartPopupAndClearCart();
                homePage.clickMiniCartIcon();
                break;
        }
    }

    @When("^I schedule an appointment for my current store$")
    public void i_schedule_appointment_for_current_store() throws Throwable {
        if (Config.isMobile()) {
            mobileHeaderPage.openMobileMenu();
            mobileHeaderPage.clickMenuLink(ConstantsDtc.SCHEDULE_APPOINTMENT);
        } else {
            homePage.openScheduleAppointment();
        }
    }

    @When("^I open the Store Locator page$")
    public void i_open_the_store_locator_page() throws Throwable {
        if (Config.isMobile()) {
            mobileHeaderPage.clickHeaderLink(MobileHeaderPage.FIND_A_STORE);
        } else {
            homePage.openChangeStore();
        }
    }

    @When("^I do a free text search for \"(.*?)\"$")
    public void i_do_a_free_text_search_for(String value) throws Throwable {
        if (value.startsWith(ConstantsDtc.PRODUCT.toUpperCase())) {
            value = product.getProduct(value).productCode;
            scenario.write("Product Code: '" + value + "'");
        }
        if (Config.isMobile()) {
            mobileHeaderPage.searchItem(value);
        } else {
            homePage.searchItem(value);
        }
    }

    @When("^I do a free text search for \"([^\"]*)\" and hit enter")
    public void i_do_a_free_text_search_and_hit_enter(String value) throws Throwable {
        if (value.startsWith(ConstantsDtc.PRODUCT.toUpperCase())) {
            value = product.getProduct(value).productCode;
            scenario.write("Product Code: '" + value + "'");
        }
        if (Config.isMobile()) {
            mobileHeaderPage.searchItem(value);
        } else {
            homePage.searchItemHitEnter(value);
        }
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
    }

    @Then("^I verify My Vehicle in the header displays as \"(.*?)\"$")
    public void i_verify_my_vehicle_in_header(String vehicle) throws Throwable {
        homePage.verifyMyVehicle(vehicle);
    }

    @Then("^I verify the header cart total is \"(.*?)\"$")
    public void i_verify_header_cart_item_total(String total) throws Throwable {
        homePage.verifyCartTotal(total);
    }

    @Then("^I verify the header cart item count is \"(.*?)\"$")
    public void i_verify_header_cart_item_count(String count) throws Throwable {
        homePage.verifyCartItemCount(count);
    }

    @Then("^I open mini cart and verify the item qty is \"(.*?)\"$")
    public void i_verify_the_mini_cart_modal_item_qty(String qty) throws Throwable {
        homePage.assertItemQtyMiniCartModal(qty);
    }

    @Then("^I verify the \"STORE HOURS\" in the My Store popup$")
    public void i_verify_store_hours_my_store_popup() throws Throwable {
        homePage.verifyMyStoreHours();
    }

    @Then("^I am brought to the homepage$")
    public void i_verify_i_am_on_homepage() throws Throwable {
        homePage.verifyHomepage();
    }

    @Then("^I verify the site logo$")
    public void i_verify_the_site_logo() throws Throwable {
        homePage.verifySiteLogo();
    }

    @When("^I select mini cart$")
    public void i_select_mini_cart() throws Throwable {
        homePage.clickMiniCartIcon();
    }

    @Then("^I verify Join/Sign-in is displayed on \"(homepage|PLP|PDP|Checkout|global header)\" page$")
    public void i_verify_the_join_sign_in_is_displayed_on_page(String page) throws Throwable {
        homePage.verifyJoinSignInIsDisplayed(page);
    }

    @Then("^I verify \"(tax exempt|Get started with Treadwell|Create Account/Sign In|edit)\" is displayed on \"(Shopping cart page|Checkout page|Order Confirmation page|global header|fitment modal|MyVehicles Modal)\"$")
    public void i_verify_the_link_is_displayed(String text, String location) throws Throwable {
        homePage.verifyLinkIsDisplayed(text, location);
    }

    @When("^I select checkout with appointment$")
    public void i_select_checkout_with_appointment() throws Throwable {
        homePage.clickCheckoutWithAppointmentButton();
    }

    @When("^I select my account header navigation$")
    public void i_select_my_account_header_navigation() throws Throwable {
        homePage.clickJoinSignInLink();
    }

    @Then("^I verify the \"My Store\" popup contains controls: \"CHANGE STORE, STORE DETAILS, SCHEDULE APPOINTMENT\"$")
    public void i_verify_my_store_popup_contains_controls() throws Throwable {
        homePage.verifyMyStoreContainsControls();
    }

    @And("^I verify the Wheel Configurator \"([^\"]*)\" is displayed$")
    public void i_verify_the_wheel_configurator_text_is_displayed(String text) throws Throwable {
        homePage.assertWheelConfiguratorTextIsDisplayed(text);
    }

    @And("^I verify the Wheel Configurator image is displayed$")
    public void i_verify_the_wheel_configurator_image_is_displayed() throws Throwable {
        homePage.assertWheelConfiguratorImageIsDisplayed();
    }

    @And("^I verify 'BROWSE WHEELS' is displayed$")
    public void i_verify_browse_wheels_is_displayed() throws Throwable {
        homePage.assertBrowseWheelsIsDisplayed();
    }

    @Then("^I verify the current 'My Store' matches the selection from the 'Check Inventory' popup$")
    public void i_verify_my_store_matches_selection_from_check_inventory_popup() throws Throwable {
        homePage.verifyMyStoreMatchesCheckInventoryPopupSelectionByPhoneNumber();
    }

    @And("^I extract the \"(Tire|Wheel)\" size on the home page$")
    public void i_extract_size_on_the_home_page(String text) throws Throwable {
        homePage.extractTireOrWheelSizeOnHomepage(text);
    }

    @Then("^I verify extracted \"(Tire|Wheel)\" size with size on PDP page$")
    public void i_verify_extracted_size_with_size_from_page(String elementName) throws Throwable {
        homePage.assertTireOrWheelSizeOnPDP(elementName);
    }
}
