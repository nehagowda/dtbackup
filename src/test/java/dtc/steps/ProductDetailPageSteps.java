package dtc.steps;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.data.Product;
import dtc.pages.CommonActions;
import dtc.pages.ProductDetailPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/24/16.
 */
public class ProductDetailPageSteps {

    private Driver driver;
    private ProductDetailPage productDetailPage;
    private CommonActions commonActions;
    private final String elementNameList = "\"(Enter vehicle|help me choose|Check nearby stores|what is Load Range|" +
            "what is OE Designation|Mail In Rebate|Promotions|Deals and Rebates|Subtotal|Sub total tool tip|" +
            "Load Index / Speed Rating|Tire Size|Add To Cart)\"";
    private Scenario scenario;
    private Product product;

    public ProductDetailPageSteps(Driver driver) {
        this.driver = driver;
        productDetailPage = new ProductDetailPage(driver);
        commonActions = new CommonActions(driver);
        product = new Product();
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @Then("^I should see product detail page with \"(.*?)\"$")
    public void i_should_see_the_product_detail_page_with(String productName) throws Throwable {
        if (productName.startsWith(ConstantsDtc.PRODUCT.toUpperCase())) {
            productName = product.getProduct(productName).productName;
            scenario.write("Product Name: '" + productName + "'");
        }
        productDetailPage.assertProductNameOnProductDetailPage(productName);
    }

    @Then("^I should see product specification \"(.*?)\" value to be \"(.*?)\"$")
    public void i_should_see_product_specification_value_to_be(String specLabel, String specValue) throws Throwable {
        productDetailPage.assertProductSpecValue(specLabel, specValue);
    }

    @Then("^I should see product specification label \"(.*?)\" present on the page$")
    public void i_should_see_product_specification_label_present(String specLabel) throws Throwable {
        productDetailPage.assertProductSpecLabelVisible(specLabel);
    }

    @Then("^I should see product inventory stock message displayed on the page$")
    public void i_should_see_the_product_inventory_stock_details() throws Throwable {
        productDetailPage.assertProductInventoryMessage();
    }

    @Then("^I verify that the price for the product on the product details page is \"(.*?)\"$")
    public void i_verify_product_price_pdp(String price) throws Throwable {
        productDetailPage.assertProductPrice(price);
    }

    @And("^I verify expected \"(Tire|Wheel)\" attributes are displayed for product detail page section \"(.*?)\"$")
    public void i_verify_expected_type_attributes_for_product_detail_page_section
            (String productType, String pageSection) throws Throwable {
        productDetailPage.verifyExpectedTypeAttributesForProductDetailSection(productType, pageSection);
    }

    @And("^I verify the \"We cannot ship items to locations\" message is \"(displayed|not displayed)\" on the product detail page$")
    public void i_verify_cannot_ship_message_displayed_on_product_detail_page(String displayStatus) throws Throwable {
        productDetailPage.verifyCannotShipMessageForProductDetailPage(displayStatus);
    }

    @Then("^I verify \"(Standard|Canonical)\" PDP page is displayed$")
    public void i_verify_pdp_page_is_displayed(String pdpType) throws Throwable {
        productDetailPage.verifyProductDetailsPageTypeDisplayed(pdpType);
    }

    @Then("^I verify the stock count message on PDP$")
    public void i_verify_the_stock_count_message_on_pdp() throws Throwable {
        productDetailPage.verifyStockCountTextPdp();
    }

    @Then("^I should see \"([^\"]*)\" on product details page$")
    public void i_should_see_product_on_detail_page(String product) throws Throwable {
        productDetailPage.assertItemIdOnProductDetailPage(product);
    }

    @And("^I verify the free shipping label on PDP$")
    public void i_verify_the_free_shipping_label_on_pdp() throws Throwable {
        productDetailPage.verifyFreeShippinglabelPdp();
    }

    @Then("^I verify Customer review portion on PDP contains \"(.*?)\" text$")
    public void i_verify_customer_review_portion_on_pdp_contains_text(String text) throws Throwable {
        productDetailPage.assertReviewPortionText(text);
    }

    @And("^I verify product info on the PDP page$")
    public void i_verify_product_info_on_the_pdp_page() throws Throwable {
        productDetailPage.assertProductBrandAndNameOnPdpPage();
    }

    @When("^I click " + elementNameList + " element in PDP$")
    public void i_click_element_in_pdp(String elementName) throws Throwable {
        productDetailPage.clickWebElementOnPdpPage(elementName);
    }

    @Then("^I verify " + elementNameList + " exists in PDP$")
    public void i_verify_if_elements_exists_in_pdp(String elementName) throws Throwable {
        productDetailPage.verifyIfEachElementInTheListIsDisplayed(elementName);
    }

    @Then("^I verify PDP has single price$")
    public void i_verify_pdp_has_single_price() throws Throwable {
        productDetailPage.assertSinglePriceOnPdp();
    }

    @Then("^I verify the product availability section is displayed$")
    public void i_verify_the_product_availability_section_is_displayed() throws Throwable {
        productDetailPage.assertProductAvailibilitySectionOnPdp();
    }


    @And("^I verify price range on PDP page$")
    public void i_verify_price_range_on_pdp_page() throws Throwable {
        productDetailPage.assertPdpPriceRange();
    }

    @And("^I verify canonical product attributes on PDP \"(TIRES|WHEELS)\"$")
    public void i_verify_canonical_product_attributes_on_pdp(String option) throws Throwable {
        productDetailPage.checkAttributeOptions(option);
    }

    @And("^I verify \"(Load Range|Load Index|OE Designation)\" description in popup$")
    public void i_verify_description_in_popup(String popUpName) throws Throwable {
        productDetailPage.assertPopUpText(popUpName);
    }

    @And("I verify calculation of subtotal using qty and retail price on PDP$")
    public void i_verify_calculation_of_subtotal_using_qty_and_retail_price_on_pdp() throws Throwable {
        productDetailPage.assertPdpSubtotalCalculation();
    }

    @And("I close \"(Load Range|Load Index|OE Designation)\" popup in PDP$")
    public void i_close_popup_in_pdp(String popUpName) throws Throwable {
        productDetailPage.closeLoadRangeOrLoadIndexPopup(popUpName);
    }

    @And("I verify review ratings on Canonical PDP page$")
    public void i_verify_review_ratings_on_canonical_pdp_page() throws Throwable {
        productDetailPage.assertPdpReviewRatingDetails();
    }

    @And("^I verify \"(.*?)\" text not present on PDP page$")
    public void i_verify_text_not_present_on_pdp_page(String text) throws Throwable {
        productDetailPage.assertTextNotPresentOnPdp(text);
    }

    @And("^I verify quantity value \"(.*?)\" on PDP page$")
    public void i_verify_quantity_value_on_pdp_page(String value) throws Throwable {
        productDetailPage.assertDefaultQuantityValueOnPdp(value);
    }

    @Then("^I verify Sub Total Tool Tip has the required text in PDP$")
    public void i_verify_tire_sub_total_tool_tip_has_the_required_text_in_pdp() throws Throwable {
        productDetailPage.verifyTextOfElementPdp(ConstantsDtc.TOOL_TIP_CONTENT,
                ConstantsDtc.SUB_TOTAL_TOOL_TIP_MESSAGE);
    }

    @Then("^I verify element \"(.*?)\" has text \"(.*?)\" in PDP$")
    public void i_verify_element_has_text_in_pdp(String elementName, String validation) throws Throwable {
        productDetailPage.verifyTextOfElementPdp(elementName, validation);
    }

    @And("^I verify selected wheel color matches with displayed wheel image on PDP$")
    public void i_verify_selected_wheel_color_matches_with_displayed_wheel_image_on_pdp() throws Throwable {
        productDetailPage.verifyColorSwatchWithWheelImage();
    }

    @And("^I verify bread crumb section contains the text \"(.*?)\"$")
    public void i_verify_bread_crumb_section_contains_the_text(String text) throws Throwable {
        productDetailPage.assertBreadCrumbsText(text);
    }

    @And("^I verify add to cart button is \"(ENABLED|DISABLED|DISPLAYED)\"$")
    public void i_verify_add_to_cart_button_is(String checkOption) throws Throwable {
        productDetailPage.assertPdpAddToCartEnabledDisabledDisplayed(checkOption);
    }

    @And("^I verify product has mileage warranty \"(.*?)\"$")
    public void i_verify_product_has_mileage_warranty(String withoutORWithRange) throws Throwable {
        productDetailPage.verifyMileWarrantyRange(withoutORWithRange);
    }

    @And("^I verify tooltip and tooltip text for each icon on PDP$")
    public void i_verify_tooltip_and_tooltip_text_for_each_icon_on_pdp() throws Throwable {
        productDetailPage.verifyTooltipTextOnPdp();
    }

    @Then("^I verify the values of \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" dropdown$")
    public void i_verify_the_values_of_dropdown(String width, String ratio, String diameter) throws Throwable {
        commonActions.verifyPdpTireSizeDropdownValues(width, ratio, diameter);
    }

    @And("^I verify the tire size dropdowns are populated on the PDP page$")
    public void i_verify_the_tire_size_dropdowns_are_populated() throws Throwable {
        commonActions.verifyPdpTireSizeDropDownsArePopulated();
    }

    @When("^I select \"([^\"]*)\" value from \"([^\"]*)\" drop down on PDP page for \"([^\"]*)\"$")
    public void i_select_value_from_drop_down_on_pdp_page_for(String dropDownValue, String dropDown, String category) throws Throwable {
        commonActions.chooseValueFromPdpDropdown(dropDownValue, dropDown, category);
    }

    @When("^I set first available size options on PDP page$")
    public void i_set_first_available_size_options_on_pdp_page() throws Throwable {
        commonActions.selectAllFirstAvailableSizeOptionsOnPdpPage();
    }

    @And("^I extract the product details on the PDP page$")
    public void i_extract_the_product_details_on_the_pdp_page() throws Throwable {
        commonActions.extractAndSaveProductInfoOnPdpPage();
    }

    @Then("^I verify the values in \"([^\"]*)\" \"([^\"]*)\" dropdown for wheels$")
    public void i_verify_the_values_in_dropdown_for_wheels(String width, String diameter) throws Throwable {
        commonActions.verifyDropDownValuesForWheels(width, diameter);
    }

    @Then("^I verify the \"([^\"]*)\" Link color and help text$")
    public void i_verify_the_link_color_and_help_text(String linkText) throws Throwable {
        productDetailPage.verifyLinkTextColorAndHelpText(linkText);
    }

    @And("^I verify categories present in all the review blocks in PDP page")
    public void i_verify_categories_present_in_all_the_review_blocks_in_pdp_page(DataTable table) throws Throwable {
        productDetailPage.verifyTextPresentInReviewBlocks(table);
    }

    @Then("^I verify payment plan available is \"(not displayed|displayed)\"$")
    public void i_verify_payment_plan_available_is_displayed(String option) throws Throwable {
        productDetailPage.verifyFinancingLearnMorePresent(option);
    }

    @Then("^I verify the \"(.*?)\" size is displayed on PDP page$")
    public void i_verify_the_size_is_displayed_on_pdp_page(String size) throws Throwable {
        productDetailPage.verifyTextOfElementPdp(ProductDetailPage.SIZE, size);
    }
}