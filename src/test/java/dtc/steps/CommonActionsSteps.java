package dtc.steps;

import common.Config;
import common.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.BrowsePageData;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.data.Vehicle;
import dtc.pages.CommonActions;
import dtc.pages.ProductListPage;
import dtc.pages.ProductDetailPage;
import dtc.pages.CartPage;
import dtc.pages.SolicitedReviewPage;
import dtc.pages.HomePage;
import dtc.pages.CheckoutPage;
import dtc.pages.FooterPage;
import utilities.CommonUtils;
import utilities.DomObjectsExtractor;
import utilities.Driver;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 10/24/16.
 */
public class CommonActionsSteps {
    private CheckoutPageSteps checkoutPageSteps;
    private CommonActions commonActions;
    private HomePageSteps homePageSteps;
    private OrderPageSteps orderPageSteps;
    private Customer customer;
    private ProductListPage productListPage;
    private ProductDetailPage productDetailPage;
    private FitmentPageSteps fitmentPageSteps;
    private FooterPage footerPage;
    private CartPage cartPage;
    private SearchAutocompleteDropDownPageSteps searchAutocompleteDropDownPageSteps;
    private DomObjectsExtractor domObjectsExtractor;
    private SolicitedReviewPage solicitedReviewPage;
    private HomePage homePage;
    private Driver driver;
    private final Logger LOGGER = Logger.getLogger(CommonActionsSteps.class.getName());
    private Vehicle vehicle;

    public static double cartSubtotal;
    public static double checkoutSubtotal;
    public static double orderTotal;
    public static double salesTax;
    public static double salesTaxCheckout;
    public static double orderTotalCheckout;
    public static double pdpPrice;
    public static double miniCartQuickTotal;
    public static String storeName;
    public static String pdpProductName;

    public static HashMap<String, String> plpProductPrice = new HashMap<>();
    public static HashMap<String, String> cartProductPrice = new HashMap<>();

    public CommonActionsSteps(Driver driver) throws IOException {
        this.driver = driver;
        checkoutPageSteps = new CheckoutPageSteps(driver);
        commonActions = new CommonActions(driver);
        homePageSteps = new HomePageSteps(driver);
        orderPageSteps = new OrderPageSteps(driver);
        fitmentPageSteps = new FitmentPageSteps(driver);
        customer = new Customer();
        productListPage = new ProductListPage(driver);
        productDetailPage = new ProductDetailPage(driver);
        searchAutocompleteDropDownPageSteps = new SearchAutocompleteDropDownPageSteps(driver);
        domObjectsExtractor = new DomObjectsExtractor(driver);
        cartPage = new CartPage(driver);
        solicitedReviewPage = new SolicitedReviewPage(driver);
        homePage = new HomePage(driver);
        footerPage = new FooterPage(driver);
        vehicle = new Vehicle();
    }

    @And("^I verify the \"([^\"]*)\" line item is present on the shopping cart page$")
    public void i_verify_the_line_item_is_present_on_the_shopping_cart_page(String tipContainerText) throws Throwable {
        commonActions.assertElementWithTextIsVisible(CommonActions.optionNameBy, tipContainerText);
    }

    @Then("^I confirm that taxes are listed on the \"(order|checkout)\" page$")
    public void i_confirm_that_taxes_are_listed_on_the_page(String page) throws Throwable {
        commonActions.confirmTaxes(page, CommonActions.ZERO_DOLLARS, true);
    }

    @And("^I confirm that fees are listed on the \"(order|checkout)\" page$")
    public void i_confirm_that_fees_are_listed_on_the_page(String page) throws Throwable {
        if (page.equalsIgnoreCase(ConstantsDtc.ORDER)) {
            orderPageSteps.i_expand_fee_details_for_item_listed_on_order_confirmation_page();
        } else {
            checkoutPageSteps.i_expand_cart_item_details_section_of_cart_summary_on_checkout_page();
            checkoutPageSteps.i_expand_fee_details_for_item_listed_in_cart_summary_on_checkout_page();
        }
        //TODO CCL - could set last arg to true if we knew what amount was to be expected instead of CommonActions.ZERO_DOLLARS ($0)
        commonActions.confirmFees(page, CommonActions.ZERO_DOLLARS, false);
    }

    @Then("^I am brought to the page with path \"(.*?)\"$")
    public void i_am_brought_to_the_page_with_path(String path) throws Throwable {
        commonActions.waitForUrl(path, Constants.THIRTY);
    }

    @Then("^I am brought to the page with header \"(.*?)\"$")
    public void i_am_brought_to_the_page_with_header(String headerText) throws Throwable {
        if (headerText.equalsIgnoreCase(ConstantsDtc.MY_ACCOUNT)) {
            headerText = ConstantsDtc.HELLO +
                    customer.getCustomer(Customer.CustomerType.MY_ACCOUNT_USER_A.toString()).firstName;
        }
        commonActions.assertPageHeader(headerText);
    }

    @Then("^I am brought to the \"(America's Tire|Discount Tire|Discount Tire Direct)\" site$")
    public void i_am_brought_to_the_site_for(String siteToValidate) throws Throwable {
        commonActions.assertPageTitle(siteToValidate);
    }

    @And("^I go to the \"(next|previous|First|Last)\" page of product list results$")
    public void i_go_to_next_prev_page_of_product_results(String direction) throws Throwable {
        commonActions.navToDifferentPageOfResults(direction);
    }

    @When("^I \"(continue|Search in another area)\" the Welcome Popup$")
    public void i_continue_welcome_popup(String action) throws Throwable {
        commonActions.closeWelcomePopUp(action);
    }

    @And("^I navigate back to previous page$")
    public void i_navigate_to_previous_page() throws Throwable {
        commonActions.navigateToPreviousPage();
    }

    @And("^I navigate to newly opened next tab$")
    public void i_navigate_to_already_opened_next_tab() throws Throwable {
        commonActions.switchToNextOpenedTab();
    }

    @And("^I navigate to previous browser tab$")
    public void i_navigate_to_previous_browser_tab() throws Throwable {
        commonActions.switchToPreviousOpenedTab();
    }

    @Then("^I verify the window with header \"([^\"]*)\" is displayed$")
    public void i_verify_the_page_with_header_is_displayed(String text) throws Throwable {
        commonActions.switchToWindowWithHeader(text);
    }

    @Then("^I verify the header subtext \"([^\"]*)\" on the page$")
    public void i_verify_the_header_subtext_on_the_page(String text) throws Throwable {
        commonActions.assertPageElementTextByElement(FooterPage.payYourBillHeaderSubtextBy, text);
    }

    @Then("^I should see text \"(.*?)\" present in the page source$")
    public void i_should_see_text_present_on_the_page(String text) throws Throwable {
        commonActions.assertTextPresentInPageSource(text);
    }

    @And("^I verify the \"(.*?)\" link in the breadcrumb container$")
    public void i_verify_the_link_in_the_breadcrumb_container(String linkText) throws Throwable {
        commonActions.verifyBreadcrumbLinks(linkText);
    }

    @And("^I navigate to the \"([^\"]*)\" url$")
    public void i_navigate_to_the_url(String urlLink) throws Throwable {
        commonActions.navigateToPage(urlLink);
    }

    @And("^I verify \"(Customer Reviews|Complete your application and get a response instantly)\" is displayed$")
    public void i_verify_customer_reviews_is_displayed(String element) throws Throwable {
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        commonActions.assertElementWithTextIsVisible(CommonActions.headerFourthBy, element);
    }

    @And("^I click on the \"([^\"]*)\" link$")
    public void i_click_on_the_link(String elementText) throws Throwable {
        commonActions.clickElementWithLinkText(elementText);
    }

    @And("^I verify the \"(PLP|PDP|BRANDS|CheckFit)\" banner color is \"(Green|Yellow|Red)\"$")
    public void i_verify_the_banner_color_is(String page, String color) throws Throwable {
        commonActions.assertBannerColor(page, color);
    }

    @Then("^I verify the message on the \"(PLP|PDP|CheckFit)\" banner contains \"(.*?)\"$")
    public void i_verify_the_message_on_top_banner_contains(String page, String text) throws Throwable {
        commonActions.assertStringInTopBanner(page, text);
    }

    @Then("^I verify Vehicle Details Banner on \"(PLP|PDP|my vehicle|CheckFit)\" contains \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_verify_the_message_on_top_banner_contains(String page, String year, String make, String modal, String trim) throws Throwable {
        ArrayList<String> vehicleValuesList = new ArrayList<>(Arrays.asList(year, make, modal, trim));
        commonActions.assertStringInVehicleDetailsBanner(page, vehicleValuesList);
    }

    @Then("^I verify the \"(PDP|PLP)\" results banner message contains \"(.*?)\"$")
    public void i_verify_the_results_banner_message_contains(String page, String text) throws Throwable {
        commonActions.assertResultsMessageContains(page, text);
    }

    @When("^I browse to the \"(Homepage|PLP|PDP|Shopping Cart|Checkout|Appointment|Order Confirmation)\" page with defaults$")
    public void i_browse_to_page_with_defaults(String validationPage) throws Throwable {
        BrowsePageData browsePageData = new BrowsePageData();

        homePageSteps.i_go_to_the_homepage();
        //Homepage ends here

        if (validationPage.equalsIgnoreCase(ConstantsDtc.CHECKOUT) && Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) ||
                validationPage.equalsIgnoreCase(ConstantsDtc.ORDER_CONFIRMATION) && Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) ||
                validationPage.equalsIgnoreCase(ConstantsDtc.APPOINTMENT) && Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            LOGGER.info("\n \n SKIPPING: " + validationPage + " page test for " + Config.getSiteRegion() + " environment.");
        } else {
            if (!validationPage.equalsIgnoreCase(Constants.HOMEPAGE)) {
                if (validationPage.equalsIgnoreCase(ConstantsDtc.PLP)) {
                    homePageSteps.i_do_a_free_text_search_and_hit_enter(ConstantsDtc.COLOR_BLACK);
                    //PLP ends here
                } else {
                    homePageSteps.i_do_a_free_text_search_and_hit_enter(browsePageData.defaultItemCode);

                    //PDP ends here
                    if (!validationPage.equalsIgnoreCase(ConstantsDtc.PDP)) {
                        commonActions.addToCart(ConstantsDtc.ADD_TO_CART, ConstantsDtc.VIEW_SHOPPING_CART);

                        //Shopping Cart ends here
                        if (!validationPage.equalsIgnoreCase(ConstantsDtc.SHOPPING_CART)) {

                            if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
                                checkoutPageSteps.i_select_the_checkout_option(browsePageData.defaultCheckout);
                                //Checkout ends here

                                if (validationPage.equalsIgnoreCase(ConstantsDtc.ORDER_CONFIRMATION)) {
                                    checkoutPageSteps.
                                            i_enter_shipping_info_and_continue_to_next_page(browsePageData.defaultCustomer);
                                    checkoutPageSteps.i_select_shipping_option(CommonActions.defaultShippingOption,
                                            browsePageData.defaultCustomer);
                                    checkoutPageSteps.
                                            i_enter_payment_info_and_confirm_checkout_summary(browsePageData.defaultCustomer);
                                    checkoutPageSteps.i_place_the_order(browsePageData.defaultCustomer);
                                    orderPageSteps.i_am_brought_to_the_order_confirmation_page();
                                    //Order Confirmation ends here
                                } else {
                                    checkoutPageSteps.i_select_the_checkout_option(browsePageData.checkoutWithAppointment);
                                    //Appointment ends here
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Then("^I close open tabs$")
    public void i_close_tabs() throws Throwable {
        commonActions.closeTabs();
    }

    @And("^I close browser with url containing \"(.*?)\"$")
    public void i_close_browser_with_url(String urlText) throws Throwable {
        commonActions.closeBrowserWithUrl(urlText);
    }

    @And("^I extract the subtotal amount on the \"(cart|checkout)\" page$")
    public void i_extract_subtotal_amount_on_page(String page) throws Throwable {
        if (page.equalsIgnoreCase(Constants.CART)) {
            cartSubtotal = commonActions.extractSubtotalAmount(page);
        } else {
            checkoutSubtotal = commonActions.extractSubtotalAmount(page);
        }
    }

    @Then("^I extract the DOM elements from \"(.*?)\" page and store in \"(.*?)\" excel spreadsheet$")
    public void i_parse_dom_creates_new_excel(String page, String excelName) throws Throwable {
        domObjectsExtractor.parseDomCreateExcel(page, excelName);
    }

    @And("^I extract the \"(order total|sales tax|Instant Savings|Mail In Rebate)\" on the cart page$")
    public void i_extract_amount_on_the_cart_page(String text) throws Throwable {
        if (text.equalsIgnoreCase(Constants.ORDER_TOTAL))
            orderTotal = commonActions.cleanMonetaryStringToDouble(CommonActions.totalAmount.getText());
        if (text.equalsIgnoreCase(ConstantsDtc.SALES_TAX)) {
            salesTax = commonActions.extractTaxOnCart();
            driver.scenarioData.genericData.put(ConstantsDtc.TAX, new DecimalFormat(".##").format(salesTax));
        }
        if (text.equalsIgnoreCase(ConstantsDtc.INSTANT_SAVINGS)) {
            driver.scenarioData.cartInstantPromotionPrice.clear();
            cartPage.getInstantSavingsOnCart();
        }
        if (text.equalsIgnoreCase(ConstantsDtc.MAIL_IN_REBATE)) {
            driver.scenarioData.cartMailInPromotion.clear();
            cartPage.getMailInRebateOnCart();
        }
    }

    @And("^I extract the product price from \"(PDP|PLP|Cart)\" page$")
    public void i_extract_the_product_price_from(String text) throws Throwable {
        String itemPrice = null;

        if (text.equalsIgnoreCase(ConstantsDtc.PDP)) {
            itemPrice = ProductDetailPage.productPrice.getText();
            if (itemPrice == null) {
                itemPrice = ProductDetailPage.mapProductPrice.getText();
            }
            pdpPrice = commonActions.cleanMonetaryStringToDouble(itemPrice);
        } else if (text.equalsIgnoreCase(ConstantsDtc.PLP)) {
            plpProductPrice = productListPage.getProductPriceOnPlp();
        } else {
            cartProductPrice.clear();
            cartProductPrice = cartPage.getProductPriceOnCart();
        }
    }

    @And("^I extract the product name from \"PDP\" page")
    public void i_extract_the_product_name_from_pdp_page() throws Throwable {
        pdpProductName = productDetailPage.getPdpProductName();
    }

    @When("^I extract my store name on the cart page")
    public void i_extract_my_store_name_on_the_cart_page() throws Throwable {
        storeName = CommonActions.cartMyStore.getText().replace("(...)", "");
    }

    @And("^I extract the \"(order total|sales tax)\" on the checkout page for \"([^\"]*)\"$")
    public void i_extract_amount_on_the_checkout_page(String text, String customer) throws Throwable {
        driver.waitForPageToLoad();
        if (text.equalsIgnoreCase(Constants.ORDER_TOTAL))
            orderTotalCheckout = commonActions.cleanMonetaryStringToDouble(CommonActions.orderTotal.getText());
        else {
            if (customer.toLowerCase().contains(ConstantsDtc.CANADA_CUSTOMER.toLowerCase())) {
                salesTaxCheckout = commonActions.cleanMonetaryStringToDouble(CommonActions.taxSubtotal.getText());
            } else if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
                try {
                    salesTaxCheckout = commonActions.cleanMonetaryStringToDouble
                            (driver.getElementWithText(CheckoutPage.checkoutSalesTaxBy, ConstantsDtc.ESTIMATED_TAXES).getText());
                } catch (Exception e) {
                    salesTaxCheckout = commonActions.cleanMonetaryStringToDouble
                            (driver.getElementWithText(CheckoutPage.checkoutSalesTaxBy, ConstantsDtc.TAXES).getText());
                }
            } else {
                salesTaxCheckout = commonActions.cleanMonetaryStringToDouble
                        (driver.getElementWithText(CheckoutPage.checkoutSalesTaxBy, ConstantsDtc.TAXES).getText());
            }
        }
    }

    @Then("^I verify the \"(.*?)\" button is \"(enabled|disabled)\"$")
    public void i_verify_the_button_is_enabled_or_disabled(String buttonText, String enabledOrDisabled) throws Throwable {
        commonActions.assertButtonEnabledDisabled(buttonText, enabledOrDisabled);
    }

    @And("^I extract the mini cart quick total$")
    public void i_extract_the_mini_cart_quick_total() throws Throwable {
        miniCartQuickTotal = commonActions.cleanMonetaryStringToDouble(CartPage.miniCartPrice.getText());
    }

    @And("^I verify that Mini Cart is not displayed on \"(Shipping Details|Shipping Method|Payment|Order Confirmation)\" page$")
    public void i_verify_that_mini_cart_is_not_displayed_on_page(String page) throws Throwable {
        commonActions.assertMiniCartNotDisplayed(page);
    }

    @And("^I verify the result list for \"(Most Recent|Lowest Rated|Overall Rating)\" is sorted in \"(Descending|Ascending)\" order$")
    public void i_verify_result_list_is_sorted_in_order(String value, String sortOrder) throws Throwable {
        commonActions.verifyRatingOrder(value, sortOrder);
    }

    @And("^I verify the \"(Store Reviews|Product Reviews)\" result list for \"(Most Recent|Lowest Rated)\" is sorted in \"(Descending|Ascending)\" order$")
    public void i_verify_reviews_result_list_is_sorted_in_order(String reviewType, String value, String sortOrder) throws Throwable {
        if (reviewType.equalsIgnoreCase(ConstantsDtc.STORE_REVIEWS)) {
            if (value.equalsIgnoreCase(ConstantsDtc.MOST_RECENT)) {
                solicitedReviewPage.assertReviewMostRecentSortOrder(reviewType);
            } else if (value.equalsIgnoreCase(ConstantsDtc.LOWEST_RATED)) {
                commonActions.verifyRatingOrder(value, sortOrder);
            }
        } else {
            solicitedReviewPage.assertReviewMostRecentSortOrder(reviewType);
        }
    }

    @Then("^I verify the \"(.*?)\" displayed for \"(.*?)\" on \"(PLP|PDP|Compare products|Shopping Cart|Order Confirmation|Check Availability|Check Inventory)\" page$")
    public void i_verify_inventory_message_displayed_for_item_on_page(String text, String item, String page) throws Throwable {
        commonActions.assertInventoryMessage(text, item, page);
    }

    @Then("^I verify check nearby stores link is displayed for \"(.*?)\" on \"(PLP|PDP|Shopping Cart)\" page$")
    public void i_verify_check_nearby_stores_display_for_item_on_page(String item, String page) throws Throwable {
        commonActions.assertCheckNearbyStoresLinkDisplay(item, page);
    }

    @Then("^I verify the \"(.*?)\" for set displayed for \"(.*?)\" on \"(PLP|PDP|Shopping Cart|Order Confirmation)\" page$")
    public void i_verify_inventory_message_for_set_displayed_for_item_on_page(String text, String item, String page) throws Throwable {
        commonActions.assertInventoryMessageForSet(text, item, page);
    }

    @Then("^I verify the \"(Inventory Message)\" tooltip for \"(.*?)\" is displayed$")
    public void i_verify_the_tooltip_for_product(String text, String item) throws Throwable {
        String tooltipMessage = ConstantsDtc.INVENTORY_MESSAGE_TOOLTIP;
        commonActions.assertToolTip(text, item, tooltipMessage);
    }

    @Then("^I verify 'Need It Now\\? Call Store' display for \"(.*?)\" on \"(PLP|PDP)\" page$")
    public void i_verify_need_it_now_display_for_product_on_page(String item, String page) throws Throwable {
        if (page.equalsIgnoreCase(ConstantsDtc.PLP)) {
            productListPage.assertNeedItNowDisplayPlp(item);
        } else {
            productDetailPage.assertNeedItNowDisplayPdp(item);
        }
    }

    @Then("^I verify the installation appointment message on \"(Checkout|Order Confirmation)\" page for extended assortment orders$")
    public void i_verify_the_installation_appointment_message_on_page_for_extended_assortment_orders(String page) throws Throwable {
        commonActions.assertInstallationAppointmentMessage(page);
    }

    @Then("^I verify the stock count message on \"(Shopping Cart|Check Availability|Check Inventory)\" page for \"(.*?)\"$")
    public void i_verify_the_stock_count_message_on_page_for_item(String page, String item) throws Throwable {
        commonActions.assertStockCountTextForItem(page, item);
    }

    @And("^I select the \"Check nearby stores\" link for item \"(.*?)\" on \"(PLP|PDP|Shopping Cart)\" page$")
    public void i_click_the_check_nearby_stores_link(String itemCode, String page) throws Throwable {
        commonActions.clickCheckNearbyStores(itemCode, page);
    }

    @And("^I select the first \"Check nearby stores\" link on \"(PLP|PDP|Shopping Cart)\" page$")
    public void i_click_the_first_check_nearby_stores_link(String page) throws Throwable {
        commonActions.clickCheckNearbyStores(page);
    }

    @And("^I verify the \"([^\"]*)\" link is displayed$")
    public void i_verify_the_link_is_displayed_on_page(String linktext) throws Throwable {
        commonActions.verifyLinkIsDisplayedOnPage(linktext);
    }

    @When("^I launch the \"(Appointment|Email|Storybook|orderCancellation)\" URL")
    public void i_launch_the_url(String urlType) throws Throwable {
        commonActions.openUrl(urlType);
    }

    @And("^I verify the \"(Tax Policy|Address Verification|Share Store Details|Share Installer Details)\" modal is displayed$")
    public void i_verify_the_tax_exempt_modal_displayed(String text) throws Throwable {
        commonActions.assertModalPopupIsDisplayed(text);
    }

    @When("^I close the tax exempt modal")
    public void i_close_tax_exempt_modal() throws Throwable {
        commonActions.closeTaxPolicyPopup();
    }

    @And("^I verify \"(Shopping cart|Checkout)\" page is displayed")
    public void i_verify_page_displayed(String page) throws Throwable {
        commonActions.assertPageIsDisplayed(page);
    }

    @And("^I verify Tax Estimation Message is displayed for invalid response from \"(AVS Service)\" on \"(Checkout|Order Confirmation)\" page$")
    public void i_verify_message_for_invalid_service_response_on_page(String service, String page) throws Throwable {
        commonActions.assertMessage(service, page);
    }

    @And("^I verify the \"(Estimated Environmental Fee|Est. Enviro. Fee|Estimated Taxes|Enviro. Fee|Taxes|Environmental Fee)\" label " +
            "displayed on \"(Shopping cart|Checkout|Order Confirmation)\" page$")
    public void i_verify_the_label_displayed_on_page(String text, String page) throws Throwable {
        commonActions.assertLabelDisplayed(text, page);
    }

    @And("^I verify \"(Enviro|Tax|Studded)\" is \"(Charged|Not-Charged)\" for \"(.*?)\" product on \"(Shopping cart|Checkout|Order Confirmation)\" page$")
    public void i_verify_if_charged_or_not_charged(String text, String applicable, String product, String page) throws Throwable {
        cartPage.assertIfApplicable(text, applicable, product, page);
    }

    @And("^I verify \"(Product|Vehicle Details Banner)\" image is displayed on \"(Check Inventory|PLP|PDP)\"$")
    public void i_verify_image_displayed_on_page(String text, String page) throws Throwable {
        commonActions.assertImageDisplayed(text, page);
    }

    @And("^I verify Vehicle Details Banner on \"(PLP|PDP|My Vehicles Modal|Fitment results box)\" contains \"(Edit Vehicle)\"$")
    public void i_verify_the_vehicle_details_banner_contains_text(String page, String text) throws Throwable {
        ArrayList<String> vehicleValuesList = new ArrayList<>(Arrays.asList(text.split(" ")));
        commonActions.assertStringInVehicleDetailsBanner(page, vehicleValuesList);
    }

    @And("^I verify \"(Regular|Staggered)\" Vehicle Details Banner on \"(PLP|PDP|My Vehicles Modal)\" contains \"(OE Tire|OE FRONT Tire|OE REAR Tire|OE Wheel|OE FRONT Wheel|OE REAR Wheel|Non-OE Tire|Non-OE FRONT Wheel|Non-OE REAR Wheel|Non-OE Wheel|Non-OE FRONT Tire|Non-OE REAR Tire|Non-OE set FRONT Tire|Non-OE set REAR Tire|FRONT Tire|FRONT Wheel)\" fitment banner with \"(.*?)\" and \"(.*?)\"$")
    public void i_verify_the_vehicle_details_banner_contains_fitment_banner(String vehicle, String page, String type, String sizeBadge, String text) throws Throwable {
        if (vehicle.equalsIgnoreCase(ConstantsDtc.STAGGERED)) {
            commonActions.assertFitmentBannerInStaggeredVehicleDetailsBanner(page, type, sizeBadge, text);
        } else {
            commonActions.assertFitmentBannerInVehicleDetailsBanner(page, type, sizeBadge, text);
        }
    }

    @And("^I switch to main window$")
    public void i_switch_to_main_window() throws Throwable {
        commonActions.switchToMainBrowserWindow();
    }

    @When("^I launch the baseUrl$")
    public void i_launch_the_baseURL() throws Throwable {
        homePage.goToHome();
    }

    @Then("^I verify treadwell product banner display \"(TOP RECOMMENDATION|RECOMMENDED)\" \"(.*?)\" on \"(PLP|PDP|Compare products)\" page$")
    public void i_verify_treadwell_product_banner_with_ranking_on_page(String text, String ranking, String page) throws Throwable {
        commonActions.assertProductBanner(text, ranking, page);
    }

    @When("^I select \"(View Ride Rating Description|View Tire Life & Cost Rating Description|View Stopping Distance Rating Description)\" on treadwell section on \"(Compare products|PDP)\" page$")
    public void i_select_option_on_treadwell_section_on_page(String option, String page) throws Throwable {
        commonActions.selectOptionOnTreadwellSection(option, page);
    }

    @When("^I take Screenshots of all items in checkout page and save it to \"([^\"]*)\" folder with a file name of \"([^\"]*)\" extracted from examples$")
    public void i_take_screenshots_of_all_items_in_checkout_page_and_save(String childFolderName, String imageName) throws Throwable {
        if (childFolderName.toLowerCase().contains(Constants.GEO)) {
            childFolderName = Config.getStateCode().toLowerCase();
        }
        commonActions.screenshotCoveringAllItemsInCheckOutPage(childFolderName, imageName);
    }

    @When("^I switch to \"(Paypal|Outlook|Newly launched)\" window$")
    public void i_switch_to_window(String window) throws Throwable {
        commonActions.switchToWindow(window);
    }

    @When("^I click on \"(Found it Lower?|Instant Price Match)\" link$")
    public void i_click_on_price_link(String linkText) throws Throwable {
        commonActions.clickLowerPriceLink(linkText);
    }

    @And("^I click the \"(Add Vehicle|Load More Reviews|Shop Tires|Shop Wheels)\" button link$")
    public void i_click_the_button_link(String name) throws Throwable {
        commonActions.clickButtonLink(name);
    }

    @And("^I should see \"([^\"]*)\" details on the \"(PDP|Service Appointment)\" page$")
    public void i_should_see_given_details_on_page(String value, String page) throws Throwable {
        commonActions.verifyVehicleDescription(value, page);
    }

    @Then("^I should see the \"(Vehicle Image|X|Share Icon|Directions|Schedule Appointment)\" on the \"(Wheel Configurator modal window|Store Details|Store Locator)\" page$")
    public void i_verify_the_option_displayed_on_page(String option, String page) throws Throwable {
        commonActions.assertElementOnpage(option, page);
    }

    @And("^I select \"(View Details|Share|X|Directions)\" option on \"(Wheel Configurator modal window|Store Details|Store Locator)\" page$")
    public void i_select_option(String option, String page) throws Throwable {
        commonActions.selectOption(option, page);
    }

    @Then("^I verify \"(email verification link|Copied|Success)\" message displayed$")
    public void i_verify_message_displayed(String text) throws Throwable {
        commonActions.verifyMessageDisplayed(text);
    }

    @And("^I select send to \"(Email|Mobile|Phone)\" \"([^\"]*)\" on \"(Wheel Configurator modal window|Store Details|Installer Locator)\" page$")
    public void i_select_send_to_option_on_page(String option, String value, String page) throws Throwable {
        commonActions.sendTo(option, value, page);
    }

    @When("^I close popup modal$")
    public void i_close_popup_modal() throws Throwable {
        commonActions.closeModalWindow();
    }

    @And("^I should see \"([^\"]*)\" url is launched$")
    public void i_should_see_launched_url(String url) throws Throwable {
        commonActions.confirmCurrentUrl(url, true);
    }

    @Then("^I verify the nearest stores sorted based on miles on \"(Store Details|Store Locator)\" page$")
    public void i_verify_the_nearest_stores_sorted_based_on_miles(String page) throws Throwable {
        commonActions.assertNearestStoresDisplayOrder(page);
    }

    @When("^I update the quantity in \"(PLP|PDP|Catalog|Cart)\" with \"([^\"]*)\"$")
    public void i_update_the_quantity(String page, String quantity) throws Throwable {
        commonActions.updateQuantity(page, quantity);
    }

    @When("^I \"(increase|decrease)\" the quantity in \"(PLP|PDP|Catalog|Cart)\" by \"([^\"]*)\"$")
    public void i_update_the_quantity(String changeDirection, String page, String changeAmount) throws Throwable {
        commonActions.changeQuantity(changeDirection, page, changeAmount);
    }

    @And("^I add item to my cart and \"(View shopping Cart|Continue Shopping|Close Added To Cart popup|Verify Vehicle Fitment|shop for wheels)\"$")
    public void i_add_item_to_my_cart_and_take_action(String action) throws Throwable {
        commonActions.addToCart(ConstantsDtc.ADD_TO_CART, action);
    }

    @And("^I select \"(Add To Cart|Add To Package)\"$")
    public void i_select(String buttonType) throws Throwable {
        commonActions.clickAddToCartOrPackageButton(buttonType);
    }

    @And("^I select the \"([^\"]*)\" link in the breadcrumb$")
    public void i_select_the_link_in_the_breadcrumb_container(String linkText) throws Throwable {
        commonActions.selectBreadcrumbLink(linkText);
    }

    @When("^I \"(expand|collapse)\" \"(Split credit card payment|Someone else will pick up my order|Expedite your Store Experience|Stores located by city|Tire and Wheel Installers located by city|optional tire and wheel size)\"$")
    public void i_expand_collapse_collapsible_toggle_button(String option, String buttonType) throws Throwable {
        commonActions.selectCollapsibleToggleButton(option, buttonType);
    }

    @And("^I validate the \"(Special Order|Unable to process the card|Call Us)\" message$")
    public void i_validate_the_message(String message) throws Throwable {
        commonActions.verifyGlobalMessage(message);
    }

    @When("^I expand expedite your store experience$")
    public void i_expand_expedite_your_store_experience() throws Throwable {
        commonActions.expandExpediteYourStoreExperience();
    }

    @And("^I select \"([^\"]*)\" link$")
    public void i_select_for_link(String text) throws Throwable {
        commonActions.clickLink(text);
    }

    @Then("^I select \"(CHECKOUT WITH APPOINTMENT|APPOINTMENT ONLY)\" button$")
    public void i_select_appointment_only_or_with_checkout(String btnText) throws Throwable {
        commonActions.clickButtonByText(btnText);
        if (driver.isElementDisplayed(CommonActions.chooseStoreButton)) {
            driver.waitForMilliseconds();
            commonActions.closeWelcomePopUp(Constants.CONTINUE);
        }
    }

    @And("^I verify saved store details in \"([^\"]*)\" page$")
    public void i_verify_saved_store_details_in_page(String pageType) throws Throwable {
        commonActions.verifyStoreAddressesOnPage(pageType);
    }

    @When("^I click the discount tire logo")
    public void i_click_the_discount_tire_logo() throws Throwable {
        commonActions.clickSiteLogo();
    }

    @Then("^I verify that selected vehicle \"([^\"]*)\" as the current vehicle in Checkfit$")
    public void i_verify_that_selected_vehicle_as_the_current_vehicle_in_checkfit(String vehicle) throws Throwable {
        commonActions.verifySelectedVehicleOnCheckfit(vehicle);
    }

    @Then("^I verify the footer link launched URLs heading, breadcrumb and url content")
    public void i_verify_the_footer_link_launched_urls_heading_breadcrumb_url_content() throws Throwable {
        footerPage.verifyFooterLinks();
    }

    @When("^I switch to originated web page or window tab")
    public void i_switch_to_originated_window_or_tab() throws Throwable {
        commonActions.switchToOriginatedTabOrPage();
    }

    @And("^I verify button with text \"([^\"]*)\" is displayed$")
    public void i_verify_button_with_is_displayed(String text) throws Throwable {
        commonActions.verifyButtonWithTextIsDisplayed(text);
    }

    @Then("^I verify that the \"(.*?)\" element is not displayed$")
    public void i_verify_that_the_element_is_not_displayed(String element) {
        commonActions.assertElementNotDisplayed(element);
    }

    @And("^I set fitment type to \"(staggered|non-staggered)\"$")
    public void i_set_fitment_type_to_staggered_or_non_staggered(String fitmentType) throws Throwable {
        if (fitmentType.equalsIgnoreCase(ConstantsDtc.STAGGERED))
            driver.scenarioData.setStaggeredProduct(true);
        else
            driver.scenarioData.setStaggeredProduct(false);
    }

    @Then("^I should see Cart Popup has \"([^\"]*)\" option displayed$")
    public void i_verify_CartPopUp(String cartOption) throws Throwable {
        commonActions.assertCartPopUpOptions(cartOption);
    }

    @Then("^I verify suggested selling carousel inner-tile for \"(PDP|PLP|Homepage|Add to Cart)\" is \"(displayed|not displayed)\" with title \"(.*?)\" with no more than five products and no duplicate products in the carousel$")
    public void i_verify_suggested_selling_carsousel_inner_title_for_page_is_displayed_with_title(String page, String displayed, String title) throws Throwable {
        commonActions.assertCarouselTitleProductListSizeAndNoDuplicates(page, displayed, title);
    }

    @Then("^I verify that \"(.*?)\" \"(.*?)\" is present at position \"(.*?)\" of the carousel$")
    public void i_verify_that_product_is_present_at_position_of_the_carousel(String productName, String productType, int productPosition) throws Throwable {
        commonActions.assertProductPosition(productName, productType, productPosition);
    }

    @And("^I verify that tooltip icon is present in the carousel for tire product$")
    public void i_verify_that_tooltip_icon_is_present_for() throws Throwable {
        commonActions.assertToolTipIcon();
    }

    @Then("I verify that the suggested selling carousel does not display the selected product on \"(PDP|Add to cart)\" page$")
    public void i_verify_that_the_suggested_selling_carousel_does_not_display_the_selected_product_on_page(String page) throws Throwable {
        commonActions.assertSuggestedSellingProductsNotDisplayed(page);
    }

    @Then("^I verify the product in cart is not displayed in the \"(Homepage|Add to Cart|PDP)\" carousel$")
    public void i_verify_the_product_in_cart_is_not_displayed_in_the_homepage_carousel(String page) throws Throwable {
        commonActions.assertProductInCartNotDisplayedInCarousel(page);
    }

    @When("^I select image with alt text of \"([^\"]*)\"$")
    public void i_select_the_image_with_alt_text(String altText) throws Throwable {
        commonActions.selectImageWithAltText(altText);
    }
    @Then("^I verify that the products in the carousel are of \"(.*?)\"$")
    public void i_verify_that_the_product_in_the_carousel_of_size(String size) throws Throwable {
        commonActions.assertCarouselProductsSize(size);
    }
}