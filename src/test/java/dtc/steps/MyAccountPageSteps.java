package dtc.steps;

import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.pages.CommonActions;
import dtc.pages.MyAccountPage;
import dtc.pages.OrderPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utilities.CommonUtils;
import utilities.Driver;
import org.openqa.selenium.WebDriver;
import webservices.pages.WebServices;

/**
 * Created by ankitarora on 02/19/18.
 */
public class MyAccountPageSteps {

    private MyAccountPage myAccountPage;
    private Customer customer;
    private CommonActions commonActions;
    private WebDriver webDriver;
    private Driver driver;
    private Scenario scenario;

    private static final String SAVED_PASSWORD = "saved password";

    public MyAccountPageSteps(Driver driver) {
        myAccountPage = new MyAccountPage(driver);
        customer = new Customer();
        commonActions = new CommonActions(driver);
        webDriver = driver.getDriver();
        this.driver = driver;
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @Then("^I verify my account sign-in modal is displayed$")
    public void i_verify_my_account_sign_in_modal_is_displayed() throws Throwable {
        commonActions.assertWebElementDisplayed(myAccountPage.returnWebElement(CommonActions.EMAIL),CommonActions.EMAIL);
        commonActions.assertWebElementDisplayed(myAccountPage.returnWebElement(ConstantsDtc.PASSWORD),ConstantsDtc.PASSWORD);
    }

    @Then("^I should see \"(.*?)\" field is displayed$")
    public void i_should_see_field_is_displayed(String elementName) throws Throwable {
        myAccountPage.assertFieldDisplayed(elementName);
    }

    @And("^I verify that \"(Sign-In|Reset Password|CREATE AN ACCOUNT|Update|DONE EDITING)\" is disabled by default$")
    public void i_verify_my_account_button_state_disabled(String buttonName) throws Throwable {
        myAccountPage.verifyMyAccountButtonState(buttonName, Constants.DISABLED);
    }

    @And("^I should see \"(Sign-In|RESET PASSWORD|CREATE AN ACCOUNT|DONE EDITING|Update)\" is enabled$")
    public void i_should_see_my_account_button_state_enabled(String buttonName) throws Throwable {
        myAccountPage.verifyMyAccountButtonState(buttonName, Constants.ENABLED);
    }

    @When("^I enter my account email address on \"(homepage|checkout)\" page$")
    public void i_enter_new_email_in_email_address_field(String page) throws Throwable {
        myAccountPage.enterEmailIdOnPage(page);
    }

    @When("^I enter \"(.*?)\" in email address field$")
    public void i_enter_email_in_email_address_field(String value) throws Throwable {
        commonActions.enterTextIntoField(MyAccountPage.myAccountEmailAddress, value);
    }

    @When("^I enter \"(.*?)\" in password field$")
    public void i_enter_password_in_password_field(String value) throws Throwable {
        if (value.equalsIgnoreCase(SAVED_PASSWORD)) {
            value = driver.scenarioData.genericData.get(SAVED_PASSWORD);
        }
        commonActions.clearAndPopulateEditField(MyAccountPage.myAccountPassword, value);
        driver.waitSeconds(Constants.TWO);
    }

    @And("^I sign in with \"(.*?)\" and \"(.*?)\" if login popup modal is displayed$")
    public void i_sign_in_if_login_popup_modal_is_displayed(String email, String password) throws Throwable {
        if (commonActions.popupModalDisplayed(ConstantsDtc.SIGN_INTO_YOUR_ACCOUNT)) {
            i_enter_email_in_email_address_field(email);
            i_enter_password_in_password_field(password);
            commonActions.clickButtonByText(ConstantsDtc.SIGN_IN, Constants.SIXTY);
            driver.waitForPageToLoad();
        }
    }

    @Then("^I verify keep me signed-in option is displayed$")
    public void i_verify_keep_me_signed_in_option_is_displayed() throws Throwable {
        myAccountPage.assertKeepMeSignedInIsDisplayed();
    }

    @And("^I select keep me signed-in option$")
    public void i_select_keep_me_signed_in_option() throws Throwable {
        myAccountPage.setKeepMeSignedInCheckbox();
    }

    @And("^I deselect keep me signed-in option$")
    public void i_deselect_keep_me_signed_in_option() throws Throwable {
        myAccountPage.unSetKeepMeSignedInCheckbox();
    }

    @And("^I select forgot password option$")
    public void i_select_forgot_password_option() throws Throwable {
        myAccountPage.clickForgotPasswordLink();
    }

    @And("^I select reset password option$")
    public void i_select_reset_password_option() throws Throwable {
        myAccountPage.clickResetPasswordButton();
    }

    @When("^I enter \"(.*?)\" in reset password email address field$")
    public void i_enter_email_in_reset_password_email_address_field(String value) throws Throwable {
        driver.scenarioData.genericData.put(WebServices.MY_ACCOUNT_EMAIL_ID, value);
        commonActions.enterTextIntoField(MyAccountPage.myAccountResetPasswordEmail, value);
    }

    @Then("^I should see my \"(.*?)\" name displayed on homepage header$")
    public void i_should_see_name_is_displayed(String firstNameType) throws Throwable {
        myAccountPage.assertSignedInUserFirstNameIsDisplayed(firstNameType);
    }

    @And("^I select sign-up now$")
    public void i_select_sign_up_option() throws Throwable {
        myAccountPage.clickSignUpNowLink();
    }

    @Then("^I verify email validation error message should display for all invalid email addresses$")
    public void i_verify_email_validation_error_message_display_on_invalid_email_addrresses() throws Throwable {
        myAccountPage.assertEmailFieldErrorMessageValidation();
    }

    @And("^I generate and set random email address for user \"(.*?)\"$")
    public void i_generate_set_random_email_for_user(String user) throws Throwable {
        myAccountPage.generateAndSetFakeRandomEmailForUser(user);
    }

    @Then("^I verify email sent confirmation message displayed on \"(account confirmation modal|checkout page)\"$")
    public void i_verify_email_sent_confirmation_message_displayed_on_account_confirmation_modal(String page) throws Throwable {
        if (page.contains(MyAccountPage.ACCOUNT_CONFIRMATION_MODAL))
            myAccountPage.assertAccountModalHasMessagePresent(CommonActions.successMessageBy, ConstantsDtc.EMAIL_SENT_MESSAGE);
        else if (page.contains(ConstantsDtc.CHECKOUT)) {
            myAccountPage.assertAccountModalHasMessagePresent(MyAccountPage.myAccountErrorValidationBy, MyAccountPage.EMAIL_VERIFICATION_LINK_MESSAGE);
        }
    }

    @Then("^I verify my email address displayed on account confirmation modal$")
    public void i_verify_email_address_displayed_on_account_confirmation_modal() throws Throwable {
        myAccountPage.assertAccountModalHasMessagePresent(CommonActions.successMessageBy,
                CommonActions.generatedRandomEmail);
    }

    @Then("^I verify my email authentication link message displayed on account confirmation modal$")
    public void i_verify_email_authentication_link_message_displayed_on_account_confirmation_modal() throws Throwable {
        myAccountPage.assertAuthenticationEmailLinkMessagePresent();
    }

    @And("^I select continue shopping option$")
    public void i_select_continue_shopping_option() throws Throwable {
        myAccountPage.clickContinueShoppingButton();
    }

    @Then("^I verify the password requirement validation for passed progress steps$")
    public void i_verify_the_password_requirement_validation_for_passed_progress_steps() throws Throwable {
        myAccountPage.assertPasswordProgressStepsValidation();
    }

    @Then("^I verify Reset Your Password instruction displayed on forgot password modal$")
    public void i_verify_reset_your_password_instruction_displayed_on_forgot_password_modal() throws Throwable {
        myAccountPage.assertResetYourPasswordInstructionDisplayed();
    }

    @Then("^I verify Reset Your Password email validation error message should display for all invalid email addresses$")
    public void i_verify_reset_password_email_validation_error_message_display_on_invalid_email_addrresses() throws Throwable {
        myAccountPage.assertResetYourPasswordEmailFieldErrorMessageValidation();
    }

    @Then("^I verify Reset Your Password instruction displayed after reset password$")
    public void i_verify_reset_your_password_instruction_displayed_after_reset_password() throws Throwable {
        myAccountPage.assertPasswordInstructionDisplayedAfterResetPassword();
    }

    @Then("^I verify email account not authenticated message displayed$")
    public void i_verify_email_account_not_authenticated_message_displayed() throws Throwable {
        myAccountPage.assertAccountModalHasMessagePresent(CommonActions.formGroupMessageErrorElementBy,
                ConstantsDtc.EMAIL_ACCOUNT_NOT_AUTHENTICATED_MESSAGE);
    }

    @Then("^I verify invalid credentials error validation message is \"(displayed|not displayed)\"$")
    public void i_should_see_invalid_credentials_error_validation_message(String isDisplayed) throws Throwable {
        myAccountPage.verifyInvalidCredentials(isDisplayed);
    }

    @Then("^I should see my value populated in \"(Email)\" field$")
    public void i_should_see_my_value_populated_in_field(String inputField) throws Throwable {
        myAccountPage.verifyMyAccountInputFieldValue(inputField.toLowerCase(), customer.getCustomer(
                Customer.CustomerType.MY_ACCOUNT_USER_A.toString()).email);
    }

    @And("^I close My Account modal popup$")
    public void i_close_my_account_modal_popup() throws Throwable {
        myAccountPage.clickMyAccountPopup();
    }

    @Then("^I verify successfully updated your e-mail address message displayed$")
    public void i_verify_successfully_updated_your_email_address_message_displayed_on_account_confirmation_modal() throws Throwable {
        commonActions.assertPageHeader(ConstantsDtc.UPDATED_EMAIL_SENT_MESSAGE);
    }

    @Then("^I verify my updated email authentication link message displayed$")
    public void i_verify_my_updated_email_authentication_link_message_displayed_on_account_confirmation_modal() throws Throwable {
        myAccountPage.assertAuthenticationUpdatedEmailLinkMessagePresent();
    }

    @Then("^I should see reset your password message is displayed$")
    public void i_should_see_reset_your_password_message_is_displayed() throws Throwable {
        myAccountPage.assertAccountModalHasMessagePresent(CommonActions.dtModalTitleBy,
                ConstantsDtc.RESET_YOUR_PASSWORD);
    }

    @Then("^I verify reset password modal is displayed$")
    public void i_verify_reset_password_modal_is_displayed() throws Throwable {
        i_should_see_reset_your_password_message_is_displayed();
        commonActions.assertWebElementDisplayed(myAccountPage.returnWebElement(MyAccountPage.RESET_PASSWORD),MyAccountPage.RESET_PASSWORD);
    }

    @When("^I enter \"(.*?)\" in reset password field$")
    public void i_enter_password_in_reset_password_field(String value) throws Throwable {
        if(value.equalsIgnoreCase(Constants.RANDOM)) {
            value = "Discount@" + CommonUtils.getRandomNumber(0, Constants.ONE_THOUSAND);
            driver.scenarioData.genericData.put(SAVED_PASSWORD,value);
        }
        commonActions.enterTextIntoField(MyAccountPage.myAccountResetPassword, value);
    }

    @Then("^I verify \"(SIGN-IN|CONTACT INFO)\" is displayed$")
    public void i_verify_is_displayed(String headerText) throws Throwable {
        myAccountPage.assertProfileHeader(headerText);
    }

    @And("^I click on \"(alternate contact|primary contact|E-mail Address|password)\" edit link$")
    public void i_click_on_edit_link (String customerType) throws Throwable {
        myAccountPage.clickEditLink(customerType);
    }

    @Then("^I add contact info as \"([^\"]*)\" for \"(primary|alternate)\" customer$")
    public void i_add_contact_info_as(String customerType, String Customer) throws Throwable {
        myAccountPage.populatePrimaryAndAlternateContact(customerType,Customer);
    }

    @Then("^I verify Edit and Delete links are displayed for \"(Primary|alternate)\" address$")
    public void i_verify_links_are_displayed(String contactType) throws Throwable {
        myAccountPage.assertEditDeleteLink(contactType);
    }

    @Then("^I verify add contact information link is displayed on the page$")
    public void i_verify_add_contact_information_link() throws Throwable {
        myAccountPage.assertAddContactInformationLink();
    }

    @Then("^I verify contact selection drop down displays \"(Primary Information|Alternate Information)\"$")
    public void i_verify_contact_selection_drop_down_displays_contact_information(String contactType) throws Throwable {
        myAccountPage.assertContactSelectionDropDown(contactType);
    }

    @When("^I select \"(Primary Information|Alternate Information)\" from drop down$")
    public void i_select_secondary_contact_information_from_drop_down(String contactType) throws Throwable {
        myAccountPage.selectContactType(contactType);
    }

    @Then("^I verify \"([^\"]*)\" is displayed on checkout page$")
    public void i_verify_is_displayed_on_checkout_page(String text) throws Throwable {
        myAccountPage.assertCreateAccountLabelDisplayed(text);
    }

    @Then("^I select create account checkbox$")
    public void i_select_create_account_checkbox() throws Throwable {
        myAccountPage.selectCreateAnAccountCheckBox();
    }

    @When("^I select \"(Profile|My Vehicles|Orders|Appointments)\" tab$")
    public void i_select_tab(String inputSelection) throws Throwable {
        myAccountPage.clickTabOnMyAccountPage(inputSelection);
    }

    @Then("^I am brought to the \"([^\"]*)\" tab$")
    public void i_am_brought_to_tab(String selectedTab) throws Throwable {
        myAccountPage.verifyActiveTab(selectedTab);
    }

    @When("^I enter my account phone number$")
    public void I_enter_my_account_phone_number() {
        while (!CommonActions.phone.getAttribute(Constants.VALUE).isEmpty()) {
            CommonActions.phone.sendKeys(Keys.BACK_SPACE);
        }
        String phoneNumber = customer.getCustomer(Customer.CustomerType.MY_ACCOUNT_NEW_CUSTOMER.toString()).phone;
        driver.scenarioData.genericData.put(Constants.CUSTOMER_PHONE_NUMBER, phoneNumber);
        commonActions.clearAndPopulateEditField(CommonActions.phone, phoneNumber);
    }

    @Then("^I verify contact info for both primary and alternate user$")
    public void i_verify_contact_info_for_both_primary_and_secondary_user() {
        myAccountPage.assertPrimaryAndAlternateAddress();
    }

    @Then("^I verify create account modal is displayed$")
    public void i_verify_create_account_modal_is_displayed() {
        commonActions.assertWebElementDisplayed(myAccountPage.returnWebElement(ConstantsDtc.CREATE_ACCOUNT),ConstantsDtc.CREATE_ACCOUNT);
    }

    @When("^I enter \"(invalid|incorrect)\" address by customer type \"(.*?)\"$")
    public void i_enter_address(String addressType, String customerType) {
        if (addressType.equalsIgnoreCase(ConstantsDtc.INCORRECT)) {
            commonActions.clearEditField(CommonActions.address1);
            commonActions.enterTextIntoField(CommonActions.address1,
                    customer.getCustomer(customerType).incorrectAddress1);
        } else {
            commonActions.clearEditField(CommonActions.address1);
            commonActions.enterTextIntoField(CommonActions.address1, MyAccountPage.INVALID_ADDRESS);
        }
        commonActions.clickBrowserBody();
    }

    @When("^I populate all the required fields for \"(.*?)\"$")  
    public void i_populate_required_fields_for (String customerType) {
        myAccountPage.populateDataForCustomer(customerType);
        Customer customerInfo = customer.getCustomer(customerType);
        scenario.write(customerInfo.getCustomerDataString(customerInfo));

    }

    @When("^I select create an account using my information$")
    public void i_select_create_an_account_using_my_information (){
        myAccountPage.setKeepMeSignedInCheckbox();
    }

    @And("^I verify \"(existing email error message)\" is displayed$")
    public void i_verify_existing_email_address_error_message(String buttonText) throws Throwable {
        commonActions.assertElementWithTextIsVisible(MyAccountPage.myAccountErrorValidationBy,MyAccountPage.EXISTING_EMAIL_ERROR_MESSAGE);
    }

    @Then("^I verify create an account using my information checkbox is selected$")
    public void i_verify_create_account_using_my_information_checkbox_is_selected() throws Throwable {
        commonActions.verifyCheckboxSelected(webDriver.findElement(CommonActions.checkboxInputBy),
                MyAccountPage.CREATE_AN_ACCOUNT_USING_MY_INFORMATION);
    }

    @When("^I verify \"(.*?)\" field cannot be empty error message$")
    public void i_verify_field_cannot_be_empty_error_message (String field) throws Throwable {
        myAccountPage.assertFieldEmptyErrorMessage(field);
    }

    @When("^I verify \"(.*?)\" error message$")
    public void i_verify_create_account_error_message (String field) throws Throwable {
        myAccountPage.assertCreateAccountFieldErrorMessage(field);
    }

    @When("^I enter a zipcode of \"(.*?)\" in create account modal$")
    public void i_enter_zipcode_in_create_account_modal (String zip) throws Throwable {
         commonActions.clearEditField(CommonActions.orderSummaryZipCode);
         CommonActions.orderSummaryZipCode.sendKeys(zip);
    }

    @When("^I delete \"(.*?)\" saved vehicles$")
    public void i_delete_all_saved_vehicles(String numberOfVehicles) throws Throwable {
        myAccountPage.deleteAllSavedVehicles(numberOfVehicles);
    }

    @When("^I save all vehicles in the recent search$")
    public void i_save_all_vehicles_in_the_recent_search() throws Throwable {
        myAccountPage.saveAllRecentSearchVehicles();
    }

    @When("^I verify order number on order history summary page on \"(Orders|Appointments)\" tab$")
    public void i_verify_order_numbers_with_order_history_summary_page(String tab) throws Throwable {
        myAccountPage.assertOrderNumberOnMyAccountPage(tab, driver.scenarioData.getCurrentOrderNumber());
    }

    @When("^I click on order view details link in order history summary page$")
    public void i_click_on_order_view_details_link_in_order_history_summary_page() throws Throwable {
        myAccountPage.viewDetailsClickByRow(driver.scenarioData.getCurrentOrderNumber());
    }

    @When("^I update my account first name to \"(.*?)\"$")
    public void i_update_my_account_first_name_to(String nameStatusType) {
        myAccountPage.updateFirstName(nameStatusType);
    }

    @When("^I enter my account zipcode by customer type \"([^\"]*)\"$")
    public void i_enter_my_account_zipcode_by_customer_type(String customerType) {
        while (CommonActions.orderSummaryZipCode.getText().equals(" ")) {
            CommonActions.orderSummaryZipCode.sendKeys(Keys.BACK_SPACE);
        }
        CommonActions.orderSummaryZipCode.sendKeys(customer.getCustomer(customerType).zip);
        driver.waitForPageToLoad();
    }

    @Then("^I verify there are sufficient number of orders on order history tab")
    public void i_verify_there_are_sufficient_number_of_orders_on_order_history_tab() throws Throwable {
        myAccountPage.assertTotalNumberOfOrders();
    }

    @When("^I navigate to My Account email authenticate url$")
    public void i_navigate_to_my_account_email_authenticate_url() throws Throwable {
        myAccountPage.navigateMyAccountEmailUrl();
    }

    @Then("^I navigate to My Account password reset url$")
    public void i_navigate_to_my_account_password_reset_url() throws Throwable {
        myAccountPage.navigateMyAccountPasswordResetUrl();
    }

    @When("^I enter \"(.*?)\" into \"(.*?)\" field$")
    public void i_enter_into_field(String text, String fieldName) throws Throwable {
        WebElement field = myAccountPage.returnWebElement(fieldName);
        commonActions.clearAndPopulateEditField(field, text);
        if (fieldName.equalsIgnoreCase(Constants.EMAIL))
            driver.scenarioData.genericData.put(WebServices.MY_ACCOUNT_EMAIL_ID, text);
    }

    @When("^I select \"(.*?)\" for appointment with order number$")
    public void i_select_option_for_appointment_with_order_number (String option) throws Throwable {
        String appointmentOrderNumber = driver.scenarioData.getCurrentOrderNumber();
        myAccountPage.selectOptionForOrder(option,appointmentOrderNumber);
    }

    @When("^I select the reason \"(.*?)\" for cancellation$")
    public void i_select_the_reason_for_cancellation (String reason) throws Throwable {
        myAccountPage.selectReasonForCancellation(reason);
    }

    @Then("^I verify the sorting options are displayed on \"(order|appointment)\" summary page$")
    public void i_verify_the_sorting_options_is_displayed_on_summary_page(String page) throws Throwable {
        myAccountPage.verifySortingIsDisplayed(page);
    }

    @Then("^I verify the default sorting order is \"(Descending|Ascending)\"$")
    public void i_verify_the_default_sorting_is_specified_order(String order) throws Throwable {
        myAccountPage.verifyOrderOfSorting(order);
    }

    @Then("^I verify that orders are sorted in \"(Descending|Ascending)\" order$")
    public void i_verify_that_orders_are_sorted_in_order (String order) throws Throwable {
        myAccountPage.assertSortedOrder(order);
    }
}