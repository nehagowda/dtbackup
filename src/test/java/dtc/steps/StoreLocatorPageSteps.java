package dtc.steps;

import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import dtc.data.ConstantsDtc;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.pages.HomePage;
import dtc.pages.MobileHeaderPage;
import dtc.pages.StoreLocatorPage;
import dtc.pages.CommonActions;
import dtc.pages.StoreChangeModalPage;
import org.openqa.selenium.WebDriver;
import dtc.pages.HeaderPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/24/2016.
 */
public class StoreLocatorPageSteps {

    private HomePage homePage;
    private StoreLocatorPage storeLocatorPage;
    private MobileHeaderPage mobileHeaderPage;
    private CommonActions commonActions;
    private StoreChangeModalPage storeChangeModalPage;
    private HeaderPage headerPage;
    private Scenario scenario;
    private WebDriver webDriver;
    private Driver driver;

    public StoreLocatorPageSteps(Driver driver) {
        homePage = new HomePage(driver);
        mobileHeaderPage = new MobileHeaderPage(driver);
        storeLocatorPage = new StoreLocatorPage(driver);
        commonActions = new CommonActions(driver);
        storeChangeModalPage = new StoreChangeModalPage(driver);
        headerPage = new HeaderPage(driver);
        webDriver = driver.getDriver();
        this.driver = driver;
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @When("^I search for store within \"(10|25|50|75|100)\" miles of \"([^\"]*)\"$")
    public void i_search_for_store_within_miles_of(String searchRange, String cityStateOrZip) throws Throwable {
        String siteRegion = Config.getSiteRegion().toLowerCase();
        if (siteRegion.equalsIgnoreCase(ConstantsDtc.DT) || siteRegion.equalsIgnoreCase(ConstantsDtc.AT)) {
            if (Config.isMobile()) {
                mobileHeaderPage.findAStore.click();
            } else {
                homePage.openChangeStore();
            }
        } else if (siteRegion.contains(ConstantsDtc.DTD)) {
            headerPage.clickNavigationOption(ConstantsDtc.INSTALLERS);
        }
        storeLocatorPage.searchForStore(searchRange, cityStateOrZip);
    }

    @And("^I select make \"(.*?)\" my store$")
    public void i_select_make_my_store(String store) throws Throwable {
        if (!storeLocatorPage.currentStoreSelected && Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            storeLocatorPage.currentStoreSelected = true;
        } else {
            storeLocatorPage.clickMakeThisMyStoreButtonFromStoreFinder(store);
        }
    }

    @Then("^I should see my store as current store$")
    public void i_see_my_store_as_current_store() throws Throwable {
        storeLocatorPage.confirmCurrentStoreTextPresent();
    }

    @When("^I change to the default store through url$")
    public void i_navigate_and_change_to_the_default_store_through_url() throws Throwable {
        storeLocatorPage.navigateToDefaultStoreThroughUrl();
        homePage.handleChooseStorePopup();
        storeLocatorPage.clickMakeThisMyStoreButtonFromStoreDetails(true);
    }

    @When("^I change to the integrated test store through url$")
    public void i_navigate_and_change_to_the_integrated_store_through_url() throws Throwable {
        storeLocatorPage.navigateToIntegratedStoreThroughUrl();
        storeLocatorPage.clickMakeThisMyStoreButtonFromStoreDetails(true);
    }

    @When("^I change to the Scottsdale store through url$")
    public void i_change_to_scottsdale_store_through_url() throws Throwable {
        storeLocatorPage.navigateToScottsdaleStoreThroughUrl();
        storeLocatorPage.clickMakeThisMyStoreButtonFromStoreDetails(true);
    }

    @When("^I change to the store with url \"(.*?)\"$")
    public void i_navigate_to_store_path_and_change_my_store(String path) throws Throwable {
        storeLocatorPage.navigateToMyStoreThroughUrlPath(path);
        storeLocatorPage.clickMakeThisMyStoreButtonFromStoreDetails(true);
    }

    @When("^I select \"(Send to phone|Appointment|Make This My Store|Directions|Share|Read reviews)\" for store #\"([1-9][0-9]*)\" in the location results$")
    public void i_select_action_for_store_on_store_locator_results(String action, String storeResultNumber) throws Throwable {
        storeLocatorPage.selectActionForStoreLocationResultItem(action, storeResultNumber);
    }

    @Then("^I verify the Current Store text color is Blue$")
    public void i_verify_the_current_store_text_color_is_blue() throws Throwable {
        storeLocatorPage.assertCurrentStoreTextColor();
    }

    @Then("^I should see this partner installer \"(.*?)\" is present in the displayed stores list$")
    public void i_should_see_partner_installer_is_displayed(String siteToValidate) throws Throwable {
        storeLocatorPage.confirmStoreFoundForSite(siteToValidate);
    }

    @Then("^I verify schedule appointment option is not available to users on DTD store locator page$")
    public void i_verify_schedule_appointment_option_is_not_available() throws Throwable {
        storeLocatorPage.assertScheduleAppointmentOptionNotAvailableOnDTDStoreLocatorPage();
    }

    @Then("^I verify the \"HOURS OF OPERATION\" in the store location results$")
    public void i_verify_hours_of_operation_for_store_search_results() throws Throwable {
        storeLocatorPage.verifyHoursOfOperationForStoreSearchResults();
    }

    @When("^I select \"(.*?)\" for store details$")
    public void i_select_store_result_item_for_store_details(String storeLinkText) throws Throwable {
        storeLocatorPage.selectStoreForStoreDetails(storeLinkText);
    }

    @When("^I search for stores within \"(10|25|50|75|100)\" miles of non default zip code$")
    public void i_search_for_stores_within_miles_of_non_default_zip_code(String searchRange) throws Throwable {
        if (Config.isMobile()) {
            mobileHeaderPage.findAStore.click();
        } else {
            homePage.openChangeStore();
        }
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            storeLocatorPage.searchForStore(searchRange, ConstantsDtc.NON_DEFAULT_STORE_ZIP_CODE_DT);
        } else {
            storeLocatorPage.searchForStore(searchRange, ConstantsDtc.NON_DEFAULT_STORE_ZIP_CODE_AT);
        }
    }

    @And("^I select non default store as my store$")
    public void i_select_non_default_store_as_my_store() throws Throwable {
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            i_select_make_my_store(ConstantsDtc.CHANGE_STORE_DT);
        } else {
            storeLocatorPage.selectActionForStoreLocationResultItem(ConstantsDtc.MAKE_THIS_MY_STORE, ConstantsDtc.STORE_DEFAULT_RESULT);
        }
    }

    @And("^I click 'Make This My Store' button on Store Details Page$")
    public void i_click_make_this_my_store_button_on_store_details_page() {
        storeLocatorPage.clickMakeThisMyStoreButtonFromStoreDetails(false);
    }

    @Then("^I verify the Stores located by \"(State|City)\" list is displayed$")
    public void i_verify_stores_located_by_state_list_displayed(String text) throws Throwable {
        storeLocatorPage.assertStoreLocatorCityStateListIsDisplayed(text);
    }

    @Then("^I verify Shop Discount Tire Direct message displays \"(searched result|no search result|searched miles expanded)\"$")
    public void i_verify_shop_discount_tire_direct_message_displays(String searchedResult) throws Throwable {
        storeLocatorPage.assertShopDtdMessageDisplayed(searchedResult);
    }

    @When("^I select shop discount tire direct$")
    public void i_select_shop_discount_tire_directs() throws Throwable {
        storeLocatorPage.selectShopDtd();
    }

    @Then("^I verify discount Tire Stores displayed for hundred plus miles display logic$")
    public void i_verify_discount_tire_stores_for_hundred_plus_miles_display_logic() throws Throwable {
        storeLocatorPage.assertStoreForHundredPlusMilesDisplayLogic();
    }

    @Then("^I verify site is \"(America's Tire|Discount Tire)\"$")
    public void i_verify_site_is_redirected_to(String siteToValidate) throws Throwable {
        homePage.verifySiteLogo(siteToValidate);
    }

    @When("^I change to EA default store through url$")
    public void i_navigate_and_change_to_the_extended_assortment_default_store_through_url() throws Throwable {
        driver.getUrl(Config.getBaseUrl().concat(Config.getEADefaultStoreCodeURL()));
        homePage.handleChooseStorePopup();
        storeLocatorPage.clickMakeThisMyStoreButtonFromStoreDetails(true);
    }
}