package dtc.data;

import common.Config;
import common.Constants;
import utilities.CommonUtils;

import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 * Created by collinreed on 4/15/19.
 */
public class Product {
    private final Logger LOGGER = Logger.getLogger(Product.class.getName());
    public String productName;
    public String productCode;

    public enum ProductType {
        PRODUCT_DEFAULT_QA_DT_1,
        PRODUCT_DEFAULT_QA_AT_1,
        PRODUCT_DEFAULT_QA_DTD_1,
        PRODUCT_DEFAULT_STG_DT_1,
        PRODUCT_DEFAULT_STG_AT_1,
        PRODUCT_DEFAULT_STG_DTD_1;

        /**
         * Get the product type based on the specified product name. If "DEFAULT" is in the product name,
         * then use -DdataSet and -DsiteRegion to determine the product type
         *
         * @param productName                  The specified product name
         * @return                          ProductType enum value
         * @throws IllegalArgumentException Illegal Argument Exception
         */
        private static ProductType getProductType(String productName) throws IllegalArgumentException {
            String env = Config.getDataSet();
            String region = Config.getSiteRegion();

            // Insert env (i.e. QA or STG) and region (i.e. DT or AT) into the product name.
            // Note: This change only happens if "DEFAULT" is in the specified prodName.
            // Therefore, "DEFAULT" is required in prodName where the product is dependent on environment and region.
            // Alternatively, by leaving "DEFAULT" out of prodName, the option is available to have product types
            // in this class that are not dependent on environment and region.
            productName = productName.replace(ConstantsDtc.DEFAULT.toUpperCase(),
                    ConstantsDtc.DEFAULT.toUpperCase() + "_" + env.toUpperCase() + "_" + region.toUpperCase());

            for (ProductType productType : values()) {
                if (productType.toString().equalsIgnoreCase(productName)) {
                    return productType;
                }
            }
            throw productNotFound(productName);
        }

        private static IllegalArgumentException productNotFound(String outcome) {
            return new IllegalArgumentException(("Invalid product [" + outcome + "]"));
        }
    }

    public Product() {
    }

    /**
     * Get the product details based on the specified product name
     *
     * @return Product instance
     */
    public Product getProduct(String prodName) {
        LOGGER.info("getProduct started");
        ProductType productType = ProductType.getProductType(prodName);
        Product product = new Product();

        switch (productType) {
            case PRODUCT_DEFAULT_QA_DT_1:
            case PRODUCT_DEFAULT_STG_DT_1:
            case PRODUCT_DEFAULT_QA_DTD_1:
            case PRODUCT_DEFAULT_STG_DTD_1:
                product.productName = "Silver Edition III";
                product.productCode = "29935";
                break;
            case PRODUCT_DEFAULT_QA_AT_1:
            case PRODUCT_DEFAULT_STG_AT_1:
                product.productName = "Premier A/S";
                product.productCode = "19609";
                break;
        }
        LOGGER.info("getProduct completed");
        return product;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty(Constants.LINE_SEPARATOR_SYSTEM_PROPERTY);

        //determine fields declared in this class
        Field[] fields = this.getClass().getDeclaredFields();

        //Field names paired with their values
        for (Field field : fields ) {
            try {
                if (CommonUtils.containsIgnoreCase(field.getName(), Constants.LOGGER))
                    continue;
                result.append(field.getName());
                result.append(": ");
                result.append(field.get(this));
            } catch (IllegalAccessException ex) {
                //continue
            }
            result.append(newLine);
        }
        return result.toString();
    }
}
