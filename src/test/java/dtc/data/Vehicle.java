package dtc.data;

import common.Constants;
import utilities.CommonUtils;

import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 * Created by collinreed on 4/15/19.
 */
public class Vehicle {
    private final Logger LOGGER = Logger.getLogger(Vehicle.class.getName());
    public String year;
    public String make;
    public String model;
    public String trim;
    public String assembly;

    public enum VehicleType {
        VEHICLE_STAGGERED_1,
        VEHICLE_NON_STAGGERED_1;

        /**
         * Get the vehicle type based on the specified vehicle name.
         *
         * @param vehicleName               The specified vehicle name
         * @return                          The VehicleType enum value
         * @throws IllegalArgumentException Illegal Argument Exception
         */
        private static VehicleType getVehicleForStaggeredType(String vehicleName) throws IllegalArgumentException {
            for (VehicleType vehicleType : values()) {
                if (vehicleType.toString().equalsIgnoreCase(vehicleName)) {
                    return vehicleType;
                }
            }
            throw vehicleNotFound(vehicleName);
        }

        private static IllegalArgumentException vehicleNotFound(String outcome) {
            return new IllegalArgumentException(("Invalid vehicle [" + outcome + "]"));
        }
    }

    public Vehicle() {
    }

    /**
     * Get the vehicle details for the specified description
     *
     * @param  vehicleTypeDescription - Text describing the vehicle that should match one of the VehicleType enums
     * @return Vehicle instance
     */
    public Vehicle getVehicle(String vehicleTypeDescription) {
        LOGGER.info("getVehicle started");
        VehicleType vehicleType = VehicleType.getVehicleForStaggeredType(vehicleTypeDescription);
        Vehicle vehicle = new Vehicle();

        switch (vehicleType) {
            case VEHICLE_STAGGERED_1:
                vehicle.year = "2010";
                vehicle.make = "Chevrolet";
                vehicle.model = "Corvette";
                vehicle.trim = "Base";
                vehicle.assembly = "none";
                break;
            case VEHICLE_NON_STAGGERED_1:
                vehicle.year = "2012";
                vehicle.make = "Honda";
                vehicle.model = "Civic";
                vehicle.trim = "Coupe DX";
                vehicle.assembly = "none";
                break;
        }
        LOGGER.info("getVehicle completed");
        return vehicle;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty(Constants.LINE_SEPARATOR_SYSTEM_PROPERTY);

        //determine fields declared in this class
        Field[] fields = this.getClass().getDeclaredFields();

        //Field names paired with their values
        for (Field field : fields ) {
            try {
                if (CommonUtils.containsIgnoreCase(field.getName(), Constants.LOGGER))
                    continue;
                result.append(field.getName());
                result.append(": ");
                result.append(field.get(this));
            } catch (IllegalAccessException ex) {
                //continue
            }
            result.append(newLine);
        }
        return result.toString();
    }
}
