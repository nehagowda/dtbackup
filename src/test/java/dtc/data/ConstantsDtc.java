package dtc.data;

import common.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;

/**
 * Created by aaronbriel on 3/28/17.
 */
public class ConstantsDtc {

    public static final String DTD = "dtd";
    public static final String DT = "dt";
    public static final String AT = "at";

    public static final String DT_QA_DEFAULT_STORE = "AZF 01";
    public static final String DT_QA_DEFAULT_STORE_CITY = "flagstaff";
    public static final String DT_QA_DEFAULT_STORE_ADDRESS = "1230 S MILTON RD, FLAGSTAFF, AZ 86001";
    public static final String DT_QA_DEFAULT_STORE_PARTIAL_ADDRESS = "1230 s milton rd";
    public static final String DT_QA_DEFAULT_NEARBY_STORE_ADDRESS = "4920 n us highway 89";
    public static final String DT_QA_DEFAULT_STORE_CODE_PATH = "/store/az/flagstaff/s/1002";
    public static final String DT_QA_EA_DEFAULT_STORE_CODE_PATH = "/store/co/brighton/s/1851";
    public static final String DT_QA_DEFAULT_STORE_PHONE_NUMBER = "928-774-5273";
    public static final String DT_QA_DEFAULT_STORE_CODE = "1002";
    public static final String DT_QA_DEFAULT_STORE_TEXT_LABEL = "S MILTON";

    public static final String DT_STG_DEFAULT_STORE = "AZP 20";
    public static final String DT_STG_DEFAULT_STORE_CITY = "scottsdale";
    public static final String DT_STG_DEFAULT_STORE_ADDRESS = "9199 E TALKING STICK WAY, SCOTTSDALE, AZ 85250";
    public static final String DT_STG_DEFAULT_STORE_CODE_PATH = "/store/az/scottsdale/s/1022";
    public static final String DT_STG_DEFAULT_STORE_PHONE_NUMBER = "480-951-9126";
    public static final String DT_STG_DEFAULT_STORE_CODE = "1022";
    //TODO: Add text label for DT staging when it is being used there

    public static final String AT_QA_DEFAULT_STORE = "CAL 01";
    public static final String AT_QA_DEFAULT_STORE_CITY = "Chino";
    public static final String AT_QA_DEFAULT_STORE_ADDRESS = "11925 CENTRAL AVE, CHINO, CA 91710";
    public static final String AT_QA_DEFAULT_STORE_CODE_PATH = "/store/ca/chino/s/1038";
    public static final String AT_QA_DEFAULT_STORE_PHONE_NUMBER = "909-591-4501";
    public static final String AT_QA_DEFAULT_STORE_CODE = "1038";
    public static final String AT_QA_DEFAULT_STORE_TEXT_LABEL = "CHINO";

    public static final String AT_STG_DEFAULT_STORE = "CAN 19";
    public static final String AT_STG_DEFAULT_STORE_CITY = "redding";
    public static final String AT_STG_DEFAULT_STORE_ADDRESS = "16 HARTNELL AVE, REDDING, CA 96002";
    public static final String AT_STG_DEFAULT_STORE_CODE_PATH = "/store/ca/redding/s/1064";
    public static final String AT_STG_DEFAULT_STORE_PHONE_NUMBER = "530-221-0667";
    public static final String AT_STG_DEFAULT_STORE_CODE = "1064";
    //TODO: Add text label for AT staging when it is being used there

    public static final String DTD_DEFAULT_PHONE_NUMBER = "800-589-6789";

    public static final String DT_STG_INTEGRATED_STORE_CODE_PATH = "/store/az/scottsdale/s/1022";
    public static final String DT_QA_INTEGRATED_STORE_CODE_PATH = "/store/az/flagstaff/s/1002";

    public static final String PAYPAL = "paypal";
    public static final String DEFAULT = "default";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "Password";
    public static final String WITH_APPOINTMENT = "with appointment";
    public static final String WITHOUT_APPOINTMENT = "without appointment";
    public static final String INSTALL_WITHOUT_APPOINTMENT = "install without appointment";
    public static final String INSTALL_WITH_APPOINTMENT = "install with appointment";
    public static final String ENVIRONMENTAL_FEE = "Enviro";
    public static final String ENVIRONMENTAL_FEE_REAR = "Enviro REAR";
    public static final String OVERSIZED_PACKAGE_FEE = "Oversized Package Fee";
    public static final String CERTIFICATES = "Certificates";
    public static final String VALVE_STEM = "Valve Stem";
    public static final String TAX = "Tax";
    public static final String HUB = "Hub";

    public static final String IN_STOCK = "In Stock";
    public static final String AVAILABLE_NOW = "Available today";
    public static final String TIRES_ON_PROMOTION = "On promotion";

    public static final String ON_ORDER = "On Order";

    public static final String ZERO_IN_STOCK_AT_MY_STORE = "0 in stock at My Store";

    public static final String AVAILABILITY_3_5_DAYS_CALLTOCONFIRM =
            "Availability: Usually 3-5 days, call to confirm.";

    public static final String SEARCH = "Search";
    public static final String CONTINUE_SHOPPING = "Continue Shopping";
    public static final String VIEW_SHOPPING_CART = "View shopping cart";

    public static final String BRAND = "brand";
    public static final String IMAGE = "image";
    public static final String PRODUCT = "product";
    public static final String INVENTORY = "inventory";
    public static final String INVENTORY_MESSAGE = "inventoryMessage";
    public static final String ITEM = "item";
    public static final String IN_CART = "in cart";

    public static final String CONTINUE_TO_CHECKOUT = "Continue to Checkout";
    public static final String REMOVE_ITEM_FROM_CART = "Remove item from cart";
    public static final String PLACE_ORDER = "Place Order";
    public static final String SAVE_CARD_DETAILS = "SAVE CARD DETAILS";

    public static final String INSTALLATION = "Installation";
    public static final String ENVIRONMENTAL = "Environmental";
    public static final String DISPOSAL = "Disposal";
    public static final String ESTIMATED_TAXES = "Estimated Taxes";
    public static final String TAXES = "Taxes";
    public static final String CART_SUBTOTAL = "Cart Subtotal";
    public static final String TOTAL = "Total";
    public static final String DISPOSAL_FEE = "Disposal Fee";
    public static final String TIRES = "TIRES";
    public static final String WHEELS = "WHEELS";
    public static final String WHEELS_NAME = "Wheels";
    public static final String ORDERS_FILE = "orders.txt";

    public static final String AMERICAS_TIRE_STORE = "America's Tire Store";
    public static final String DISCOUNT_TIRE_STORE = "Discount Tire Store";
    public static final String DISCOUNT_TIRE_SITE = "Discount Tire";
    public static final String DISCOUNT_TIRE_DIRECT = "Discount Tire Direct";

    public static final String COMPARE_ITEM_MESSAGE = "SELECT 1 OR 2 MORE";

    public static final String ORDER = "order";

    public static final String UPPERCASE = "UPPERCASE";
    public static final String LOWERCASE = "LOWERCASE";

    public static final String DIRECTIONS = "Directions";

    public static final String stateDropDownString = "addressProvince_chosen";
    public static final String countryDropDownString = "country_chosen";

    public static final String AMERICAS_TIRE = "americastire";
    public static final String DISCOUNT_TIRE = "discounttire";

    public static final List<String> MY_STORE_POPUP_STORE_HOURS =
            Arrays.asList("Mon - Fri", "8AM - 6PM", "Saturday", "8AM - 5PM", "Sunday", "Closed");

    public static final List<String> STORE_DETAILS_STORE_HOURS =
            Arrays.asList("Mon-Fri: 8:00 AM - 6:00 PM", "Sat: 8:00 AM - 5:00 PM", "Sun: CLOSED");

    public static final String PLP = "PLP";
    public static final String PDP = "PDP";
    public static final String CHECKFIT = "CheckFit";
    public static final String SHOPPING_CART = "Shopping Cart";
    public static final String CHECK_AVAILABILITY = "Check Availability";
    public static final String CHECK_INVENTORY = "Check Inventory";
    public static final String CHECKOUT = "Checkout";
    public static final String APPOINTMENT = "Appointment";
    public static final String STORYBOOK = "Storybook";
    public static final String ORDER_CONFIRMATION = "Order Confirmation";
    public static final String APPOINTMENT_CONFIRMATION = "Appointment Confirmation";
    public static final String VIEW_ALL = "View All";
    public static final String TITLE_VIEW_ALL = "View+All";
    public static final String PERCENT_DISCOUNT = "% DISCOUNT";
    public static final String PERCENT_PERCENTAGE_DISCOUNT = "% PERCENTAGE DISCOUNT";
    public static final String TPMS = "TPMS";
    public static final String FETFEE_LABEL = "Federal Excise Tax (F.E.T.)";
    public static final String STAGGERED = "Staggered";
    public static final String MOBILE = "Mobile";
    public static final String ITEM_PRICE = "Item Price";
    public static final String SUBTOTAL = "Subtotal";
    public static final String INSTALLATION_FEE = "Installation Fee";
    public static final String PRODUCT_BRAND = "ProductBrand";
    public static final String PRODUCT_NAME = "ProductName";
    public static final String PRODUCT_SIZE = "ProductSize";
    public static final String ITEM_CODE = "ItemCode";
    public static final String SHOW_FEES = "Show Fees";
    public static final String CART_ITEMS = "Cart Items";
    public static final String VIEW_CART = "View Cart";
    public static final String CONTACT_INFORMATION = "Contact Information";
    public static final String MILE_WARRANTY = "mile warranty";
    public static final String LEARN_MORE = "Learn More";

    //TODO: We may be able to have API exposed to get installation and disposal fee via webservices
    public static final double INSTALLATION_FEE_QA_DT_AZF_01 = 19.00;
    public static final double INSTALLATION_FEE_QA_AT_CA_CAL_01 = 16.00;
    public static final double INSTALLATION_FEE_STG_AT_CAN_19 = 19.00;
    public static final double INSTALLATION_FEE_STG_DT_AZP_20 = 19.00;
    public static final double DISPOSAL_FEE_QA_DT_AZF_01 = 3.50;
    public static final double DISPOSAL_FEE_QA_AT_CAL_01 = 2.00;
    public static final double DISPOSAL_FEE_STG_AT_CAN_19 = 2.50;
    public static final double DISPOSAL_FEE_STG_DT_AZP_20 = 2.75;
    public static final double HUB_CENTRIC_RING_FEE = 5.00;
    public static final double WHEEL_INSTALL_KIT_FEE = 45.00;
    public static final int DEFAULT_QUANTITY = 4;
    public static final String PAGE_FIVE = "page=5";

    public static final String APPT_NOT_SURE_OF_AVAILABILITY = "Not sure of my availability";
    public static final String APPT_MAKE_AN_APPOINTMENT_AT_A_LATER_TIME = "Make an appointment at a later time";
    public static final String APPT_THESE_ITEMS_ARE_FOR_MULTIPLE_VEHICLES = "These items are for multiple vehicles";
    public static final String APPT_MY_PREFERRED_DATE_TIME_IS_NOT_AVAILABLE = "My preferred date/time is not available";
    public static final String DEFAULT_REASON = APPT_NOT_SURE_OF_AVAILABILITY;
    public static final String RESERVE_WITHOUT_APPOINTMENT_MESSAGE =
            "Upon product arrival, you will have 10 days to install the product on your vehicle.";
    public static final String RESERVE_WITHOUT_APPOINTMENT_MULTIPLE_VEHICLES_MESSAGE =
            "We may be able to accommodate you for an appointment with multiple vehicles. Contact your selected store.";
    public static final String RESERVE_WITHOUT_APPOINTMENT_WALK_IN_MESSAGE =
            "Walk-ins are always welcome to accommodate you";
    public static final String COUNTRY = "Country";
    public static final String PHONE_TYPE = "Phone Type";
    public static final String CONTINUE_TO_SHIPPING = "Continue To Shipping Method";
    public static final String CONTINUE_TO_PAYMENT = "Continue To Payment";
    public static final String MAKE_APPOINTMENT = "Make Appointment";
    public static final String CONTINUE_TO_CUSTOMER_DETAILS = "CONTINUE TO CUSTOMER DETAILS";
    public static final String ADD_CONTACT_INFO = "ADD CONTACT INFO.";
    public static final String CC_EXPIRE_MONTH = "Exp. Month";
    public static final String CC_EXPIRE_YEAR = "Exp. Year";
    public static final String ADD = "Add";
    public static final String CONTINUE_TO_PAYPAL = "Continue to Paypal";
    public static final String ABBREV_ENVIRONMENTAL_FEE = "Enviro. Fee";

    public static final String CHECKOUT_CUSTOMER_INFO_URL = "/checkout/customer-info";
    public static final String SCOTTSDALE_STORE_CODE_PATH = "/store/az/scottsdale/s/1344";
    public static final String USE_USPS_CORRECTED_ADDRESS = "Use USPS Corrected Address";

    public static final String DRIVING_PRIORITY_EVERYDAY = "EVERYDAY";
    public static final String CHECKOUT_WITH_SHIPPING = "Checkout with shipping";
    public static final String DATEPICKER_MESSAGE = "To schedule an installation for new tires "
            + "or wheels that you are purchasing today, pick a date up to 10 business days from now. "
            + "If you just need to schedule a routine service, pick a date up to 35 business days "
            + "from today, but walk-ins are always welcome.";
    public static final String ORDER_SUMMARY = "Order Summary";
    public static final String BASE_PRICE = "BasePrice";
    public static final String QUANTITY = "Quantity";
    public static final String TOTAL_PRICE = "TotalPrice";

    public static final String HUB_CENTRIC_RING = "Hub Centric Ring";
    public static final String WHEEL_INSTALL_KIT = "Wheel Install Kit";
    public static final String WHEEL_ELEMENT = " X";

    public static final String BEFORE = "before";
    public static final String AFTER = "after";
    public static final String BRAND_SEARCH_FIELD_PLACEHOLDER_TEXT = "Search for a partial or full brand name...";
    public static final String SWITCH_STORE_MESSAGE_ON_CART = "Switching to another store will clear your cart";
    public static final String CHANGE_STORE_DT = "ORP 02";
    public static final String NON_DEFAULT_STORE_ZIP_CODE_DT = "85250";
    public static final String NON_DEFAULT_STORE_ZIP_CODE_AT = "91709";
    public static final String MAKE_THIS_MY_STORE = "Make This My Store";
    public static final String STORE_DEFAULT_RESULT = "1";

    public static final String SWITCH_VEHICLE_POPUP_MESSAGE = "SWITCHING VEHICLE WILL CLEAR CART";
    public static final String CANCEL = "cancel";
    public static final String[] TIRE_BRANDS_URLS = {"/fitmentresult/tires/optionalPlusSize/225/40-18",
            "/tires/brands/michelin-catalog?q=:relevance:tireCategory:allSeasonTires&page=0",
            "/tires/brands/nitto-catalog?q=:relevance:tireCategory:allTerrainTires&page=0",
            "/tires/brands/nitto-catalog?q=:relevance:tireCategory:competitionTires&page=0",
            "/tires/brands/goodyear-catalog?q=:relevance:tireCategory:mudTerrainTires&page=0",
            "/tires/brands/goodyear-catalog?q=:relevance:tireCategory:passengerTires&page=0",
            "/tires/brands/goodyear-catalog?q=:relevance:tireCategory:performanceTires&page=0",
            "/tires/brands/goodyear-catalog?q=:relevance:tireCategory:summerTires&page=0",
            "/tires/brands/goodyear-catalog?q=:relevance:tireCategory:touringTires&page=0",
            "/tires/trailer-catalog?q=:price-asc:brands:brand-CAR&page=0",
            "/tires/brands/goodyear-catalog?q=:relevance:tireCategory:truckTires&page=0",
            "/tires/brands/goodyear-catalog?q=:relevance:tireCategory:winterTires&page=0"};

    public static final String[] WHEEL_BRANDS_URLS = {"/fitmentresult/wheels",
            "/fitmentresult/wheels/optionalPlusSize/15?q=:price-asc&page=0",
            "/wheels/brands/konig-catalog?q=:relevance:wheelCategory:paintedWheels&page=0",
            "/wheels/brands/konig-catalog?q=:relevance:wheelCategory:passengerWheels&page=0",
            "/wheels/brands/konig-catalog?q=:relevance:wheelCategory:multiSpokeWheels&page=0"};

    public static final String ATTR_STYLE = "style";
    public static final String EMAIL_SENT_MESSAGE = "An e-mail was sent to the address you've provided:";
    public static final String EMAIL_AUTHENTICATION_LINK_MESSAGE = "Please click the link in the e-mail to authenticate your account";
    public static final String EMAIL_ACCOUNT_NOT_AUTHENTICATED_MESSAGE = "This e-mail address is linked to an account that has not yet been authenticated";
    public static final String MY_ACCOUNT_INVALID_CREDS_MESSAGE = "The e-mail address and/or password you've entered is incorrect, please try again";
    public static final String CANNOT_SHIP_ITEMS_WARNING = "We cannot ship ";
    public static final String CANNOT_SHIP_LOCATIONS = "Canada : MB ,BC ,ON ,PE ,NT ,QC ,NS ,SK ,AB ,YT ,NB ,NL ,NU";
    public static final String CANNOT_SHIP_LOCATIONS_SAFARI = "Canada: MB ,BC ,ON ,PE ,NT ,QC ,NS ,SK ,AB ,YT ,NB ,NL ,NU";
    public static final String MAKE = "MAKE";
    public static final String EMAIL_ADDRESS_LABEL = "E-mail Address";
    public static final String OE_TIRE = "ORIGINAL EQUIPMENT TIRE";
    public static final String EDIT_SIGNIN_EMAIL_LABEL = "Edit Sign-in E-mail";
    public static final String UPDATED_EMAIL_SENT_MESSAGE = "You've successfully updated your e-mail address";
    public static final String UPDATED_EMAIL_AUTHENTICATION_LINK_MESSAGE = "A link was sent to the new address to verify" +
            " this change, click it and sign-in with the new credentials";
    public static final String EMAIL_VERIFICATION_LINK_MESSAGE = "Once you've placed your order, a verification e-mail " +
            "is sent to the address you've provided. In that e-mail is a link to authenticate your account.";
    public static final String EDIT_SIGN_IN_PASSWORD = "EDIT SIGN-IN PASSWORD";
    public static final String RESET_YOUR_PASSWORD = "RESET YOUR PASSWORD";
    public static final String VIEW_MY_ACCOUNT = "View my account";

    public static final String RIDE_COMFORT = "Ride Comfort";
    public static final String CORNERING_AND_STEERING = "Cornering and Steering";
    public static final String RIDE_NOISE = "Ride Noise";
    public static final String TREAD_LIFE = "Tread Life";
    public static final String DRY_TRACTION = "Dry Traction";
    public static final String PRODUCT_RECOMMENDATION = "Product Recommendation";
    public static final String ZIP_CODE = "Zip Code";
    public static final String ZIP = "Zip";
    public static final String MILES_DRIVEN = "Miles Driven";
    public static final String DRIVING_CONDITIONS = "Driving Conditions";
    public static final String TYPE_OF_DRIVING = "Type of Driving";
    public static final String TERMS_AND_CONDITIONS = "Terms and Conditions";
    public static final String CORNERING_APPERSAND_STEERING = "Cornering & Steering";
    public static final String EMPLOYEE_KNOWLEDGE_FRIENDLINESS = "Employee Knowledge/Friendliness";
    public static final String STORE_CLEANLINESS = "Store Cleanliness";
    public static final String OVERALL_RATING = "Overall Rating";
    public static final String STORE_RECOMMENDATION = "Store Recommendation";
    public static final String GENERAL_COMMENTS = "General Comments";

    public static final String MOSTLY_DRY = "Mostly Dry";
    public static final String MIXED_DRY_WET = "Mixed Dry/Wet";
    public static final String MOSTLY_WET = "Mostly Wet";
    public static final String SNOW = "Snow";

    public static final String MOSTLY_HIGHWAY = "Mostly Highway";
    public static final String MIXED_HIGHWAY_CITY = "Mixed Highway/City";
    public static final String MOSTLY_CITY = "Mostly City";
    public static final String OFF_ROAD = "Off Road";
    public static final String RACE = "Race";

    public static final String JOIN_SIGN_IN = "Join/Sign In";
    public static final String GUEST_CUSTOMER = "GUEST";
    public static final String REGISTERED_CUSTOMER = "REGISTERED";
    public static final String NOT_APPLICABLE = "N/A";
    public static final String MY_VEHICLES = "my vehicles";
    public static final String RECENTLY_SEARCHED_VEHICLES_MESSAGE = "Your 3 recently searched vehicles";
    public static final String CLEAR_RECENT_SEARCHES = "Clear Recent Searches";
    public static final String RECENT_SEARCHES = "Recent Searches";
    public static final String SAVED_VEHICLES = "Saved Vehicles";
    public static final String FACTORY_FRONT_TIRE = "Factory front tire";
    public static final String FACTORY_REAR_TIRE = "Factory rear tire";

    public static final String BOPIS = "Bopis";
    public static final String ROPIS = "ROPIS";
    public static final String MAIL_ORDER = "MAIL_ORDER";
    public static final String CAR_CARE_ONE = "CarCareOne";
    public static final String MASTER = "master";
    public static final String SERVICE_APPOINTMENT = "Service Appointment";
    public static final String ORDER_TYPE_SERVICE_APPOINTMENT = "SERVICE_APPOINTMENT";
    public static final String ROPIS_LABEL = "Reserve now, install at store";
    public static final String BOPIS_LABEL = "Pay now, install at store";
    public static final String DEALS_AND_REBATES = "Deals and Rebates";
    public static final String GREEN_COLOR_THREE_DIGIT = "rgb(70, 186, 43)";
    public static final String GREEN_COLOR_FOUR_DIGIT = "rgba(70, 186, 43, 1)";
    public static final String GREEN_COLOR_THREE_DIGIT_2 = "rgb(82, 162, 64)";
    public static final String GREEN_COLOR_FOUR_DIGIT_2 = "rgba(82, 162, 64, 1)";
    public static final String GREY_COLOR_THREE_DIGIT = "rgb(153, 153, 153)";
    public static final String GREY_COLOR_FOUR_DIGIT = "rgba(153, 153, 153, 1)";
    public static final String BLUE_COLOR_THREE_DIGIT = "rgb(0, 174, 239)";
    public static final String BLUE_COLOR_FOUR_DIGIT = "rgba(0, 174, 239, 1)";
    public static final String BLUE_COLOR_THREE_DIGIT_2 = "rgb(36, 106, 217)";
    public static final String BLUE_COLOR_FOUR_DIGIT_2 = "rgba(36, 106, 217, 1)";
    public static final String STATE = "State";
    public static final String STORE_LOCATOR = "Store Locator";
    public static final String FITMENT_POPUP = "Fitment popup";
    public static final String COMPARE_SETS = "Compare Sets" ;

    private static final String STORE_REVIEW_PARTIAL_URL = "/solicitedReview?userId=1111&emailAddress=" +
            "ima.customer@discounttire.com&firstName=ima&lastName=customer&storeName=1002&invoiceNumber=5555";

    private static final String PRODUCT_REVIEW_PARTIAL_URL = "&tdate=2014-01-01 &em=ima.customer@discounttire.com" +
            "&fn=ima%20&ln=customer%20&cid=1003014082&tnum=8216496&fpc=";

    private static final String STORE_THANKS_HEADER = "THANKS FOR TAKING TIME TO LET US KNOW HOW WE ARE DOING.";

    private static final String STORE_SINCERELY_APPRECIATE_MESSAGE = "We sincerely appreciate your time and " +
            "feedback. This information will assist other customers like you, with valuable first-hand experience " +
            "about this store and your visit.";

    private static final String PRODUCT_YOU_ARE_ALL_DONE_HEADER = "You're all done! Your review has been submitted";

    private static final String PRODUCT_WE_APPRECIATE_YOU_TAKING_TIME_MESSAGE = "We appreciate you taking the " +
            "time to help us out. Your feedback is very important!\nWe're going to review your input and post it " +
            "to our website within 48 to 72 hours.\nThanks again for your time!";
    public static final String SPECIAL_ORDER = "Special Order";
    public static final String STORE_CHANGED = "STORE CHANGED";

    public static HashMap<String, String> solicitedReviewPartialURL = new HashMap<>();
    public static final String MAIL_IN_REBATE = "Mail In Rebate";
    public static final String INSTANT = "Instant";
    public static final String INSTANT_SAVINGS = "Instant Savings";
    public static final String FIXED_DISCOUNT = "Fixed Discount";

    public static HashMap<String, String> completedSolicitedReviewHeader = new HashMap<>();

    public static HashMap<String, String> completedSolicitedReviewMessage = new HashMap<>();

    static {
        solicitedReviewPartialURL.put(Constants.STORE, STORE_REVIEW_PARTIAL_URL);
        solicitedReviewPartialURL.put(Constants.PRODUCT, PRODUCT_REVIEW_PARTIAL_URL);
        completedSolicitedReviewHeader.put(Constants.STORE, STORE_THANKS_HEADER);
        completedSolicitedReviewHeader.put(Constants.PRODUCT, PRODUCT_YOU_ARE_ALL_DONE_HEADER);
        completedSolicitedReviewMessage.put(Constants.STORE, STORE_SINCERELY_APPRECIATE_MESSAGE);
        completedSolicitedReviewMessage.put(Constants.PRODUCT, PRODUCT_WE_APPRECIATE_YOU_TAKING_TIME_MESSAGE);
    }

    public static final String SIGN_IN_TO_SKIP_THIS_STEP = "sign in to skip this step.";
    public static final String MOST_RECENT = "Most Recent";
    public static final String MOST_HELPFUL = "Most Helpful";
    public static final String LOWEST_RATED = "Lowest Rated";
    public static final String HIGHEST_RATED = "Highest Rated";
    public static final String PERCENTAGE = "%";
    public static final String MAP = "MAP";

    public static final String STORE_REVIEWS = "Store Reviews";

    public static final String THANKSGIVING_DAY = "Thanksgiving Day";
    public static final String STORE_CLOSED = "Store Closed";
    public static final String IMAGE_FILEPATH_DT = "/_ui/responsive/theme-dt-common/images/store-img-dt.png";
    public static final String IMAGE_FILEPATH_AT = "/_ui/responsive/theme-dt-common/images/store-img-at.jpg";

    public static final String CHECK_NEARBY_STORES = "check nearby stores";

    public static final String WARRANTY = "Warranty";

    public static final String FRONT = "FRONT";
    public static final String REAR = "REAR";
    public static final String SETS = "SETS";

    public static final String COMPARE_PRODUCTS = "Compare products";
    public static final String MY_STORE_INVENTORY = "MY STORE INVENTORY";
    public static final String INVENTORY_MESSAGE_TOOLTIP = "Installation appointments are available on the next business day after products arrive at your store.";
    public static final String ORDER_NOW = "Order now";
    public static final String NEED_IT_NOW = "iNeed it now?Call store";
    public static final String SHOW_MORE = "Show More +";
    public static final String DESCENDING = "Descending";
    public static final String SHOW_ALL = "Show all";
    public static final String ADD_AN_ITEM = "Add an item";

    public static final String WEB_ORDER_DATA_FILE_IN = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\Functions\\SAP\\Data Tables\\EA\\";
    public static final String EXTENDED_ASSORTMENT = "ExtendedAssortment";
    public static final String INSTALLATION_APPOINTMENT_HEADER = "Trying to make an installation appointment?";
    public static final String INSTALLATION_APPOINTMENT_LINE1 = "You can once we have your order in stock at the store location you picked.";
    public static final String INSTALLATION_APPOINTMENT_LINE2 = "We'll let you know when we receive your product from our warehouse!";
    public static final String NEARBY_STORES = "Nearby Stores";
    public static final String CONFIRM_APPOINTMENT = "Confirm Appointment";
    public static final String APPOINTMENT_CONFIRMATION_MESSAGE = "Your appointment is confirmed!";
    public static final String ADD_TO_CALENDAR = "add to calendar";
    public static final String ORDER_NUMBER = "Order Number";
    public static final String APPOINTMENT_DETAILS = "Appointment Details";
    public static final String STORE_DETAILS = "STORE DETAILS";

    public static final String MELBOURNE = "Melbourne";
    public static final String APPOINTMENT_URL = "https://dtqa1.epic.discounttire.com/schedule-appointment/installation/initiate?token=";
    public static final String APPOINTMENT_URL_STORE = "&storeName=1851";

    public static final String COMPARE_SELECTOR_COMPARABLE = "compare-selector__comparable";
    public static final String CANADA_CUSTOMER = "customer_can";

    public static final String EDIT_SHIPPING_DETAILS = "Edit Shipping Details";

    public static final String ORDER_NOW_AVAILABLE_IN_3_TO_5_DAYS = "Order now, available in 3 - 5 days";
    public static final String ORDER_NOW_AVAILABLE_IN_2_DAYS = "Order now, available in 2 days";
    public static final String ORDER_NOW_AVAILABLE_AS_SOON_AS_TOMORROW = "Order now, available as soon as tomorrow";
    public static final String ORDER_NOW_AVAILABLE_WITHIN_ONE_BUSINESS_DAY =
            "Order now, available within 1 business day";
    public static final String ZERO_STOCK = "0 In Stock";

    public static final List<String> ON_ORDER_MESSAGES = Arrays.asList(
            ORDER_NOW_AVAILABLE_IN_3_TO_5_DAYS,
            ORDER_NOW_AVAILABLE_IN_2_DAYS,
            ORDER_NOW_AVAILABLE_AS_SOON_AS_TOMORROW,
            ORDER_NOW_AVAILABLE_WITHIN_ONE_BUSINESS_DAY);

    public static final String EDIT_VEHICLE = "Edit Vehicle";
    public static final String FACTORY_TIRE_SIZE = "Factory tire size";
    public static final String OPTIONAL_TIRE_SIZE = "Optional tire size";
    public static final String OPTIONAL_TIRE_AND_WHEEL_SIZE = "optional tire and wheel size";
    public static final String NON_OE = "Non-OE";
    public static final String OE = "OE";
    public static final String O_E = "O.E.";

    public static final String VIEW_DETAILS = "View Details";
    public static final String ADD_TO_CART = "Add To Cart";
    public static final String ADD_TO_PACKAGE = "Add To Package";
    public static final String VIEW_ON_MY_VEHICLE = "View on my vehicle";
    public static final String TAX_POLICY = "Tax Policy";
    public static final String TAX_POLICY_CONTENT = "Current law requires the collection of any and all taxes " +
            "applicable, based on: items purchased, the ship-to address and/or the location of the selected " +
            "store for order pickup. If no sales tax is included on the details of your receipt, this does not " +
            "relieve you from sales tax liability on your purchase. In some states, use tax is imposed and must " +
            "be paid by the purchaser, unless otherwise exempt. Please check your local state's sales and use " +
            "tax requirements.\n" +
            "\n" +
            "At this time we are unable to complete tax exempt orders online. If you wish to purchase using your tax-exempt, " +
            "please call us at 800-938-3456 with your proof of exemption to complete your transaction.";
    public static final String COMPARE_TIRE_REVIEWS = "Compare tire reviews";
    public static final String CHARGED = "Charged";
    public static final String NOT_CHARGED = "Not-Charged";
    public static final String GEO_IP_URL = "?customerIp=";
    public static final String VEHICLE_IMAGE = "Vehicle Image";
    public static final String SHARE_ICON = "Share Icon";
    public static final String CLOSE_SYMBOL = "X";
    public static final String WHEEL_COLOR = "Wheel Color";
    public static final String WHEEL_SIZE = "Wheel Size";

    public static final String QUICK_FILTERS = "Quick Filters";
    public static final String BRANDS = "Brands";
    public static final String PRICE_RANGE = "Price Range";
    public static final String RATINGS = "Ratings";
    public static final String MILEAGE_WARRANTY = "Mileage Warranty";
    public static final String TIRE_CATEGORY = "Tire Category";
    public static final String GOOD_BETTER_BEST = "Good Better Best";
    public static final String SIDEWALL_DESCRIPTION = "Sidewall Description";
    public static final String SPEED_RATING = "Speed Rating";
    public static final String LOAD_RANGE = "Load Range";
    public static final String SECTION_WIDTH = "Section Width";
    public static final String ASPECT_RATIO = "Aspect Ratio";
    public static final String WHEEL_DIAMETER = "Wheel Diameter";
    public static final String RIM_DIAMETER = "Rim Diameter";
    public static final String RIM_WIDTH = "Rim Width";
    public static final String WIDTH = "Width";
    public static final String RATIO = "Ratio";
    public static final String DIAMETER = "Diameter";
    public static final String BOLT_PATTERN = "Bolt Pattern";
    public static final String OFFSET = "Offset";
    public static final String OE_DESIGNATION = "OE Designation";
    public static final String SIDEWALL_OPTION = "Sidewall Option";
    public static final String MANUFACTURE_AID = "manufacturer AID";
    public static final String WHEEL_CATEGORY = "Wheel Category";
    public static final String WHEEL_WIDTH = "Wheel Width";
    public static final String VENDOR_PRODUCT_NUMBER = "Vendor Product Number";
    public static final String VPN = "VPN";
    public static final String CLEAR_ALL = "Clear All";
    public static final String CLEAR_CART = "Clear Cart";
    public static final String APPLY_FILTERS = "Apply Filters";
    public static final String SHARE = "Share";
    public static final String FACEBOOK = "facebook";
    public static final String GOOGLE_PLUS = "Google Plus";
    public static final String GOOGLE = "google";
    public static final String INSTAGRAM = "instagram";
    public static final String PINTEREST = "pinterest";
    public static final String TWITTER = "twitter";
    public static final String CANONICAL = "Canonical";
    public static final String ADD_NEW_VEHICLE = "Add New Vehicle";
    public static final String ENTER_VEHICLE = "Enter vehicle";
    public static final String WHAT_IS_LOAD_RANGE = "what is Load Range";
    public static final String TIRE_SIZE = "Tire Size";
    public static final String LOAD_INDEX_SPEED_RATING = "Load Index / Speed Rating";
    public static final String LOAD_INDEX_WITH_RATING_KEY = "LoadIndexWithRatingKey";
    public static final String WHAT_IS_OE_DESIGNATION = "what is OE Designation";
    public static final String LOAD_INDEX = "Load Index";
    public static final String TOOL_TIP_CONTENT = "Tool Tip Content";
    public static final String SUB_TOTAL_TOOL_TIP = "Sub total tool tip";
    public static final String SUB_TOTAL_TOOL_TIP_MESSAGE = "Item price x quantity. Any applicable services or fees " +
            "provided in shopping cart.";
    public static final String TOP_RECOMMENDATION = "TOP RECOMMENDATION";
    public static final String PRIMARYDRIVINGLOCATIONERRORMESSAGE = "unable to find weather data for recommendation";
    public static final String TREADWELL = "treadwell";
    public static final String PRESENT = "present";
    public static final String SELECTED = "selected";
    public static final String VIEW_RECOMMEDED_TIRES = "view recommended tires";
    public static final String VALID = "valid";
    public static final String INVALID = "invalid";
    public static final String INCORRECT = "incorrect";
    public static final String PRIMARY_DRIVING_LOCATION = "Primary Driving Location";
    public static final String MILES_DRIVEN_PER_YEAR = "Miles Driven Per Year";
    public static final String MILESDRIVENPERYEARERRORMESSAGE = "Please enter a whole number from 5 - 55";
    public static final String NOT_DISPLAYED = "not displayed";
    public static final String DISPLAYED = "displayed";
    public static final String FOUND_IT_LOWER = "Found it Lower";
    public static final String INSTANT_PRICE_MATCH = "Instant Price Match";
    public static final String SELECTED_TIRE = "SELECTED TIRE";
    public static final String CHANGE_STORE = "Change store";

    public static final List<String> tireFilterCategoriesList = new ArrayList<>(Arrays.asList(QUICK_FILTERS, BRANDS,
            PRICE_RANGE, RATINGS, MILEAGE_WARRANTY, TIRE_CATEGORY, GOOD_BETTER_BEST, SIDEWALL_DESCRIPTION,
            SPEED_RATING, LOAD_RANGE, SECTION_WIDTH, ASPECT_RATIO, DIAMETER));

    public static final List<String> wheelFilterCategoriesList = new ArrayList<>(Arrays.asList(QUICK_FILTERS, BRANDS,
            PRICE_RANGE, WHEEL_CATEGORY, WHEEL_COLOR, DIAMETER, WHEEL_WIDTH));

    public static final String TAX_OFFLINE_MESSAGE = "We use a third-party service to calculate fees and " +
            "taxes. This service is currently down, so your current order total may need to be recalculated at a " +
            "later time. The total will be updated when the transaction is completed. Please call us with any " +
            "questions or concerns.";

    public static HashMap<String, String> quickFilters = new HashMap<>();

    // Add more filter options here as needed
    static {
        quickFilters.put("Treadwell", "Treadwell Tested");
        quickFilters.put("Fuel Efficient", "Fuel Efficient");
        quickFilters.put("In Stock", "In Stock");
        quickFilters.put("Spare Tire", "Spare Tire");
        quickFilters.put("Arizonian", "brand-ARZ");
        quickFilters.put("Continental", "brand-CON");
        quickFilters.put("Falken", "brand-FAL");
        quickFilters.put("Michelin ", "brand-MCH");
        quickFilters.put("Nexen Tire", "brand-NEX");
        quickFilters.put("Road Hugger", "brand-RHG");
        quickFilters.put("Yokohama", "brand-YOK");
        quickFilters.put("Bridgestone", "brand-BRI");
        quickFilters.put("All-Season", "allSeasonTires");
        quickFilters.put("Passenger", "passengerTires");
        quickFilters.put("Performance", "performanceTires");
        quickFilters.put("Touring", "touringTires");
        quickFilters.put("Best", "Best");
        quickFilters.put("Better", "Better");
        quickFilters.put("Good", "Good");
        quickFilters.put("Black Side Wall", "Black Side Wall");
        quickFilters.put("Vertical Serrated Band", "Vertical Serrated Band");
        quickFilters.put("S - Up to 112 mph", "S - Up to 112 mph");
        quickFilters.put("T - Up to 118 mph", "T - Up to 118 mph");
        quickFilters.put("H - Up to 130 mph", "TH - Up to 130 mph");
        quickFilters.put("SL", "SL");
        quickFilters.put("XL", "XL");
        quickFilters.put("4-100.0","4-100.0");
        quickFilters.put("Original Equipment","Original Equipment");
        quickFilters.put("On Promotion","On Promotion");
    }

    public static HashMap<String, String> productBrandLogos = new HashMap<>();

    // Add more logos here as needed
    static {
        productBrandLogos.put("MICHELIN", "logo_michelin_tire.png");
        productBrandLogos.put("FALKEN", "logo_falken_tire.png");
        productBrandLogos.put("GOODYEAR", "logo_goodyear_tire.png");
        productBrandLogos.put("NITTO", "logo_nitto_tire.png");
        productBrandLogos.put("BRIDGESTONE", "logo_bridgestone_tire.png");
        productBrandLogos.put("BFGOODRICH", "logo_bfgoodrich_tire.png");
        productBrandLogos.put("PIRELLI", "logo_pirelli_tire.png");
        productBrandLogos.put("YOKOHAMA", "logo_yokohama_tire.png");
        productBrandLogos.put("MB WHEELS", "logo_mb_wheel.png");
        productBrandLogos.put("RAGE", "logo_rage_wheel.png");
        productBrandLogos.put("TSW", "logo_tsw_wheel.png");
        productBrandLogos.put("AMERICAN RACING", "logo_american-racing_wheel.png");
        productBrandLogos.put("DRAG", "logo_drag_wheel.png");
    }

    public static final String TREADWELL_TITLE = "Your Personal Tire Guide.";
    public static final String TREADWELL_SUBTITLE = "Powered by data, driven by you.";
    public static final String APPOINTMENT_MESSAGE_BAR_LABEL = "Appointment Selected:";
    public static final String TIME_LABEL = "Time:";
    public static final String READ_REVIEWS = "Read Reviews";
    public static final String SCHEDULE_APPOINTMENT = "Schedule appointment";
    public static final String ALL = "All";
    public static final String OUTLOOK = "Outlook";
    public static final String YES_DELETE_THIS_VEHICLE = "Yes, delete this vehicle";
    public static final String MY_ACCOUNT = "My Account";
    public static final String DONE_EDITING = "Done Editing";
    public static final String HELLO = "Hello, ";
    public static final String STORE_SERVICES = "Store Services";
    public static final String CAROUSEL_BUTTON = "Carousel button";
    public static final String ICON = "Icon";
    public static final String COPIED = "Copied";
    public static final String TEXT_SUCCESSFULLY_SENT = "Text Successfully Sent";
    public static final String ACTION_NEEDED = "Action needed";
    public static final String ALL_RATINGS = "All Ratings";
    public static final String FIVE_STARS = "5 Stars";
    public static final String FOUR_STARS = "4 Stars";
    public static final String THREE_STARS = "3 Stars";
    public static final String TWO_STARS = "2 Stars";
    public static final String ONE_STARS = "1 Stars";
    public static final String FILTER = "Filter";
    public static final String CREATE_ACCOUNT = "Create Account";
    public static final String LOAD_MORE_REVIEWS = "Load More Reviews";
    public static final String EACH = "each";
    public static final String MORE_STORES_NEAR_THIS_STORE = "More Stores Near This Store";
    public static final String MILES_ABBREV = "mi";
    public static final String CHECK_FOR_AVS_POPUP = "check for AVS popup";
    public static final String COMPARE_PRODUCT_URL_SEGMENT = "compare/product";
    public static final String SEE_PRICE_IN_CART = "See price in cart";
    public static final String PRICE_IN_CART = "price in cart";
    public static final String INSTALLED_PRICE_IN_CART = "Installed price in cart";
    public static final String STORES_LOCATED_BY = "Stores located by";
    public static final String SHOP_DISCOUNT_TIRE_DIRECT = "SHOP DISCOUNT TIRE DIRECT";
    public static final String WELCOME = "Welcome";
    public static final String INSTALLERS ="Installers";
    public static final String CHECKOUT_NOW = "checkout now";
    public static final String EDIT_ENTERED_ADDRESS = "Edit Entered Address";
    public static final String CREATE_AN_ACCOUNT = "CREATE AN ACCOUNT";
    public static final String SEARCH_BAR = "search bar";
    public static final String NO_MATCH_FOUND = "No match found";
    public static final String ORDERS = "Orders";
    public static final String COLLAPSED = "collapsed";
    public static final String NO_SEARCH_RESULT_MESSAGE = "There are no stores to show based on your query.";
    public static final String VALID_SEARCH_RESULT_MESSAGE = "is more than 250 mi away.";
    public static final String DT_AT_SHOP_DTD_MESSAGE = "Purchase tires and wheels online, and ship to your door. Free shipping options are available.";
    public static final String DTD_SHOP_DTD_MESSAGE = "Shop for tires and wheels online with free shipping options available.";
    public static final String EXPAND_SEARCH_RESULT_MESSAGE = "We couldn't find any stores within 100 miles, so we are expanding your search to 250 miles.";
    public static final String SHOP_TIRES = "Shop Tires";
    public static final String SHOP_WHEELS = "Shop Wheels";
    public static final String USE_THIS_STORE = "USE THIS STORE";
    public static final String VIEW_TIRES = "view tires";
    public static final String VIEW_WHEELS = "view wheels";
    public static final String CREATE_ACCOUNT_ADDRESS_LINE_1 = "create account address line 1";
    public static final String CREATE_ACCOUNT_ADDRESS_LINE_2 = "create account address line 2";
    public static final String CREATE_ACCOUNT_ZIPCODE = "create account zipcode";
    public static final String SHOW_REQUIRED_FEES = "show required fees";
    public static final String SHOW_ADD_ONS = "show add-ons";
    public static final String YOUR_APPOINTMENT_IS_SET_MESSAGE = "Your store has the details of your " +
            "service appointment and will be ready for you at the designated time. " +
            "An appointment confirmation has been sent to";
    public static final String WHEEL_CONFIGURATOR_URL_SEGMENT = "wheel-configurator";
    public static final String OUR_PAYMENT_SYSTEM_IS_DOWN = "our payment system is currently down";
    public static final String DATA = "data";
    public static final String CUSTOMERID = "customerId";
    public static final String REMOVE = "Remove";
    public static final String SAVE = "save";
    public static final String PAY_ONLINE = "Pay Online";
    public static final String PAY_IN_STORE = "Pay in Store";
    public static final String CHECKOUT_WITH_APPOINTMENT = "Checkout with appointment";
    public static final String SALES_TAX = "sales tax";
    public static final String SIGN_IN = "Sign-in";
    public static final String YOUR_APPLICATION = "your application";
    public static final String TIPS_AND_GUIDES = "Tips & Guides";
    public static final String COLOR_BLACK = "black";
    public static final String TIRE_SEARCH = "Tire Search";
    public static final String WHEEL_SEARCH = "Wheel Search";
    public static final String CREDIT = "Credit";
    public static final String FINANCE = "Finance";
    public static final String DISCOUNT_TIRE_CREDIT_CARD = "Discount Tire Credit Card";
    public static final String ADDITIONAL_LINKS = "Additional Links";
    public static final String ABOUT_US = "About Us";
    public static final String APPLY_NOW = "Apply Now";
    public static final String LOGIN_SCREEN = "Login Screen";
    public static final String COMMERCIAL_PAYMENTS = "Commercial Payments";
    public static final String OUR_STORY = "Our Story";
    public static final String WHEEL_AND_TIRE_SERVICES = "Wheel and Tire Services";
    public static final String MOTORSPORTS = "Motorsports";
    public static final String JOIN_OUR_TALENT_NETWORK = "Join Our Talent Network to Receive Job Alerts";
    public static final String CAREERS = "Careers";
    public static final String TIRE_SAFETY = "Tire Safety";
    public static final String TIRE_SIZE_CALCULATOR = "Tire Size Calculator";
    public static final String CHECK_TIRE_PRESSURE = "Check Tire Pressure";
    public static final String MORE_TOPICS = "More Topics...";
    public static final String CUSTOMER_CARE = "Customer Care";
    public static final String APPOINTMENTS = "Appointments";
    public static final String RETURN_POLICY = "Return Policy";
    public static final String REGIONAL_OFFICES = "Regional Offices";
    public static final String YOUTUBE = "youtube";
    public static final String TIRE_SIZE_AND_CONVERSION_CALCULATOR = "Tire Size and Conversion Calculator";
    public static final String CHECKING_TIRE_AIR_PRESSURE = "Checking Tire Air Pressure";
    public static final String CHECKING_AIR_PRESSURE = "Checking Air Pressure";
    public static final String CUSTOMER_SERVICE = "Customer Service";
    public static final String OUR = "Our";
    public static final String DOT_COM = ".com/";
    public static final String TIRES_DOT_COM = "tiresdotcom";
    public static final String DISCOUNT_UNDER_TIRE = "discount_tire";
    public static final String PLUS_GOOGLE = "plus.google";
    public static final String SHOP_PRODUCTS = "Shop Products";
    public static final String NO_PRICE = "No Price";
    public static final String OE_SIZE = "OE size";
    public static final String BEST_SELLER = "Best Seller";
    public static final String ITEM_ADDED_TO_CART = "Item added to cart";
    public static final String CHOOSE_PRODUCT_ADD_ONS_MESSAGE = "You can choose product add-ons and schedule installation during check out";
    public static final String LUG = "Lug";
    public static final String FUTURE_DATE_MUST_SUPERCEDE_CURRENT_DATE = "Future date must supersede the current date";
    public static final String AVAILABILITY = "Availability";
    public static final String ITEM_NUMBER_PREFIX = "Item# ";
    public static final String NO_VEHICLE_SELECTED = "No vehicle selected";
    public static final String ADD_VEHICLE = "Add Vehicle";
    public static final String YES_CLEAR_CART = "Yes, Clear Cart";
    public static final String YES_CLEAR_RECENT_SEARCHES = "Yes, Clear Recent Searches";
    public static final String NO_KEEP_THESE_SEARCHES = "No, Keep These Searches";
    public static final String ARMED_FORCE = "Armed Force";
    public static final String STATE_AP = "AP";
    public static final String STATE_NEVADA = "Nevada";
    public static final String WHEEL_CONFIGURATOR = "Wheel configurator";
    public static final String PRODUCT_NAME_CART = "Product name cart";
    public static final String PRICE_IS_MESSAGE_CLASS_SEGMENT = "price__is-message";
    public static final String SIGN_INTO_YOUR_ACCOUNT = "SIGN INTO YOUR ACCOUNT";
    public static final String TRENDING_NEAR = "Trending near";
    public static final String APPOINTMENT_DATE = "Appointment Date";
    public static final String APPOINTMENT_TIME = "Appointment Time";
    public static final String HOMEPAGE = "Homepage";
    public static final String CONTINUE_WITHOUT_VEHICLE = "Continue without vehicle";
}